### %ProjectDir% Restart {#restart}

Сервис %ShortProjectName% был недавно перезапущен.

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::General::Apps State, отражающий состояние узла.
2. Проверить состояние узла командой `systemctl status %ProjectService%`.
3. Определить время перезапуска и время, прошедшее с момента перезапуска.
4. Если узел не активен, то запустить командой `sudo systemctl start %ProjectService%`.
5. Выполнить команду `coredumpctl` для вывода информации о дампе памяти, записавшегося при нештатном завершении %ProjectName%.
6. Проверить журнал `/usr/protei/%ProjectDir%/logs/history/$Date$/err.log`.

### %ProjectDir% Restarts counter {#restart-counter}

* Уровень: WARN<br>
  Сервис %ShortProjectName% был перезапущен 3 и более раз в течение 10 минут.
* Уровень: ERR<br>
  Сервис %ShortProjectName% был перезапущен 6 и более раз в течение 10 минут.

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::General::Apps State, отражающий состояние узла.
2. Проверить состояние командой `systemctl status %ProjectService%`.
3. Определить время перезапуска и время, прошедшее с момента перезапуска.
4. Если узел не активен, то запустить узел командой `sudo systemctl start %ProjectService%`.
5. Выполнить команду `coredumpctl` для вывода информации о дампе памяти, записавшегося при нештатном завершении %ProjectName%.
6. Проверить журнал `/usr/protei/%ProjectDir%/logs/history/$Date$/err.log`.

### %ProjectDir% Stop {#stop}

* Уровень: ERR<br>
  Сервис %ShortProjectName% остановлен на хосте, systemd.unit не запущен.

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::General::Apps State, отражающий состояние узла.
2. Проверить состояние узла командой `systemctl status %ProjectService%`.
3. Если узел не активен, то запустить командой `systemctl start %ProjectService%`.

### %ProjectDir% RSS Usage {#rss-usage}

* Уровень WARN:<br>
  Потребление оперативной памяти сервисом %ShortProjectName% превышает ожидаемое значения не менее, чем в 2 раза.
* Уровень ERR:<br>
  Потребление оперативной памяти сервисом %ShortProjectName% превышает ожидаемое значения не менее, чем в 3 раза.

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::General::RSS Usage, отражающий уровень использования оперативной памяти.
2. Открыть в Grafana график %ShortProjectName% Application Metrics::General::%ShortProjectName% Storage Records, отражающий количество записей в оперативной памяти %ProjectName%.

### %ProjectDir% CPU Usage {#cpu-usage}

* Уровень WARN:<br>
  Потребление ресурсов CPU сервисом %ShortProjectName% превышает ожидаемое значения не менее, чем в 1,5 раза.
* Уровень ERR:<br>
  Потребление ресурсов CPU сервисом %ShortProjectName% превышает ожидаемое значения не менее, чем в 2 раза.

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::General::CPU Usage, отражающий уровень использования процессора.
2. Открыть в Grafana график %ShortProjectName% Application Metrics::Profilers::CPU usage by state machines, отражающий уровни использования процессора каждой машиной состояний.

### High Disk Space Usage {#space-usage}

* Уровень WARN:<br>
  Доля заполнения диска превышает 85%.
* Уровень ERR:<br>
  Доля заполнения диска превышает 95%.

#### Причины

#### Решение

1. Открыть в Grafana график System::Disk IO on $NodeName$::Disk Utilization, отражающий уровень заполненности томов на узлах.
2. Определить размер пространства, который необходимо освободить.
3. Открыть в Grafana график System::Quick overview for $NodeName$::Mounting points table, отражающий уровень заполненности томов диска.
4. Открыть в Grafana график System::Mounting points on $NodeName$, отражающий уровень использования дискового пространства для каждого процесса.
5. Проверить заполнение дискового пространства командой `df -h`.

### Host Unreachable {#unreachable}

* Уровень WARN:<br>
  Доля потерянных пакетов превышает 75%, %ProjectName% недоступен с одного или более хостов.
* Уровень ERR:<br>
  Доля потерянных пакетов превышает 75%, %ProjectName% недоступен со всех остальных узлов.

#### Причины

#### Решение

1. Открыть в Grafana график System::Ping $NodeName$::Ping packets loss, отражающий потери пакетов ответа на запрос PING до сетевых узлов.
2. Определить узлы, от которых были потери пакетов в ответе на запрос PING.
3. Открыть в Grafana график System::Ping $NodeName$::Ping response duration, отражающий время ожидания ответа на запрос PING до сетевых узлов.
4. Определить узлы, от которых время ожидания ответа на запрос PING существенно отличается от среднего.
5. Открыть в Grafana график System::Network on $NodeName$::Physical Interfaces, отражающий состояние физических интерфейсов.
6. Определить неактивные интерфейсы, если таковые есть.

### %protocol% Link State {#%protocol_lower%-state}

#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::%protocol% Peers::%protocol% State, отражающий состояние соединений %protocol%.

или

1. Отправить cli-команду `/usr/protei/%ProjectDir%/scripts/%protocol%_state` для получения информации о состоянии узлов Diameter.

### %interface% Peer State {#%interface_lower%-state}



#### Причины

#### Решение

1. Открыть в Grafana график %ShortProjectName% Application Metrics::%interface% Peers::%interface% Peer State, отражающий состояние соединений %interface%.

или

1. Отправить API-запрос `/usr/protei/%ProjectDir%/scripts/get_%interface_lower%_peers` для получения информации о состоянии узлов %interface%.

