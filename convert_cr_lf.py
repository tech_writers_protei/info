from glob import iglob
from pathlib import Path
import string

from chardet import detect

CRLF = "\r\n"
LF = "\n"

letters: str = f"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦШЧЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцшчщъыьэюя{string.printable}"


def convert(path: str | Path, __from: str, __to: str):
    with open(path, "r", errors="ignore") as f:
        _content: str = f.read()

    _content: str = _content.replace(__from, __to)

    with open(path, "w", newline="") as f:
        f.write(_content)


def convert_file(path: str | Path):
    with open(path, "r+b") as f:
        _content: bytes = f.read()

    with open(path, "w+b") as f:
        # _content: str = _content.encode("cp1251").decode("cp1251")
        _ = _content.decode("cp1251")
        print(f"{_}")
        f.write(_.encode("utf-8"))


def get_encoding(path: str | Path):
    with open(path, "r+b") as fb:
        _ = fb.read()
    print(detect(_).get("encoding"))
    print(detect(_).items())


def find_non_ascii(path: str | Path):
    with open(path, "r", errors="ignore") as fb:
        _ = fb.read()
    __flag = True

    for index, s in enumerate(iter(_)):
        if s not in letters:
            print(f"{path}: symbol {s} position {index + 1} fail")
            __flag = False

    return __flag



def convert_files(path: str | Path):
    for file in iglob("**/*", root_dir=path, recursive=True):
        path_file = Path(path).joinpath(file).resolve()

        if path_file.suffix in (".md", ".adoc"):
            print(f"File {path_file}")
            # convert(path_file, CRLF, LF)
            # print(f"{path_file} is ok: {find_non_ascii(path_file)}")
            convert_file(path_file)
            print(f"File {path_file} is ready")
    print("Finish")


if __name__ == '__main__':
    # get_encoding(r"C:\Users\tarasov-a\PycharmProjects\Protei_MME\PDF_Protei_MME.yml")
    # get_encoding(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\PDF_HLR_HSS.yml")
    # convert(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\PDF_HLR_HSS.yml", LF, CRLF)
    # convert(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common\_index.md", LF, CRLF)
    convert_file(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\PDF_HLR_HSS.yml")
    convert_file(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common\_index.md")
    # convert_files(r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common")
