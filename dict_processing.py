from typing import Mapping


def get_keys_by_value(dictionary: Mapping[str, list[str]], value: str):
    # if value not in dictionary.values():
    #     raise ValueError

    return [k for k, values in dictionary.items() if value in values]


def get_unique_values(dictionary: Mapping[str, list[str]]):
    return set(v for values in dictionary.values() for v in values)


if __name__ == '__main__':
    c_libs = {
        "rapidjson": ["HSS", "MME", "SGSN", "UDM", "NRF", "AUSF", "AMF", "SCP", "SigFW", "SGSN", "DPI.TLS-VALIDATOR",
                      "DPI.QOS-BROKER", "DPI.TF"],
        "pugixml": ["MI", "SMSW"],
        "hiredis": ["UDM", "AUSF", "SGSAPLB", "SigFW", "HDC"],
        "libpcap": ["SigMonitor", "SigFW", "SGSN", "MME", "DPI.CS"],
        "pcap++": ["MME", "SGSN"],
        "libcurl": ["CAPL", "SG", "SMSC", "DPI.CS"],
        "googletest, gmock": ["SigMonitor", "MME", "SGSN", "UDM", "AUSF", "NRF", "SCP", "S1APLB", "GTPLB", "SGSAPLB",
                              "SigFW", "STP", "DPI.TLS-VALIDATOR", "DPI.MED", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "nlohmann": ["UDM", "AUSF", "NRF", "SCP", "S1APLB", "GTPLB", "SGSAPLB", "SigFW", "SG", "DPI.TLS-VALIDATOR",
                     "DPI.QOS-BROKER", "DPI.TF"],
        "openssl": ["SigMonitor", "UDM", "AUSF", "NRF", "SCP", "AUSF", "SigFW", "RBC", "MME", "SG", "SMSC", "SMSW",
                    "SGSN", "DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.MED", "DPI.TF", "DPI.CS"],
        "asn1c": ["SigMonitor", "S1APLB", "MME", "RBC"],
        "stdc++fs": ["UDM", "MME", "SMSC"],
        "cpptoml": ["S1APLB", "GTPLB", "SGSAPLB"],
        "fmt": ["S1APLB", "GTPLB", "SGSAPLB", "SigFW", "SGSN", "DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF",
                "DPI.CS"],
        "spdlog": ["S1APLB", "GTPLB", "SGSAPLB", "DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF"],
        "spdlog_setup": ["S1APLB", "GTPLB", "SGSAPLB"],
        "concurrentqueue": ["S1APLB", "GTPLB", "SGSAPLB"],
        "libicuio": ["SigFW"],
        "google-benchmark": ["SigFW", "DPI.TLS-VALIDATOR", "DPI.MED", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "dpdk": ["SigFW", "DPI.TF", "DPI.CS"],
        "jsoncpp": ["SigFW", "MME", "SGSN", "DPI.CS"],
        "prometheus": ["SigFW", "SG", "SMSC"],
        "gsl-lite": ["SigFW", "DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "linux-rdma": ["SigFW"],
        "numactl": ["SigFW", "DPI.TF", "DPI.CS"],
        "mariadbclient, mariadbpp": ["MME"],
        "mimalloc": ["MME", "DRA", "DPI.CS"],
        "mysqlcppconn, mysqlclient": ["RBC", "SMSW"],
        "cpp-httplib": ["S1APLB", "GTPLB", "SGSAPLB"],
        "nameof": ["SGSN"],
        "expected": ["SGSN", "DPI.TLS-VALIDATOR", "DPI.MED", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "optional": ["SGSN", "DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "namedType": ["SGSN"],
        "libxml2": ["SGSN"]
    }

    c_components = {
        "mariadb_cpp": ["HSS", "MI", "RBC", "OTA", "SMSFW", "LBS", "UDR", "HSM", "RBC", "MME", "SMSW", "SGSN"],
        "galeracluster": ["HSS", "MI", "MME"],
        "kdb(redis)": ["UDM", "AUSF", "DRA", "SigFW", "GLR", "SGSAPLB", "SGSN", "MME"],
        "oracledb": [],
        "sqlite": ["DPI.MED", "DPI.CS"],
        "cli11": ["DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "cpr": ["DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "json11": ["DPI.TF", "DPI.CS"],
        "yaml-cpp": ["DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "clickhouse-cpp": ["DPI.TF", "DPI.CS"],
        "parallel-hashmap": ["DPI.TLS-VALIDATOR", "DPI.TF", "DPI.CS"],
        "hyperscan": ["DPI.TF", "DPI.CS"],
        "lksctp-tools": ["DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "libev": ["DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "http_parser": ["DPI.TLS-VALIDATOR", "DPI.QOS-BROKER", "DPI.TF", "DPI.CS"],
        "akamai-radix-tree": ["DPI.TF", "DPI.CS"],
        "sbe": ["DPI.TF", "DPI.CS"],
        "leveldb": ["DPI.CS"],
        "tinycbor": ["DPI.CS"],
        "zlib": ["DPI.CS"],
        "openldap": ["DPI.CS"],
        "pcre2": ["DPI.CS"],
        "cityhash": ["DPI.CS"]
    }

    java_components = {
        "mariadb_java": ["OTA", "SMSC", "PRBT", "DPDP.web", "Stp_3.0"],
        "clickhouse": ["OTA", "SMSC", "SigTracer"],
        "openjdk": ["DPI.TF", "DPI.CS"],
        "Mysql": ["SMSC", "PRBT", "DPDP.web", "ADC", "BSP", "PNR"],
        "Kafka": ["SMSC"],
        "oracle": ["TravelSim", "BulkMTT", "BulkXVLR", "BulkAquafon", "OnlineBilling"],
        "postgresql": ["SigTracer", "OnlineBilling"],
        "h2database": ["TravelSim", "Stp_3.0", "PNR"]
    }

    _dict = {**c_libs, **c_components, **java_components}

    for i in get_unique_values(_dict):
        libs: list[str] = get_keys_by_value(_dict, i)
        _str_libs: str = "\n".join(libs)
        print(f"{i}\n---------\n{_str_libs}\n\n")
