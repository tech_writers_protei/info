[[GetUser]]
= GetUser

Команда `GetUser` позволяет получать параметры пользователя системы.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetUser \
  -d '{ "login": "<string>" }'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|login |Логин пользователя системы. |string |M
|===

.Пример тела запроса
[source,json]
----
{
  "login": "alisa"
}
----

== Ответ

В ответе передаются статус запроса и параметры пользователя системы.

....
{
  "status": "<string>",
  "user": <User>
 }
....

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|user |Параметры пользователя. |<<entities/user.adoc#User,User>> |O
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "user": {}
}
----
