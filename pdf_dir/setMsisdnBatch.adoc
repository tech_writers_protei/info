[[SetMSISDNBatch]]
= SetMSISDNBatch

Команда `SetMSISDNBatch` позволяет изменять номера MSISDN для нескольких абонентов.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/SetMSISDNBatch \
  -d '{
  "requests": [ <SetMSISDN> ],
  "skipIfNotFound": <bool>
}'
----

== Поля тела запроса

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|requests |Запрос на изменение номера MSISDN. |[<<setMsisdn.adoc#SetMSISDN,SetMSISDN>>] |M
|skipIfNotFound |Флаг пропуска не существующих номеров MSISDN. |bool |M
|===

.Пример тела запроса
[source,json]
----
{
  "requests": [
    {
      "imsi": "250010000002",
      "msisdn": "79100000002"
    }
  ],
  "skipIfNotFound": true
}
----

== Ответ

В ответе передается только статус запроса.
