[[GetGroup]]
= GetGroup

Команда `GetGroup` позволяет получать пользовательскую группу.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetGroup \
  -d '{
  "name": "<string>",
  "id": <int>
}'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|[[name-get-group]]name |Название пользовательской группы. |string |C
|[[id-get-group]]id |Идентификатор пользовательской группы. |int |C
|===

NOTE: Задается только один из параметров <<name-get-group,name>> или <<id-get-group,id>>.

.Пример тела запроса
[source,json]
----
{ "name": "gr" }
----

== Ответ

В ответе передаются статус запроса и параметры пользовательской группы.

....
{
  "status": "<string>",
  "group": <Group>
}
....

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|group |Пользовательская группа. |<<entities/group.adoc#Group,Group>> |M
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "group": {}
}
----
