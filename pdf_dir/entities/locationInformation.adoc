[[LocationInformation]]
== Параметры местоположения абонента LocationInformation

[width="100%",cols="21%,29%,46%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M
|epsUserState |Статус абонента в сети EPS. |<<entities/epsUserState.adoc#EpsUserState,EpsUserState>> |O
|epsLocationInformation |Местоположение в сети EPS. |<<entities/epsLocationInformation.adoc#EpsLocationInformation,EpsLocationInformation>> |O
|psSubscriberState |Код статуса абонента в домене PS. |int |O
|csSubscriberState |Код статуса абонента в домене CS. |int |O
|csLocationInformation |Местоположение в домене CS. |<<entities/csPsLocationInformation.adoc#CsPsLocationInformation,CsPsLocationInformation>> |O
|psLocationInformation |Местоположение в домене PS. |<<entities/csPsLocationInformation.adoc#CsPsLocationInformation,CsPsLocationInformation>> |O
|===

.Пример
[source,json]
----
{
  "epsLocationInformation": {
    "mmeLocationInformation": {
      "ageOfLocationInformation": 22,
      "currentLocationRetrieved": 1,
      "eUtranCellGlobalIdentity": "aaaa",
      "geodeticInformation": "cccc",
      "geographicalInformation": "bbbb",
      "trackingAreaIdentity": "1a2f",
      "userCsgInformation": {
        "csgAccessMode": 1,
        "csgId": 1,
        "csgMembershipIndication": 1
      }
    }
  },
  "epsUserState": {
    "mmeUserState": 1
  }
}
----
