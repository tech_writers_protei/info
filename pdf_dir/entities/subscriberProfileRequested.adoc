[[SubscriberProfileRequested]]
== Параметры профиля абонента при запросе SubscriberProfileRequested

[options="header",cols="3,8,3,1,1"]
|===
|Поле |Описание |Тип |O/M |Версия
|imsi |Номер IMSI абонента. |string |М |
|msisdn |Номер MSISDN абонента. |string |O |
|<<administrative-state-profile,administrative_state>> |Код состояния абонента. |int |O |
|forbid_reg |Флаг запрета регистрации на VLR/SGSN без поддержки фаз CAMEL. |bool |O |
|roaming_not_allowed |Флаг запрета роуминга. По умолчанию: 0. |bool |O |
|odb-param |Запреты, установленные оператором связи. |<<entities/odb.adoc#Odb,Odb>> |O |
|pdp-data |Перечень профилей PDP. |[<<entities/pdpProfile.adoc#PdpProfile,PdpProfile>>] |O |
|eps-context-data |Перечень профилей EPS. |[<<entities/epsProfile.adoc#EpsProfile,EpsProfile>>] |O |
|eps-data |Параметры EPS. |<<entities/epsData.adoc#EpsData,EpsData>> |O |
|csi-list |Перечень профилей CSI Link. |[<<entities/csiLink.adoc#CsiLink,CsiLink>>] |O |
|oCsi-list |Перечень профилей O-CSI. |[<<entities/oCsi.adoc#OCsi,OCsi>>] |O |
|tCsi-list |Перечень профилей T-CSI. |[<<entities/tCsi.adoc#TCsi,TCsi>>] |O |
|smsCsi-list |Перечень профилей SMS-CSI. |[<<entities/smsCsi.adoc#SmsCsi,SmsCsi>>] |O |
|gprsCsi-list |Перечень профилей GPRS-CSI. |[<<entities/gprsCsi.adoc#GprsCsi,GprsCsi>>] |O |
|mCsi-list |Перечень профилей M-CSI. |[<<entities/mCsi.adoc#MCsi,MCsi>>] |O |
|dCsi-list |Перечень профилей D-CSI. |[<<entities/dCsi.adoc#DCsi,DCsi>>] |O |
|ssCsi-list |Перечень профилей D-CSI. |[<<entities/ssCsi.adoc#SsCsi,SsCsi>>] |O |
|oImCsi-list |Перечень профилей Originating IP Multimedia CSI. |[<<entities/oCsi.adoc#OCsi,OCsi>>] |O |
|vtCsi-list |Перечень профилей VMSC Terminating CSI. |[<<entities/tCsi.adoc#TCsi,TCsi>>] |O |
|dImCsi-list |Перечень профилей Dialled IP Multimedia CSI. |[<<entities/dCsi.adoc#DCsi,DCsi>>] |O |
|ssData |Перечень дополнительных услуг SS. |[<<entities/ssData.adoc#SsData,SsData>>] |O |
|ssForw |Перечень дополнительных услуг SS переадресации. |[<<entities/ssForwarding.adoc#SsForwarding,SsForwarding>>] |O |
|ssBarring |Перечень дополнительных услуг SS запрета. |[<<entities/ssBarring.adoc#SsBarring,SsBarring>>] |O |
|lcsPrivacy |Перечень профилей LCS Privacy. |[<<entities/lcsPrivacy.adoc#LcsPrivacy,LcsPrivacy>>] |O |
|lcsMolr |Перечень профилей LCS MOLR. |[<<entities/lcsMolr.adoc#LcsMolr,LcsMolr>>] |O |
|teleserviceList |Перечень идентификаторов телеуслуг. |[int] |O |
|bearerserviceList |Перечень идентификаторов служб передачи данных. |[int] |O |
|roaming-info |Параметры роуминга. |<<entities/roaming.adoc#Roaming,Roaming>> |O |
|roaming-sgsn-info |Параметры роумингового узла SGSN. |<<entities/roamingSgsn.adoc#RoamingSgsn,RoamingSgsn>> |O |
|defaultForwNumber |Номер переадресации по умолчанию. |string |O |
|defaultForwStatus |Код состояния для переадресации. |int |O |
|whiteLists |Перечень идентификаторов белых списков, <<entities/whiteList.adoc#WhiteList,WhiteList>>. |[int] |O |
|blackLists |Перечень идентификаторов черных списков, <<entities/blackList.adoc#BlackList,BlackList>>. |[int] |O |
|lcsId |Идентификатор профиля LCS. |int |O |
|tkId |Идентификатор транспортного ключа. |int |O |
|groupId |Идентификатор абонентской группы. |int |O |
|deactivatePsi |Флаг отказа от отправки сообщения MAP-PSI при получении запроса MAP-SRI.pass:q[<br>]По умолчанию: 0. |bool |O |
|baocWithoutCamel |Флаг разрешения регистрации на VLR без поддержки CAMEL при наличии запрета роуминга. |bool |O |
|ipSmGwNumber |Номер узла IP-SM-GW. |string |O |
|ipSmGwHost |Хост узла IP-SM-GW. |string |O |
|ipSmGwRealm |Realm узла IP-SM-GW. |string |O |
|smsRegistrationInfo |Перечень регистраций на узле IP-SM-GW. |[<<entities/smsRegistrationInfo.adoc#SmsRegistrationInfo,SmsRegistrationInfo>>] |O |
|networkAccessMode |Код режима работы.pass:q[<br>]`0` -- MSC + SGSN; `1` -- только MSC;`2` -- только SGSN. |int |O |
|qosGprsId |Идентификатор профиля QoS GPRS. |int |O |
|qosEpsId |Идентификатор профиля QoS EPS. |int |O |
|accessRestrictionData |Ограничения доступа. |<<entities/accessRestrictionData.adoc#AccessRestrictionData,AccessRestrictionData>> |O |
|chargingCharacteristics |Значение `ChargingCharacteristics`. |string |O |
|imsSubscription |Подписка IMS. |<<entities/imsSubscription.adoc#ImsSubscription,ImsSubscription>> |O |
|imsUserData |Параметры данных пользователя IMS. |<<entities/imsUserData.adoc#ImsUserData,ImsUserData>> |O |
|roaming-scscf-info |Перечень параметров роуминговых узлов S-CSCF. |[<<entities/roamingScscfInfo.adoc#RoamingScscfInfo,RoamingScscfInfo>>] |O |
|aMsisdn |Дополнительные номера MSISDN. |[string] |O |
|periodicLauTimer |Период между отправками запросов Location-Area-Update, в секундах. |int |O |
|periodicRauTauTimer |Период между отправками запросов Routing-Area-Update и Tracking-Area-Update, в секундах. |int |O |
|HSM_ID |Идентификатор модуля HSM. |int |O |
|opc |Код сигнальной точки отправления. |string |O |
|ueUsageType |Значение `UE-Usage-Type` для выбора выделенной опорной сети. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP&nbsp;TS&nbsp;29.272].pass:q[<br>]Диапазон: 0-127. |int |O |
|lastActivity |Дата и время последней активности. |datetime |O |
|SCA |Адрес сервисного центра. |datetime |O |
|category |Категория вызывающего абонента CgPC. См. https://www.itu.int/rec/T-REC-Q.767-199102-I/en[ITU-T Recommendation Q.767]. |int |O |
|RSD |Перечень дескрипторов выбора маршрута. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf[3GPP&nbsp;TS&nbsp;29.212]. |[string] |O |
|icsIndicator |Индикатор централизованных служб, Centralized Services, сетей IMS. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP&nbsp;TS&nbsp;29.272]. |bool |O |
|csToPsSrvcc +
AllowedIndicator |Флаг разрешения передачи голосовой части сессии из домена CS в домен PS. |bool |O |2.0.35.0
|singleNssais |Параметры подписок Single NSSAI. |<<entities/subSnssai.adoc#SubSnssai,SubSnssai>> |O |2.1.14.0
|externalId |Идентификатор для внешних устройств IoT. |string |O |2.1.7.0
|comment |Комментарий к профилю абонента. |string |O |2.1.9.0
|externalGroupIds |Перечень групповых идентификаторов устройств IoT. |[string] |O |2.1.10.0
|niddAuthorizations |Перечень авторизаций NIDD. |[<<entities/niddAuthorization.adoc#NiddAuthorization,NiddAuthorization>>] |O |2.1.10.0
|imei |Номер IMEI устройства. |string |O |2.1.10.0
|===

=== [[administrative-state-profile]]Коды состояния абонента

[width="100%",cols="5%,30%,65%",options="header",]
|===
|N |Поле |Описание
|0 |Not provisioned |Карта не привязана к номеру MSISDN
|1 |Provisioned/locked |Карта привязана к номеру MSISDN и заблокирована
|2 |Provisioned/unlocked |В эксплуатации
|3 |Provisioned/suspended |Обслуживание приостановлено
|4 |Terminated |Абонент удален
|===

.Пример
[source,json]
----
{
  "status": "OK",
  "imsi": "250510100168217",
  "msisdn": "79585179068217",
  "administrative_state": 2,
  "forbid_reg": 0,
  "roaming_not_allowed": 0,
  "odb-param": {
    "SubscriberStatus": 0,
    "general-list": 0,
    "hplmn-list": 0,
    "hl-general-list": [],
    "hl-hplmn-list": []
  },
  "pdp-data": [
    {
      "context-id": 1,
      "type": "f121",
      "vplmnAddressAllowed": 0,
      "apn": "iot2"
    },
    {
      "context-id": 4,
      "type": "f121",
      "vplmnAddressAllowed": 0,
      "apn": "iot"
    }
  ],
  "eps-context-data": [
    {
      "eps_context_id": 1,
      "service_selection": "iot2",
      "vplmnDynamicAddressAllowed": 0,
      "pdnGwType": 1,
      "pdnType": 0,
      "specificApns": []
    },
    {
      "eps_context_id": 4,
      "service_selection": "iot",
      "vplmnDynamicAddressAllowed": 0,
      "pdnGwType": 1,
      "pdnType": 0,
      "specificApns": []
    }
  ],
  "eps-data": [
    {
      "ueMaxDl": 100000000,
      "ueMaxUl": 100000000,
      "ratType": 1004,
      "defContextId": 1
    }
  ],
  "csi-list": [
    {
      "type-id": 4,
      "profile-id": 1
    }
  ],
  "mCsi-list": [
    {
      "id": 1,
      "serviceKey": 1,
      "gsmScfAddress": "79585340009",
      "notificationToCse": 1,
      "csiActive": 1,
      "csiMts": [
        {
          "id": 6,
          "csiId": 1,
          "mmCode": 0
        },
        {
          "id": 9,
          "csiId": 1,
          "mmCode": 1
        },
        {
          "id": 12,
          "csiId": 1,
          "mmCode": 2
        }
      ]
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 16 ]
    }
  ],
  "ssForw": [],
  "ssBarring": [
    {
      "ss_Code": 146,
      "ss_Status": 1,
      "tele_service": 16
    }
  ],
  "lcsPrivacy": [],
  "lcsMolr": [],
  "teleserviceList": [ 17, 33, 34 ],
  "bearerserviceList": [],
  "roaming-info": {
    "mscAreaRestricted": -1,
    "supportedVlrCamel": 3,
    "supportedVlrLcs": 15,
    "ueLcsNotSupported": 0,
    "version": 0,
    "dtregistered": 1620118895000,
    "ispurged": 0
  },
  "roaming-sgsn-info": {
    "dtregistered": 1620118895000
  },
  "defaultForwStatus": 0,
  "whiteLists": [
    {
      "id": 22,
      "name": "MegaFon"
    }
  ],
  "blackLists": [],
  "tkId": 1,
  "groupId": 622,
  "deactivatePsi": 0,
  "networkAccessMode": 0,
  "qosGprsId": 1,
  "qosEpsId": 1,
  "accessRestrictionData": {},
  "category": 10
}
----
