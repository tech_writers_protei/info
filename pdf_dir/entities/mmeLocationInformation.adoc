[[MmeLocationInformation]]
== Параметры местоположения абонента на узле MME MmeLocationInformation

[width="100%",cols="3,8,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|ageOfLocationInformation |Значение `AgeOfLocation`. |int |O
|currentLocationRetrieved |Флаг предоставления текущего местоположения абонента. |int |O
|eUtranCellGlobalIdentity |Глобальный идентификатор соты в сетях E-UTRAN. |hex |O
|geodeticInformation |Геодезическое местоположение отправки вызова. См. https://www.itu.int/rec/T-REC-Q.763-199912-I/en[ITU-T Recommendation Q.763]. |hex |O
|geographicalInformation |Описание географической области. См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123032/18.01.00_60/ts_123032v180100p.pdf[3GPP&nbsp;TS&nbsp;23.032]. |hex |O
|trackingAreaIdentity |Идентификатор области отслеживания. |hex |O
|enodebId |Идентификатор базовой станции сети. |string |O
|extendedEnodebId |Расширенный идентификатор базовой станции сети. |string |O
|userCsgInformation |Информация о доступе к ячейке закрытой группы, CSG. |<<entities/userCsgInformation.adoc#UserCsgInformation,UserCsgInformation>> |O
|===

.Пример
[source,json]
----
{
  "ageOfLocationInformation": 1,
  "eUtranCellGlobalIdentity": "00f1102b2d1010",
  "trackingAreaIdentity": "00f1100001"
}
----
