[[Impu]]
== Параметры IMPU

[cols="3,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|Identity |Публичный идентификатор пользователя сети IMS. |string |M
|UserState |Состояние публичного идентификатора.pass:q[<br>]`0` -- Not registered;pass:q[<br>]`1` - Registered;pass:q[<br>]`2` -- Unregistered;pass:q[<br>]`4` -- Authenticated.pass:q[<br>]*Примечание.* Заполняется только в GetProfileResponse. |int |O
|Type |Код типа идентификатора.pass:q[<br>]`0` -- Distinct Public User Identity;pass:q[<br>]`1` -- Distinct Public Service Identity;pass:q[<br>]`2` -- Wildcarded Public Service Identity. |int |M
|BarringIndication |Индикатор запрета регистрации. |bool |M
|CanRegister |Индикатор возможности регистрации. |bool |O
|ServiceProfileName |Имя профиля услуги. |string |O
|WildcardPsi |Код шаблона публичной услуги. |string |O
|PsiActivation |Флаг активации публичной услуги. |bool |O
|Default |Флаг использования услуги по умолчанию.pass:q[<br>]*Примечание.* Используется только для управления идентификаторами. |bool |O
|delete |Флаг удаления значения параметра. |bool |O
|===

.Пример
[source,json]
----
{
  "Identity": "sip:79000000001@ims.protei.ru",
  "BarringIndication": 0,
  "Type": 0,
  "CanRegister": 1,
  "ServiceProfileName": "sp"
}
----
