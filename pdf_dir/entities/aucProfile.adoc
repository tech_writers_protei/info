[[AucProfile]]
== Параметры профиля аутентификации AucProfile

[width="100%",cols="2,8,2,1,2",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|imsi |Номер IMSI абонента. |string |М |2.0.39.0
|iccid |Идентификатор смарт-карты абонента. |string |O |2.0.47.0
|[[algo-auc-profile]]<<algorithm-auc-profile,algorithm>> |Код алгоритма аутентификации. |int |М |2.0.39.0
|ki |Секретный ключ Ki для аутентификации.pass:q[<br>]*Примечание.* Если <<algo-auc-profile,algorithm>> = TUAK, то длина должна быть 32 или 64 бита, для других алгоритмов -- 32 бита. |string |М |2.0.39.0
|opc |Значение поля конфигурации алгоритма оператором. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |string |O |2.0.39.0
|opId |Идентификатор ключа оператора для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|tkId |Идентификатор транспортного ключа для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|opcTkId |Идентификатор транспортного ключа OPC. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|cId |Идентификатор аддитивной постоянной для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|rId |Идентификатор постоянной поворота для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|amfUmts |Поле управления аутентификацией в сетях UMTS. |string |O |2.0.39.0
|amfLte |Поле управления аутентификацией в сетях LTE. |string |O |2.0.39.0
|amfIms |Поле управления аутентификацией в сетях IMS. |string |O |2.0.39.0
|resSize |Длина ответа пользователя для алгоритма MILENAGE, в байтах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|ckSize |Длина ключа конфиденциальности для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|ikSize |Длина ключа целостности для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|macSize |Код сетевой аутентификации для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]. |int |O |2.0.39.0
|keccakIterations |Количество раундов алгоритма хэширования SHA-3 (Keccak). См. https://nvlpubs.nist.gov/nistpubs/fips/nist.fips.202.pdf[NIST FIPS PUB 202]. |int |O |2.0.39.0
|sqnType |Код способа выбора последовательности SQN.pass:q[<br>]`0` -- не зависимый от времени; `1` -- основанный на времени. |int |O |2.0.39.0
|sqn |Номер последовательности SQN. |int |O |2.0.42.0
|dif |Разность между значением SEQ номера SQN и значением глобального счетчика GLC. |int |O |2.0.42.0
|===

.Пример
[source,json]
----
{
  "imsi": "250010000001",
  "algorithm": 4,
  "ki": "12345678900987654321123456789009",
  "opc": "09876543211234567890098765432112"
}
----

=== [[algorithm-auc-profile]]Коды алгоритмов аутентификации

[cols="1,4,8",options="header",]
|===
|N |Поле |Описание
|1 |COMP128 v1 |
|2 |COMP128 v2 |
|3 |COMP128 v3 |
|4 |MILENAGE |Cм. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP&nbsp;TS&nbsp;35.205]
|6 |XOR |Cм. https://www.etsi.org/deliver/etsi_ts/134100_134199/134108/15.02.00_60/ts_134108v150200p.pdf[3GPP TS 34.108&#44; 8.1.2.1]
|7 |TUAK |Cм. https://www.etsi.org/deliver/etsi_ts/135200_135299/135231/18.00.00_60/ts_135231v180000p.pdf[3GPP&nbsp;TS&nbsp;35.231]
|8 |S3G128 |Cм. https://protect.gost.ru/v.aspx?control=8&id=224247[ГОСТ 34.10-2018]
|9 |S3G256 |Cм. https://protect.gost.ru/v.aspx?control=8&id=224247[ГОСТ 34.10-2018]
|===
