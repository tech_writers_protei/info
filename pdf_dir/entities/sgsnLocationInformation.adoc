[[SgsnLocationInformation]]
== Параметры местоположения абонента на узле SGSN SgsnLocationInformation

[width="100%",cols="3,8,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|ageOfLocationInformation |Значение `Age-Of-Location-Information`. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272[3GPP&nbsp;TS&nbsp;29.272]. |int |O
|currentLocationRetrieved |Флаг предоставления текущего местоположения абонента, `Current-Location-Retrieved`. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272[3GPP&nbsp;TS&nbsp;29.272]. |int |O
|cellGlobalIdentity |Глобальный идентификатор соты в сетях E-UTRAN, `Cell-Global-Identity`. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272[3GPP&nbsp;TS&nbsp;29.272]. |string |O
|geodeticInformation |Геодезическое местоположение отправки вызова, `Geodetic-Information`. См. https://www.itu.int/rec/T-REC-Q.763-199912-I/en[ITU-T Recommendation Q.763]. |string |O
|geographicalInformation |Описание географической области, `Geographical-Information`. См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123032/18.01.00_60/ts_123032v180100p.pdf[3GPP&nbsp;TS&nbsp;23.032]. |string |O
|locationAreaIdentity |Область отслеживания, `Location-Area-Identity`. |hex |O
|routingAreaIdentity |Область маршрутизации, `Routing-Area-Identity`. |hex |O
|serviceAreaIdentity |Область обслуживания, `Service-Area-Identity`. |hex |O
|userCsgInformation |Информация о доступе к ячейке закрытой группы, CSG. |<<entities/userCsgInformation.adoc#UserCsgInformation,UserCsgInformation>> |O
|===

.Пример
[source,json]
----
{
  "ageOfLocationInformation": 1,
  "cellGlobalIdentity": "00f1102b2d1010",
  "routingAreaIdentity": "00f1100001"
}
----
