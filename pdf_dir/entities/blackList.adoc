[[BlackList]]
== Параметры черного списка BlackList

[width="100%",cols="3,10,3,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|name |Название списка. |string |M
|vlr_masks |Перечень масок номера узла VLR. |[<<entities/blVlrMask.adoc#BlVlrMask,BlVlrMask>>] |O
|plmn_masks |Перечень масок сети PLMN. |[<<entities/blPlmnMask.adoc#BlPlmnMask,BlPlmnMask>>] |O
|===

.Пример
[source,json]
----
{
  "action": "create",
  "name": "blacklist",
  "vlr_masks": [
    {
      "country": "rus",
      "mask": "7689",
      "network": "supertelecom"
    }
  ]
}
----
