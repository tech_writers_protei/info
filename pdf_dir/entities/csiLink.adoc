[[CsiLink]]
== Параметры привязки профиля CsiLink

[width="100%",cols="2,8,2,1,2",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|<<type-id-csi-link,type-id>> |Идентификатор типа профиля CSI. |int |M |
|profile-id |Идентификатор профиля CSI. |int |M |
|preffixGt |Глобальный заголовок GT, к которому привязан профиль CSI. |string |O |
|delete |Флаг удаления значения параметра.pass:q[<br>]*Примечание.* При удалении связи с CSI-профилем отправляется запрос Delete-Subscriber-Data. |bool |O |2.0.34.1
|===

.Пример
[source,json]
----
{
  "type-id": 0,
  "profile-id": 1,
  "preffixGt": "7900"
}
----

=== [[type-id-csi-link]]Идентификаторы типа CSI

[cols="1,4,4",options="header",]
|===
|N |Поле |Описание
|0 |O-CSI |Профиль O-CSI
|1 |T-CSI |Профиль T-CSI
|2 |SMS-CSI |Профиль SMS-CSI
|3 |GPRS-CSI |Профиль GPRS-CSI
|4 |M-CSI |Профиль M-CSI
|5 |D-CSI |Профиль D-CSI
|6 |TIF-CSI |Профиль TIF-CSI
|7 |SS-CSI |Профиль SS-CSI
|8 |USSD-CSI |Профиль USSD-CSI
|9 |O-IM-CSI |Профиль O-IM-CSI
|10 |VT-IM-CSI |Профиль VT-IM-CSI
|11 |D-IM-CSI |Профиль D-IM-CSI
|===
