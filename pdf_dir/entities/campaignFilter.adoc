[[CampaignFilter]]
== Параметры фильтра операций CampaignFilter

[width="100%",cols="3,8,2,1,2",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|sortDirection |Порядок сортировки значений.pass:q[<br>]`ASC` / `DESC`.pass:q[<br>]По умолчанию: ASC. |string |O |2.0.50.0
|id |Идентификатор операции. |int |O |2.0.50.0
|userId |Идентификатор пользователя, создавшего операцию. |int |O |2.0.50.0
|userLogin |Логин пользователя, создавшего операцию. |string |O |2.0.50.0
|creationDateFrom |Наиболее ранняя дата создания операции, подходящая для вывода. |datetime |O |2.0.50.0
|creationDateTo |Наиболее поздняя дата создания операции, подходящая для вывода. |datetime |O |2.0.50.0
|<<campaign-status-campaign-filter,campaignStatus>> |Текущее состояние операции. |string |M |2.0.50.0
|<<campaign-type-campaign-filter,campaignType>> |Тип операции. |string |M |2.0.50.0
|===

.Пример
[source,json]
----
{
  "sortDirection": "DESC",
  "campaignStatus": "SUCCEED",
  "campaignType": "CHANGE_PROFILE"
}
----

=== [[campaign-status-campaign-filter]]Состояния операций

[cols=",",options="header",]
|===
|Значение |Описание
|NEW |Операция еще не начата
|IN_PROGRESS |Операция в процессе выполнения
|ERROR |Во время выполнения операции произошла ошибка
|PARTLY_SUCCEED |Операция выполнена частично
|SUCCEED |Операция успешно выполнена
|CANCELED |Операция отменена
|===

=== [[campaign-type-campaign-filter]]Типы операций

[cols=",",options="header",]
|===
|Значение |Описание
|ADD_SUBSCRIBER_PROFILE |Добавление профиля абонента
|CHANGE_IMSI |Изменение номера IMSI
|CHANGE_MSISDN |Изменение номера MSISDN
|CHANGE_PROFILE |Изменение профиля
|DELETE_SUBSCRIBER |Удаление абонента
|DELETE_SUBSCRIBER_PROFILE |Удаление профиля абонента
|SIMCARDS_INFORMATION_LOAD |Загрузка информации на SIM-карту
|EXPORT_SUBSCRIBER_PROFILE |Экспорт профиля абонента
|===
