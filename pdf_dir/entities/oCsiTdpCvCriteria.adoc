[[OCsiTdpCvCriteria]]
== Параметры критерия причины отбоя триггерной точки обнаружения О-CSI OCsiTdpCvCriteria

[width="100%",cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|causeValue |Причина отбоя вызова согласно https://www.itu.int/rec/T-REC-Q.850-201810-I/en[ITU-T Recommendation Q.850]. |int |М
|delete |Флаг удаления значения параметра. |bool |О
|===

.Пример
[source,json]
----
{
  "causeValue": 1
}
----
