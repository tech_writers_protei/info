[[ServicePointTrigger]]
== Параметры условия активации точки триггера ServicePointTrigger

[width="100%",cols="3,8,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|ConditionNegated |Флаг отрицания выражения. |bool |O
|Group |Идентификатор группы условий активации точки триггера. |int |O
|Type |Тип условий активации точки триггера. |int |O
|RequestUri |SIP Request-URI. |string |O
|[[method-spt]]Method |Метод SIP. |string |O
|Header |Заголовок SIP. |string |O
|Content |Содержимое заголовка SIP. |string |O
|<<session-case-spt,SessionCase>> |Идентификатор типа сессии активации точки триггера. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129228/18.00.00_60/ts_129228v180000p.pdf[3GPP&nbsp;TS&nbsp;29.228]. |int |O
|SdpLine |Значение параметра Session Description Line. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129228/18.00.00_60/ts_129228v180000p.pdf[3GPP&nbsp;TS&nbsp;29.228]. |regex |O
|SdpLineContent |Значение параметра Session Description Content. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129228/18.00.00_60/ts_129228v180000p.pdf[3GPP&nbsp;TS&nbsp;29.228]. |regex |O
|RegistrationType |Код типа регистрации.pass:q[<br>]`0` -- INITIAL_REGISTRATION, первичная регистрация;pass:q[<br>]`1` -- RE-REGISTRATION, повторная регистрация;pass:q[<br>]`2` -- DE-REGISTRATION, дерегистрация.pass:q[<br>]*Примечание.* Активно при <<method-spt,Method>> = REGISTER. |int |O
|Delete |Флаг удаления значения параметра. |bool |O
|===

.Пример
[source,json]
----
{
  "Group": 1,
  "Method": "INVITE",
  "SessionCase": 1,
  "ConditionNegated": 2,
  "Type": 3,
  "RequestUri": "http://ims.protei.ru/spt1",
  "Header": "header",
  "Content": "headerContent",
  "SdpLine": "sdpLine",
  "SdpLineContent": "sdpLineContent",
  "RegistrationType": 1
}
----

=== [[session-case-spt]]Идентификаторы типов сессий

[cols="1,7",options="header",]
|===
|Код |Тип сессии
|0 |Originating
|1 |Terminating_Registered
|2 |Originating_Unregistered
|3 |Terminating_Unregistered
|4 |Originating_CDIV
|===
