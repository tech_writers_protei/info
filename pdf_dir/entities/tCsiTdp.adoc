[[TCsiTdp]]
== Параметры триггерной точки T-CSI TCsiTdp

[options="header",cols="3,9,3,1"]
|===
|Поле |Описание |Тип |O/M
|tdpId |Идентификатор триггерной точки обнаружения. |int |М
|serviceKey |Идентификатор службы, `ServiceKey`. |int |М
|gsmScfAddress |Адрес узла IM-SSF. |string |О
|defaultHandling |Код действия при обработке вызова по умолчанию.pass:q[<br>]`0` -- продолжить вызов; `1` -- отбить вызов. |int |О
|csiTdpBsCriterias |Перечень критериев базовой услуги триггерной точки обнаружения T-CSI. |[<<entities/tCsiTdpBsCriteria.adoc#TCsiTdpBsCriteria,TCsiTdpBsCriteria>>] |O
|csiTdpCvCriterias |Перечень критериев причины отбоя триггерной точки обнаружения T-CSI. |[<<entities/tCsiTdpCvCriteria.adoc#TCsiTdpCvCriteria,TCsiTdpCvCriteria>>] |O
|delete |Флаг удаления записи. |bool |O
|===

.Пример
[source,json]
----
{
  "tdpId": 1,
  "serviceKey": 1,
  "gsmScfAddress": "78924813183138",
  "defaultHandling": 1
}
----
