[[PlmnZc]]
== Параметры кодов зон сети PlmnZc

[width="100%",cols="15%,44%,35%,6%",options="header",]
|===
|Поле |Описание |Тип |O/M
|plmn |Код сети назначения. |string |M
|zoneCodes |Перечень кодов зон. |[<<entities/zoneCode.adoc#ZoneCode,ZoneCode>>] |M
|delete |Флаг удаления значения параметра. |bool |O
|===

.Пример
[source,json]
----
{   
  "plmn": "25001",
  "zoneCodes": [
    { "zoneCode": 1 },
    { "zoneCode": 2 }
  ]
}
----
