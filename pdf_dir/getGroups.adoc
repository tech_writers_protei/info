[[GetGroups]]
= GetGroups

Команда `GetGroups` позволяет получать пользовательские группы в системе.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetGroups \
  -d '{ "full": <bool> }'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|full |Флаг запроса всей информации. По умолчанию: false. |bool |O
|===

.Пример тела запроса
[source,json]
----
{ "full": true }
----

== Ответ

В ответе передаются статус запроса и пользовательские группы.

....
{
  "status": "<string>",
  "groups": [ <Group> ]
}
....

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|groups |Перечень пользовательских групп. |[<<entities/group.adoc#Group,Group>>] |M
|===

.Пример тела запроса
[source,json]
----
{
  "status": "OK",
  "groups": []
}
----
