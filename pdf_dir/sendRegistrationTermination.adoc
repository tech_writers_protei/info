[[SendRegistrationTermination]]
= SendRegistrationTermination

Команда `SendRegistrationTermination` позволяет отменять регистрацию в сетях IMS.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/SendRegistrationTermination \
  -d '{
  "impi": "<string>",
  "impu": "<string>"
}'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|[[impi-send-registration-termination]]impi |Приватный идентификатор пользователя в сети IMS. |string |C
|[[impu-send-registration-termination]]impu |Публичный идентификатор пользователя в сети IMS. |string |C
|===

NOTE: Задается только один из параметров: <<impi-send-registration-termination,impi>> или <<impu-send-registration-termination,impu>>.

.Пример тела запроса
[source,json]
----
{ "impi": "250010000001@ims.protei.ru" }
----

== Ответ

В ответе передается только статус запроса.
