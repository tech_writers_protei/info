[[ChangeSnssai]]
= ChangeSnssai

Команда `ChangeSnssai` позволяет изменить данные Single Network Slice Selection Assistance Information.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeSnssai \
  -d '{
  "action": "<string>",
  "snssai": <Snssai>
}'
----

== Поля тела запроса

[cols="2,9,2,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|action |Выполняемое действие.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M |2.1.14.0
|snssai |Профиль SNSSAI. |<<entities/snssai.adoc#Snssai,Snssai>> |M |2.1.14.0
|===

.Пример тела запроса
[source,json]
----
{
  "action": "create",
  "snssai": {
    "name": "snssai1",
    "sst": 1,
    "sd": "123456",
    "contexts": [ 1 ]
  }
}
----

== Ответ

В ответе передаются статус запроса и параметры профиля S-NSSAI.

....
{
  "status": "<string>",
  "snssai": <Snssai>
}
....

== Поля ответа

[width="100%",cols="13%,23%,43%,7%,14%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|status |Статус запроса. |string |M |2.1.14.0
|snssai |Профиль SNSSAI. |<<entities/snssai.adoc#Snssai,Snssai>> |M |2.1.14.0
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "snssai": {
    "name": "snssai1",
    "sst": 1,
    "sd": "123456",
    "contexts": [ 1 ]
  }
}
----
