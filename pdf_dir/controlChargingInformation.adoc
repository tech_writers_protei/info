[[ControlChargingInformation]]
= ControlChargingInformation

Команда `ControlChargingInformation` позволяет изменять параметры тарификации.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/ControlChargingInformation \
  -d '{
  "Action": "<string>",
  "ChargingInformation": <ChargingInformation>,
  "Force": <bool>
}'
----

== Поля тела запроса

[cols="3,9,3,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|Action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|ChargingInformation |Параметры тарификации. |<<entities/chargingInformation.adoc#ChargingInformation,ChargingInformation>> |M
|Force |Флаг принудительного удаления в случае связи с профилем. |bool |O
|===

.Пример тела запроса
[source,json]
----
{
  "Action": "create",
  "ChargingInformation": {
    "Name": "ci",
    "PriEcf": "priecf@ims.protei.ru",
    "SecEcf": "sececf@ims.protei.ru",
    "PriCcf": "priccf@ims.protei.ru",
    "SecCcf": "secccf@ims.protei.ru"
  }
}
----

== Ответ

В ответе передается только статус запроса.
