[[GetLcsProfile]]
= GetLcsProfile

Команда `GetLcsProfile` позволяет получать профиль услуг LCS.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetLcsProfile \
  -d '{ "context-id": <int> }'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|id |Идентификатор профиля LCS. |int |M
|===

.Пример тела запроса
[source,json]
----
{ "id": 1 }
----

== Ответ

В ответе передаются статус запроса и профиль LCS.

....
{
  "status": "<string>",
  "lcsProfile": <LcsProfile>
}
....

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|lcsProfile |Профиль LCS. |<<entities/lcsProfile.adoc#LcsProfile,LcsProfile>> |M
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "lcsProfile": {}
}
----
