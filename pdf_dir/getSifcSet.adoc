[[GetSifcSet]]
= GetSifcSet

Команда `GetSifcSet` позволяет получать набор общих iFC.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/GetSifcSet \
  -d '{ "Id": <int> }'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|Id |Идентификатор набора общих iFC. |int |M
|===

.Пример тела запроса
[source,json]
----
{ "Id": 1 }
----

== Ответ

В ответе передаются статус запроса и набор общих iFC.

....
{
  "status": "<string>",
  "SIfcSet": <SIfcSet>
}
....

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|SIfcSet |Набор общих iFC. |<<entities/sIfcSet.adoc#SIfcSet,SIfcSet>> |M
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "SIfcSet": {}
}
----
