[[GetProfilesValue]]
= GetProfilesValue

Команда `GetProfilesValue` позволяет получать перечень значений общих сущностей.

== Запрос

[source,bash]
----
$ curl -X GET https://<host>:<port>/ProfileService/GetProfilesValue/<entity>
----

== Поля запроса query

В запросе передается название класса общих объектов, <<entities/entity.adoc#Entity,Entity>>.

== Ответ

В ответе передаются статус запроса и перечень значений общих сущностей.

[source,]
----
{ 
  "status": "<string>",
  "values": [ <Entity> ]
}
----

== Поля ответа

[width="100%",cols="9%,36%,50%,5%",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|values |Перечень значений общих сущностей. |<<entities/entity.adoc#Entity,Entity>> |M
|===

.Пример ответа
[source,json]
----
{
  "values": [
    {
      "eps_context_id": 1,
      "name": "eps1",
      "service_selection": "test.protei.ru",
      "vplmnDynamicAddressAllowed": 1,
      "qosClassId": 6,
      "allocateRetPriority": 4,
      "maxDl": 1000,
      "maxUl": 1000,
      "pdnType": 1
    }
  ]
}
----
