[[DeleteSubscriberCampaign]]
= DeleteSubscriber

Команда `DeleteSubscriber` позволяет удалять несколько абонентов из файла.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/DeleteSubscriber \
  -H 'Content-Type: text/csv' \
  -d @<path_to_file>.csv
----

В теле запроса передается файл формата CSV.

Формат файла:

[source,csv]
----
imsi;msisdn;
----

== Поля файла

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|imsi |Номер IMSI абонента. |string |M
|msisdn |Номер MSISDN абонента. |string |О
|===

.Пример файла
[source,csv]
----
250010000000004;79110000004;
250010000000005;79110000005;
----

== Ответ

В ответе передаются параметры операции Campaign.

....
{
  "status": "<string>",
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "finishDate": "<datetime>",
  "startDate": "<datetime>",
  "campaignCounter": <CampaignCounter>
}
....

== Поля ответа

[cols="3,9,3,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|status |Статус запроса. |string |M |
|campaignCounter |Счетчик операций. |<<entities/campaignCounter.adoc#CampaignCounter,CampaignCounter>> |M |2.0.56.0
|<<campaign-status-delete-subscriber-campaign,campaignStatus>> |Статус операции. |string |M |2.0.56.0
|<<campaign-type-delete-subscriber-campaign,campaignType>> |Тип операции. Должно быть `DELETE_SUBSCRIBER`. |string |M |2.0.56.0
|id |Идентификатор операции. |int |M |2.0.56.0
|creationDate |Дата создания операции. Формат:pass:q[<br>]`YYYY-MM-DDThh:mm:ss`. |datetime |O |2.0.56.0
|startDate |Дата запуска операции. Формат:pass:q[<br>]`YYYY-MM-DDThh:mm:ss`. |datetime |O |2.0.56.0
|finishDate |Дата окончания операции. Формат:pass:q[<br>]`YYYY-MM-DDThh:mm:ss`. |datetime |O |2.0.56.0
|===

=== [[campaign-status-delete-subscriber-campaign]]Состояния операций

[cols="1,4",options="header",]
|===
|Поле |Описание
|NEW |Операция еще не начата
|IN_PROGRESS |Операция в процессе выполнения
|ERROR |Во время выполнения операции произошла ошибка
|PARTLY_SUCCEED |Операция выполнена частично
|SUCCEED |Операция успешно выполнена
|CANCELED |Операция отменена
|===

=== [[campaign-type-delete-subscriber-campaign]]Типы операций

[cols=",",options="header",]
|===
|Поле |Описание
|ADD_SUBSCRIBER_PROFILE |Добавление профиля абонента
|CHANGE_IMSI |Изменение номера IMSI
|CHANGE_MSISDN |Изменение номера MSISDN
|CHANGE_PROFILE |Изменение профиля
|DELETE_SUBSCRIBER |Удаление абонента
|DELETE_SUBSCRIBER_PROFILE |Удаление профиля абонента
|SIMCARDS_INFORMATION_LOAD |Загрузка информации на SIM-карту
|EXPORT_SUBSCRIBER_PROFILE |Экспорт профиля абонента
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "DELETE_SUBSCRIBER",
  "id": 49,
  "creationDate": "2022-01-01T12:50:37",
  "startDate": "2022-01-01T12:50:37",
  "campaignCounter": {
    "total": 2,
    "success": 0,
    "error": 0
  }
}
----
