[[ChangeAucConfig]]
= ChangeAucConfig

Команда `ChangeAucConfig` позволяет изменять параметры центра аутентификации.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeAucConfig \
  -d '{
  "opParams": [ <OpParam> ],
  "tkParams": [ <TkParam> ],
  "cParams": [ <CParam> ],
  "rParams": [ <RParam> ]
}'
----

== Поля тела запроса

[width="100%",cols="12%,50%,34%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M
|opParams |Перечень полей конфигурации алгоритма оператором. |[<<entities/opParam.adoc#OpParam,OpParam>>] |O
|tkParams |Перечень транспортных ключей. |[<<entities/tkParam.adoc#TkParam,TkParam>>] |O
|cParams |Перечень аддитивных постоянных. |[<<entities/cParam.adoc#CParam,CParam>>] |O
|rParams |Перечень постоянных поворота. |[<<entities/rParam.adoc#RParam,RParam>>] |O
|===

.Пример тела запроса
[source,json]
----
{
  "opParams": [
    {
      "id": 1,
      "op": "12345678900987654321123456789009"
    }
  ],
  "tkParams": [
    {
      "id": 1,
      "tk": "12345678900987654321123456789009",
      "algorithm": 0
    }
  ],
  "cParams": [
    {
      "id": 1,
      "c": "123456789009"
    }
  ],
  "rParams": [
    {
      "id": 1,
      "r": "123456789009"
    }
  ]
}
----

== Ответ

В ответе передается только статус запроса.
