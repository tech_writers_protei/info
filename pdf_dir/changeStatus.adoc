[[ChangeStatus]]
= ChangeStatus

Команда `ChangeStatus` позволяет изменять статус абонента на узле HLR/HSS.

При наличии активной регистрации на узлах VLR, SGSN или MME у абонента в состоянии Provisioned/unlocked она прекращается запросом MAP-CL или Diameter S6: CLR.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeStatus \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>",
  "status": <int>
}'
----

== Поля тела запроса

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|[[imsi-change-status]]imsi |Номер IMSI абонента. |string |C
|[[msisdn-change-status]]msisdn |Номер MSISDN. |string |C
|<<status-change-status,status>> |Код состояния абонента. |int |M
|===

NOTE: Если состояние меняется с "UNLOCKED" на любой другой, то с узла HLR отправляется запрос Send-Cancel-Location.

NOTE: Задается только один из параметров <<imsi-change-status,imsi>> или <<msisdn-change-status,msisdn>>.

=== [[status-change-status]]Коды состояния абонента

[width="100%",cols="5%,30%,65%",options="header",]
|===
|N |Поле |Описание
|0 |Not provisioned |Карта не привязана к номеру MSISDN
|1 |Provisioned/locked |Карта привязана к номеру MSISDN и заблокирована
|2 |Provisioned/unlocked |В эксплуатации
|3 |Provisioned/suspended |Обслуживание приостановлено
|4 |Terminated |Абонент удален
|===

.Пример тела запроса
[source,json]
----
{
  "imsi": "250010000001",
  "status": 1
}
----

== Ответ

В ответе передается только статус запроса.
