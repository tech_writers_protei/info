[[SendProvideSubscriberInformation]]
= SendProvideSubscriberInformation

Команда `SendProvideSubscriberInfo` позволяет получать информацию о регистрации абонента.

Используется для интеграции с внешними платформами.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/SendProvideSubscriberInfo \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>"
}'
----

== Поля тела запроса

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|[[imsi-send-provide-subscriber-information]]imsi |Номер IMSI абонента. |string |C
|[[msisdn-send-provide-subscriber-information]]msisdn |Номер MSISDN абонента. |string |C
|===

NOTE: Задается только один из параметров: <<imsi-send-provide-subscriber-information,imsi>> или <<msisdn-send-provide-subscriber-information,msisdn>>.

.Пример тела запроса
[source,json]
----
{ "imsi": "250010000001" }
----

== Ответ

В ответе передается статус запроса, состояние активности абонента и информация о местоположении абонента.

....
{
  "status": "<string>",
  "subscriberState": <bool>,
  "locationInformation": {<object>}
}
....

== Поля ответа

[cols="3,9,3,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|subscriberState |Флаг активности абонента. |bool |O
|locationInformation |Информация о местоположении абонента. |<<entities/locationInformation.adoc#LocationInformation,LocationInformation>> |O
|===

.Пример тела запроса
[source,json]
----
{
  "status": "OK",
  "subscriberState": 1,
  "locationInformation": {}
}
----
