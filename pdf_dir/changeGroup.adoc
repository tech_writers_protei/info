[[ChangeGroup]]
= ChangeGroup

Команда `ChangeGroup` позволяет изменять группу абонентов.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeGroup \
  -d '{
  "action": "<string>",
  "group": <Group>,
  "force": <bool>
}'
----

== Поля тела запроса

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|group |Пользовательская группа. |<<entities/group.adoc#Group,Group>> |M
|force |Флаг принудительного удаления при наличии связи с профилем абонента. |bool |O
|===

.Пример тела запроса
[source,json]
----
{
  "action": "create",
  "group": {
    "name": "gr",
    "diamRealm": "protei.ru",
    "diamError": 5444,
    "gtList": [
      {
        "hlrId": 1,
        "diamHost": "hss1.protei.ru"
      },
      {
        "hlrId": 2,
        "diamHost": "hss2.protei.ru"
      }
    ]
  }
}
----

== Ответ

В ответе передается только статус запроса.
