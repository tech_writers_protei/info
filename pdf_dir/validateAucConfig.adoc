[[ValidateAucConfig]]
= ValidateAucConfig

Команда `ValidateAucConfig` позволяет валидировать параметры центра аутентификации AuC.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ValidateAucConfig \
  -d '{
  "opParam": <object>,
  "tkParam": <object>,
  "cParam": <object>,
  "rParam": <object>,
}'
----

== Поля тела запроса

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|opParam |OP параметры. |<<entities/validateOpParam.adoc#OpParam,OpParam>> |O
|tkParam |ТК параметры. |<<entities/validateTkParam.adoc#TkParam,TkParam>> |O
|cParam |С параметры. |<<entities/validateCParam.adoc#CParam,CParam>> |O
|rParam |R параметры. |<<entities/validateRParam.adoc#RParam,RParam>> |O
|===

.Пример тела запроса
[source,json]
----
{
  "opParam": {
    "id": 1,
    "op": "12345678900987654321123456789009"
  }
}
----

== Ответ

В ответе передается результаты проверки параметров центра аутентификации AuC.

[source,]
----
{
  "opParam": { "op": <bool> },
  "tkParam": { "tk": <bool> },
  "cParam": { "c": <bool> },
  "rParam": { "r": <bool> }
}
----

== Поля ответа

[cols="1,6,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|op |Результат проверки параметра Op. |bool |M
|tk |Результат проверки параметра Tk. |bool |M
|c |Результат проверки параметра C. |bool |M
|r |Результат проверки параметра R. |bool |M
|===

.Пример ответа
[source,json]
----
{
  "opParam": { "op": true },
  "tkParam": { "tk": true },
  "cParam": { "c": true },
  "rParam": { "r": true }
}
----
