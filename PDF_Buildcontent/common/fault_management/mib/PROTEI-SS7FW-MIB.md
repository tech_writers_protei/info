
```mib
-- PROTEI-SS7FW

PROTEI-SS7FW-MIB DEFINITIONS ::= BEGIN
  IMPORTS
    DisplayString FROM SNMPv2-TC
--    enterprises FROM RFC1155-SMI
    protei FROM PROTEI
    OBJECT-TYPE FROM RFC-1212
    TRAP-TYPE FROM RFC-1215
    Counter FROM RFC1155-SMI;

--protei OBJECT IDENTIFIER ::= { enterprises 20873 }
proteiSS7FW            OBJECT IDENTIFIER ::= { protei 160 }
proteiDBRM            OBJECT IDENTIFIER ::= { protei 161 }

ss7fwGeneralTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwGeneralEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW GENERAL TABLE"
	::= { proteiSS7FW 1 }

ss7fwGeneralEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwGeneralEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"GENERAL ENTRY"
	INDEX { ss7fw-genIndex }
	::= { ss7fwGeneralTable 1 }

Ss7fwGeneralEntry ::= SEQUENCE {
	ss7fw-genIndex Counter,
	ss7fw-genCA
		DisplayString,
	ss7fw-genOSTATE
		DisplayString
}

ss7fw-genIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW GENERAL INDEX"
	::= { ss7fwGeneralEntry 1 }

ss7fw-genCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW GENERAL CA"
	::= { ss7fwGeneralEntry 2 }

ss7fw-genOSTATE  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW GENERAL OSTATE"
	::= { ss7fwGeneralEntry 3 }

ss7fwStatTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwStatEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD TRAFFIC STAT"
	::= { proteiSS7FW 2 }

ss7fwStatEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwStatEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"TRAFFIC STAT ENTRY"
	INDEX { ss7fw-statIndex }
	::= { ss7fwStatTable 1 }

Ss7fwStatEntry ::= SEQUENCE {
	ss7fw-statIndex Counter,
	ss7fw-statCA
		DisplayString,
	ss7fw-statParam
		DisplayString
}

ss7fw-statIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT INDEX"
	::= { ss7fwStatEntry 1 }

ss7fw-statCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwStatEntry 2 }

ss7fw-statParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwStatEntry 3 }

ss7fwTCStatTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwTCStatEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD TRAFFIC STAT"
	::= { proteiSS7FW 3 }

ss7fwTCStatEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwTCStatEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"TRAFFIC STAT ENTRY"
	INDEX { ss7fw-tcStatIndex }
	::= { ss7fwTCStatTable 1 }

Ss7fwTCStatEntry ::= SEQUENCE {
	ss7fw-tcStatIndex Counter,
	ss7fw-tcStatCA
		DisplayString,
	ss7fw-tcStatParam
		DisplayString
}

ss7fw-tcStatIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT INDEX"
	::= { ss7fwTCStatEntry 1 }

ss7fw-tcStatCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwTCStatEntry 2 }

ss7fw-tcStatParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwTCStatEntry 3 }

ss7fwReloadTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwReloadEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD TABLE"
	::= { proteiSS7FW 4 }

ss7fwReloadEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwReloadEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"RELOAD ENTRY"
	INDEX { ss7fw-rldIndex }
	::= { ss7fwReloadTable 1 }

Ss7fwReloadEntry ::= SEQUENCE {
	ss7fw-rldIndex Counter,
	ss7fw-rldCA
		DisplayString,
	ss7fw-rldOSTATE
		DisplayString,
	ss7fw-rldParam
		DisplayString
}

ss7fw-rldIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD INDEX"
	::= { ss7fwReloadEntry 1 }

ss7fw-rldCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD CA"
	::= { ss7fwReloadEntry 2 }

ss7fw-rldOSTATE  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD OSTATE"
	::= { ss7fwReloadEntry 3 }

ss7fw-rldParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RELOAD PARAM"
	::= { ss7fwReloadEntry 4 }

ss7fwRuleTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwRuleEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RULE TABLE"
	::= { proteiSS7FW 5 }

ss7fwRuleEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwRuleEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"RELOAD ENTRY"
	INDEX { ss7fw-ruleIndex }
	::= { ss7fwRuleTable 1 }

Ss7fwRuleEntry ::= SEQUENCE {
	ss7fw-ruleIndex Counter,
	ss7fw-ruleCA
		DisplayString,
	ss7fw-ruleRULEID
		DisplayString,
	ss7fw-ruleCURRENT
		DisplayString
}

ss7fw-ruleIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RULE INDEX"
	::= { ss7fwRuleEntry 1 }

ss7fw-ruleCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RULE CA"
	::= { ss7fwRuleEntry 2 }

ss7fw-ruleRULEID  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RULE ID"
	::= { ss7fwRuleEntry 3 }

ss7fw-ruleCURRENT  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW RULE CURRENT"
	::= { ss7fwRuleEntry 4 }

ss7fwOverloadQLTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwOverloadQLEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW OVERLOAD QUEUE LOGIC"
	::= { proteiSS7FW 6 }

ss7fwOverloadQLEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwOverloadQLEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"OVERLOAD Q"
	INDEX { ss7fw-overloadQLIndex }
	::= { ss7fwOverloadQLTable 1 }

Ss7fwOverloadQLEntry ::= SEQUENCE {
	ss7fw-overloadQLIndex Counter,
	ss7fw-overloadQLCA
		DisplayString,
	ss7fw-overloadQLParam
		DisplayString
}

ss7fw-overloadQLIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT INDEX"
	::= { ss7fwOverloadQLEntry 1 }

ss7fw-overloadQLCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwOverloadQLEntry 2 }

ss7fw-overloadQLParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwOverloadQLEntry 3 }

ss7fwOverloadQCTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwOverloadQCEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW OVERLOAD QUEUE CODER"
	::= { proteiSS7FW 7 }

ss7fwOverloadQCEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwOverloadQCEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"OVERLOAD Q"
	INDEX { ss7fw-overloadQCIndex }
	::= { ss7fwOverloadQCTable 1 }

Ss7fwOverloadQCEntry ::= SEQUENCE {
	ss7fw-overloadQCIndex Counter,
	ss7fw-overloadQCCA
		DisplayString,
	ss7fw-overloadQCParam
		DisplayString
}

ss7fw-overloadQCIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW  INDEX"
	::= { ss7fwOverloadQCEntry 1 }

ss7fw-overloadQCCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwOverloadQCEntry 2 }

ss7fw-overloadQCParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwOverloadQCEntry 3 }

ss7fwOverloadHLTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwOverloadHLEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW OVERLOAD HANDLER LOGIC"
	::= { proteiSS7FW 8 }

ss7fwOverloadHLEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwOverloadHLEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"OVERLOAD H"
	INDEX { ss7fw-overloadHLIndex }
	::= { ss7fwOverloadHLTable 1 }

Ss7fwOverloadHLEntry ::= SEQUENCE {
	ss7fw-overloadHLIndex Counter,
	ss7fw-overloadHLCA
		DisplayString,
	ss7fw-overloadHLParam
		DisplayString
}

ss7fw-overloadHLIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT INDEX"
	::= { ss7fwOverloadHLEntry 1 }

ss7fw-overloadHLCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwOverloadHLEntry 2 }

ss7fw-overloadHLParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwOverloadHLEntry 3 }

ss7fwOverloadHCTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwOverloadHCEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW OVERLOAD HANDLER CODER"
	::= { proteiSS7FW 9 }

ss7fwOverloadHCEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwOverloadHCEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"OVERLOAD H"
	INDEX { ss7fw-overloadHCIndex }
	::= { ss7fwOverloadHCTable 1 }

Ss7fwOverloadHCEntry ::= SEQUENCE {
	ss7fw-overloadHCIndex Counter,
	ss7fw-overloadHCCA
		DisplayString,
	ss7fw-overloadHCParam
		DisplayString
}

ss7fw-overloadHCIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT INDEX"
	::= { ss7fwOverloadHCEntry 1 }

ss7fw-overloadHCCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT CA"
	::= { ss7fwOverloadHCEntry 2 }

ss7fw-overloadHCParam  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW TRAFFIC STAT PARAM"
	::= { ss7fwOverloadHCEntry 3 }


ss7fwDbrmClientTable  OBJECT-TYPE
	SYNTAX SEQUENCE OF Ss7fwDbrmClientEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT TABLE"
	::= { proteiDBRM 1 }

ss7fwDbrmClientEntry  OBJECT-TYPE
	SYNTAX 	Ss7fwDbrmClientEntry
	ACCESS     not-accessible
	STATUS     mandatory
	DESCRIPTION 
		"DBRM CLIENT ENTRY"
	INDEX { ss7fw-dbrmClientIndex }
	::= { ss7fwDbrmClientTable 1 }

Ss7fwDbrmClientEntry ::= SEQUENCE {
	ss7fw-dbrmClientIndex Counter,
	ss7fw-dbrmClientCA
		DisplayString,
	ss7fw-dbrmClientOSTATE
		DisplayString,
	ss7fw-dbrmClientSchema
		DisplayString,
	ss7fw-dbrmClientAddress
		DisplayString,
	ss7fw-dbrmClientBackend
		DisplayString
}

ss7fw-dbrmClientIndex  OBJECT-TYPE
	SYNTAX     Counter
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT INDEX"
	::= { ss7fwDbrmClientEntry 1 }

ss7fw-dbrmClientCA  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-only
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT CA"
	::= { ss7fwDbrmClientEntry 2 }

ss7fw-dbrmClientOSTATE  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT OSTATE"
	::= { ss7fwDbrmClientEntry 3 }

ss7fw-dbrmClientSchema  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT SCHEMA"
	::= { ss7fwDbrmClientEntry 4 }

ss7fw-dbrmClientAddress  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT ADDRESS"
	::= { ss7fwDbrmClientEntry 5 }

ss7fw-dbrmClientBackend  OBJECT-TYPE
	SYNTAX     DisplayString
	ACCESS     read-write
	STATUS     mandatory
	DESCRIPTION 
		"SS7FW DBRM CLIENT BACKEND"
	::= { ss7fwDbrmClientEntry 6 }

trap-ss7fw-gen-OSTATE-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-genCA, ss7fw-genOSTATE }
  DESCRIPTION "ss7fw-gen-OSTATE-Activate"
        --&ACTIONS { log, normal }
        --&CLEARS { 1002 }
	--&MATCH  { ss7fw-genCA, ss7fw-genOSTATE }
        --&MESG   "SS7FW (address: $'1) is $'2"
  ::= 1001

trap-ss7fw-gen-OSTATE-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-genCA, ss7fw-genOSTATE }
  DESCRIPTION "ss7fw-gen-OSTATE-Fail"
        --&ACTIONS { log, critical, alarm, sound }
        --&CLEARS { 1001 }
	--&MATCH  { ss7fw-genCA, ss7fw-genOSTATE }
        --&MESG   "SS7FW (address: $'1) is $'2"
  ::= 1002
  
trap-ss7fw-Reload  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-rldCA,    ss7fw-rldOSTATE, ss7fw-rldParam }
  DESCRIPTION "ss7fw-Reload"
        --&ACTIONS { log, info}
	--&MATCH  { ss7fw-rldCA,    ss7fw-rldOSTATE, ss7fw-rldParam }
        --&MESG   "SS7FW (address: $'1): Reload config file $'2 $'3"
  ::= 4000
                     
trap-ss7fw-Rule  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-ruleCA,    ss7fw-ruleRULEID, ss7fw-ruleCURRENT }
  DESCRIPTION "ss7fw-Rule"
        --&ACTIONS { log, info}
	--&MATCH  { ss7fw-ruleCA,    ss7fw-ruleRULEID, ss7fw-ruleCURRENT }
        --&MESG   "SS7FW (address: $'1): Rule id $'2 current $'3"
  ::= 5000
                     
trap-ss7fw-Stat  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-statCA,    ss7fw-statParam }
  DESCRIPTION "ss7fw-Stat"
        --&ACTIONS { log, info}
	--&MATCH  { ss7fw-statCA,   ss7fw-statParam }
        --&MESG   "SS7FW (address: $'1): Stat: $'2"
  ::= 2000
                     
trap-ss7fw-TCStat  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-tcStatCA,    ss7fw-tcStatParam }
  DESCRIPTION "ss7fw-TCStat"
        --&ACTIONS { log, info}
	--&MATCH  { ss7fw-tcStatCA,   ss7fw-tcStatParam }
        --&MESG   "SS7FW (address: $'1): TC Stat: $'2"
  ::= 3000
                     
trap-ss7fw-OverloadHL-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadHLCA,    ss7fw-overloadHLParam }
  DESCRIPTION "ss7fw-OverloadHL"
        --&ACTIONS { log, warning}
	--&MATCH  { ss7fw-overloadHLCA,   ss7fw-overloadHLParam }
        --&MESG   "SS7FW (address: $'1): Overload handler Logic: $'2"
  ::= 6002
                     
trap-ss7fw-OverloadHC-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadHCCA,    ss7fw-overloadHCParam }
  DESCRIPTION "ss7fw-OverloadHC"
        --&ACTIONS { log, warning}
	--&MATCH  { ss7fw-overloadHCCA,   ss7fw-overloadHCParam }
        --&MESG   "SS7FW (address: $'1): Overload handler Coder: $'2"
  ::= 7002
                     
trap-ss7fw-OverloadQL-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadQLCA,    ss7fw-overloadQLParam }
  DESCRIPTION "ss7fw-OverloadQL"
        --&ACTIONS { log, warning}
	--&MATCH  { ss7fw-overloadQLCA,   ss7fw-overloadQLParam }
        --&MESG   "SS7FW (address: $'1): Overload Queue Logic: $'2"
  ::= 8002

trap-ss7fw-OverloadQC-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadQCCA,    ss7fw-overloadQCParam }
  DESCRIPTION "ss7fw-OverloadQC"
        --&ACTIONS { log, warning}
	--&MATCH  { ss7fw-overloadQCCA,   ss7fw-overloadQCParam }
        --&MESG   "SS7FW (address: $'1): Overload Queue Coder: $'2"
  ::= 9002

trap-ss7fw-OverloadHL-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadHLCA,    ss7fw-overloadHLParam }
  DESCRIPTION "ss7fw-OverloadHL"
        --&ACTIONS { log, normal}
	--&MATCH  { ss7fw-overloadHLCA,   ss7fw-overloadHLParam }
        --&MESG   "SS7FW (address: $'1): Overload handler Logic: $'2"
  ::= 6001
                     
trap-ss7fw-OverloadHC-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadHCCA,    ss7fw-overloadHCParam }
  DESCRIPTION "ss7fw-OverloadHC"
        --&ACTIONS { log, normal}
	--&MATCH  { ss7fw-overloadHCCA,   ss7fw-overloadHCParam }
        --&MESG   "SS7FW (address: $'1): Overload handler Coder: $'2"
  ::= 7001
                     
trap-ss7fw-OverloadQL-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadQLCA,    ss7fw-overloadQLParam }
  DESCRIPTION "ss7fw-OverloadQL"
        --&ACTIONS { log, normal}
	--&MATCH  { ss7fw-overloadQLCA,   ss7fw-overloadQLParam }
        --&MESG   "SS7FW (address: $'1): Overload Queue Coder: $'2"
  ::= 8001

trap-ss7fw-OverloadQC-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-overloadQCCA,    ss7fw-overloadQCParam }
  DESCRIPTION "ss7fw-OverloadQC"
        --&ACTIONS { log, normal}
	--&MATCH  { ss7fw-overloadQCCA,   ss7fw-overloadQCParam }
        --&MESG   "SS7FW (address: $'1): Overload Queue Coder: $'2"
  ::= 9001

trap-ss7fw-DbrmClient-Activate  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-dbrmClientCA,    ss7fw-dbrmClientSchema, ss7fw-dbrmClientAddress, ss7fw-dbrmClientBackend }
  DESCRIPTION "ss7fw-DbrmClient"
        --&ACTIONS { log, normal}
        --&CLEARS { 10002 }
	--&MATCH  { ss7fw-dbrmClientCA,    ss7fw-dbrmClientSchema, ss7fw-dbrmClientAddress, ss7fw-dbrmClientBackend }
        --&MESG   "DBRM Client (address: $'1): Schema: $'2 Address: $'3 Backend: $'4"
  ::= 10001

trap-ss7fw-DbrmClient-Fail  TRAP-TYPE
  ENTERPRISE proteiSS7FW
  VARIABLES { ss7fw-dbrmClientCA,    ss7fw-dbrmClientSchema, ss7fw-dbrmClientAddress, ss7fw-dbrmClientBackend }
  DESCRIPTION "ss7fw-DbrmClient"
        --&ACTIONS { log, warning}
        --&CLEARS { 10001 }
	--&MATCH  { ss7fw-dbrmClientCA,    ss7fw-dbrmClientSchema, ss7fw-dbrmClientAddress, ss7fw-dbrmClientBackend }
        --&MESG   "DBRM Client (address: $'1): Schema: $'2 Address: $'3 Backend: $'4"
  ::= 10002
END
```