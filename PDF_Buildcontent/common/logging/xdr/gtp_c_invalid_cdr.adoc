== Описание используемых полей

[width="100%",cols="4%,27%,63%,6%",options="header",]
|===
|N |Параметр |Описание |Тип
|1 |DateTime |Дата и время формирования записи. |datetime
|2 |link:#block-cause-gtp_c_invalid_cdr[Cause] |Код причины блокировки. |int
|3 |Invalid pos |Порядковый номер байта, где возникла проблема при декодинге GTP-C. |int
|4 |Raw message |Перечень байтов в шестнадцатеричном формате. *Примечание.* Максимальный размер — 500 байт. |[hex]
|===

=== Пример

[source,log]
----
2023-11-08 14:51:46.010;2;Create session Req;200499871|80.10.181.68;0;42;434109990000010;998331005712;humans.mnc010.mcc434.gprs;25002;127.0.0.1;5555;10.0.0.1;2123;ACTION_BLOCK;RULE_BLOCK;UNKNOWN;GTP (1);;[];
2023-12-21 15:02:02.681;301;32;[48 40 00 37 00 00 00 10 d0 d3 66 00 48 00 08 00];
----

== [[block-cause-gtp_c_invalid_cdr]]Код причины блокировки

\{\{< getcontent path="/Mobile/Shared/FW/block_cause.md" >}}
