
### Описание используемых полей ###

| N     | Параметр                                | Описание                                                                                                       | Тип              |
|-------|-----------------------------------------|----------------------------------------------------------------------------------------------------------------|------------------|
| 1     | DateTime                                | Дата и время формирования записи.                                                                              | datetime         |
| 2     | ID                                      | Идентификатор запроса на Multi IMSI.                                                                           | int              |
| 3     | Version                                 | Версия GTP сообщения.                                                                                          | int              |
| 4     | [Message Type](#gtp-message-gtp_mi_cdr) | Тип обрабатываемого сообщения GTP.                                                                             | int              |
| 5     | TEID Remote                             | Идентификатор GTP-C <abbr title="Tunnel Endpoint Identifier">TEID</abbr> получателя.                           | hex              |
| 6     | TEID Local                              | Идентификатор GTP-C <abbr title="Tunnel Endpoint Identifier">TEID</abbr> отправителя.                          | hex              |
| 7     | F-TEID Local IP                         | Значение GTP-C <abbr title="Fully Qualified Tunnel Endpoint Identifier">F-TEID</abbr> IP-адрес отправителя.    | ip               |
| 8     | GTP-U                                   | Параметры плоскости пользователя.pass:q[\<br\>]GTPv1: NSAPI, TEID, GSN User plane;pass:q[\<br\>]GTPv2: Bearer ID, TEID, F-TEID IP. | \[{int,hex,ip}\] |
| 8.1.1 | NSAPI                                   | Идентификатор <abbr title="Network Service Access Point Identifier">NSAPI</abbr>.                              | int              |
| 8.1.2 | TEID                                    | TEID узла.                                                                                                     | hex              |
| 8.1.3 | GSN User plane                          | IP-адрес обслуживающего узла, SGSN Address for user traffic.                                                   | ip               |
| 8.2.1 | Bearer ID                               | Идентификатор bearer-службы.                                                                                   | int              |
| 8.2.2 | TEID                                    | TEID узла.                                                                                                     | hex              |
| 8.2.3 | F-TEID IP                               | Значение GTP-C <abbr title="Fully Qualified Tunnel Endpoint Identifier">F-TEID</abbr> IP-адрес узла.           | ip               |
| 9     | SeqNumber                               | Значение `Sequence Number` из заголовка сообщения.                                                             | int              |
| 10    | GTP_Cause                               | Код результата выполнения запроса GTP-C.                                                                       | int              |
| 11    | [Multi IMSI Result](#mi-result)         | Код результата обработки сообщения.                                                                            | int              |
| 12    | IMSI Previous                           | Номер IMSI абонента до подмены.                                                                                | string           |  
| 13    | IMSI New                                | Номер IMSI абонента после подмены.                                                                             | string           |  
| 14    | MSISDN Previous                         | Номер MSISDN абонента до подмены.                                                                              | string           |
| 15    | MSISDN New                              | Номер MSISDN абонента после подмены.                                                                           | string           |
| 16    | APN Previous                            | Назначенная точка доступа, <abbr title="Access Point Name">APN</abbr> до подмены.                              | string           |
| 17    | MNC New                                 | Код мобильной сети после подмены.                                                                              | string           |
| 18    | MCC New                                 | Мобильный код страны после подмены.                                                                            | string           |
| 19    | PLMN                                    | Установленная сеть <abbr title="Public Land Mobile Network">PLMN</abbr>.                                       | string           |
| 20    | SrcIP                                   | IP-адрес отправителя.                                                                                          | string           |
| 21    | SrcPort                                 | Порт отправителя сообщения.                                                                                    | int              |
| 22    | DestIP                                  | IP-адрес получателя сообщения.                                                                                 | string           |
| 23    | DestPort                                | Порт получателя сообщения.                                                                                     | int              |
| 24    | Duration                                | Время обработки запроса, в миллисекундах.                                                                      | int              |

#### Примеры

```log
2024-02-19 23:46:48.113;8891889172147601409;2;32;0x00000000;0x80464269;193.27.231.61;[{5,0x8245a269,193.27.231.61}];416361;;0;250770002725692;234568897678934;79410253885;79213280432;testinternet.mnc077.mcc250.gprs;002;257;25001;193.27.231.61;39872;193.232.47.142;2123;4;
2024-02-15 23:50:13.764;8891889172147601409;2;33;0x80464269;0x00000010;193.232.47.142;[{5,0x00000010,193.232.47.142}];416361;16;407;;;;;;;;193.232.47.142;2123;193.27.231.61;39872;0;
```

### [[gtp-message-gtp_mi_cdr]]Код сообщений GTP

{{< getcontent path="/Mobile/Shared/FW/gtp_message.md" >}}

### [[mi-result]]Результат обработки сообщения

| Код | Результат                     | Описание                                                                                                                               |
|-----|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| 0   | BC_SUCCESS                    | Подмена параметров в сообщении успешно выполнена                                                                                       |
| 400 | BC_GTP_MI_REPLACEMENT_FAIL    | Ошибка при замене параметров в сообщении                                                                                               |
| 401 | BC_GTP_MI_PROFILE_NOT_FOUND   | Не найден требуемый профиль, домашний или дополнительный активный, в ответе от узла Multi IMSI                                         |
| 402 | BC_GTP_MI_NO_SUCH_USER_ID     | Исходные значения параметров абонента IMSI и MSISDN в ответе от узла Multi IMSI не соответствуют ни одному из запросов                 |
| 403 | BC_GTP_MI_MATCH_CFG_MASK_FAIL | Не найдено ни одного соответствия между IP-адресом входящего сообщения и масками домашней и гостевой подсетей, заданных в конфигурации |
| 404 | BC_GTP_MI_RESPONSE_PARSE_FAIL | Ошибка при парсинге ответа от узла Multi IMSI                                                                                          |
| 405 | BC_GTP_MI_RESPONSE_FAIL       | Получен пустой ответ или запрос не был успешно выполнен                                                                                |
| 406 | BC_GTP_MI_RESPONSE_TIMEOUT    | Таймаут ответа от узла Multi IMSI                                                                                                      |
| 407 | BC_GTP_MI_IMSI_MSISDN_EMPTY   | В исходном сообщении GTP нет полей IMSI и MSISDN, запрос на узел Multi IMSI не был создан                                              |