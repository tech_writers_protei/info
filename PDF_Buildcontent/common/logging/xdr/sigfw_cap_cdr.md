
### Описание используемых полей ###

| N  | Параметр                              | Описание                                                                                                                                                                                                                     | Тип        |
|----|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| 1  | DateTime                              | Дата и время формирования записи.                                                                                                                                                                                            | datetime   |
| 2  | Msg_Id                                | Идентификатор сообщения.                                                                                                                                                                                                     | int        |
| 3  | MTP3_OPC                              | Значение <abbr title="Origination Point Code">OPC</abbr>.pass:q[\<br\>]**Примечание.** Если отсутствует, то значение `-1`.                                                                                                             | int        |
| 4  | MTP3_DPC                              | Значение <abbr title="Destination Point Code">DPC</abbr>.pass:q[\<br\>]**Примечание.** Если отсутствует, то значение `-1`.                                                                                                             | int        |
| 5  | SCCP.GT_A                             | Номер GT отправителя.                                                                                                                                                                                                        | string     |
| 6  | SCCP.GT_A_SSN                         | Номер подсистемы, <abbr title="Subsystem Number">SSN</abbr>, GT отправителя.                                                                                                                                                 | int        |
| 7  | SCCP.GT_A_NP                          | Код плана нумерации, <abbr title="Numbering Plan">NP</abbr>, GT отправителя.                                                                                                                                                 | int        |
| 8  | SCCP.GT_A_TT                          | Код типа трансляции, <abbr title="Translation Type">TT</abbr>, GT отправителя.                                                                                                                                               | int        |
| 9  | SCCP.GT_A_NAI                         | Индикатор источника адреса, <abbr title="Nature of Address Indicator">NAI</abbr>, GT отправителя.                                                                                                                            | int        |
| 10 | SCCP.GT_A_ES                          | Схема кодирования, <abbr title="Encoding Scheme">ES</abbr>, GT отправителя.                                                                                                                                                  | int        |
| 11 | SCCP.GT_A_RI                          | Индикатор маршрута, <abbr title="Routing Indicator">RI</abbr>, GT отправителя.                                                                                                                                               | int        |
| 12 | SCCP.GT_B                             | Номер GT получателя.                                                                                                                                                                                                         | string     |
| 13 | SCCP.GT_B_SSN                         | Номер подсистемы, <abbr title="Subsystem Number">SSN</abbr>, GT получателя.                                                                                                                                                  | int        |
| 14 | SCCP.GT_B_NP                          | Код плана нумерации, <abbr title="Numbering Plan">NP</abbr>, GT получателя.                                                                                                                                                  | int        |
| 15 | SCCP.GT_B_TT                          | Код типа трансляции, <abbr title="Translation Type">TT</abbr>, GT получателя.                                                                                                                                                | int        |
| 16 | SCCP.GT_B_NAI                         | Индикатор источника адреса, <abbr title="Nature of Address Indicator">NAI</abbr>, GT получателя.                                                                                                                             | int        |
| 17 | SCCP.GT_B_ES                          | Схема кодирования, <abbr title="Encoding Scheme">ES</abbr>, GT получателя.                                                                                                                                                   | int        |
| 18 | SCCP.GT_B_RI                          | Индикатор маршрута, <abbr title="Routing Indicator">RI</abbr>, GT получателя.                                                                                                                                                | int        |
| 19 | TCAP_Msg_Type                         | Тип сообщения.pass:q[\<br\>]`2` - TCAP_BEGIN / `4` - TCAP_END / `5` - TCAP_CONTINUE / `7` - TCAP_ABORT.                                                                                                                                | int        |
| 20 | TCAP_ACN                              | Имя контекста приложения, <abbr title="Application Context Name">ACN</abbr>.pass:q[\<br\>]**Примечание.** Записываются только цифры, без точек.                                                                                        | string     |
| 21 | TCAP_OTID                             | Значение <abbr title="Original Transaction Identifier">OTID</abbr>.                                                                                                                                                          | hex        |
| 22 | TCAP_DTID                             | Значение <abbr title="Destination Transaction Identifier">DTID</abbr>.                                                                                                                                                       | hex        |
| 23 | TCAP_OpCode                           | Код операции MAP / CAP, `OpCode`.pass:q[\<br\>]**Примечание.** Если отсутствует, то значение `-1`.                                                                                                                                     | int        |
| 24 | Component                             | Код компоненты.pass:q[\<br\>]`1` - INVOKE / `2` - RETURN_RESULT / `3` - RETURN_ERROR / `4` - REJECT / `7` - RETURN_RESULT_NOT_LAST.                                                                                                    | int        |
| 25 | IMSI                                  | Номер IMSI.                                                                                                                                                                                                                  | string     |
| 26 | VLR                                   | Номер обслуживающего узла VLR/SGSN.                                                                                                                                                                                          | string     |
| 27 | MSC                                   | Адрес обслуживающего узла MSC.                                                                                                                                                                                               | string     |
| 28 | CAP_SCF                               | Адрес SCF в протоколе CAP.                                                                                                                                                                                                   | string     |
| 29 | [Action](#action-sigfw_cap_cdr)       | Код дальнейшего действия.                                                                                                                                                                                                    | int        |
| 30 | [Cause](#block-cause-sigfw_cap_cdr)   | Код причины блокировки.                                                                                                                                                                                                      | int        |
| 31 | [Result](#rule-result-sigfw_cap_cdr)  | Код ошибки CAP, определенный правилами.                                                                                                                                                                                      | int        |
| 32 | TermRule                              | Последнее правило, которое активировало завершение обработку сообщения.pass:q[\<br\>]Если в последнем правиле Action не активировался, остается пустым.                                                                                | string     |
| 33 | TermAction                            | Действие последнего правила цепочки.pass:q[\<br\>]Если в последнем правиле Action не активировался, остается пустым.                                                                                                                   | string     |
| 34 | TagKeys                               | Перечень присвоенных ключей тегов.                                                                                                                                                                                           | \[string\] |
| 35 | TagValues                             | Перечень присвоенных значений тегов.                                                                                                                                                                                         | \[string\] |
| 36 | Nodes                                 | Перечень имен пройденных правил и роутеров, по порядку прохождения.                                                                                                                                                          | \[string\] |
| 37 | Types                                 | Перечень типов узлов.                                                                                                                                                                                                        | \[string\] |
| 38 | Keys                                  | Перечень признаков, что сообщение удовлетворило **все** ключи соответствующего узла.pass:q[\<br\>]`0` - хотя бы один ключ не пройден;pass:q[\<br\>]`1` - пройдены все ключи;pass:q[\<br\>]`2` - если узел являлся маршрутизатором или правилом без ключей. | \[int\]    |
| 39 | Thresholds                            | Перечень признаков, что сообщение не превысило порог соответствующего узла.pass:q[\<br\>]`0` - хотя бы один ключ не пройден;pass:q[\<br\>]`1` - пройдены все ключи;pass:q[\<br\>]`2` - если узел являлся маршрутизатором или правилом без порогов.         | \[int\]    |
| 40 | Actions                               | Перечень действий.pass:q[\<br\>]Если узел являлся маршрутизатором, или сообщение не дошло до действий (не прошло по ключу или порогу), то пустая строка.                                                                               | \[string\] |
| 41 | TermCategory                          | Номер категории последнего правила в цепочке.                                                                                                                                                                                | int        |
| 42 | TermAttackType                        | Тип атаки последнего правила в цепочке.                                                                                                                                                                                      | string     |
| 43 | [VC_Status](#vc-status-sigfw_cap_cdr) | Статус проверки перемещения                                                                                                                                                                                                  | int        |
| 44 | Velocity                              | Скорость перемещения, в километрах в час.                                                                                                                                                                                    | double     |
| 45 | NewLocation                           | Идентификатор новой локации.                                                                                                                                                                                                 | int        |
| 46 | StoreLocation                         | Идентификатор текущей локации.                                                                                                                                                                                               | int        |
| 47 | NewNetworkArea                        | Идентификатор новой сетевой зоны.                                                                                                                                                                                            | int        |
| 48 | StoreNetworkArea                      | Идентификатор текущей сетевой зоны.                                                                                                                                                                                          | int        |
| 49 | TransactionId                         | Уникальный идентификатор для связки всех сообщений транзакции.                                                                                                                                                               | string     |
| 50 | TreeId                                | Идентификатор логики.                                                                                                                                                                                                        | int        |
| 51 | Duration                              | Время обработки запроса, в миллисекундах.                                                                                                                                                                                    | int        |
| 52 | SwitchingMode                         | Флаг активации переходного режима пропуска хвоста TCAP-транзакции.                                                                                                                                                           | bool       |
| 53 | [LC_Status](#lc-status-sigfw_cap_cdr) | Статус проверки актуальности информации о местоположении.                                                                                                                                                                    | int        |
| 54 | StoreRegTime                          | Время, прошедшее после последней регистрации абонента, в секундах.                                                                                                                                                           | int        |

#### Пример

```log
2023-12-26 16:02:51;0;-1;-1;43022242641;6;1;0;4;6;2;11199342636;6;1;0;4;6;2;4;040010501;00000701;;0;1;444520000000701;79990000002;79990000002;;1;116;;;;[];[];[];[];[];[];[];;;0;0.00;-1;-1;-1;-1;tr-0;397;0;0;0;
2023-12-26 16:02:29;0;-1;-1;99999342639;6;1;0;4;6;2;99999342636;6;1;0;4;6;2;2;040010501;00000003;;0;1;250621001694189;79043300021;79043300021;;0;0;0;RouterCheckCap;;[];[];["Main","MainSS7","RouterCheckCap"];["routerprotocol","routertcap","routersccp"];[2,2,2];[0,0,0];[",","];0;;0;0.00;-1;-1;-1;-1;tr-268435583;123;0;0;0;
2023-12-26 16:02:29;0;-1;-1;43122242641;6;1;0;4;6;2;11199342640;6;1;0;4;6;2;2;040010501;00000705;;0;1;444520000000706;99990100001;99990000001;;1;108;0;RuleCapMaskComparation;Reject;[];[];["Main","MainSS7","RouterCheckCap","RuleCapMaskComparation"];["routerprotocol","routertcap","routersccp","rule"];[2,2,2,1];[0,0,0,0];[",",","Reject"];0;;0;0.00;-1;-1;-1;-1;tr-268435672;325;1;0;0;
2023-12-26 16:02:24;52;300;400;93199342635;6;1;0;4;6;2;99999342634;6;1;0;4;6;2;2;040010293;00000002;00000007;71;1;203000000000002;93199342535;71113332211;1111111;1;108;0;SomeRule;SomeAction;["Message",yes];[];["FirstRule","SecondRule"];["routerprotocol","routertcap","rule"];[2,1,0,1,2];[0,0,0,0,0];[",",","Reject"];0;123;0;102.536;555;556;665;663;tr-268435673;1;100;0;0;
```

### [[action-sigfw_cap_cdr]]Действие, Action


* 0 -- Pass, пропустить;

* 1 -- Block, блокировать, данная компонента не прошла проверку;

* 2 -- BlockOtherComponent, блокировать, соседняя компонента не прошла проверку;

* 5 -- Pass & SendAlarm, пропустить и отправить аварию.

### [[block-cause-sigfw_cap_cdr]]Код причины блокировки

{{< getcontent path="/Mobile/Shared/FW/block_cause.md" >}}

### [[rule-result-sigfw_cap_cdr]]Результат обработки правилами

{{< getcontent path="/Mobile/Shared/FW/rule_result.md" >}}

### [[vc-status-sigfw_cap_cdr]]Статус VelocityCheck, VC_Status

{{< getcontent path="/Mobile/Shared/FW/vc_status.md" >}}

### [[lc-status-sigfw_cap_cdr]]Статус LocationCheck, LC_Status

{{< getcontent path="/Mobile/Shared/FW/lc_status.md" >}}