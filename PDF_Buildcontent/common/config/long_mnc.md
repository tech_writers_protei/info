
В файле задаются сети PLMN, имеющие трехзначные MNC.

Консольная команда для обновления конфигурационного файла без перезапуска узла - **reload long_mnc.json**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр | Описание                                    | Тип    | O/M | P/R | Версия   |
|----------|---------------------------------------------|--------|-----|-----|----------|
| mcc      | Мобильный код страны.                       | string | M   | R   | 1.1.0.31 |
| mnc      | Код мобильной сети, состоящий из трёх цифр. | string | M   | R   | 1.1.0.31 |

#### Пример ####

```json
[
  {
    "mcc":"404",
    "mnc":"854"
  },
  {
    "mcc":"404",
    "mnc":"874"
  }
]
```

