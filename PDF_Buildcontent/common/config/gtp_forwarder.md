
В файле задаются настройки компоненты GTP Forwarder для трансляции сообщений.

## Используемые секции ##


* **[\[General\]](#general-gtp-forwarder)** - общие параметры компонент;

* **[\[IpFragmentation\]](#ip-fragmentation)** - параметры алгоритмов фрагментации и дефрагментации IP-пакетов;

* **[\[DumpWriter\]](#dump-writer)** -- параметры записи файлов дампа;

* **[\[NICs\]](#nics)** -- параметры сетевых интерфейсов NIC;

* **[\[PacketStatistics\]](#packet-statistics)** -- параметры ведения статистики;

* **[\[GtpSession\]](#gtp-session-gtp-forwarder)** -- параметры контроля сессий.

### Описание параметров ###

<!--
1. RxMbufPoolSize - ... в пуле на Rx у очередей данного порта. Это в смысле для очередей на прием?
2. Почти уверен, что это параметр для DPDK. А как он там называется? Чтобы сослаться на доку, мол, если что, читай там. Поискал похожее, но сходу не нашел.
-->

| Параметр                                                   | Описание                                                                                                                                                                                                              | Тип        | O/M | P/R | Версия   |
|------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|----------|
| **[[general-gtp-forwarder]]\[General\]**        | Общие параметры интерфейсов.                                                                                                                                                                                          | object     | O   | P   | 1.0.14.0 |
| ForwardOtherPackets                                        | Флаг передачи не-GTP пакетов.pass:q[\<br\>]По умолчанию: 1.                                                                                                                                                                     | bool       | O   | R   | 1.0.14.0 |
| GTP_C_Port                                                 | UDP-порт для фильтрации GTPv2-C пакетов.pass:q[\<br\>]По умолчанию: 2123.                                                                                                                                                       | int        | O   | R   | 1.0.14.0 |
| GTP_U_Port                                                 | UDP-порт для фильтрации GTPv1-U пакетов.pass:q[\<br\>]По умолчанию: 2152.                                                                                                                                                       | int        | O   | R   | 1.0.14.0 |
| MTU                                                        | Максимальный размер фрейма, в байтах.pass:q[\<br\>]По умолчанию: 3000.                                                                                                                                                          | int        | O   | P   | 1.0.15.7 |
| UdpPortsToForward                                          | Перечень UDP-портов назначения для фильтрации пакетов, которые должны быть переданы.                                                                                                                                  | \[int\]    | O   | R   | 1.1.0.3  |
| TcpPortsToForward                                          | Перечень TCP-портов назначения для фильтрации пакетов, которые должны быть переданы.                                                                                                                                  | \[int\]    | O   | R   | 1.1.0.3  |
| **[[ip-fragmentation]]\[IpFragmentation\]**     | Параметры алгоритмов фрагментации и дефрагментации.                                                                                                                                                                   | object     | O   | P   | 1.0.14.0 |
| FragmentTTL                                                | Время жизни одного фрагмента, в миллисекундах.pass:q[\<br\>]По умолчанию: 1000.                                                                                                                                                 | string     | O   | P   | 1.0.15.8 |
| [[bucket-entries]]BucketEntries                 | Количество записей в одной ячейке таблицы дефрагментации.pass:q[\<br\>]По умолчанию: 64.pass:q[\<br\>]**Примечание.** Значение должно быть целой положительной степенью числа 2. При необходимости будет изменено приложением.            | int        | O   | P   | 1.1.0.30 |
| [[bucket-num]]BucketNum                         | Количество ячеек в таблице дефрагментации.pass:q[\<br\>]По умолчанию: 64.                                                                                                                                                       | int        | O   | P   | 1.1.0.30 |
| MaxEntries                                                 | Максимальное допустимое количество записей в таблице дефрагментации.pass:q[\<br\>]По умолчанию: 2048.pass:q[\<br\>]**Примечание.** Не может превышать значение `BucketEntries * BucketNum`. При необходимости будет изменено приложением. | int        | O   | P   | 1.1.0.30 |
| **[[dump-writer]]\[DumpWriter\]**               | Параметры записи файлов дампа.                                                                                                                                                                                        | object     | O   | R   | 1.0.15.9 |
| *\<FileName\>*                                             | Название файла дампа.pass:q[\<br\>]`RX` - дамп всех входящих пакетов;pass:q[\<br\>]`TX` - дамп всех исходящих пакетов;pass:q[\<br\>]`RX_GTP_C` - дамп входящих GTP-C пакетов;pass:q[\<br\>]`TX_GTP_C` - дамп исходящих GTP-C пакетов.                         | object     | O   | R   | 1.0.15.9 |
| WriteDump                                                  | Флаг записи файлов дампа.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                                                         | bool       | O   | R   | 1.0.15.9 |
| DumpFileSize                                               | Максимальный размер PCAP-файла для ротации, в мегабайтах.pass:q[\<br\>]По умолчанию: 50.                                                                                                                                        | int        | O   | R   | 1.0.15.9 |
| DumpFileCount                                              | Максимальное количество PCAP-файлов для ротации.pass:q[\<br\>]По умолчанию: 20.                                                                                                                                                 | int        | O   | R   | 1.0.15.9 |
| CpuId                                                      | Номер CPU, где запускается поток для записи дампа.pass:q[\<br\>]**Примечание.** Если не указано, привязка к CPU не применяется.                                                                                                 | int        | O   | P   | 1.1.0.30 |
| Path                                                       | Директория для записи PCAP-файлов.pass:q[\<br\>]По умолчанию: ./dumps.                                                                                                                                                          | string     | O   | P   | 1.1.0.30 |
| **[[nics]]\[NICs\]**                            | Параметры контроллеров сетевых интерфейсов.                                                                                                                                                                           | \[object\] | M   | P   | 1.0.14.0 |
| Type                                                       | Тип драйвера, используемого для интерфейса.pass:q[\<br\>]`AF_PACKET` / `DPDK`.                                                                                                                                                  | string     | M   | P   | 1.0.14.0 |
| Port                                                       | Идентификатор интерфейса.pass:q[\<br\>]Для `Type = AF_PACKET` задается название интерфейса.pass:q[\<br\>]Для `Type = DPDK` задается PCI-адрес устройства.                                                                                 | string     | M   | P   | 1.0.14.0 |
| TxID                                                       | Идентификатор интерфейса для передачи данных из списка доступных контроллеров.                                                                                                                                        | int        | M   | P   | 1.0.14.0 |
| Queues                                                     | Количество используемых очередей интерфейса. По умолчанию: 1.pass:q[\<br\>]**Примечание.** Соответствует количеству форвардеров.                                                                                                | int        | O   | P   | 1.0.14.0 |
| CpuIds                                                     | Перечень ядер CPU, где запускаются потоки форвардеров.**Примечание.** Если не указано, форвардеры не привязываются к конкретным CPU процессора.                                                                       | \[int\]    | O   | P   | 1.1.0.23 |
| MAC                                                        | MAC-адрес для подмены.pass:q[\<br\>]В пакете заменяется `Destination MAC` на значение, указанное в Tx-интерфейсе.pass:q[\<br\>]**Примечание.** Если не указано, подмена не выполняется.                                                   | string     | O   | P   | 1.1.0.3  |
| VlanId                                                     | Идентификатор VLAN для подмены.pass:q[\<br\>]В пакете заменяется `VlanId` на значение, указанное в Tx-интерфейсе.pass:q[\<br\>]**Примечание.** Если не указано, подмена не выполняется.                                                   | int        | O   | P   | 1.1.0.3  |
| NbRxDesc                                                   | Количество Rx-дескрипторов, выделенных для каждой Rx-очереди.pass:q[\<br\>]По умолчанию: 2048.                                                                                                                                  | int        | O   | P   | 1.1.0.30 |
| NbTxDesc                                                   | Количество Tx-дескрипторов, выделенных для каждой Tx-очереди.pass:q[\<br\>]По умолчанию: 1024.                                                                                                                                  | int        | O   | P   | 1.1.0.30 |
| RxMbufPoolSize                                             | Количество буферов `rte_mbuf` в пуле на Rx у очередей данного порта.pass:q[\<br\>]**Примечание.** Рекомендуются значения, на 1 меньше какой-либо степени числа 2.pass:q[\<br\>]По умолчанию: 32&nbsp;767.                                 | int        | O   | P   | 1.1.0.30 |
| **[[packet-statistics]]\[PacketStatistics\]**   | Параметры журналирования.                                                                                                                                                                                             | object     | O   | R   | 1.1.0.11 |
| Interval                                                   | Интервал записи статистики, в секундах.pass:q[\<br\>]По умолчанию: 5.                                                                                                                                                           | int        | O   | R   | 1.1.0.11 |
| **[[gtp-session-gtp-forwarder]]\[GtpSession\]** | Параметры контроля сессий GTP.                                                                                                                                                                                        | object     | O   | P   | 1.1.0.18 |
| GtpUSessionValidationEnable                                | Флаг проверки соответствия GTP-U пакетов установленным сессиям.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                   | int        | O   | R   | 1.1.0.18 |
| GtpUMaxRedisRequests                                       | Максимальное количество обращений к базе данных Redis в секунду для GTP-U трафика.pass:q[\<br\>]По умолчанию: 10&nbsp;000.                                                                                                      | int        | O   | R   | 1.1.0.29 |

#### Пример

```ini
[General]
ForwardOtherPackets = 1;
UdpPortsToForward = { 32555; 33666 }
TcpPortsToForward = { 22777; 23333 }

[DumpWriter]
RX = {
  WriteDump = 1
  DumpFileSize = 100
  DumpFileCount = 5
}
TX = {
  WriteDump = 1
  DumpFileSize = 50
}

[DumpWriter]
RX = {
    WriteDump = 0
}
TX = {
    WriteDump = 0
}
RX_GTP_C = {
    WriteDump = 1
    CpuId = 7
    DumpFileSize = 20
}
TX_GTP_C = {
    WriteDump = 1
    CpuId = 7
    DumpFileSize = 20
}

[NICs]
{
  Port = eth0;
  Type = AF_PACKET;
  Queues = 1;
  CpuIds = { 0 };
  TxID = 1;
  MAC = 1:2:3:4:5:6;
  VlanId = 15;
};
{
  Port = eth1;
  Type = AF_PACKET;
  Queues = 1;
  CpuIds = { 1 };
  TxID = 0;
  MAC = aa:bb:cc:dd:ee:ff;
  VlanId = 10;
};
```