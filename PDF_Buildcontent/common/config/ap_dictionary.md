
{{< getcontent path="/Mobile/Shared/config/ap_dictionary.md" >}}

#### Пример

```ini
# Dictionary

OSTATE = { { 1; ACTIVATE }; { 0; FAIL }; };
ASTATE = { { 1; UNBLOCKED }; { 0; BLOCKED }; };
HSTATE = { { 1; ON }; { 0; OFF }; };
```