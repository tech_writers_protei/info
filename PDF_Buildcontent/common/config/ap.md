
{{< getcontent path="/Mobile/Shared/config/ap.md" >}}

#### Пример ####

```ini
[General]
Root = PROTEI(1,3,6,1,4,1,20873)
ApplicationAddress = SS7FW
MaxConnectionCount = 10
ManagerThread = 1
CyclicTreeWalk = 0

[Dynamic]
[AtePath2ObjName]
{ SS7FW(146).General(1,1); CA(2); }
{ SS7FW(146).General(1,1); OSTATE(3); }
{ SS7FW(146).Traffic,Stat(2,1); CA(2); };
{ SS7FW(146).Traffic,Stat(2,1); PARAM(3); };
{ SS7FW(146).Abonent,Stat(3,1); CA(2); };
{ SS7FW(146).Abonent,Stat(3,1); PARAM(3); };
{ SS7FW(146).Reload(4,1); CA(2); };
{ SS7FW(146).Reload(4,1); OSTATE(3); };
{ SS7FW(146).Reload(4,1); PARAM(4); };

{ SS7FW(146).OVRLOAD,Handler,SL(5,1); CA(2); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); CA(2); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); PARAM(4); };

[SNMP]
ListenIP  =  0.0.0.0;
ListenPort  =  3128

[StandardMib]
#sysDescr
{1.3.6.1.2.1.1.1.0;STRING;"CallMe"; };
#sysObjectID
{1.3.6.1.2.1.1.2.0;OBJECT_ID;1.3.6.1.4.1.20873; };

[AtePath2Oid]
[SNMPTrap]
FirstVarOwn  =  0;
IndexID  =  1;

[Filter]
CA_Object = ".*"
CT_Object = ".*"
CA_Var = ".*"
TrapIndicator = -1
DynamicIndicator = -1

[SpecificTrapCT_Object]
{ SS7FW.General; 1; }
{ SS7FW.Traffic.Stat; 2; }
{ SS7FW.Abonent.Stat; 3; }
{ SS7FW.Reload; 4; }
{ SS7FW.OVRLOAD.*; 5; }

[SpecificTrapCA_Var]
{ Alarm.Decode.*; 111; }
{ Alarm.Encode.*; 121; }
{ Alarm.CDI.*; 131; }
{ Warn.ErrCodeInfo.ASP.Connect.*; 141; }
{ Warn.ASPUP.*; 151; }
{ Warn.ASPDN.*; 161; }
{ Info.DAVA.*; 171; }
{ Info.DUNA.*; 181; }
{ Info.SCON.*; 191; }
{ Info.DUPU.*; 211; }
{ Info.DRST.*; 221; }
{ Info.ASP.UP.*; 231; }
{ Info.ASP.Connect.*; 232; }
{ Alarm.DPC.*; 241; }
{ Alarm.UP.*; 251; }
{ Alarm.ChCfg.Invalid.*; 261; }
{ Warn.Act.*; 271; }
{ Warn.Deact.*; 281; }
{ Warn.Reg.*; 291; }
{ Warn.Dereg.*; 311; }
{ Warn.ASP.Failure.*; 321; }
{ Info.LinkUP.Num.*; 331; }
{ Info.ASInit.*; 341; }
{ Info.AS.ACT.*; 342; }
```

### ap_dictionary