
**RouterSCCP** маршрутизирует сообщения по адресу вызывающего и вызываемого абонента.

**Примечание.** Также может подменять параметры *CdPA*, *CgPA*.

### Описание параметров ###

| Параметр                                         | Описание                                                                                                                                  | Тип        | O/M | P/R | Версия |
|--------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| ID                                               | Уникальный идентификатор узла маршрутизации.                                                                                              | int        | O   | R   |        |
| Name                                             | Уникальное имя узла маршрутизации.                                                                                                        | string     | M   | R   |        |
| Type                                             | Тип узла маршрутизации. Должно быть `RouterSCCP`.                                                                                         | string     | M   | R   |        |
| Routing                                          | Параметры переадресации.                                                                                                                  | \[object\] | M   | R   |        |
| *{*                                              |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Key                                  | Главный ключ для задания адресации.                                                                                                       | \[object\] | M   | R   |        |
| &nbsp;&nbsp;*{*                                  |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_A](#gt-sccp)         | Параметры GT отправителя.                                                                                                                 | object     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_B](#gt-sccp)         | Параметры GT получателя.                                                                                                                  | object     | O   | R   |        |
| &nbsp;&nbsp;*}*                                  |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Route                                | Параметры последующей маршрутизации при совпадении ключа.                                                                                 | object     | O   | R   |        |
| &nbsp;&nbsp;*{*                                  |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_A_set](#gt-set-sccp) | Параметры измененного GT отправителя.                                                                                                     | object     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_B_set](#gt-set-sccp) | Параметры измененного GT получателя.                                                                                                      | object     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;GOTO                     | Название узла, куда перенаправляется запрос.pass:q[\<br\>]**Примечание.** Должно совпадать с именем существующего правила / маршрутизатора / "OUT". | string     | M   | R   |        |
| &nbsp;&nbsp;*}*                                  |                                                                                                                                           |            |     |     |        |
| *}*                                              |                                                                                                                                           |            |     |     |        |

#### [[gt-sccp]]Параметры GT

| Параметр | Описание                                                       | Тип   | O/M | P/R | Версия |
|----------|----------------------------------------------------------------|-------|-----|-----|--------|
| TT       | Значение <abbr title="Translation Type">TT</abbr>.             | int   | O   | R   |        |
| NAI      | Значение <abbr title="Nature of Address Indicator">NAI</abbr>. | int   | O   | R   |        |
| SSN      | Значение <abbr title="Subsystem Number">SSN</abbr>.            | int   | O   | R   |        |
| NP       | Значение <abbr title="Numbering Plan">NP</abbr>.               | int   | O   | R   |        |
| ES       | Значение <abbr title="Encoding Scheme">ES</abbr>.              | int   | O   | R   |        |
| RI       | Значение <abbr title="Routing Indicator">RI</abbr>.            | int   | O   | R   |        |
| Address  | Шаблон адреса.                                                 | regex | O   | R   |        |

#### [[gt-set-sccp]]Параметры изменения GT

| Параметр              | Описание                                                       | Тип    | O/M | P/R | Версия |
|-----------------------|----------------------------------------------------------------|--------|-----|-----|--------|
| TT                    | Значение <abbr title="Translation Type">TT</abbr>.             | int    | O   | R   |        |
| NAI                   | Значение <abbr title="Nature of Address Indicator">NAI</abbr>. | int    | O   | R   |        |
| SSN                   | Значение <abbr title="Subsystem Number">SSN</abbr>.            | int    | O   | R   |        |
| NP                    | Значение <abbr title="Numbering Plan">NP</abbr>.               | int    | O   | R   |        |
| ES                    | Значение <abbr title="Encoding Scheme">ES</abbr>.              | int    | O   | R   |        |
| RI                    | Значение <abbr title="Routing Indicator">RI</abbr>.            | int    | O   | R   |        |
| Address               | Параметры изменения адреса.                                    | object | O   | R   |        |
| *{*                   |                                                                |        |     |     |        |
| &nbsp;&nbsp;DelDigits | Количество удаляемых цифр из начала номера.                    | int    | O   | R   |        |
| &nbsp;&nbsp;AddPrefix | Символы, добавляемые в начало номера.                          | string | O   | R   |        |
| *}*                   |                                                                |        |     |     |        |

#### Пример ####

```json
{
  "ID": 20,
  "Name": "MO_Router",
  "Type": "RouterSCCP",
  "Routing": [
    {
      "Key": {
        "GT_A": {
          "Address": "7921.(0,10)",
          "TT": 1, 
          "NAI": 2,
          "SSN": 7,
          "NP": 1,
          "ES": 4
        },    
        "GT_B": {}
      },
      "Route": {
        "GT_A_set": {
          "NAI": 1,
          "TT": 1,
          "NP": 1,
          "SSN": 6,
          "ES": 4,
          "RI": 0,
          "Address": {
            "DelDigits": 4,
            "AddPrefix": "7911"
          }
        },    
        "GT_B_set": {},
        "GOTO": "Rule1"
      }
    },
    {
      "Key": { "GT_A": { "TT": 55 } },
      "Route": {
        "GT_A_set": { "TT": 0 },
        "GOTO": "MO_LocalRule"
      }
    },
    {
      "Key": { "GT_A": { "TT": 76 } },
      "Route": {
        "GT_A_set": { "TT" : 0 },
        "GOTO": "MO_InboundRule"
      }
    },
    {
      "Key": { "GT_A": { "TT": 75 } },
      "Route": {
        "GT_A_set": { "TT": 0 },
        "GOTO": "MO_OutboundRule"
      }
    },
    {
      "Key": {},
      "Route": {
        "GT_A_set": { "TT": 0 },
        "GOTO": "UnknownDirectionRule"
      }
    }
  ]
}
```