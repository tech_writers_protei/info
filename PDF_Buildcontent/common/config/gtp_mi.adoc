В файле задаются настройки работы c компонентой Multi IMSI.

== Описание параметров

[width="100%",cols="14%,64%,10%,3%,3%,6%",options="header",]
|===
|Параметр |Описание |Тип |O/M |P/R |Версия
|*[General]* |Общие параметры работы с GTP MI. | | | |
|DirectionID |HTTP-направление для Multi IMSI. |int |M |R |1.1.0.32
|IpMasksHome |Mаски хостов домашней сети. |[ip/string] |M |R |1.1.0.32
|IpMasksGuest |Маски хостов гостевой сети.pass:q[<br>]По умолчанию: все подсети не подходящие под маску IpMasksHome. |[ip/string] |O |R |1.1.0.32
|===

=== Пример

[source,ini]
----
[General]
DirectionID = 1;
IpMasksHome = { 192.168.231.1/30; e80:a0a4::/32 }
IpMasksGuest = { 192.168.231.1/26; e80:a0a4::/32 }
----
