
{{ getcontent path="/Mobile/Shared/config/trace.md" }}

#### Пример ####

```
[Trace]
common = {
  tracing = 1;
  dir = ".";
  no_signal = all;
}

logs = {
  stat = {
    file = "logs/stat/stat-%Y%m%d.log";
    mask = date & time & tick;
    level = 4;
    separator = ";";
  };
  tc_stat = {                                                                                                                                                                                         
    file = "logs/stat.log";                                                                                                                                                                        
    mask = file & date & time & tick;                                                                                                                                                                         
    level = 4;                                                                                                                                                                                        
    separator = ";";                                                                                                                                                                                   
  };
  trace = {
    file = "logs/trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    period = day;
    separator = ";";
  };
  SS7FW_trace = {
    file = "logs/ss7fw_trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    period = day;
    tee = trace;
    separator = ";";
  };
  http_trace = {
    file = "logs/http_trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    separator = ";";
    tee = trace;
  };
  http_warning = {
    file = "logs/http_warning.log";
    mask = file & date & time & tick & pid;
    level = 1;
    separator = ";";
  };
  GTP_C_trace = {
    file = "logs/gtp_c/GTP_C_%H%M_%d%m%Y.log";
    mask = date & time & tick;
    period = hour;
    level = 10;
  };
  GTP_C_warning = {
    file = "logs/gtp_c_warning.log";
    mask = date & time & tick;
    level = 10;
  };
  profilers = {
    file = "logs/profile.log";
    mask = date & time & tick;
    level = 1;
    separator = ";";
  };
  alarm_info = {
    file = "logs/alarm_info.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 12;
  };
  alarm_trace = {
    file = "logs/alarm_trace.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  alarm_cdr = {
    file = "logs/cdr/alarm_cdr.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 4;
  };
  config = {
    file = "logs/config.log";
    mask = file & date & time & tick & pid;
    level = 1;
    period = hour;
    tee = trace;
  };
  warning = {
    file = "logs/warning.log";
    mask = date & time & tick & file;
    level = 1;
    tee = trace;
  };
  info = {
    file = "logs/info.log";
    mask = date & time & tick & file;
    level = 10;
    tee = trace;
  };
  si = {
    file = "logs/si_trace.log";
    mask = date & time & tick & pid & file;
    level = 10;
    tee = trace;
  };
  SigFW_cdr_map = {
    file = "cdr/map/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SigFW_cdr_cap = {
    file = "cdr/cap/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SigFW_tcap_invalid = {
    file = "cdr/tcap_ivalid/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SS7FW_DIAM_cdr = {
    file = "cdr/cdr_diam/cdr_diam-%Y%m%d-%H%M.log";
    mask = date & time & tick;
    separator = "     ";
    period = day;
    level = 1;
  };
  SS7FW_ATI_cdr = {
    file = "cdr/cdr_ati/cdr_ati-%Y%m%d-%H%M.log";
    mask = date & time & tick;
    separator = ";";
    period = day;
    level = 1;
  };
  SS7FW_udp_cdr = {
    file = "logs/cdr_udp.log";
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  SS7FW_udp_in_cdr = {
    file = "logs/cdr_udp_in.log";
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  gtp_c_cdr = {
    file = "cdr/gtp_c/GTP_C_%H%M_%d%m%Y.cdr";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 1;
  };
  gtp_c_invalid_cdr = {
    file = "cdr/gtp_c_invalid/GTP_C_Invalid_%H%M_%d%m%Y.cdr";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 1;
  };
  gtp_mi_cdr = {
    file="cdr/gtp_mi/GTP_MI_%H%M_%d%m%Y.cdr";
    period=hour;
    mask="date & time & tick";
    separator=";";
    level=1;
  };
}
```