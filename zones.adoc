= Локальные стенды
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:lang: en
:experimental:
:table-caption: Таблица

.Адреса локальных зон
|===

|Продукт |Тип |Логин |Пароль |IP |Еще

|ADC
|Web
|support
|elephant
|192.168.126.79:8080/adc.admin/
|192.168.126.79:8080/apple_settings/apple_settings_page.html for Apple devices

|Billing OCS
|Server
|support
|elephant
|192.168.126.21
|

|Call Me
|Server
|support
|elephant
|192.168.126.186
|

|CPE.PRBT
|Server
|root
|elephant
|192.168.125.148
|

|CPE.SB
|Server
|support
|elephant
|192.168.110.116
|

|CPE.SB
|Server
|root
|elephant
|192.168.126.153
|

|data_proc
|Server
|root
|elephant
|192.168.126.153
|

|DIAMETER.BS
|Server
|support
|elephant
|192.168.110.116
|

|DPDP.Core
|Server
|root
|elephant
|192.168.125.148
|

|DPDP.Web
|Server
|root
|elephant
|192.168.125.148
|

|DPDP.Web
|Web
|admin
|sql
|172.30.137.234:10018
|

|DPDP.Web
|Web
|dpdp_admin
|dpdp_admin
|192.168.125.148:8081
|

|DPDP.Web
|Web
|viktor_admin
|sql
|192.168.125.148:8081
|

|DPDP.Web
|Web
|admin
|sql
|192.168.109.246:8081
|

|DRA
|Server
|support
|elephant
|192.168.126.186
|

|DRA
|Server
|support
|elephant
|192.168.126.3
|

|EIR
|Server
|support
|elephant
|192.168.110.189
|

|GGSN
|Server
|support
|elephant
|192.168.126.245
|

|GGSN
|Server
|support
|elephant
|192.168.125.26
|

|Git
|Server
|tarasov-a
|
|192.168.0.131
|

|GLR
|API
|support
|elephant
|192.168.126.3
|support/elephant

|GMSC
|Server
|support
|elephant
|192.168.126.3
|

|HLR
|API
|support
|elephant
|192.168.110.140
|support/elephant

|HLR
|API
|support
|elephant
|192.168.126.153
|support/elephant

|HLR.Core
|Server
|support
|elephant
|192.168.110.140
|

|HLR.Core
|Server
|support
|elephant
|192.168.126.3
|

|HSS
|API
|support
|elephant
|192.168.126.3
|support/elephant

|HSS
|Server
|support
|elephant
|192.168.126.3
|

|I-CSCF
|Server
|support
|elephant
|149.255.118.70
|

|IMS TAS
|Server
|support
|elephant
|149.255.118.70
|

|IP-SM-GW
|Server
|support
|elephant
|192.168.126.186
|

|ITG
|CLI
|support
|elephant
|192.168.99.181
|

|ITG
|Server
|support
|elephant
|192.168.126.3
|

|MEDIA_SERVER
|Server
|root
|elephant
|192.168.125.148
|

|MIG.TOOL
|Server
|root
|elephant
|192.168.125.148
|

|MKD
|Web
|secureAdmin
|elephant
|192.168.99.101
|

|MKD
|Web
|secureAdmin
|elephant
|192.168.99.54:8240
|

|MKD
|Web
|secureAdmin
|elephant
|192.168.99.115
|

|MME
|Server
|support
|elephant
|192.168.126.67
|

|MultiIMSI
|API
|support
|elephant
|192.168.126.3
|support/elephant

|MultiIMSI
|Database
|support
|elephant
|192.168.126.153
|mimsi/sql

|MV RADIUS
|Server
|support
|elephant
|192.168.126.3
|

|OTA
|Server
|support
|elephant
|192.168.126.3
|

|OTA
|Server
|support
|elephant
|192.168.126.186
|

|P-CSCF
|Server
|support
|elephant
|149.255.118.70
|

|PGW
|Server
|support
|elephant
|192.168.126.245
|

|PGW
|Server
|support
|elephant
|192.168.125.26
|

|RBC
|Server
|support
|elephant
|192.168.126.186
|

|RBT.Core
|Server
|root
|elephant
|192.168.125.148
|

|RBT.Web
|Web
|support
|elephant
|192.168.125.148:8080/rbt
|

|RBT.Web
|Web
|
|
|192.168.109.246:8080/rbt/
|DPDP Web


|RBT.Web
|Server
|root
|elephant
|192.168.125.148
|

|RDS
|Server
|support
|elephant
|192.168.126.3
|

|RG
|Server
|support
|elephant
|192.168.126.3
|

|RG
|Server
|support
|elephant
|192.168.126.186
|

|RG
|Server
|support
|elephant
|192.168.126.240
|

|RG
|Web
|
|
|192.168.126.241:8080/rg/auth.jsp
|

|S-CSCF
|Server
|support
|elephant
|149.255.118.70
|

|SB.Core
|Server
|support
|elephant
|192.168.110.189
|

|SB.Core
|Server
|root
|elephant
|192.168.126.153
|

|SB.Core
|Server
|support
|elephant
|192.168.110.116
|

|SB.Core
|Server
|support
|elephant
|192.168.126.3
|

|SB.Web
|Web
|support
|elephant
|192.168.126.37:8080/sb
|

|SB.Web
|Web
|support
|elephant
|192.168.126.7:8080/SB_safari
|

|SBC
|Server
|support
|elephant
|149.255.118.70
|

|SC_Lite
|Server
|support
|elephant
|192.168.110.116
|

|SC_Lite
|Server
|support
|elephant
|192.168.126.3
|

|SC_Lite
|Server
|support
|elephant
|192.168.126.186
|

|SC_Lite GIS
|Server
|support
|elephant
|192.168.126.186
|

|SCP
|Server
|root
|elephant
|192.168.126.153
|

|SCP
|Server
|support
|elephant
|192.168.126.3
|

|SCP
|Server
|support
|elephant
|192.168.126.186
|

|Sender XVLR
|Server
|support
|elephant
|192.168.110.116
|

|SigMonitor
|Server
|support
|elephant
|192.168.110.116
|

|SigMonitor
|Server
|root
|elephant
|192.168.126.153
|

|SigtranTester SG
|Server
|root
|elephant
|192.168.126.153
|

|SG
|Server
|root
|elephant
|192.168.126.153
|

|SG.DNS
|Server
|root
|elephant
|192.168.126.153
|

|SG.GTP
|Server
|root
|elephant
|192.168.126.153
|

|SGW
|Server
|support
|elephant
|192.168.126.245
|

|SGW
|Server
|support
|elephant
|192.168.126.236
|

|SigFW
|Server
|support
|2waeK31R
|192.168.109.250/ss7fw
|

|SigFW
|Web
|Writer
|Writer1!
|192.168.109.250/ss7fw
|

|SMSC
|Server
|support
|elephant
|192.168.126.186
|

|SMPP.BS
|Server
|support
|elephant
|192.168.126.186
|

|SMPP PROXY
|Server
|support
|elephant
|192.168.110.116
|

|SMPP PROXY
|Server
|root
|elephant
|192.168.125.148
|

|SMPP PROXY
|Server
|support
|elephant
|192.168.126.186
|

|SMSFW
|Web
|support
|elephant
|192.168.44.116/smsfw
|

|SMSFW
|Server
|support
|elephant
|192.168.126.186
|

|SN2
|Server
|support
|elephant
|192.168.110.116
|

|SN2
|Web
|root_demo
|Ckjyb[f
|192.168.110.234:8080/SN2
|

|SN2
|Web
|admin_demo
|Ckjyb[f
|192.168.110.234:8080/SN2
|

|SN2
|Web
|admin_writer
|elephant
|192.168.110.234:8080/SN2
|

|SN2
|Web
|dealer_writer
|elephant
|192.168.110.234:8080/SN2
|

|SN2
|Web
|user_writer
|elephant
|192.168.110.234:8080/SN2
|

|SN2
|Web
|support
|elephant
|192.168.99.115
|

|SPL_SIP
|Server
|root
|elephant
|192.168.126.153
|

|SPL_SIP
|Server
|root
|elephant
|192.168.125.148
|

|SRF
|Server
|support
|elephant
|192.168.126.3
|

|SS7FW
|Web
|support
|elephant
|192.168.77.54
|

|SigFW
|Web
|support
|elephant
|192.168.110.19:8080/sigfw
|

|SS7FW
|Server
|support
|elephant
|192.168.110.19
|

|SS7FW
|Server
|support
|elephant
|192.168.126.3
|

|SSW4
|Server
|support
|elephant
|149.255.118.70
|

|SSW5
|Server
|support
|elephant
|149.255.118.70
|

|SSW5
|PPS
|SecureAdmin
|elephant
|192.168.90.240:8080
|

|STP
|Server
|support
|elephant
|192.168.126.3
|

|STP
|Server
|support
|elephant
|192.168.126.186
|

|SVN
|Server
|tarasov-a
|
|192.168.100.156
|

|TRACER
|Server
|root
|elephant
|192.168.126.153
|

|UDM
|Server
|support
|elephant
|192.168.126.3
|

|UDSF
|Server
|support
|elephant
|192.168.126.245
|

|UDSF
|Server
|support
|elephant
|192.168.125.26
|

|VO
|Web
|admin
|&rgZ(I3d
|172.16.1.28:8080
|

|VO
|Web
|admin
|&rgZ(I3d
|192.168.108.253:8080/vo.rtc
|

|VO
|Web
|admin
|&rgZ(I3d
|192.168.108.208:10022
|

|XML_Gate
|API
|support
|elephant
|192.168.126.21
|admin/sql

|ЕЦОВ
|Web
|1001
|1001
|192.168.126.158/GTOm/login.jsp
|

|===
