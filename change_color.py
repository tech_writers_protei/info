# -*- coding: utf-8 -*-
from pathlib import Path
from re import findall


class Image:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._content: str | None = None
        self._replace: dict[str, set[str]] = dict()

    def read(self):
        with open(self._path, "rb") as fb:
            _ = fb.read()
        self._content = _.decode()

    def write(self, name: str | None = None):
        path: Path = self._path.with_stem(name) if name is not None else self._path

        with open(path, "wb") as fb:
            fb.write(bytes(self))

    def __bytes__(self):
        return self._content.encode("utf-8") if self._content is not None else b''

    def find_attribute(self, attribute: str):
        if attribute not in self._replace:
            self._replace[attribute] = set()

        line: str
        for line in findall(rf"{attribute}:.+?;", self._content):
            if line.removesuffix(";").split(":")[-1] != "none":
                self._replace[attribute].add(line)

    def set_attribute(self, color: str):
        color: str = color.removeprefix("#")
        # if attribute not in self._replace or self._replace.get(attribute):
        #     print(f"No attribute {attribute}")
        #     return

        for k, values in self._replace.items():
            for v in values:
                self._content = self._content.replace(v, f"{k}:#{color};")

    def change_color(self, color: str):
        self.find_attribute("fill")
        self.find_attribute("stroke")
        self.set_attribute(color)


if __name__ == '__main__':
    _color: str = "5b6770"
    _path: Path = Path("/Users/andrewtarasov/Desktop/Все/SVG/")
    # for file in _path.iterdir():
    #     image = Image(file)
    #     image.read()
    #     image.change_color("5b6770")
    #     image.write(f"{file.stem}_grey")
    for file in _path.iterdir():
        if file.stem.endswith("_grey"):
            file.replace(file.resolve().parent.parent.joinpath("svg_grey").joinpath(file.name))