---
title: "gtp.cfg"
description: "Параметры логики GTP"
weight: 20
type: docs
---

The file sets the settings for working with GTP protocols.

## Sections used ##

* **[\[General\]](#general-gtp)** -- general parameters for working with GTP;
* **[\[SeqNumberValidation\]](#seq-number-validation)** -- transaction verification parameters based on the `Sequence Number` value;
* **[\[GTP_C_Limits\]](#gtp-c-limits)** -- restriction parameters for the GTP-C protocol;
* **[\[GtpSession\]](#gtp-session-gtp)** -- storage parameters for GTP sessions.

### Description of the parameters ###

| Parameter                                                       | Description                                                                                                                                                                                                                                                                                 | Type        | O/M | P/R | Version   |
|-----------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|-----|-----|-----------|
| **<a name="general-gtp">\[General\]</a>**                       | General parameters for working with GTP.                                                                                                                                                                                                                                                    | object      | O   | P   | 1.0.14.10 |
| Mode                                                            | The mode of operation with the network.`DPDK` – forwarding of packets from network interfaces;`UPF` – receiving and sending GTP-C packets via UPF.Default: DPDK.                                                                                                                            | string      | O   | P   | 1.1.0.29  |
| SeqNumberValidationEnabled                                      | The flag for enabling verification of responses by ordinal number, `Sequence Number`, at the transaction level. Default: 0.                                                                                                                                                                 | bool        | O   | R   | 1.0.14.10 |
| CheckWhiteListIpMasks                                           | The flag for restricting the processing of messages only from the direction in the [Directions](#directions) list for GTP-C/GTP-U.                                                                                                                                                          | bool        | O   | R   | 1.1.0.29  |
| gtpCSessionValidationEnable                                     | The flag for checking the compliance of GTP-C messages with established sessions. By default: 0.                                                                                                                                                                                            | bool        | O   | R   | 1.1.2.0   |
| gtpSessionSave                                                  | The flag for saving GTP sessions in the database. Default: 0.                                                                                                                                                                                                                               | bool        | O   | R   | 1.1.2.0   |
| **<a name="seq-number-validation">\[SeqNumberValidation\]</a>** | Parameters of the transaction verification algorithm by sequence number, `Sequence Number`.                                                                                                                                                                                                 | object      | O   | P   | 1.0.14.10 |
| <a name="transaction-expire-time">TransactionExpireTime</a>     | Transaction storage time, in milliseconds. Default: 30,000.                                                                                                                                                                                                                                 | int         | O   | R   | 1.0.14.10 |
| <a name="transaction-store-size">TransactionStoreSize</a>       | The period for viewing a table with transactions for deleting records, with a storage time longer than [TransactionExpireTime](#transaction-expire-time), in milliseconds. Default: 3,000.                                                                                                  | int         | O   | R   | 1.0.14.10 |
| TransactionStoreSize                                            | The size of the table for storing transactions. Default: 10,000,000.                                                                                                                                                                                                                        | int         | O   | P   | 1.0.14.10 |
| **<a name="gtp-c-limits">\[GTP_C_Limits\]</a>**                 | Parameters for limiting the bandwidth of the GTP-C interface.                                                                                                                                                                                                                               | object      | O   | R   | 1.1.0.29  |
| <a name="max-queue-size">MaxQueueSize</a>                       | The maximum size of the queue of primitives. Default: 0, the limit is not used.**Note.** Upon reaching, the emergency mode is activated: further actions with GTP-C packets are determined only by the value of the TransmitOverloadPackets field, without checking according to the rules. | int         | O   | R   | 1.1.0.29  |
| NormalQueueSize                                                 | The threshold of the queue of primitives for disabling emergency mode and processing GTP-C packets according to the rules.Default: 0.8 * [MaxQueueSize](#max-queue-size).                                                                                                                   | int         | O   | R   | 1.1.0.29  |
| <a name="transmit-overload-packets">TransmitOverloadPackets</a> | The flag for transmitting GTP-C packets when the emergency mode is activated or there are no free logics. Default: 1.                                                                                                                                                                       | bool        | O   | R   | 1.1.0.29  |
| DefaultMaxPackets                                               | The maximum allowed number of GTP-C packets per second that do not fall under any of the directions specified in the [Directions](#directions) field. Default: 0, the limit is not used.                                                                                                    | int         | O   | R   | 1.1.0.29  |
| MinLength                                                       | The minimum size of the GTP-C packet. Default: 0, the limit is not used.                                                                                                                                                                                                                    | int         | O   | R   | 1.1.0.29  |
| MaxLength                                                       | The maximum size of the GTP-C packet. Default: 0, the limit is not used.                                                                                                                                                                                                                    | int         | O   | R   | 1.1.0.29  |
| <a name="directions">Directions</a>                             | Parameters for bandwidth restrictions by direction.                                                                                                                                                                                                                                         | [object]    | O   | R   | 1.1.0.29  |
| IpMasks                                                         | A list of IP address masks.                                                                                                                                                                                                                                                                 | [ip/string] | M   | R   | 1.1.0.29  |
| MaxPackets                                                      | The maximum allowed number of GTP-C packets per second if the source address, `SrcIP`, falls under one of the masks. By default: 0                                                                                                                                                          | int         | O   | R   | 1.1.0.29  |
| **<a name="gtp-session-gtp">\[GtpSession\]</a>**                | Parameters for saving GTP sessions.                                                                                                                                                                                                                                                         | object      | O   | P   | 1.1.2.0   |
| SessionStorageSize                                              | The size of the table for storing sessions. Default: 1,048,576.                                                                                                                                                                                                                             | int         | O   | P   | 1.1.2.0   |
| SessionExpireTime                                               | Session lifetime, in seconds. By default: 86,400. **Note.** If 0, then the lifetime is not limited.                                                                                                                                                                                         | int         | O   | R   | 1.1.2.0   |
| SessionCleanupInterval                                          | The interval for clearing tables from outdated sessions, in seconds. Default: 60.                                                                                                                                                                                                           | int         | O   | R   | 1.1.2.0   |
| SessionDenyTTL                                                  | Lifetime of cached unsuccessful responses from Redis for GTP session requests, in seconds. Default: 3,600.                                                                                                                                                                                  | int         | O   | R   | 1.1.2.0   |

**Note.** The size of the table for storing transactions, [TransactionStoreSize](#transaction-store-size)
is calculated using the formula:

`2 * tps * TransactionExpireTime / 1000`

* tps -- number of transactions per second.

#### Sample ####

```ini
[General]
Mode = DPDK
SeqNumberValidationEnabled = 0;
CheckWhiteListIpMasks = 1
gtpCSessionValidationEnable = 1;
gtpSessionSave = 1;

[SeqNumberValidation]
TransactionExpireTime = 30000;
TransactionCleanPeriod = 3000;
TransactionStoreSize = 10000000;

[GTP_C_Limits]
MaxQueueSize = 50000
NormalQueueSize = 40000
TransmitOverloadPackets = 1
DefaultMaxPackets = 10000
Directions = {
  {
    IpMasks = { 10.10.1.0/24; e80:a0a4::/32 }
    MaxPackets = 5000
  }
  {
    IpMasks = { 10.10.2.0/24; fe80:a885::/32 }
    MaxPackets = 7000
  }
}

[GtpSession]
SessionStorageSize = 1048576;
SessionExpireTime = 86400;
SessionCleanupInterval = 60;
SessionDenyTTL = 3600;
```
