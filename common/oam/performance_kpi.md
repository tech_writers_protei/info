---
title: "KPI"
description: "Описание показателей производительности"
weight: 60
type: docs
---

Для узла Protei_MME отслеживаются следующие ключевые показатели:

* Количество зарегистрированных абонентов:

SAU = [realTimeAttachedUsersAtEcmIdleStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmIdleStatus) + 
[realTimeAttachedUsersAtEcmConnectedStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmConnectedStatus)

* Количество bearer-служб

Number of bearers = [realTimeAttachedUsersAtEcmIdleStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmIdleStatus) + 
[realTimeAttachedUsersAtEcmConnectedStatus](../../logging/stat/MME_users/#realTimeAttachedUsersAtEcmConnectedStatus) + 
[realTimePdnConnectionNumber](../../logging/stat/MME_users/#realTimePdnConnectionNumber)

* Количество выделенных bearer-служб

Number of dedicated bearers = [realTimeDedicatedBearerNumber](../../logging/stat/MME_users/#realTimeDedicatedBearerNumber)

* Доля успешных процедур Attach

Attach SR = ([attachSuccess](../../logging/stat/MME_s1_Attach/#attachSuccess) + 
[combinedAttachSuccess](../../logging/stat/MME_s1_Attach/#combinedAttachSuccess) + 
[emergencyAttachSuccess](../../logging/stat/MME_s1_Attach/#emergencyAttachSuccess)) * 100% / 
([attachRequest](../../logging/stat/MME_s1_Attach/#attachRequest) + 
[combinedAttachRequest](../../logging/stat/MME_s1_Attach/#combinedAttachRequest) + 
[emergencyAttachRequest](../../logging/stat/MME_s1_Attach/#emergencyAttachRequest))

* Доля успешных процедур аутентификации и шифрования

Authentication SR = ([securityModeCommandComplete](../../logging/stat/MME_s1_Security/#securityModeCommandComplete) * 100%) / 
[authenticationRequest](../../logging/stat/MME_s1_Security/#authenticationRequest)

* Доля успешных запросов TRACKING AREA UPDATE

TAU SR = ([intraTauSuccess](../../logging/stat/MME_tau/#intraTauSuccess) + 
[interTauSuccess](../../logging/stat/MME_tau/#interTauSuccess)) * 100% / 
([intraTauRequest](../../logging/stat/MME_tau/#intraTauRequest) + [interTauRequest](../../logging/stat/MME_tau/#interTauRequest))

TAU SR = ([intraTauSuccess](../../logging/stat/MME_tau/#intraTauSuccess) + 
[periodTauSuccess](../../logging/stat/MME_tau/#periodTauSuccess) + 
[intraCombinedTauSuccess](../../logging/stat/MME_tau/#intraCombinedTauSuccess) + 
[interCombinedTauSuccess](../../logging/stat/MME_tau/#interCombinedTauSuccess) + 
[interTauSuccess](../../logging/stat/MME_tau/#interTauSuccess)) * 100% / 
([intraTauRequest](../../logging/stat/MME_tau/#intraTauRequest) + 
[periodTauRequest](../../logging/stat/MME_tau/#periodTauRequest) + 
[intraCombinedTauRequest](../../logging/stat/MME_tau/#intraCombinedTauRequest) + 
[interCombinedTauRequest](../../logging/stat/MME_tau/#interCombinedTauRequest) + 
[interTauRequest](../../logging/stat/MME_tau/#interTauRequest))

* Доля успешных процедур Paging

Paging SR = [s1PagingSuccess](../../logging/stat/MME_paging/#s1PagingSuccess) * 100% / 
[s1PagingAttempt](../../logging/stat/MME_paging/#s1PagingAttempt)

* Доля успешных запросов SGsAP-LOCATION-UPDATE

SGsAP Location Update SR = [sGsApLocationUpdateAccept](../../logging/stat/MME_sgs_Interface/#sGsApLocationUpdateAccept) * 100% / 
[sGsApLocationUpdateRequest](../../logging/stat/MME_sgs_Interface/#sGsApLocationUpdateRequest)

* Доля успешных процедур SRVCC

SRVCC SR = [srvccPsToCsCompleteAcknowledge](../../logging/stat/MME_sv_Interface/#srvccPsToCsCompleteAcknowledge) * 100% / 
[srvccPsToCsRequest](../../logging/stat/MME_sv_Interface/#srvccPsToCsRequest)

* Доля успешных процедур MO CS Fallback

CSFB MO SR = (([csfbMoUeContextModificationResponse](../../logging/stat/MME_s1_Service/#csfbMoUeContextModificationResponse) + 
[csfbMoInitialContextSetupResponse](../../logging/stat/MME_s1_Service/#csfbMoInitialContextSetupResponse)) * 100% / 
([ExtendedServiceRequestMoCsfb](../../logging/stat/MME_s1_Service/#ExtendedServiceRequestMoCsfb) + 
[ExtendedServiceRequestMoCsfbE](../../logging/stat/MME_s1_Service/#ExtendedServiceRequestMoCsfbE))

* Доля успешных процедур MT CS Fallback

CSFB MT SR = ([csfbMtUeContextModificationResponse](../../logging/stat/MME_s1_Service/#csfbMtInitialContextSetupResponse) + 
[csfbMtInitialContextSetupResponse](../../logging/stat/MME_s1_Service/#csfbMtInitialContextSetupResponse)) * 100% / 
[ExtendedServiceRequestMtCsfb](../../logging/stat/MME_s1_Service/#ExtendedServiceRequestMtCsfb)

* Доля успешных межсетевых хэндоверов

Inter-Node Handover from LTE to UMTS SR = [interNodeHandoverSuccessFromLteToUmts](../../logging/stat/MME_handover/#interNodeHandoverSuccessFromLteToUmts) * 100% / 
[interNodeHandoverRequestFromLteToUmts](../../logging/stat/MME_handover/#interNodeHandoverRequestFromLteToUmts)

Inter-Node Handover from UMTS to LTE SR = [interNodeHandoverSuccessFromUmtsToLte](../../logging/stat/MME_handover/#interNodeHandoverSuccessFromUmtsToLte) * 100% / 
[interNodeHandoverRequestFromUmtsToLte](../../logging/stat/MME_handover/#interNodeHandoverRequestFromUmtsToLte)

* Доля успешных хэндоверов S1

Intra-Node S1 Handover SR = ([intraS1BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#intraS1BasedHandoverSuccessSgwNotChange) + 
[intraS1BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#intraS1BasedHandoverSuccessSgwChange)) * 100% / 
([intraS1BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#intraS1BasedHandoverRequestSgwNotChange) + 
[intraS1BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#intraS1BasedHandoverRequestSgwChange))

Intra-Node X2 Handover SR = ([intraX2BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#intraX2BasedHandoverSuccessSgwNotChange) + 
[intraX2BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#intraX2BasedHandoverSuccessSgwChange)) * 100% / 
([intraX2BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#intraX2BasedHandoverRequestSgwNotChange) + 
[intraX2BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#intraX2BasedHandoverRequestSgwChange))

Inter-Node S1 Handover SR = ([interS1BasedHandoverSuccessSgwNotChange](../../logging/stat/MME_handover/#interS1BasedHandoverSuccessSgwNotChange) + 
[interS1BasedHandoverSuccessSgwChange](../../logging/stat/MME_handover/#interS1BasedHandoverSuccessSgwChange)) / 
([interS1BasedHandoverRequestSgwNotChange](../../logging/stat/MME_handover/#interS1BasedHandoverRequestSgwNotChange) + 
[interS1BasedHandoverRequestSgwChange](../../logging/stat/MME_handover/#interS1BasedHandoverRequestSgwChange)) * 100%

* Доля успешных запросов GTP: Create Bearer и GTP: Create Session

Сreate Bearer SR = [createBearerResponse](../../logging/stat/MME_s11_Interface/#createBearerResponse) * 100% / 
[createBearerRequest](../../logging/stat/MME_s11_Interface/#createBearerRequest)

Сreate Session SR = [createSessionResponse](../../logging/stat/MME_s11_Interface/#createSessionResponse) * 100% / 
[createSessionRequest](../../logging/stat/MME_s11_Interface/#createSessionRequest)

* Доля успешных запросов GTP: Bearer Modification

Bearer Modification SR = [modifyBearerResponse](../../logging/stat/MME_s11_Interface/#modifyBearerResponse) * 100% / 
[modifyBearerRequest](../../logging/stat/MME_s11_Interface/#modifyBearerRequest)

* Доля успешных запросов GTP: Dedicated Bearer Creation

Dedicated Bearer Creation SR = [dedicatedBearerActiveSuccess](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveSuccess) * 100% / 
[dedicatedBearerActiveRequest](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveRequest)

* Доля успешных запросов GTP: Dedicated Bearer Modification

Dedicated Bearer Modification SR = [ueInitBearerResModRequest](../../logging/stat/MME_s1_Bearer_Modification/#ueInitBearerResModRequest) * 100% / 
[dedicatedBearerActiveRequest](../../logging/stat/MME_s1_Bearer_Activation/#dedicatedBearerActiveRequest)

* Доля использованных мощностей CPU

Average utilization = [averageCpuUtilization](../../logging/stat/MME_resource/#averageCpuUtilization)

Maximum utilization = [maxCpuUtilization](../../logging/stat/MME_resource/#maxCpuUtilization)
