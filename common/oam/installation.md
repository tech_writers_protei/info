---
title: "Установка"
description: "Инструкция по развертыванию узла PROTEI MME"
weight: 10
type: docs
---

### Установка программного обеспечения

1. Cкопировать на целевой сервер дистрибутив Protei_MME.
2. Создать локальную виртуальную машину от лица суперпользователя.

   ```bash
   $ su -
   $ pvcreate /dev/sdb
   $ vgcreate protei /dev/sdb
   ```

3. Разметить дисковое пространство.

   ```bash
   $ lvcreate --name protei_app -l 5%FREE protei
   $ lvcreate --name protei_cdr -l 45%FREE protei
   $ lvcreate --name protei_log -l 100%FREE protei
   ```

4. Создать файловую систему.

   ```bash
   $ mkfs.xfs /dev/mapper/protei-app
   $ mkfs.xfs /dev/mapper/protei-log
   $ mkfs.xfs /dev/mapper/protei-cdr
   ```

5. Создать директории.

   ```bash
   $ mkdir -p /usr/protei/
   $ mkdir -p /usr/protei/log
   $ mkdir -p /usr/protei/cd
   ```

6. Открыть файл `/etc/fstab/` и добавить строки.

   ```bash
   $ /dev/mapper/protei-app /usr/protei xfs defaults 0 0
   $ /dev/mapper/protei-log /usr/protei/log xfs defaults 0 0
   $ /dev/mapper/protei-cdr /usr/protei/cdr xfs defaults 0 0
   ```

7. Применить изменения из файла `fstab` к текущей системе.

   ```bash
   $ mount -a
   ```

8. Обеспечить доступ к репозиторию для разрешения зависимостей.
9. Установить необходимые библиотеки.

   ```bash
   $ apt-get install build-essential cmake gcc gdb git g++ iputils-ping libboost-all-dev libmariadb-dev libpcap-dev libsctp-dev libssl-dev libz-dev make pkg-config python3 rsync ssh ta
   ```

10. Перенести архив `Protei_MME.<version>.tgz` в директорию `/usr/protei/Protei_MME/bin/`.

    ```bash
    $ mv Protei_MME.<version>.tgz /usr/protei/Protei_MME/bin/
    ```

11. Распаковать архив на обоих узлах Protei_MME.

    ```bash
    $ tar -xvzf Protei_MME.<version>.tgz
    ```

12. Запустить приложение и проверить статус.

    ```bash
    $ systemctl start mme
    $ systemctl status mme
    ```