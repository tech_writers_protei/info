---
title: "HTTP API"
description: "Описание методов HTTP API"
weight: 30
type: docs
---

## Общая информация HTTP API ##

HTTP API используется для получения информации:

- о состоянии GTP-/SGs-соединений;
- о состоянии базовых станций;
- о текущем статусе абонентов;
- о состоянии баз данных.

Все запросы обрабатываются в синхронном режиме.
Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Запросы передаются по протоколу HTTP.

Приложение должно использовать один адрес URL для отправки запросов:

```curl
http://host:port/mme/v1/command?arg=value&...&arg=value
```

| Поле    | Описание                                 | Тип       | O/M |
|---------|------------------------------------------|-----------|-----|
| host    | IP-адрес или DNS-имя PROTEI MME API.     | ip/string | M   |
| port    | Номер порта для установления соединения. | int       | M   |
| command | Имя выполняемой команды.                 | string    | M   |
| arg     | Параметр команды.                        | string    | O   |
| value   | Значение параметра.                      | Any       | O   |

Все запросы осуществляются с помощью метода **HTTP GET**.

## Ответ от PROTEI MME ##

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON и `Content-Type: application/json`.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```text
200 OK
```

**Примечание.** Именно такой ответ передает система, если не указано иное.

## Команды ##

* [clear_dns_cache](#clear_dns_cache-api) - очистить cache адресов DNS;
* [deact_bearer](#deact_bearer_api) - деактивировать bearer-службу;
* [detach](#detach-api) - удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr-api) - удалить регистрации абонентов на определенном VLR и запретить новые;
* [disconnect_enb](#disconnect_enb) - разорвать соединение с eNodeB;
* [enable_vlr](#enable_vlr-api) - разрешить регистрации на определенном VLR;
* [get_db_status](#get_db_status-api) - получить информацию о состоянии базы данных;
* [get_gtp_peers](#get_gtp_peers) - получить информацию о состоянии GTP-соединений;
* [get_location](#get_location-api) - получить местоположение абонента;
* [get_metrics](#get_metrics-api) - получить текущие значения метрик;
* [get_profile](#get_profile-api) - получить информацию об абоненте;
* [get_s1_peers](#get_s1_peers-api) - получить информацию о состоянии базовых станций LTE из хранилища MME;
* [get_sgs_peers](#get_sgs_peers) - получить информацию о состоянии SGs-соединений;
* [reset_metrics](#reset_metrics-api) - сбросить текущие значения метрик;
* [ue_disable_trace](#ue_disable_trace) - деактивировать трассировку на eNodeB для абонента;
* [ue_enable_trace](#ue_enable_trace-api) - активировать трассировку на eNodeB для абонента.

### Очистить cache адресов DNS, clear_dns_cache {#clear_dns_cache-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/clear_dns_cache
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/clear_dns_cache
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Деактивировать bearer-службу, deact_bearer {#deact_bearer_api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/deact_bearer&imsi=<imsi>&bearer_id=<id>
```

#### Поля запроса ####

| Поле                              | Описание                                    | Тип    | O/M |
|-----------------------------------|---------------------------------------------|--------|-----|
| imsi                              | Номер IMSI абонента.                        | string | M   |
| <a name="bearer_id">bearer_id</a> | Идентификатор деактивируемой bearer-службы. | int    | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/deact_bearer&imsi=25048123456789&bearer_id=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No bearer_id` - некорректный запрос: не задано значение **[bearer_id](#bearer_id)**;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to deact bearer` - услуга недоступна ввиду невозможности деактивировать bearer-службу.

### Удалить регистрацию абонента из сети, detach {#detach-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach?imsi=<imsi>[&reattach=<bool>][&purge=<bool>]
```

#### Поля запроса ####

| Поле     | Описание                                                | Тип    | O/M |
|----------|---------------------------------------------------------|--------|-----|
| imsi     | Номер IMSI абонента.                                    | string | M   |
| reattach | Флаг переподключения к сети. По умолчанию: 0.           | bool   | O   |
| purge    | Флаг полного удаления профиля из сети. По умолчанию: 1. | bool   | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach?imsi=25048123456789&reattach=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to detach UE` - услуга недоступна ввиду невозможности удаления регистрации.

### Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr {#detach_by_vlr-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach_by_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле                              | Описание           | Тип | O/M |
|-----------------------------------|--------------------|-----|-----|
| <a name="ip_detach_by_vlr">ip</a> | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_detach_by_vlr)**;

### Разорвать соединение с eNodeB, disconnect_enb {#disconnect_enb}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/disconnect_enb&ip=<ip>
```

#### Поля запроса ####

| Поле                            | Описание         | Тип | O/M |
|---------------------------------|------------------|-----|-----|
| <a name="ip_disable_vlr">ip</a> | IP-адрес eNodeB. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/disconnect_enb&ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_disable_vlr)**;
* `404 Not Found` - указанная eNodeB не найдена;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Разрешить регистрации на определенном VLR, enable_vlr {#enable_vlr-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/enable_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле                           | Описание           | Тип | O/M |
|--------------------------------|--------------------|-----|-----|
| <a name="ip_enable_vlr">ip</a> | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **[ip](#ip_enable_vlr)**.

### Получить информацию о состоянии базы данных, get_db_status {#get_db_status-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_db_status
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_db_status
```

#### Поля ответа ####

| Поле                               | Описание                                                                    | Тип        | O/M |
|------------------------------------|-----------------------------------------------------------------------------|------------|-----|
| Writer                             | Параметры соединения, добавляющего записи в базу данных.                    | object     | M   |
| **{**                              |                                                                             |            |     |
| &nbsp;&nbsp;State                  | Состояние соединения.<br>`connected` / `disconnected` / `busy` / `unknown`. | string     | M   |
| &nbsp;&nbsp;Last job               | Параметры последнего изменения.                                             | object     | M   |
| &nbsp;&nbsp;**{**                  |                                                                             |            |     |
| &nbsp;&nbsp;&nbsp;&nbsp;Done       | Флаг завершения операции.                                                   | bool       | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;Rows saved | Количество записанных строк.                                                | int        | M   |
| &nbsp;&nbsp;&nbsp;&nbsp;Timestamp  | Временная метка завершения операции. Формат:<br>`YYYY-MM-DD hh:mm:ss`       | datetime   | M   |
| &nbsp;&nbsp;**}**                  |                                                                             |            |     |
| **}**                              |                                                                             |            |     |
| Readers                            | Параметры соединений, считывающих записи из базы данных.                    | \[object\] | M   |
| **{**                              |                                                                             |            |     |
| &nbsp;&nbsp;State                  | Состояние соединения.<br>`connected` / `disconnected` / `busy` / `unknown`. | string     | M   |
| **}**                              |                                                                             |            |     |

#### Пример ответа ####

```json5
{
  "Writer": {
    "State": "connected",
    "Last job": {
      "Done": true,
      "Rows saved": 0,
      "Timestamp": "2023-01-01 01:23:45"
    }
  },
  "Readers": [
    {
      "State": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить местоположение абонента, get_location {#get_location-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_location?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса ####

```
http://localhost:65535/mme/v1/get_location&imsi=25048123456789
```

#### Поля ответа ####

| Поле                        | Описание                    | Тип    | O/M |
|-----------------------------|-----------------------------|--------|-----|
| [EPS_State](#eps_state-api) | Код состояния абонента EPS. | int    | M   |
| PLMN                        | Идентификатор сети PLMN.    | string | M   |
| TAC                         | Код области отслеживания.   | string | M   |
| CellID                      | Идентификатор соты.         | hex    | M   |
| ENB-ID                      | Идентификатор eNodeB.       | int    | M   |


#### Значения EPS State {#eps_state-api}

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* `0` - Detached;
* `1` - AttachedNotReachableForPaging;
* `2` - AttachedReachableForPaging;
* `3` - ConnectedNotReachableForPaging;
* `4` - ConnectedReachableForPaging;
* `5` - NotProvidedFromSGSN.

#### Пример ответа ####

```json5
{
  "EPS_State": 0,
  "PLMN": "25002",
  "TAC": "0",
  "CellID": 0x10,
  "ENB-ID": 15000
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить текущие значения метрик, get_metrics {#get_metrics-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_metrics
```

#### Пример ответа ####

```console
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 13352  100 13352    0     0   362k      0 --:--:-- --:--:-- --:--:--  372k
```

```json5
{
  "diameterBase": {
    "capabilitiesExchangeAnswer": {
      "": 63
    },
    "capabilitiesExchangeAnswer3010": {
      "": 63
    },
    "capabilitiesExchangeRequest": {
      "": 63
    },
    "deviceWatchdogAnswer": {
      "": 111
    },
    "deviceWatchdogAnswer2001": {
      "": 111
    },
    "deviceWatchdogRequest": {
      "": 111
    },
    "disconnectPeerAnswer": {
      "": 0
    },
    "disconnectPeerRequest": {
      "": 0
    }
  },
  "handover": {
    "interNodeHandoverRequestFromLteToUmts": 0,
    "interNodeHandoverRequestFromUmtsToLte": 0,
    "interNodeHandoverSuccessFromLteToUmts": 0,
    "interNodeHandoverSuccessFromUmtsToLte": 0,
    "interS1BasedHandoverRequestSgwChange": 0,
    "interS1BasedHandoverRequestSgwNotChange": 0,
    "interS1BasedHandoverSuccessSgwChange": 0,
    "interS1BasedHandoverSuccessSgwNotChange": 0,
    "intraS1BasedHandoverRequestSgwChange": 0,
    "intraS1BasedHandoverRequestSgwNotChange": 0,
    "intraS1BasedHandoverSuccessSgwChange": 0,
    "intraS1BasedHandoverSuccessSgwNotChange": 0,
    "intraX2BasedHandoverRequestSgwChange": 0,
    "intraX2BasedHandoverRequestSgwNotChange": 0,
    "intraX2BasedHandoverSuccessSgwChange": 0,
    "intraX2BasedHandoverSuccessSgwNotChange": 0
  },
  "paging": {
    "s1PagingRequest": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingRequestCsfb": {
      "": 0
    },
    "s1PagingRequestCsfbLastEnodeb": {
      "": 0
    },
    "s1PagingRequestCsfbLastTa": {
      "": 0
    },
    "s1PagingRequestLastEnodebPS": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingRequestLastTaPS": {
      "": 0
    },
    "s1PagingRequestPS": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingSuccess": {
      "": 21,
      "TAI:001010001": 21
    },
    "s1PagingSuccessCsfb": {
      "": 0
    },
    "s1PagingSuccessPS": {
      "": 21,
      "TAI:001010001": 21
    }
  },
  "resource": {
    "averageCpuUtilization": 1.4252042165557826,
    "maxCpuUtilization": 2.1956087824351296
  },
  "s11Interface": {
    "bearerResourceCommand": 0,
    "bearerResourceFailureIndication": 0,
    "createBearerRequest": 2,
    "createBearerResponse": 2,
    "createBearerResponse16": 2,
    "createIndirectDataForwardingTunnelRequest": 0,
    "createIndirectDataForwardingTunnelResponse": 0,
    "createSessionRequest": 9,
    "createSessionResponse": 9,
    "createSessionResponse16": 9,
    "deleteBearerCommand": 0,
    "deleteBearerFailureIndication": 0,
    "deleteBearerRequest": 1,
    "deleteBearerResponse": 1,
    "deleteBearerResponse16": 1,
    "deleteIndirectDataForwardingTunnelRequest": 0,
    "deleteIndirectDataForwardingTunnelResponse": 0,
    "deleteSessionRequest": 9,
    "deleteSessionResponse": 9,
    "deleteSessionResponse16": 9,
    "downlinkDataNotification": 42,
    "downlinkDataNotificationAck": 42,
    "downlinkDataNotificationAck16": 42,
    "downlinkDataNotificationFailureInd": 0,
    "modifyBearerCommand": 0,
    "modifyBearerFailureIndication": 0,
    "modifyBearerRequest": 121,
    "modifyBearerResponse": 121,
    "modifyBearerResponse16": 121,
    "releaseAccessBearersRequest": 58,
    "releaseAccessBearersResponse": 58,
    "releaseAccessBearersResponse16": 58,
    "suspendAcknowledge": 0,
    "suspendNotification": 0,
    "updateBearerRequest": 6,
    "updateBearerResponse": 6,
    "updateBearerResponse16": 3,
    "updateBearerResponse94": 3
  },
  "s13Interface": {
    "meIdentityCheckAnswer": {
      "": 0
    },
    "meIdentityCheckRequest": {
      "": 0
    }
  },
  "s1Attach": {
    "attachFail": 0,
    "attachRequest": {
      "": 0
    },
    "attachSuccess": {
      "": 0
    },
    "combinedAttachFail": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "combinedAttachFail12": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "combinedAttachRequest": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "combinedAttachSuccess": {
      "": 5,
      "IMSIPLMN:00101": 5,
      "TAI:001010001": 5
    },
    "emergencyAttachFail": 0,
    "emergencyAttachRequest": {
      "": 0
    },
    "emergencyAttachSuccess": {
      "": 0
    }
  },
  "s1BearerActivation": {
    "activateDefaultEpsBearerContextAccept": {
      "": 9,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 9,
      "TAI:001010001": 9
    },
    "activateDefaultEpsBearerContextReject": 0,
    "activateDefaultEpsBearerContextRequest": {
      "": 9,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 9,
      "TAI:001010001": 9
    },
    "activateDefaultEpsBearerContextRequestIpv4Only": {
      "": 4,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "IMSIPLMN:00101": 4,
      "TAI:001010001": 4
    },
    "dedicatedBearerActiveReject": 0,
    "dedicatedBearerActiveRequest": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "dedicatedBearerActiveRequestQci1": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "dedicatedBearerActiveSuccess": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "pdnConnectivityReject": 0,
    "pdnConnectivityRequest": {
      "": 10,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 10,
      "TAI:001010001": 10
    }
  },
  "s1BearerDeactivation": {
    "deactivateEpsBearerContextAccept": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "deactivateEpsBearerContextRequest": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "dedicatedBearerDeactivationRequest": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "dedicatedBearerDeactivationRequest36": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "defaultBearerDeactivationRequest": {
      "": 0
    },
    "pdnDisconnectReject": 0,
    "pdnDisconnectRequest": {
      "": 0
    },
    "pdnDisconnectSuccess": {
      "": 0
    }
  },
  "s1BearerModification": {
    "hssInitBearerModRequest": {
      "": 0
    },
    "hssInitBearerModSuccess": {
      "": 0
    },
    "modifyEpsBearerContextReject": 0,
    "pgwInitBearerModRequest": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "pgwInitBearerModSuccess": {
      "": 3,
      "IMSIPLMN:00101": 3,
      "TAI:001010001": 3
    },
    "ueInitBearerResModReject": 0,
    "ueInitBearerResModRequest": {
      "": 0
    }
  },
  "s1Detach": {
    "detachRequest": {
      "": 0
    },
    "detachRequestCombined": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "detachRequestImsi": {
      "": 0
    },
    "detachRequestMmeInit": {
      "": 0
    },
    "detachSuccess": {
      "": 0
    },
    "detachSuccessCombined": {
      "": 0
    },
    "detachSuccessImsi": {
      "": 0
    },
    "detachSuccessMmeInit": {
      "": 0
    }
  },
  "s1Interface": {
    "eNbConfigurationUpdateRequest": {
      "": 0
    },
    "eNbConfigurationUpdateSuccess": {
      "": 0
    },
    "eNodeBInitS1ResetRequest": {
      "": 3,
      "TAI:001010051": 3,
      "TAI:466920051": 3
    },
    "eNodeBInitS1ResetSuccess": {
      "": 3,
      "TAI:001010051": 3,
      "TAI:466920051": 3
    },
    "eRabModificationConfirm": {
      "": 0
    },
    "eRabModificationIndication": {
      "": 0
    },
    "eRabModifyRequest": {
      "": 5,
      "TAI:001010001": 5
    },
    "eRabModifyResponse": {
      "": 5,
      "TAI:001010001": 5
    },
    "eRabReleaseCommand": {
      "": 1,
      "TAI:001010001": 1
    },
    "eRabReleaseResponse": {
      "": 1,
      "TAI:001010001": 1
    },
    "eRabSetupRequest": {
      "": 6,
      "TAI:001010001": 6
    },
    "eRabSetupResponse": {
      "": 6,
      "TAI:001010001": 6
    },
    "initialContextSetupFailure": {
      "": 0
    },
    "initialContextSetupRequest": {
      "": 62,
      "TAI:001010001": 62
    },
    "initialContextSetupResponse": {
      "": 62,
      "TAI:001010001": 62
    },
    "mmeConfigurationUpdateRequest": {
      "": 0
    },
    "mmeConfigurationUpdateSuccess": {
      "": 0
    },
    "numberOfEnodeb": {
      "": 4,
      "TAI:001010001": 3,
      "TAI:001010006": 2,
      "TAI:001010051": 1,
      "TAI:250510001": 1,
      "TAI:466920051": 1,
      "TAI:999990001": 2,
      "TAI:999990006": 1
    },
    "s1SetupRequest": {
      "": 2,
      "TAI:001010051": 1,
      "TAI:208930001": 1
    },
    "s1SetupSuccess": {
      "": 1,
      "TAI:001010051": 1
    },
    "ueContextModificationFailure": 0,
    "ueContextModificationRequest": {
      "": 0
    },
    "ueContextModificationResponse": {
      "": 0
    },
    "ueContextReleaseCommand": {
      "": 65,
      "TAI:001010001": 65
    },
    "ueContextReleaseCommand0_20": {
      "": 58,
      "TAI:001010001": 58
    },
    "ueContextReleaseCommand2_2": {
      "": 7,
      "TAI:001010001": 7
    },
    "ueContextReleaseComplete": {
      "": 65,
      "TAI:001010001": 65
    },
    "ueContextReleaseRequest": {
      "": 58,
      "TAI:001010001": 58
    },
    "ueContextReleaseRequest0_20": {
      "": 58,
      "TAI:001010001": 58
    }
  },
  "s1Security": {
    "authenticationFailure": 1,
    "authenticationFailure21": 1,
    "authenticationReject": 0,
    "authenticationRequest": 2,
    "authenticationResponse": 1,
    "securityModeCommand": 5,
    "securityModeCommandComplete": 5,
    "securityModeCommandReject": 0
  },
  "s1Service": {
    "ExtendedServiceRequest": {
      "": 0
    },
    "ExtendedServiceSuccess": {
      "": 0
    },
    "ServiceRequest": {
      "": 57,
      "TAI:001010001": 57
    },
    "ServiceSuccess": {
      "": 57,
      "TAI:001010001": 57
    },
    "csfbMoInitialContextSetupResponse": {
      "": 0
    },
    "csfbMoUeContextModificationResponse": {
      "": 0
    }
  },
  "s6aInterface": {
    "authenticationInformationAnswer": {
      "": 2,
      "IMSIPLMN:00101": 2,
      "IMSIPLMN:25002": 0
    },
    "authenticationInformationAnswer2001": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "authenticationInformationAnswer5001e": {
      "": 0,
      "IMSIPLMN:25002": 0
    },
    "authenticationInformationRequest": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "cancelLocationAnswer": {
      "": 0,
      "IMSIPLMN:00101": 0
    },
    "cancelLocationAnswer2001": {
      "": 0,
      "IMSIPLMN:00101": 0
    },
    "cancelLocationRequest": {
      "": 0
    },
    "deleteSubscriberDataAnswer": {
      "": 0
    },
    "deleteSubscriberDataRequest": {
      "": 0
    },
    "insertSubscriberDataAnswer": {
      "": 0
    },
    "insertSubscriberDataRequest": {
      "": 0
    },
    "notifyAnswer": {
      "": 9,
      "IMSIPLMN:00101": 9
    },
    "notifyAnswer2001": {
      "": 6,
      "IMSIPLMN:00101": 6
    },
    "notifyAnswer5423e": {
      "": 3,
      "IMSIPLMN:00101": 3
    },
    "notifyRequest": {
      "": 9,
      "IMSIPLMN:00101": 9
    },
    "purgeUeAnswer": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "purgeUeAnswer2001": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "purgeUeRequest": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "resetAnswer": {
      "": 0
    },
    "resetRequest": {
      "": 0
    },
    "updateLocationAnswer": {
      "": 1,
      "IMSIPLMN:00101": 1
    },
    "updateLocationAnswer2001": {
      "": 1,
      "IMSIPLMN:00101": 1
    },
    "updateLocationRequest": {
      "": 1,
      "IMSIPLMN:00101": 1
    }
  },
  "sgsInterface": {
    "sGsApLocationUpdateAccept": {
      "": 5,
      "TAI:001010001": 5
    },
    "sGsApLocationUpdateRequest": {
      "": 5,
      "TAI:001010001": 5
    }
  },
  "svInterface": {
    "srvccPsToCsCompleteAcknowledge": 0,
    "srvccPsToCsCompleteNotification": 0,
    "srvccPsToCsRequest": 0,
    "srvccPsToCsResponse": 0
  },
  "tau": {
    "interCombinedTauReject": 0,
    "interCombinedTauRequest": {
      "": 0
    },
    "interCombinedTauSuccess": {
      "": 0
    },
    "interTauReject": 0,
    "interTauRequest": {
      "": 0
    },
    "interTauSuccess": {
      "": 0
    },
    "intraCombinedTauReject": 0,
    "intraCombinedTauRequest": {
      "": 0
    },
    "intraCombinedTauSuccess": {
      "": 0
    },
    "intraTauReject": 0,
    "intraTauRequest": {
      "": 0
    },
    "intraTauSuccess": {
      "": 0
    },
    "periodTauReject": 0,
    "periodTauRequest": {
      "": 0
    },
    "periodTauSuccess": {
      "": 0
    }
  },
  "users": {
    "realTimeAttachedUsersAtDeregStatus": {
      "": 10,
      "IMSIPLMN:00101": 2,
      "IMSIPLMN:20893": 4,
      "IMSIPLMN:99999": 3,
      "TAI:001010001": 2,
      "TAI:208930001": 4,
      "TAI:999990001": 4
    },
    "realTimeAttachedUsersAtEcmConnectedStatus": {
      "": 5,
      "IMSIPLMN:20893": 4,
      "IMSIPLMN:99999": 1,
      "TAI:208930001": 4,
      "TAI:999990001": 1
    },
    "realTimeAttachedUsersAtEcmIdleStatus": {
      "": 10,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "APN:internet.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 3,
      "IMSIPLMN:20893": 3,
      "IMSIPLMN:99999": 3,
      "TAI:001010001": 3,
      "TAI:208930001": 3,
      "TAI:999990001": 4
    },
    "realTimeDedicatedBearerNumber": {
      "": 1,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "realTimePdnConnectionNumber": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "APN:internet.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    }
  }
}
```

### Получить информацию об абоненте, get_profile {#get_profile-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_profile?[imsi=<imsi>&][guti=<guti>&][imeisv=<imeisv>&][select=<fields>]
```

#### Поля запроса ####

| Поле                        | Описание                                                                                                   | Тип    | O/M |
|-----------------------------|------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi                        | Номер IMSI абонента.                                                                                       | string | C   |
| guti                        | Глобальный уникальный временный идентификатор абонента.                                                    | string | C   |
| imeisv                      | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | C   |
| <a name="select">select</a> | Запрашиваемые поля, разделенные запятой.                                                                   | string | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_profile?imsi=25048123456789&select=guti,imeisv
```

#### Поля ответа ####

| Поле   | Описание                                                                                                   | Тип    | O/M |
|--------|------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi   | Номер IMSI абонента.                                                                                       | string | C   |
| guti   | Глобальный уникальный временный идентификатор абонента.                                                    | string | C   |
| imeisv | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | C   |

**Примечание.** Используемые параметры соответствуют значениям, указанным в запросе в поле [select](#select).

#### Пример ответа ####

```json5
{
  "imsi": "250020123456789",
  "guti": "2500200020111111111",
  "imeisv": "3520990012345612"
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено.

### Получить информацию о состоянии GTP-соединений, get_gtp_peers {#get_gtp_peers}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_gtp_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_gtp_peers
```

#### Поля ответа ####

| Поле           | Описание                                                                                                                                           | Тип        | O/M |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|
| gtpPeers       | Перечень соединений.                                                                                                                               | \[object\] | M   |
| ip             | IP-адрес соединения.                                                                                                                               | ip         | M   |
| version        | Версия протокола GTP.<br>`V1`/`V2`.                                                                                                                | string     | M   |
| restartCounter | Значение счетчика перезапусков. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf). | int        | M   |
| state          | Состояние соединения.<br>`connected`/`disconnected`/`busy`/`unknown`.                                                                              | string     | M   |

#### Пример ответа ####

```json5
{
  "gtpPeers": [
    {
      "ip": "127.0.0.1",
      "version":"V2",
      "restartCounter": 1,
      "state": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Получить информацию о состоянии базовых станций LTE из хранилища MME, get_s1_peers {#get_s1_peers-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_s1_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_s1_peers
```

#### Поля ответа ####

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | ip     | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа ####

```json5
[
  {
    "PLMN": "99999",
    "TAC": 1,
    "id": "00101-176849",
    "ip": "172.30.153.1",
    "name": "enb2b2d1",
    "state": true
  }
]
```

### Получить информацию о состоянии SGs-соединений, get_sgs_peers {#get_sgs_peers}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_sgs_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_sgs_peers
```

#### Поля ответа ####

| Поле     | Описание                                                                    | Тип        | O/M |
|----------|-----------------------------------------------------------------------------|------------|-----|
| sgsPeers | Перечень соединений.                                                        | \[object\] | M   |
| ip       | IP-адрес соединения.                                                        | ip         | M   |
| gt       | Глобальный заголовок соединения.                                            | string     | M   |
| state    | Состояние соединения.<br>`connected` / `disconnected` / `busy` / `unknown`. | string     | M   |
| blocked  | Флаг блокировки соединения.                                                 | bool       | M   |

#### Пример ответа ####

```json5
{
  "sgsPeers": [
    {
      "ip": "127.0.0.1",
      "gt": "250021234567890",
      "state": "connected",
      "blocked": false
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Сбросить текущие значения метрик, reset_metrics {#reset_metrics-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/reset_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/reset_metrics
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `412 Precondition Failed, Metrics disabled` - запрос не выполнен, поскольку сбор метрик не активирован;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата.

### Деактивировать трассировку на eNodeB для абонента, ue_disable_trace {#ue_disable_trace}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_disable_trace?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_disable_trace?imsi=25048123456789
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `406 Not Acceptable, No trace id data` - запрос не может быть выполнен ввиду отсутствия соответствующей логики;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging.

### Активировать трассировку на eNodeB для абонента, ue_enable_trace {#ue_enable_trace-api}

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_enable_trace?imsi=<imsi>&TCE_IP=<ip>
```

#### Поля запроса ####

| Поле                        | Описание                                                                                             | Тип    | O/M |
|-----------------------------|------------------------------------------------------------------------------------------------------|--------|-----|
| imsi                        | Номер IMSI абонента.                                                                                 | string | M   |
| <a name="tce_ip">TCE_IP</a> | IP-адрес узла сбора и хранения файлов трассировки, <abbr title="Trace Collection Entity">TCE</abbr>. | ip     | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_enable_trace?imsi=25048123456789&TCE_IP=127.0.0.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, UE already traced` - некорректный запрос: указанное устройство уже отслеживается;
* `400 Bad Request, Trace Collection Entity IP Address empty` - некорректный запрос: не задано значение **[TCE_IP](#tce_ip)**;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, No free trace ID` - услуга недоступна ввиду отсутствия свободных логик;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging.