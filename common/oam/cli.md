---
title: "Скрипты командной строки"
description: "Скрипты bash для методов HTTP API"
weight: 35
type: docs
---

В этом разделе в качестве примеров обращения к API используются bash-скрипты, которые находятся в директории
`/usr/protei/Protei_MME/scripts/` на серверах MME.

Описание скриптов и примеры применения можно получить в стандартном
выводе при запуске скрипта без указания входных параметров.

* [clear_dns_cache](#clear_dns_cache-cli) -- очистить cache адресов DNS;
* [deact_bearer](#deact_bearer_cli) -- деактивировать bearer-службу;
* [detach](#detach-cli) -- удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr-cli) -- удалить регистрации абонентов на определенном VLR и запретить новые;
* [diam_state](#diam_state) -- получить информацию о состоянии PCSM-узлов;
* [enable_vlr](#enable_vlr-cli) -- разрешить регистрации на определенном VLR;
* [enb_disconnect](#enb_disconnect) -- разорвать соединение с eNodeB;
* [get_bearers](#get_bearers) -- получить информацию о bearer-службах;
* [get_location](#get_location-cli) -- получить местоположение абонента;
* [get_metrics](#get_metrics-cli) -- получить текущие значения метрик;
* [get_profile](#get_profile-cli) -- получить информацию об абоненте;
* [gtp_peers](#gtp_peers) -- получить информацию о состоянию GTP-соединений;
* [s1_peers](#s1_peers) -- получить информацию о состоянии базовых станций LTE из хранилища MME;
* [s1_table](#s1_table) -- получить информацию о состоянии базовых станций LTE из хранилища MME в табличном виде;
* [sgs_peers](#sgs_peers) -- получить информацию о состоянии SGs-соединений;
* [trace_UE](#trace_ue) -- добавить/удалить трассировку на MME для абонента.

### Очистить cache адресов DNS, clear_dns_cache {#clear_dns_cache-cli}

#### Запрос

```bash
$ ./clear_dns_cache
```

#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Деактивировать bearer-службу, deact_bearer {#deact_bearer_cli}

#### Запрос

```bash
$ ./deact_bearer {imsi} {bearer_id}
```

#### Поля запроса ####

| Поле      | Описание                                    | Тип    | O/M |
|-----------|---------------------------------------------|--------|-----|
| imsi      | Номер IMSI абонента.                        | string | M   |
| bearer_id | Идентификатор деактивируемой bearer-службы. | int    | M   |

#### Пример запроса

```bash
$ ./deact_bearer 001010000000390 5
```

#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Удалить регистрацию абонента из сети, detach {#detach-cli}

**Примечание.** При выполнении команды со значениями по умолчанию:
`reattach = 0`, регистрация абонента удаляется без последующей перерегистрации;
`purge = 1`, направляется запрос в HSS для удаления данных о VLR в абонентском профиле.

При `reattach = 1` удаляется регистрация с последующей перерегистрацией;
при `purge = 0` запрос на HSS не направляется.

#### Запрос

```bash
$ ./detach {imsi} [{reattach}] [{purge}]
```

#### Поля запроса ####

| Поле     | Описание                                                   | Тип    | O/M |
|----------|------------------------------------------------------------|--------|-----|
| imsi     | Номер IMSI абонента.                                       | string | M   |
| reattach | Флаг переподключения к сети.<br>По умолчанию: 0.           | bool   | O   |
| purge    | Флаг полного удаления профиля из сети.<br>По умолчанию: 1. | bool   | O   |

#### Пример запроса

```bash
$ ./detach 001010000000390 1
```

#### Ответ

```console
<status>
```

#### Пример ответа

```console
Done
```

### Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr {#detach_by_vlr-cli}

#### Запрос ####

```bash
$ ./detach_by_vlr <ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | O/M |
|------|--------------------|-----|-----|
| ip   | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```bash
$ ./detach_by_vlr 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Получить информацию о состоянии всех Diameter PCSM, diam_state {#diam_state}

#### Запрос

```bash
$ ./diam_state
```

#### Ответ

```console
———-DIAM PCSM TABLE———- +
NAME             STATE
pcsm_name   pcsm_state
```

#### Поля ответа

| Поле  | Описание                                    | Тип    | O/M |
|-------|---------------------------------------------|--------|-----|
| name  | Имя узла Diameter PCSM.                     | string | M   |
| state | Состояние узла.<br>`"ACTIVATE"` / `"FAIL"`. | string | M   |

#### Пример ответа

```console
———-DIAM PCSM TABLE———- +
NAME             STATE
"Sg.DIAM.PCSM.1" "ACTIVATE"
"Sg.DIAM.PCSM.2" "ACTIVATE"
"Sg.DIAM.PCSM.3" "FAIL"
"Sg.DIAM.PCSM.4" "FAIL"
"Sg.DIAM.PCSM.5" "FAIL"
```

#### Примечание

В случае добавления Diameter-соединений необходимо в скрипте явно задать их количество:

```shell
PCSM_MIN_ID=1
PCSM_MAX_ID=6
```

| Поле        | Описание                                | Тип | O/M |
|-------------|-----------------------------------------|-----|-----|
| PCSM_MIN_ID | Наименьший идентификатор PCSM.          | int | M   |
| PCSM_MAX_ID | Максимальное возможное количество PCSM. | int | M   |

### Разрешить регистрации на определенном VLR, enable_vlr {#enable_vlr-cli}

#### Запрос ####

```bash
$ ./enable_vlr <ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | O/M |
|------|--------------------|-----|-----|
| ip   | IP-адрес узла VLR. | ip  | M   |

#### Пример запроса ####

```bash
$ ./detach_by_vlr 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Разорвать соединение с eNodeB, enb_disconnect {#enb_disconnect}

#### Запрос

```bash
$ ./enb_disconnect {ip}
```

#### Поля запроса

| Поле | Описание         | Тип | O/M |
|------|------------------|-----|-----|
| ip   | IP-адрес eNodeB. | ip  | M   |

#### Пример запроса

```bash
$ ./enb_disconnect 192.168.1.1
```

#### Ответ ####

```console
<status>
```

#### Пример ответа ####

```console
Done
```

### Получить информацию о bearer-службах, get_bearers {#get_bearers}

#### Запрос

```bash
$ ./get_bearers
```

#### Ответ

```console
  IMSI  ;  MSISDN  ;  GUTI  ;  ENB_IP  ;  CellId   ;  EMM  ;  ECM  ;  Subscriber_IP  ;
--------------------------------------------------------------------------------------
 <imsi> ; <msisdn> ; <guti> ; <enb_ip> ; <cell_id> ; <emm> ; <ecm> ; <subscriber_ip> ;
```

#### Поля ответа

| Поле          | Описание                                                                         | Тип    | O/M |
|---------------|----------------------------------------------------------------------------------|--------|-----|
| IMSI          | Номер IMSI абонента.                                                             | string | M   |
| MSISDN        | Номер MSISDN абонента.                                                           | string | M   |
| GUTI          | Глобальный уникальный временный идентификатор абонента.                          | string | M   |
| ENB\_IP       | IP-адрес eNodeB.                                                                 | ip     | M   |
| CellId        | Идентификатор соты.                                                              | string | M   |
| EMM           | Состояние EMM для UE абонента.<br>`0` - EMM-DEREGISTERED / `1` - EMM-REGISTERED. | int    | M   |
| ECM           | Состояние ECM для UE абонента.<br>`0` - ECM-IDLE / `1` - ECM-CONNECTED.          | int    | M   |
| Subscriber_IP | IP-адрес абонента.                                                               | ip     | O   |

#### Пример ответа

```console
      IMSI       ;    MSISDN     ;       GUTI        ;     ENB_IP      ;   CellId   ; EMM  ; ECM  ; Subscriber_IP ;
-------------------------------------------------------------------------------------------------------------------
 001010000000398 ;  76000000398  ;   99999;1;1;339   ;  172.30.153.1   ;  45273345  ;  0   ;  0   ;
```

### Получить местоположение абонента, get_location {#get_location-cli}

#### Запрос

```bash
$ ./get_location {imsi}
```

#### Поля запроса

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса

```bash
$ ./get_location 001010000000390
```

#### Ответ

```text
{
  "CellID": <cell_id>,
  "ENB-ID": <enodeb_id>,
  "EPS_State": <eps_state>,
  "PLMN": <plmn_id>,
  "TAC": <tac>
}
```

#### Поля ответа ####

| Поле                        | Описание                    | Тип    | O/M |
|-----------------------------|-----------------------------|--------|-----|
| CellID                      | Идентификатор соты.         | hex    | M   |
| ENB-ID                      | Идентификатор eNodeB.       | string | M   |
| [EPS_State](#eps_state-cli) | Код состояния абонента EPS. | int    | M   |
| PLMN                        | Идентификатор сети PLMN.    | string | M   |
| TAC                         | Код области отслеживания.   | int    | M   |


#### Значения EPS State {#eps_state-cli}

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* `0` - Detached;
* `1` - AttachedNotReachableForPaging;
* `2` - AttachedReachableForPaging;
* `3` - ConnectedNotReachableForPaging;
* `4` - ConnectedReachableForPaging;
* `5` - NotProvidedFromSGSN;

#### Пример ответа

```json5
{
  "CellID": 45273345,
  "ENB-ID": "00101-176849",
  "EPS_State": 4,
  "PLMN": "00101",
  "TAC": 1
}
```

### Получить текущие значения метрик, get_metrics {#get_metrics-cli}

#### Запрос ####

```bash
$ ./get_metrics
```

#### Пример ответа ####

```console
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 13352  100 13352    0     0   362k      0 --:--:-- --:--:-- --:--:--  372k
```

```json5
{
  "diameterBase": {
    "capabilitiesExchangeAnswer": {
      "": 63
    },
    "capabilitiesExchangeAnswer3010": {
      "": 63
    },
    "capabilitiesExchangeRequest": {
      "": 63
    },
    "deviceWatchdogAnswer": {
      "": 111
    },
    "deviceWatchdogAnswer2001": {
      "": 111
    },
    "deviceWatchdogRequest": {
      "": 111
    },
    "disconnectPeerAnswer": {
      "": 0
    },
    "disconnectPeerRequest": {
      "": 0
    }
  },
  "handover": {
    "interNodeHandoverRequestFromLteToUmts": 0,
    "interNodeHandoverRequestFromUmtsToLte": 0,
    "interNodeHandoverSuccessFromLteToUmts": 0,
    "interNodeHandoverSuccessFromUmtsToLte": 0,
    "interS1BasedHandoverRequestSgwChange": 0,
    "interS1BasedHandoverRequestSgwNotChange": 0,
    "interS1BasedHandoverSuccessSgwChange": 0,
    "interS1BasedHandoverSuccessSgwNotChange": 0,
    "intraS1BasedHandoverRequestSgwChange": 0,
    "intraS1BasedHandoverRequestSgwNotChange": 0,
    "intraS1BasedHandoverSuccessSgwChange": 0,
    "intraS1BasedHandoverSuccessSgwNotChange": 0,
    "intraX2BasedHandoverRequestSgwChange": 0,
    "intraX2BasedHandoverRequestSgwNotChange": 0,
    "intraX2BasedHandoverSuccessSgwChange": 0,
    "intraX2BasedHandoverSuccessSgwNotChange": 0
  },
  "paging": {
    "s1PagingRequest": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingRequestCsfb": {
      "": 0
    },
    "s1PagingRequestCsfbLastEnodeb": {
      "": 0
    },
    "s1PagingRequestCsfbLastTa": {
      "": 0
    },
    "s1PagingRequestLastEnodebPS": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingRequestLastTaPS": {
      "": 0
    },
    "s1PagingRequestPS": {
      "": 28,
      "TAI:001010001": 28
    },
    "s1PagingSuccess": {
      "": 21,
      "TAI:001010001": 21
    },
    "s1PagingSuccessCsfb": {
      "": 0
    },
    "s1PagingSuccessPS": {
      "": 21,
      "TAI:001010001": 21
    }
  },
  "resource": {
    "averageCpuUtilization": 1.4252042165557826,
    "maxCpuUtilization": 2.1956087824351296
  },
  "s11Interface": {
    "bearerResourceCommand": 0,
    "bearerResourceFailureIndication": 0,
    "createBearerRequest": 2,
    "createBearerResponse": 2,
    "createBearerResponse16": 2,
    "createIndirectDataForwardingTunnelRequest": 0,
    "createIndirectDataForwardingTunnelResponse": 0,
    "createSessionRequest": 9,
    "createSessionResponse": 9,
    "createSessionResponse16": 9,
    "deleteBearerCommand": 0,
    "deleteBearerFailureIndication": 0,
    "deleteBearerRequest": 1,
    "deleteBearerResponse": 1,
    "deleteBearerResponse16": 1,
    "deleteIndirectDataForwardingTunnelRequest": 0,
    "deleteIndirectDataForwardingTunnelResponse": 0,
    "deleteSessionRequest": 9,
    "deleteSessionResponse": 9,
    "deleteSessionResponse16": 9,
    "downlinkDataNotification": 42,
    "downlinkDataNotificationAck": 42,
    "downlinkDataNotificationAck16": 42,
    "downlinkDataNotificationFailureInd": 0,
    "modifyBearerCommand": 0,
    "modifyBearerFailureIndication": 0,
    "modifyBearerRequest": 121,
    "modifyBearerResponse": 121,
    "modifyBearerResponse16": 121,
    "releaseAccessBearersRequest": 58,
    "releaseAccessBearersResponse": 58,
    "releaseAccessBearersResponse16": 58,
    "suspendAcknowledge": 0,
    "suspendNotification": 0,
    "updateBearerRequest": 6,
    "updateBearerResponse": 6,
    "updateBearerResponse16": 3,
    "updateBearerResponse94": 3
  },
  "s13Interface": {
    "meIdentityCheckAnswer": {
      "": 0
    },
    "meIdentityCheckRequest": {
      "": 0
    }
  },
  "s1Attach": {
    "attachFail": 0,
    "attachRequest": {
      "": 0
    },
    "attachSuccess": {
      "": 0
    },
    "combinedAttachFail": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "combinedAttachFail12": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "combinedAttachRequest": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "combinedAttachSuccess": {
      "": 5,
      "IMSIPLMN:00101": 5,
      "TAI:001010001": 5
    },
    "emergencyAttachFail": 0,
    "emergencyAttachRequest": {
      "": 0
    },
    "emergencyAttachSuccess": {
      "": 0
    }
  },
  "s1BearerActivation": {
    "activateDefaultEpsBearerContextAccept": {
      "": 9,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 9,
      "TAI:001010001": 9
    },
    "activateDefaultEpsBearerContextReject": 0,
    "activateDefaultEpsBearerContextRequest": {
      "": 9,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 9,
      "TAI:001010001": 9
    },
    "activateDefaultEpsBearerContextRequestIpv4Only": {
      "": 4,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "IMSIPLMN:00101": 4,
      "TAI:001010001": 4
    },
    "dedicatedBearerActiveReject": 0,
    "dedicatedBearerActiveRequest": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "dedicatedBearerActiveRequestQci1": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "dedicatedBearerActiveSuccess": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 2,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    },
    "pdnConnectivityReject": 0,
    "pdnConnectivityRequest": {
      "": 10,
      "APN:ims.mnc001.mcc001.gprs": 4,
      "APN:internet.mnc001.mcc001.gprs": 4,
      "APN:todpi.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 10,
      "TAI:001010001": 10
    }
  },
  "s1BearerDeactivation": {
    "deactivateEpsBearerContextAccept": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "deactivateEpsBearerContextRequest": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "dedicatedBearerDeactivationRequest": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "dedicatedBearerDeactivationRequest36": {
      "": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "defaultBearerDeactivationRequest": {
      "": 0
    },
    "pdnDisconnectReject": 0,
    "pdnDisconnectRequest": {
      "": 0
    },
    "pdnDisconnectSuccess": {
      "": 0
    }
  },
  "s1BearerModification": {
    "hssInitBearerModRequest": {
      "": 0
    },
    "hssInitBearerModSuccess": {
      "": 0
    },
    "modifyEpsBearerContextReject": 0,
    "pgwInitBearerModRequest": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "pgwInitBearerModSuccess": {
      "": 3,
      "IMSIPLMN:00101": 3,
      "TAI:001010001": 3
    },
    "ueInitBearerResModReject": 0,
    "ueInitBearerResModRequest": {
      "": 0
    }
  },
  "s1Detach": {
    "detachRequest": {
      "": 0
    },
    "detachRequestCombined": {
      "": 6,
      "IMSIPLMN:00101": 6,
      "TAI:001010001": 6
    },
    "detachRequestImsi": {
      "": 0
    },
    "detachRequestMmeInit": {
      "": 0
    },
    "detachSuccess": {
      "": 0
    },
    "detachSuccessCombined": {
      "": 0
    },
    "detachSuccessImsi": {
      "": 0
    },
    "detachSuccessMmeInit": {
      "": 0
    }
  },
  "s1Interface": {
    "eNbConfigurationUpdateRequest": {
      "": 0
    },
    "eNbConfigurationUpdateSuccess": {
      "": 0
    },
    "eNodeBInitS1ResetRequest": {
      "": 3,
      "TAI:001010051": 3,
      "TAI:466920051": 3
    },
    "eNodeBInitS1ResetSuccess": {
      "": 3,
      "TAI:001010051": 3,
      "TAI:466920051": 3
    },
    "eRabModificationConfirm": {
      "": 0
    },
    "eRabModificationIndication": {
      "": 0
    },
    "eRabModifyRequest": {
      "": 5,
      "TAI:001010001": 5
    },
    "eRabModifyResponse": {
      "": 5,
      "TAI:001010001": 5
    },
    "eRabReleaseCommand": {
      "": 1,
      "TAI:001010001": 1
    },
    "eRabReleaseResponse": {
      "": 1,
      "TAI:001010001": 1
    },
    "eRabSetupRequest": {
      "": 6,
      "TAI:001010001": 6
    },
    "eRabSetupResponse": {
      "": 6,
      "TAI:001010001": 6
    },
    "initialContextSetupFailure": {
      "": 0
    },
    "initialContextSetupRequest": {
      "": 62,
      "TAI:001010001": 62
    },
    "initialContextSetupResponse": {
      "": 62,
      "TAI:001010001": 62
    },
    "mmeConfigurationUpdateRequest": {
      "": 0
    },
    "mmeConfigurationUpdateSuccess": {
      "": 0
    },
    "numberOfEnodeb": {
      "": 4,
      "TAI:001010001": 3,
      "TAI:001010006": 2,
      "TAI:001010051": 1,
      "TAI:250510001": 1,
      "TAI:466920051": 1,
      "TAI:999990001": 2,
      "TAI:999990006": 1
    },
    "s1SetupRequest": {
      "": 2,
      "TAI:001010051": 1,
      "TAI:208930001": 1
    },
    "s1SetupSuccess": {
      "": 1,
      "TAI:001010051": 1
    },
    "ueContextModificationFailure": 0,
    "ueContextModificationRequest": {
      "": 0
    },
    "ueContextModificationResponse": {
      "": 0
    },
    "ueContextReleaseCommand": {
      "": 65,
      "TAI:001010001": 65
    },
    "ueContextReleaseCommand0_20": {
      "": 58,
      "TAI:001010001": 58
    },
    "ueContextReleaseCommand2_2": {
      "": 7,
      "TAI:001010001": 7
    },
    "ueContextReleaseComplete": {
      "": 65,
      "TAI:001010001": 65
    },
    "ueContextReleaseRequest": {
      "": 58,
      "TAI:001010001": 58
    },
    "ueContextReleaseRequest0_20": {
      "": 58,
      "TAI:001010001": 58
    }
  },
  "s1Security": {
    "authenticationFailure": 1,
    "authenticationFailure21": 1,
    "authenticationReject": 0,
    "authenticationRequest": 2,
    "authenticationResponse": 1,
    "securityModeCommand": 5,
    "securityModeCommandComplete": 5,
    "securityModeCommandReject": 0
  },
  "s1Service": {
    "ExtendedServiceRequest": {
      "": 0
    },
    "ExtendedServiceSuccess": {
      "": 0
    },
    "ServiceRequest": {
      "": 57,
      "TAI:001010001": 57
    },
    "ServiceSuccess": {
      "": 57,
      "TAI:001010001": 57
    },
    "csfbMoInitialContextSetupResponse": {
      "": 0
    },
    "csfbMoUeContextModificationResponse": {
      "": 0
    }
  },
  "s6aInterface": {
    "authenticationInformationAnswer": {
      "": 2,
      "IMSIPLMN:00101": 2,
      "IMSIPLMN:25002": 0
    },
    "authenticationInformationAnswer2001": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "authenticationInformationAnswer5001e": {
      "": 0,
      "IMSIPLMN:25002": 0
    },
    "authenticationInformationRequest": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "cancelLocationAnswer": {
      "": 0,
      "IMSIPLMN:00101": 0
    },
    "cancelLocationAnswer2001": {
      "": 0,
      "IMSIPLMN:00101": 0
    },
    "cancelLocationRequest": {
      "": 0
    },
    "deleteSubscriberDataAnswer": {
      "": 0
    },
    "deleteSubscriberDataRequest": {
      "": 0
    },
    "insertSubscriberDataAnswer": {
      "": 0
    },
    "insertSubscriberDataRequest": {
      "": 0
    },
    "notifyAnswer": {
      "": 9,
      "IMSIPLMN:00101": 9
    },
    "notifyAnswer2001": {
      "": 6,
      "IMSIPLMN:00101": 6
    },
    "notifyAnswer5423e": {
      "": 3,
      "IMSIPLMN:00101": 3
    },
    "notifyRequest": {
      "": 9,
      "IMSIPLMN:00101": 9
    },
    "purgeUeAnswer": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "purgeUeAnswer2001": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "purgeUeRequest": {
      "": 2,
      "IMSIPLMN:00101": 2
    },
    "resetAnswer": {
      "": 0
    },
    "resetRequest": {
      "": 0
    },
    "updateLocationAnswer": {
      "": 1,
      "IMSIPLMN:00101": 1
    },
    "updateLocationAnswer2001": {
      "": 1,
      "IMSIPLMN:00101": 1
    },
    "updateLocationRequest": {
      "": 1,
      "IMSIPLMN:00101": 1
    }
  },
  "sgsInterface": {
    "sGsApLocationUpdateAccept": {
      "": 5,
      "TAI:001010001": 5
    },
    "sGsApLocationUpdateRequest": {
      "": 5,
      "TAI:001010001": 5
    }
  },
  "svInterface": {
    "srvccPsToCsCompleteAcknowledge": 0,
    "srvccPsToCsCompleteNotification": 0,
    "srvccPsToCsRequest": 0,
    "srvccPsToCsResponse": 0
  },
  "tau": {
    "interCombinedTauReject": 0,
    "interCombinedTauRequest": {
      "": 0
    },
    "interCombinedTauSuccess": {
      "": 0
    },
    "interTauReject": 0,
    "interTauRequest": {
      "": 0
    },
    "interTauSuccess": {
      "": 0
    },
    "intraCombinedTauReject": 0,
    "intraCombinedTauRequest": {
      "": 0
    },
    "intraCombinedTauSuccess": {
      "": 0
    },
    "intraTauReject": 0,
    "intraTauRequest": {
      "": 0
    },
    "intraTauSuccess": {
      "": 0
    },
    "periodTauReject": 0,
    "periodTauRequest": {
      "": 0
    },
    "periodTauSuccess": {
      "": 0
    }
  },
  "users": {
    "realTimeAttachedUsersAtDeregStatus": {
      "": 10,
      "IMSIPLMN:00101": 2,
      "IMSIPLMN:20893": 4,
      "IMSIPLMN:99999": 3,
      "TAI:001010001": 2,
      "TAI:208930001": 4,
      "TAI:999990001": 4
    },
    "realTimeAttachedUsersAtEcmConnectedStatus": {
      "": 5,
      "IMSIPLMN:20893": 4,
      "IMSIPLMN:99999": 1,
      "TAI:208930001": 4,
      "TAI:999990001": 1
    },
    "realTimeAttachedUsersAtEcmIdleStatus": {
      "": 10,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "APN:internet.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 3,
      "IMSIPLMN:20893": 3,
      "IMSIPLMN:99999": 3,
      "TAI:001010001": 3,
      "TAI:208930001": 3,
      "TAI:999990001": 4
    },
    "realTimeDedicatedBearerNumber": {
      "": 1,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 1,
      "TAI:001010001": 1
    },
    "realTimePdnConnectionNumber": {
      "": 2,
      "APN:ims.mnc001.mcc001.gprs": 1,
      "APN:internet.mnc001.mcc001.gprs": 1,
      "IMSIPLMN:00101": 2,
      "TAI:001010001": 2
    }
  }
}
```

### Получить информацию об абоненте, get_profile {#get_profile-cli}

#### Запрос

```bash
$ ./get_profile {imsi}
```

#### Поля запроса

| Поле | Описание             | Тип    | O/M |
|------|----------------------|--------|-----|
| imsi | Номер IMSI абонента. | string | M   |

#### Пример запроса

```bash
$ ./get_profile 001010000000398
```

#### Пример ответа

```json5
{
  "5G_EA": null,
  "5G_IA": null,
  "A-MSISDN": null,
  "ARD": 0,
  "AWoPDN": false,
  "ActivityDetected": null,
  "CP_CIoT_opt": true,
  "CellID": 917760,
  "DRX_param": null,
  "EEA": 240,
  "EIA": 112,
  "EMM_State": "DEREGISTERED",
  "ENB_IP": "172.30.37.58",
  "ENB_UE_ID": null,
  "EUTRAN_Trace_ID": null,
  "E_DRX_param": "eDRX value 2, Paging Time Window 0",
  "EmergencyAttach": false,
  "GEA": null,
  "GTP_C_TEID_SELF": null,
  "GUTI": "99999;1;1;339",
  "GUTI_LastReallocTimestamp": "2023-09-16_15:07:22",
  "GUTI_ReallocTrigger_count": 1,
  "HSS_Host": "hss.epc.mnc048.mcc250.3gppnetwork.org",
  "HSS_reset": null,
  "IMEISV": null,
  "IMSI": "001010000000398",
  "MME_UE_ID": null,
  "MSISDN": "76000000398",
  "MS_Classmark2": null,
  "MS_Classmark3": null,
  "NCC": null,
  "NEAF": null,
  "NH": null,
  "ODB": null,
  "PDN_Conn_restricted": false,
  "PDN_Manager": {
    "Barebone #1": {
      "Bearer": {
        "ARP": "A&R priority level 10, may trigger preemption, not preemptable",
        "QCI": 6,
        "id": "0",
        "type": "default"
      },
      "Connectivity": {
        "APN": "ims.mnc000.mcc101.gprs",
        "APN AMBR": {
          "DL": "100000000 b/sec",
          "UL": "100000000 b/sec"
        },
        "HSS Conconsole": "1",
        "PAA": "no IPv4, no IPv6",
        "PDN Type": "ipv4",
        "S11 UL F-TEID": "0:0.0.0.0-::"
      }
    }
  },
  "PLMN": "00101",
  "RFSP_id": null,
  "TAC": 1,
  "TCE_IP": null,
  "UEA": 0,
  "UE_radio_cap": null,
  "UE_radio_cap_for_paging": null,
  "UIA": 0,
  "dl_NAS_count": 3,
  "dl_UE_AMBR": 1024000000,
  "knasenc_type": 0,
  "knasint_type": 2,
  "ul_NAS_count": 0,
  "ul_UE_AMBR": 1024000000
}
```

### Получить информацию о состоянии GTP-соединений на MME, gtp_peers {#gtp_peers}

#### Запрос

```bash
$ ./gtp_peers
```

#### Ответ

```text
{
  "gtpPeers": {
    "<iface_name>": [
      {
        "ip": <ip>,
        "restartCounter": <restart_counter>,
        "state": <connection_state>,
        "version": <gtp_version>
      }
    ]
  },
  "selfRestartCounter": <node_restart_counter>
}
```

#### Поля ответа

| Поле               | Описание                                                                                                                                                | Тип        | O/M |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|
| gtpPeers           | Перечень соединений.                                                                                                                                    | \[object\] | M   |
| selfRestartCounter | Значение счетчика перезапусков узла. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf). | int        | M   |
| <iface_name>       | Перечень параметров соединений по интерфейсу.                                                                                                           | \[object\] | M   |
| ip                 | IP-адрес соединения.                                                                                                                                    | ip         | M   |
| restartCounter     | Значение счетчика перезапусков. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf).      | int        | M   |
| state              | Флаг активности соединения.                                                                                                                             | bool       | M   |
| version            | Версия протокола GTP.                                                                                                                                   | string     | M   |

#### Пример ответа

```json5
{
  "gtpPeers": {
    "S10": [
      {
        "ip": "192.168.125.182",
        "restartCounter": 5,
        "state": false,
        "version": 2
      }
    ],
    "S11": [
      {
        "ip": "192.168.126.11",
        "restartCounter": null,
        "state": true,
        "version": 2
      },
      {
        "ip": "192.168.125.95",
        "restartCounter": null,
        "state": true,
        "version": 2
      }
    ]
  },
  "selfRestartCounter": 36
}
```

### Получить информацию о состоянии базовых станций LTE на MME, s1_peers {#s1_peers}

#### Запрос

```bash
$ ./s1_peers [ip={ip}] [id={id}] [plmn={plmn}] [name={name}]
```

#### Поля запроса

| Поле | Описание                                       | Тип    | O/M |
|------|------------------------------------------------|--------|-----|
| ip   | IP-адрес базовой станции.                      | ip     | C   |
| id   | Идентификатор базовой станции.                 | string | C   |
| plmn | Код PLMN, которой принадлежит базовая станция. | string | C   |
| name | Имя базовой станции.                           | string | C   |

#### Пример запроса

```bash
$ ./s1_peers plmn 99999
```

#### Ответ

```text
[
  {
    "PLMN": <plmn_id>,
    "TAC": <tac>,
    "id": <enodeb_id>,
    "ip": <enodeb_ip>,
    "name": <enodeb_name>,
    "state": <enodeb_state>
  }
]
```

#### Поля ответа

| Поле  | Описание                  | Тип    | O/M |
|-------|---------------------------|--------|-----|
| PLMN  | Идентификатор сети PLMN.  | string | M   |
| TAC   | Код области отслеживания. | int    | M   |
| id    | Идентификатор eNodeB.     | string | M   |
| ip    | IP-адрес eNodeB.          | string | M   |
| name  | Имя eNodeB.               | string | M   |
| state | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```json5
{
  "PLMN": "99999",
  "TAC": 1,
  "id": "00101-176849",
  "ip": "172.30.153.1",
  "name": "enb2b2d1",
  "state": true
}
```

### Получить информацию о состоянии базовых станций LTE на MME в табличном виде, s1_table {#s1_table}

#### Запрос

```bash
$ ./s1_table
```

#### Ответ

```console
PLMN    TAC  eNB-ID    IP        Name        State
——————— ———— ————————— ————————— ——————————— ————————————
plmn_id tac  enodeb_id enodeb_ip enodeb_name enodeb_state
```

#### Поля ответа

| Поле   | Описание                  | Тип    | O/M |
|--------|---------------------------|--------|-----|
| PLMN   | Идентификатор сети PLMN.  | string | M   |
| TAC    | Код области отслеживания. | int    | M   |
| ENB-ID | Идентификатор eNodeB.     | string | M   |
| IP     | IP-адрес eNodeB.          | ip     | M   |
| Name   | Имя eNodeB.               | string | M   |
| State  | Флаг активности eNodeB.   | bool   | M   |

#### Пример ответа

```console
PLMN  TAC  eNB-ID       IP           Name     State
————— ———— ———————————— ———————————— ———————— —————
00101 1    00101-176849 172.30.153.1 enb2b2d1 false
99912 1    00101-176849 172.30.153.1 enb2b2d1 false
99999 1    00101-176849 172.30.153.1 enb2b2d1 false
```

### Получить информацию о состоянии SGs-соединений, sgs_peers {#sgs_peers}

#### Запрос

```bash
$ ./sgs_peers
```

#### Ответ

```console
% Total  % Received  % Xferd  Average   Speed    Time    Time     Time     Current
                              Dload    Upload    Total   Spent    Left     Speed
% total  %     rcvd  % dlvrd   avg_dl rate_ul  hh:mm:ss hh:mm:ss hh:mm:ss rate_now
{
  "sgsPeers": [
    {
      "blocked": <connection_state>,
      "gt": <global_title>,
      "ip": <ip>,
      "state": <activity_state>
    }
  ]
}
```

#### Поля ответа

| Поле     | Описание                    | Тип        | O/M |
|----------|-----------------------------|------------|-----|
| sgsPeers | Перечень соединений.        | \[object\] | M   |
| blocked  | Флаг блокировки соединения. | bool       | M   |
| gt       | Глобальный заголовок узла.  | string     | M   |
| ip       | IP-адрес соединения.        | ip         | M   |
| state    | Флаг активности соединения. | string     | M   |

#### Пример ответа

```console
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   127  100   127    0     0   3968      0 --:--:-- --:--:-- --:--:--  3968
{
  "sgsPeers": [
    {
      "blocked": false,
      "gt": "79216567568",
      "ip": "192.168.125.154",
      "state": true
    }
  ]
}
```

### Управление отслеживанием абонента, trace_UE {#trace_ue}

Файлы с данными отслеживания находятся в директории `/usr/protei/Protei_MME/logs/pcap_trace`.

Информация для указанного абонента хранится в файле `/usr/protei/log/Protei_MME/pcap_trace/{IMSI}_%Y-%m-%d_%H:%M:%s.pcap`.

#### Запрос на добавление отслеживания абонента

```bash
$ ./trace_UE {IMSI} add
$ ./trace_UE {MSISDN} add
```

#### Запрос на прекращение отслеживания абонента

```bash
$ ./trace_UE {IMSI} delete
$ ./trace_UE {MSISDN} delete
```

#### Поля ответа

| Поле   | Описание               | Тип    | O/M |
|--------|------------------------|--------|-----|
| IMSI   | Номер IMSI абонента.   | string | C   |
| MSISDN | Номер MSISDN абонента. | string | C   |

#### Ответ

```console
<status>
```

#### Пример запроса на добавление отслеживания абонента

```bash
$ ./trace_UE 001010000000311 add
```

#### Пример запроса на прекращение отслеживания абонента

```bash
$ ./trace_UE 001010000000311 delete
```
#### Пример ответа

```console
Done
```