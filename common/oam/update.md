---
title: "Обновление"
description: "Инструкция по обновлению узла Protei_MME"
weight: 40
type: docs
---

Исполняемый файл узла PROTEI MME хранится в директории `/usr/protei/Protei_MME/bin`.

Для установки новой версии программного обеспечения узла MME необходимо выполнить следующие действия:

1. Разместить архив `Protei_MME.<version>.tgz` с исполняемым файлом новой версии в папку
   `/usr/protei/Protei_MME/bin/`.

   ```bash
   $ cd /usr/protei/Protei_MME/bin/
   ```

2. Распаковать архив на обоих узлах MME.

   ```bash
   $ tar -xvzf Protei_MME.<version>.tgz
   ```
3. Удалить архив после распаковки.

   ```bash
   $ rm Protei_MME.<version>.tgz
   ```

4. Обновить указатель символьной ссылки на новый исполняемый файл.

   ```bash
   $ ln -si Protei_MME.<version>.r64 Protei_MME
   ```

5. Проверить наличие всех обязательных конфигурационных файлов.

   ```bash
   $ ll /usr/protei/Protei_MME/config
   ```

6. Перезапустить узел.

   ```bash
   $ systemctl restart mme
   ```

   или

   ```bash
   $ /usr/protei/Protei_MME/restart
   ```

7. Проверить состояние приложения.

   ```bash
   $ systemctl status mme
   ```

При необходимости, возврат к предыдущей версии программного обеспечения может быть выполнен по той же процедуре.
