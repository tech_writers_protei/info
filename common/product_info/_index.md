---
title: "История версий"
description: ""
weight: 50
type: docs
---
## 1.50.3.0.263 (2024-09-14)


 Коллизия TAU с Active Flag и DDN
- Basic **Bug**

- При отсутствии ответа на ICS Req будет выполняться сброс S1-контекста, явность/неявность которого будет определяться параметром releaseOnIcsFail


 Локальное понижение QoS, в случае повышения QoS со стороны PGW/PCRF
- Basic **UserStory**

- В qos_rules.json добавлено поле qciRemapping, в соответствии с которым будет производиться замена значений QCI


 Поддержка Masked IMEISV
- Basic **UserStory**

- Поле Masked IMEISV должно отправляться в сообщениях ICS Req и Handover Req если выставлен флаг sendMaskedImeisv в plmn.json



## 1.50.2.0.262 (2024-09-11)


 Релиз контекста во время S1 HO со сменой TAC
- Important **Bug**

- Подготовка к перехвату TAU Req выполняется на более раннем этапе S1 HO - сразу после отправки HO Command



## 1.50.1.0.261 (2024-09-09)


 Неуспешный X2 HO со сменой SGW
- Basic **Bug**

- Старый адрес PGW не теряется после X2 HO. Не меняется SGW при X2 HO в пределах TAC. Сброс сессий будет выполняться сразу при появлении TAU Req или другого прерывания, не дожидаясь истечения таймера. Поле E-RAB to be switched in UL list будет в Path Switch Req Ack.


 Не работает SRVCC rejection by MSC
- Important **Bug**

- Убрана отправка SRVCC PS to CS Complete Ack при неуспешном SRVCC PS to CS Complete Notification


 ISD MSISDN Update
- Basic **UserStory**

- При наличии нового MSISDN в ISD он будет сохраняться, после чего будет выполняться явный Detach с флагом Reattach required


 Поддержка работы с несколькими инстансами MariaDB
- Basic **UserStory**

- В параметре hostname mme.json теперь поддерживается массив хостов. Первый элемент массива считается первичным хостом, что означает, что ММЕ будет пытаться установить соединение в первую очередь именно с ним. При недоступности первичного, будут делаться попытки соединения с другими хостами, но в конечном итоге, ММЕ будет стремиться восстановить соединение с первичным.


 Некорректная работа распределения трафика по S1-ассоциациям при работе в 2 SCTP-потока
- Basic **Bug**

- Применение правильного количества SCTP-потоков


 Исправить E-RAB Release cause
- Basic **Bug**

- Для E-RAB Release cause в E-RABReleaseCommand во время процедуры PDN Disconnect будет использоваться значение NAS: Normal Release



## 1.50.0.0.261 (2024-08-29)


 Перенос конфигурации в JSON формат
- Important **Epic**

- ММЕ переведён на json-конфигурацию, поддержка cfg удалена



## 1.49.6.0.259 (2024-08-29)


 Коллизия Update Bearer и X2HO со сменой TAC
- Basic **Bug**

- TAU Req в Uplink NAS Transport будет прерывать процедуру Update Bearer с причиной Temporarily rejected due to handover/TAU/RAU procedure in progress


 S1 Handover отсутствует IE HandoverRestriction List и Handover EDR
- Basic **Bug**

- MME будет отправлять Handover Restriction List в HO Request, если он был настроен в параметре EquivalentPLMNs в served_plmn.cfg


 Повторный резолв APN без Extra_APN_label при неуспешном поиске PGW в DNS
- Important **UserStory**

- В случае неуспеха запроса в DNS адресов PGW с extra APN label будет делаться повторный запрос без extra APN label


 Relative MME Capacity для всех PLMN
- Basic **UserStory**

- В mme.cfg добавлен параметр RelativeMME_Capacity, применяемый в случае отсутствия RelativeMME_Capacity для конкретного PLMN в served_plmn.cfg


 Не работает SRVCC Cancellation
- Important **Bug**

- В ответ на HO Cancel будет отправляться S1 Handover Cancel Ack, а после получения SRVCC PS to CS Cancel Ack процедура будет завершаться с соответствующим кодом ошибки


 Распределение S1AP сообщений по SCTP-потокам
- Basic **Task**

- Распределение UE-associated S1AP сообщений по SCTP-потокам согласно настройкам компонента S1AP и возможностям базовой станции



## 1.49.5.0.257 (2024-08-06)


 Некорректное кодирование AVP ECGI в ISDA
- Critical **Bug**

- Исправлено кодирование ECGI в ISD Answer


 Не отправляется ULI В Create Bearer Response и Delete Bearer Response в сторону SPGW
- Critical **Bug**

- В GTPv2 Create Bearer Res и Delete Bearer Res будет присутствовать поле ULI



## 1.49.4.0.256 (2024-08-05)


 InitialContextSetupFailure после TAU с cause -2:9
- Important **Bug**

- Возвращён старый механизм обработки TAU Req при Paging, с продолжением Paging после завершения TAU без Active Flag



## 1.49.3.0.255 (2024-07-31)


 TAU All EPS Bearers Inactive
- Basic **Bug**

- После TAU Reject с причиной No EPS bearer context activated выполняется неявный Detach, при необходимости также отправляется SGsAP Detach Ind


 MME отправляет лишнее сообщение Eps Detach Indication в сторону MSC в сценарии с Eps Attach
- Basic **Bug**

- Убрана отправка лишнего EPS Detach Ind


 MME отправляет лишнее сообщение Eps Detach Indication в сторону MSC после SGSAPLocationUpdateReject
- Basic **Bug**

- Убрана отправка лишнего EPS Detach Ind



## 1.49.2.0.254 (2024-07-29)


 Поддержка PGW Restart Notification в GTPv2 Echo Request / Echo Response
- Basic **UserStory**

- В mme.cfg в секцию General добавлена поддержка флага PrnInEcho, который добавляет соответствующий флаг в GTP-C Echo Req и Res


 HandoverCancel не отменяет HandoverRequired
- Important **Bug**

- Handover Cancel отменяет Handover Required, находящиеся в очереди ожидания профиля абонента


 Service Request не проходит проверку целостности при поднятых трех PDN Connectivity
- Important **Bug**

- Исправлена обработка NAS-запросов из очереди ожидания профиля абонента, содержащих "устаревшие" NAS SeqN


 HO from UTRAN не найдено сообщение TAU-Request
- Basic **Bug**

- ММЕ сможет перехватывать TAU Req приходящие до Modify Bearer во время процедуры HO from UTRAN


 Переход в UTRAN Теряются PDP-Type и PDP-Addr после HO from UTRAN
- Basic **Bug**

- Исправлена обработка входящего Create Session Res при входящем HO


 TAU в ответ на PS Paging
- Important **Bug**

- Любой TAU Req, приходящий во время процедуры Paging вместо Service Req, будет обрабатываться таким образом, будто у него выставлен active flag


 ISD All-APN-Configurations-Included-Indicator
- Basic **Bug**

- ISD с All_APN_CONFIGURATIONS_INCLUDED приводит к выполнению Detach с флагом reAttach-required. При последующей регистрации абонента ММЕ использует информацию о PDN, полученную в ISD.


 ISD DIAMETER UNABLE TO COMPLY 5012
- Basic **Bug**

- При запросе местоположения абонента, находящегося в состоянии Idle, будет выполнена процедура Paging по последнему TAC, состоящая из одной попытки. В случае успеха будет возвращено текущее местоположение абонента, в случае неудачи - последнее известное.


 MME не удаляет inactive биреры, указанные в TrackingAreaUpdateRequest в EPS Bearer Context status
- Basic **Bug**

- MME сбрасывает все ранее известные биреры, не указанные в EPS Bearer Context status IE в TAU Req


 ММЕ не отправляет GTP-c TEID в modify bearer request во время входящего TAU со сменой MME без смены SGW
- Basic **Bug**

- MME отправляет GTP-C TEID в Modify Bearer Request во время входящего TAU со сменой MME без смены SGW



## 1.49.1.0.253 (2024-07-10)


 NOR по активации Default Bearer не включает PDN GW Identity
- Basic **Bug**

- Добавлено поле MIP6_Agent_Info в Notify сообщение - в зависимости от имеющихся данных, в него будет записан либо PGW_FQDN, либо IP


 Default LAC as TAC
- Basic **UserStory**

- В served_plmn.cfg добавлена поддержка параметра DefaultLACasTAC, который позволит считать LAC равным текущему TAC в случае отсутствия явного соответствия в перечне TAC = LAI


 Цикличный TAU с типом combined TA/LA updating
- Important **Bug**

- TAU Accept передаётся в сообщении ICS Req в случае выполнения процедуры Initial Context Setup


 Переработать сбор метрик по абонентам
- Basic **UserStory**

- Сбор абонентских метрик выполняется при добавлении, изменении и удалении абонентского профиля, что избавляет ММЕ от необходимости периодической блокировки хранилища профилей для сбора этих метрик


 Коллизия X2HO и SGs Paging
- Basic **Bug**

- Исправлена реакция ММЕ на появление NAS Non Delivery Ind с причиной 'X2 HO triggered' во время выполнения процедуры MT Call


 В сценарии TAU со сменой MME VOLTE в contextResponse отсутствует информация по биреру apn=ims
- Basic **Bug**

- В GTPv2 Context Res при процедуре TAU со сменой MME возвращаются все активные биреры


 Некорректная аутентификация во время TAU mapped
- Important **Bug**

- Сброс ключей шифрования в некоторых сценариях TAU Mapped



## 1.49.0.0.252 (2024-07-09)


 Не работает SRVCC для MOCN
- Important **UserStory**

- Дополнен конфиг rac_rules.cfg


 Утечка storage
- Basic **Bug**

- Запуск таймера UE Activity при переходе абонента в EMM Deregistered


 Оптимизация обработки S1AP сообщений
- Basic **UserStory**

- Доработаны механизмы разбора входящих S1AP сообщений, содержащих NAS


 В edr по процедуре SRVCC to GERAN некорректно заполняется поле Target CellID
- Basic **Bug**

- Исправлено заполнение поля Target CellID


 Некорректный cause в edr
- Basic **Bug**

- Cause в GTP-C CDR для Downlink Data должен быть равен тому, который был отправлен в DDN Ack


 Не считается ExtendedServiceSuccess для MO и MO Emergency
- Basic **Bug**

- Добавлен подсчет метрики ExtendedServiceSuccess после получения UE Context Modification Response для процедуры Extended Service Request


 MME не удалил сессию на SPGW после возвращения из CSFB
- Basic **Bug**

- Добавлен сброс сессий для случая, когда абон делает Attach по GUTI от другого MME, не являющегося одногруппником текущего


 Успешный reload некорректных правил
- Basic **Bug**

- Вывод сообщения об ошибке вида "rule with name 'name' was not found" добавлен в mme_config.log


 Во время csfb MT Call idle не считается ExtendedServiceRequest
- Basic **Bug**

- Добавлен подсчет метрик ExtendedServiceRequest и ExtendedServiceSuccess для CSFB MT Call в Idle mode


 Не анализируется InterfaceType передаваемый в F-TEID
- Low **UserStory**

- Главным признаком типа F-TEID в CreateSessionResponse считается поле interface type, а не instance id


 Юнит-тестирование выборки с весами и приоритетами
- Basic **Task**

- Добавлены юнит-тесты для Tm_WePr_IP_Vec в ММЕ и Tm_WePr_GT_Vec в компоненте SGsAP


 Таймер ожидания Attach после Detach с ReAttach Required
- Basic **UserStory**

- В served_plmn.cfg добавлен параметр ReattachTimeout для настройки соответствующего таймера


 MME игнорирует 'No Key is avaliable на UE' отправляя AuthReq с защитой целостности
- Basic **Bug**

- Auth Req отправляется в виде Plain NAS Message при аутентификации по KSI, отличном от последнего использованного


 Поддержка UE Context Modification Indication на стороне ММЕ
- Basic **UserStory**

- Добавлена отправка UE Context Modification Confirm в ответ на UE Context Modification Indication


 Собрать ММЕ под Астра 1.7.4
- Basic **Task**

- Добавлена сборка под Астру


 Некорректная Cause в Update Modification Response
- Basic **Bug**

- ММЕ корректно реагирует на S1AP Path Switch Req, приходящий во время процедуры bearer modification


 MME Balancer mode (S1AP). Контроль соединения с балансировщиком
- Basic **UserStory**

- В S1AP компоненту добавлен контроль количества соединений к балансировщику


 Неуспешный Attach после обнуления NAS Count
- Basic **Bug**

- Исправлена работа со счётчиками NAS Count в некоторых процедурах


 MME падает с core при конфигурировании в mme.cfg Ranges
- Basic **Bug**

- ММЕ не будет падать при некорректной настройке ranges


 MME не релизит радиобирер после неуспешной активации дедикатного бирера
- Important **Bug**

- ММЕ будет выполнять E-RAB Release для всех биреров, по которым пришли отказы от UE


 Коллизия Create Bearer и X2HO
- Basic **Bug**

- ММЕ корректно реагирует на S1AP Path Switch Req, приходящий во время процедуры bearer creation


 Некорректная работа алгоритмов NAS шифрования SNOW 3G и ZUC
- Basic **Bug**

- Алгоритмы SNOW-3G и ZUC получили поддержку многопоточности


 На MME в edr возникает цикл неуспешных PDN Disconnection req
- Basic **Bug**

- Поддерживается любой порядок прихода сообщений E-RAB Release Response и Up NAS Transport (Deact EPS bearer context accept)



## 1.48.14.0.16.fv (2024-07-05)


 Исходящий inter-MME S1 Handover
- Basic **UserStory**

- Исправлено кодирование TI в GTPv2 Fwd Relocation Req и Context Res



## 1.48.13.0.15.fv (2024-06-19)


 DownlinkNASTransport при неуспешном S1Release
- Important **Bug**

- Перевод абонента в ECM Idle при таймауте ожидания UE Context Release Complete



## 1.48.12.0.14.fv (2024-06-17)


 Цикл ICS из-за сброса MME UE ID
- Important **Bug**

- Исправлен сброс старого S1-контекста при TAU, теперь MME UE ID = 0 возникать не должен


 Неуспешный TAU после хэндовера из 3G в 4G
- Basic **Bug**

- ММЕ увеличивает свой счётчик NAS Count при отправке NAS security transparent container, благодаря чему первый же TAU Accept должен иметь правильное значение этого счётчика


 Всплески использования AL логик
- Critical **Bug**

- Внедрены оптимизации процедуры Paging, снижающие вероятность возникновения проблемы


 Дополнительные параметры qos_rules
- Basic **UserStory**

- В qos_rules.cfg добавлены параметры PDP_AMBR_DL и PDP_AMBR_UL, ограничивающие битрейты для nonGBR биреров по-умолчанию, отправляемые на SGSN и абонентскому устройству при Attach


 Некорректный APN-AMBR в NAS Attach accept
- Basic **Bug**

- Исправлено кодирование битрейтов больше 256К, кратных 256К


 ММЕ падение с core
- Critical **Bug**

- В компоненте HTTP устранена причина падения


 Неуспешный 4G -> 3G HO после 3G -> 4G HO
- Basic **Bug**

- Обновление charging characteristics активных PDN соединений при выполнении процедуры UL


 Добавить возможность задать таймер ожидания ответа на UEContextReleaseCommand
- Basic **UserStory**

- В mme.cfg в секцию Timeout добавлен параметр UEContextRelease, который определяет длительность ожидания UE Context Release Complete



## 1.48.11.0.8.fv (2024-05-29)


 Не работает S1HO с dedicated bearer
- Basic **Bug**

- Устранена отправка лишнего ICS Req


 S1 HO внутри одного TAC выполняется слишком долго
- Basic **Bug**

- В s1ap.cfg в секцию SCTP_AdditionalInfo добавлена поддержка флага nodelay, активирующего режим мгновенной отправки SCTP сообщений


 Цикл ICS из-за сброса MME UE ID
- Important **Bug**

- Для устранения цикла были исправлены ошибки в обработке NAS Non Delivery со старой eNodeB и прерывания TAU


 Некорректный APN-AMBR в NAS Attach accept
- Basic **Bug**

- Исправлено кодирование NAS APN-AMBR IE


 некорректное повышение IPv4 PDN до IPv4v6 PDN
- Basic **Bug**

- Некорректное повышение PDN Type устранено


 Коллизия процедур: X2 Handover полученный во время TAU с Active Flag
- Important **Bug**

- Приоритет процедуры X2 был повышен, теперь она будет прерывать TAU


 ММЕ не отправляет GTP-c TEID в modify bearer request во время входящего s1 handover без смены SGW
- Important **Bug**

- ММЕ будет заполнять поле Sender F-TEID в Modify Bearer Req при выполнении S1 HO без смены SGW



## 1.48.10.0.6.fv (2024-05-07)


 Массовая просадка kpi Attach после изменения скоростей на PGW
- Critical **Bug**

- В компоненте S1AP было исправлено кодирование битрейта в Quality of Service NAS IE


 Функционал Default APN for DNS failure
- Basic **UserStory**

- В served_plmn.cfg добавлена поддержка флага UseDefaultAPNonFailure, включение которого разрешает использовать APN из профиля по-умолчанию при отсутствии известных адресов SGW/PGW для APN, запрошенного абонентом


 Перепосылка Attach Accept + ICS Response
- Important **Bug**

- Реализовано ожидание ICS Response в ответ на ICS Request + Attach Accept в течение таймера T3450, также был добавлен инкремент NAS Count в Attach Accept при его перепосылке



## 1.48.9.0.5.fv (2024-04-25)


 Коллизия x2ho и TAU. Не выполняется релиз s1контекста
- Basic **Bug**

- Полноценный сброс S1-контекста и Access Bearers при обнаружении смены S1-контекста в TAU Req



## 1.48.8.0.4.fv (2024-04-25)


 Неуспешный S1 HO со сменой MME
- Important **Bug**

- Сброс кэша DNS выполняется для всех направлений


 Изменить Release Cause code для SRVCC
- Basic **UserStory**

- В mme.cfg добавлен параметр SRVCCS1ReleaseCauseCode, позволяющий изменить значение Release Cause code для SRVCC


 Падение с core
- Critical **Bug**

- Убрана необязательная отправка S1 Context Release Cmd, с которой связано падение


 Обрывы VoLTE после X2HO со сменой TAC
- Important **Bug**

- Kenb rekeying в процессе TAU выполняется только при отсутствии активных PDN соединений


 Во время inter MME S1 HO не приходят DeleteSessionRequest
- Basic **Bug**

- Убрано отсоединение GTP-пиров, полученных из DNS, выполнявшееся при сбросе кэша DNS



## 1.48.7.0.3.fv (2024-04-11)


 Transaction id 0 для второго PDP контекста
- Important **Bug**

- Transaction Identifier для одного и того же контекста будет одинаковым в сообщениях E-RAB Setup Req, SGSN Context Res и Fwd Relocation Req (GTPv1)



## 1.48.6.0.2.fv (2024-04-03)


 Зависание ММЕ
- Critical **Bug**

- Устранена взаимоблокировка GTP-C EchoManager и served_plmn.cfg, возникавшая при перезагрузке served_plmn.cfg



## 1.48.5.0.1.fv (2024-04-02)


 MSC Pool, MSC Offload
- Basic **UserStory**

- В процедурах TAU и Attach восстановлено использование корректного GT для отправки TMSI Realloc


 Неправильное значение Long MNC в Update Location Requset
- Basic **Bug**

- Исправлено кодирование трёхзначного MNC в DIAM-сообщениях Update Location Req и Authentication Information Req, а также в S1AP NAS EPS MobileIdentity IE



## 1.48.4.0.248 (2024-03-26)


 Некорректный DNS запрос для TAU Mapped
- Critical **Bug**

- Исправлена функция формирования идентификатора, необходимого для получения адреса SGSN из DNS


 Детектировать S1-AP Initial UE Context Request and Response и для UeContextModification
- Basic **UserStory**

- Добавлены метрики csfbMtUeContextModificationRequest, csfbMoUeContextModificationRequest, csfbMtInitialContextSetupResponse, csfbMoInitialContextSetupRequest


 Не записывается неуспешный TAU cdr
- Important **Bug**

- Параметр FilterUnresolvedTauByPLMN из served_plmn.cfg по-умолчанию имеет значение false



## 1.48.3.0.247 (2024-03-22)


 Падение MME с core
- Critical **Bug**

- Исправлена обработка сообщения NAS Non Delivery Ind, поступившего во время процедуры Detach


 Некорректно отправляется Delete Session Request по таймеру в сценариях со сменой MME/SGSN
- Important **Bug**

- При выполнении процедур SGSN Context, HO to UTRAN (в т.ч. SRVCC) и S1 HO сброс сессий на SGW будет выполняться одним Delete Session Req без указания поля EPS Bearer ID


 Преобразовать cfg code_mapping_rules.cfg -> json. Реализация
- Basic **UserStory**

- Реализовано чтение code_mapping_rules.json


 No such name в DNS trace
- Basic **Bug**

- В случае поступления DNS-ответа с кодом ошибки No such name в dns_warning будет выводиться сообщение



## 1.48.2.0.246 (2024-03-18)


 Массовая рассылка Paging сообщений за один примитив
- Basic **IT improvement**

- Для рассылки множества Paging был добавлен примитив Pr_S1AP_SEND_PAGINGS


 Разбить ExtendedServiceRequest: добавить ExtendedServiceRequestMoCsfb
- Basic **UserStory**

- Добавлена метрика ExtendedServiceRequestMoCsfb


 Разбить ExtendedServiceRequest: добавить ExtendedServiceRequestMtCsfb
- Basic **UserStory**

- Добавлена метрика ExtendedServiceRequestMtCsfb


 Разбить ExtendedServiceRequest: добавить ExtendedServiceRequestMoCsfbE
- Basic **UserStory**

- Добавлена метрика ExtendedServiceRequestMoCsfbE


 Сделать параметр OldS1ReleaseCauseCode конфигурируемым
- Basic **UserStory**

- Для некоторых S1 процедур добавлена поддержка параметра OldS1ReleaseCauseCode


 Добавить метрику csfbMTUeContextModificationResponse
- Basic **UserStory**

- Метрика добавлена


 Добавить метрику csfbMtInitialContextSetupRequest
- Basic **UserStory**

- Метрика добавлена


 Просадка KPI до 0% при рестарте соседнего MME
- Critical **Bug**

1. Отключен вызов mallinfo
2. Сокращено число примитивов, используемых для процедуры Paging
3. Оптимизировано кодирование полей сообщения Paging
4. Различные оптимизации компонента S1AP



## 1.48.1.0 (2024-03-17)


 NAS Non delivery Indication
- Basic **UserStory**

- Добавлена поддержка сообщений такого типа


 Переход на FSM_TRACE в SL файлах
- Basic **UserStory**

- Переход выполнен


 SGSap Throttling
- Basic **UserStory**

- В конфиге sgsap.cfg теперь можно ограничить число сообщений в секунду


 Падение MME2 в core
- Important **Bug**

- Добавлена проверка минимального размера содержимого NAS Message Container


 HTTP Detach Detached абонента
- Basic **Bug**

- Исправлена обработка случая с разрегистрированным абоном


 ММЕ обрабатывает некорректные профили от HSS
- Basic **Bug**

- В случае отсутствия обязательных параметров выводится ошибка с кодом -5:10


 После TAU со сменой MME используется запрещенный алгоритм шифрования
- Basic **Bug**

- Исправлено выполнение переаутентификации в случаях с нулевым KSI от стороннего ММЕ


 Неточности в записях CDR
- Basic **Bug**

- Все неточности устранены


 Не отправляется ReleaseAccessBearers после S1-Context Release в рамках TAU
- Basic **Bug**

- Исправлено


 Реализовать Equivalent PLMN
- Basic **UserStory**

- В сообщения Attach Accept и TAU Accept добавлено поле Equivalent PLMNs, содержимое которого определяется параметром EquivalentPLMNs в served_plmn.cfg


 Преобразовать cfg zc Rules -> json. Реализация
- Basic **UserStory**

- Реализовано чтение zc_rules.json


 Доработка сборки проекта на jenkins
- Basic **UserStory**

- Созданы job'ы MME_master, MME_develop, MME_custom_branch


 Преобразовать cfg forbidden_imei_rules.cfg -> json. Реализация
- Basic **UserStory**

- Реализовано чтение forbidden_imei_rules.json


 Преобразовать cfg RAC Rules -> json. Реализация
- Basic **UserStory**

- Реализовано чтение rac_rules.json


 Добавить использование linter в проект
- Basic **UserStory**

- Теперь в проекте применяется clang-format


 Добавить возможность учитывать Error Diagnostic при настройке соответсвий S6a error code к NAS Cause code
- Basic **Freq**

- В code_mapping_rules.cfg добавлено поле ErrorDiagnostic


 MME игнорирует CL
- Basic **Bug**

- При поступлении CL, процедура Attach по данному абоненту будет прерываться


 Тип детача по команде от SPGW
- Basic **UserStory**

- Таблица соответствий GTP Cause к NAS Cause была дополнена, теперь она применяется при выполнении сброса бирера по запросу от SGW


 Исключить сценарий короткой аутентификации
- Basic **Bug**

- Сценарий короткой аутентификации был удалён


 Коллизия Delete Bearer Request и X2-Handover
- Basic **Bug**

- Биреры, которые SGW отметил как неизвестные в ответе на Modify Req при X2 HO, будут удаляться из ММЕ


 MME не возвращает никакой ответ на GTPv1 Identification Request
- Basic **Bug**

- ММЕ будет возвращать GTPv1 Identification Response с причиной Mandatory IE missing в качестве ответа на Identification Request без P-TMSI Signature


 Добавление поля "кол-во MME_UE_ID" в mme.cfg, загрузка с проверкой условия и прерывание перезагрузки конфига
- Basic **UserStory**

- Добавлена поддержка поля Number_of_MME_UE_ID в mme.cfg


 Purge Request не заполняем биты
- Basic **Bug**

- Теперь биты PUR Flags будут корректно заполняться



## 1.48.0.0.245 (2024-01-31)


 Reset ставится в очередь
- Basic **Bug**

- Повышен приоритет процедуры Reset


 Downlink Data Notification Acknowledge приходит через раз
- Basic **Bug**

- DDN помещается в очередь ожидания, что позволяет отправить ответ на каждый полученный запрос такого типа


 Сообщение Notify reqest блокирует работу других процедур
- Basic **Bug**

- Обработка Notify делается в неблокирующем режиме


 2 UeContextReleaseRequest на Target EnB при неуспехе S1-Ho
- Basic **Bug**

- Однократная отправка UeContextReleaseRequest при неуспехе S1-Ho


 игнор init Attach во время paging по Update Bearer Request
- Important **UserStory**

- Коректная обработка прерывания процедуры Bearer Modification


 MME не всегда отвечает на Delete Bearer Request
- Basic **Bug**

- Delete Bearer Request помещается в очередь ожидания, что позволяет отправить ответ на каждый полученный запрос такого типа


 Не возвращаются некоторые метрики по TAU Reject
- Basic **Bug**

- Извлечение типа TAU выполняется перед отбоем по белому списку IMSI


 Не отправлять Reset при получении ErrorIndication c unknown-pair-ue-s1ap-id
- Basic **UserStory**

- Излишняя отправка Reset устранена


 Выбор S11 SGW по canonical node name
- Basic **UserStory**

- Добавлено извлечение адреса SGW по x-3gpp-sgw:x-s5-gtp


 Attach с GUTI другого MME
- Important **Bug**

- Убрана необязательная отправка Security Mode Cmd, приводившая к проблеме


 TAU-InitialUeMessage после S1HO
- Basic **Bug**

- Принудительное закрытие старого S1-контекста, отправка TAU Accept в новый S1-контекст


 Цикличный TAU с типом combined TA/LA updating
- Basic **Bug**

- В TAU Accept добавлено поле EPS bearer context status, отправляемое при наличии аналогичного поля в TAU Req


 При выполнении процедуры PDN Disconnect нет записи в CDR
- Basic **Bug**

- Записи о Delete Session добавлены в GTP-C CDR


 S1HO Mandatory IE Incorrect
- Important **Bug**

- Ошибка с некорректным IE устранена


 MME не удаляет созданную сессию при неудачном Attach
- Basic **Bug**

- Добавлено удаление сессии при неудачном Attach


 Service Req не проходит контроль целостности
- Basic **UserStory**

- Исправлен вывод UL NAS Count из предыдущего значения (из БД) и того, которое пришло в Service Req


 Игнор Attach Initial-Ue-Message во время Detach
- Basic **Bug**

- После успешного Detach с re-attach, вызванного по HTTP, purge профиля абонента выполняться не будет


 ММЕ упал с коре
- Critical **Bug**

- Устранена гонка данных при обработке Extended Service Req


 SGSN Context Req decoding failed
- Basic **UserStory**

- Добавлена поддержка GTPv1 MM Context GSM Key and Triplets


 Выбор eNB при HOfromUTRAN
- Important **UserStory**

- Испарвлено чтение eNodeB ID


 MME не удалил сессию на SPGW
- Important **Bug**

- Добавлено удаление сессии после неуспешного Attach


 Записывать Target eNb ID в DEC в S1 HO edr и I-RAT HO edr
- Basic **Freq**

- Target eNB ID записывается в DEC


 Улучшение алгоритма обработки Paging по Detach
- Basic **UserStory**

- Отказ на TAU и обычный/Ext/CP Service Req с причиной "implicitly detached" в случаях, когда Paging был вызван необходимостью detach


 MME не делает CS Paging после S1 релиза cs-fallback-triggered
- Important **Bug**

- Причина cs-fallback-triggered при разрыве S1-контекста более не должна блокировать SGs Paging


 Не работает S1HO с dedicated bearer
- Basic **Bug**

- Добавлено кодирование MBR и GBR в Fwd Relocation Req и Handover Req


 Неуспешный TAU в Uplink NAS transport
- Basic **Bug**

- ICS при обработке TAU Req с Active Flag сделан необязательным


 Исключить выставление бита CRSI в Indication для CSR и Modify Session Request
- Critical **UserStory**

- Флаг CRSI убран


 Инвертируются значения PCI и PVI в GTPv2
- Important **Bug**

- Исправлено считывание значений PCI и PVI из сообщений от HSS


 Нормализация GTP сигнализации в сценарии Attach по образцу E///
- Critical **UserStory**

- Из Create Session Request и Modify Bearer Request отправляемых при процедурах Attach и PDN Connectivity Req удалены поля Linked EPS Bearer ID и RAT Type


 Некорректная Timezone
- Basic **Bug**

- Исправлено кодирование Timezone


 Использование mimalloc
- Basic **UserStory**

- Используется mimalloc


 Неправильное заполнение ULR-Flags в сценарии TAU-перехода с Gn/Gp SGSN
- Basic **Bug**

- Исправлено заполнение ULR-Flags в сценарии TAU-перехода с Gn/Gp SGSN


 MME присылает повторное сообщение S1 Context Release в рамках процедуры CSFB
- Basic **UserStory**

- Исправлена обработка прерываний процедур ICS и S1 Release, приводившая к проблеме


 Цикл неуспешных Service Request
- Basic **Bug**

- Цикл возникать не должен, второй service req будет отбитваться с причиной implicitly detached


 Доработка TAU cdr MME
- Basic **Freq**

- Добавлен параметр FilterUnresolvedTauByPLMN, регулирующий вывод CDR при отбое запроса TAU из макро сетей


 Нулевой UEAMBR в Handover Request
- Important **Bug**

- В случае отсутствия соответствующего поля в Fwd Relocation Req, UE AMBR вычисляется как сумма APN AMBR отдельных PDN Connectivity


 Передача OLD LAI в SGsAP
- Important **Freq**

- Добавлена передача Old LAI в SGsAP Location Update Req


 Добавить счётчик ServiceReject
- Basic **UserStory**

- Счётчик добавлен



## 1.47.1.0.243 (2023-11-21)


 Управление триггерами восстановления абонентов
- Basic **UserStory**

- Добавлены флаги, регулирующие восстановление абонентского профиля при указанных процедурах


 На MME в edr возникает цикл неуспешных PDN Disconnection req
- Basic **Bug**

- Исправлено


 SGSN Context Response diff with ZTE and Huawei
- Important **UserStory**

- Поля добавлены


 В http response в секции s1Attach отсутствуют метрики emergencyAttachRequest, emergencyAttachSuccess
- Important **Bug**

- Метрики добавлены


 Отсутствует агрегированное значение attachFail, combinedAttachFail
- Important **Bug**

- Значения добавлены


 Context not found в ответ на Release Access Bearers
- Basic **Bug**

- Добавлена процедура Detach


 Флаг SGAP-EPS_Detach-Indication после Ho/Reselect в 2g/3g
- Basic **UserStory**

- Добавлена возможность отключения процедуры SGsAP Detach Ind после HO с помощью флага DetachIndAfterHO


 Не работает входящий S1HO со сменой MME
- Basic **Bug**

- Исправлено


 Topon выбор SGW при переходах из 2G/3G с Gn SGSN
- Basic **UserStory**

- Добавлены отправки FQDN и учёт входящего PGW FQDN при выборе SGW


 Не работает исходящий S1HO с двумя бирерами
- Basic **Bug**

- Исправлено


 На MME в edr у конкретных абонентов возникает цикл неуспешных TAU
- Basic **Bug**

- Исправлено


 Некорректное время в cdr
- Basic **Bug**

- Исправлено


 Не отработал Fallback на Ipv4 сессию по команде PGW
- Basic **Bug**

- Исправлено


 Учитывать S8-интерфейс в DNS ответе при выборе PGW
- Basic **UserStory**

- Добавлен учёт S8-интерфейса


 Не выставляется Indication Dual Address Bearer Flag для Ipv4v6 бирера на MME
- Basic **Bug**

- Флаг выставляется


 Не резолвятся PGW Мегафона через DNS
- Basic **Bug**

- Игнорирование регистра при сравнении доменных имён


 В списке GTP пиров отображается пир с пустым ip адресом
- Low **Bug**

- Исправлено


 Добавление метрики s1Attach emergencyAttachFail
- Important **Task**

- Метрика добавлена


 Extended Service Request для MO Call не обрабатывается если абонент пришел через TAU с Mapped GUTI
- Basic **Bug**

- Добавлен отбой TAU с причиной No EPS bearer context activated


 SGS Location Update после смены MME
- Basic **Freq**

- Добавлен SGs Location Update в описанных ситуациях


 Service Request без ответа
- Important **Bug**

- Исправлено


 Метрики: S1 Security
- Basic **UserStory**

- Метрики добавлены


 Добавить счётчики в Detach
- Basic **UserStory**

- Счётчики добавлены


 ReleaseOnICS_Fail = I применить к сценарию Service Reject
- Basic **UserStory**

- Сброс контекста в описанной ситуации определяется флагом ReleaseOnICS_Fail


 Нет аутентификации после Attach Request not security protected
- Basic **Bug**

- Исправлено


 Service Request Integrity Protection Verification
- Basic **UserStory**

- Добавлена проверка целостности Service Req, при неудаче выводится ошибка -1:12


 Двойной UE Context Release после TAU Reject с implicit detach
- Basic **Bug**

- Исправлено


 S1 HO со сменой MME выполняется слишком долго
- Basic **Bug**

- Исправлено


 DDN vs Service Req Mo-Data
- Basic **Bug**

- Исправлено


 Аутентификация абонента в состоянии connected во время TAU
- Basic **Bug**

- Исправлено


 Нет SGs LU после Reset
- Basic **Bug**

- Исправлено


 Отбивать periodic TAU от MME чужих групп
- Important **UserStory**

- Periodic TAU будет отбиваться в том случае, когда для его выполнения требуется запрос профиля со старого MME/SGSN



## 1.47.0.0.238 (2023-10-10)


 Несколько SGS подключений с одним MSC
- Basic **UserStory**

- Добавлена поддержка нескольких SGS соединений с одним MSC, в доке components/sgsap.cfg описан соответствующий вариант элемента таблицы PeerTable


 HTTP Запрос для сброса метрик reset_metrics не работает
- Important **Bug**

- Исправлено


 Некорректная причина attach reject
- Basic **Bug**

- Исправлено


 Некорректная причина attach reject - cause 2 "IMSI Unknown in HSS"
- Basic **Bug**

- Исправлено


 Поправить rx/tx в метриках
- Basic **Bug**

- Исправлено


 Восстановление абонента по SGS Paging
- Important **UserStory**

- Внесены исправления



## 1.46.2.0.237 (2023-10-05)


 Абонент попадает в цикл Initial Context Setup Failure
- Important **Bug**

- Для избежания зацикливания выполняется Detach


 Выбор через DNS topon S/PGW из списка с одинаковым весом не соответствует полученному приоритету
- Important **Bug**

- Исправлено


 Политика paging для SGS
- Important **UserStory**

- Для MT Call и MT SMS выполняется укороченный вариант процедуры Paging - только для последней TA и только одна попытка.


 Перенести метрики S11_Bearers в S11_Interface
- Basic **UserStory**

- Метрики перенесены


 Forward Relocation Request Mandatory IE incorrect
- Important **Bug**

- В mme.cfg добавлен новый раздел VendorSpecific с флагом LongUTRAN_TransparentContainer, который регулирует поддержку таких UTRAN Transparent Container. По-умолчанию флаг выставлен, но если его сбросить, то при обнаружении слишком длинного контейнера процедура HO 4G->3G будет прервана и выполнится сброс S1 контекста.


 Выдача метрик Diameter по HTTP
- Basic **UserStory**

- Метрики диаметра возвращаются по HTTP-запросу get_metrics


 Не работает SRVCC после ISD
- Important **Freq**

- Исправлено


 Метрика не возвращается по http
- Basic **Bug**

- Исправлено


 Имена групп метрик в lowerCamelCase
- Important **UserStory**

- Имена групп метрик отредактированы


 Неуспешный код ошибки после успешного MT вызова
- Basic **Bug**

- Исправлено


 Приостановка сессий на время CS Fallback и SRVCC
- Important **UserStory**

- Реализован механизм Suspend/Resume, активируемый флагом Suspend в mme.cfg. DDN для абонента в состоянии Suspension будет отбиваться с причиной "Unable to page UE due to Suspension".


 В CDR для Reset не отображаются enodeb_id
- Basic **Bug**

- Исправлено


 Восстановление абонента по SGS Paging
- Important **UserStory**

- Реализован поиск профиля неизвестного абона в БД


 SGSaP Service Request добавить поля
- Important **UserStory**

- Поля добавлены


 Добавить отсутствующие метрики
- Important **UserStory**

- Метрики добавлены


 Нулевое значение метрики attachRequest при получении метрики attachFail11PlmnNotAllowed
- Important **Bug**

- Исправлено


 Разбиение по APN
- Important **UserStory**

- Добавлено разбиение указанных метрик на группы по APN


 Утечка памяти в библиотеке PCAP
- Critical **Bug**

- Утечка устранена в ветке develop, ММЕ переведён на неё


 Нулевое значение метрики activateDefaultEpsBearerContextAccept при запуске сценария DetachHSS
- Important **Bug**

- Исправлено


 MME не сделал reAttach по команде detach_by_vlr
- Important **Bug**

- Исправлено


 Extended Service Request Subscriber Restoration in MME-pool
- Basic **UserStory**

- Реализован поиск профиля неизвестного абона в БД


 Перепутаны метрики по detach
- Basic **Bug**

- Исправлено


 TAU Subscriber Restoration in MME-pool
- Basic **UserStory**

- Реализован поиск профиля неизвестного абона в БД


 Рестарт MME после обновления версии (Бованенково)
- Basic **Bug**

- Исправлено


 В рамках CSFB на MSC отправляется некорректный Mobile Station Classmark 2 в SGsAP-SERVICE-REQUEST
- Important **Bug**

- Исправлено


 В ответе на запрос get_metrics для s6a отсутствует деление на группы
- Basic **Bug**

- Исправлено


 Поправить ipv4 и ipv6
- Basic **Bug**

- Исправлено


 Поправить детализацию ошибок по метрикам
- Basic **Bug**

- Исправлено


 Не работает метрика numberOfEnodeb
- Important **Bug**

- Исправлено



## 1.46.1.0.234 (2023-09-08)


 Защита от перегрузок при процедуре S1AP Attach
- Basic **UserStory**

- Добавлено общесистемное ограничение по Attach Req


 Переформатировать файл для RnD-MME-1_MME-Users_2023-08-18T1955+0300_5min
- Basic **UserStory**

- Убран вывод нулевого числа dedicated bearer


 Поддержать список ответов DNS NAPTR
- Important **Freq**

- Поддержка добавлена


 Нет ответа на Update Bearer во время TAU
- Important **Freq**

- Процедура обновления бирера теперь ставится в очередь


 Метрики MME
- Basic **Epic**

- Полная поддержка метрик, описанных в задаче



## 1.46.0.0.233 (2023-08-25)


 S1ap Trace Procedures
- Basic **UserStory**

- Добавлена поддержка трассировок. Таблица БД обновлена.


 Некорректное значение Cause в UeContextReleaseCommand
- Basic **Bug**

- Исправлено


 Возвращать метрики по http запросу
- Basic **UserStory**

- Добавлена поддержка HTTP-запросов get_metrics и reset_metrics


 S1 mode MM: attach, detach, security, service
- Basic **UserStory**

- Метрики добавлены


 Игнор аттача по GUTI.
- Basic **Bug**

- Исправлено


 Ошибка Context not Found на запрос SGSN Context Req
- Important **Bug**

- Величина генерируемого M-TMSI ограничена значением 0x3fffffff


 S11 bearer
- Basic **UserStory**

- Метрики добавлены


 S1 mode handover
- Basic **UserStory**

- Метрики добавлены


 S1 mode TAU metrics
- Basic **UserStory**

- Метрики добавлены


 S1 mode MM dedic bearer activation
- Basic **UserStory**

- Добавлены метрики activation и connectivity


 Метрики: SGs interface
- Basic **UserStory**

- Метрики добавлены


 Метрики: Sv interface
- Basic **UserStory**

- Метрики добавлены


 Target eNB ID в cdr S1 HO
- Basic **Task**

- Поле добавлено в CDR


 Метрики: HW Resource
- Basic **UserStory**

- Метрики добавлены


 Не записывается EMM Cause в TAU и Attach cdr
- Important **Bug**

- Исправлено


 MME отбивает I-RAT HO to UTRAN с причиной с cause ho-target-not-allowed
- Important **Bug**

- Исправлено


 Роуминг с PLMN с 3ех значным MNC
- Basic **UserStory**

- Добавлена поддержка конфига long_mnc.cfg


 Attach и TAU при временной недоступности MSC
- Important **UserStory**

- Изменено значение EMM Cause отправляемого в описанной ситуации


 PS Paging после CSFB в 2G
- Important **Bug**

- Исправлено


 S1 mode user resource
- Basic **UserStory**

- Метрики добавлены


 S1 mode RealTimeCounters
- Basic **UserStory**

- Метрики добавлены


 S1 mode bearer deactivation
- Basic **UserStory**

- Метрики добавлены


 S1 release
- Basic **UserStory**

- Метрики добавлены


 S1 EPS bearer activation
- Basic **UserStory**

- Метрики добавлены


 Доработки по метрикам
- Basic **Task**

- Внесены исправления


 Метрики: S1 interface
- Basic **UserStory**

- Добавлены все оставшиеся метрики


 Нет релиза S1 контекста после comb TAU без Active флага
- Important **Bug**

- Добавлен сброс S1-контекста по завершении процедуры combined TAU без Active флага


 Неуспешный MT вызов на MSC после перехода в 3G
- Basic **Freq**

- Добавлена отправка EPS-DETACH-IND


 S1 Error Indication
- Basic **Bug**

- Исправлено


 S1 mode bearer modification
- Basic **UserStory**

- Метрики добавлены


 Доработки по метрикам
- Basic **UserStory**

- Доработки внесены


 S1 mode paging
- Basic **UserStory**

- Метрики добавлены


 No CLI + incorrect paging identity CS Servince Notification
- Important **Bug**

- Отправка CLI добавлена, значение Paging Identity исправлено


 Переподключение к DB при потери связности с ней
- Important **Bug**

- Попытка восстановления соединения с БД при очередной записи профилей


 SGS Multiple PLMN
- Basic **Freq**

- Добавлена поддержка Multiple PLMN, соответствующие доработки внесены в sgsap.cfg и served_plmn.cfg


 Переформатировать файл для RnD-MME-1_MME-Users_2023-08-18T1955+0300_5min
- Basic **UserStory**

- Файл переформатирован


 Downlink Data Notification для ECM Connected абонентов
- Basic **UserStory**

- При получении DDN с соответствующим cause выполняется сброс S1-контекста



## 1.45.0.0.232 (2023-07-13)


 MVP для S11
- Basic **UserStory**

- Добавлена поддержка сбора метрик, определённых в MVP для S11


 MVP для S1
- Basic **UserStory**

- Добавлена поддержка сбора метрик, определённых в MVP для S1


 Утечка storage
- Basic **Bug**

- Вывод количества ключей в хранилище по отдельности


 Некорректное заполнение GTP cause в SGSN Context cdr
- Basic **Bug**

- Добавлен вывод SGSN Context Ack Cause


 Балансировка DNS
- Important **Freq**

- Задействован механизм балансировки библиотеки DNS



## 1.44.3.0.231 (2023-06-27)


 Поддержать выделение id-MME-UE-ID, TMSI, TEID из range конфигурации
- Basic **UserStory**

- Исправлена инициализация ограничителей MME UE ID и TEID при пустой БД. Релизную сборку 1.44.2.0 необходимо либо откатить, либо обновить на более свежую.



## 1.44.2.0.230 (2023-06-21)


 Поддержать выделение id-MME-UE-ID, TMSI, TEID из range конфигурации
- Basic **UserStory**

- Поддержка добавлена


 Падение MME core при передачи параметра в create session response
- Basic **Bug**

- Исправлено


 Нестабильный gngp коннект к SGSN без видимых причин
- Basic **Bug**

- Поле Recovery убрано из GTPv1 Echo Req


 LAC 0 в Initial Context Setup Request
- Basic **Bug**

- Исправлено


 Добавить поле LAC в SGsAP cdr
- Basic **UserStory**

- Поле добавлено


 Утечка обработчиков
- Critical **Bug**

- Добавлено заполнение списка S1-контекстов в Reset Ack


 Перевод сборки на CMake
- Basic **UserStory**

- Сборка переведена на CMake


 P-CSCF Restoration
- Basic **UserStory**

- Добавлена поддержка P-CSCF Restoration через PDN Disconnection


 Проблема при выполнении сценария Detach initiated by HSS
- Basic **Bug**

- Исправлено



## 1.43.0.0.226 (2023-04-05)

 Всплеск Service Reject (CS Domain not available) после рестарта MME
- Basic **Bug**

- Добавлено сохранение GT выбранного VLR в БД. Таблица БД обновлена.


 Create Session Request PAA 0.0.0.0
- Basic **Bug**

- Исправлено


 Унифицированный cdr для diam транзакций
- Basic **UserStory**

- CDR для DIAM унифицирован


 Убрать флаг Handover Indication
- Basic **Freq**

- Флаг убран


 Добавить поле Cause в gtp-c cdr
- Low **Task**

- Поле добавлено


 Не записывать в БД профили чужих абонентов
- Basic **Freq**

- Исправлено


 Убрать EBI из Create Session Request в сценарии TAU с mapped GUTI
- Basic **Bug**

- EBI убран


 Update Bearer Incorrect cause
- Basic **Bug**

- Исправлено


 ММЕ не может обработать ресет по множеству S1 соединений.
- Basic **Bug**

- Добавлен Reset по ENB UE ID


 Релиз контекста сразу после TAU с Active флагом
- Basic **Bug**

- Исправлено


 Reset CDR
- Basic **Freq**

- CDR добавлен


 Неправильно формируется GUTI из TLLI
- Basic **Bug**

- Теперь в третий байт M-TMSI пишется 0 вместо RAC


 Высокая длительность процедуры attach при отсутствии рабочей внешей БД
- Basic **Bug**

- Исправлено


 Не проходят MT процедуры при работе с MSC pool
- Basic **Bug**

- Исправлено


 QoS Mapping при переходе из 4G в 3G
- Basic **UserStory**

- Сопоставление QoS доработано


 NCC после обнуления
- Critical **Bug**

- Исправлено


## 1.42.0.0.225 (2023-03-01)

 S11 create session request with non-zero teid
- Basic **Freq**

- На все PDN Connectivity профиля приходится один S11 UL TEID. Для данной фичи необходима поддержка со стороны SGW.


 Вынести компоненты в отдельные репозитории
- Basic **Task**

- Перенос выполнен


 MME выбирает разные SGW для разных PDN одного абонента
- Basic **Bug**

- Исправлено


 Не работает включение работы с внешней БД через reload
- Basic **Bug**

- Исправлено


 Некорректный тип cause для Service reject отправляемых абонентам по которым был сделан Implicit Detach
- Critical **Bug**

- Убрана причина EPS services and non-EPS services not allowed


 Некорректное кодирование Timezone с минутами
- Basic **Bug**

- Исправлено


 Унифицированный cdr для diam транзакций
- Basic **UserStory**

- CDR переведены в указанный формат


 RAC считается равным 0, если его нет во входящем сообщении
- Basic **Freq**

- При отсутствии указания RAC, он считается равным 0


#### 1.41.0.0.223 (2023-01-25)

 Смена библиотеки для регулярных выражений
- Basic **Task**
- Выполнен переход на boost::regex. Все маски, для которых раньше использовался синтаксис templ_selector, следует переписать под синтаксис PCRE

 TopON выбор PGW
- Basic **Freq**
- Добавлена поддержка topon. В конфигах served_plmn, apn_rules и tac_rules появилась возможность указать FQDN для адресов SGW и PGW.

 Некорректный вывод результата команды очистки DNS кеша
- Basic **Bug**
- Исправлено

 Интеграция с MSC in a pool
- Basic **Freq**
- Добавлена поддержка MSC Pool

 APN resolution by Geographical Area
- Basic **Freq**
- В tac_rules добавлен параметр Extra_APN_label, в котором можно указать идентификатор, используемый при поиске адреса PGW в DNS

 Support saving Auth-vectors
- Basic **Freq**
- В served_plmn добавлен параметр AuthVectorsNum, который определяет количество запрашиваемых векторов аутентификации

 Отбой Attach при несовпадении допустимых алгоритмов шифрования и integrity
- Basic **Freq**
- Сделано

 Удаление сессий по обнаружению сбоя SGW
- Basic **Freq**
- Сделано

 eDRX Narrowband S1 Mode Paging Period и Narrowband S1 Mode PTW Length
- Basic **Freq**
- Добавлены параметры PTW и eDRX, которые определяют Paging Time Window и значение eDRX

 Topon выбор SGW при переходах из 2G/3G
- Basic **Freq**
- При переходе из 2G/3G в 4G новый SGW выбирается согласно PGW FQDN, если тот содержит topon

 PGW Restart Notification
- Basic **Freq**
- Добавлена поддержка PGW Restart Notification

 Убрать записи об абонентах не попадающих под IMSI WL
- Basic **Freq**
- Записи убраны

 Попытка поднять контекст для задетаченного абонента
- Basic **Bug**
- Исправлено

 Вывод в CDR информации об отказах от очереди ожидания
- Basic **Task**
- В CDR добавлен вывод информации об отказах от очереди ожидания

 Тип интерфейса для взаимодействия с SGSN в rac_rules.cfg
- Basic **Freq**
- Добавлена возможность указать флаг GTPv1 для адреса SGSN в rac_rules

 Не удалили контекст при получении ошибки PGW not responding в Modify Bearer Response
- Basic **Bug**
- Исправлено

 Перенести nbiot записи s1ap_cdr на уровень 2
- Basic **Freq**
- CDR перенесены на уровень 2

 Не хватает сообщений в трейсе по абоненту
- Low **Freq**
- Добавлен вывод Purge Answer

 Отображение не разрешенных PLMN в s1_peers
- Basic **Task**
- Такие PLMN более не отображаются

 Connect Cdr убрать символы из поля Requsted APN
- Basic **Freq**
- Символы заменены на пробел

 Некорректная работа команды detach c флагом purge=0
- Basic **Bug**
- Исправлено

 Добавить поле в get_location
- Low **Freq**
- Поле добавлено

 Добавить поле Cause в gtp-c cdr
- Low **Task**
- Поле добавлено

 Укороченное значение IMSI
- Basic **Bug**
- Исправлено

#### 1.40.0.0.222 (2022-10-31)

 SRVCC Sv-Interface
- Basic **Freq**
- Добавлена поддержка SRVCC. Таблица БД обновлена.

 Массовые отбои после 10-15 минут работы под нагрузкой
- Critical **Bug**
- Исправлено

 Журнал paging
- Basic **Freq**
- Добавлен журнал с типом "paging_cdr"

 Журнал TAU
- Basic **Freq**
- Добавлен журнал с типом "tau_cdr"

 Журнал LTE Handoverов
- Basic **Freq**
- Добавлен журнал с типом "lte_handover_cdr"

 Журнал Inter-Rat Handover
- Basic **Freq**
- Добавлен журнал с типом "irat_handover_cdr"

 Выбор APN для гостевых абонентов
- Basic **Bug**
- Исправлено

 Журнал dedicated bearers
- Basic **Freq**
- Добавлен журнал с типом "dedicated_bearer_cdr"

 Журнал Service Req и S1 Context Release
- Basic **Freq**
- Добавлен журнал с типом "s1ap_context_cdr"

 Поддержка DNS SRV запросов для резолвинга SGW/PGW
- Basic **Freq**
- Добавлены поддержка SRV и механизм извлечения SRV и A/AAAA из Additional Records

 Добавить маски для APN NI в apn_rules.cfg
- Basic **Freq**
- Добавлена поддержка масок

 Конфигурируемые модели пейджинга
- Basic **Freq**
- Добавлен параметр PagingModel, который определяет, какая модель paging используется

 Поддержка веса и приоритета записей в DNS резолвинге SGW/PGW
- Basic **Freq**
- Поддержка добавлена

 Handover 2g/3g
- Basic **Freq**
- Набор исправлений для GTPv1

 Разрешить регистрацию абонентов с неизвестными для EIR устройствами
- Basic **Freq**
- Добавлен параметр AllowUnknownIMEI

 Создать новый журнал для абонентского трейса ue_trace
- Basic **Freq**
- Внесены доработки

 Посылка ULI в modify bearer request
- Basic **Bug**
- Исправлено

 Detach с Rettach-Required после failure-in-radio-interface-procedure
- Basic **Freq**
- Исправлено

 InitialContextSetup Failure Cause
- Basic **Freq**
- В S1AP CDR Initial Context Setup добавлены поля CauseType и CauseReason

 Purge после Attach
- Basic **Bug**
- Исправлено

 S11UliAlwaysSent
- Basic **Freq**
- Добавлен флаг S11UliAlwaysSent

 QCI, QoS MBR correction by APN
- Basic **Freq**
- В qos_rules.cfg добавлена поддержка масок APN-NI

 Не отправили NB-IoT UE Identity Index
- Basic **Bug**
- Исправлено

 HTTP Get SGS Peers SL id: 4679 SL timeot
- Basic **Bug**
- Исправлено

 Регистр APN-NI в apn_rules.cfg
- Basic **Bug**
- Исправлено

 VoPS Based on IMS APN
- Basic **Freq**
- В served_plmn и qos_rules добавлены параметры VoPS_APN_NI, определяющие списки APN-NI для VoPS

 Stoped here: <{internet}>
- Basic **Bug**
- Исправлено

 Добавить записи в diam cdr
- Low **Freq**
- Записи добавлены

 В текстовом трейсе UE для сообщения Attach Accept не пишется NAS
- Basic **Bug**
- Исправлено

 Non-IP flags в Update Location Request
- Basic **Freq**
- Флаги добавлены

 Detach по DSD во время IDLE
- Basic **Bug**
- Исправлено

 Очитска кеша DNS
- Basic **Freq**
- Добавлена поддержка http-запроса clear_dns_cache

 поддержка SON
- Basic **Freq**
- Поддержка добавлена

#### 1.39.1.0.221 (2022-10-06)

 рост загрузки SI потока при использовании sctp-буфера
- Basic **Bug**
- Сборка с доработкой ATE-712

#### 1.39.0.0.220 (2022-08-16)

 Extended MBR 5G
- Basic **Freq**
- Добавлена поддержка Extended MBR. Таблица БД обновлена.

#### 1.38.0.0.218 (2022-08-03)

 SGSAP VLR Reset Procedure
- Basic **Freq**
- Добавлена поддержка процедуры VLR Reset. Таблица БД обновлена.

 Конфигурация мапинга Result-Code в NAS коды ошибок
- Basic **Freq**
- Учёт Experimental Result Code

 Отправка GTP Response на неправильный адрес
- Important **Bug**
- Исправлено

 Запись IMEISV в connect cdr
- Basic **Task**
- Добавлено

 TAU в Connected состоянии разрывает S1-контекст
- Basic **Bug**
- Исправлено

 Некорректное округление битрейта в GTP
- Basic **Bug**
- Исправлено

 GTPv1 Identification Request
- Basic **Freq**
- Отдельный CDR для GTPv1 Identification Req

 Cancel Location во время MT Paging
- Basic **Freq**
- Причина Interrat redirection трактуется как недоступность абона для PS

#### 1.37.2.0.217 (2022-07-27)

 Нет ответа на GTP SGSN Context Request
- Basic **Freq**
- Добавлена поддержка запросов с P-TMSI + P-TMSI Signature и TLLI + P-TMSI Signature

 Конфигурация мапинга Result-Code в NAS коды ошибок
- Basic **Freq**
- В served_plmn.cfg добавлено поле CodeMapping_rules, в котором можно перечислить соответствующие правила. Сами правила описываются в конфиге code_mapping_rules.cfg.

 UE Timezone в GTPv2
- Basic **Freq**
- Добавлено

 Sgs Multihoming
- Basic **Freq**
- Добавлена возможность указать адреса для multihoming в разделе LocalInterfaces. Синтаксис такой же, как в аналогичном разделе настройки Diameter.

#### 1.37.1.0.215 (2022-07-14)

 Перехват исключений из mariadbpp
- Important **Bug**
- Исправлено

#### 1.37.0.0.214 (2022-07-12)

 Mobile Reachable timer Implicit Detach timer PPF Flag
- Important **Freq**
- Добавлена поддержка двух таймеров. Таблица БД обновлена.

 Переопределения параметров QoS поступивших от HSS
- Basic **Freq**
- Добавлен параметр QoS_rules, в котором можно перечислить соответствующие правила. Правила описываются в qos_rules.cfg и позволяют связать маски IMSI с параметрами QoS.

 Поддержать процедуру MME Configuration Update
- Basic **Freq**
- Реализовано

 Restriction VoLTE based on TAC
- Basic **Freq**
- Добавлен флаг IMS_VoPS в tac_rules.cfg

 Добавить поля CN domain и TAC для Paging в cdr
- Basic **Freq**
- Поля добавлены

 CS Paging по IMSI
- Basic **Bug**
- Исправлено

 Отправка LAI в Tracking area update accept
- Important **Bug**
- Исправлено

 DNS Warning
- Basic **Freq**
- Ошибочные записи вынесены в лог с типом "dns_warning"

 SGS Detach при неуспешном Attach
- Basic **Bug**
- Добавлена отправка SGS Detach Ind

 Причина релиза S1-контекста в cdr
- Basic **Freq**
- К CDR S1Release добавлен вывод CauseType и CauseReason - тип и код причины

 Некорректное согласование QoS после TAU
- Basic **Bug**
- Добавлено сохранение APN AMBR из Create Session Res

 Paging после релиза ue-not-available-for-ps-service
- Important **Bug**
- После сброса контекста с причиной "ue-not-available for-ps-service", на SGsAP Paging Req должен возвращаться отказ с причиной "IMSI detached for EPS services"

 HTTP интерфейс для мониторинга доступности БД
- Basic **Freq**
- Добавлена поддержка http-запроса get_db_status

#### 1.36.1.0.212 (2022-06-10)

 Отсутствуют S1AP ответы после получения ENBConfigurationUpdate
- Basic **Bug**
- Исправлено

 MME не делает релиз контекста сразу после процедуры HSS initiated Detach с флагом Re-attach
- Basic **Bug**
- Attach Req (reattach) тождественен Detach Accept при выполнении Detach

 Добавить API команду для деактивации бирера у абонента
- Basic **Task**
- Некоторые исправления

#### 1.36.0.0.211 (2022-06-07)

 MME Control of overload
- Important **Freq**
- Реализовано

 Создать новый журнал для абонентского трейса ue_trace
- Basic **Freq**
- Добавил новый журнал типа "ue_trace", его необходимо прописать в trace.cfg. Туда попадают описания входящих и исходящих сообщений, связанных с абоном.

 Удаление PDN context по DSD
- Basic **Freq**
- Реализовано

 Прерывать процедуру TAU при поступлении нового TAU в состоянии ожидания TAU Complete
- Basic **Freq**
- Реализовано

 Сбрасывать S1 контекст при неуспешной отработке процедры Service Request
- Basic **Freq**
- Реализовано

 Диапазон TAC'ов в tac_rules.cfg
- Basic **Freq**
- Добавлена возможность определить TAC в виде списка (через запятую) конкретных значений и диапазонов.

 Таймер T3422
- Basic **Freq**
- Добавлен параметр T3422, который определяет длительность одноимённого таймера

 Реализовать таймер T3450
- Basic **Freq**
- Добавлен параметр T3450, который определяет длительность одноимённого таймера

 Реализовать таймер T3460
- Basic **Freq**
- Добавлен параметр T3460, который определяет длительность одноимённого таймера

 Реализовать таймер T3470
- Basic **Freq**
- Добавлен параметр T3470, который определяет длительность одноимённого таймера

 Число попыток ретнасмита по T3450, T3460, T3470
- Basic **Freq**
- Добавлены параметры T3450_RepeatCount, T3460_RepeatCount и T3470_RepeatCount

 Secondary RAT Reporting 5G NSA
- Basic **Freq**
- Добавлена поддержка процедуры Secondary RAT Reporting

 S1 Multihoming
- Basic **Freq**
- Добавлена возможность указать адреса для multihoming в разделе LocalInterfaces. Синтаксис такой же, как в аналогичном разделе настройки Diameter.

 HandoverRestrictionLIst
- Basic **Freq**
- Добавлен параметр EquivalentPLMNs, который определяет список эквивалентных PLMN

 Поддержка S6a Reset для массового сброса абонентов
- Important **Freq**
- Поддержка сохранения флага Reset. Таблица БД обновлена.

 Добавить API команду для деактивации бирера у абонента
- Basic **Task**
- Добавлен http-запрос deact_bearer, который деактивирует указанный бирер

 Отправка лишнего Context Release после S1 Handover
- Basic **Bug**
- Исправлено

 Падение с core
- Basic **Bug**
- Исправлено

 В Attach cdr убирать перенос строки из APN полученном от абонента
- Basic **Task**
- Исправлено

 PCO для PAP Аутентификации
- Basic **Freq**
- Добавлена поддержка PAP

 Не успешный MO CSFB для абонентов в IDLE mode
- Important **Bug**
- Исправлено

 Regional-Subscription-Zone-Code
- Basic **Freq**
- Добавлен параметр ZC_rules в котором можно перечислить правила, связывающие ZC и TAC

 S1 Handover со сменой MME
- Basic **Bug**
- Исправлено

#### 1.35.0.0.210 (2022-04-18)

 Paging устройств, которые подключены к NB-IOT RAT
- Basic **Freq**
- Исправлено определение NB IoT статуса при paging

 Функциональность dedicated core network на MME
- Basic **Freq**
- В served_plmn.cfg добавлены поля для настройки UE Usage Type

 Не доставляется вторая из нескольких отправленных подряд MT SMS
- Basic **Bug**
- Исправлено

 Сброс контекста после ICS Failure
- Basic **Freq**
- Исправлено для случая TAU

 Добавить NB-IoT Paging eDRX Information в Paging
- Basic **Freq**
- Добавлено

 Приоритетный выбор SGW
- Basic **Freq**
- В tac_rules.cfg добавлена возможность указания приоритета в SGW_IP

 Переотправка Create(Attach) на следующий SGW
- Basic **Freq**
- В apn_rules.cfg добавлен параметр SGW_IP, который позволяет связать адреса PGW с предпочтительными адресами SGW

 MME Capacity в зависимости от TAC
- Basic **Freq**
- В mme.cfg добавлен параметр RelativeMME_CapacityForTAC, который позволяет привязать Relative MME Capacity к конкретным TAC

 Error indication unknown-enb-ue-s1ap-id
- Basic **Freq**
- Добавлен сброс контекста по приходу Error Ind

#### 1.34.0.0.209 (2022-03-24)

 Unknown mme ue s1ap id
- Basic **Bug**
- Добавлен сброс старого контекста перед Detach с новым

 Падает приложение при использовании NAS шифрования
- Basic **Bug**
- Исправлено шифрование по алгоритму SNOW3G

 Не пересылаются данные после CP Service Req
- Basic **Bug**
- Исправлено

 HandoverPreparationFailure после релиза контекста
- Basic **Bug**
- Исправлено

 Paging устройств, которые подключены к NB-IOT RAT
- Basic **Freq**
- Добавлена поддержка хранения и передачи UeRadioCapabilityForPaging. Таблица БД обновлена.

#### 1.33.0.0.208 (2022-03-11)

 HTTP-метод для мониторинга S1-покдлючений
- Basic **Freq**
- Добавлен http-метод get_s1_peers

 TAU с причиной -4
- Low **Task**
- Причина уточнена

 Отправка SGsAP-TMSI-REALLOCATION-COMPLETE во время combined attach
- Basic **Bug**
- Исправлено

 Не записался diam cdr AI при неуспешной аутентификации
- Basic **Bug**
- CDR добавлен

 Пул коннекций с БД
- Basic **Freq**
- Добавлен пул соединений на чтение и запись БД

 Запрет переходов с Mapped GUTI из определенных LAC
- Basic **Freq**
- Добавлен параметр ForbiddenLAC

 Сброс контекста после ICS Failure
- Basic **Freq**
- Добавлено, способ сброса определяется параметром ReleaseOnICS_Fail

 Отправлять значение UE-AMBR равное сумме APN-AMBR всех активных APN
- Basic **Bug**
- Исправлено вычисление UE-AMBR

 S1 Handover Unknown UE
- Basic **Bug**
- Исправлено

 Attach в Uplink NAS transport
- Basic **Bug**
- Исправлено

 Журнал подключения и отключения устройств
- Basic **Freq**
- Добавлен новый CDR типа connect_cdr. Он обязательный, его требуется добавить в конфиг.

 Mobile identity - IMSI
- Basic **Task**
- Исправлен тип возвращаемого Mobile identity

 Не создается GTP-С коннекция к новому SGW
- Basic **Bug**
- Исправлено

 Назначение работающего SGW абоненту
- Basic **Freq**
- Добавлено

 ContextReq SL terminated
- Basic **Bug**
- Предупреждения устранены

#### 1.32.0.0.207 (2022-01-31)

 Не запрещать Multiple PDN Connections на MME
- Basic **Freq**
- Добавлена поддержка multiple PDN Connections. Расширено поле PDN_Manager в БД, БД желательно обновить.

 Обновление кодека S1AP до Release 16
- Basic **Task**
- Добавлена поддержка S1AP Release 16

 Формат Diameter-Session Id MME не соответсвует рекомендуемому из RFC6733
- Basic **Freq**
- Формат Diameter-Session Id приведён в соответствие со стандартом

 Передача CHAP параметров в PCO
- Low **Task**
- Параметры добавлены

#### 1.31.1.1.206 (2022-01-10)

 MME не ждет TAU Complete в сценарии, когда выдается TMSI полученный из SGS
- Basic **Freq**
- Добавлен флаг, который выставляется при успешном SGS LOC_UPD_ACC, для последующего перехода в состояние TAU_COMPLETE

 Handover X2 пустые ENB и MME UE Id
- Basic **Bug**
- Исправлено

 S1AP: Unknown UE, eNB UE ID
- Basic **Bug**
- Отбой Extended Service Request при помощи service reject

 Unexpected primitive in OPENED: GTP_C_INCOMING_PEER
- Basic **Bug**
- Исправлено

 SGsAP: Tm_IMEISV: IMEISV has wrong size - 0 symbols, expected 16
- Basic **Bug**
- Добавлено извлечение IMEISV из SGSN Context Response

 Некорректный Mapping GTP Cause в причину отбоя Handover
- Basic **Freq**
- Причина отбоя изменена

 TAU reject при GTP cause: No Resources available
- Basic **Freq**
- Исправлено

 release s1 context после TAU в рамках процедуры X2 HO
- Basic **Bug**
- Исправлено

 Убрать сохранение TEID S11 после Detach
- Basic **Bug**
- Смена S11 TEID после Detach

 release s1 context после TAU в рамках процедуры S1 HO
- Important **Bug**
- Исправлено

 Поддержка Update Bearer Request на MME
- Basic **Freq**
- Добавлен paging при его необходимости

 разрешается Emergency Attach без сконфигурированных Emergency PDN
- Basic **Bug**
- Отбой Emergency Attach если в конфиге нет Emergency PDN

 Релиз S1 контекста при Extended Service Request
- Basic **Freq**
- Исправлено

 Нет освобождения S1-контекста после UE-Initiated Detach
- Basic **Bug**
- Добавил освобождение контекста

 TAU без флага активации биреров иницировал ICS
- Basic **Bug**
- Исправлено

#### 1.31.1.0.201 (2021-11-26)

 Paging для МТ СМС с cs доменом
- Important **Bug**
- Исправлено

 Релиз s1 контекста после TAU
- Basic **Freq**
- Добавлен релиз контекста

 Обработка Create Session Response с Context not Found
- Basic **Bug**
- Исправлено

 TAU MAPPED GUTI Не хватает данных в хранилище
- Basic **Bug**
- Исправлено

 TAU MME-Change Active Flag --> ICS Failure
- Basic **Bug**
- Исправлена обработка Security Key

 Delete Session Request после CLR с MME Update_Procedure
- Basic **Bug**
- Добавлен сброс сессий

 Релиз S1-контекста после Service Req на новой базовке
- Basic **Bug**
- Добавлен сброс старого контекста при приходе Service Req

 TAU Active Flag несколько ICS запросов
- Basic **Freq**
- Исправлено

 Поддержка SGSAP Release Request
- Basic **Bug**
- Добавлена обработка SGSAP Release Request

 GUTI Type в TAU Request
- Basic **Freq**
- Добавлено определение типа GUTI по наличию P-TMSI signature

 Service Reject после ICS Failure ведет к ErrorIndication
- Basic **Bug**
- Service Reject заменён на сброс контекста

 Релиз S1-контекста после Attach-Request
- Basic **Bug**
- Сброс старого контекста и создание нового при Attach Req от абонента в ECM Connected

 MME не удаляет абонентский профиль при отсутствии Detach Accept
- Basic **Bug**
- Удаление профиля при отсутствии Detach Accept

#### 1.23.2.0.131 (2021-08-26)

 Cancel Location не удаляет профиль абонента
- Basic **Bug**
- Сброс SGW-сессий и удаление профиля абонента при приходе Cancel Location с причиной Initial Attach Procedure

#### 1.23.1.0.130 (2021-08-26)

 Запись detach в S1AP CDR для attach отбитого по WhiteIMSI
- Basic **Task**
- Убран CDR об успешном Detach при отказе по белом списку. Изменён формат записи в reject CDR.

#### 1.23.0.0.129 (2021-08-25)

 Обработка Create Session Response с Context not Found
- Basic **Bug**
- Удаление дедикатных биреров при получении их в Modify Bearer Res со статусом Context not found

#### 1.22.4.0.128 (2021-08-18)

 Поддержка Indirect Forwarding для inter-RAT HO
- Basic **Freq**
- Отправка данных SGW при Indirect Forwarding. Сброс Indirect Forwarding Tunnel при отмене HO.

#### 1.22.3.0.127 (2021-08-12)

 ULR после смены MME
- Basic **Freq**
- Отправка ULR с флагом "Skip Subscriber Data" в случае смены MME на соседний при Attach или Service Req

#### 1.22.2.0.126 (2021-08-10)

 Поддержка UE Power Saving Mode
- Basic **Freq**
- Отправка расширенного T3412 только при его наличии в исходном запросе и включённой поддержке PSM

#### 1.22.1.0.125 (2021-08-10)

 Поддержка UE Power Saving Mode
- Basic **Freq**
- В served_plmn.cfg добавлены флаг PSM, разрешающий Power Save Mode и таймер T3324

#### 1.22.0.0.124 (2021-08-09)

 Поддержка UE Power Saving Mode
- Basic **Freq**
- Добавлена поддержка UE Power Saving Mode. Таблица БД обновлена.

#### 1.21.2.0.123 (2021-08-05)

 Неуспешный Service Request при смене MME
- Basic **Bug**
- Корректный учёт UL NAS Count из пришедшего Service Req

#### 1.21.1.0.122 (2021-08-05)

 Добавить в cdr событие по разрыву коннекта с EnodeB
- Basic **Freq**
- Добавлено событие "eNodeB Disconnected"

#### 1.21.0.0.121 (2021-08-04)

 Поддержка Extended PCO
- Basic **Freq**
- Добавлена поддержка Extended PCO. Таблица БД обновлена.

#### 1.20.2.0.119 (2021-08-03)

 APN-O Replacement
- Basic **Freq**
- Использование APN-OI Replacement из ULA для формирования APN

#### 1.20.1.0.118 (2021-07-30)

 Доработка TA CDR
- Basic **Freq**
- В TAU CDR добавлена информация об исходном и конечном местоположениях, а также тип TAU

#### 1.20.0.0.116 (2021-07-29)

 Обработка Network-Access-Mode PS-Only
- Basic **Freq**
- Добавлена поддержка Network-Access-Mode в ULA. Таблица БД обновлена.

#### 1.19.1.0.115 (2021-07-29)

 Поддержка Indirect Forwarding для inter-RAT HO
- Basic **Freq**
- Устранено падение

#### 1.19.0.0.114 (2021-07-28)

 Поддержка Indirect Forwarding для inter-RAT HO
- Basic **Freq**
- Добавлена поддержка Indirect Forwarding для inter-RAT HO. В served_plmn.cfg добавлен параметр IndirectFwd.

#### 1.18.0.0.113 (2021-07-27)

 Добавить HTTP-запрос текущего состояния и местоположения
- Basic **Freq**
- CDR дополнен, документация обновлена

#### 1.17.0.0.112 (2021-07-27)

 Конфигурировать Supported-Features, который отправляется в сторону HSS по S6a
- Basic **Freq**
- В diam_dest.cfg добавлены параметры для настройки Feature-List-ID 1 и Feature-List-ID 2

#### 1.16.4.0.111 (2021-07-23)

 Добавить Cancellation Type и флаги в DIAM CL Cdr
- Basic **Task**
- CDR дополнен, документация обновлена

#### 1.16.3.0.110 (2021-07-23)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- Изменены набор выводимых полей и формат вывода

#### 1.16.2.0.109 (2021-07-23)

 Выставление флага CSFB Indication в Forward Relocation Req для HO вызванного CSFB
- Basic **Freq**
- Выставление флага CSFBI при Cause 'Radio Network - CS Fallback Triggered'

#### 1.16.1.0.108 (2021-07-23)

 Не пересылается Downlink MT данные после Attach
- Basic **Bug**
- Создание GTP-U PCSM для S11-U UL FTEID при загрузке данных из БД

#### 1.16.0.0.107 (2021-07-23)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- Добавлена поддержка HTTP GET запроса get_profile, который позволяет получить данные из профиля абонента

#### 1.15.33.0.105 (2021-07-22)

 Устанавливается флаг DFI в Forward Relocation Request для Inter-RAT HO to UTRAN
- Basic **Bug**
- Установка DFI только при HO вида E-UTRAN->E-UTRAN

#### 1.15.32.0.104 (2021-07-21)

 Не пересылается Downlink трафик после успешного пейджинга
- Basic **Bug**
- Устранена проблема передачи G-PDU по новому соединению после успешной обработки DDN

#### 1.15.31.0.103 (2021-07-21)

 В HandoverCommand не передается "Bearers Subject to Data forwarding list"
- Basic **Bug**
- Добавлена передача RNC F-TEID в Handover Command

#### 1.15.30.0.102 (2021-07-21)

 Не пересылается Downlink трафик после успешного пейджинга
- Basic **Bug**
- Корректная обработка CP Service Request без пользовательских данных

#### 1.15.29.0.101 (2021-07-19)

 DDL ACK c TED=0
- Basic **Bug**
- Вычисление TEID на основе списка биреров в DDN

#### 1.15.28.0.100 (2021-07-14)

 Paging для NB-IOT абонентов без Service Accept
- Basic **Bug**
- Исправлена выдача MME UE ID

#### 1.15.27.0.99 (2021-07-14)

 Paging для NB-IOT абонентов без Service Accept
- Basic **Bug**
- Отправка Service Accept при получении CP Service Req после Paging

#### 1.15.26.0.98 (2021-07-12)

 Control Plane Service Request
- Basic **Freq**
- GTP-U пакет, отправляемый по новому адресу впервые, более не блокируется

#### 1.15.25.0.97 (2021-07-08)

 Emergency bearer services in S1 mode
- Basic **Freq**
- Выставление EMC BS в EPS network feature support

#### 1.15.24.0.96 (2021-07-07)

 Active) S11-U интерфейс на MME
- Important **Freq**
- Доработана поддержка отдельного порта для S11-U

#### 1.15.23.0.95 (2021-07-07)

 ICS поднимает 1 E-RAB контекст из 2-х
- Basic **Bug**
- Теперь ICS после DDN поднимает все известные биреры

#### 1.15.22.0.94 (2021-07-07)

 Active) S11-U интерфейс на MME
- Important **Freq**
- Добавлена настройка адреса и порта для S11-U, порт по-умолчанию 2152

#### 1.15.21.0.93 (2021-07-07)

 Не обрабатываем attach по некоторым абонентам
- Basic **Bug**
- Устранено зависание при попытке отправить DIAM Purge после Detach

#### 1.15.20.0.92 (2021-07-01)

 Не обновили QCI после ISD
- Basic **Bug**
- Обновление QCI при выполнении Paging

#### 1.15.19.0.91 (2021-06-30)

 Поддержка Control Plane CIoT EPS optimization
- Important **Freq**
- В EPS Network Feature Support IE добавлен флаг Control Plane CIoT optimization

#### 1.15.18.0.90 (2021-06-30)

 Опция запроса IMEI в SecurityModeCommand
- Basic **Freq**
- В served_plmn.cfg добавлен флаг RequestIMEISV

#### 1.15.17.0.89 (2021-06-29)

 Падение в core после получения запроса http detach
- Basic **Bug**
- Исправлена ошибка при выводе HTTP CDR

#### 1.15.16.0.88 (2021-06-10)

 Неуспешный PS HO к UTRAN
- Basic **Bug**
- RAN Cause вместо RANAP Cause в Forward Relocation Req

#### 1.15.15.0.87 (2021-06-09)

 Неуспешный PS HO к UTRAN
- Basic **Bug**
- Исправлено кодирование Cause в Fwd Relocation Req

#### 1.15.14.0.86 (2021-06-08)

 Чтение базы данных при обнаружении сбоя MME
- Basic **Freq**
- Добавлено сохранение старого ENB UE ID при загрузке профиля с соседнего ММЕ

#### 1.15.13.0.85 (2021-06-07)

 Неуспешный PS HO к UTRAN
- Basic **Bug**
- Исправлено кодирование APN в Fwd Relocation Req, добавлен приём Handover Required при MT Call

#### 1.15.12.0.84 (2021-06-07)

 Неуспешный PS HO к UTRAN
- Basic **Bug**
- Устранено падение при формировании Forward Relocation Req

#### 1.15.11.0.83 (2021-06-04)

 Неуспешный PS HO к UTRAN
- Basic **Bug**
- Добавлен приём Handover Required при обработке Extended Service Request

#### 1.15.10.0.82 (2021-06-04)

 смена plmn в guti
- Basic **Bug**
- Обновление GUTI при Attach в случае несоответствия PLMN

#### 1.15.9.0.81 (2021-06-04)

 Чтение базы данных при обнаружении сбоя MME
- Basic **Freq**
- Для периодического Echo Req учитывается каждый пропуск ответного Echo Res

#### 1.15.8.0.80 (2021-06-03)

 Чтение базы данных при обнаружении сбоя MME
- Basic **Freq**
- При появлении Service Req от абонента, зарегистрированного на ММЕ из той же группы, делается попытка загрузить его данные из БД

#### 1.15.7.0.79 (2021-05-28)

 Чтение базы данных при обнаружении сбоя MME
- Basic **Freq**
- Добавлена загрузка профилей абонентов, которые были зарегистрированы на упавшем MME

#### 1.15.6.0.78 (2021-05-26)

 GTP Path management
- Basic **Freq**
- Вывод в CDR сообщения о смене удалённого счётчика

#### 1.15.5.0.77 (2021-05-26)

 GTP Path management
- Basic **Freq**
- Файл restart_cnt перенесён в config/.restart_cnt, локальный счётчик сделан единым

#### 1.15.4.0.76 (2021-05-25)

 GTP Path management
- Basic **Freq**
- Добавлен контроль счётчика Recovery с помощью отдельного файла

#### 1.15.3.0.75 (2021-05-25)

 GUTI Reallocation
- Basic **Freq**
- Устранено падение, связанное с таймером GUTI Reallocation

#### 1.15.2.0.71 (2021-05-24)

 S1 CellTrafficTrace
- Basic **Freq**
- Исправлен вывод сообщений, которые не удалось декодировать

#### 1.15.1.0.70 (2021-05-19)

 GUTI Reallocation
- Basic **Freq**
- Запись о неудавшемся GUTI Reallocation больше не выводится в CDR

#### 1.15.0.0.69 (2021-05-14)

 GTP Path management
- Basic **Freq**
- Добавлено отслеживание состояния SGW и других MME

#### 1.14.2.0.68 (2021-05-14)

 Не отправляется Release Access Bearers после UeContextReleaseRequest для CSFB
- Basic **Bug**
- Исправлено сохранение S1 DL FTEID при Attach

#### 1.14.1.0.67 (2021-05-13)

 Для MT вызова отправляется Paging с указанием PS домена
- Basic **Bug**
- Указание CS домена в Paging при MT Call

#### 1.14.0.0.66 (2021-05-11)

 Возможность использования нескольких интерфейсов для GTP-C трафика
- Basic **Freq**
- В gtp_c.cfg возможно указать уникальные адрес и порт для каждого интерфейса

#### 1.13.0.0.65 (2021-04-28)

 GUTI Reallocation
- Basic **Freq**
- Добавлена возможность смены GUTI по времени и по событиям. Настройки в mme.cfg, таблица БД обновлена.

#### 1.12.2.0.63 (2021-04-23)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- Устранено зависание при выполнении HTTP Detach с опцией purge

#### 1.12.1.0.62 (2021-04-23)

 Белые списки IMSI уровне PLMN и на уровне TAC
- Important **Freq**
- В правила imsi_rules.cfg добавлена возможность указать причину отбоя

#### 1.12.0.0.60 (2021-04-22)

 Отказ в регистрации на основании Access-Restirction-Data из профиля абонента
- Basic **Freq**
- Приём Access-Restriction-Data, отказ в Attach, TAU, HO->UTRAN и HO->GERAN при наличии ограничений. Таблица БД обновлена.

#### 1.11.2.0.59 (2021-04-21)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- Отправка Purge на HSS при HTTP detach

#### 1.11.1.0.58 (2021-04-21)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- В HTTP detach добавлены ключи reattach и purge

#### 1.11.0.0.57 (2021-04-21)

 Проработка надежной архитектуры для хранения абонентских контекстов на MME
- Important **Freq**
- Добавлена поддержка HTTP GET запроса для отсоединения абонента от сети. Документация дополнена описаниями HTTP конфига и CDR.

#### 1.10.3.0.56 (2021-04-20)

 Не обрабатывается HandoverCancel
- Basic **Bug**
- Продлено ожидание HO Cancel

#### 1.10.2.0.55 (2021-04-19)

 Не обрабатывается HandoverCancel
- Basic **Bug**
- Устранён пропуск TAU Req от новой базовки после S1 HO.

#### 1.10.1.0.54 (2021-04-16)

 Таймаут освобождения ресурсов при S1 HO IntraLTE
- Basic **Task**
- Исправлена ошибка при создании Kenb

#### 1.10.0.0.53 (2021-04-15)

 Таймаут освобождения ресурсов при S1 HO IntraLTE
- Basic **Task**
- Убрано ожидание TAU Req при S1 HO IntraLTE. В конфиг mme.cfg добавлен параметр S1_HO_ContextRelease, который задаёт таймаут сброса ресурсов исходной базовки.

#### 1.9.6.0.52 (2021-04-13)

 Повторная попытка аутентификации RES != XRES при TMSI
- Basic **Freq**
- При неравенстве RES и XRES запрашивается IMSI. Если IMSI совпадает, то аутентификация завершается с ошибкой, иначе - повторная аутентификация.

#### 1.9.5.0.51 (2021-04-09)

 Разработка S1-интерфейса с eNodeB
- Basic **Freq**
- Добавлена обработка SNMP GET запросов. По параметру ENB.INFO можно получить информацию о зарегистрированных базовках.

#### 1.9.4.0.50 (2021-04-08)

 Handover S1 со сменой SGW
- Basic **Task**
- Корректная обработка ситуации смены Global eNb ID базовкой

#### 1.9.3.0.49 (2021-04-07)

 Правила выбора HSS hostname и realm по IMSI
- Basic **Freq**
- Добавлен конфиг diam_dest.cfg, который позволяет связать DIAM Destination Host и Destination Realm с IMSI

#### 1.9.2.0.48 (2021-04-06)

 Поддержка процедуры Alert через SGs интерфейс
- Basic **Freq**
- Добавлена отправка SGsAP UE Activity Indication. Таблица БД обновлена.

#### 1.9.1.0.47 (2021-04-06)

 Падение в core в рамках процедуры Attach
- Basic **Bug**
- Причина падения устранена

#### 1.9.0.0.46 (2021-04-02)

 Поддержка процедуры Alert через SGs интерфейс
- Basic **Freq**
- Добавлена обработка входящего Alert Req. Таблица БД обновлена.

#### 1.8.3.0.45 (2021-04-01)

 Поддержка Explicit IMSI detach для SGs интерфейса
- Basic **Freq**
- Убрано отправление SGsAP Detach Ind при процедуре Detach, инициированной ММЕ, но без отправки Detach Req

#### 1.8.2.0.44 (2021-04-01)

 В S1AP cdr неуспешный S1Setup записывается с кодом ошибки 0
- Basic **Bug**
- Добавлен код соответствующей ошибки

#### 1.8.1.0.43 (2021-04-01)

 Поддержка Explicit IMSI detach для SGs интерфейса
- Basic **Freq**
- Исправлены коды IE

#### 1.8.0.0.42 (2021-03-29)

 Поддержка Explicit IMSI detach для SGs интерфейса
- Basic **Freq**
- Добавлена поддержка оповещения VLR о detach обоих типов

#### 1.7.0.0.41 (2021-03-25)

 Handover 2g/3g
- Basic **Freq**
- Поддержка Handover вида E-UTRAN <-> GERAN

#### 1.6.11.0.39 (2021-03-25)

 Отправка Paging на абонента, который еще не перешел в idle mode
- Basic **Bug**
- Устранено падение в core

#### 1.6.10.0.38 (2021-03-24)

 Отправка Paging на абонента, который еще не перешел в idle mode
- Basic **Bug**
- Проблема была в создании нового профиля для старого абонента. Решено путём удаления старого профиля при новой регистрации.

#### 1.6.9.0.37 (2021-03-23)

 GBR в initialContextSetupRequest
- Basic **Bug**
- Указание битрейтов для GBR-биреров, при наличии

#### 1.6.8.0.36 (2021-03-19)

 Неуспешные MO, MT SMS через SGs
- Basic **Bug**
- Поддержка частичной передачи multipart MT SMS

#### 1.6.7.0.35 (2021-03-18)

 Неуспешные MO, MT SMS через SGs
- Basic **Bug**
- Добавлена поддержка multipart SMS

#### 1.6.6.0.34 (2021-03-17)

 Deactivate Bearer Unknown E-RAB-ID
- Basic **Bug**
- Бирер удаляется, даже если неизвестен базовке. В CDR прописывается соответствующая ошибка.

#### 1.6.5.0.33 (2021-03-16)

 Некорректное кодирование IMEI в GTP
- Basic **Bug**
- Исправлено кодирование IMEI. В Create Session Req теперь передаётся IMEISV вместо IMEI.

#### 1.6.4.0.32 (2021-03-16)

 Корректировка логики замены неизвестного APN
- Basic **Freq**
- В случае отсутствия APN в запросе MME использует APN по-умолчанию, если он не занят. В ответе абоненту отправляется APN NI.

#### 1.6.3.0.31 (2021-03-16)

 Delete Session Request Operation Indication
- Basic **Freq**
- Исправлена работа с флагом SGWCI

#### 1.6.2.0.30 (2021-03-16)

 Delete Session Request Operation Indication
- Basic **Freq**
- Исправлено выставление флага OI в Indication

#### 1.6.1.0.29 (2021-03-15)

 Смена SGW в процедуре TAU без смены TAC
- Basic **Bug**
- Исправлено. Баг возникал при наличии нескольких адресов для одной пары PLMN и TAC.

#### 1.6.0.0.28 (2021-03-12)

 Handover 2g/3g
- Basic **Freq**
- Поддержка Handover вида E-UTRAN <-> UTRAN

#### 1.5.0.0.27 (2021-03-05)

 Поддержка передачи MO и MT SMS через SGS интерфейс
- Basic **Freq**
- Добавлена поддержка передачи MO и MT SMS через SGS интерфейс. Таблица БД обновлена.

#### 1.4.8.0.26 (2021-03-03)

 Падение при отсутствии связи с БД
- Basic **Bug**
- Проблема была в том, что не срабатывал перехват исключений, решение - добавить флаг -lgcc_s

#### 1.4.7.0.25 (2021-02-25)

 Прерывание Delete Bearer Request
- Basic **Bug**
- Процедура Delete Bearer не может быть прервана

#### 1.4.6.0.24 (2021-02-18)

 EMM Information
- Basic **Freq**
- В NAS IE TZ and Time выставляется время UTC+0

#### 1.4.5.0.23 (2021-02-18)

 Поддержка Control Plane CIoT EPS optimization
- Important **Freq**
- Исправлен вызов AL, выполняющей MT Data Transport

#### 1.4.4.0.22 (2021-02-16)

 2х байтный EPS Network Feature support
- Basic **Task**
- Длина EPS Network Feature support в ответе зависит от длины UE network capability в запросе

#### 1.4.3.0.21 (2021-02-12)

 Поддержка Update Bearer Request
- Basic **Task**
- Исправления в кодах S1AP ESM Cause

#### 1.4.2.0.20 (2021-02-12)

 Поддержка Update Bearer Request
- Basic **Task**
- Добавлена обработка запроса Update Bearer Request от SGW

#### 1.4.1.0.19 (2021-02-12)

 Поддержка Control Plane CIoT EPS optimization
- Important **Freq**
- Исправлено падение в компоненте GTP-C

#### 1.4.0.0.18 (2021-02-12)

 Поддержка Control Plane CIoT EPS optimization
- Important **Freq**
- Добавлена поддержка MO и MT Data Transport in Control Plane CIoT EPS Optimisation with P-GW connectivity.

#### 1.3.1.0.17 (2021-02-01)

 Процедура Reset
- Basic **Freq**
- Добавлена поддержка сброса всего S1 интерфейса (s1-Interface: reset-all)

#### 1.3.0.0.16 (2021-01-29)

 Добавить список поддерживаемых алгоритмов NAS шифрования
- Basic **Freq**
- Добавлена возможность указать списки алгоритмов шифрования и контроля целостности. В таблицу БД внесены изменения, базу нужно обновить.

#### 1.2.5.0.15 (2021-01-27)

 Падение в core
- Basic **Bug**
- Устранено падение по причине отсутствия соединения с БД

#### 1.2.4.0.14 (2021-01-22)

 Падение в core
- Basic **Bug**
- Причина падения устранена

#### 1.2.3.0.13 (2021-01-18)

 S1AP CDR для процедуры Extended Service Req
- Basic **Task**
- Добавлено

#### 1.2.2.0.12 (2021-01-15)

 Реализовать процедуру UE requested bearer resource modification
- Basic **Freq**
- Исправлена обработка отсутствия ответа от SGW

#### 1.2.1.0.11 (2021-01-15)

 Трассировка S1 интерфейса
- Basic **Freq**
- NAS IE выводятся одним сообщением

#### 1.2.0.0.10 (2021-01-15)

 Трассировка S1 интерфейса
- Basic **Freq**
- Добавлен вывод содержимого NAS сообщений

#### 1.1.1.0.9 (2021-01-12)

 Поддержка процедуры получения местоположения абонента
- Basic **Freq**
- Добавлена поддержка флага Current Location Request

#### 1.1.0.0.8 (2021-01-12)

 Поддержка процедуры получения местоположения абонента
- Basic **Freq**
- Добавлено

#### 1.0.4.0.7 (2021-01-11)

 Падает приложение при использовании NAS шифрования
- Basic **Bug**
- Исправлено падение при использовании EEA3 (ZUC)

#### 1.0.3.0.6 (2020-12-25)

 Handover X2 на реальной сети
- Basic **Bug**
- MME ждёт TAU Req только если была смена TA

#### 1.0.2.0.5 (2020-12-24)

 Статистика MME
- Basic **Freq**
- В TAU CDR добавлено поле EMM Cause, добавлены обязательные CDR об успехе Detach и Paging

#### 1.0.1.0.4 (2020-12-23)

 Статистика MME
- Basic **Freq**
- В Attach CDR добавлены поля Attach Type и EMM Cause


#### 1.0.0.0.2 (2020-12-22)

 Версионированная сборка
- Basic **Task**
- Выполнено*/

