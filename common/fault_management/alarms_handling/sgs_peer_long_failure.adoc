---
title: "SGs Peer Long Failure"
description: ""
weight: 20
type: docs
---
:table-caption: Таблица
[[sgs-peer-long-failure]]
== PROTEI MME SGS Peer Long Failure

Полное имя аварии: MME.<NodeName>.Protei_MME.SGSPeerLong

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Одно или несколько SGs-подключений на узле PROTEI MME неактивны в течение 15 минут
.2+a|t1 -- Флаг наличия неактивных интерфейсов SGs +
t2 -- Флаг неактивности всех интерфейсов SGs

|ERROR
|Все SGs-подключения на узле PROTEI MME неактивны в течение 15 минут

|===

.Действия
. Открыть в Grafana график MME Application Metrics::SGSAP::SGsAP State, отражающий состояние подключений по интерфейсу SGs.

или

. Отправить API-запрос *_get_sgs_peers_* для получения информации о состоянии подключений по интерфейсу SGs.

или

. Выполнить cli-команду *_sgs_peers_* для получения информации о состоянии подключений по интерфейсу SGs.
. Определить неактивные узлы и время разрывов.
. Проверить журнал *_/usr/protei/Protei_MME/logs/sgsap_warning.log_* для получения дополнительной информации о причинах разрыва соединений.
. Проверить состояния соединений с неактивными узлами командой `ping`.
