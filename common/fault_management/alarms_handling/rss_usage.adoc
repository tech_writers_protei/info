---
title: "RSS Usage"
description: ""
weight: 20
type: docs
---
:table-caption: Таблица
[[rss-usage]]
== PROTEI MME RSS Usage

Полное имя аварии: MME.<NodeName>.Protei_MME.rss_usage_anomaly

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Потребление оперативной памяти сервисом MME превышает ожидаемое значения не менее, чем в 2 раза.
.2+a|t1 -- Текущий объем занятой оперативной памяти

|ERROR
|Потребление оперативной памяти сервисом MME превышает ожидаемое значения не менее, чем в 3 раза.

|===
