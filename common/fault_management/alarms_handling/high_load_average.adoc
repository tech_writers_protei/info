---
title: "High Load Average"
description: ""
weight: 20
type: docs
---
:table-caption: Таблица
[[high-load-average]]
== PROTEI MME High Load Average

Полное имя аварии: MME.<NodeName>.LoadAverage

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Нагрузка на каждый процессор превышает установленное значение в течение 5 минут
.2+a|t1 -- Средняя нагрузка на каждый узел +
t2 -- Количество процессоров

|ERROR
|Нагрузка на каждый процессор превышает установленное значение в 1,5 раза в течение 5 минут

|===

