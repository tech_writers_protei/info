---
title: "Increasing Swap Usage"
description: ""
weight: 20
type: docs
---
:table-caption: Таблица
[[increasing-swap-usage]]
== PROTEI MME Increasing Swap Usage

Полное имя аварии: MME.<NodeName>.SwapUsage

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Объем использованной памяти подкачки за последние 10 минут узла PROTEI MME увеличился на 200 Мбайт
|t1 -- Скользящее среднее использованной памяти подкачки

|===
