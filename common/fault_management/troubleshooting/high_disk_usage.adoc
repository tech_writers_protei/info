---
title: "Переполнение диска"
description: ""
weight: 10
type: docs
draft: true
---
= Переполнение диска
:source-highlighter: highlightjs
:highlightjs-languages: perl
:experimental:
:icons: font
:toc-title: Содержание
:toc: auto
:solved: Если проблема решена, то зафиксировать выполненные действия согласно внутреннему регламенту.
:logs: Plugins::SafeDelete::logs_remove
:cdr: Plugins::SafeDelete::cdr_remove
:eraser: Plugins::SafeDelete::eraser_logs
:reload: Перезагрузить сервис MME.

== Симптомы

На узле занято практически все пространство жесткого диска.

== Возможные причины

* Файлы логов и xDR не ротируются или архивируются корректно;
* Старые файлы логов и xDR не удаляются корректно;
* Интервал ротации и архивации файлов логов и xDR слишком велик;
* Интервал удаления файлов логов и xDR слишком велик;
* Выделенное пространство слишком мало.

== Процедуры

. Проверить активность скрипта `eraser` в `cron`.
+
[source,shell]
----
$ crontab -l
1 * * * * cd /usr/protei/eraser/; ./start_daemon.sh 1>/dev/null 2>&1
----
+
. Если в выводе команды отсутствует запуск скрипта `/usr/protei/eraser/start_daemon.sh`, то добавить задачу в cron.
+
[source,bash]
----
$ crontab -e
$ 1 * * * * cd /usr/protei/eraser/; ./start_daemon.sh 1>/dev/null 2>&1
----
+
. {reload}
+
[source,shell]
----
$ systemctl restart mme
----
или
+
[source,shell]
----
$ sh /usr/protei/Protei_MME/restart
----
+
. {solved}
. Открыть файл настройки утилиты `eraser`, обрабатывающей лог-файлы и xdr, по пути `/usr/protei/eraser/Config/Setup.pm`.
+
[source,bash]
----
$ nano /usr/protei/eraser/Config/Setup.pm
----
+
[source,perl]
----
our $config =
{
	# lines omitted
	cron_start => 1, # <1>
	plugins =>
	[
		{
			enabled => 1, # <2>
			name => 'Plugins::SafeDelete',
			id => 'logs_remove',
			settings =>
			{
				# lines omitted
				time_delay => 20*24*60*60, # <5>
				commands   =>
				[
					'rm -f %{name}',
					'# removed file %{name}'
				]
			}
		},
		{
			enabled => 1, # <3>
			name => 'Plugins::SafeDelete',
			id => 'cdr_remove',
			settings =>
			{
				time_delay => 90*24*60*60, # <6>
				commands =>
				[
					'rm -f %{name}',
					'# removed file %{name}'
				]
			}
		},
		{
			enabled => 1, # <4>
			name => 'Plugins::SafeDelete',
			id => 'eraser_logs',
			settings =>
			{
				time_delay => 5*24*60*60, # <7>
				commands   =>
				[
				   'rm -f %{name}',
				]
			}
		},
		# lines omitted
	]
};
----
<1> Флаг активации скрипта [[code-1]]
<2> Флаг активации плагина `{logs}` [[code-2]]
<3> Флаг активации плагина `{cdr}` [[code-3]]
<4> Флаг активации плагина `{eraser}` [[code-4]]
<5> Максимальное время жизни файла для плагина `{logs}`, в секундах [[code-5]]
<6> Максимальное время жизни файла для плагина `{cdr}`, в секундах [[code-6]]
<7> Максимальное время жизни файла для плагина `{eraser}`, в секундах [[code-7]]
+
. Проверить, что скрипт активирован, <<code-1,1>>.
. Если скрипт не активирован, то изменить значение в конфигурационном файле.
+
[source,perl]
----
cron_start => 1
----
+
. Проверить, что активированы плагины:

* `{logs}`, <<code-2,2>>
* `{cdr}`, <<code-3,3>>
* `{eraser}`, <<code-4,4>>
. Если плагины не активированы, то изменить значения в конфигурационном файле.
+
[source,perl]
----
enabled => 1
----
+
. Сохранить изменения в файле и закрыть его.
. {reload}
. {solved}
. Уменьшить время хранения файлов для плагинов:
* `{logs}`, <<code-5,5>>
* `{cdr}`, <<code-6,6>>
* `{eraser}`, <<code-7,7>>
+
[source,perl]
----
time_delay => <ms>
----
+
.Пример
[source,perl]
----
time_delay => 24*60*60
----
+
. Сохранить изменения в файле и закрыть его.
. {reload}
. {solved}
. Если проблема не решена, то обратиться в link:../#contacts[службу технической поддержки] ООО "НТЦ ПРОТЕЙ".
