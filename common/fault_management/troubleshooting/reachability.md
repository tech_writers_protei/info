---
title: "Диагностика сетевой связности"
description: ""
weight: 5
type: docs
---

Для проведения диагностики сетевой связности между элементами сети необходимо выполнить следующие шаги:

1. Проверить сетевую доступность узла с другим узлом по адресу `<ip>` с помощью команды `ping`.

   ```bash
   $ ping -c 5 <ip>
   ```

2. Если в полученном ответе нет потерь пакетов, т.е. `0% packet loss`, установленное подключение между элементами сети
   работает.

   ```shell
   $ ping -c 5 8.8.8.8
   PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
   64 bytes from 8.8.8.8: icmp_seq=1 ttl=109 time=4.68 ms
   64 bytes from 8.8.8.8: icmp_seq=2 ttl=109 time=4.70 ms
   64 bytes from 8.8.8.8: icmp_seq=3 ttl=109 time=4.72 ms
   64 bytes from 8.8.8.8: icmp_seq=4 ttl=109 time=4.68 ms
   64 bytes from 8.8.8.8: icmp_seq=5 ttl=109 time=4.69 ms

   --- 8.8.8.8 ping statistics ---
   5 packets transmitted, 5 received, 0% packet loss, time 4008ms
   rtt min/avg/max/mdev = 4.681/4.696/4.719/0.013 ms
   ```

3. Проверить текущий статус сетевого интерфейса, используемого для взаимодействия с целевым узлом.

   ```bash
   $ ip address
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
       link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
       inet 127.0.0.1/8 scope host lo
          valid_lft forever preferred_lft forever
       inet6 ::1/128 scope host
          valid_lft forever preferred_lft forever
   2: enp3s0f0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
       link/ether e4:11:5b:da:23:7c brd ff:ff:ff:ff:ff:ff
       inet 192.168.0.5/17 brd 192.168.127.255 scope global noprefixroute enp3s0f0
           valid_lft forever preferred_lft forever
       inet6 fe80::e611:5bff:feda:237c/64 scope link
           valid_lft forever preferred_lft forever
   ```

4. Если статус сетевого интерфейса -- `DOWN`, активировать его и повторить алгоритм сначала.

   ```bash
   $ ip link set <nic> up
   ```

   ```bash
   $ ip link set enp3s0f0 up
   ```

   **Примечание.** Команда активирует интерфейс только на время текущей сессии.
   После рестарта узла сетевой интерфейс будет деактивирован.
   Рекомендуется привести настройку сетевых интерфейсов на сервере в соответствие со схемой IP-адресации, принятом на объекте эксплуатации.

5. Проверить, что в таблице маршрутизации указан маршрут к целевому узлу.

    ```shell
    $ route -n
    Kernel IP routing table
    Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
    0.0.0.0         192.168.0.254   0.0.0.0         UG    101    0        0 enp3s0f0
    192.168.0.0     0.0.0.0         255.255.128.0   U     101    0        0 enp3s0f0
    192.168.122.0   0.0.0.0         255.255.255.0   U     0      0        0 virbr0
    ```

6. Если маршрут отсутствует, добавить его в конфигурацию сервера, перезагрузить сетевой менеджер и повторить
   алгоритм сначала.

   ```bash
   $ nmcli connection modify <nic> +ipv4.routes "<network_ip> <gateway_ip>"
   $ nmcli connection reload
   ```

   ```bash
   $ nmcli connection modify enp3s0f0 +ipv4.routes "127.0.0.1 192.168.0.1"
   $ nmcli connection reload
   ```

7. Провести диагностику неполадок на сетевом пути к целевому узлу.

   ```bash
   $ traceroute -d <ip>
   ```

   ```shell
   $ traceroute -d 8.8.8.8
   traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
    1  192.168.0.1 (192.168.0.1)  0.163 ms  0.124 ms  0.157 ms
    2  5.178.81.254 (5.178.81.254)  0.499 ms  0.476 ms  0.455 ms
    3  92.53.93.91 (92.53.93.91)  1.225 ms  1.082 ms  1.093 ms
    4  109.239.137.237 (109.239.137.237)  1.235 ms  1.212 ms  1.190 ms
    5  74.125.244.181 (74.125.244.181)  1.264 ms 74.125.244.133 (74.125.244.133)  1.227 ms 74.125.244.181 (74.125.244.181)  1.196 ms
    6  142.251.51.187 (142.251.51.187)  12.586 ms  12.886 ms  12.739 ms
    7  72.14.232.191 (72.14.232.191)  11.614 ms 216.239.54.201 (216.239.54.201)  13.349 ms 172.253.70.51 (172.253.70.51)  13.022 ms
    8  * 172.253.66.108 (172.253.66.108)  11.487 ms 172.253.66.110 (172.253.66.110)  5.334 ms
    9  * * *
   10  * * *
   11  * * *
   12  * * *
   13  * * *
   14  * * *
   15  * * *
   16  * * *
   17  dns.google (8.8.8.8)  4.595 ms *  4.539 ms
   ```

8. На основе вывода определить сбойный участок сети.
9. Если проблема не решена, то обратиться в [службу технической поддержки](../#contacts) ООО "НТЦ ПРОТЕЙ".
