---
title: "LTE Handover EDR"
description: "Log File lte_handover_cdr"
weight: 20
type: docs
---
:asciidoctorconfigdir: ../../..
:experimantal:
:icons: font
:toc: right
:table-caption: Table
:source-highlighter: highlightjs


ifeval::["{backend}" == "pdf"]
:nbiot: NB-IoT
:tac: TAC
endif::[]

ifeval::["{backend}" != "pdf"]
:nbiot: pass:q[<abbr title="Narrow Band Internet of Things">NB-IoT</abbr>]
:tac: pass:q[<abbr title="Tracking Area Code">TAC</abbr>]
endif::[]

[[lte-ho-edr]]
= LTE Handover EDR

.Log Fields
[options="header",cols="1,4,13,2"%breakable]
|===
|N |Field |Description |Type

|1 |DateTime |Event date and time. Format: +
`YYYY-MM-DD HH:MM:SS`. |datetime
|2 |<<event-lte,Event>> |Event or procedure to generate the record. |string
|3 |eNodeB UE ID |S1 context identifier in the source eNodeB. |int
|4 |Target eNodeB UE |S1 context identifier in the destination eNodeB. |int
|5 |MME UE ID |S1 context identifier in the MME node. |int
|6 |IMSI |Subscriber IMSI number. |string
|7 |MSISDN |Subscriber MSISDN number. |string
|8 |GUTI |Globally unique temporary identifier. |string
|9 |IMEI |Device IMEI number. |string
|10 |Source PLMN |Source PLMN identifier. |string
|11 |Source TAC |Source tracking area code. |string
|12 |Source CellID |Source cell identifier. |int/hex
|13 |Source eNodeB IP |IP address of the source eNodeB. |ip
|14 |Source IP SGW |IP address of the source SGW node. |ip
|15 |Target PLMN |Destination PLMN identifier. |string
|16 |Target TAC |Destination tracking area code. |string
|17 |Target CellID |Destination cell identifier. |int/hex
|18 |Target eNodeB IP |IP address of the destination eNodeB. |ip
|19 |Target IP SGW |IP address of the destination SGW node. |ip
|20 |Duration |Procedure duration, in milliseconds. |int
|21 |Procedure Type:ErrorCode |Procedure type + MME link:../error_code/#error-codes[internal result code]. Format: +
`<procedure_type>:<error_code>`. +
`1` -- <<handover_s1,Handover S1>>; +
`2` -- <<handover_x2,Handover X2>>. |object
|22 |Target eNodeB ID |Destination eNodeB identifier. |int/hex
|===

[[event-lte]]
[discrete]
== Event

* <<handover_s1,Handover S1>>;
* <<handover_x2,Handover X2>>.

<<<

[[handover_s1]]
== Handover S1

[source,log%unbreakable]
----
DateTime,S1,Source eNodeB UE ID,Target eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,
Source PLMN,Source TAC,Source CellID,Source eNodeB IP,Source IP SGW,Target PLMN,
Target TAC,Target CellID,Target eNodeB IP,Target IP SGW,Duration,1:ErrorCode,
Target eNodeB ID
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Destination PLMN identifier is forbidden.
|2 |IP address of the destination MME is not found.
|3 |Subscriber is in the state ECM-IDLE.
|4 |S1AP: HANDOVER FAILURE message has been received.
|5 |S1AP: HANDOVER CANCEL message has been received.
|6 |Destination MME rejected the requets to register a subscriber.
|7 |Destination SGW rejected the request to create a session.
|8 |Destination eNodeB accepted no default bearer services.
|9 |Destination eNodeB has not sent the S1AP: HANDOVER REQUEST ACKNOWLEDGE.
|10 |Destination MME has not sent the GTPv2-C: Forward Access Context Acknowledge.
|11 |Destination eNodeB has not sent the S1AP: HANDOVER NOTIFY.
|12 |Destination MME has not sent the GTPv2-C: Forward Relocation Complete Notification.
|13 |eNodeB has not sent the S1AP: UE CONTEXT RELEASE COMPLETE.
|14 |Failed to clear sessions in the SGW node.
|15 |Attempt to provide handover from the {nbiot} {tac}.
|16 |Failed to handle the Context-Request message.
|17 |Specified IMSI is forbidden in the destination PLMN.
|18 |Destination eNodeB cleared the S1 context.
|===

[[handover_x2]]
== Handover X2

[source,log%unbreakable]
----
DateTime,X2,Source eNodeB UE ID,Target eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,IMEI,
Source PLMN,Source TAC,Source CellID,Source eNodeB IP,Source IP SGW,Target PLMN,
Target TAC,Target CellID,Target eNodeB IP,Target IP SGW,Duration,2:ErrorCode,
Target eNodeB ID
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |No objects to create on the new SGW node.
|2 |No objects to modify on the new SGW node.
|3 |Failed to create any object on the new SGW node.
|4 |Failed to modify any object on the previous SGW node.
|===