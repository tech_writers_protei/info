---
title: "MME Error codes"
description: "Global and Local MME Error Codes"
weight: 30
type: docs
---
:asciidoctorconfigdir: ../../..
:experimantal:
:icons: font
:toc: right
:table-caption: Table
:source-highlighter: highlightjs


[[error-codes]]
= MME Error codes

The `ErrorCode` field provides the procedure execution result formatted as following:

<<global,`<global_error_code>`>>[:<<local,`<local_error_code>`>>]

[[global]]
Global error codes are uniform for all procedures.

.Global error codes
[options="header",cols="1,12"%breakable]
|===
|Code |Description

|-7 |No free auxilary logics, AL.
|-6 |Network failure to send a message.
|-5 |Incoming message is missing required data.
|-4 |Storage data gap.
|[[third]]-3 |Interrupted by another procedure.
|-2 |Response timeout expired.
|-1 |Logic failure.
|0 |Successful execution.
|===

NOTE: On success, the ErrorCode is set to *0*, with no local error code.

[[local]]
Local error codes depend on the procedure and are given in the EDR descriptions, except for <<third,code -3>>.

.Local codes for the global *-3*
[options="header",cols="1,12"%breakable]
|===
|Code |Description

|1 |Profile waiting queue is not empty, but the procedure requires the immediate implementation.
|2 |Interrupted by another procedure.
|3 |Key for the request to the storage is deleted.
|4 |Requested profile is deleted.
|5 |Profile waiting queue overflow.
|===