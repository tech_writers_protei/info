---
title: "S1AP EDR"
description: "Log File s1ap_cdr"
weight: 20
type: docs
---
:asciidoctorconfigdir: ../../..
:experimantal:
:icons: font
:toc: right
:table-caption: Table
:source-highlighter: highlightjs


ifeval::["{backend}" == "pdf"]
:kasme: KASME
:mac: MAC
:emm: EMM
endif::[]

ifeval::["{backend}" != "pdf"]
:kasme: pass:q[<abbr title="Key Access Security Management Entity">`KASME`</abbr>]
:mac: pass:q[<abbr title="Message Authentication Code">MAC</abbr>]
:emm: pass:q[<abbr title="EPC Mobility Management">EMM</abbr>]
endif::[]

[[s1ap-edr]]
= S1AP EDR

.Log Fields
[options="header",cols="1,4,13,2"%breakable]
|===
|N |Field |Description |Type

|1 |DateTime |Event date and time. Format: +
`YYYY-MM-DD HH:MM:SS`. |datetime
|2 |<<event-s1ap,Event>> |Event or procedure to generate the record. |string
|3 |eNodeB UE ID |S1 context identifier in the eNodeB. |int
|4 |MME UE ID |S1 context identifier in the MME node. |int
|5 |IMSI |Subscriber IMSI number. |string
|6 |MSISDN |Subscriber MSISDN number. |string
|7 |GUTI |Globally unique temporary identifier. |string
|8 |PLMN |PLMN identifier. |string
|9 |CellID |Cell identifier. |int/hex
|10 |IMEI |Device IMEI number. |string
|11 |Duration |Procedure duration, in milliseconds. |int
|12 |ErrorCode |MME link:../error_code/#error-codes[internal result code]. |int
|===

[[event-s1ap]]
[discrete]
== Event

* <<authentication,Authentication>>;
* <<bearer_resource_modification,Bearer Resource Modification>>;
* <<deactivate_bearer,Deactivate Bearer>>;
* <<e_rab_modification,E-RAB Modification>>;
* <<guti_reallocation,GUTI Reallocation>>;
* <<initial_context_setup,Initial Context Setup>>;
* <<mt_data_transport,MT Data Transport>>;
* <<mo_data_transport,MO Data Transport>>;
* <<modify_eps_bearer_context,Modify EPS bearer context>>;
* <<pdn_disconnection-s1ap,PDN Disconnection>>;
* <<secondary_rat_data_usage_report,Secondary RAT Data Usage Report>>;
* <<ue_context_modification,UE Context Modification>>;
* <<trace_failure_indication,Trace Failure Indication>>.

<<<

[[authentication]]
== Authentication

[source,log%unbreakable]
----
DateTime,Authentication,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Unsuccessful Diameter: Authentication-Information-Answer message has been received.
|2 |Diameter: Authentication-Information-Answer is missing the authentication credentials.
|3 |Parsed {kasme} value is empty.
|4 |S1 Security Mode Reject message has been received.
|5 |Impossible to handle the S1 Identity Response to get the IMSI number.
|6 |Received `RES` value mismatches the expected `XRES` one.
|7 |{mac} failed.
|8 |Failed to distinguish the ciphering algorithm.
|9 |Failed to distinguish the integrity check algorithm.
|10 |`UE-Usage-Type` value is illegal.
|===

[[bearer_resource_modification]]
== Bearer Resource Modification

[source,log%unbreakable]
----
DateTime,Bearer Resource Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,
CellID,IMEI,Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Subscriber is in the state {emm}-IDLE.
|2 |Bearer service is not specified.
|3 |Requested bearer service is not found.
|4 |Reject from the SGW node has been received.
|5 |Auxilary logic failed.
|===

[[deactivate_bearer]]
== Deactivate Bearer

[source,log%unbreakable]
----
DateTime,Deactivate Bearer,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|7 |Paging procedure failed.
|8 |eNodeB sent a reject message.
|9 |Requested bearer service is not found.
|===

<<<

[[e_rab_modification]]
== E-RAB Modification

[source,log%unbreakable]
----
DateTime,E-RAB Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Free bearer service appropriate for modification is not found.
|===

[[guti_reallocation]]
== GUTI Reallocation

[source,log%unbreakable]
----
DateTime,GUTI Reallocation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

[[initial_context_setup]]
== Initial Context Setup

[source,log%unbreakable]
----
DateTime,Initial Context Setup,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,
IMEI,CauseType,CauseReason,Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |No free bearer service to activate.
|2 |ICS Failure message has been received.
|===

[[mt_data_transport]]
== MT Data Transport

[source,log%unbreakable]
----
DateTime,MT Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

[[mo_data_transport]]
== MO Data Transport

[source,log%unbreakable]
----
DateTime,MO Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,
Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Requested bearer service is not found.
|2 |Failed to modify the bearer service.
|3 |`User Data Container` value is missing.
|===

[[modify_eps_bearer_context]]
== Modify EPS Bearer Context

[source,log%unbreakable]
----
DateTime,Modify EPS Bearer Context,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,
CellID,IMEI,Bearer ID,Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|11 |eNodeB rejected the bearer service.
|12 |UE rejected the bearer service.
|===

<<<

[[pdn_disconnection-s1ap]]
== PDN Disconnection

[source,log%unbreakable]
----
DateTime,PDN Disconnection,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,APN,
IMEI,Duration,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Failed to decode the PDN Disconnect Request.
|2 |Failed to handle the PDN Disconnect Request to get the bearer service.
|===

[[secondary_rat_data_usage_report]]
== Secondary RAT Data Usage Report

[source,log%unbreakable]
----
DateTime,Secondary RAT Data Usage Report,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,
CellID,IMEI,Duration,ErrorCode
----

[[ue_context_modification]]
== UE Context Modification

[source,log%unbreakable]
----
DateTime,UE Context Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,
IMEI,Duration,ErrorCode
----

[[trace_failure_indication]]
== Trace Failure Indication

[source,log%unbreakable]
----
DateTime,Trace Failure Indication,ENB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,
IMEI,Duration,ErrorCode
----