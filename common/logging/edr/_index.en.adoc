---
title: "EDR"
description: "EDR Log File Description"
weight: 10
type: docs
---
:asciidoctorconfigdir: ../../..
:experimantal:
:icons: font
:toc: right
:table-caption: Table
:source-highlighter: highlightjs


The section provides a description of the EDR log files.
The default folder to store EDR files is `/usr/protei/cdr/Protei_MME/`.