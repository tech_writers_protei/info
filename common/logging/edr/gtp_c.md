---
title: "GTP-C EDR"
description: "Журнал gtp_c_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N  | Поле                  | Описание                                                                                                                                           | Тип      |
|----|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1  | DateTime              | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                            | datetime |
| 2  | [Event](#event-gtp-c) | Событие или процедура, сформировавшая запись.                                                                                                      | string   |
| 3  | IMSI                  | Номер IMSI абонента.                                                                                                                               | string   |
| 4  | MSISDN                | Номер MSISDN абонента.                                                                                                                             | string   |
| 5  | GUTI                  | Глобальный уникальный временный идентификатор абонента.                                                                                            | string   |
| 6  | PLMN                  | Идентификатор <abbr title="Public Landing Mobile Network">PLMN</abbr>.                                                                             | string   |
| 7  | CellID                | Идентификатор соты.                                                                                                                                | int/hex  |
| 8  | IMEI                  | Номер IMEI устройства.                                                                                                                             | string   |
| 9  | APN                   | Имя точки доступа.                                                                                                                                 | string   |
| 10 | SGW IP                | IP-адрес узла SGW.                                                                                                                                 | ip       |
| 11 | SGW TEID              | Идентификатор конечной точки туннеля на узле SGW.                                                                                                  | string   |
| 12 | Self TEID             | Идентификатор конечной точки туннеля на узле MME.                                                                                                  | string   |
| 13 | Cause                 | Код причины разрыва соединения. См. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf). | int      |
| 14 | Duration              | Длительность процедуры, в миллисекундах.                                                                                                           | int      |
| 15 | ErrorCode             | Внутренний [код MME](../error_code/) результата.                                                                                                   | object   |

#### События и процедуры, Event {#event-gtp-c}

- [Create Indirect Data Forwarding Tunnel](#create_indirect_data_forwarding_tunnel);
- [Create Session Response](#create_session_response);
- [Connection Lost](#connection_lost);
- [Context](#context);
- [Detach Notification](#detach_notification);
- [Delete Bearer Request](#delete_bearer_request);
- [Delete Indirect Data Forwarding Tunnel](#delete_indirect_data_forwarding_tunnel);
- [Delete Session Response](#delete_session_response);
- [Downlink Data Notification](#downlink_data_notification);
- [Forward Relocation Request MME](#forward_relocation_request_mme);
- [Modify Bearer Response](#modify_bearer_response);
- [PGW Restart Notification](#pgw_restart_notification);
- [Release Access Bearers Response](#release_access_bearers_response);
- [Remote Restart](#remote_restart);
- [SGSN Context](#sgsn_context);
- [Identification Request](#identification_request);
- [Update Bearer Request](#update_bearer_request);
- [UE Activity Notification](#ue_activity_notification);
- [Suspend](#suspend);
- [Resume](#resume);

#### Create Indirect Data Forwarding Tunnel {#create_indirect_data_forwarding_tunnel}

```log
DateTime,Create Indirect Data Forwarding Tunnel,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Create Session Response {#create_session_response}

```log
DateTime,Create Session,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Connection Lost {#connection_lost}

```log
DateTime,Connection Lost,IP
```

#### Context {#context}

```log
DateTime,Context,IMSI,MSISDN,GUTI,PLMN,IMEI,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                |
|---|---------------------------------------------------------|
| 1 | Не удалось декодировать сообщение GTP: TAU-Request.     |
| 2 | Сообщение TAU-Request не прошло проверку целостности.   |
| 3 | Получен отказ в сообщении GTP: Context-Acknowledgement. |

#### Detach Notification {#detach_notification}

```log
DateTime,Detach Notification,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Delete Bearer Request {#delete_bearer_request}

```log
DateTime,Delete Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                           |
|---|------------------------------------------------------------------------------------|
| 1 | Нет данных о PDN-соединениях абонента.                                             |
| 2 | Ошибка сброса контекста.                                                           |
| 3 | Устройство абонента находится в режиме <abbr title="Power Saving Mode">PSM</abbr>. |
| 4 | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен.                       |
| 5 | Абонент недоступен для PS-сетей.                                                   |
| 6 | Получено сообщение GTPv2-C: Failure Indication от узла SGW.                        |
| 7 | Ошибка процедуры Paging.                                                           |
| 8 | Получен отказ деактивации от базовой станции.                                      |
| 9 | Указанная bearer-служба не найдена.                                                |

#### Delete Indirect Data Forwarding Tunnel {#delete_indirect_data_forwarding_tunnel}

```log
DateTime,Delete Indirect Data Forwarding Tunnel,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Delete Session Response {#delete_session_response}

```log
DateTime,Delete Session,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                 |
|---|----------------------------------------------------------|
| 1 | Для данной сессии не указана bearer-служба по умолчанию. |

#### Downlink Data Notification {#downlink_data_notification}

```log
DateTime,Downlink Data,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                              |
|---|---------------------------------------------------------------------------------------|
| 1 | <abbr title="Globally Unique Temporary UE Identity">GUTI</abbr> абонента неизвестен.  |
| 2 | Нет данных о PDN-соединениях абонента.                                                |
| 3 | Ошибка процедуры Paging.                                                              |
| 4 | Ошибка процедуры Service Request.                                                     |
| 5 | Отказ SGW в модификации bearer-службы.                                                |
| 6 | Устройство абонента находится в состоянии <abbr title="Power Saving Mode">PSM</abbr>. |
| 7 | Флаг <abbr title="Paging Proceed Flag">PPF</abbr> уже сброшен.                        |
| 8 | Абонент недоступен для PS-сетей.                                                      |
| 9 | Абонент находится в состоянии Suspension.                                             |

#### Forward Relocation Request MME {#forward_relocation_request_mme}

```log
DateTime,FwdRelocationMME,IMSI,MSISDN,GUTI,Target_PLMN,Target_TAC,Target_Cell_Id,IMEI,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N  | Описание                                                                                                                                  |
|----|-------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | Получено сообщение GTP-C: Relocation Cancel Request.                                                                                      |
| 2  | Узел SGW назначения не принял запрос на создание сессии.                                                                                  |
| 3  | Нет данных о eNodeB назначения.                                                                                                           |
| 4  | Получено сообщение S1AP: HANDOVER FAILURE.                                                                                                |
| 5  | eNodeB не приняла ни одной bearer-службы по умолчанию.                                                                                    |
| 6  | Не получено сообщение S1AP: HANDOVER NOTIFY.                                                                                              |
| 7  | Не найдено сообщение GTPv2-C: Forward Relocation Complete Acknowledge.                                                                    |
| 8  | Ошибка процедуры GTPv2-C: Modify Bearer.                                                                                                  |
| 9  | Не удалось извлечь идентификаторы IMSI, <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr>.       |
| 10 | В сообщении GTPv2-C: Forward Relocation Request не указан <abbr title="Next Hop Indicator">NHI</abbr>.                                    |
| 11 | Попытка хэндовера из области <abbr title="Tracking Area Code">TAC</abbr> сети <abbr title="Narrow Band Internet of Things">NB-IoT</abbr>. |

#### Modify Bearer Response {#modify_bearer_response}

```log
DateTime,Modify Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### PGW Restart Notification {#pgw_restart_notification}

```log
DateTime,PGW Restart Notification,SGW IP,PGW IP
```

#### Release Access Bearers Response {#release_access_bearers_response}

```log
DateTime,Release Access Bearers,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Remote Restart {#remote_restart}

```log
DateTime,Remote Restart,IP,Counter
```

#### SGSN Context {#sgsn_context}

```log
DateTime,SGSN Context,IMSI,MSISDN,GUTI,PLMN,IMEI,Cause,Duration,ErrorCode
```

| N | Описание               |
|---|------------------------|
| 1 | Ошибка аутентификации. |

#### Identification Request {#identification_request}

```log
DateTime,Identification,IMSI,MSISDN,GUTI,Target_PLMN,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                    |
|---|-------------------------------------------------------------|
| 1 | Не удалось декодировать сообщение S1 Attach Request.        |
| 2 | Сообщение S1 Attach Request не прошло проверку целостности. |

#### Update Bearer Request {#update_bearer_request}

```log
DateTime,Update Bearer,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                              |
|---|---------------------------------------------------------------------------------------|
| 1 | Для указанного абонента не разрешены PDN Connectivity.                                |
| 2 | В запросе SGW не указаны bearer-службы.                                               |
| 3 | Вместо ожидаемого сообщения GTP-C: Update Bearer Request получено другое.             |
| 4 | Запрошенные bearer-службы не найдены на узле MME.                                     |
| 5 | Несовпадение идентификаторов <abbr title="Procedure Transaction ID">`PTI`</abbr>.     |
| 6 | Устройство абонента находится в состоянии <abbr title="Power Saving Mode">PSM</abbr>. |
| 7 | Во время процесса Paging не получен ответ от абонента.                                |
| 8 | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен.                          |
| 9 | Абонент недоступен для PS-сетей.                                                      |

#### UE Activity Notification {#ue_activity_notification}

```log
DateTime,UE Activity Notification,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Suspend {#suspend}

```log
DateTime,Suspend,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```

#### Resume {#resume}

```log
DateTime,Resume,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,APN,SGW IP,SGW TEID,Self TEID,Cause,Duration,ErrorCode
```