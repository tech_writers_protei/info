---
title: "Connect EDR"
description: "Журнал connect_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N  | Поле                    | Описание                                                                                                                                                                                                                                                             | Тип      |
|----|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1  | DateTime                | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`.                                                                                                                                                                                                              | datetime |
| 2  | [Event](#event-connect) | Событие или процедура, сформировавшая запись.                                                                                                                                                                                                                        | string   |
| 3  | Trigger                 | Сторона, инициировавшая процедуру.<br>`UE` / `HSS` / `SPGW` / `MME` / `HTTP`.                                                                                                                                                                                        | string   |
| 4  | eNodeB UE ID            | Идентификатор S1-контекста на стороне исходной eNodeB.                                                                                                                                                                                                               | int      |
| 5  | MME UE ID               | Идентификатор S1-контекста на узле MME.                                                                                                                                                                                                                              | int      |
| 6  | IMSI                    | Номер IMSI абонента.                                                                                                                                                                                                                                                 | string   |
| 7  | MSISDN                  | Номер MSISDN абонента.                                                                                                                                                                                                                                               | string   |
| 8  | GUTI                    | Глобальный уникальный временный идентификатор абонента.                                                                                                                                                                                                              | string   |
| 9  | PLMN                    | Идентификатор <abbr title="Public Landing Mobile Network">PLMN</abbr>.                                                                                                                                                                                               | string   |
| 10 | TAC                     | Код области отслеживания.                                                                                                                                                                                                                                            | string   |
| 11 | CellID                  | Идентификатор соты.                                                                                                                                                                                                                                                  | int/hex  |
| 12 | Requested APN           | <abbr title="Access Point Name">APN</abbr>, полученный от UE.                                                                                                                                                                                                        | string   |
| 13 | Used APN                | <abbr title="Access Point Name">APN</abbr>, активировавший PDN.                                                                                                                                                                                                      | string   |
| 14 | PDN Type                | Тип подключения к сети передачи данных.<br>`IPv4` / `IPv6` / `IPv4v6` / `IPv4_or_IPv6`.                                                                                                                                                                              | string   |
| 15 | QCI                     | Идентификатор класса QoS. См. [3GPP TS 23.203](https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf).<br>**Примечание.** Для процедуры [Dedicated Bearer Deactivation](../dedicated_bearer/#dedicated_bearer_deactivation) 0. | int      |
| 16 | IMEISV                  | Номер IMEISV абонента.                                                                                                                                                                                                                                               | string   |
| 17 | Attach Type             | Код типа сообщения Attach.<br>`cmb` -- Combined, EPS + CS 2G/3G;<br>`eps` -- только EPS;<br>`emr` -- Emergency.<br>**Примечание.** Заполняется только для события [Attach](#attach).                                                                                 | string   |
| 18 | EMM Cause               | Причина завершения вызова в LTE-сетях, `EMM Cause`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).                                                                                               | int      |
| 19 | Duration                | Длительность процедуры, в миллисекундах.                                                                                                                                                                                                                             | int      |
| 20 | ErrorCode               | Внутренний [код MME](../error_code/) результата.                                                                                                                                                                                                                     | int      |

#### События и процедуры, Event {#event-connect}

- [Attach](#attach);
- [PDN Connectivity Request](#pdn_connectivity_request);
- [PDN Disconnection](#pdn_disconnection);
- [Detach](#detach-connect).

#### Attach {#attach}

```log
DateTime,Attach,Trigger,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,TAC,Cell ID,Requested APN,Used APN,PDN Type,QCI,IMEISV,Attach Type,EMM Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N  | Описание                                                                                                                                                                                    |
|----|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | Не удалось получить значение SGW IP, необходимое для отправки запроса GTP: Create Session Request.                                                                                          |
| 2  | Ошибка аутентификации.                                                                                                                                                                      |
| 3  | Ошибка обработки сообщения Update-Location-Request. См. записи в [DIAM EDR](../diam/).                                                                                                      |
| 5  | Получен запрос Detach от абонента, запрос успешно обработан.                                                                                                                                |
| 6  | Получен запрос Detach от абонента, в процессе обработки возникла ошибка.                                                                                                                    |
| 7  | Невозможно отправить запрос ICS Request ввиду отсутствия значения <abbr title="Key Access Security Management Entity">KASME</abbr> или KeNB.                                                |
| 8  | Невозможно получить значения NAS-полей из сообщения S1 Attach Request.                                                                                                                      |
| 9  | Невозможно получить значение `EPS Mobile Identity` из сообщения S1 Attach Request.                                                                                                          |
| 10 | Невозможно получить значение `IMSI` из сообщения S1 Identity Response.                                                                                                                      |
| 11 | Получено неожидаемое сообщение по протоколу Diameter вместо сообщения Diameter: ME-Identity-Check-Answer.                                                                                   |
| 12 | Не удалось получить значение `Equipment-Status` из сообщения Diameter: ME-Identity-Check-Answer.                                                                                            |
| 13 | ME обнаружен в черном списке узла EIR.                                                                                                                                                      |
| 14 | ME обнаружен в сером списке узла EIR.                                                                                                                                                       |
| 15 | Несовпадение типа PDN, запрошенного абонентом, и того, что был получен от узла HSS в сообщении Diameter: Update-Location-Answer и/или от узла SGW в сообщении GTP: Create Session Response. |
| 16 | Невозможно выбрать APN: запрошенный APN не найден, APN по умолчанию запрещен.                                                                                                               |
| 17 | Получен отказ в сообщении GTP: Create Session Response. См. записи в [GTP-C EDR](../gtp_c/).                                                                                                |
| 18 | Получено сообщение S1AP: INITIAL CONTEXT SETUP FAILURE/E-RAB MODIFY RESPONSE с ошибкой модификации bearer-службы.                                                                           |
| 19 | Устройство абонента не поддерживает запрос S1 Attach Without PDN Connectivity, при этом запрос S1 Attach Request отправлен без сообщения S1 PDN Connectivity Request.                       |
| 20 | Процедура Location Update для не-EPS служб не успешна.                                                                                                                                      |
| 21 | IMSI не найден в белом списке.                                                                                                                                                              |
| 22 | Запрет, заданный оператором; Operator Determined Barring.                                                                                                                                   |
| 23 | Не получено сообщение S1AP: INITIAL CONTEXT SETUP RESPONSE/E-RAB MODIFY RESPONSE.                                                                                                           |
| 24 | Не получено сообщение S1AP: UE CAPABILITY INFORMATION INDICATION.                                                                                                                           |
| 25 | Не получено сообщение S1 Attach Complete.                                                                                                                                                   |
| 26 | Не удалось получить значение PGW IP, необходимое для отправки запроса GTP: Create Session Request.                                                                                          |
| 27 | Ошибка ввиду не сконфигурированных Emergency PDN, либо не найден PDN с запрошенным APN.                                                                                                     |
| 28 | Не получено сообщение GTP: Modify Bearer Response.                                                                                                                                          |
| 29 | Не удалось добавить поле APN в сообщение S1 Activate Default EPS Bearer Context Request.                                                                                                    |
| 30 | Недопустимое значение `UE-Usage-Type`.                                                                                                                                                      |
| 31 | IMEI не найден в базе узла EIR.                                                                                                                                                             |
| 33 | В профиле абонента со старого ММЕ указан недопустимый алгоритм шифрования/целостности.                                                                                                      |

#### PDN Connectivity Request {#pdn_connectivity_request}

```log
DateTime,PDN Connectivity Req,Trigger,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,TAC,Cell ID,Requested APN,Used APN,PDN Type,QCI,IMEISV,Attach Type,EMM Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N  | Описание                                                                                                         |
|----|------------------------------------------------------------------------------------------------------------------|
| 1  | Не удалось декодировать сообщение S1 PDN Connectivity Request.                                                   |
| 2  | В сообщении S1 PDN Connectivity Request не задано значение `APN`.                                                |
| 3  | Запрошен недопустимый тип PDN: разрешен только IPv4.                                                             |
| 4  | Запрошен недопустимый тип PDN: разрешен только IPv6.                                                             |
| 5  | Запрошен недопустимый тип PDN.                                                                                   |
| 6  | Получен отказ в сообщении GTP: Create Session Response. См. записи в [GTP-C EDR](../gtp_c/).                     |
| 7  | UE отклонило запрос на создание bearer-службы. Получено сообщение S1 Activate Default EPS Bearer Context Reject. |
| 8  | Для этой сети PLMN не определены Emergency PDN.                                                                  |
| 9  | Для данного абонента запрещены PDN Connectivity.                                                                 |
| 10 | eNodeB отклонила запрос на создание bearer-службы.                                                               |
| 11 | Запрет, заданный оператором; Operator Determined Barring.                                                        |
| 12 | Создание PDN Connectivity для экстренно зарегистрированного абонента запрещено.                                  |
| 13 | Не удалось получить значение `PGW IP`.                                                                           |
| 14 | Достигнуто максимальное допустимое количество bearer-служб.                                                      |
| 15 | Не найдено свободное APN для Emergency PDN Connectivity.                                                         |
| 16 | Невозможно закодировать значение `APN`.                                                                          |
| 17 | PDN Connectivity по умолчанию запрещен или не найден.                                                            |
| 18 | Не удалось получить значение `SGW IP`.                                                                           |
| 19 | Запрос на создание второго PDN Connectivity для абонента NB-IoT.                                                 |
| 20 | Услуга <abbr title="Voice over PS Session">VoPS</abbr> недоступна.                                               |
| 21 | Не удалось установить соединение с узлом <abbr title="Service Capability Exposure Function">SCEF</abbr>.         |

#### PDN Disconnection {#pdn_disconnection}

```log
DateTime,PDN Disconnection,Trigger,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,TAC,Cell ID,Requested APN,Used APN,PDN Type,QCI,IMEISV,Attach Type,EMM Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                           |
|---|------------------------------------------------------------------------------------|
| 1 | Не удалось декодировать сообщение S1 PDN Disconnect Request.                       |
| 2 | Не удалось извлечь связанную bearer-службу из сообщения S1 PDN Disconnect Request. |
| 3 | У абонента не указана bearer-служба по умолчанию для данного SGW TEID.             |
| 4 | Не удалось декодировать сообщение GTP: Delete Session Response.                    |
| 5 | Запрос GTP: Delete Session Request не принят.                                      |
| 6 | Ошибка процедуры Detach.                                                           |

#### Detach {#detach-connect}

```log
DateTime,Detach,Trigger,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,TAC,Cell ID,Requested APN,Used APN,PDN Type,QCI,IMEISV,Attach Type,EMM Cause,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                         |
|---|------------------------------------------------------------------|
| 1 | Не найдены ключи шифрования.                                     |
| 2 | Не удалось получить значение `Detach type`.                      |
| 3 | Ошибка сброса контекста.                                         |
| 4 | Ошибка удаления сессии на узле SGW.                              |
| 5 | Не получено сообщение S1 Detach Accept.                          |
| 6 | Не получено сообщение S1 Attach Request для процедуры Re-Attach. |

 **Примечание.** Ошибка под кодом 3 может возникнуть, например, если не получен ответ от станции eNodeB на сообщение S1 UE Context Release Command.