---
title: "DIAM EDR"
description: "Log File diam_cdr"
weight: 20
type: docs
---
:asciidoctorconfigdir: ../../..
:experimantal:
:icons: font
:toc: right
:table-caption: Table
:source-highlighter: highlightjs


ifeval::["{backend}" == "pdf"]
:ppf: PPF
:qci: QCI
endif::[]

ifeval::["{backend}" != "pdf"]
:ppf: pass:q[<abbr title="Page Proceed Flag">`PPF`</abbr>]
:qci: pass:q[<abbr title="QoS Class Identifier">`QCI`</abbr>]
endif::[]

[[diam-edr]]
= Diameter EDR

Different procedures have records with similar formatting.
The data is presented in CSV format, the separator is comma.

.Log Fields
[options="header",cols="1,4,13,2"%breakable]
|===
|N |Field |Description |Type

|1 |Timestamp |Event date and time. Format: +
`YYYY-MM-DD HH:MM:SS`. |datetime
|2 |<<event-diam,Event>> |Event or procedure to generate the record. |string
|3 |SessionId |Diameter `Session-Id` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. Format: +
`<DiameterIdentity>;<high 32 bits>;<low 32 bits>` |string
|4 |IMSI |Subscriber IMSI number formatted as https://www.itu.int/rec/T-REC-E.212-201609-I/en[ITU-T E.212]. |string
|5 |MSISDN |Subscriber MSISDN number formatted as https://www.itu.int/rec/T-REC-E.164-201011-I/en[ITU-T E.164]. |string
|6 |Origin-Host |Diameter `Origin-Host` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. |string
|7 |Origin-Realm |Diameter `Origin-Realm` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. |string
|8 |Dest-Host |Diameter `Destination-Host` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. |string
|9 |Dest-Realm |Diameter `Destination-Realm` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. |string
|10 |Flags |Procedure specific Diameter request flags. |int
|11 |Duration |Procedure duration, in milliseconds. |int
|12 |Result-Code |Diameter `Result-Code` value. See https://tools.ietf.org/html/rfc6733[RFC 6733]. |int
|13 |ErrorCode |MME link:../error_code/#error-codes[internal result code]. |int
|===

[[event-diam]]
[discrete]
== Event

* <<authentication-info-request,Authentication-Info-Request>>;
* <<cancel-location-request,Cancel-Location>>;
* <<update-location-request,Update-Location>>;
* <<insert-subscriber-data-request,Insert-Subscriber-Data-Request>>;
* <<delete-subscriber-data-request,Delete-Subscriber-Data-Request>>;
* <<notify-request,Notify-Request>>;
* <<purge-request,Purge-Request>>;
* <<me-identity-check-request,ME-Identity-Check-Request>>;
* <<reset-request,Reset-Request>>;
* <<mo-forward-short-message-answer,MO-Forward-Short-Message-Answer>>;
* <<mt-forward-short-message-answer,MT-Forward-Short-Message-Answer>>.

<<<

[[authentication-info-request]]
== Authentication-Info-Request

[source,log%unbreakable]
----
DateTime,AI,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[cancel-location-request]]
== Cancel-Location

[source,log%unbreakable]
----
DateTime,CL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Paging procedure failed.
|2 |Detach procedure failed.
|===

[[update-location-request]]
== Update-Location

[source,log%unbreakable]
----
DateTime,UL,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |Received unexpected Diameter message instead of the Diameter: Update-Location-Answer one.
|2 |Diameter: Update-Location-Answer is missing the subscription information.
|3 |`Access-Restriction-Data` prohibits using the E-UTRAN.
|4 |`QCI` values in the range of 128-254 are not allowed.
|5 |Illegal {qci} value has been received.
|6 |Diameter: Update-Location-Answer is missing the MSISDN number.
|7 |Diameter: Update-Location-Answer message has an illegal `Result-Code`.
|8 |No zone contains the current `TAC` value.
|9 |Default context has the Wildcard APN.
|10 |Diameter: Update-Location-Answer message has an invalid subscriber profile.
|===

[[insert-subscriber-data-request]]
== Insert-Subscriber-Data-Request

[source,log%unbreakable]
----
DateTime,ISD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |`Current Location Request` is enabled, while `EPS Location Information Request` is not.
|2 |Paging procedure failed.
|3 |{ppf} value prohibits starting the Paging procedure.
|4 |PLMN does not support P-CSCF Restoration procedures.
|===

<<<

[[delete-subscriber-data-request]]
== Delete-Subscriber-Data-Request

[source,log%unbreakable]
----
DateTime,DSD,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

.Local error codes
[options="header",cols="1,15"%breakable]
|===
|N |Description

|1 |`Complete APN Configuration Profile Withdrawal` flag is enabled.
|2 |Failed to get the `Context-Identifier` value.
|3 |PDN Connectivity having the specified `Context-Identifier` value is not found.
|4 |Default context cannot be deleted.
|===

[[notify-request]]
== Notify-Request

[source,log%unbreakable]
----
DateTime,Notify,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[purge-request]]
== Purge-Request

[source,log%unbreakable]
----
DateTime,Purge,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[reset-request]]
== Reset-Request

[source,log%unbreakable]
----
DateTime,Reset,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[me-identity-check-request]]
== ME-Identity-Check-Request

[source,log%unbreakable]
----
DateTime,ECR,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[mo-forward-short-message-answer]]
== MO-Forward-Short-Message-Answer

[source,log%unbreakable]
----
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----

[[mt-forward-short-message-answer]]
== MT-Forward-Short-Message-Answer

[source,log%unbreakable]
----
DateTime,MO_FSM,SessionId,IMSI,MSISDN,Origin-Host,Origin-Realm,Dest-Host,Dest-Realm,
Flags,Duration,Result-Code,ErrorCode
----