---
title: "SGsAP EDR"
description: "Журнал sgsap_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N  | Поле                  | Описание                                                | Тип      |
|----|-----------------------|---------------------------------------------------------|----------|
| 1  | DateTime              | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |
| 2  | [Event](#event-sgsap) | Событие или процедура, сформировавшая запись.           | string   |
| 3  | MME UE ID             | Идентификатор S1-контекста на узле MME.                 | string   |
| 4  | IMSI                  | Номер IMSI абонента.                                    | string   |
| 5  | MSISDN                | Номер MSISDN абонента.                                  | string   |
| 6  | GUTI                  | Глобальный уникальный временный идентификатор абонента. | string   |
| 7  | PLMN                  | Идентификатор PLMN.                                     | string   |
| 8  | LAC                   | Код локальной области LAC.                              | int      |
| 9  | CellID                | Идентификатор соты.                                     | int/hex  |
| 10 | IMEI                  | Номер IMEI устройства.                                  | string   |
| 11 | Duration              | Длительность процедуры, в миллисекундах.                | int      |
| 12 | ErrorCode             | Внутренний [код MME](../error_code/) результата.        | object   |

#### События и процедуры, Event {#event-sgsap}

- [MT Call](#mt_call);
- [MT SMS](#mt_sms);
- [MO Call](#mo_call);
- [MO SMS](#mo_sms);
- [Detach Indication](#detach_indication);
- [VLR Reset Indication](#vlr_reset_indication);
- [VLR Disconnection](#vlr_disconnection);
- [UE Activity Indication](#ue_activity_indication);
- [Peer connected indication](#peer_connected_indication);
- [Peer disconnected indication](#peer_disconnected_indication).

#### MT Call {#mt_call}

```log
DateTime,MT Call,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                     |
|---|--------------------------------------------------------------|
| 1 | Отсутствует сообщение ICS Response.                          |
| 2 | Отсутствует сообщение S1 Extended Service Request.           |
| 3 | Отсутствует сообщение S1AP: UE CONTEXT MODIFICATION REQUEST. |
| 4 | Отсутствует сообщение S1AP: UE CONTEXT RELEASE REQUEST.      |
| 5 | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен. |
| 6 | Абонент недоступен для PS-сетей.                             |
| 7 | Процедура Paging завершилась с ошибкой.                      |

#### MT SMS {#mt_sms}

```log
DateTime,MT SMS,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                            |
|---|-------------------------------------------------------------------------------------|
| 1 | Отсутствует VLR GT.                                                                 |
| 2 | Процедура Paging завершилась с ошибкой.                                             |
| 3 | Вместо ожидаемого сообщения SGsAP-DOWNLINK-UNITDATA получено SGsAP-RELEASE-REQUEST. |
| 4 | Флаг <abbr title="Page Proceed Flag">PPF</abbr> уже сброшен.                        |
| 5 | Абонент недоступен для PS-сетей.                                                    |

#### MO Call {#mo_call}

```log
DateTime,MO Call,MME UE ID,IMSI,MSISDN,GUTI,PLMN,LAC,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N  | Описание                                           |
|----|----------------------------------------------------|
| 8  | Для данного абонента неизвестно значение `VLR GT`. |
| 9  | Ошибка процедуры S1AP: INITIAL CONTEXT SETUP.      |
| 15 | Неизвестно значение `LAI` для текущих PLMN и TAC.  |

#### MO SMS {#mo_sms}

```log
DateTime,MO SMS,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,LAC,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание            |
|-----|---------------------|
| 1   | Отсутствует VLR GT. |

#### Detach Indication {#detach_indication}

```log
DateTime,Detach Ind,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### VLR Reset Indication {#vlr_reset_indication}

```log
DateTime,VLR Reset Indication,MME name,Duration,ErrorCode
```

#### VLR Disconnection {#vlr_disconnection}

```log
DateTime,VLR Disconnection,VLR IP,Duration,ErrorCode
```

#### UE Activity Indication {#ue_activity_indication}

```log
DateTime,UE Activity Indication,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Peer Connected Indication {#peer_connected_indication}

```log
DateTime,Connected,IP:port
```

#### Peer Disconnected Indication {#peer_disconnected_indication}

```log
DateTime,Disconnected,IP:port
```