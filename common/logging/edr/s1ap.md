---
title: "S1AP EDR"
description: "Журнал s1ap_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N  | Поле                 | Описание                                                | Тип      |
|----|----------------------|---------------------------------------------------------|----------|
| 1  | DateTime             | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |
| 2  | [Event](#event-s1ap) | Событие или процедура, сформировавшая запись.           | string   |
| 3  | eNodeB UE ID         | Идентификатор S1-контекста на стороне eNodeB.           | int      |
| 4  | MME UE ID            | Идентификатор S1-контекста на узле MME.                 | int      |
| 5  | IMSI                 | Номер IMSI абонента.                                    | string   |
| 6  | MSISDN               | Номер MSISDN абонента.                                  | string   |
| 7  | GUTI                 | Глобальный уникальный временный идентификатор абонента. | string   |
| 8  | PLMN                 | Идентификатор PLMN.                                     | string   |
| 9  | CellID               | Идентификатор соты.                                     | int/hex  |
| 10 | IMEI                 | Номер IMEI устройства.                                  | string   |
| 11 | Duration             | Длительность процедуры, в миллисекундах.                | int      |
| 12 | ErrorCode            | Внутренний [код MME](../error_code/) результата.        | int      |

#### События и процедуры, Event {#event-s1ap}

- [Authentication](#authentication);
- [Bearer Resource Modification](#bearer_resource_modification);
- [Deactivate Bearer](#deactivate_bearer);
- [E-RAB Modification](#e_rab_modification);
- [GUTI Reallocation](#guti_reallocation);
- [Initial Context Setup](#initial_context_setup);
- [MT Data Transport](#mt_data_transport);
- [MO Data Transport](#mo_data_transport);
- [Modify EPS bearer context](#modify_eps_bearer_context);
- [Secondary RAT Data Usage Report](#secondary_rat_data_usage_report);
- [UE Context Modification](#ue_context_modification);
- [Trace Failure Indication](#trace_failure_indication).

#### Authentication {#authentication}

```log
DateTime,Authentication,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N  | Описание                                                                                       |
|----|------------------------------------------------------------------------------------------------|
| 1  | Неуспешное сообщение Diameter: Authentication-Information-Answer.                              |
| 2  | В сообщении Diameter: Authentication-Information-Answer отсутствуют данные для аутентификации. |
| 3  | Полученный ключ <abbr title="Key Access Security Management Entries">`KASME`</abbr> пуст.      |
| 4  | Получено сообщение S1 Security Mode Reject.                                                    |
| 5  | Невозможно извлечь номер IMSI из сообщения S1 Identity Response.                               |
| 6  | Несовпадение полученного `RES` с ожидаемым `XRES`.                                             |
| 7  | Ошибка <abbr title="Message Authentication Code">`MAC`</abbr>.                                 |
| 8  | Не удалось определить алгоритм шифрования.                                                     |
| 9  | Не удалось определить алгоритм контроля целостности.                                           |
| 10 | Недопустимое значение `UE-Usage-Type`.                                                         |

#### Bearer Resource Modification {#bearer_resource_modification}

```log
DateTime,Bearer Resource Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                                                             |
|-----|--------------------------------------------------------------------------------------|
| 1   | Абонент находится в состоянии <abbr title="EPC Mobility Management">EMM</abbr>-IDLE. |
| 2   | Не указана ни одна bearer-служба.                                                    |
| 3   | Запрошена неизвестная bearer-служба.                                                 |
| 4   | Получен отказ от узла SGW.                                                           |
| 5   | Ошибка вспомогательной логики.                                                       |

#### Deactivate Bearer {#deactivate_bearer}

```log
DateTime,Deactivate Bearer,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                            |
|-----|-------------------------------------|
| 7   | Ошибка процедуры Paging.            |
| 8   | Получен отказ от станции eNodeB.    |
| 9   | Указанная bearer-служба не найдена. |

#### E-RAB Modification {#e_rab_modification}

```log
DateTime,E-RAB Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                             |
|-----|------------------------------------------------------|
| 1   | Не найдены подходящие для модификации bearer-службы. |

#### GUTI Reallocation {#guti_reallocation}

```log
DateTime,GUTI Reallocation,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Initial Context Setup {#initial_context_setup}

```log
DateTime,Initial Context Setup,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,CauseType,CauseReason,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                 |
|-----|------------------------------------------|
| 1   | Отсутствуют bearer-службы для активации. |
| 2   | Получено сообщение ICS Failure.          |

#### MT Data Transport {#mt_data_transport}

```log
DateTime,MT Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### MO Data Transport {#mo_data_transport}

```log
DateTime,MO Data Transport,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                                    |
|-----|---------------------------------------------|
| 1   | Запрошена неизвестная bearer-служба.        |
| 2   | Ошибка модификации bearer-службы.           |
| 3   | Отсутствует значение `User Data Container`. |

#### Modify EPS Bearer Context {#modify_eps_bearer_context}

```log
DateTime,Modify EPS Bearer Context,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Bearer ID,Duration,ErrorCode
```

Локальные коды ошибок:

| N   | Описание                         |
|-----|----------------------------------|
| 11  | Bearer-служба отвергнута eNodeB. |
| 12  | Bearer-служба отвергнута UE.     |

#### Secondary RAT Data Usage Report {#secondary_rat_data_usage_report}

```log
DateTime,Secondary RAT Data Usage Report,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### UE Context Modification {#ue_context_modification}

```log
DateTime,UE Context Modification,eNodeB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Trace Failure Indication {#trace_failure_indication}

```log
DateTime,Trace Failure Indication,ENB UE ID,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```
