---
title: "HTTP EDR"
description: "Журнал http_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N  | Поле                 | Описание                                                | Тип      |
|----|----------------------|---------------------------------------------------------|----------|
| 1  | DateTime             | Дата и время события. Формат:<br>`YYYY-MM-DD HH:MM:SS`. | datetime |
| 2  | [Event](#event-http) | Событие или процедура, сформировавшая запись.           | string   |
| 3  | MME UE ID            | Идентификатор S1-контекста на узле MME.                 | int      |
| 4  | IMSI                 | Номер IMSI абонента.                                    | string   |
| 5  | MSISDN               | Номер MSISDN абонента.                                  | string   |
| 6  | GUTI                 | Глобальный уникальный временный идентификатор абонента. | string   |
| 7  | PLMN                 | Идентификатор PLMN.                                     | string   |
| 8  | CellID               | Идентификатор соты.                                     | int/hex  |
| 9  | IMEI                 | Номер IMEI устройства.                                  | string   |
| 10 | Duration             | Длительность процедуры, в миллисекундах.                | int      |
| 11 | ErrorCode            | Внутренний [код MME](../error_code/) результата.        | object   |

#### События и процедуры, Event {#event-http}

- [Deact bearer](#deact_bearer-http);
- [Detach](#detach-http);
- [Disconnect eNodeB](#disconnect_enodeb);
- [Get Profile](#get_profile);
- [Get DB Status](#get_db_status-http);
- [Get GTP Peers](#get_gtp_peers-http);
- [Get S1 Peers](#get_s1_peers-http);
- [Get SGs Peers](#get_sgs_peers-http);
- [Clear DNS Cache](#clear_dns_cache-http);
- [UE enable trace](#ue_enable_trace-http);
- [UE disable trace](#ue_disable_trace-http);
- [Get Metrics](#get_metrics-http);
- [Reset Metrics](#reset_metrics-http).

#### Deact Bearer {#deact_bearer-http}

```log
DateTime,Deact bearer,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                         |
|---|--------------------------------------------------|
| 1 | Не удалось получить идентификатор bearer-службы. |
| 2 | Неизвестный идентификатор bearer-службы.         |
| 3 | Ошибка процедуры Paging.                         |
| 4 | Ошибка деактивации bearer-службы.                |

#### Detach {#detach-http}

```log
DateTime,Detach,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                 |
|---|--------------------------|
| 1 | Ошибка процедуры Paging. |
| 2 | Ошибка процедуры Detach. |

#### Disconnect eNodeB {#disconnect_enodeb}

```log
DateTime,Disconnect eNodeB,IP,Duration,ErrorCode
```

#### Get Profile {#get_profile}

```log
DateTime,GetProfile,MME UE ID,IMSI,MSISDN,GUTI,PLMN,CellID,IMEI,Duration,ErrorCode
```

#### Get DB Status {#get_db_status-http}

```log
DateTime,Get DB Status,Duration,ErrorCode
```

#### Get GTP Peers {#get_gtp_peers-http}

```log
DateTime,Get GTP Peers,Duration,ErrorCode
```

#### Get S1 Peers {#get_s1_peers-http}

```log
DateTime,Get S1 Peers,Duration,ErrorCode
```

#### Get SGs Peers {#get_sgs_peers-http}

```log
DateTime,Get SGS Peers,Duration,ErrorCode
```

#### Clear DNS Cache {#clear_dns_cache-http}

```log
DateTime,Clear DNS Cache,Duration,ErrorCode
```

#### UE enable trace {#ue_enable_trace-http}

```log
DateTime,UE enable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                                                  |
|---|---------------------------------------------------------------------------|
| 1 | Абонент уже находится в списке отслеживания.                              |
| 2 | Не указан IP-адрес узла <abbr title="Trace Collection Entity">TCE</abbr>. |
| 3 | Ошибка процедуры Paging.                                                  |
| 4 | Не найдены свободные идентификаторы Trace ID.                             |

#### UE disable trace {#ue_disable_trace-http}

```log
DateTime,UE disable trace,MME UE ID,IMSI,MSISDN,GUTI,PLMN,Cell ID,IMEI,Duration,ErrorCode
```

Локальные коды ошибок:

| N | Описание                                               |
|---|--------------------------------------------------------|
| 1 | Ошибка процедуры Paging.                               |
| 2 | Идентификатор E-UTRAN Trace ID не найден для абонента. |

#### Get Metrics {#get_metrics-http}

```log
DateTime,Get Metrics,Duration,ErrorCode
```

#### Reset Metrics {#reset_metrics-http}

```log
DateTime,Reset Metrics,Duration,ErrorCode
```