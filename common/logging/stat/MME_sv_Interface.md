---
title: "Sv interface"
description: "Статистика процедур Sv"
weight: 20
type: docs
---

Файл **<node_name>\_MME-svInterface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса Sv.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf).

| Tx/Rx | Метрика                                                                                    | Описание                                                            | Группа |
|-------|--------------------------------------------------------------------------------------------|---------------------------------------------------------------------|--------|
| Tx    | <a name="srvccPsToCsRequest">srvccPsToCsRequest</a>                                        | Количество сообщений GTPv2-C: SRVCC PS to CS Request.               |        |
| Rx    | <a name="srvccPsToCsResponse">srvccPsToCsResponse</a>                                      | Количество сообщений GTPv2-C: SRVCC PS to CS Response.              |        |
| Rx    | <a name="pagingReqsrvccPsToCsCompleteNotificationuest">srvccPsToCsCompleteNotification</a> | Количество сообщений GTPv2-C: SRVCC PS to CS Complete Notification. |        |
| Tx    | <a name="srvccPsToCsCompleteAcknowledge">srvccPsToCsCompleteAcknowledge</a>                | Количество сообщений GTPv2-C: SRVCC PS to CS Complete Acknowledge.  |        |

#### Пример файла ####

```csv
tx,srvccPsToCsRequest,,0
rx,srvccPsToCsResponse,,0
rx,srvccPsToCsCompleteNotification,,0
tx,srvccPsToCsCompleteAcknowledge,,0
```