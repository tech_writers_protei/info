---
title: "S1 Bearer Deactivation"
description: "S1 Bearer Deactivation Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1BearerDeactivation\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the S1 Bearer Deactivation procedures.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Metrics                                                                                 | Description                                                                                                                                        | Group          |
|-------|-----------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| Tx    | <a name="deactivateEpsBearerContextRequest">deactivateEpsBearerContextRequest</a>       | Number of messages S1 Deactivate EPS Bearer Context Request.                                                                                       | IMSIPLMN:Value |
| Rx    | <a name="deactivateEpsBearerContextAccept">deactivateEpsBearerContextAccept</a>         | Number of messages S1 Deactivate EPS Bearer Context Accept.                                                                                        | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest">defaultBearerDeactivationRequest</a>         | Number of messages S1 Deactivate EPS Bearer Context Request with `EBI = 5`.                                                                        | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest26">defaultBearerDeactivationRequest26</a>     | Number of messages S1 Deactivate EPS Bearer Context Request with `EBI = 5` and cause "#26 Insufficient resources".                                 | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest36">defaultBearerDeactivationRequest36</a>     | Number of messages S1 Deactivate EPS Bearer Context Request with `EBI = 5` and cause "#36 Regular deactivation".                                   | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest38">defaultBearerDeactivationRequest38</a>     | Number of messages S1 Deactivate EPS Bearer Context Request with `EBI = 5` and cause "#38 Network failure".                                        | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest39">defaultBearerDeactivationRequest39</a>     | Number of messages S1 Deactivate EPS Bearer Context Request with `EBI = 5` and cause "#39 Reactivation requested".                                 | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest">dedicatedBearerDeactivationRequest</a>     | Number of messages S1 Deactivate EPS Bearer Context Request with the specified field `Dedicated Bearer ID`.                                        | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest26">dedicatedBearerDeactivationRequest26</a> | Number of messages S1 Deactivate EPS Bearer Context Request with the specified field `Dedicated Bearer ID` and cause "#26 Insufficient resources". | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest36">dedicatedBearerDeactivationRequest36</a> | Number of messages S1 Deactivate EPS Bearer Context Request with the specified field `Dedicated Bearer ID` and cause "#36 Regular deactivation".   | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest38">dedicatedBearerDeactivationRequest38</a> | Number of messages S1 Deactivate EPS Bearer Context Request with the specified field `Dedicated Bearer ID` and cause "#38 Network failure".        | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest39">dedicatedBearerDeactivationRequest39</a> | Number of messages S1 Deactivate EPS Bearer Context Request with the specified field `Dedicated Bearer ID` and cause "#39 Reactivation requested". | IMSIPLMN:Value |
| Rx    | <a name="pdnDisconnectRequest">pdnDisconnectRequest</a>                                 | Number of messages S1 PDN Disconnect Request.                                                                                                      | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectSuccess">pdnDisconnectSuccess</a>                                 | Number of messages S1 Deactivate EPS Bearer Context Request responding to the S1 PDN Disconnect Request.                                           | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject">pdnDisconnectReject</a>                                   | Number of messages S1 PDN Disconnect Reject.                                                                                                       | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject35">pdnDisconnectReject35</a>                               | Number of messages S1 PDN Disconnect Reject with the cause "#35 PTI already in use".                                                               | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject43">pdnDisconnectReject43</a>                               | Number of messages S1 PDN Disconnect Reject with the cause "#43 Invalid EPS bearer identity".                                                      | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject49">pdnDisconnectReject49</a>                               | Number of messages S1 PDN Disconnect Reject with the cause "#49 Last PDN disconnection not allowed".                                               | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject96">pdnDisconnectReject96</a>                               | Number of messages S1 PDN Disconnect Reject with the cause "#96 Invalid mandatory information".                                                    | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject111">pdnDisconnectReject111</a>                             | Number of messages S1 PDN Disconnect Reject with the cause "#111 Protocol error, unspecified".                                                     | IMSIPLMN:Value |

**Note.** The ESM error causes see in [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Group

| Field    | Description                                          | Type |
|----------|------------------------------------------------------|------|
| IMSIPLMN | PLMN identifier based on the first 5 digits of IMSI. | int  |

#### Group Sample ####

```
IMSIPLMN,25099
```

#### File Sample ####

```csv
tx,deactivateEpsBearerContextRequest,,0
rx,deactivateEpsBearerContextAccept,,0
tx,defaultBearerDeactivationRequest,,0
tx,dedicatedBearerDeactivationRequest,,0
rx,pdnDisconnectRequest,,0
tx,pdnDisconnectReject,,0
```
