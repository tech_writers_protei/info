---
title: "S6a Interface"
description: "Статистика процедур S6a"
weight: 20
type: docs
---

Файл **<node_name>\_MME-S6a-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для интерфейса S6a.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf) 
и [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Метрика                                                                         | Описание                                                           | Группа         | 
|-------|---------------------------------------------------------------------------------|--------------------------------------------------------------------|----------------|
| Tx    | <a name="authenticationInformationRequest">authenticationInformationRequest</a> | Количество сообщений Diameter: Authentication-Information-Request. | IMSIPLMN:Value | 
| Rx    | <a name="authenticationInformationAnswer">authenticationInformationAnswer</a>   | Количество сообщений Diameter: Authentication-Information-Answer.  | IMSIPLMN:Value | 
| Tx    | <a name="updateLocationRequest">updateLocationRequest</a>                       | Количество сообщений Diameter: Update-Location-Request.            | IMSIPLMN:Value | 
| Rx    | <a name="updateLocationAnswer">updateLocationAnswer</a>                         | Количество сообщений Diameter: Update-Location-Answer.             | IMSIPLMN:Value | 
| Rx    | <a name="resetRequest">resetRequest</a>                                         | Количество сообщений Diameter: Reset-Request.                      | IMSIPLMN:Value | 
| Tx    | <a name="resetAnswer">resetAnswer</a>                                           | Количество сообщений Diameter: Reset-Answer.                       | IMSIPLMN:Value | 
| Rx    | <a name="cancelLocationRequest">cancelLocationRequest</a>                       | Количество сообщений Diameter: Cancel-Location-Request.            | IMSIPLMN:Value | 
| Rx    | <a name="cancelLocationAnswer">cancelLocationAnswer</a>                         | Количество сообщений Diameter: Cancel-Location-Answer.             | IMSIPLMN:Value | 
| Rx    | <a name="insertSubscriberDataRequest">insertSubscriberDataRequest</a>           | Количество сообщений Diameter: Insert-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Tx    | <a name="insertSubscriberDataAnswer">insertSubscriberDataAnswer</a>             | Количество сообщений Diameter: Insert-Subscriber-Data-Answer.      | IMSIPLMN:Value | 
| Tx    | <a name="purgeUeRequest">purgeUeRequest</a>                                     | Количество сообщений Diameter: Purge-UE-Request.                   | IMSIPLMN:Value | 
| Rx    | <a name="purgeUeAnswer">purgeUeAnswer</a>                                       | Количество сообщений Diameter: Purge-UE-Answer.                    | IMSIPLMN:Value | 
| Tx    | <a name="notifyRequest">notifyRequest</a>                                       | Количество сообщений Diameter: Notify-Request.                     | IMSIPLMN:Value | 
| Rx    | <a name="notifyAnswer">notifyAnswer</a>                                         | Количество сообщений Diameter: Notify-Answer.                      | IMSIPLMN:Value | 
| Rx    | <a name="deleteSubscriberDataRequest">deleteSubscriberDataRequest</a>           | Количество сообщений Diameter: Delete-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Tx    | <a name="deleteSubscriberDataAnswer">deleteSubscriberDataAnswer</a>             | Количество сообщений Diameter: Delete-Subscriber-Data-Answer.      | IMSIPLMN:Value | 

В таблице ниже приведены возможные значения AVP `Result-Code` и AVP `Experimental-Result-Code` из перечисленных в
[3GPP TS 29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229/17.02.00_60/ts_129229v170200p.pdf) и
[3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).

**Примечание.** Коды AVP `Experimental-Result-Code` отмечены буквой `e`.

| Код   | Описание                                        |
|-------|-------------------------------------------------|
| 2001  | DIAMETER_SUCCESS                                |
| 4181e | DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE        |
| 5001e | DIAMETER_ERROR_USER_UNKNOWN                     |
| 5003e | DIAMETER_ERROR_IDENTITY_NOT_REGISTERED          |
| 5004e | DIAMETER_ERROR_ROAMING_NOT_ALLOWED              |
| 5012e | DIAMETER_ERROR_SERVING_NODE_FEATURE_UNSUPPORTED |
| 5420e | DIAMETER_ERROR_UNKNOWN_EPS_SUBSCRIPTION         |
| 5421e | DIAMETER_ERROR_RAT_NOT_ALLOWED                  |
| 5423e | DIAMETER_ERROR_UNKNOWN_SERVING_NODE             |

При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP `Experimental-Result-Code` отмечены дополнительной буквой `e` в конце.

Пример: `updateLocationAnswer5420e`.

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
IMSIPLMN:25099
```

#### Пример файла ####

```csv
rx,updateLocationAnswer,,71
rx,updateLocationAnswer2001,,71
rx,cancelLocationRequest,,2
rx,authenticationInformationAnswer,,134
rx,authenticationInformationAnswer2001,,134
rx,authenticationInformationAnswer4181e,,0
rx,insertSubscriberDataRequest,,0
rx,deleteSubscriberDataRequest,,0
rx,purgeUeAnswer,,47
rx,purgeUeAnswer2001,,47
rx,resetRequest,,0
rx,notifyAnswer,,118
rx,notifyAnswer2001,,90
rx,notifyAnswer5423e,,28
tx,updateLocationRequest,,71
tx,cancelLocationAnswer,,2
tx,cancelLocationAnswer2001,,2
tx,authenticationInformationRequest,,134
tx,insertSubscriberDataAnswer,,0
tx,deleteSubscriberDataAnswer,,0
tx,purgeUeRequest,,47
tx,resetAnswer,,0
tx,notifyRequest,,118
rx,updateLocationAnswer2001,IMSIPLMN:00101,59
rx,updateLocationAnswer,IMSIPLMN:00101,59
rx,cancelLocationRequest,IMSIPLMN:00101,2
rx,authenticationInformationAnswer2001,IMSIPLMN:00101,129
rx,authenticationInformationAnswer,IMSIPLMN:00101,129
rx,purgeUeAnswer2001,IMSIPLMN:00101,46
rx,purgeUeAnswer,IMSIPLMN:00101,46
rx,notifyAnswer2001,IMSIPLMN:00101,86
rx,notifyAnswer5423e,IMSIPLMN:00101,28
rx,notifyAnswer,IMSIPLMN:00101,114
rx,updateLocationAnswer2001,IMSIPLMN:20893,12
rx,updateLocationAnswer,IMSIPLMN:20893,12
rx,authenticationInformationAnswer2001,IMSIPLMN:20893,5
rx,authenticationInformationAnswer4181e,IMSIPLMN:20893,0
rx,authenticationInformationAnswer,IMSIPLMN:20893,5
rx,purgeUeAnswer2001,IMSIPLMN:20893,1
rx,purgeUeAnswer,IMSIPLMN:20893,1
rx,notifyAnswer2001,IMSIPLMN:20893,4
rx,notifyAnswer,IMSIPLMN:20893,4
rx,cancelLocationRequest,IMSIPLMN:99999,0
tx,updateLocationRequest,IMSIPLMN:00101,59
tx,cancelLocationAnswer2001,IMSIPLMN:00101,2
tx,cancelLocationAnswer,IMSIPLMN:00101,2
tx,authenticationInformationRequest,IMSIPLMN:00101,129
tx,purgeUeRequest,IMSIPLMN:00101,46
tx,notifyRequest,IMSIPLMN:00101,114
tx,updateLocationRequest,IMSIPLMN:20893,12
tx,authenticationInformationRequest,IMSIPLMN:20893,5
tx,purgeUeRequest,IMSIPLMN:20893,1
tx,notifyRequest,IMSIPLMN:20893,4
tx,cancelLocationAnswer2001,IMSIPLMN:99999,0
tx,cancelLocationAnswer,IMSIPLMN:99999,0
```