---
title: "Diameter"
description: "Diameter Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-Diameter\_\<datetime>\_<granularity>.csv** contains statistical
information on MME metrics for the Diameter: Base interface procedures.

### Field Description ###

For more information, see [RFC 6733](https://www.ietf.org/rfc/rfc6733.txt.pdf).

| Tx/Rx | Metrics                                                                     | Description                                                                                                       | Group       |
|-------|-----------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|-------------|
| Tx/Rx | <a name="deviceWatchdogRequest">deviceWatchdogRequest</a>                   | Number of messages Diameter: Device-Watchdog-Request, DWR.                                                        | REALM:Value |
| Tx/Rx | <a name="deviceWatchdogAnswer">deviceWatchdogAnswer</a>                     | Number of messages Diameter: Device-Watchdog-Answer, DWA.                                                         | REALM:Value |
| Tx/Rx | <a name="deviceWatchdogAnswer2001">deviceWatchdogAnswer2001</a>             | Number of messages Diameter: Device-Watchdog-Answer, DWA, with AVP `Result-Code = DIAMETER_SUCCESS (2001)`.       | REALM:Value |
| Tx/Rx | <a name="capabilitiesExchangeRequest">capabilitiesExchangeRequest</a>       | Number of messages Diameter: Capabilities-Exchange-Request, CER.                                                  | REALM:Value |
| Tx/Rx | <a name="capabilitiesExchangeAnswer">capabilitiesExchangeAnswer</a>         | Number of messages Diameter: Capabilities-Exchange-Answer, CEA.                                                   | REALM:Value |
| Tx/Rx | <a name="capabilitiesExchangeAnswer2001">capabilitiesExchangeAnswer2001</a> | Number of messages Diameter: Capabilities-Exchange-Answer, CEA, with AVP `Result-Code = DIAMETER_SUCCESS (2001)`. | REALM:Value |
| Tx/Rx | <a name="disconnectPeerRequest">disconnectPeerRequest</a>                   | Number of messages Diameter: Disconnect-Peer-Request, DPR.                                                        | REALM:Value |
| Tx/Rx | <a name="disconnectPeerAnswer">disconnectPeerAnswer</a>                     | Number of messages Diameter: Disconnect-Peer-Answer, DPA.                                                         | REALM:Value |
| Tx/Rx | <a name="disconnectPeerAnswer2001">disconnectPeerAnswer2001</a>             | Number of messages Diameter: Disconnect-Peer-Answer, DPA, with AVP `Result-Code = DIAMETER_SUCCESS (2001)`.       | REALM:Value |

### Group

| Field | Description                                                                                                      | Type   |
|-------|------------------------------------------------------------------------------------------------------------------|--------|
| REALM | `Destination-Realm` value from diam_dest.cfg::DestRealm. Format:<br>`<domain>.mnc<MNC>.mcc<MCC>.3gppnetwork.org` | string |

#### Group Sample ####

```
REALM:epc.mnc001.mcc001.3gppnetwork.org
```

#### File Sample ####

```csv
rx,capabilitiesExchangeRequest,,0
rx,capabilitiesExchangeAnswer,,0
rx,capabilitiesExchangeAnswer2001,,0
rx,deviceWatchdogRequest,,211
rx,deviceWatchdogAnswer,,87
rx,deviceWatchdogAnswer2001,,87
tx,capabilitiesExchangeRequest,,0
tx,capabilitiesExchangeAnswer,,0
tx,deviceWatchdogRequest,,87
tx,deviceWatchdogAnswer,,211
tx,deviceWatchdogAnswer2001,,211
```