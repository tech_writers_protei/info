---
title: "S1 Attach"
description: "S1 Attach Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1Attach\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the Attach procedures in the S1 interface.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Metrics                                                     | Description                                                                                                       | Group                     |
|-------|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|---------------------------|
| Rx    | <a name="attachRequest">attachRequest</a>                   | Number of messages S1 Attach Request с `EPS Attach Type = EPS attach`.                                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachSuccess">attachSuccess</a>                   | Number of messages S1 Attach Accept с `EPS Attach Type = EPS attach`.                                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail">attachFail</a>                         | Number of messages S1 Attach Reject.                                                                              | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail2">attachFail2</a>                       | Number of messages S1 Attach Reject with the cause "#2 IMSI unknown in HSS".                                      | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail3">attachFail3</a>                       | Number of messages S1 Attach Reject with the cause "#3 Illegal UE".                                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail6">attachFail6</a>                       | Number of messages S1 Attach Reject with the cause "#6 Illegal ME".                                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail7">attachFail7</a>                       | Number of messages S1 Attach Reject with the cause "#7 EPS services not allowed".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail8">attachFail8</a>                       | Number of messages S1 Attach Reject with the cause "#8 EPS and non-EPS services not allowed".                     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail11">attachFail11</a>                     | Number of messages S1 Attach Reject with the cause "#11 PLMN not allowed".                                        | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail12">attachFail12</a>                     | Number of messages S1 Attach Reject with the cause "#12 Tracking Area not allowed".                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail13">attachFail13</a>                     | Number of messages S1 Attach Reject with the cause "#13 Roaming not allowed in this tracking area".               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail14">attachFail14</a>                     | Number of messages S1 Attach Reject with the cause "#14 EPS services not allowed in this PLMN".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail15">attachFail15</a>                     | Number of messages S1 Attach Reject with the cause "#15 No Suitable Cells In tracking area".                      | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail17">attachFail17</a>                     | Number of messages S1 Attach Reject with the cause "#17 Network failure".                                         | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail19">attachFail19</a>                     | Number of messages S1 Attach Reject with the cause "#19 ESM failure".                                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail25">attachFail25/a>                      | Number of messages S1 Attach Reject with the cause "#25 Not authorized for this CSG".                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="attachFail111">attachFail111</a>                   | Number of messages S1 Attach Reject with the cause "#111 Protocol error, unspecified".                            | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="combinedAttachRequest">combinedAttachRequest</a>   | Number of messages S1 Attach Request with `EPS Attach type = combined EPS/IMSI attach`.                           | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachSuccess">combinedAttachSuccess</a>   | Number of messages S1 Attach Accept with `EPS Attach type = combined EPS/IMSI attach`.                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail">combinedAttachFail</a>         | Number of messages S1 Attach Reject в ответ на  Attach Request с IE `EPS Attach type = combined EPS/IMSI attach`. | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail3">combinedAttachFail3</a>       | Number of messages S1 Attach Reject with the cause "#3 Illegal UE".                                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail6">combinedAttachFail6</a>       | Number of messages S1 Attach Reject with the cause "#6 Illegal ME".                                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail7">combinedAttachFail7</a>       | Number of messages S1 Attach Reject with the cause "#7 EPS services not allowed".                                 | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail8">combinedAttachFail8</a>       | Number of messages S1 Attach Reject with the cause "#8 EPS and non-EPS services not allowed".                     | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail11">combinedAttachFail11</a>     | Number of messages S1 Attach Reject with the cause "#11 PLMN not allowed".                                        | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail12">combinedAttachFail12</a>     | Number of messages S1 Attach Reject with the cause "#12 Tracking Area not allowed".                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail13">combinedAttachFail13</a>     | Number of messages S1 Attach Reject with the cause "#13 Roaming not allowed in this tracking area".               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail14">combinedAttachFail14</a>     | Number of messages S1 Attach Reject with the cause "#14 EPS services not allowed in this PLMN".                   | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail15">combinedAttachFail15</a>     | Number of messages S1 Attach Reject with the cause "#15 No Suitable Cells In tracking area".                      | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail19">combinedAttachFail19</a>     | Number of messages S1 Attach Reject with the cause "#19 ESM failure".                                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail25">combinedAttachFail25</a>     | Number of messages S1 Attach Reject with the cause "#25 Not authorized for this CSG".                             | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="combinedAttachFail111">combinedAttachFail111</a>   | Number of messages S1 Attach Reject with the cause "#111 Protocol error, unspecified".                            | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="emergencyAttachRequest">emergencyAttachRequest</a> | Number of messages S1 Attach Request with `EPS Attach type = EPS emergency attach`.                               | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="emergencyAttachSuccess">emergencyAttachSuccess</a> | Number of messages S1 Attach Accept with `EPS Attach type = EPS emergency attach`.                                | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="emergencyAttachFail">emergencyAttachFail</a>       | Number of messages S1 Attach Reject with `EPS Attach type = EPS emergency attach`.                                | TAI:Value, IMSIPLMN:Value |

**Note.** The EMM error causes see in [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Group ###

| Field       | Description                                            | Type |
|-------------|--------------------------------------------------------|------|
| TAI         | Tracking Area Identifier. Format:<br>`<plmn_id><tac>`. | hex  |
| \<plmn_id\> | PLMN identifier.                                       | int  |
| \<tac\>     | Tracking Area Code.                                    | hex  |
| IMSIPLMN    | PLMN identifier based on the first 5 digits of IMSI.   | int  |

#### Group Sample ####

```
TAI:2500103E8
IMSIPLMN:25099
```

#### File Sample ####

```csv
rx,attachRequest,,288
rx,attachRequest,IMSIPLMN:20893,287
rx,attachRequest,TAI:208930001,288
tx,attachSuccess,,276
tx,attachSuccess,IMSIPLMN:20893,276
tx,attachSuccess,TAI:208930001,276
tx,attachFail17,,15
tx,attachFail17,IMSIPLMN:20893,14
tx,attachFail17,TAI:208930001,15
rx,combinedAttachRequest,,7
rx,combinedAttachRequest,IMSIPLMN:00101,2
rx,combinedAttachRequest,IMSIPLMN:25020,1
rx,combinedAttachRequest,IMSIPLMN:25060,2
rx,combinedAttachRequest,IMSIPLMN:99999,2
rx,combinedAttachRequest,TAI:001010001,7
tx,combinedAttachSuccess,,5
tx,combinedAttachSuccess,IMSIPLMN:00101,2
tx,combinedAttachSuccess,IMSIPLMN:99999,3
tx,combinedAttachSuccess,TAI:001010001,4
tx,combinedAttachSuccess,TAI:999990001,1
tx,combinedAttachFail12,,3
tx,combinedAttachFail12,IMSIPLMN:25020,1
tx,combinedAttachFail12,IMSIPLMN:25060,2
tx,combinedAttachFail12,TAI:001010001,3
rx,emergencyAttachRequest,,0
tx,emergencyAttachSuccess,,0
```