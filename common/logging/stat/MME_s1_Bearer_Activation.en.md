---
title: "S1 Bearer Activation"
description: "S1 Bearer Activation Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1BearerActivation\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the S1 Bearer Activation procedures.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Metrics                                                                                         | Description                                                                                                                            | Group                                |
|-------|-------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| Tx    | <a name="dedicatedBearerActiveRequest">dedicatedBearerActiveRequest</a>                         | Number of messages S1 Activate Dedicated EPS Bearer Context Request.                                                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveSuccess">dedicatedBearerActiveSuccess</a>                         | Number of messages S1 Activate Dedicated EPS Bearer Context Accept.                                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject">dedicatedBearerActiveReject</a>                           | Number of messages S1 Activate Dedicated EPS Bearer Context Reject                                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject26">dedicatedBearerActiveReject26</a>                       | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#26 Insufficient Resources".                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject31">dedicatedBearerActiveReject31</a>                       | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#31 Request rejected, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject43">dedicatedBearerActiveReject43</a>                       | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#43 Invalid EPS bearer identity".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="dedicatedBearerActiveReject111">dedicatedBearerActiveReject111</a>                     | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#95-111 Protocol error, unspecified".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci1">dedicatedBearerActiveRequestQci1</a>                 | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 1`.                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci2">dedicatedBearerActiveRequestQci2</a>                 | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 2`.                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci3">dedicatedBearerActiveRequestQci3</a>                 | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 3`.                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci4">dedicatedBearerActiveRequestQci4</a>                 | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 4`.                                                     | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci65">dedicatedBearerActiveRequestQci65</a>               | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 65`.                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci66">dedicatedBearerActiveRequestQci66</a>               | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 66`.                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="dedicatedBearerActiveRequestQci67">dedicatedBearerActiveRequestQci67</a>               | Number of messages S1 Activate Dedicated EPS Bearer Context Request для `QCI = 67`.                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest">activateDefaultEpsBearerContextRequest</a>     | Number of messages S1 Activate Dedicated EPS Bearer Context Request.                                                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest50">activateDefaultEpsBearerContextRequest50</a> | Number of messages S1 Activate Dedicated EPS Bearer Context Request with the cause "#50 PDN type IPv4 only allowed".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest51">activateDefaultEpsBearerContextRequest51</a> | Number of messages S1 Activate Dedicated EPS Bearer Context Request with the cause "#51 PDN type IPv6 only allowed".                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextRequest52">activateDefaultEpsBearerContextRequest52</a> | Number of messages S1 Activate Dedicated EPS Bearer Context Request with the cause "#52 Single address bearers only allowed".          | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="activateDefaultEpsBearerContextAccept">activateDefaultEpsBearerContextAccept</a>       | Number of messages S1 Activate Dedicated EPS Bearer Context Accept.                                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject">activateDefaultEpsBearerContextReject</a>       | Number of messages S1 Activate Dedicated EPS Bearer Context Reject.                                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject26">activateDefaultEpsBearerContextReject26</a>   | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#26 Insufficient resources".                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject31">activateDefaultEpsBearerContextReject31</a>   | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#31 Request rejected, unspecified".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject47">activateDefaultEpsBearerContextReject47</a>   | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#47 PTI mismatch".                                  | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject95">activateDefaultEpsBearerContextReject95</a>   | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#95 Semantically incorrect message".                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject96">activateDefaultEpsBearerContextReject96</a>   | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#96 Invalid mandatory information".                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="activateDefaultEpsBearerContextReject111">activateDefaultEpsBearerContextReject111</a> | Number of messages S1 Activate Dedicated EPS Bearer Context Reject with the cause "#95-111 Protocol error, unspecified".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Rx    | <a name="pdnConnectivityRequest">pdnConnectivityRequest</a>                                     | Number of messages S1 PDN Connectivity Request.                                                                                        | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject">pdnConnectivityReject</a>                                       | Number of messages S1 PDN Connectivity Reject.                                                                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject8">pdnConnectivityReject8</a>                                     | Number of messages S1 PDN Connectivity Reject with the cause "#8 Operator determined barring".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject26">pdnConnectivityReject26</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#26 Insufficient resources".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject27">pdnConnectivityReject27</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#27 Missing or unknown APN".                                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject28">pdnConnectivityReject28</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#28 Unknown PDN type".                                                   | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject29">pdnConnectivityReject29</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#29 User authentication failed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject30">pdnConnectivityReject30</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#30 Request rejected by Serving GW or PDN GW".                           | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject31">pdnConnectivityReject31</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#31 Request rejected, unspecified".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject32">pdnConnectivityReject32</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#32 Service option not supported".                                       | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject33">pdnConnectivityReject33</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#33 Requested service option not subscribed".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject34">pdnConnectivityReject34</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#34 Service option temporarily out of order".                            | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject35">pdnConnectivityReject35</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#35 PTI already in use".                                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject38">pdnConnectivityReject38</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#38 Network failure".                                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject41">pdnConnectivityReject41</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#41 Semantic error in the TFT operation".                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject42">pdnConnectivityReject42</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#42 Syntactical error in the TFT operation".                             | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject44">pdnConnectivityReject44</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#44 Semantic errors in packet filter(s)".                                | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject45">pdnConnectivityReject45</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#45 Syntactical error in packet filter(s)".                              | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject50">pdnConnectivityReject50</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#50 PDN type IPv4 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject51">pdnConnectivityReject51</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#51 PDN type IPv6 only allowed".                                         | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject54">pdnConnectivityReject54</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#54 PDN connection does not exist".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject55">pdnConnectivityReject55</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#55 Multiple PDN connections for a given APN not allowed".               | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject56">pdnConnectivityReject56</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#56 Collision with network initiated request".                           | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject60">pdnConnectivityReject60</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#60 Bearer handling not supported".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject65">pdnConnectivityReject65</a>                                   | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#65 Maximum number of EPS bearers reached".                              | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject96">pdnConnectivityReject96</a>                                   | Number of messages S1 PDN Connectivity Reject with the cause "#96 Invalid mandatory information".                                      | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject111">pdnConnectivityReject111</a>                                 | Number of messages S1 PDN Connectivity Reject with the cause "#95-111 Protocol error, unspecified".                                    | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject112">pdnConnectivityReject112</a>                                 | Number of messages S1 PDN Connectivity Reject with the cause "#112 APN restriction value incompatible with active EPS bearer context". | TAI:Value, IMSIPLMN:Value, APN:Value |
| Tx    | <a name="pdnConnectivityReject113">pdnConnectivityReject113</a>                                 | Number of messages S1 PDN CONNECTIVITY REJECT with the cause "#113 Multiple accesses to a PDN connection not allowed".                 | TAI:Value, IMSIPLMN:Value, APN:Value |

### Group ###

| Field       | Description                                                                | Type   |
|-------------|----------------------------------------------------------------------------|--------|
| TAI         | Tracking Area Identifier. Format:<br>`<plmn_id><tac>`.                     | hex    |
| \<plmn_id\> | PLMN identifier.                                                           | int    |
| \<tac\>     | Tracking Area Code.                                                        | hex    |
| IMSIPLMN    | PLMN identifier based on the first 5 digits of IMSI.                       | int    |
| APN         | Tracking Area Identifier. Format:<br>`<name>.mnc<mnc>.mcc<mcc>.<network>`. | string |
| \<name\>    | Name.                                                                      | string |
| \<mnc\>     | Mobile Network Code.                                                       | int    |
| \<mcc\>     | Mobile Country Code.                                                       | int    |
| \<network\> | Network type.                                                              | string |

**Note.** The ESM error causes see in [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

#### Group Sample ####

```
TAI,2500103E8
IMSIPLMN,25099
APN:internet.mnc01.mcc250.gprs
```

#### File Sample ####

```csv
tx,dedicatedBearerActiveRequest,,5
tx,dedicatedBearerActiveRequest,APN:ims.mnc001.mcc001.gprs,5
tx,dedicatedBearerActiveRequest,IMSIPLMN:00101,5
tx,dedicatedBearerActiveRequest,TAI:001010001,5
tx,dedicatedBearerActiveRequestQci1,,5
tx,dedicatedBearerActiveRequestQci1,APN:ims.mnc001.mcc001.gprs,5
tx,dedicatedBearerActiveRequestQci1,IMSIPLMN:00101,5
tx,dedicatedBearerActiveRequestQci1,TAI:001010001,5
rx,dedicatedBearerActiveSuccess,,2
rx,dedicatedBearerActiveSuccess,APN:ims.mnc001.mcc001.gprs,2
rx,dedicatedBearerActiveSuccess,IMSIPLMN:00101,2
rx,dedicatedBearerActiveSuccess,TAI:001010001,2
rx,dedicatedBearerActiveReject,,0
tx,activateDefaultEpsBearerContextRequest,,12
tx,activateDefaultEpsBearerContextRequest,APN:a.mnc093.mcc208.gprs,4
tx,activateDefaultEpsBearerContextRequest,APN:ims.mnc001.mcc001.gprs,4
tx,activateDefaultEpsBearerContextRequest,APN:internet.mnc001.mcc001.gprs,4
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:00101,8
tx,activateDefaultEpsBearerContextRequest,IMSIPLMN:20893,4
tx,activateDefaultEpsBearerContextRequest,TAI:001010001,8
tx,activateDefaultEpsBearerContextRequest,TAI:208930001,4
rx,activateDefaultEpsBearerContextAccept,,12
rx,activateDefaultEpsBearerContextAccept,APN:a.mnc093.mcc208.gprs,4
rx,activateDefaultEpsBearerContextAccept,APN:ims.mnc001.mcc001.gprs,4
rx,activateDefaultEpsBearerContextAccept,APN:internet.mnc001.mcc001.gprs,4
rx,activateDefaultEpsBearerContextAccept,IMSIPLMN:00101,8
rx,activateDefaultEpsBearerContextAccept,IMSIPLMN:20893,4
rx,activateDefaultEpsBearerContextAccept,TAI:001010001,8
rx,activateDefaultEpsBearerContextAccept,TAI:208930001,4
rx,pdnConnectivityRequest,,13
rx,pdnConnectivityRequest,APN:a.mnc093.mcc208.gprs,4
rx,pdnConnectivityRequest,APN:ims.mnc001.mcc001.gprs,4
rx,pdnConnectivityRequest,APN:internet.mnc001.mcc001.gprs,4
rx,pdnConnectivityRequest,IMSIPLMN:00101,8
rx,pdnConnectivityRequest,IMSIPLMN:20893,4
rx,pdnConnectivityRequest,IMSIPLMN:25002,1
rx,pdnConnectivityRequest,TAI:001010001,8
rx,pdnConnectivityRequest,TAI:001010051,1
rx,pdnConnectivityRequest,TAI:208930001,4
tx,pdnConnectivityReject,,0
```
