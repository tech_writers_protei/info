---
title: "Sv interface"
description: "Sv Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-svInterface\_\<datetime\>\_\<granularity\>.csv** contains statistical information on MME metrics for the Sv interface.

### Field Description ###

For more information, see [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf).

| Tx/Rx | Metrics                                                                                    | Description                                                       | Group |
|-------|--------------------------------------------------------------------------------------------|-------------------------------------------------------------------|-------|
| Tx    | <a name="srvccPsToCsRequest">srvccPsToCsRequest</a>                                        | Number of messages GTPv2-C: SRVCC PS to CS Request.               |       |
| Rx    | <a name="srvccPsToCsResponse">srvccPsToCsResponse</a>                                      | Number of messages GTPv2-C: SRVCC PS to CS Response.              |       |
| Rx    | <a name="pagingReqsrvccPsToCsCompleteNotificationuest">srvccPsToCsCompleteNotification</a> | Number of messages GTPv2-C: SRVCC PS to CS Complete Notification. |       |
| Tx    | <a name="srvccPsToCsCompleteAcknowledge">srvccPsToCsCompleteAcknowledge</a>                | Number of messages GTPv2-C: SRVCC PS to CS Complete Acknowledge.  |       |

#### File Sample ####

```csv
tx,srvccPsToCsRequest,,0
rx,srvccPsToCsResponse,,0
rx,srvccPsToCsCompleteNotification,,0
tx,srvccPsToCsCompleteAcknowledge,,0
```