---
title: "S1 Bearer Deactivation"
description: "Статистика процедур S1 Bearer Deactivation"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1BearerDeactivation\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур 
S1 Bearer Deactivation.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Метрика                                                                                 | Описание                                                                                                                                         | Группа         |
|-------|-----------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| Tx    | <a name="deactivateEpsBearerContextRequest">deactivateEpsBearerContextRequest</a>       | Количество сообщений S1 Deactivate EPS Bearer Context Request.                                                                                   | IMSIPLMN:Value |
| Rx    | <a name="deactivateEpsBearerContextAccept">deactivateEpsBearerContextAccept</a>         | Количество сообщений S1 Deactivate EPS Bearer Context Accept.                                                                                    | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest">defaultBearerDeactivationRequest</a>         | Количество сообщений S1 Deactivate EPS Bearer Context Request с `EBI = 5`.                                                                       | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest26">defaultBearerDeactivationRequest26</a>     | Количество сообщений S1 Deactivate EPS Bearer Context Request с `EBI = 5` и причиной "#26 Insufficient resources".                               | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest36">defaultBearerDeactivationRequest36</a>     | Количество сообщений S1 Deactivate EPS Bearer Context Request с `EBI = 5` и причиной "#36 Regular deactivation".                                 | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest38">defaultBearerDeactivationRequest38</a>     | Количество сообщений S1 Deactivate EPS Bearer Context Request с `EBI = 5` и причиной "#38 Network failure".                                      | IMSIPLMN:Value |
| Tx    | <a name="defaultBearerDeactivationRequest39">defaultBearerDeactivationRequest39</a>     | Количество сообщений S1 Deactivate EPS Bearer Context Request с `EBI = 5` и причиной "#39 Reactivation requested".                               | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest">dedicatedBearerDeactivationRequest</a>     | Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем `Dedicated Bearer ID`.                                         | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest26">dedicatedBearerDeactivationRequest26</a> | Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем `Dedicated Bearer ID` и причиной "#26 Insufficient resources". | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest36">dedicatedBearerDeactivationRequest36</a> | Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем `Dedicated Bearer ID` и причиной "#36 Regular deactivation".   | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest38">dedicatedBearerDeactivationRequest38</a> | Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем `Dedicated Bearer ID` и причиной "#38 Network failure".        | IMSIPLMN:Value |
| Tx    | <a name="dedicatedBearerDeactivationRequest39">dedicatedBearerDeactivationRequest39</a> | Количество сообщений S1 Deactivate EPS Bearer Context Request с заполненным полем `Dedicated Bearer ID` и причиной "#39 Reactivation requested". | IMSIPLMN:Value |
| Rx    | <a name="pdnDisconnectRequest">pdnDisconnectRequest</a>                                 | Количество сообщений S1 PDN Disconnect Request.                                                                                                  | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectSuccess">pdnDisconnectSuccess</a>                                 | Количество сообщений S1 Deactivate EPS Bearer Context Request в ответ на запрос S1 PDN Disconnect Request.                                       | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject">pdnDisconnectReject</a>                                   | Количество сообщений S1 PDN Disconnect Reject.                                                                                                   | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject35">pdnDisconnectReject35</a>                               | Количество сообщений S1 PDN Disconnect Reject с причиной "#35 PTI already in use".                                                               | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject43">pdnDisconnectReject43</a>                               | Количество сообщений S1 PDN Disconnect Reject с причиной "#43 Invalid EPS bearer identity".                                                      | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject49">pdnDisconnectReject49</a>                               | Количество сообщений S1 PDN Disconnect Reject с причиной "#49 Last PDN disconnection not allowed".                                               | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject96">pdnDisconnectReject96</a>                               | Количество сообщений S1 PDN Disconnect Reject с причиной "#96 Invalid mandatory information".                                                    | IMSIPLMN:Value |
| Tx    | <a name="pdnDisconnectReject111">pdnDisconnectReject111</a>                             | Количество сообщений S1 PDN Disconnect Reject с причиной "#111 Protocol error, unspecified".                                                     | IMSIPLMN:Value |

**Примечание.** Описание причин ошибок ESM см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Группа

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
IMSIPLMN,25099
```

#### Пример файла ####

```csv
tx,deactivateEpsBearerContextRequest,,0
rx,deactivateEpsBearerContextAccept,,0
tx,defaultBearerDeactivationRequest,,0
tx,dedicatedBearerDeactivationRequest,,0
rx,pdnDisconnectRequest,,0
tx,pdnDisconnectReject,,0
```
