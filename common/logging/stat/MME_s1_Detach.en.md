---
title: "S1 Detach"
description: "S1 Detach Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1Detach\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the Detach procedures in the S1 interface.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).

| Tx/Rx | Metrics                                                   | Description                                                                                                                                  | Group                     |
|-------|-----------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|---------------------------|
| Rx    | <a name="detachRequest">detachRequest (UE-initiated)</a>  | Number of messages S1 Detach Request with `Detach type = EPS detach`, UE-initiated.                                                          | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccess">detachSuccess</a>                 | Number of messages S1 Detach Accept in the EPS Detach procedure, responding to Detach Request with `Detach type = EPS detach`.               | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachRequestImsi">detachRequestImsi</a>         | Number of messages S1 Detach Request with `Detach type = IMSI detach`, UE-initiated.                                                         | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccessImsi">detachSuccessImsi</a>         | Number of messages S1 Detach Accept in the EPS Detach procedure, responding to Detach Request with `Detach type = IMSI detach`.              | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachRequestCombined">detachRequestCombined</a> | Number of messages S1 Detach Request with `Detach type = combined EPS/IMSI detach`, UE-initiated.                                            | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachSuccessCombined">detachSuccessCombined</a> | Number of messages S1 Detach Accept in the EPS Detach procedure, responding to Detach Request with `Detach type = combined EPS/IMSI detach`. | TAI:Value, IMSIPLMN:Value |
| Tx    | <a name="detachRequestMmeInit">detachRequestMmeInit</a>   | Number of messages S1 Detach Request, MME-initiated.                                                                                         | TAI:Value, IMSIPLMN:Value |
| Rx    | <a name="detachSuccessMmeInit">detachSuccessMmeInit</a>   | Number of messages S1 Detach Accept in the EPS Detach procedure, responding to Detach Request, MME-initiated.                                | TAI:Value, IMSIPLMN:Value |

### Group ###

| Field       | Description                                            | Type |
|-------------|--------------------------------------------------------|------|
| TAI         | Tracking Area Identifier. Format:<br>`<plmn_id><tac>`. | hex  |
| \<plmn_id\> | PLMN identifier.                                       | int  |
| \<tac\>     | Tracking Area Code.                                    | hex  |
| IMSIPLMN    | PLMN identifier based on the first 5 digits of IMSI.   | int  |

#### Group Sample ####

```
TAI:2500103E8
IMSIPLMN:25099
```

#### File Sample ####

```csv
rx,detachRequest,,0
rx,detachSuccess,,1
rx,detachSuccess,IMSIPLMN:00101,1
rx,detachSuccess,TAI:001010001,1
tx,detachRequestMmeInit,,1
tx,detachRequestMmeInit,IMSIPLMN:00101,1
tx,detachRequestMmeInit,TAI:001010001,1
tx,detachSuccessMmeInit,,0
```