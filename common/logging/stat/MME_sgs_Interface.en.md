---
title: "SGs interface"
description: "SGs Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-sgsInterface\_\<datetime\>\_\<granularity\>.csv** contains statistical information on MME metrics for the SGs interface.

### Field Description ###

For more information, see [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).

| Tx/Rx | Metrics                                                             | Description                                       | Group     |
|-------|---------------------------------------------------------------------|---------------------------------------------------|-----------|
| Tx    | <a name="sGsApLocationUpdateRequest">sGsApLocationUpdateRequest</a> | Number of messages SGsAP-LOCATION-UPDATE-REQUEST. | TAI:Value |
| Rx    | <a name="sGsApLocationUpdateAccept">sGsApLocationUpdateAccept</a>   | Number of messages SGsAP-LOCATION-UPDATE-ACCEPT.  | TAI:Value |

### Group ###

| Field       | Description                                            | Type |
|-------------|--------------------------------------------------------|------|
| TAI         | Tracking Area Identifier. Format:<br>`<plmn_id><tac>`. | hex  |
| \<plmn_id\> | PLMN identifier.                                       | int  |
| \<tac\>     | Tracking Area Code.                                    | hex  |

#### Group Sample ####

```
TAI:2500103E8
```

#### File Sample ####

```csv
tx,sGsApLocationUpdateRequest,,7
tx,sGsApLocationUpdateRequest,TAI:001010001,7
rx,sGsApLocationUpdateAccept,,7
rx,sGsApLocationUpdateAccept,TAI:001010001,7
```