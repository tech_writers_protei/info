---
title: "Users"
description: "Subscriber and Session Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-users\_\<datetime\>\_\<granularity\>.csv** contains statistical information on MME metrics for the number of subscribers and sessions.

### Field Description ###

| Tx/Rx | Metrics                                                                                           | Description                                                       | Group                                |
|-------|---------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|--------------------------------------|
|       | <a name="realTimePdnConnectionNumber">realTimePdnConnectionNumber</a>                             | Number of active PDN connections.                                 | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeDedicatedBearerNumber">realTimeDedicatedBearerNumber</a>                         | Number of active dedicated bearer services.                       | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtEcmIdleStatus">realTimeAttachedUsersAtEcmIdleStatus</a>           | Number of registered subscribers with the ECM-IDLE state.         | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtEcmConnectedStatus">realTimeAttachedUsersAtEcmConnectedStatus</a> | Number of registered subscribers with the ECM-CONNECTED state.    | TAI:Value, IMSIPLMN:Value, APN:Value |
|       | <a name="realTimeAttachedUsersAtDeregStatus">realTimeAttachedUsersAtDeregStatus</a>               | Number of registered subscribers with the EMM-DEREGISTERED state. | TAI:Value, IMSIPLMN:Value, APN:Value |

### Group ###

| Field       | Description                                                         | Type   |
|-------------|---------------------------------------------------------------------|--------|
| TAI         | Tracking Area Identifier. Format:<br>`<plmn_id><tac>`.              | hex    |
| \<plmn_id\> | PLMN identifier.                                                    | int    |
| \<tac\>     | Tracking Area Code.                                                 | hex    |
| IMSIPLMN    | PLMN identifier based on the first 5 digits of IMSI.                | int    |
| APN         | Access Point Name. Format:<br>`<name>.mnc<mnc>.mcc<mcc>.<network>`. | string |
| \<name\>    | Name.                                                               | string |
| \<mnc\>     | Mobile Network Code.                                                | int    |
| \<mcc\>     | Mobile Country Code.                                                | int    |
| \<network\> | Network type.                                                       | string |

#### Group Sample ####

```
TAI:2500103E8
IMSIPLMN:25099
APN:internet.mnc01.mcc250.gprs
```

#### File Sample ####

```csv
,realTimeAttachedUsersAtEcmIdleStatus,,1
,realTimeAttachedUsersAtEcmIdleStatus,IMSIPLMN:00101,1
,realTimeAttachedUsersAtEcmIdleStatus,TAI:001010001,1
,realTimeAttachedUsersAtEcmConnectedStatus,,2
,realTimeAttachedUsersAtEcmConnectedStatus,IMSIPLMN:00101,1
,realTimeAttachedUsersAtEcmConnectedStatus,IMSIPLMN:20893,1
,realTimeAttachedUsersAtEcmConnectedStatus,TAI:001010010,1
,realTimeAttachedUsersAtEcmConnectedStatus,TAI:208930001,1
,realTimeUsersAtEmmDeregisteredStatus,,0
,realTimePdnConnectionNumber,,3
,realTimePdnConnectionNumber,IMSIPLMN:00101,2
,realTimePdnConnectionNumber,IMSIPLMN:20893,1
,realTimePdnConnectionNumber,TAI:001010001,1
,realTimePdnConnectionNumber,TAI:001010010,1
,realTimePdnConnectionNumber,TAI:208930001,1
,realTimeDedicatedBearerNumber,,0
```