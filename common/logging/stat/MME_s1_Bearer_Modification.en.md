---
title: "S1 Bearer Modification"
description: "S1 Bearer Modification Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1BearerModification\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the S1 Bearer Modification procedures.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

| Tx/Rx | Metrics                                                                       | Description                                                                                                                                        | Group          |
|-------|-------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| Tx    | <a name="pgwInitBearerModRequest">pgwInitBearerModRequest</a>                 | Number of messages S1 Modify EPS Bearer Context Request in the PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD) procedures.                 | IMSIPLMN:Value |
| Rx    | <a name="pgwInitBearerModSuccess">pgwInitBearerModSuccess</a>                 | Number of messages S1 Modify EPS Bearer Context Accept in the PGW Initiated Bearer Modification (PGW_INIT_BEARER_MOD) procedures.                  | IMSIPLMN:Value |
| Tx    | <a name="hssInitBearerModRequest">hssInitBearerModRequest</a>                 | Number of messages S1 Modify EPS Bearer Context Request in the HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) procedures. | IMSIPLMN:Value |
| Rx    | <a name="hssInitBearerModSuccess">hssInitBearerModSuccess</a>                 | Number of messages S1 Modify EPS Bearer Context Accept in the HSS Initiated Subscribed QoS Modification (HSS_INIT_SUBSCRIBED_QOS_MOD) procedures.  | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject">modifyEpsBearerContextReject</a>       | Number of messages S1 Modify EPS Bearer Context Reject.                                                                                            | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject26">modifyEpsBearerContextReject26</a>   | Number of messages S1 Modify EPS Bearer Context Reject with the cause "#26 Insufficient resources".                                                | IMSIPLMN:Value |
| Rx    | <a name="modifyEpsBearerContextReject111">modifyEpsBearerContextReject111</a> | Number of messages S1 Modify EPS Bearer Context Reject with the cause "#111 Protocol error, unspecified".                                          | IMSIPLMN:Value |
| Rx    | <a name="ueInitBearerResModRequest">ueInitBearerResModRequest</a>             | Number of messages S1 Bearer Resource Modification Request.                                                                                        | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject">ueInitBearerResModReject</a>               | Number of messages S1 Bearer Resource Modification Reject.                                                                                         | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject26">ueInitBearerResModReject26</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#26 Insufficient resources".                                             | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject30">ueInitBearerResModReject30</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#30 Request rejected by Serving GW or PDN GW".                           | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject31">ueInitBearerResModReject31</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#31 Request rejected, unspecified".                                      | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject32">ueInitBearerResModReject32</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#32 Service option not supported".                                       | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject33">ueInitBearerResModReject33</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#33 Requested service option not subscribed".                            | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject37">ueInitBearerResModReject37</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#37 EPS QoS not accepted".                                               | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject59">ueInitBearerResModReject59</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#59 Unsupported QCI value".                                              | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject96">ueInitBearerResModReject96</a>           | Number of messages S1 Bearer Resource Modification Reject with the cause "#96 Invalid mandatory information".                                      | IMSIPLMN:Value |
| Tx    | <a name="ueInitBearerResModReject111">ueInitBearerResModReject111</a>         | Number of messages S1 Bearer Resource Modification Reject with the cause "#111 Protocol error, unspecified".                                       | IMSIPLMN:Value |

**Note.** The ESM error causes see in [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

### Group

| Field    | Description                                          | Type |
|----------|------------------------------------------------------|------|
| IMSIPLMN | PLMN identifier based on the first 5 digits of IMSI. | int  |

#### Group Sample ####

```
IMSIPLMN:25099
```

#### File Sample ####

```csv
tx,pgwInitBearerModRequest,,0
rx,pgwInitBearerModSuccess,,0
tx,hssInitBearerModRequest,,0
rx,hssInitBearerModSuccess,,0
rx,modifyEpsBearerContextReject,,0
rx,ueInitBearerResModRequest,,0
tx,ueInitBearerResModReject,,0
```