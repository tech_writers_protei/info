---
title: "MME Paging"
description: "Статистика процедур пейджинга"
weight: 20
type: docs
---

Файл **<node_name>\_MME-paging\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедуры Paging.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf) и 
[3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

| Tx/Rx | Метрика                                                                   | Описание                                                                                                                                                                               | Группа    |
|-------|---------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Tx    | <a name="s1PagingAttempt">s1PagingAttempt</a>                             | Количество попыток отправить сообщение S1 Paging.                                                                                                                                      | TAI:Value |
| Tx    | <a name="s1PagingRequest">s1PagingRequest</a>                             | Количество сообщений S1 Paging Request.                                                                                                                                                | TAI:Value |
| Rx    | <a name="s1PagingSuccess">s1PagingSuccess</a>                             | Количество сообщений S1 TAU, Service Request и Extended Service Request в ответ на S1 Paging Request.                                                                                  | TAI:Value |
| Tx    | <a name="s1PagingAttemptPS">s1PagingAttemptPS</a>                         | Количество попыток отправить сообщение S1 Paging Request с `CN-Domain = PS`.                                                                                                           | TAI:Value |
| Tx    | <a name="s1PagingRequestPS">s1PagingRequestPS</a>                         | Количество сообщений S1 Paging Request с `CN-Domain = PS`.                                                                                                                             | TAI:Value |
| Rx    | <a name="s1PagingSuccessPS">s1PagingSuccessPS</a>                         | Количество сообщений S1 TAU и Service Request в ответ на запрос S1 Paging Request.                                                                                                     | TAI:Value |
| Tx    | <a name="s1PagingRequestLastEnodebPS">s1PagingRequestLastEnodebPS</a>     | Количество сообщений S1 Paging Request с `CN-Domain = PS` последним посещенным абонентами eNodeB.                                                                                      | TAI:Value |
| Tx    | <a name="s1PagingRequestLastTaPS">s1PagingRequestLastTaPS</a>             | Количество сообщений S1 Paging Request с `CN-Domain = PS` всем eNodeB последних посещенных областей отслеживания, Tracking Area.                                                       | TAI:Value |
| Tx    | <a name="s1PagingAttemptCsfb">s1PagingAttemptCsfb</a>                     | Количество попыток отправить сообщение S1 Paging Request с `CN-Domain = CS` во время процедуры CS Fallback.                                                                            | TAI:Value |
| Tx    | <a name="s1PagingRequestCsfb">s1PagingRequestCsfb</a>                     | Количество сообщений S1 Paging Request с `CN-Domain = CS` во время процедуры CS Fallback после получения сообщения SGs-PAGING-REQUEST с `Service Indicator Value = CS Call Indicator`. | TAI:Value |
| Rx    | <a name="s1PagingSuccessCsfb">s1PagingSuccessCsfb</a>                     | Количество сообщений S1 Extended Service Request в ответ на запрос S1 Paging Request с `CN-Domain = CS` во время процедуры CS Fallback.                                                | TAI:Value |
| Tx    | <a name="s1PagingRequestCsfbLastEnodeb">s1PagingRequestCsfbLastEnodeb</a> | Количество сообщений S1 Paging Request с `CN-Domain = CS` во время процедуры CS fallback последним посещенным абонентами eNodeB.                                                       | TAI:Value |
| Tx    | <a name="s1PagingRequestCsfbLastTa">s1PagingRequestCsfbLastTa</a>         | Количество сообщений S1 Paging Request с `CN-Domain = СS` всем eNodeB последних посещенных областей отслеживания, Tracking Area.                                                       | TAI:Value |

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
tx,s1PagingAttempt,,34
tx,s1PagingAttempt,TAI:001010001,33
tx,s1PagingAttempt,TAI:999990001,1
tx,s1PagingRequest,,50
tx,s1PagingRequest,TAI:001010001,46
tx,s1PagingRequest,TAI:999990001,4
rx,s1PagingSuccess,,34
rx,s1PagingSuccess,TAI:001010001,33
rx,s1PagingSuccess,TAI:999990001,1
tx,s1PagingAttemptPS,,34
tx,s1PagingAttemptPS,TAI:001010001,33
tx,s1PagingAttemptPS,TAI:999990001,1
tx,s1PagingRequestPS,,50
tx,s1PagingRequestPS,TAI:001010001,46
tx,s1PagingRequestPS,TAI:999990001,4
rx,s1PagingSuccessPS,,34
rx,s1PagingSuccessPS,TAI:001010001,33
rx,s1PagingSuccessPS,TAI:999990001,1
tx,s1PagingRequestLastEnodebPS,,46
tx,s1PagingRequestLastEnodebPS,TAI:001010001,46
tx,s1PagingRequestLastTaPS,,4
tx,s1PagingRequestLastTaPS,TAI:999990001,4
tx,s1PagingAttemptCsfb,,0
tx,s1PagingRequestCsfb,,0
rx,s1PagingSuccessCsfb,,0
tx,s1PagingRequestCsfbLastEnodeb,,0
tx,s1PagingRequestCsfbLastTa,,0
```