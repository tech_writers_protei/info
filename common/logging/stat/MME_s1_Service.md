---
title: "S1 Service"
description: "Статистика процедур S1/S1AP Service"
weight: 20
type: docs
---

Файл **<node_name>\_MME-s1Service\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам MME для процедур S1/S1AP Service.

### Описание параметров ###

Подробную информацию см. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf) и
[3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).

| Tx/Rx | Метрика                                                                               | Описание                                                                                                                                                                                             | Группа    |
|-------|---------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|
| Rx    | <a name="ExtendedServiceRequest">ExtendedServiceRequest</a>                           | Количество сообщений S1 Extended Service Request.                                                                                                                                                    | TAI:Value |
| Rx    | <a name="ExtendedServiceRequestMoCsfb">ExtendedServiceRequestMoCsfb</a>               | Количество сообщений S1 Extended Service Request, mobile originating CS fallback or 1xCS fallback.                                                                                                   | TAI:Value |
| Rx    | <a name="ExtendedServiceRequestMtCsfb">ExtendedServiceRequestMtCsfb</a>               | Количество сообщений S1 Extended Service Request, mobile terminating CS fallback or 1xCS fallback.                                                                                                   | TAI:Value |
| Rx    | <a name="ExtendedServiceRequestMoCsfbE">ExtendedServiceRequestMoCsfbE</a>             | Количество сообщений S1 Extended Service Request, mobile originating CS fallback emergency call or 1xCS fallback emergency call.                                                                     | TAI:Value |
| Rx    | <a name="ExtendedServiceSuccess">ExtendedServiceSuccess</a>                           | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE или S1AP: UE CONTEXT MODIFICATION RESPONSE.                                                                                                | TAI:Value |
| Tx    | <a name="csfbMtUeContextModificationRequest">csfbMtUeContextModificationRequest</a>   | Количество сообщений S1AP: UE CONTEXT MODIFICATION REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном сетью.                                                             | TAI:Value |
| Rx    | <a name="csfbMtUeContextModificationResponse">csfbMtUeContextModificationResponse</a> | Количество сообщений S1AP: UE CONTEXT MODIFICATION RESPONSE в ответ на сообщения S1AP: UE CONTEXT MODIFICATION REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном сетью. | TAI:Value |
| Tx    | <a name="csfbMoUeContextModificationRequest">csfbMoUeContextModificationRequest</a>   | Количество сообщений S1AP: UE CONTEXT MODIFICATION REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном абонентом.                                                         | TAI:Value |
| Rx    | <a name="csfbMoUeContextModificationResponse">csfbMoUeContextModificationResponse</a> | Количество сообщений S1AP: UE CONTEXT MODIFICATION RESPONSE в ответ на запросы S1AP: UE CONTEXT MODIFICATION REQUEST с заполненным полем `CS Fallback Indicator`.                                    | TAI:Value |
| Tx    | <a name="csfbMtInitialContextSetupRequest">csfbMtInitialContextSetupRequest</a>       | Количество сообщений S1AP: INITIAL CONTEXT SETUP REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном сетью.                                                               | TAI:Value |
| Rx    | <a name="csfbMtInitialContextSetupResponse">csfbMtInitialContextSetupResponse</a>     | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE в ответ на S1AP: INITIAL CONTEXT SETUP REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном сетью.               | TAI:Value |
| Tx    | <a name="csfbMoInitialContextSetupRequest">csfbMoInitialContextSetupRequest</a>       | Количество сообщений S1AP: INITIAL CONTEXT SETUP REQUEST с заполненным полем `CS Fallback Indicator` при вызове, инициированном абонентом.                                                           | TAI:Value |
| Rx    | <a name="csfbMoInitialContextSetupResponse">csfbMoInitialContextSetupResponse</a>     | Количество сообщений S1AP: INITIAL CONTEXT SETUP RESPONSE в ответ на запросы S1AP: INITIAL CONTEXT SETUP REQUEST с заполненным полем `CS Fallback Indicator`.                                        | TAI:Value |
| Rx    | <a name="ServiceRequest">ServiceRequest</a>                                           | Количество сообщений S1 Service Request.                                                                                                                                                             | TAI:Value |
| Rx    | <a name="ServiceSuccess">ServiceSuccess</a>                                           | Количество сообщений S1AP: INITIAL CONTEXT SETUP COMPLETE в ответ на запросы S1 Service Request.                                                                                                     | TAI:Value |
| Tx    | <a name="ServiceReject">ServiceReject</a>                                             | Количество сообщений S1 Service Reject в ответ на запросы S1 Service Request или S1 Extended Service Request.                                                                                        | TAI:Value |

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| TAI         | Идентификатор области отслеживания. Формат:<br>`<plmn_id><tac>`. | hex |
| \<plmn_id\> | Идентификатор сети PLMN.                                         | int |
| \<tac\>     | Код области отслеживания.                                        | hex |

#### Пример ####

```
TAI:2500103E8
```

#### Пример файла ####

```csv
rx,ExtendedServiceRequest,,0
rx,ExtendedServiceRequestMoCsfb,,0
rx,ExtendedServiceRequestMtCsfb,,0
rx,ExtendedServiceRequestMoCsfbE,,0
rx,ExtendedServiceSuccess,,0
tx,csfbMtUeContextModificationRequest,,0
rx,csfbMtUeContextModificationResponse,,0
tx,csfbMoUeContextModificationRequest,,0
rx,csfbMoUeContextModificationResponse,,0
tx,csfbMtInitialContextSetupRequest,,0
rx,csfbMtInitialContextSetupResponse,,0
tx,csfbMoInitialContextSetupRequest,,0
rx,csfbMoInitialContextSetupResponse,,0
rx,ServiceRequest,,137
rx,ServiceRequest,TAI:001010001,121
rx,ServiceRequest,TAI:250010001,16
rx,ServiceSuccess,,122
rx,ServiceSuccess,TAI:001010001,121
rx,ServiceSuccess,TAI:250010001,1
tx,ServiceReject,,16
tx,ServiceReject,TAI:001010001,1
tx,ServiceReject,TAI:250010001,15
```