---
title: "S1 Security"
description: "NAS Procedure Statistics"
weight: 20
type: docs
---

The file **<node_name>\_MME-s1Security\_\<datetime\>\_\<granularity\>.csv** contains statistical
information on MME metrics for the NAS protocol.

### Field Description ###

For more information, see [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf) и
[3GPP TS 24.501](https://www.etsi.org/deliver/etsi_ts/124500_124599/124501/17.10.01_60/ts_124501v171001p.pdf).

| Tx/Rx | Metrics                                                               | Description                                                                   | Group |
|-------|-----------------------------------------------------------------------|-------------------------------------------------------------------------------|-------|
| Tx    | <a name="securityModeCommand">securityModeCommand</a>                 | Number of messages NAS SECURITY MODE COMMAND.                                 |       |
| Rx    | <a name="securityModeCommandComplete">securityModeCommandComplete</a> | Number of messages NAS SECURITY MODE COMPLETE.                                |       |
| Rx    | <a name="securityModeCommandReject">securityModeCommandReject</a>     | Number of messages NAS SECURITY MODE REJECT.                                  |       |
| Tx    | <a name="authenticationRequest">authenticationRequest</a>             | Number of messages NAS AUTHENTICATION REQUEST.                                |       |
| Rx    | <a name="authenticationResponse">authenticationResponse</a>           | Number of messages NAS AUTHENTICATION RESPONSE.                               |       |
| Tx    | <a name="authenticationReject">authenticationReject</a>               | Number of messages NAS AUTHENTICATION REJECT.                                 |       |
| Rx    | <a name="authenticationFailure">authenticationFailure</a>             | Number of messages NAS AUTHENTICATION Failure                                 |       |
| Rx    | <a name="authenticationFailure21">authenticationFailure21</a>         | Number of messages NAS AUTHENTICATION FAILURE with cause "#21 Synch failure". |       |

**Note.** The EMM error causes see in [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/15.08.00_60/ts_124301v150800p.pdf).

#### File Sample ####

```csv
tx,securityModeCommand,,8
rx,securityModeCommandComplete,,8
rx,securityModeCommandReject,,0
tx,authenticationRequest,,14
rx,authenticationResponse,,7
tx,authenticationReject,,0
rx,authenticationFailure,,7
rx,authenticationFailure21,,7
```
