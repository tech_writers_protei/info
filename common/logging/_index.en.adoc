---
title: "Journals"
description: "Description of journals and statistics"
weight: 50
type: docs
---

The Protei_MME node keeps the following journals:

* EDR, stored in `/usr/protei/cdr/Protei_MME/`:

** link:./edr/connect/[connect_cdr] -- PDN connection event EDR file;
** link:./edr/dedicated_bearer/[dedicated_bearer_cdr] -- dedicated bearer service event EDR file;
** link:./edr/diam/[diam_cdr] -- Diameter event EDR file;
** link:./edr/enodeb/[enodeb_cdr] -- eNodeB event EDR file;
** link:./edr/gtp_c/[gtp_c_cdr] -- GTP-C event EDR file;
** link:./edr/gtp_c_overload/[gtp_c_overload] -- GTP-C overload event EDR file;
** link:./edr/http/[http_cdr] -- HTTP event EDR file;
** link:./edr/irat_handover/[irat_handover_cdr] -- inter-RAT handover EDR file;
** link:./edr/lte_handover/[lte_handover_cdr] -- LTE handover EDR file;
** link:./edr/paging/[paging_cdr] -- Paging event EDR file;
** link:./edr/reject/[reject_cdr] -- rejected request event EDR file;
** link:./edr/s1ap/[s1ap_cdr] -- S1AP event EDR file;
** link:./edr/s1ap_context/[s1ap_context_cdr] -- S1AP context event EDR file;
** link:./edr/s1ap_overload/[s1ap_overload_cdr] -- S1AP overload event EDR file;
** link:./edr/sgsap/[sgsap_cdr] -- SGsAP event EDR file;
** link:./edr/tau/[tau_cdr] -- Tracking-Area-Update event EDR file;
+
* log, stored in `/usr/protei/log/Protei_MME/`:
** alarm -- alarm event log file;
** alarm_cdr -- alarm subsystem CDR log file;
** alarm_trace -- alarm subsystem action log file;
** bc_trace -- base component action log file;
** bc_warning -- base component alert log file;
** COM_trace -- component configuration subsystem action log file;
** COM_warning -- component configuration subsystem alert log file;
** config -- subscriber list, dictionary, and configuration file uploads log file
** db_trace -- MME database action log file;
** diam_info -- Diameter event log file;
** diam_trace -- Diameter action log file;
** diam_warning -- Diameter alert log file;
** dns_trace -- DNS protocol action log file;
** dns_warning -- DNS alert log file;
** GTP_C_trace -- GTP Control Plane: GTPv1-C and GTPv2, action log file;
** GTP_C_warning -- GTP Control Plane: GTPv1-C and GTPv2, alert log file;
** http_trace -- HTTP interface action log file;
** mme_config -- configuration file: *mme.cfg*, *served_plmn.cfg*, and *{asterisk}_rules* log file;
** profilers -- application logic and resources utilization log file;
** S1AP_trace -- S1AP action log file;
** S1AP_warning -- S1AP alert log file;
** sctp_binary -- SCTP connection dump file;
** Sg_info -- Signaling subsystem event log file;
** Sg_trace -- Signaling subsystem action log file;
** Sg_warning -- Signaling subsystem alert log file;
** SGsAP_trace -- SGsAP action log file;
** SGsAP_warning -- SGsAP alert log file;
** si -- socket interface action log file;
** si_info -- socket interface event log file;
** si_warning -- socket interface alert log file;
** trace -- action log file;
** ue_trace -- UE action log file.

The PROTEI MME node keeps the following files with statistics, stored in `/usr/protei/cdr/Protei_MME/metrics/`:

* MME_Diameter.csv -- MME metric statistics for the Diameter: Base procedures;
* MME_handover.csv -- MME metric statistics for the handover procedures;
* MME_paging.csv -- MME metric statistics for the Paging procedures;
* MME_resource.csv -- MME metric statistics for the server resources utilization;
* MME_s11_Interface.csv -- MME metric statistics for the S11 procedures;
* MME_s1_Attach.csv -- MME metric statistics for the S1 Attach procedures;
* MME_s1_Bearer_Activation.csv -- MME metric statistics for the S1 Bearer Activation procedures;
* MME_s1_Bearer_Deactivation.csv -- MME metric statistics for the S1 Bearer Deactivation procedures;
* MME_s1_Bearer_Modification.csv -- MME metric statistics for the S1 Bearer Modification procedures;
* MME_s1_Detach.csv -- MME metric statistics for S1 Detach procedures;
* MME_s1_Interface.csv -- MME metric statistics for the S1AP procedures;
* MME_s1_Security.csv -- MME metric statistics for the NAS procedures;
* MME_s1_Service.csv -- MME metric statistics for the S1/S1AP Service procedures;
* MME_S6a_interface.csv -- MME metric statistics for the S6a procedures;
* MME_sgs_Interface.csv -- MME metric statistics for the SGs procedures;
* MME_sv_Interface.csv -- MME metric statistics for the Sv procedures;
* MME_tau.csv -- MME metric statistics for the TRACKING AREA UPDATE procedures;
* MME_users.csv -- MME metric statistics for subscribers and sessions.