---
title: "trace.json"
description: "Параметры ведения xDR и log"
weight: 20
type: docs
---

В файле задаются настройки подсистемы журналирования.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload trace**, см.
[Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр                                        | Описание                                                                                                                                                                                        | Тип      | O/M | P/R | Версия |
|-------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| <a name="common">common</a>                     | Общие настройки системы журналирования.                                                                                                                                                         | object   | O   | R   |        |
| **{**                                           |                                                                                                                                                                                                 |          |     |     |        |
| &nbsp;&nbsp;tracing                             | Флаг активности системы журналирования.<br>По умолчанию: true.                                                                                                                                  | bool     | O   | R   |        |
| &nbsp;&nbsp;dir                                 | Путь к директории, где находятся журналы.<br>**Примечание.** Путь может содержать ".." и маску формата времени.<br>По умолчанию: от каталога по умолчанию.                                      | string   | O   | R   |        |
| &nbsp;&nbsp;noSignal                            | Коды сигналов, не перехватываемых системой журналирования.<br>`all` -- не перехватывать никакие сигналы.<br>**Примечание.** Разделитель -- запятая.<br>По умолчанию: перехватывать все сигналы. | int      | O   | R   |        |
| &nbsp;&nbsp;<a name="separator">separator</a>   | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.<br>По умолчанию: " ".                                                 | string   | O   | R   |        |
| &nbsp;&nbsp;localWrite                          | Флаг ведения журнала локально.<br>По умолчанию: true.                                                                                                                                           | bool     | O   | R   |        |
| **}**                                           |                                                                                                                                                                                                 |          |     |     |        |
| remoteSide                                      | Параметры удаленного сервера.                                                                                                                                                                   | object   | O   | R   |        |
| **{**                                           |                                                                                                                                                                                                 |          |     |     |        |
| &nbsp;&nbsp;ip                                  | IP-адрес удаленного сервера.                                                                                                                                                                    | ip       | M   | R   |        |
| &nbsp;&nbsp;port                                | Прослушиваемый порт удаленного сервера.                                                                                                                                                         | int      | M   | R   |        |
| **}**                                           |                                                                                                                                                                                                 |          |     |     |        |
| logs                                            | Перечень конфигураций журналов.                                                                                                                                                                 | [object] | O   | R   |        |
| **{**                                           |                                                                                                                                                                                                 |          |     |     |        |
| &nbsp;&nbsp;logName                             | Наименование журнала.                                                                                                                                                                           | string   | O   | R   |        |
| &nbsp;&nbsp;aliases                             | Дополнительные названия журнала.                                                                                                                                                                | [string] | O   | R   |        |
| &nbsp;&nbsp;[mask](#mask)                       | Маска формата вывода автоматических полей в журнале.                                                                                                                                            | [string] | O   | R   |        |
| &nbsp;&nbsp;<a name="file">file</a>             | Путь к файлу лога.<br> По умолчанию: от каталога по умолчанию.                                                                                                                                  | string   | O   | R   |        |
| &nbsp;&nbsp;<a name="level-trace">level</a>     | Уровень журнала.<br>**Примечание.** Сообщения с большим уровнем игнорируются.                                                                                                                   | int      | O   | R   |        |
| &nbsp;&nbsp;localLevel                          | Уровень локального журнала.<br>**Примечание.** Сообщения с большим уровнем игнорируются.                                                                                                        | int      | O   | R   |        |
| &nbsp;&nbsp;[period](#period)                   | Период обновления файла лога. Формат:<br>`<interval>+<shift>`.                                                                                                                                  | string   | O   | R   |        |
| &nbsp;&nbsp;<a name="interval">\<interval\></a> | Период между соседними обновлениями.                                                                                                                                                            | string   | O   | R   |        |
| &nbsp;&nbsp;\<shift\>                           | Первоначальный сдвиг.<br>**Примечание.** Не может превышать [interval](#interval), в случае некорректного значения игнорируется.                                                                | units    | O   | R   |        |
| &nbsp;&nbsp;[buffering](#buffering)             | Настройки буферизированной записи.                                                                                                                                                              | object   | O   | R   |        |
| &nbsp;&nbsp;separator                           | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.<br>По умолчанию: значение [separator](#separator).                    | string   | O   | R   |        |
| &nbsp;&nbsp;[type](#type)                       | Перечень типов журналов и дополнительных настроек.                                                                                                                                              | [string] | O   | R   |        |
| &nbsp;&nbsp;<a name="tee">tee</a>               | Перечень дублирующихся потоков вывода.<br>`stdout` / `cout` / `info` / `<log_file_name>`.<br>**Примечание.** При знаке минуса "–" не пишется имя исходного лога при дублировании.               | [string] | O   | R   |        |
| &nbsp;&nbsp;<a name="limit-trace">limit</a>     | Максимальное количество строк в файле.<br>                                                                                                                                                      | int      | O   | R   |        |
| forceRecreate                                   | Флаг создания пустых журналы по прошествии периода [period](#period).<br>По умолчанию: false.                                                                                                   | bool     | O   | R   |        |
| **}**                                           |                                                                                                                                                                                                 |          |     |     |        |

**Примечание.** В параметре [file](#file) при указании не существующих директорий система создает все необходимые каталоги.
Допускается задание пустого имени файла, если [level](#level-trace) = 0.
Тогда запись производится согласно параметру [tee](#tee).
При отсутствии запись на диск не производится.
Путь может содержать ".." и маску формата времени.

**Примечание.** По достижении предела строк, заданного параметром [limit](#limit-trace) файл автоматически открывается заново.
Действительное количество строк в файле не исследуется.
Если имя файла зависит от времени, то открывается новый файл, иначе файл очищается.

#### Пример ####

```json
{
  "common": {
    "tracing": true,
    "dir": ".",
    "noSignal": "all"
  },
  "logs": [
    {
      "logName": "connect_cdr",
      "file": "cdr/s1ap/connect-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 2,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "dedicated_bearer_cdr",
      "file": "cdr/s1ap/dedicated_bearer-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "diam_cdr",
      "file": "cdr/diam/diam-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "level": 2,
      "separator": ","
    },
    {
      "logName": "enodeb_cdr",
      "file": "cdr/s1ap/enodeB-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "gtp_c_cdr",
      "file": "cdr/gtp_c/gtp_c-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "period": "1day",
      "level": 2,
      "separator": ","
    },
    {
      "logName": "http_cdr",
      "file": "cdr/http/http-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "irat_handover_cdr",
      "file": "cdr/s1ap/irat_handover-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "lte_handover_cdr",
      "file": "cdr/s1ap/lte_handover-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "paging_cdr",
      "file": "cdr/s1ap/paging_cdr-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "reject_cdr",
      "file": "cdr/reject/reject-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "s1ap_cdr",
      "file": "cdr/s1ap/s1ap-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "s1ap_context_cdr",
      "file": "cdr/s1ap/s1ap_context-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "s1ap_overload_cdr",
      "file": "cdr/s1ap_overload/s1ap_overload-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "period": "1hour",
      "level": 2,
      "separator": ","
    },
    {
      "logName": "sgsap_cdr",
      "file": "cdr/sgsap/sgsap-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time" ],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "tau_cdr",
      "file": "cdr/s1ap/tau-%Y%m%d-%H%M.cdr",
      "mask": [ "date", "time", "tick" ],
      "period": "1day",
      "level": 1,
      "tee": [ "s1ap_cdr" ],
      "separator": ","
    },
    {
      "logName": "alarm_cdr",
      "file": "logs/alarm/alarm-cdr.log",
      "period": "hour",
      "mask": [ "date", "time", "tick" ],
      "separator": ";",
      "level": 6
    },
    {
      "logName": "alarm_trace",
      "file": "logs/alarm/alarm_trace.log",
      "period": "hour",
      "mask": [ "date", "time", "tick" ],
      "separator": ";",
      "level": 6
    },
    {
      "logName": "bc_trace",
      "file": "logs/bc_trace.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "bc_warning",
      "file": "logs/bc_warning.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "COM_warning",
      "file": "logs/com_warning.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "config",
      "file": "logs/config.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10
    },
    {
      "logName": "db_trace",
      "file": "logs/db_trace.log",
      "mask": [ "file", "date", "time", "tick" ],
      "period": "1hour",
      "level": 10
    },
    {
      "logName": "diam_info",
      "file": "logs/diam_info.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "diam_trace",
      "file": "logs/diam/diam_trace-%Y%m%d-%H%M.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "period": "hour",
      "level": 19
    },
    {
      "logName": "diam_warning",
      "file": "logs/diam_warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "dns_trace",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "period": "day"
    },
    {
      "logName": "dns_warning",
      "file": "logs/dns_warning.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "period": "day"
    },
    {
      "logName": "GTP_C_trace",
      "file": "logs/gtp_c/gtp_c_trace-%Y%m%d-%H%M.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "period": "hour",
      "level": 11
    },
    {
      "logName": "GTP_C_warning",
      "file": "logs/gtp_c_warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "http_trace",
      "file": "logs/http_trace.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "mme_config",
      "file": "logs/mme_config.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10
    },
    {
      "logName": "profilers",
      "file": "logs/profilers/profilers_%Y%m%d-%H%M.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "period": "hour",
      "separator": ";"
    },
    {
      "logName": "S1AP_trace",
      "file": "logs/s1ap/s1ap_trace-%Y%m%d-%H%M.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "period": "hour",
      "level": 10
    },
    {
      "logName": "S1AP_warning",
      "file": "logs/s1ap_warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "SCTP_binary",
      "file": "logs/sctp_binary.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 0,
      "separator": ";"
    },
    {
      "logName": "Sg_info",
      "file": "logs/sg/sg_info.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "Sg_trace",
      "file": "logs/sg/sg_trace.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "Sg_warning",
      "file": "logs/sg/sg_warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "SGsAP_trace",
      "file": "logs/sgsap/sgsap_trace-%Y%m%d-%H%M.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "period": "hour",
      "level": 10
    },
    {
      "logName": "SGsAP_warning",
      "file": "logs/sgsap_warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10
    },
    {
      "logName": "si",
      "file": "logs/si/trace.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10,
      "tee": [ "trace", "fsm" ]
    },
    {
      "logName": "si_info",
      "file": "logs/si/info.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10,
      "tee": [ "trace" ]
    },
    {
      "logName": "si_warning",
      "file": "logs/si/warning.log",
      "mask": [ "date", "time", "tick", "pid", "file" ],
      "level": 10,
      "trace": [ "trace" ]
    },
    {
      "logName": "trace",
      "file": "logs/trace/trace_%Y%m%d-%H%M.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 11,
      "period": "hour",
      "separator": ";"
    },
    {
      "logName": "ue_trace",
      "file": "logs/trace/ue_trace_%Y%m%d-%H%M.log",
      "mask": [ "file", "date", "time", "tick" ],
      "level": 10,
      "period": "hour",
      "separator": ";"
    }
  ]
}
```

### Модификаторы mask {#mask}

Маска формата вывода автоматических полей в журнале. Возможные модификаторы: `date & time & tick & state & pid & tid & level & file`.

| Параметр                | Описание                                                                                                | Тип        |
|-------------------------|---------------------------------------------------------------------------------------------------------|------------|
| date                    | Дата создания. Формат: `DD/MM/YY`.                                                                      | date       |
| <a name="time">time</a> | Время создания. Формат: `hh:mm:ss`.                                                                     | time       |
| tick                    | Миллисекунды. Формат:<br>если задано [time](#time): `.mss`;<br>если не задано [time](#time): `.mssmss`. | string     |
| state                   | Состояние системы.                                                                                      | int/string |
| pid                     | Идентификатор процесса. Формат: `xxxxxx`.                                                               | int        |
| tid                     | Идентификатор потока. Формат: `xxxxxx`.                                                                 | int        |
| level                   | Уровень журнала для записи.                                                                             | int        |
| file                    | Файл и строка в файле с исходным кодом, откуда производится вывод.                                      | string     |

### Модификаторы period {#period}

| Параметр | Описание                                                                                                                                         | Тип    |
|----------|--------------------------------------------------------------------------------------------------------------------------------------------------|--------|
| count    | Количество стандартных периодов.                                                                                                                 | int    |
| type     | Единицы измерения периода.<br>`sec` - секунда / `min` - минута / `hour` - час / `day` - день / `week` - неделя / `month` - месяц / `year` - год. | string |

**Пример:** `day+3hour` - файл обновляется каждый день в 3 часа ночи.

### Модификаторы type {#type}

Три пары взаимоисключающих значений: log/cdr, truncate/append, name_now/name_period.

| Параметр                        | Описание                                                                                               | Тип      |
|---------------------------------|--------------------------------------------------------------------------------------------------------|----------|
| <a name="name-now">nameNow</a>  | Текущее время для имени файла.                                                                         | datetime |
| namePeriod                      | Начало периода записи.                                                                                 | datetime |
| <a name="truncate">truncate</a> | Флаг очистки файла при открытии.                                                                       | bool     |
| <a name="append">append</a>     | Файл добавления информации в конец файла.                                                              | bool     |
| <a name="log">log</a>           | Комбинация из [truncate](#truncate) и [name_now](#name-now), при падении пишется информация о сигнале. | string   |
| <a name="cdr">cdr</a>           | Комбинация из [append](#append) и [name_now](#name-now), при падении не пишется информация о сигнале.  | string   |

### Модификаторы buffering {#buffering}

| Параметр                               | Описание                                                                                                                 | Тип    |
|----------------------------------------|--------------------------------------------------------------------------------------------------------------------------|--------|
| <a name="cluster-size">clusterSize</a> | Размер кластера, в килобайтах.<br>По умолчанию: 128.                                                                     | int    |
| clustersInBuffer                       | Количество кластеров [cluster_size](#cluster-size) в буфере.<br>По умолчанию: 0.                                         | int    |
| overflowAction                         | Действие, выполняемое при переполнении буфера.<br>`erase` -- удаление / `dump` -- запись на диск.<br>По умолчанию: dump. | string |

### Зарезервированные имена журналов ####

* **stdout** -- стандартный вывод;
* **stderr** -- стандартный вывод ошибок;
* **trace** -- журнал по умолчанию;
* **warning** -- журнал предупреждений;
* **error** -- журнал ошибок;
* **config** -- журнал чтения конфигурации;
* **info** -- журнал информации о событиях, адаптирован для стороннего пользователя.