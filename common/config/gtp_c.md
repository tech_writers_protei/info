---
title: "gtp_c.json"
description: "Параметры компонента GTP-C"
weight: 20
type: docs
---

В файле задаются настройки компонента GTP-C.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload gtp_c**, см. [Управление](../../oam/system_management/).

### Используемые секции ###

* **[localAddress](#local-address-gtp-c)** -- параметры локального хоста;
* **[s11](#s11)** -- параметры интерфейса S11;
* **[s11-u](#s11-u)** -- параметры интерфейса S11-U;
* **[s10](#s10)** -- параметры интерфейса S10;
* **[s3](#s3)** -- параметры интерфейса S3;
* **[sv](#sv)** -- параметры интерфейса Sv;
* **[gnGp](#gngp)** -- параметры интерфейса GnGp;
* **[timers](#timers-gtp-c)** -- параметры таймеров;
* **[repeat](#repeat)** -- параметры повторов;
* **[overload](#overload-gtp-c)** -- параметры перегрузки.

### Описание параметров ###

| Параметр                                                      | Описание                                                                                                                                                                                   | Тип                   | O/M | P/R | Версия    |
|---------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|-----|-----|-----------|
| **<a name="local-address-gtp-c">localAddress</a>**            | Параметры локального хоста.                                                                                                                                                                | object                | M   | P   |           |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;<a name="local-host-gtp-c">localHost</a>          | IP-адрес локального хоста.                                                                                                                                                                 | ip                    | M   | P   |           |
| &nbsp;&nbsp;<a name="local-port-gtp-c">localPort</a>          | Прослушиваемый порт. По умолчанию: 2123.                                                                                                                                                   | int                   | O   | P   |           |
| &nbsp;&nbsp;<a name="dscp">dscp</a>                           | Используемое значение DSCP.                                                                                                                                                                | int                   | O   | P   |           |
| &nbsp;&nbsp;<a name="ip-mtu-discover-gtp-c">ipMtuDiscover</a> | Значение опции `IP_MTU_DISCOVER`. Диапазон: 0-5.<br>По умолчанию: 0.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.                               | int                   | O   | P   |           |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| **<a name="s11">s11</a>**                                     | Параметры интерфейса S11.                                                                                                                                                                  | [Iface](#iface-gtp-c) | O   | P   |           |
| **<a name="s11-u">s11U</a>**                                  | Параметры интерфейса S11-U.                                                                                                                                                                | [Iface](#iface-gtp-c) | O   | P   | 1.15.22.0 |
| **<a name="s10">s10</a>**                                     | Параметры интерфейса S10.                                                                                                                                                                  | [Iface](#iface-gtp-c) | O   | P   |           |
| **<a name="s3">s3</a>**                                       | Параметры интерфейса S3.                                                                                                                                                                   | [Iface](#iface-gtp-c) | O   | P   |           |
| **<a name="sv">sv</a>**                                       | Параметры интерфейса Sv.                                                                                                                                                                   | [Iface](#iface-gtp-c) | O   | P   |           |
| **<a name="gngp">gnGp</a>**                                   | Параметры интерфейса GnGp.                                                                                                                                                                 | object                | O   | P   |           |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;localHost                                         | IP-адрес хоста для интерфейса GnGp.<br>По умолчанию: значение `s3::LocalHost`.                                                                                                             | ip                    | O   | P   |           |
| &nbsp;&nbsp;localPort                                         | Прослушиваемый порт для интерфейса GnGp.<br>По умолчанию: значение `s3::LocalPort`.                                                                                                        | int                   | O   | P   |           |
| &nbsp;&nbsp;dscp                                              | Используемое значение DSCP.<br>По умолчанию: значение `s3::DSCP`.                                                                                                                          | int                   | O   | P   |           |
| &nbsp;&nbsp;ipMtuDiscover                                     | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>По умолчанию: значение `s3::ipMtuDiscover`.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`. | int                   | O   | P   |           |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| **<a name="timers-gtp-c">timers</a>**                         | Параметры таймеров.                                                                                                                                                                        | object                | O   | R   |           |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;<a name="resp-timeout">responseTimeout</a>        | Время ожидания ответного сообщения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                         | int                   | O   | R   |           |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| **<a name="repeat">repeat</a>**                               | Параметры повторов запросов.                                                                                                                                                               | object                | O   | R   |           |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;count                                             | Количество повторных запросов, отправляемых по истечении времени [Response_Timeout](#resp-timeout).<br>По умолчанию: 4.                                                                    | int                   | O   | R   |           |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| **<a name="overload-gtp-c">overload</a>**                     | Параметры перегрузки.                                                                                                                                                                      | object                | O   | R   | 1.1.0.0   |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;enable                                            | Флаг детектирования перегрузки.<br>По умолчанию: 0.                                                                                                                                        | bool                  | O   | R   | 1.1.0.0   |
| &nbsp;&nbsp;ddn                                               | Максимальное количество сообщений GTP-C: Downlink Data Notification в секунду.<br>По умолчанию: 1000.                                                                                      | int                   | O   | R   | 1.1.0.0   |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| **<a name="balancer-gtp-c">balancer</a>**                     | Параметры режима балансировщика.                                                                                                                                                           | object                | O   | R   | 1.1.0.0   |
| **{**                                                         |                                                                                                                                                                                            |                       |     |     |           |
| &nbsp;&nbsp;enable                                            | Флаг активации режима балансировщика.<br>По умолчанию: 0.                                                                                                                                  | bool                  | O   | R   | 1.1.0.0   |
| &nbsp;&nbsp;balancerInnerIp                                   | IP-адрес балансировщика, с которым будет работать MME.                                                                                                                                     | ip                    | M   | P   | 1.1.0.0   |
| &nbsp;&nbsp;s10balancerOuterIp                                | Выходной IP-адрес для S10-интерфейса балансировщика.                                                                                                                                       | ip                    | M   | P   | 1.1.0.0   |
| &nbsp;&nbsp;s11balancerOuterIp                                | Выходной IP-адрес для S11-интерфейса балансировщика.                                                                                                                                       | ip                    | M   | P   | 1.1.0.0   |
| &nbsp;&nbsp;localBalancerPort                                 | Порт узла MME для исходящих сообщений в сторону балансировщика <br>По умолчанию: 2124.                                                                                                     | int                   | O   | P   | 1.1.0.0   |
| **}**                                                         |                                                                                                                                                                                            |                       |     |     |           |

#### Пример ####

```json
{
  "localAddress": {
    "localHost": "192.168.100.1",
    "localPort": 2123
  },
  "s11": {
    "localHost": "192.168.100.2",
    "dscp": 50
  },
  "s11u": {
    "localHost": "192.168.100.3",
    "localPort": 2152
  },
  "s10": {
    "localHost": "192.168.100.6",
    "localPort": 2123,
    "ipMtuDiscover": 100
  },
  "timers": {
    "responseTimeout": 30000
  },
  "repeat": {
    "count": 2
  },
  "overload": {
    "enable": true,
    "ddn": 2000
  },
  "balancer": {
    "enable": true,
    "balancerInnerIp": "192.168.0.8",
    "s10balancerOuterIp": "192.168.1.8",
    "s11balancerOuterIp": "192.168.1.8",
    "localBalancerPort": 2124
  }
}
```

### Тип Iface {#iface-gtp-c}

| Параметр      | Описание                                                                                                                                                                                                        | Тип | O/M | P/R | Версия |
|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----|-----|-----|--------|
| localHost     | IP-адрес хоста для интерфейса.<br>По умолчанию: значение [localAddress::LocalHost](#local-host-gtp-c).                                                                                                          | ip  | O   | P   |        |
| localPort     | Прослушиваемый порт для интерфейса.<br>По умолчанию: значение [localAddress::LocalPort](#local-port-gtp-c).                                                                                                     | int | O   | P   |        |
| dscp          | Используемое значение DSCP.<br>По умолчанию: значение [localAddress::dscp](#dscp).                                                                                                                              | int | O   | P   |        |
| ipMtuDiscover | Значение опции `IP_MTU_DISCOVER`. Диапазон: 0-5.<br>По умолчанию: значение [s3::ipMtuDiscover](#ip-mtu-discover-gtp-c).<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`. | int | O   | P   |        |
