---
title: "sgsap.json"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонента SGsAP.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload sgsap**, см.
[Управление](../../oam/system_management/).

## Используемые секции ##

* **[localAddress](#local-address-sgsap)** -- параметры локального хоста;
* **[localInterfaces](#local-interfaces-sgsap)** -- параметры адресов для multihoming;
* **[sctpAdditionalinfo](#sctp-config-sgsap)** -- дополнительные параметры SCTP;
* **[timers](#timers-sgsap)** -- параметры таймеров;
* **[retryCounters](#retry-counters)** -- параметры счетчиков повторных попыток;
* **[lacGt](#lac-gt)** -- параметры соответствий LAC и GT;
* **[mscPool](#msc-pool)** -- параметры соответствий LAC и наборов GT с параметрами;
* **[overload](#overload-sgsap)** -- параметры перегрузки.

### Описание параметров ###

| Параметр                                                               | Описание                                                                                                                                                                                                                                                                                          | Тип        | O/M | P/R | Версия |
|------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| **<a name="local-address-sgsap">localAddress</a>**                     | Параметры локального хоста.                                                                                                                                                                                                                                                                       | object     | M   | P   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;<a name="local-host-sgsap">localHost</a>                   | IP-адрес локального хоста.                                                                                                                                                                                                                                                                        | string     | M   | P   |        |
| &nbsp;&nbsp;<a name="local-port-sgsap">localPort</a>                   | Прослушиваемый порт.<br>По умолчанию: 29118.                                                                                                                                                                                                                                                      | int        | O   | P   |        |
| &nbsp;&nbsp;**<a name="local-interfaces-sgsap">localInterfaces</a>**   | Параметры адресов для multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>" }`.                                                                                                                                                                                                                 | [{ip:int}] | O   | P   |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="timers-sgsap">timers</a>**                                  | Параметры таймеров.                                                                                                                                                                                                                                                                               | object     | O   | P   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;reconnectTimeout                                           | Время ожидания попытки переподключения после разрыва соединения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                                                                   | int        | O   | P   |        |
| &nbsp;&nbsp;responseTimeout                                            | Время ожидания ответного сообщения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                                                                                                | int        | O   | R   |        |
| &nbsp;&nbsp;ts8                                                        | Таймер Ts8, в миллисекундах.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 4&nbsp;000.                                                                                                                   | int        | O   | R   |        |
| &nbsp;&nbsp;ts9                                                        | Таймер Ts9, в миллисекундах.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 4&nbsp;000.                                                                                                                   | int        | O   | R   |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="retry-counters">retryCounters</a>**                         | Параметры счетчиков повторных попыток.                                                                                                                                                                                                                                                            | object     | O   | R   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;ns8                                                        | Таймер Ns8.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 2.                                                                                                                                             | int        | O   | R   |        |
| &nbsp;&nbsp;ns9                                                        | Таймер Ns9.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 2.                                                                                                                                             | int        | O   | R   |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="lac-gt">lacGt</a>**                                         | Параметры соответствий LAC и GT.                                                                                                                                                                                                                                                                  | {object}   | O   | R   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;defaultGt                                                  | Глобальный заголовок по умолчанию.                                                                                                                                                                                                                                                                | string     | O   | R   |        |
| &nbsp;&nbsp;bindings                                                   | Перечень связок LAC и GT.                                                                                                                                                                                                                                                                         | [object]   | M   | R   |        |
| &nbsp;&nbsp;**{**                                                      |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;plmn                                           | Код сети PLMN.                                                                                                                                                                                                                                                                                    | string     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;lac                                            | Код локальной области LAC.                                                                                                                                                                                                                                                                        | int        | M   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;gt                                             | Глобальный заголовок.                                                                                                                                                                                                                                                                             | string     | M   | R   |        |
| &nbsp;&nbsp;**}**                                                      |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="msc-pool">mscPool</a>**                                     | Перечень соответствий LAC и наборов GT с весами и приоритетами.                                                                                                                                                                                                                                   | [object]   | O   | R   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;plmn                                                       | Код сети PLMN.                                                                                                                                                                                                                                                                                    | string     | O   | R   |        |
| &nbsp;&nbsp;lac                                                        | Код локальной области LAC.                                                                                                                                                                                                                                                                        | int        | M   | R   |        |
| &nbsp;&nbsp;gtList                                                     | Перечень глобальных заголовков с их весами и приоритетами.                                                                                                                                                                                                                                        | [object]   | M   | R   |        |
| &nbsp;&nbsp;**{**                                                      |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;gt                                             | Глобальный заголовок.                                                                                                                                                                                                                                                                             | string     | M   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;weight                                         | Вес глобального заголовка. По умолчанию: 1.                                                                                                                                                                                                                                                       | int        | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;priority                                       | Приоритет глобального заголовка. По умолчанию: 0.                                                                                                                                                                                                                                                 | int        | O   | R   |        |
| &nbsp;&nbsp;**}**                                                      |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="overload-sgsap">overload</a>**                              | Параметры перегрузки.                                                                                                                                                                                                                                                                             | object     | O   | R   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;enable                                                     | Флаг детектирования перегрузки. По умолчанию: false.                                                                                                                                                                                                                                              | bool       | O   | R   |        |
| &nbsp;&nbsp;limit                                                      | Максимальное количество сообщений SGsAP в секунду. По умолчанию: 1000.                                                                                                                                                                                                                            | int        | O   | R   |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| **<a name="sctp-config-sgsap">sctpAdditionalinfo</a>**                 | Дополнительные параметры SCTP.                                                                                                                                                                                                                                                                    | object     | O   | P   |        |
| **{**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |
| &nbsp;&nbsp;nodelay                                                    | Флаг активации SCTP nodelay. По умолчанию: false.                                                                                                                                                                                                                                                 | bool       | O   | P   |        |
| &nbsp;&nbsp;maxInitRetransmits                                         | Максимальное количество попыток отправить сообщение INIT для создания ассоциации.<br>По умолчанию: 10.                                                                                                                                                                                            | int        | O   | R   |        |
| &nbsp;&nbsp;initTimeout                                                | Время ожидания ответа на сообщение INIT, в миллисекундах.<br>По умолчанию: 100.                                                                                                                                                                                                                   | int        | O   | R   |        |
| &nbsp;&nbsp;inStreams                                                  | Запрашиваемое количество входящих потоков.<br>Диапазон: 1-65&nbsp;536. По умолчанию: 1.                                                                                                                                                                                                           | int        | O   | P   |        |
| &nbsp;&nbsp;outStreams                                                 | Количество исходящих SCTP-потоков.<br>Диапазон: 1-65&nbsp;536. По умолчанию: 1.                                                                                                                                                                                                                   | int        | O   | P   |        |
| &nbsp;&nbsp;associationMaxRetrans                                      | Максимальное количество перепосылок в рамках ассоциации, после превышения которых ассоциация считается неактивной.<br>По умолчанию: 10.                                                                                                                                                           | int        | O   | R   |        |
| &nbsp;&nbsp;rtoMax                                                     | Максимальное значение RTO, в миллисекундах.<br>По умолчанию: 60&nbsp;000.                                                                                                                                                                                                                         | int        | O   | R   |        |
| &nbsp;&nbsp;rtoMin                                                     | Минимальное значение RTO, в миллисекундах.<br>По умолчанию: 1000.                                                                                                                                                                                                                                 | int        | O   | R   |        |
| &nbsp;&nbsp;rtoInitial                                                 | Начальное значение RTO, в миллисекундах.<br>По умолчанию: 3000.                                                                                                                                                                                                                                   | int        | O   | R   |        |
| &nbsp;&nbsp;hbInterval                                                 | Период отправки сигнала SCTP heartbeat, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                                                                                            | int        | O   | R   |        |
| &nbsp;&nbsp;maxRetrans                                                 | Максимальное количество попыток отправки сообщения на адрес.<br>По умолчанию: 5.                                                                                                                                                                                                                  | int        | O   | R   |        |
| &nbsp;&nbsp;sackDelay                                                  | Значение `SACK_DELAY`.<br>По умолчанию: 200.                                                                                                                                                                                                                                                      | int        | O   | R   |        |
| &nbsp;&nbsp;shutdownEvent                                              | Флаг включения индикации о событии SHUTDOWN от ядра.<br>По умолчанию: true.                                                                                                                                                                                                                       | bool       | O   | R   |        |
| &nbsp;&nbsp;assocChangeEvent                                           | Флаг включения индикации об изменении состояния ассоциации от ядра.<br>По умолчанию: false.                                                                                                                                                                                                       | bool       | O   | R   |        |
| &nbsp;&nbsp;peerAddrChangeEvent                                        | Флаг включения индикации об изменении состояния пира в ассоциации от ядра.<br>По умолчанию: false.                                                                                                                                                                                                | bool       | O   | R   |        |
| &nbsp;&nbsp;sndBuf                                                     | Размер буфера сокета для отправки, `net.core.wmem_default`.<br>**Внимание!** Значение удваивается! Удвоенный размер не может превышать значение `net.core.wmem_max`.<br>По умолчанию: значения kernel.                                                                                            | int        | O   | R   |        |
| &nbsp;&nbsp;ipMtuDiscover                                              | Индикатор разрешения использования алгоритма Path MTU Discovery.<br>`-1` -- нет изменений;<br>`0` -- алгоритм не используется, флаг заголовка IP `Don't fragment (DF)` не активирован;<br>`1` -- алгоритм используется, флаг заголовка IP `Don't fragment (DF)` активирован.<br>По умолчанию: -1. | int        | O   | R   |        |
| &nbsp;&nbsp;dscp                                                       | Значение поля заголовка IP DSCP/ToS.<br>По умолчанию: -1, не задается.                                                                                                                                                                                                                            | int        | O   | R   |        |
| &nbsp;&nbsp;txBufStatistics                                            | Флаг включения вывода в `si.log` о состоянии sctp-буфера.<br>По умолчанию: false.<br>**Примечание.** Должна быть включена запись журнала `si.log`, для чего достаточно задать `level = 1`. Информация выводится при заполнении SCTP-буфера.                                                       | bool       | O   | R   |        |
| &nbsp;&nbsp;<a name="warning-tx-buf-size-sgsap">warningTxBufSize</a>   | Порог для вывода сообщений SCTP-буфера в журнал `warning.log`, в байтах.<br>По умолчанию: 1024.                                                                                                                                                                                                   | int        | O   | R   |        |
| &nbsp;&nbsp;<a name="critical-tx-buf-size-sgsap">criticalTxBufSize</a> | Порог для срабатывания индикации о переполнении SCTP-буфера, в байтах.<br>По умолчанию: 0.                                                                                                                                                                                                        | int        | O   | R   |        |
| &nbsp;&nbsp;onCriticalTxBufSize                                        | Действие по достижении [criticalTxBufSize](#critical-tx-buf-size-sgsap).<br>`0` -- отправка индикации о переполнении на верхний уровень;<br>`1` -- разрыв SCTP-ассоциации.<br>По умолчанию: 0.                                                                                                    | int        | O   | R   |        |
| **}**                                                                  |                                                                                                                                                                                                                                                                                                   |            |     |     |        |

#### Пример ####

```json
{
  "localAddress": {
    "localHost": "192.168.100.1",
    "localPort": 29111,
    "localInterfaces": [
      "192.168.100.10:29118",
      "192.168.101.10:29118",
      "192.168.102.10:29118"
    ]
  },
  "timers": {
    "reconnectTimeout": 20000,
    "responseTimeout": 10000,
    "ts8": 2000,
    "ts9": 3000
  },
  "retryCounters": {
    "ns8": 3,
    "ns9": 4
  },
  "lacGt": {
    "defaultGt": "79211230000",
    "bindings": [
      {
        "lac": 5,
        "gt": "79231234567"
      },
      {
        "plmn": "25048",
        "lac": 7,
        "gt": "79111234567"
      }
    ]
  },
  "mscPool": [
    {
      "lac": 5,
      "gtlist": [
        {
          "gt": "79231234567",
          "weight": 1,
          "priority": 0
        },
        {
          "gt": "79237654321",
          "weight": 10,
          "priority": 1
        }
      ]
    },
    {
      "plmn": "25048",
      "lac": 7,
      "gtList": [
        {
          "gt": "79111234567",
          "weight": 1,
          "priority": 1
        },
        {
          "gt": "79117654321",
          "weight": 10,
          "priority": 1
        }
      ]
    }
  ]
}
```