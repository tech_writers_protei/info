---
title: "plmn.json"
description: "Параметры обслуживаемых сетей"
weight: 20
type: docs
---

В файле задаются сети, обслуживаемые узлом ММЕ.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

#### Описание параметров сети PLMN ####

| Параметр                                                            | Описание                                                                                                                                                                                             | Тип      | O/M | P/R | Версия   |
|---------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|----------|
| <a name="mcc">mcc</a>                                               | Мобильный код страны.                                                                                                                                                                                | string   | M   | R   |          |
| <a name="mnc">mnc</a>                                               | Код мобильной сети.                                                                                                                                                                                  | string   | M   | R   |          |
| <a name="mmegi">mmegi</a>                                           | **Десятичное** значение `MME Group ID`.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                    | int      | O   | R   |          |
| <a name="mmec">mmec</a>                                             | **Десятичное** значение `MME Code`.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                        | int      | O   | R   |          |
| equivalentPlmns                                                     | Перечень эквивалентных PLMN. См. [3GPP TS 29.292](https://www.etsi.org/deliver/etsi_ts/129200_129299/129292/17.00.00_60/ts_129292v170000p.pdf).                                                      | [object] | O   | R   | 1.36.0.0 |
| **{**                                                               |                                                                                                                                                                                                      |          |     |     |          |
| &nbsp;&nbsp;mcc                                                     | Мобильный код страны.                                                                                                                                                                                | string   | M   | R   |          |
| &nbsp;&nbsp;mnc                                                     | Код мобильной сети.                                                                                                                                                                                  | string   | M   | R   |          |
| **}**                                                               |                                                                                                                                                                                                      |          |     |     |          |
| <a name="relative-mme-capacity-served-plmn">relativeMmeCapacity</a> | Относительная емкость узла MME, `Relative MME Capacity`.<br>См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).<br>По умолчанию: 255. | int      | O   | R   |          |
| relativeMmeCapacityForTac                                           | Перечень связей [Relative MME Capacity](#relative-mme-capacity) и TAC.                                                                                                                               | [object] | O   | R   | 1.35.0.0 |
| **{**                                                               |                                                                                                                                                                                                      |          |     |     |          |
| &nbsp;&nbsp;tac                                                     | Код зоны отслеживания.                                                                                                                                                                               | int      | M   | R   |          |
| &nbsp;&nbsp;relativeCapacity                                        | Относительная емкость.                                                                                                                                                                               | int      | M   | R   |          |
| **}**                                                               |                                                                                                                                                                                                      |          |     |     |          |
| originHost                                                          | Значение `Origin-Host` для протокола Diameter и SGsAP.<br>По умолчанию: mmec[\<mmec\>](#mmec).mmegi[\<mmegi\>](#mmegi).<br>mme.epc.mnc[\<mnc\>](#mnc).mcc[\<mcc\>](#mcc).<br>3gppnetwork.org.        | string   | O   | R   |          |
| originRealm                                                         | Значение `Origin-Realm` для протокола Diameter.<br>По умолчанию: epc.mnc[\<mnc\>](#mnc).<br>mcc[\<mcc\>](#mcc).3gppnetwork.org.                                                                      | string   | O   | R   |          |
| ueUsageTypes                                                        | Перечень `UE Usage Type`, связанных с текущим [MMEGI](#mmegi).<br>См. [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.08.00_60/ts_123401v170800p.pdf).                 | [int]    | O   | R   | 1.35.0.0 |
| foreignUeUsageTypes                                                 | Перечень связей между `UE Usage Type` и другими `MMEGI`. Формат:<br>`<ue_usage_type>,<ue_usage_type>-<mmegi>`.                                                                                       | [object] | O   | R   |          |
| **{**                                                               |                                                                                                                                                                                                      |          |     |     |          |
| &nbsp;&nbsp;mmegi                                                   | Значение `MME Group ID`.                                                                                                                                                                             | int      | M   | R   |          |
| &nbsp;&nbsp;ueUsageTypes                                            | Перечень значений `UE Usage Type`.                                                                                                                                                                   | [int]    | M   | R   |          |
| **}**                                                               |                                                                                                                                                                                                      |          |     |     |          |

#### Описание параметров EMM

| Параметр              | Описание                                                                                                                                                                               | Тип      | O/M | P/R | Версия |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| emmInfo               | Флаг отправки сообщения EMM Information.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: false. | bool     | O   | R   |        |
| netName               | Краткое название сети.<br>По умолчанию: Protei.                                                                                                                                        | string   | O   | R   |        |
| netFullname           | Полное название сети.<br>По умолчанию: Protei Network.                                                                                                                                 | string   | O   | R   |        |
| [Timezone](#timezone) | Часовые пояса.<br>По умолчанию: UTC+3.                                                                                                                                                 | timezone | O   | R   |        |

#### Описание параметров таймеров и счетчиков

| Параметр                         | Описание                                                                                                                                                                                                                                         | Тип    | O/M | P/R | Версия   |
|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| t3324                            | Таймер T3324, в секундах.<br>См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>**Примечание.** Используется только при `"PSM": 1`.<br>По умолчанию: значение T3324 абонента. | int    | O   | R   |          |
| t3402                            | Таймер T3402, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                                              | int    | O   | R   |          |
| t3402Rej                         | Таймер T3402 для абонентов, не попавших в белый список IMSI, в секундах.<br>По умолчанию: 0.                                                                                                                                                     | int    | O   | R   |          |
| t3412                            | Таймер T3412, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 60.                                                                             | int    | O   | R   |          |
| <a name="t3412-ext">t3412Ext</a> | Таймер T3412 Extended, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: не определено.                                                         | int    | O   | R   |          |
| t3412ExtSource                   | Приоритетный источник таймера [T3412 Extended](#t3412-ext).<br>`mme` / `ue`. По умолчанию: ue.                                                                                                                                                   | string | O   | R   |          |
| <a name="t3413">t3413</a>        | Таймер T3413, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 10.                                                                             | int    | O   | R   |          |
| t3413Sgs                         | Таймер T3413 для процедуры S1AP Paging, вызванного процедурой SGsAP Paging, в секундах.<br>По умолчанию: значение [T3413](#t3413).                                                                                                               | int    | O   | R   |          |
| t3422                            | Таймер T3422, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                              | int    | O   | R   | 1.36.0.0 |
| <a name="t3450">t3450</a>        | Таймер T3450, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                              | int    | O   | R   | 1.36.0.0 |
| t3450RepeatCount                 | Количество повторных отправок по таймеру [T3450](#t3450).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                     | int    | O   | R   | 1.36.0.0 |
| <a name="t3460">t3460</a>        | Таймер T3460, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                              | int    | O   | R   | 1.36.0.0 |
| t3460RepeatCount                 | Количество повторных отправок по таймеру [T3460](#t3460).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                     | int    | O   | R   | 1.36.0.0 |
| <a name="t3470">t3470</a>        | Таймер T3470, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                              | int    | O   | R   | 1.36.0.0 |
| t3470RepeatCount                 | Количество повторных отправок по таймеру [T3470](#t3470).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                     | int    | O   | R   | 1.36.0.0 |
| purgeDelay                       | Время задержки отправки сообщения Diameter: Purge-Request на узел HSS, в секундах.<br>**Примечание.** По умолчанию сообщение не отправляется, а данные не удаляются.                                                                             | int    | O   | R   |          |
| pagingRepeatCount                | Количество отправок запросов Paging на каждую станцию eNodeB в рамках текущего шага используемой модели пейджинга.<br>Диапазон: 1-5. По умолчанию: 4.                                                                                            | int    | O   | R   |          |
| ueActivityTimeout                | Время ожидания активности от устройства UE, в секундах.<br>По умолчанию: 600.                                                                                                                                                                    | int    | O   | R   |          |
| reattachTimeout                  | Время ожидания сообщения S1 Attach Request при выполнении процедуры Detach с повторной регистрацией, в миллисекундах.<br>По умолчанию: 500.                                                                                                      | int    | O   | R   |          |

#### Описание параметров флагов

| Параметр                       | Описание                                                                                                                                                                                                                                                                                                      | Тип  | O/M | P/R | Версия    |
|--------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|-----|-----|-----------|
| allowDefaultApn                | Флаг разрешения использования APN по умолчанию.<br>По умолчанию: true.                                                                                                                                                                                                                                        | bool | O   | R   |           |
| allowOperatorQci               | Флаг разрешения использования <abbr title="QoS Class Identifier">`QCI`</abbr> из диапазона 128-254.<br>По умолчанию: false.                                                                                                                                                                                   | bool | O   | R   |           |
| allowEmptyMsisdn               | Флаг разрешения регистрации абонентов без MSISDN.<br>По умолчанию: false.                                                                                                                                                                                                                                     | bool | O   | R   |           |
| allowExtDrx                    | Флаг разрешения использования `Extended DRX`.<br>См. [3GPP TS 25.300](https://www.etsi.org/deliver/etsi_ts/125300_125399/125300/17.00.00_60/ts_125300v170000p.pdf).<br>По умолчанию: 0.                                                                                                                       | bool | O   | R   |           |
| resetOnSetup                   | Флаг отправки сообщения S1AP: RESET после запроса S1AP: S1 SETUP REQUEST.<br>По умолчанию: false.                                                                                                                                                                                                             | bool | O   | R   |           |
| indirectFwd                    | Флаг разрешения создания Indirect Data Forwarding Tunnel при хэндовере.<br>По умолчанию: false.                                                                                                                                                                                                               | bool | O   | R   | 1.19.0.0  |
| pagingModel                    | Индикатор используемой модели для процедуры Paging.<br>`0` -- по умолчанию;<br>`1` -- начало с последней базовой станции.<br>По умолчанию: 0.                                                                                                                                                                 | int  | O   | R   | 1.40.0.0  |
| srvcc                          | Флаг поддержки <abbr title="Single Radio Voice Call Continuity">`SRVCC`</abbr>.<br>См. [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf).<br>По умолчанию: false.                                                                                 | bool | O   | R   |           |
| useVlr                         | Флаг использования VLR.<br>По умолчанию: false.<br>**Примечание.** По умолчанию SGs не используется. При запросе NAS ATTACH REQUEST от UE и значении `EPS attach "type": combined EPS/IMSI attach, 2` узел MME в ответе NAS ATTACH ACCEPT задает значение `EPS attach "result": combined EPS/IMSI attach, 2`. | bool | O   | R   |           |
| <a name="psm">psm</a>          | Флаг разрешения режима `Power Saving Mode`.<br>См. [3GPP TS 23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/12.02.00_60/ts_123682v120200p.pdf).<br>По умолчанию: false.                                                                                                                     | bool | O   | R   | 1.22.1.0  |
| moCsfbInd                      | Флаг отправки сообщения SGsAP-MO-CSFB-Indication при получении запроса Extended Service Request.<br>По умолчанию: false.                                                                                                                                                                                      | bool | O   | R   |           |
| cpCiotOpt                      | Флаг разрешения оптимизации Control Plane CIoT EPS.<br>См. [3GPP TS 36.300](https://www.etsi.org/deliver/etsi_ts/136300_136399/136300/14.10.00_60/ts_136300v141000p.pdf).<br>По умолчанию: false.                                                                                                             | bool | O   | R   | 1.15.19.0 |
| <a name="ims-vops">imsVops</a> | Значение `IMS Voice over PS Session Indicator`.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: false.                                                                                                                 | bool | O   | R   |           |
| s11UliAlwaysSent               | Флаг отправки User Location Information в запросе GTP: Modify Bearer Request.<br>См. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf).<br>По умолчанию: false.                                                                                   | bool | O   | R   | 1.40.0.0  |
| emergencyAttachWoAuth          | Флаг разрешения процедуры Emergency Attach без аутентификации.<br>По умолчанию: false.                                                                                                                                                                                                                        | bool | O   | R   |           |
| eUtran                         | Флаг поддержки сети E-UTRAN.<br>По умолчанию: true.                                                                                                                                                                                                                                                           | bool | O   | R   |           |
| utran                          | Флаг поддержки сети UTRAN.<br>По умолчанию: true.                                                                                                                                                                                                                                                             | bool | O   | R   |           |
| geran                          | Флаг поддержки сети GERAN.<br>По умолчанию: true.                                                                                                                                                                                                                                                             | bool | O   | R   |           |
| pCscfRestoration               | Флаг поддержки процедуры P-CSCF Restoration.<br>По умолчанию: true.                                                                                                                                                                                                                                           | bool | O   | R   | 1.44.2.0  |
| authentication                 | Флаг включения аутентификации.<br>По умолчанию: true.                                                                                                                                                                                                                                                         | bool | O   | R   |           |
| authVectorsNum                 | Количество запрашиваемых векторов аутентификации.<br>Диапазон: 1-7. По умолчанию: 1.                                                                                                                                                                                                                          | int  | O   | R   | 1.41.0.0  |
| notifyOnPgwAlloc               | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении динамического IP-адреса узла PGW для APN.<br>По умолчанию: true.                                                                                                                                                        | bool | O   | R   |           |
| notifyOnUeSrvcc                | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении флага поддержки SRVCC на устройстве UE.<br>По умолчанию: true.                                                                                                                                                          | bool | O   | R   |           |
| forceIdentityReq               | Флаг отправки Identity Request вне зависимости от наличия сохраненного ранее IMSI.<br>По умолчанию: false.                                                                                                                                                                                                    | bool | O   | R   |           |
| requestImeisv                  | Флаг требования IMEISV от абонента.<br>По умолчанию: true.                                                                                                                                                                                                                                                    | bool | O   | R   | 1.15.18.0 |
| rejectForeignGuti              | Флаг запрета перехода на MME абонентов, которые были зарегистрированы в других PLMN на сторонних MME при работе в условиях RAN Sharing.<br>По умолчанию: true.<br>                                                                                                                                            | bool | O   | R   | 1.49.0.0  |
| rejectOnLuFail                 | Флаг отправки сообщения SGsAP-LOCATION-UPDATE-REJECT в случае неуспеха процедуры SGsAP-Location-Update.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: false.                                                         | bool | O   | R   |           |
| useDefaultApnOnFailure         | Флаг использования APN из профиля по умолчанию при отсутствии известных адресов SGW/PGW для APN, запрошенного абонентом.<br>По умолчанию: false.                                                                                                                                                              | bool | O   | R   | 1.49.0.0  |
| defaultLacAsTac                | Флаг использования LAC, равного текущему TAC, в случае отсутствия соответствия TAC к LAI в перечне ниже.<br>По умолчанию: false.                                                                                                                                                                              | bool | O   | R   | 1.49.0.0  |
| sendMaskedImeisv               | Флаг заполнения поля `Masked IMEISV` в соответствующих S1AP-сообщениях. См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/18.02.00_60/ts_136413v180200p.pdf).<br>По умолчанию: true.                                                                                             | bool | O   | R   | 1.50.3.0  |

#### Описание параметров IMEI

| Параметр         | Описание                                                                        | Тип    | O/M | P/R | Версия   |
|------------------|---------------------------------------------------------------------------------|--------|-----|-----|----------|
| checkImei        | Флаг включения проверки IMEI на узле EIR.<br>По умолчанию: false.               | bool   | O   | R   |          |
| allowGreyImei    | Флаг разрешения подключения телефона с неизвестным IMEI.<br>По умолчанию: true. | bool   | O   | R   |          |
| allowUnknownImei | Флаг обслуживания устройств, неизвестных для EIR.<br>По умолчанию: false.       | bool   | O   | R   | 1.40.0.0 |
| eirHost          | Значение `Destination-Host` для узла EIR.                                       | string | O   | R   |          |
| eirRealm         | Значение `Destination-Realm` для узла EIR.                                      | string | O   | R   |          |

#### Описание параметров правил

| Параметр                                              | Описание                                                                                                                                      | Тип         | O/M | P/R | Версия   |
|-------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|-------------|-----|-----|----------|
| <a name="apn-rules">apnRules</a>                      | Перечень имен [правил APN](../rules/apn_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                           | [string]    | O   | R   |          |
| <a name="tac_rules">tacRules</a>                      | Перечень имен [правил TAC](../rules/tac_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                           | [string]    | O   | R   |          |
| <a name="rac-rules">racRules</a>                      | Перечень имен [правил RAC](../rules/rac_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                           | [string]    | O   | R   |          |
| <a name="zc-rules">zcRules</a>                        | Перечень имен [правил ZoneCodes](../rules/zc_rules/).                                                                                         | [string]    | O   | R   | 1.36.0.0 |
| <a name="qos-rules">qosRules</a>                      | Перечень имен [правил QoS](../rules/qos_rules/).                                                                                              | [string]    | O   | R   | 1.37.0.0 |
| <a name="code-mapping-rules">codeMappingRules</a>     | Перечень имен [правил Diameter: Result-Code и EMM Cause](../rules/code_mapping_rules/).                                                       | [string]    | O   | R   | 1.37.2.0 |
| <a name="forbidden-imei-rules">forbiddenImeiRules</a> | Перечень имен [правил запрещённых IMEI](../rules/forbidden_imei_rules/).                                                                      | [string]    | O   | R   |          |
| <a name="emergency-numbers">emergencyNumbers</a>      | Перечень [типов и правил номеров экстренных служб](../rules/emergency_numbers/).                                                              | [string]    | O   | R   |          |
| <a name="emergency-pdn">emergencyPdn</a>              | Перечень имен [правил PDN Connectivity](../rules/emergency_pdn/), применяемых в случае Emergency Attach.                                      | [string]    | O   | R   |          |
| <a name="imsi-wl">imsiWhitelist</a>                   | Перечень имен [правил TAC и IMSI](../rules/imsi_rules/).<br>Если TAC не указан, то набор масок применяется для всей PLMN.                     | [string]    | O   | R   |          |
| <a name="pgw_plmn">pgw</a>                            | Параметры узла PGW по умолчанию.                                                                                                              | object      | O   | R   |          |
| **{**                                                 |                                                                                                                                               |             |     |     |          |
| &nbsp;&nbsp;ip                                        | IP-адрес узла.                                                                                                                                | ip          | M   | R   |          |
| &nbsp;&nbsp;fqdn                                      | Fully Qualified Domain Name узла.                                                                                                             | string      | M   | R   |          |
| **}**                                                 |                                                                                                                                               |             |     |     |          |
| <a name="sgw_plmn">sgw</a>                            | Параметры узла SGW по умолчанию.                                                                                                              | {string,ip} | O   | R   |          |
| **{**                                                 |                                                                                                                                               |             |     |     |          |
| &nbsp;&nbsp;ip                                        | IP-адрес узла.                                                                                                                                | ip          | M   | R   |          |
| &nbsp;&nbsp;fqdn                                      | Fully Qualified Domain Name узла.                                                                                                             | string      | M   | R   |          |
| **}**                                                 |                                                                                                                                               |             |     |     |          |
| otherMme                                              | Параметры других узлов MME с соответствующими значениями `MMEGI` и `MMEC`.                                                                    | [object]    | O   | R   |          |
| **{**                                                 |                                                                                                                                               |             |     |     |          |
| &nbsp;&nbsp;mmegi                                     | **Десятичное** значение `MME Group ID`. По умолчанию: значение [mmegi](#mmegi).                                                               | int         | O   | R   |          |
| &nbsp;&nbsp;mmec                                      | **Десятичное** значение `MME Code`.                                                                                                           | int         | M   | R   |          |
| &nbsp;&nbsp;ip                                        | IP-адрес узла.                                                                                                                                | ip          | M   | R   |          |
| **}**                                                 |                                                                                                                                               |             |     |     |          |
| sgsn                                                  | Параметры узлов SGSN с соответствующими контроллерами RNC. Формат:<br>`<rnc>-<ip>,<rnc>-<ip>`.                                                | [object]    | O   | R   |          |
| **{**                                                 |                                                                                                                                               |             |     |     |          |
| &nbsp;&nbsp;rnc                                       | Значение `RNC Identifier`. См. [3GPP TS 25.401](https://www.etsi.org/deliver/etsi_ts/125400_125499/125401/18.00.00_60/ts_125401v180000p.pdf). | int<br>hex  | M   | R   |          |
| &nbsp;&nbsp;ip                                        | IP-адрес узла.                                                                                                                                | ip          | M   | R   |          |
| **}**                                                 |                                                                                                                                               |             |     |     |          |

#### Описание остальных параметров

| Параметр      | Описание                                                                                                                                                                                                                                  | Тип      | O/M | P/R | Версия   |
|---------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|----------|
| vopsApnNi     | Перечень значений `APN-NI`, которые могут быть использованы для VoPS.<br>По умолчанию: ["ims"].<br>**Примечание.**Если у абонента нет подписки ни на один из указанных APN, то поддержка VoPS не заявляется в запросах Attach/TAU Accept. | [string] | O   | R   | 1.40.0.0 |
| nriLength     | Количество используемых бит идентификатора <abbr title="Network Resource Identifier">`NRI`</abbr> узла SGSN.<br>**Примечание.** Если не задано, то не используется в рамках соответствующей сети PLMN.                                    | int      | O   | R   | 1.44.1.0 |
| ldn           | Значение <abbr title="Local Distinguished Name">`LDN`</abbr>, отправляемое при активированном SRVCC.<br>По умолчанию: Protei_MME.                                                                                                         | string   | O   | R   |          |
| [eDrx](#edrx) | Значение <abbr title="Extended Discontinuous Reception">`eDRX`</abbr>. См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>По умолчанию: не определено.                 | int      | O   | R   | 1.41.0.0 |
| [ptw](#ptw)   | Значение `Paging Time Window` для Extended DRX. См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>По умолчанию: не определено.                                        | int      | O   | R   | 1.41.0.0 |

#### Описание параметров соответствий TAC-LAC

| Параметр     | Описание                                                                                                                                                                           | Тип      | O/M | P/R | Версия   |
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|----------|
| forbiddenLAC | Перечень кодов LAC, из которых переход абонентов запрещен.                                                                                                                         | [int]    | O   | R   | 1.33.0.0 |
| tacLai       | Перечень соответствий TAC к LAI. Формат:<br>`<tac> = [<plmn>-]<lac>;`<br>**Примечание.** Один TAC может быть связан только с одним LAI, один LAI может быть связан со многими TAC. | [string] | O   | R   |          |
| tac          | Код зоны отслеживания.                                                                                                                                                             | int      | O   | R   |          |
| \<plmn\>     | Код сети PLMN.<br>По умолчанию: значение [\<MCC\>](#mcc)[\<MNC\>](#mnc).                                                                                                           | int      | O   | R   |          |
| \<lac\>      | Код локальной области LAC.                                                                                                                                                         | int      | M   | R   |          |

#### Пример ####

```json
[
  {
    "mcc": "999",
    "mnc": "99",
    "mmegi": 1,
    "mmec": 1,
    "utran": false,
    "t3413": 5,
    "t3412": 180,
    "t3402": 10,
    "t3402Rej": 10,
    "t3450": 1,
    "t3460": 1,
    "t3470": 1,
    "checkImei": false,
    "emergencyNumbers": [
      "112",
      "911"
    ],
    "eirHost": "eir.epc.mnc01.mcc250.3gppnetwork.org",
    "useVlr": false,
    "imsVops": true,
    "allowGreyImei": false,
    "resetOnSetup": false,
    "imsiWhitelist": [
      "WList1",
      "WList5"
    ],
    "apnRules": [
      "FullRule",
      "Rule_1"
    ],
    "tacRules": [
      "Rule_1",
      "Rule_2",
      "Rule_3",
      "Rule_4",
      "Rule_5",
      "Rule_6"
    ],
    "relativeMmeCapacity": 255,
    "forceIdentityReq": true,
    "emmInfo": true,
    "netFullname": "Protei_Network_999",
    "netName": "test99",
    "timezone": "UTC+3",
    "eDrx": 3,
    "ptw": 2
  },
  {
    "mcc": "001",
    "mnc": "01",
    "geran": false,
    "mmegi": 1,
    "mmec": 1,
    "mmeIp": [
      "1-2-192.168.125.182"
    ],
    "t3324": 60,
    "t3413": 1,
    "t3412": 120,
    "t3412Ext": 14400,
    "t3412ExtSource": "mme",
    "t3402": 5,
    "t3402Rej": 10,
    "t3450": 5,
    "t3450RepeatCount": 4,
    "t3460": 6,
    "t3460RepeatCount": 3,
    "t3470": 1,
    "ueActivityTimeout": 600,
    "checkImei": false,
    "requestImeisv": true,
    "authVectorsNum": 2,
    "emergencyNumbers": [
      "112",
      "911"
    ],
    "emergencyPdn": [
      "EMRG"
    ],
    "originHost": "mme1.epc.mnc001.mcc001.3gppnetwork.org",
    "originRealm": "epc.mnc001.mcc001.3gppnetwork.org",
    "eirHost": "eir.epc.mnc01.mcc250.3gppnetwork.org",
    "imsVops": true,
    "vopsApnNi": [
      "ims"
    ],
    "allowGreyImei": false,
    "useVlr": false,
    "rejectOnLuFail": false,
    "resetOnSetup": false,
    "purgeDelay": 0,
    "pagingModel": 1,
    "imsiWhitelist": [
      "WList1",
      "WList5"
    ],
    "apnRules": [
      "FullRule",
      "Rule_1",
      "Rule_2",
      "Rule_3"
    ],
    "tacRules": [
      "Rule_1",
      "Rule_2",
      "Rule_3",
      "Rule_4",
      "Rule_5",
      "Rule_6",
      "Rule_7"
    ],
    "codeMappingRules": [
      "Rule1",
      "Rule2"
    ],
    "relativeMmeCapacity": 100,
    "forceIdentityReq": true,
    "emmInfo": true,
    "netFullname": "Protei_Network_001",
    "netName": "protei",
    "timezone": "TAC1_UTC+2",
    "cpCiotOpt": true,
    "psm": true,
    "tacLai": [
      "1 = 1",
      "7 = 1",
      "9 = 25048-5",
      "10 = 25048-7"
    ],
    "foreignUeUsageTypes": [
      {
        "mmegi": 2,
        "ueUsageTypes": [
          32,
          64
        ]
      }
    ],
    "qosRules": [
      "Rules"
    ],
    "allowDefaultApn": true,
    "eDrx": 3
  },
  {
    "mmegi": 3,
    "mmec": 4
  },
  {
    "MCC": "208",
    "mnc": "93",
    "utran": false,
    "mmegi": 1,
    "mmec": 1,
    "t3413": 5,
    "t3412Ext": 600,
    "requestImeisv": false,
    "checkImei": false,
    "allowEmptyMsisdn": true,
    "eirHost": "eir.epc.mnc01.mcc250.3gppnetwork.org",
    "imsVops": true,
    "psm": true,
    "allowExtDrx": true,
    "allowGreyImei": false,
    "resetOnSetup": true,
    "useDefaultApnOnFailure": true,
    "defaultLacAsTac": true,
    "apnRules": [
      "FullRule",
      "Rule_1"
    ],
    "tacRules": [
      "Rule_4"
    ],
    "sgw": {
      "ip": "192.168.125.95"
    },
    "tacLai": [
      "2 = 3",
      "1 = 1",
      "5 = 25048-4"
    ],
    "cpCiotOpt": true,
    "ptw": 3
  }
]
```

### Значения eDRX {#edrx}

Значение eDRX задается 4 младшими битами октета 3.
В конфигурации используется десятичное (Dec) значение, полученное из конвертации двоичного (Bin).

Пример: для режима A/Gb необходимо использовать значение 195,84 сек. Согласно
[3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf),
разделу 10.5.5.32, или таблицам ниже, младшие биты должны быть 0111<sub>2</sub>, что соответствует 7<sub>10</sub>.

Итого: `"eDRX": 7;`

#### Режим A/Gb

| bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 1,88 сек                |
| 0001 | 1   | 3,76 сек                |
| 0010 | 2   | 7,53 сек                |
| 0011 | 3   | 12,24 сек               |
| 0100 | 4   | 24,48 сек               |
| 0101 | 5   | 48,96 сек               |
| 0110 | 6   | 97,92 сек               |
| 0111 | 7   | 195,84 сек              |
| 1000 | 8   | 391,68 сек              |
| 1001 | 9   | 783,36 сек              |
| 1010 | 10  | 1566,72 сек             |
| 1011 | 11  | 3133,44 сек             |

#### Режим Iu

| bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 10,24 сек               |
| 0001 | 1   | 20,48 сек               |
| 0010 | 2   | 40,96 сек               |
| 0011 | 3   | 81,92 сек               |
| 0100 | 4   | 163,84 сек              |
| 0101 | 5   | 327,68 сек              |
| 0110 | 6   | 655,36 сек              |
| 0111 | 7   | 1310,72 сек             |
| 1000 | 8   | 1966,08 сек             |
| 1001 | 9   | 2621,44 сек             |

#### Режим S1

| bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 5,12 сек                |
| 0001 | 1   | 10,24 сек               |
| 0010 | 2   | 20,48 сек               |
| 0011 | 3   | 40,96 сек               |
| 0100 | 4   | 61,44 сек               |
| 0101 | 5   | 81,92 сек               |
| 0110 | 6   | 102,4 сек               |
| 0111 | 7   | 122,88 сек              |
| 1000 | 8   | 143,36 сек              |
| 1001 | 9   | 163,84 сек              |
| 1010 | 10  | 327,68 сек              |
| 1011 | 11  | 655,36 сек              |
| 1100 | 12  | 1&nbsp;310,72 сек       |
| 1101 | 13  | 2&nbsp;621,44 сек       |
| 1110 | 14  | 5&nbsp;242,88 сек       |
| 1111 | 15  | 10&nbsp;485,76 сек      |

### Значения PTW {#ptw}

Значение PTW задается 4 старшими битами октета 3.
В конфигурации используется десятичное (Dec) значение, полученное из конвертации двоичного (Bin).

Пример: для режима NB-S1 необходимо использовать значение 15,36 сек. Согласно
[3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf),
разделу 10.5.5.32, или таблицам ниже, старшие биты должны быть 0101<sub>2</sub>, что соответствует 5<sub>10</sub>.

Итого: `"PTW": 5;`

#### Режим Iu

| bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 0 сек            |
| 0001 | 1   | 1 сек            |
| 0010 | 2   | 2 сек            |
| 0011 | 3   | 3 сек            |
| 0100 | 4   | 4 сек            |
| 0101 | 5   | 5 сек            |
| 0110 | 6   | 6 сек            |
| 0111 | 7   | 7 сек            |
| 1000 | 8   | 8 сек            |
| 1001 | 9   | 9 сек            |
| 1010 | 10  | 10 сек           |
| 1011 | 11  | 12 сек           |
| 1100 | 12  | 14 сек           |
| 1101 | 13  | 16 сек           |
| 1110 | 14  | 18 сек           |
| 1111 | 15  | 20 сек           |

#### Режим WB-S1

| bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 1,28 сек         |
| 0001 | 1   | 2,56 сек         |
| 0010 | 2   | 3,84 сек         |
| 0011 | 3   | 5,12 сек         |
| 0100 | 4   | 6,4 сек          |
| 0101 | 5   | 7,68 сек         |
| 0110 | 6   | 8,96 сек         |
| 0111 | 7   | 10,24 сек        |
| 1000 | 8   | 11,52 сек        |
| 1001 | 9   | 12,8 сек         |
| 1010 | 10  | 14,08 сек        |
| 1011 | 11  | 15,36 сек        |
| 1100 | 12  | 16,64 сек        |
| 1101 | 13  | 17,92 сек        |
| 1110 | 14  | 19,20 сек        |
| 1111 | 15  | 20,48 сек        |

#### Режим NB-S1

| bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 2,56 сек         |
| 0001 | 1   | 5,12 сек         |
| 0010 | 2   | 7,68 сек         |
| 0011 | 3   | 10,24 сек        |
| 0100 | 4   | 12,8 сек         |
| 0101 | 5   | 15,36 сек        |
| 0110 | 6   | 17,92 сек        |
| 0111 | 7   | 20,48 сек        |
| 1000 | 8   | 23,04 сек        |
| 1001 | 9   | 25,6 сек         |
| 1010 | 10  | 28,16 сек        |
| 1011 | 11  | 30,72 сек        |
| 1100 | 12  | 33,28 сек        |
| 1101 | 13  | 35,84 сек        |
| 1110 | 14  | 38,4 сек         |
| 1111 | 15  | 40,96 сек        |

### Формат Timezone {#timezone}

Формат записи часового пояса: `[TACX_]UTC<sign>HH:MM[_DST+Y]`

* Необязательная часть `TAC<X>_` -- задает ассоциацию между часовым поясом и TAC, при отсутствии ассоциируется со всеми TAC в данной PLMN:
  * X -- код TAC;
* Обязательная часть `UTC<sign><HH>:<MM>` -- задает часовой пояс через отклонение от UTC:
  * sign -- знак отклонения от UTC, `+` или `-`;
  * HH -- количество часов;
  * MM -- количество минут;
* Необязательная часть `_DST+<Y>` -- задает дополнительное отклонение при переводе часов на летнее время:
  * Y -- количество добавленных часов, диапазон: 0-2;

### Адрес PGW ###

Адрес PGW может быть получен из ряда источников.
Приоритеты источников в порядке убывания:

1. [apn_rules.cfg::PGW_IP](../rules/apn_rules/#pgw_apn);
2. [dns.cfg](../dns/);
3. [PGW_IP](#pgw_plmn).

### Адрес SGW ###

Адрес SGW может быть получен из ряда источников.
В зависимости от процедуры SGW выбирается либо по APN, либо по TAC.

* В случае выбора по APN используется только [apn_rules.cfg::SGW_IP](../rules/apn_rules/#sgw_apn);
* В случае выбора по TAC приоритеты источников в порядке убывания:

1. [tac_rules.cfg::SGW_IP](../rules/tac_rules/#sgw_tac);
2. [dns.cfg](../dns/);
3. [SGW_IP](#sgw_plmn).