---
title: "apn_rules.json"
description: "Параметры APN NI и адресов PGW, SGW"
weight: 20
type: docs
---

В файле задаются правила, связывающие APN NI и адреса PGW и SGW.
Имя секции является именем правила и может использоваться в качестве ссылки в параметрах [plmn.json::apnRules](../../plmn/#apn-rules) и [imsi_rules.json::apnRules](../imsi_rules/#apn-rules-imsi).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../../oam/system_management/).

### Описание параметров ###

| Параметр                    | Описание                                                                                                                                                                                         | Тип      | O/M | P/R | Версия |
|-----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| name                        | Имя правила.                                                                                                                                                                                     | string   | M   | R   |        |
| apnNi                       | Регулярное выражение APN NI для правила.                                                                                                                                                         | string   | M   | R   |        |
| <a name="pgw_apn">pgwIp</a> | Перечень параметров узла PGW.                                                                                                                                                                    | [object] | M   | R   |        |
| fqdn                        | Полностью определенное доменное имя.                                                                                                                                                             | string   | O   | R   |        |
| ip                          | IP-адрес.                                                                                                                                                                                        | ip       | M   | R   |        |
| weight                      | Вес адреса.<br>Диапазон: 0-65&nbsp;535.<br>**Примечание.** Если 0 или отсутствует, то адрес игнорируется.<br>Если указан единственный адрес без указания веса, то присваивается значение&nbsp;1. | int      | O   | R   |        |
| priority                    | Приоритет адреса.<br>Диапазон: 0-65&nbsp;535.<br>Чем меньше значение, тем выше приоритет.<br>По умолчанию: 0.                                                                                    | int      | O   | R   |        |
| <a name="sgw_apn">sgwIp</a> | Перечень параметров узла SGW.                                                                                                                                                                    | [object] | O   | R   |        |
| fqdn                        | Полностью определенное доменное имя.                                                                                                                                                             | string   | O   | R   |        |
| ip                          | IP-адрес.                                                                                                                                                                                        | ip       | M   | R   |        |
| weight                      | Вес адреса.<br>Диапазон: 0-65&nbsp;535.<br>**Примечание.** Если 0 или отсутствует, то адрес игнорируется.<br>Если указан единственный адрес без указания веса, то присваивается значение&nbsp;1. | int      | O   | R   |        |
| priority                    | Приоритет адреса.<br>Диапазон: 0-65&nbsp;535.<br>Чем меньше значение, тем выше приоритет.<br>По умолчанию: 0.                                                                                    | int      | O   | R   |        |

#### Пример ####

```json
[
  {
    "name": "FullRule",
    "apnNi": "ims",
    "pgw": [
      {
        "fqdn": "pgw1.s5s8.site.node.epc.mnc001.mcc001.3gppnetwork.org",
        "ip": "192.168.126.244",
        "weight": 1,
        "priority": 1
      }
    ],
    "sgw": [
      {
        "ip": "192.168.0.19",
        "weight": 5,
        "priority": 2
      },
      {
        "ip": "192.168.0.20",
        "weight": 6,
        "priority": 2
      }
    ]
  },
  {
    "name": "Rule_1",
    "apnNi": "ims",
    "pgw": [
      {
        "fqdn": "pgw1.s5s8.site.node.epc.mnc001.mcc001.3gppnetwork.org",
        "ip": "192.168.126.244",
        "weight": 1,
        "priority": 1
      }
    ],
    "sgw": [
      {
        "ip": "192.168.0.19",
        "weight": 5,
        "priority": 2
      },
      {
        "ip": "192.168.0.20",
        "weight": 6,
        "priority": 2
      }
    ]
  },
  {
    "name": "Rule_2",
    "apnNi": "internet",
    "pgw": [
      {
        "ip": "192.168.126.241"
      },
      {
        "ip": "192.168.126.242"
      }
    ]
  },
  {
    "name": "Rule_3",
    "apnNi": "internet",
    "sgw": [
      {
        "fqdn": "sgw1.s11.site.node.epc.mnc0250mcc48.3gppnetwork.org",
        "ip": "192.168.0.19"
      }
    ],
    "pgw": [
      {
        "ip": "192.168.126.244"
      }
    ]
  }
]
```

### Адрес PGW

Адрес PGW может быть получен из ряда источников.
Приоритеты источников в порядке убывания:

1. [PGW_IP](#pgw_apn);
2. [dns.cfg](../../dns/);
3. [served_plmn.cfg::PGW_IP](../../plmn/#pgw_served_plmn).

### Адрес SGW

Адрес SGW может быть получен из ряда источников.
В зависимости от процедуры SGW выбирается либо по APN, либо по TAC.
В случае выбора по APN используется только [SGW_IP](#sgw_apn).