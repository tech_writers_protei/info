---
title: "emergency_numbers.json"
description: "Параметры номеров экстренных служб"
weight: 20
type: docs
---

В файле задаются связи между номерами экстренных служб и их типами.
Имя секции может использоваться в качестве ссылки в параметре [plmn.json::emergencyNumbers](../../plmn/#emergency-numbers).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../../oam/system_management/).

### Описание параметров ###

| Параметр       | Описание                                                                        | Тип    | O/M | P/R | Версия |
|----------------|---------------------------------------------------------------------------------|--------|-----|-----|--------|
| name           | Имя правила.                                                                    | string | M   | R   |        |
| number         | Номер.                                                                          | string | M   | R   |        |
| police         | Флаг использования номера для полиции.<br>По умолчанию: false.                  | bool   | O   | R   |        |
| ambulance      | Флаг использования номера для cкорой помощи.<br>По умолчанию: false.            | bool   | O   | R   |        |
| fireBrigade    | Флаг использования номера для пожарной охраны.<br>По умолчанию: false.          | bool   | O   | R   |        |
| marineGuard    | Флаг использования номера для береговой охраны.<br>По умолчанию: false.         | bool   | O   | R   |        |
| mountainRescue | Флаг использования номера для горноспасательной службы.<br>По умолчанию: false. | bool   | O   | R   |        |

#### Пример ####

```json
[
  {
    "name": "112",
    "number": "112",
    "police": true,
    "ambulance": true,
    "fireBrigade": true,
    "marineGuard": true,
    "mountainRescue": true
  },
  {
    "name": "911",
    "number": "911",
    "ambulance": true,
    "fireBrigade": true,
    "marineGuard": true,
    "mountainRescue": true
  },
  {
    "name": "AmbulanceOnly",
    "number": "03",
    "ambulance": true
  }
]
```