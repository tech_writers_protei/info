---
title: "imsi_rules.json"
description: "Параметры префиксов IMSI"
weight: 20
type: docs
---

В файле задаются правила, связывающие <abbr title="Tracking Area Code">TAC</abbr> с регулярными выражениями для номеров IMSI.
Имя секции является именем правила и может использоваться в качестве ссылки в параметре [plmn.json::imsiRules](../../plmn/#imsiRules).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../../oam/system_management/).

### Принцип работы

Набор правил позволяет формировать белый и черный списки IMSI.

* Если номер IMSI не удовлетворяет ни одному правилу, то считается, что находится в черном списке.
* Если для номера IMSI нашлось правило, то дальнейшие действия зависят от того, задано ли значение параметра [imsiList::emmCause](#emm-cause-imsi).
* Если значение параметра приведено, то номер IMSI оказывается в черном списке, и абонент получает отбой по причине [imsiList::emmCause](#emm-cause-imsi).
* Если значение параметра не приведено, то номер IMSI оказывается в белом списке.

Отсутствие правил делает любой IMSI участником белого списка.
Пустое значение [imsiList::prefix](#prefix) делает правило подходящим для любого номера IMSI.

### Поиск правила

Алгоритм поиска соответствующего правила:

1. Осуществляется поиск правила, у которого текущий TAC абонента находится в [tacList](#tac-list).
   1. Если такое нашлось, то из списка [imsiList](#imsi-list) осуществляется поиск правила, у которого значение [imsiList::prefix](#prefix) больше других совпадает с номером IMSI.
      1. Если такое нашлось, то параметры из правила применяются к абоненту.
      2. Если такое не нашлось, то абонент получает отказ с причиной [emmCause](#emm-cause-tac).
   2. Если такое не нашлось, то среди всех правил с пустым списком [tacList](#tac-list) осуществляется поиск такого, у которого значение [imsiList::prefix](#prefix) больше других совпадает с номером IMSI.
       1. Если такое нашлось, то параметры из правила применяются к абоненту.
       2. Если такое не нашлось, то абонент получает отказ с причиной `11, PLMN not allowed`.
2. Во всех остальных ситуациях абонент получает отказ с причиной `11, PLMN not allowed`.

### Описание параметров ###

| Параметр                                            | Описание                                                                                                                                                                                                                                                                                                                                                           | Тип       | O/M | P/R | Версия   |
|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------|-----|-----|----------|
| name                                                | Имя правила.                                                                                                                                                                                                                                                                                                                                                       | string    | M   | R   |          |
| <a name="tac-list">tacList</a>                      | Код области отслеживания для данного правила.                                                                                                                                                                                                                                                                                                                      | [int/hex] | O   | R   |          |
| <a name="emm-cause-tac">emmCause</a>                | Код причины отбоя в сообщении.<br>По умолчанию: 11, PLMN not allowed.                                                                                                                                                                                                                                                                                              | int       | O   | R   | 1.12.1.0 |
| <a name="imsi-list">imsiList</a>                    | Перечень правил, привязанных к префиксам IMSI.                                                                                                                                                                                                                                                                                                                     | [object]  | O   | R   |          |
| **{**                                               |                                                                                                                                                                                                                                                                                                                                                                    |           |     |     |          |
| &nbsp;&nbsp;<a name="prefix">prefix</a>             | Префикс IMSI.                                                                                                                                                                                                                                                                                                                                                      | string    | M   | R   |          |
| &nbsp;&nbsp;<a name="emm-cause-prefix">emmCause</a> | Код причины отбоя в сообщении. По умолчанию: не задан.                                                                                                                                                                                                                                                                                                             | int       | O   | R   |          |
| &nbsp;&nbsp;destRealm                               | Realm узла назначения.                                                                                                                                                                                                                                                                                                                                             | string    | O   | R   |          |
| &nbsp;&nbsp;destHost                                | Хост узла назначения.                                                                                                                                                                                                                                                                                                                                              | string    | O   | R   |          |
| &nbsp;&nbsp;apnOi                                   | Значение `APN OI`, добавляемый к значению `APN NI` для формирования полного APN.<br>**Примечание.** Если не задан или пуст, то APN OI формируется либо из IMSI, либо из PLMN                                                                                                                                                                                       | string    | O   | R   |          |
| &nbsp;&nbsp;<a name="apn-rules-imsi">apnRules</a>   | Перечень применяемых [правил APN](../apn_rules/).                                                                                                                                                                                                                                                                                                                  | [string]  | O   | R   | 1.50.1.0 |
| &nbsp;&nbsp;<a name="qos-rules">qosRules</a>        | Перечень применяемых [правил QoS](../qos_rules/).                                                                                                                                                                                                                                                                                                                  | [string]  | O   | R   |          |
| &nbsp;&nbsp;<a name="tac-rules-imsi">tacRules</a>   | Перечень применяемых [правил TAC](../tac_rules/).                                                                                                                                                                                                                                                                                                                  | [string]  | O   | R   | 1.50.1.0 |
| &nbsp;&nbsp;imsVops                                 | Флаг `IMS Voice over PS session indicator`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>**Примечание.** Учитывается только при активации [plmn.json::imsVops](../../plmn/#ims-vops) и отсутствии запрета со стороны [plmn.json::tacRules](../../plmn/#tac-rules).<br>По умолчанию: true. | bool      | O   | R   |          |
| &nbsp;&nbsp;featureList1                            | Битовая маска опций `Feature-List-ID 1`. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).<br>По умолчанию: 0x10000007.                                                                                                                                                                           | hex       | O   | R   |          |
| &nbsp;&nbsp;featureList2                            | Битовая маска опций `Feature-List-ID 2`. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).<br>По умолчанию: 0x08000000.                                                                                                                                                                           | hex       | O   | R   |          |
| &nbsp;&nbsp;foreignUeUsageType                      | Значение `UE Usage Type`, вынуждающее узел ММЕ отвечать отбоем на запросы S1 Attach Request и TAU-Request с сообщением S1 Reroute NAS Request.<br>Диапазон: 0-255.<br>По умолчанию: не задан.                                                                                                                                                                      | int       | O   | R   |          |
| **}**                                               |                                                                                                                                                                                                                                                                                                                                                                    |           |     |     |          |

#### Пример ####

```json
[
  {
    "name" : "RuleName1",
    "tacList" : [1, "3-10"],
    "emmCause" : 11,
    "imsiList" : [
      {
        "prefix": "2502010",
        "destRealm": "EPC.MNC020.MCC250.3GPPNETWORK.ORG",
        "destHost": "SAE-HSS01.NSK.EPC.MNC020.MCC250.3GPPNETWORK.ORG",
        "apnOi": "mnc040.mcc250.gprs",
        "qosRules": [ "Qos_Rule1" ],
        "imsVops": true,
        "featureList1": "0x00000000",
        "featureList2": "0x00000000"
      },
      {
        "prefix": "25048123",
        "destRealm": "EPC.MNC048.MCC250.3GPPNETWORK.ORG",
        "destHost": "SAE-HSS01.NSK.EPC.MNC048.MCC250.3GPPNETWORK.ORG",
        "qosRules": [ "Qos_Rule2", "Qos_Rule3" ],
        "imsVops": false,
        "featureList1": "0x00000001",
        "featureList2": "0x00000000",
        "foreignUeUsageType": 15
      }
    ]
  },
  {
    "name" : "RuleName2",
    "imsiList" : [
      { "prefix": "25048" }
    ]
  }
]
```