---
title: "emergency_pdn.json"
description: "Параметры PDN-подключений для экстренных служб"
weight: 20
type: docs
---

В файле задаются настройки PDN-подключений для служб передачи данных, применяемые в случае процедуры Emergency Attach без запроса профиля у узла HSS.
Имя секции может использоваться в качестве ссылки в параметре [plmn.json::emergencyPdn](../../plmn/#emergency-pdn).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../../oam/system_management/).

### Описание параметров ###

| Параметр       | Описание                                                                                                                                                                    | Тип                    | O/M | P/R | Версия |
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|-----|-----|--------|
| name           | Имя правила.                                                                                                                                                                | string                 | M   | R   |        |
| apnNi          | Индикатор сети APN.                                                                                                                                                         | string                 | M   | R   |        |
| servedPartyIp  | Обслуживаемый статический IP-адрес.<br>**Примечание.** Может быть указан один IPv4, один IPv6 или оба одновременно.                                                         | ip/string<br>ip,string | O   | R   |        |
| ambr           | Показатели скоростей передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для DL- и UL-направлений.                                                                | object                 | O   | R   |        |
| **{**          |                                                                                                                                                                             |                        |     |     |        |
| &nbsp;&nbsp;dl | Скорость передачи в направлении DL, в байтах в секунду. По умолчанию: 0.                                                                                                    | int                    | M   | R   |        |
| &nbsp;&nbsp;ul | Скорость передачи в направлении DL, в байтах в секунду. По умолчанию: 0.                                                                                                    | int                    | M   | R   |        |
| **}**          |                                                                                                                                                                             |                        |     |     |        |
| pdnType        | Тип подключения к сети передачи данных.<br>`IPv4` / `IPv6` / `IPv4v6` / `IPv4_or_IPv6`.<br>По умолчанию: IPv4.                                                              | string                 | O   | R   |        |
| qci            | Идентификатор класса QoS.<br>По умолчанию: 0.                                                                                                                               | int                    | O   | R   |        |
| priorityLvl    | Уровень приоритета.<br>По умолчанию: 0.                                                                                                                                     | int                    | O   | R   |        |
| preemptCap     | Флаг `Pre-emption Capability`.<br>См. [3GPP TS 23.107](https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf).<br>По умолчанию: 0.    | bool                   | O   | R   |        |
| preemptVuln    | Флаг `Pre-emption Vulnerability`.<br>См. [3GPP TS 23.107](https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf).<br>По умолчанию: 0. | bool                   | O   | R   |        |

#### Пример ####

```json
[
  {
    "name": "Rule_for_IMS",
    "apnNi": "ims",
    "servedPartyIp": [ "127.0.0.1", "::1" ],
    "ambr": {
      "dl": 100500,
      "ul": 100500
    },
    "pdnType": "IPv4v6",
    "priorityLvl": 1,
    "preemptVuln": true
  },
  {
    "name": "internet_rule",
    "apnNi": "internet",
    "servedPartyIp": [ "192.168.126.67" ],
    "pdnType": "IPv4",
    "qci": 7,
    "preemptCap": true,
    "preemptVuln": true
  }
]
```