---
title: "code_mapping_rules.json"
description: "Параметры Diameter: Result Code, Error Diagnostic и EMM Cause"
weight: 20
type: docs
---

В файле задаются правила, связывающие Diameter: Result-Code, Error-Diagnostic и EMM Cause.
Имя секции является именем правила и может использоваться в качестве ссылки в параметре
[plmn.json::codeMappingRules](../../plmn/#code-mapping-rules).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload plmn**, см. [Управление](../../../oam/system_management/).

### Описание параметров ###

| Параметр        | Описание                                                                                                                                                               | Тип    | O/M | P/R | Версия   |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| name            | Имя правила.                                                                                                                                                           | string | M   | R   |          |
| resultCode      | Перечень значений `Diameter: Result-Code`. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                                                        | [int]  | M   | R   | 1.37.2.0 |
| errorDiagnostic | Перечень значений `Error-Diagnostic`. См. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).               | [int]  | O   | R   | 1.48.1.0 |
| emmCause        | Причина отбоя сетью EMM-запроса от UE, `EMM Cause`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | int    | M   | R   | 1.37.2.0 |

#### Пример ####

```json
[
  {
    "name": "Rule1",
    "resultCode": [ 5421 ],
    "emmCause": 123
  },
  {
    "name": "Rule2",
    "resultCode": [ 1, 2, 3 ],
    "errorDiagnostic": [ 1, 2, 3 ],
    "emmCause": 1
  }
]
```