---
title: "mme.json"
description: "Основные параметры MME"
weight: 20
type: docs
---

В файле задаются основные настройки PROTEI MME.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload mme**, см. [Управление](../../oam/system_management/).

### Используемые секции ###

* **[general](#general-mme)** -- общие параметры узла;
* **[timeout](#timeout)** -- параметры времени ожидания;
  * **[hoTimeout](#ho_timeout)** -- параметры времени ожидания при хэндовере;
* **[database](#database)** -- параметры баз данных;
* **[gutiReallocation](#guti)** -- параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>;
* **[dcn](#dcn)** -- параметры <abbr title="Dedicated Core Network">DCN</abbr>;
* **[ranges](#ranges)** -- параметры допустимых диапазонов идентификаторов;
* **[profileRestoration](#profile_restoration)** -- флаги, разрешающие определенным процедурам поиск незнакомых
  профилей абонентов в базе данных;
* **[vendorSpecific](#vendor_specific)** -- параметры, задающие ограничения, вызванные оборудованием других производителей;
* **[enbId](#enodeb)** -- параметры допустимых eNodeB.

### Описание параметров ###

| Параметр                                                 | Описание                                                                                                                                                                                                                                                   | Тип      | O/M | P/R | Версия    |
|----------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|-----------|
| **<a name="general-mme">general</a>**                    | Основные параметры MME.                                                                                                                                                                                                                                    | object   | O   | P   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;handlers                                     | Количество задействованных логик. По умолчанию: 10.                                                                                                                                                                                                        | int      | O   | P   |           |
| &nbsp;&nbsp;coreCount                                    | Количество ядер. Диапазон: 1-14. По умолчанию: 2.                                                                                                                                                                                                          | int      | O   | P   |           |
| &nbsp;&nbsp;echoSendPeriod                               | Периодичность отправки сообщения Echo Request по интерфейсу GTP-C, в секундах.<br>Диапазон: 10-600. По умолчанию: 30.                                                                                                                                      | int      | O   | R   |           |
| &nbsp;&nbsp;isr                                          | Флаг `Operation Indication` в сообщении Delete Session Request.<br>По умолчанию: false.                                                                                                                                                                    | bool     | O   | R   |           |
| &nbsp;&nbsp;dnsIpv6                                      | Флаг запроса IPv6-адреса от DNS-сервера.<br>По умолчанию: false.                                                                                                                                                                                           | bool     | O   | R   |           |
| &nbsp;&nbsp;releaseonicsFail                             | Индикатор способа очистки контекста после получения сообщения ICS Failure.<br>`E` -- explicit, явно / `I` -- implicit, неявно.<br>По умолчанию: E.                                                                                                         | string   | O   | R   | 1.33.0.0  |
| &nbsp;&nbsp;releaseOnServiceReject                       | Индикатор способа очистки контекста после получения сообщения Service Reject.<br>`E` -- explicit, явно / `I` -- implicit, неявно.<br>По умолчанию: E.                                                                                                      | string   | O   | R   | 1.48.0.0  |
| &nbsp;&nbsp;ignoreEnbUeIdReset                           | Флаг игнорирования значения `UE-associated logical S1-connection Item` при проведении процедуры S1AP: RESET для устройств, имеющих указанный идентификатор `eNB UE S1AP ID`.<br>По умолчанию: false.                                                       | bool     | O   | R   |           |
| &nbsp;&nbsp;radioPriority                                | Значение `Radio Priority`, отправляемое в сообщении SGSN Context Response.<br>Диапазон: 0-7. По умолчанию: 2.                                                                                                                                              | int      | O   | R   | 1.47.1.0  |
| &nbsp;&nbsp;radioPrioritySms                             | Значение `Radio Priority SMS`, отправляемое в сообщении SGSN Context Response.<br>Диапазон: 0-7. По умолчанию: 1.                                                                                                                                          | int      | O   | R   | 1.47.1.0  |
| &nbsp;&nbsp;oldS1ReleaseCauseCode                        | Код причины, отправляемый в запросе S1AP: UE CONTEXT RELEASE COMMAND на удаление неактуального контекста, если обнаружено дублирование E-RAB на стороне MME.<br>`0` -- NORMAL_RELEASE / `1` -- RELEASE_DUE_TO_EUTRAN_GENERATED_REASON.<br>По умолчанию: 0. | int      | O   | R   | 1.48.2.0  |
| &nbsp;&nbsp;s1apPcsmThreads                              | Количество потоков, выделяемых для S1AP PCSM. По умолчанию: 1.                                                                                                                                                                                             | int      | O   | R   | 1.48.12.0 |
| &nbsp;&nbsp;srvccS1ReleaseCauseCode                      | Код причины, отправляемый в сообщении S1AP: UE CONTEXT RELEASE COMMAND после успешного SRVCC.<br>`0` -- NORMAL_RELEASE / `1` -- SUCCESSFUL_HANDOVER.<br>По умолчанию: 0.                                                                                   | int      | O   | R   | 1.49.0.0  |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="timeout">timeout</a>**                        | Параметры времени ожидания.                                                                                                                                                                                                                                | object   | O   | R   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;contextReqRelease                            | Время ожидания удаления сессии на предыдущем узле SGW после приёма Context-Request от нового узла ММЕ, в миллисекундах.<br>По умолчанию: 5&nbsp;000.                                                                                                       | int      | O   | R   |           |
| &nbsp;&nbsp;**<a name="ho-timeout">hoTimeout</a>**       | Параметры времени ожидания при хэндовере.                                                                                                                                                                                                                  | object   | O   | R   |           |
| &nbsp;&nbsp;**{**                                        |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;&nbsp;&nbsp;s1HoContextRelease               | Время ожидания очистки контекста на предыдущей базовой станции при хэндовере S1, в миллисекундах.<br>По умолчанию: 5&nbsp;000.                                                                                                                             | int      | O   | R   | 1.10.0.0  |
| &nbsp;&nbsp;&nbsp;&nbsp;hoFromUtranTau                   | Время ожидания сообщения TAU-Request при хэндовере из сети UTRAN, в миллисекундах.<br>По умолчанию: 10&nbsp;000.                                                                                                                                           | int      | O   | R   |           |
| &nbsp;&nbsp;&nbsp;&nbsp;hoFromGeranTau                   | Время ожидания сообщения TAU-Request при хэндовере из сети GERAN, в миллисекундах.<br>По умолчанию: 10&nbsp;000.                                                                                                                                           | int      | O   | R   |           |
| &nbsp;&nbsp;&nbsp;&nbsp;x2HoDelSession                   | Время ожидания отложенного удаления сессий на исходном SGW при X2-хэндовере, в миллисекундах.<br>По умолчанию: 2&nbsp;000.                                                                                                                                 | int      | O   | R   |           |
| &nbsp;&nbsp;&nbsp;&nbsp;eToUtranHoRelease                | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в UTRAN, в миллисекундах.<br>По умолчанию: 5&nbsp;000.                                                                                                                 | int      | O   | R   |           |
| &nbsp;&nbsp;&nbsp;&nbsp;eToGeranHoRelease                | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в GERAN, в миллисекундах.<br>По умолчанию: 5&nbsp;000.                                                                                                                 | int      | O   | R   |           |
| &nbsp;&nbsp;**}**                                        |                                                                                                                                                                                                                                                            |          |     |     |           |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="database">database</a>**                      | Параметры базы данных.                                                                                                                                                                                                                                     | object   | O   | P   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;enable                                       | Флаг взаимодействия с базой данных.<br>По умолчанию: false.                                                                                                                                                                                                | bool     | O   | R   |           |
| &nbsp;&nbsp;<a name="hostname">hostname</a>              | Перечень DNS-имен или IP-адресов хостов баз данных.<br>По умолчанию: [localhost].<br>**Примечание.** Если задан параметр [unixSock](#unix-sock), то игнорируется.                                                                                          | [string] | O   | P   |           |
| &nbsp;&nbsp;<a name="port">port</a>                      | Порт подключения к базе данных.<br>По умолчанию: 3306.<br>**Примечание.** Если задан параметр [unixSock](#unix-sock), то игнорируется.                                                                                                                     | int      | O   | P   |           |
| &nbsp;&nbsp;username                                     | Логин для авторизации в базе данных.                                                                                                                                                                                                                       | string   | O   | P   |           |
| &nbsp;&nbsp;password                                     | Пароль для авторизации в базе данных.                                                                                                                                                                                                                      | string   | O   | P   |           |
| &nbsp;&nbsp;dbName                                       | Название базы данных.                                                                                                                                                                                                                                      | string   | O   | P   |           |
| &nbsp;&nbsp;updatePeriod                                 | Периодичность обновления базы данных, в миллисекундах.<br>По умолчанию: 10&nbsp;000.                                                                                                                                                                       | int      | O   | R   |           |
| &nbsp;&nbsp;readerCount                                  | Количество соединений базы данных с правами на чтение.<br>Диапазон: 1-100. По умолчанию: 3.                                                                                                                                                                | int      | O   | P   |           |
| &nbsp;&nbsp;<a name="unix-sock">unixSock</a>             | Путь до сокета базы данных.<br>**Примечание.** Если задан, то параметры [Hostname](#hostname) и [Port](#port) игнорируются.                                                                                                                                | string   | O   | P   |           |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="guti">gutiReallocation</a>**                  | Параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</abbr>.                                                                                                                                                                  | object   | O   | R   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;reqCount                                     | Максимальное количество обработанных запросов Attach/TAU/Service Request до смены GUTI.<br>По умолчанию: 0.                                                                                                                                                | int      | O   | R   |           |
| &nbsp;&nbsp;reallocPeriod                                | Периодичность смены GUTI, в миллисекундах.<br>По умолчанию: 0.                                                                                                                                                                                             | int      | O   | R   |           |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="dcn">dcn</a>**                                | Параметры <abbr title="Dedicated Core Network">DCN</abbr>.                                                                                                                                                                                                 | [object] | O   | R   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;id                                           | Идентификатор сети.                                                                                                                                                                                                                                        | int      | M   | R   |           |
| &nbsp;&nbsp;relativeCapacity                             | Относительная емкость.                                                                                                                                                                                                                                     | int      | M   | R   |           |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="ranges">ranges</a>**                          | Диапазоны допустимых значений идентификаторов.                                                                                                                                                                                                             | object   | O   | P   | 1.44.2.0  |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;<a name="mme-ue-id">mmeUeId</a>              | Границы диапазона допустимых MME UE ID. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне 1-(2<sup>32</sup>-1).                                                                                                        | string   | O   | P   | 1.44.2.0  |
| &nbsp;&nbsp;<a name="m-tmsi">mTmsi</a>                   | Диапазон допустимых M-TMSI для GUTI. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне 1-(2<sup>32</sup>-1).                                                                                                           | string   | O   | P   | 1.44.2.0  |
| &nbsp;&nbsp;<a name="teid">teid</a>                      | Диапазон допустимых TEID, используемых для идентификации абонента на интерфейсе S11. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне 1-(2<sup>32</sup>-1).                                                           | string   | O   | P   | 1.44.2.0  |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="profile_restoration">profileRestoration</a>** | Параметры, определяющие набор процедур, при которых делается попытка найти незнакомый абонентский профиль в базе данных.                                                                                                                                   | object   | O   | P   | 1.46.3.0  |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;serviceReq                                   | Флаг разрешения поиска профиля при получении запроса Service Request.<br>По умолчанию: true.                                                                                                                                                               | bool     | O   | R   | 1.46.3.0  |
| &nbsp;&nbsp;extServiceReq                                | Флаг разрешения поиска профиля при получении Extended Service Request.<br>По умолчанию: false.                                                                                                                                                             | bool     | O   | R   | 1.46.3.0  |
| &nbsp;&nbsp;tau                                          | Флаг разрешения поиска профиля при получении запроса TAU Request.<br>По умолчанию: false.                                                                                                                                                                  | bool     | O   | R   | 1.46.3.0  |
| &nbsp;&nbsp;mtSms                                        | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT SMS.<br>По умолчанию: false.                                                                                                                                                      | bool     | O   | R   | 1.46.3.0  |
| &nbsp;&nbsp;mtCall                                       | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT Call.<br>По умолчанию: false.                                                                                                                                                     | bool     | O   | R   | 1.46.3.0  |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="vendor_specific">vendorSpecific</a>**         | Параметры ограничений, вызванные оборудованием других производителей.                                                                                                                                                                                      | object   | O   | P   | 1.46.2.0  |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;longUtranTransparentContainer                | Флаг разрешения использования длинного, более 1024 байт, `UTRAN Transparent Container` при хэндовере от 4G к 3G.<br>По умолчанию: true.                                                                                                                    | bool     | O   | R   | 1.46.2.0  |
| &nbsp;&nbsp;suspend                                      | Флаг поддержки процедуры Suspend для bearer-служб.<br>По умолчанию: false.                                                                                                                                                                                 | bool     | O   | R   | 1.46.2.0  |
| &nbsp;&nbsp;detachIndAfterHo                             | Флаг выполнения процедуры Detach Indication после перехода абонента к 2G/3G.<br>По умолчанию: true.                                                                                                                                                        | bool     | O   | R   | 1.46.3.0  |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| **<a name="enodeb">enbId</a>**                           | Перечень масок белого списка идентификаторов eNodeB.<br>**Примечание.** Если записей нет, то любой считается допустимым.                                                                                                                                   | [object] | O   | R   |           |
| **{**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |
| &nbsp;&nbsp;id                                           | Маска идентификатора.                                                                                                                                                                                                                                      | regex    | M   | R   |           |
| &nbsp;&nbsp;enable                                       | Флаг активности маски. По умолчанию: true.                                                                                                                                                                                                                 | bool     | O   | R   |           |
| **}**                                                    |                                                                                                                                                                                                                                                            |          |     |     |           |

**Примечание**. При задании значений [mTmsi](#m-tmsi), [teid](#teid), [mmeUeId](#mme-ue-id) должно выполняться соотношение `mTmsi >= teid >= mmeUeId`.

**Примечание.** Первый элемент массива [hostname](#hostname) считается первичным хостом: ММЕ будет пытаться установить соединение в первую очередь именно с ним.
При недоступности первичного будут попытки соединения с другими хостами, но в конечном итоге ММЕ стремится восстановить соединение с первичным.

#### Пример ####

```json
{
  "general": {
    "handlers": 100,
    "coreCount": 3,
    "releaseOnIcsFail": "I",
    "echoSendPeriod": 22,
    "radioPrioritySms": 3
  },
  "timeout": {
    "contextReqRelease": 3000,
    "handover": {
      "hoFromGeranTau": 8000,
      "eToGeranHoRelease": 4000
    }
  },
  "database": {
    "enable": true,
    "username": "mme",
    "password": "777",
    "dbName": "MME",
    "unixSock": "/var/lib/mysql/mysql.sock"
  },
  "gutiReallocation": { "reqCount": 100 },
  "dcn": [
    {
      "id": 10,
      "relativeCapacity": 50
    },
    {
      "id": 24,
      "relativeCapacity": 70
    }
  ],
  "ranges": {
    "mmeUeId": "50-10000",
    "teid": "100-50000"
  },
  "vendorSpecific": {
    "suspend": true,
    "detachIndAfterHo": false
  },
  "profileRestoration": { "mtSms": true },
  "enbId": [
    {
      "id": "107217"
    },
    {
      "enable": true,
      "id": "3584*"
    },
    {
      "enable": false,
      "id": "3585*"
    }
  ]
}
```