---
title: "ue_trace.json"
description: "Параметры трассировки абонентов"
weight: 20
type: docs
---

В файле задаются абоненты, по которым выводятся сообщения в **ue_trace**.
Каждому абоненту соответствует одна секция.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload ue_trace**, см.
[Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр                             | Описание      | Тип    | O/M | P/R | Версия |
|--------------------------------------|---------------|--------|-----|-----|--------|
| <a name="msisdn-ue-trace">msisdn</a> | Номер MSISDN. | string | C   | R   |        |
| <a name="imsi-ue-trace">imsi</a>     | Номер IMSI.   | string | C   | R   |        |

**Примечание.** Хотя бы один из параметров [msisdn](#msisdn-ue-trace), [imsi](#imsi-ue-trace) должен быть задан в каждой секции.

#### Пример ####

```json
[
  {
    "msisdn": "76000000316",
    "imsi": "001010000000398"
  },
  {
    "msisdn": "76000000317"
  },
  {
    "imsi": "001010000000399"
  }
]
```