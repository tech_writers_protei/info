---
title: "long_mnc.json"
description: "Параметры PLMN с трехзначными MNC"
weight: 20
type: docs
---

В файле задаются сети PLMN, имеющие трехзначные коды MNC.
Каждая секция соответствует одной сети PLMN.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload long_mnc**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр | Описание                                    | Тип    | O/M | P/R | Версия   |
|----------|---------------------------------------------|--------|-----|-----|----------|
| mcc      | Мобильный код страны.                       | string | M   | R   | 1.46.0.0 |
| mnc      | Код мобильной сети, состоящий из трех цифр. | string | M   | R   | 1.46.0.0 |

#### Пример ####

```json
[
  {
    "mcc": "404",
    "mnc": "854"
  },
  {
    "mcc": "404",
    "mnc": "874"
  }
]
```