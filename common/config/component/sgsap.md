---
title: "sgsap.json"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload component/sgsap**, см. [Управление](../../oam/system_management/).

### Описание параметров Sg.SGsAP {#sgsap}

| Параметр                                                        | Описание                                       | Тип      | O/M | P/R | Версия  |
|-----------------------------------------------------------------|------------------------------------------------|----------|-----|-----|---------|
| [peerTable](#peer-table)                                        | Перечень пиров.                                | [object] | O   | R   |         |
| **{**                                                           |                                                |          |     |     |         |
| &nbsp;&nbsp;gt                                                  | Глобальный заголовок пира.                     | string   | M   | R   |         |
| &nbsp;&nbsp;peerName                                            | Имя пира.                                      | string   | O   | R   |         |
| &nbsp;&nbsp;pcsmList                                            | Набор параметров отдельных PCSM.               | [object] | M   | R   |         |
| &nbsp;&nbsp;**{**                                               |                                                |          |     |     |         |
| &nbsp;&nbsp;&nbsp;&nbsp;peerIP                                  | IP-адрес пира.                                 | ip       | M   | R   | 1.1.0.0 |
| &nbsp;&nbsp;&nbsp;&nbsp;<a name="pcsm-component-sgsap">pcsm</a> | Компонентный адрес соответствующего узла PCSM. | string   | M   | R   | 1.1.0.0 |
| &nbsp;&nbsp;&nbsp;&nbsp;priority                                | Приоритет.<br>По умолчанию: 0, высший.         | int      | O   | R   | 1.1.0.0 |
| &nbsp;&nbsp;&nbsp;&nbsp;weight                                  | Вес.<br>По умолчанию: 1.                       | int      | O   | R   | 1.1.0.0 |
| &nbsp;&nbsp;**}**                                               |                                                |          |     |     |         |
| **}**                                                           |                                                |          |     |     |         |
| defaultPCSM                                                     | Перечень адресов узлов PCSM по умолчанию.      | [string] | O   | R   |         |

### Описание параметров Sg.SGsAP.PCSM ###

| Параметр                                                                         | Описание                                                                                                                                                                                                                                                                                          | Тип         | O/M | P/R | Версия   |
|----------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|-----|-----|----------|
| pcsmAddress                                                                      | Компонентый адрес компонента.                                                                                                                                                                                                                                                                     | string      | M   | R   |          |
| <a name="peer-ip">peerIp</a>                                                     | IP-адрес подключения узла PCSM.                                                                                                                                                                                                                                                                   | ip          | M   | R   |          |
| <a name="peer-port">peerPort</a>                                                 | Порт подключения узла PCSM.                                                                                                                                                                                                                                                                       | string      | M   | R   |          |
| <a name="src-ip">srcIp</a>                                                       | Локальный IP-адрес узла PCSM.<br>По умолчанию: [sgsap.json::localAddress::LocalHost](../../sgsap/#local-host-sgsap).                                                                                                                                                                              | string      | O   | R   |          |
| <a name="src-port">srcPort</a>                                                   | Локальный порт узла PCSM.<br>По умолчанию: [sgsap.json::localAddress::LocalPort](../../sgsap/#local-port-sgsap).                                                                                                                                                                                  | string      | O   | R   |          |
| remoteInterfaces                                                                 | Удаленные адреса для Multihoming. Формат:<br>`[ "<ip>:<port>" ]`.                                                                                                                                                                                                                                 | [{ip:port}] | O   | R   |          |
| localInterfaces                                                                  | Локальные адреса для Multihoming. Формат:<br>`[ "<ip>:<port>" ]`.                                                                                                                                                                                                                                 | [{ip:port}] | O   | R   | 1.37.2.0 |
| **sctpAdditionalInfo**                                                           | Дополнительные параметры SCTP.                                                                                                                                                                                                                                                                    | object      | O   | R   |          |
| **{**                                                                            |                                                                                                                                                                                                                                                                                                   |             |     |     |          |
| &nbsp;&nbsp;nodelay                                                              | Флаг активации SCTP nodelay. По умолчанию: false.                                                                                                                                                                                                                                                 | bool        | O   | P   |          |
| &nbsp;&nbsp;inStreams                                                            | Количество входящих потоков.<br>По умолчанию: 2.                                                                                                                                                                                                                                                  | int         | O   | P   |          |
| &nbsp;&nbsp;outStreams                                                           | Количество исходящих потоков.<br>По умолчанию: 2.                                                                                                                                                                                                                                                 | int         | O   | P   |          |
| &nbsp;&nbsp;maxInitRetransmits                                                   | Максимальное количество попыток отправить сообщение INIT/COOKIE ECHO.                                                                                                                                                                                                                             | int         | O   | P   |          |
| &nbsp;&nbsp;initTimeout                                                          | Время ожидания сообщения INIT.                                                                                                                                                                                                                                                                    | int         | O   | P   |          |
| &nbsp;&nbsp;rtoMax                                                               | Максимальное значение RTO.                                                                                                                                                                                                                                                                        | int         | O   | P   |          |
| &nbsp;&nbsp;rtoMin                                                               | Минимальное значение RTO.                                                                                                                                                                                                                                                                         | int         | O   | P   |          |
| &nbsp;&nbsp;rtoInitial                                                           | Начальное значение RTO.                                                                                                                                                                                                                                                                           | int         | O   | P   |          |
| &nbsp;&nbsp;hbInterval                                                           | Периодичность отправки сигнала heartbeat.                                                                                                                                                                                                                                                         | int         | O   | P   |          |
| &nbsp;&nbsp;dscp                                                                 | Значение поля заголовка IP DSCP/ToS.                                                                                                                                                                                                                                                              | int         | O   | P   | 4.1.8.55 |
| &nbsp;&nbsp;associationMaxRetrans                                                | Максимальное количество повторных отправок, при превышении которого маршрут считается недоступным.                                                                                                                                                                                                | int         | O   | P   |          |
| &nbsp;&nbsp;sackDelay                                                            | Время ожидания отправки сообщения SACK.                                                                                                                                                                                                                                                           | int         | O   | P   |          |
| &nbsp;&nbsp;sndBuf                                                               | Размер буфера сокета для отправки, параметр `net.core.wmem_default` Linux Kernel.<br>**Внимание.** Значение удваивается. Удвоенный размер не может превышать значение `net.core.wmem_max`.                                                                                                        | int         | O   | P   |          |
| &nbsp;&nbsp;shutdownEvent                                                        | Флаг включения индикации о событии `SHUTDOWN` от ядра.                                                                                                                                                                                                                                            | bool        | O   | P   |          |
| &nbsp;&nbsp;assocChangeEvent                                                     | Флаг включения индикации об изменении состояния ассоциации от ядра.                                                                                                                                                                                                                               | bool        | O   | P   |          |
| &nbsp;&nbsp;peerAddrChangeEvent                                                  | Флаг включения индикации об изменении состояния peer в ассоциации от ядра.                                                                                                                                                                                                                        | bool        | O   | P   |          |
| &nbsp;&nbsp;ipMtuDiscover                                                        | Индикатор разрешения использования алгоритма Path MTU Discovery.<br>`-1` -- нет изменений;<br>`0` -- алгоритм не используется, флаг заголовка IP `Don't fragment (DF)` не активирован;<br>`1` -- алгоритм используется, флаг заголовка IP `Don't fragment (DF)` активирован.<br>По умолчанию: -1. | int         | O   | R   |          |
| &nbsp;&nbsp;dscp                                                                 | Значение поля заголовка IP DSCP/ToS.<br>По умолчанию: -1, не задается.                                                                                                                                                                                                                            | int         | O   | R   |          |
| &nbsp;&nbsp;txBufStatistics                                                      | Флаг включения вывода в `si.log` о состоянии sctp-буфера.<br>По умолчанию: false.<br>**Примечание.** Должна быть включена запись журнала `si.log`, для чего достаточно задать `level = 1`. Информация выводится при заполнении SCTP-буфера.                                                       | bool        | O   | R   |          |
| &nbsp;&nbsp;<a name="warning-tx-buf-size-component-sgsap">warningTxBufSize</a>   | Порог для вывода сообщений SCTP-буфера в журнал `warning.log`, в байтах.<br>По умолчанию: 1024.                                                                                                                                                                                                   | int         | O   | R   |          |
| &nbsp;&nbsp;<a name="critical-tx-buf-size-component-sgsap">criticalTxBufSize</a> | Порог для срабатывания индикации о переполнении SCTP-буфера, в байтах.<br>По умолчанию: 0.                                                                                                                                                                                                        | int         | O   | R   |          |
| &nbsp;&nbsp;onCriticalTxBufSize                                                  | Действие по достижении [criticalTxBufSize](#critical-tx-buf-size-component-sgsap).<br>`0` -- отправка индикации о переполнении на верхний уровень;<br>`1` -- разрыв SCTP-ассоциации.<br>По умолчанию: 0.                                                                                          | int         | O   | R   |          |
| **}**                                                                            |                                                                                                                                                                                                                                                                                                   |             |     |     |          |

**Примечание.** При значениях `peerIp = ""` и `peerPort = 0` PCSM ожидает подключения с адреса, указанного в значении параметра [component/sgsap.json::peerTable::pcsmList::pcsm](#pcsm-component-sgsap).

#### Пример ####

```json
{
  "sgsap": {
    "peerTable": [
      {
        "gt": "79216567568",
        "peerName": "CC79.NDC216.LSP567568.3gppnetwork.org",
        "pcsmList": [
          {
            "peerIp": "192.168.125.154",
            "pcsm": "Sg.SGsAP.PCSM.1",
            "priority": 1,
            "weight": 1
          }
        ]
      }
    ],
    "defaultPcsm": [
      "Sg.SGsAP.PCSM.0"
    ]
  },
  "pcsm": [
    {
      "pcsmAddress": "Sg.SGsAP.PCSM.1",
      "peerIp": "192.168.125.154",
      "peerPort": 29118,
      "srcIp": "192.168.126.67",
      "srcPort": 29119,
      "remoteInterfaces": [],
      "localInterfaces": [],
      "sctpAdditionalInfo": {
        "rtoMax": 1500,
        "rtoMin": 500,
        "rtoInitial": 500,
        "hbInterval": 1500,
        "maxRetrans": 10,
        "dscp": 46,
        "warningTxBufSize": 1,
        "criticalTxBufSize": 1,
        "onCriticalTxBufSize": 1
      }
    }
  ]
}
```