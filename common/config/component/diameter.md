---
title : "diameter.json"
description: "Параметры компоненты Diameter"
weight: 20
type: docs
---

В файле задаются настройки компоненты Diameter.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload component/diameter**, см. [Управление](../../oam/system_management/).

### Описание компоненты DIAM

| Параметр                                                               | Описание                                                                                                           | Тип            | O/M | P/R | Версия   |
|------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|----------------|-----|-----|----------|
| **<a name="peer-table">peerTable</a>**                                 | Таблица хостов.                                                                                                    | [object]       | M   | P   |          |
| **{**                                                                  |                                                                                                                    |                |     |     |          |
| &nbsp;&nbsp;<a name="host-identity-component">hostIdentity</a>         | Идентификатор хоста.                                                                                               | string         | M   | P   |          |
| &nbsp;&nbsp;<a name="peer-table-pcsm">pcsm</a>                         | Компонентный адрес PCSM.                                                                                           | string         | M   | P   |          |
| &nbsp;&nbsp;<a name="remote-interfaces-component">remoteInterfaces</a> | Перечень адресов хоста. Формат:<br>`[ "<ip:port>" ]`.                                                              | [string<br>ip] | O   | P   | 4.1.11.2 |
| &nbsp;&nbsp;<a name="weight-peer-table">weight</a>                     | Вес PCSM. По умолчанию: 1.                                                                                         | int            | O   | P   | 4.1.10.0 |
| **}**                                                                  |                                                                                                                    |                |     |     |          |
| **<a name="routing-table">routingTable</a>**                           | Таблица realm.                                                                                                     | object         | O   | P   |          |
| **{**                                                                  |                                                                                                                    |                |     |     |          |
| &nbsp;&nbsp;<a name="component-realm">realm</a>                        | Идентификатор направления.                                                                                         | regex          | M   | P   |          |
| &nbsp;&nbsp;<a name="component-route">route</a>                        | Перечень хостов.                                                                                                   | [object]       | M   | P   |          |
| &nbsp;&nbsp;**{**                                                      |                                                                                                                    |                |     |     |          |
| &nbsp;&nbsp;&nbsp;&nbsp;peer                                           | Идентификатор хоста.                                                                                               | string         | M   | P   | 4.1.11.0 |
| &nbsp;&nbsp;&nbsp;&nbsp;<a name="weight-routing-table">weight</a>      | Вес хоста.<br>По умолчанию: 1.                                                                                     | int            | O   | P   | 4.1.11.0 |
| &nbsp;&nbsp;&nbsp;&nbsp;<a name="priority-routing-tablet">priority</a> | Приоритет хоста.<br>По умолчанию: 1.<br>**Примечание.** Чем меньше значение, тем более приоритетным является хост. | int            | O   | P   | 4.1.11.0 |
| &nbsp;&nbsp;**}**                                                      |                                                                                                                    |                |     |     |          |
| **}**                                                                  |                                                                                                                    |                |     |     |          |
| **<a name="default-pcsm">defaultPcsm</a>**                             | Перечень PCSM по умолчанию.                                                                                        | [object]       | O   | P   |          |
| **{**                                                                  |                                                                                                                    |                |     |     |          |
| &nbsp;&nbsp;pcsm                                                       | Компонентный адрес PCSM.                                                                                           | string         | M   | P   | 4.1.12.0 |
| &nbsp;&nbsp;<a name="weight-default-pcsm">weight</a>                   | Вес хоста.<br>По умолчанию: 1.                                                                                     | int            | O   | P   | 4.1.12.0 |
| &nbsp;&nbsp;<a name="priority-default-pcsm">priority</a>               | Приоритет хоста.<br>По умолчанию: 1.<br>**Примечание.** Чем меньше значение, тем более приоритетным является хост. | int            | O   | P   | 4.1.12.0 |
| **}**                                                                  |                                                                                                                    |                |     |     |          |

**Примечание.** Сначала маршрут выбирается из хостов с наивысшим приоритетом.
При их недоступности выбор производится из хостов со следующим приоритетом и т.д.

**Примечание.** Параметр [peerTable::weight](#weight-peer-table) используется для распределения нагрузки между PCSM с одинаковыми значениями [peerTable::hostIdentity](#host-identity-component).
При отправке сообщения по хосту каждый PCSM с подходящим `Host-Identity` может быть выбран с вероятностью

`Weight / TotalWeightPCSM`

* [Weight](#weight-peer-table) -- вес PCSM;
* TotalWeightPCSM -- сумма весов всех PCSM с данным Host-Identity.

**Примечание.**

Параметр [routingTable::route::weight](#weight-routing-table) используется для распределения нагрузки между хостами с одинаковым приоритетом [routingTable::route::priority](#priority-routing-table).

Параметр [defaultPcsm::weight](#weight-default-pcsm) используется для распределения нагрузки между хостами с одинаковым приоритетом [defaultPcsm::priority](#priority-default-pcsm).

Каждый хост может быть выбран с вероятностью

`Weight / TotalWeightHost`

* Weight -- вес хоста;
* TotalWeightHost -- сумма весов всех хостов с данным приоритетом.

### Описание компоненты DIAM.PCSM

| Параметр                                                    | Описание                                                                                                                                                                                                                                                                                                                                                                                        | Тип    | O/M | P/R | Версия   |
|-------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| pcsmAddress                                                 | Компонентный адрес PCSM.                                                                                                                                                                                                                                                                                                                                                                        | string | M   | P   |          |
| <a name="peer-ip-component-diam">peerIp</a>                 | Адрес сервера.<br>По умолчанию: 0.0.0.0.<br>**Примечание.** Только для client.                                                                                                                                                                                                                                                                                                                  | ip     | M   | P   |          |
| <a name="peer-port-component-diam">peerPort</a>             | Порт сервера.<br>По умолчанию: 0.<br>**Примечание.** Только для client.                                                                                                                                                                                                                                                                                                                         | int    | M   | P   |          |
| <a name="scr-ip">srcIp</a>                                  | Локальный адрес.<br>**Примечание.** Только для client.                                                                                                                                                                                                                                                                                                                                          | ip     | O   | P   |          |
| <a name="src-port">srcPort</a>                              | Локальный порт.<br>**Примечание.** Только для client.                                                                                                                                                                                                                                                                                                                                           | int    | O   | P   |          |
| originState                                                 | Идентификатор состояния, `Origin-State`.                                                                                                                                                                                                                                                                                                                                                        | int    | O   | P   |          |
| originHost                                                  | Идентификатор хоста, `Origin-Host`.<br>По умолчанию: значение [diameter.cfg::localPeerCapabilities::Origin-Host](../../diameter/#origin-host-diam).                                                                                                                                                                                                                                             | string | O   | P   |          |
| originRealm                                                 | Realm хоста, `Origin-Realm`.<br>По умолчанию: значение [diameter.cfg::localPeerCapabilities::Origin-Realm](../../diameter/#origin-realm-diam).                                                                                                                                                                                                                                                  | string | O   | P   |          |
| replaceOriginIdentities                                     | Флаг подмены `Origin-Host` и `Origin-Realm` на указанные в PCSM значения во всех отправляемых сообщениях. По умолчанию: значение [diameter.json::general::replaceOriginIdentities](../../diameter/#replace-origin-identities).                                                                                                                                                                  | bool   | O   | P   | 4.1.12.5 |
| transport                                                   | Протокол транспортного уровня.<br>`tcp` / `sctp`.<br>По умолчанию: tcp.<br>**Примечание.** Только для **client**.                                                                                                                                                                                                                                                                               | string | O   | P   |          |
| inStreams                                                   | Количество входящих потоков.<br>По умолчанию: 2.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                | int    | O   | P   |          |
| outStreams                                                  | Количество исходящих потоков.<br>По умолчанию: 2.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                               | int    | O   | P   |          |
| maxInitRetransmits                                          | Максимальное количество попыток отправить сообщение INIT/COOKIE ECHO.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                           | int    | O   | P   |          |
| initTimeout                                                 | Время ожидания сообщения INIT.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                                  | int    | O   | P   |          |
| rtoMax                                                      | Максимальное значение RTO.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                                      | int    | O   | P   |          |
| rtoMin                                                      | Минимальное значение RTO.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                                       | int    | O   | P   |          |
| rtoInitial                                                  | Начальное значение RTO.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                                         | int    | O   | P   |          |
| hbInterval                                                  | Периодичность отправки сигнала heartbeat.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                       | int    | O   | P   |          |
| dscp                                                        | Значение поля заголовка IP DSCP/ToS.<br>**Примечание.** Только для client.                                                                                                                                                                                                                                                                                                                      | int    | O   | P   | 4.1.8.55 |
| associationMaxRetrans                                       | Максимальное количество повторных отправок, при превышении которого маршрут считается недоступным.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                              | int    | O   | P   |          |
| sackDelay                                                   | Время ожидания отправки сообщения SACK.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                                         | int    | O   | P   |          |
| sndBuf                                                      | Размер буфера сокета для отправки, параметр `net.core.wmem_default` Linux Kernel.<br>**Внимание.** Значение удваивается. Удвоенный размер не может превышать значение `net.core.wmem_max`.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                      | int    | O   | P   |          |
| shutdownEvent                                               | Флаг включения индикации о событии `SHUTDOWN` от ядра.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                                          | bool   | O   | P   |          |
| assocChangeEvent                                            | Флаг включения индикации об изменении состояния ассоциации от ядра.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                             | bool   | O   | P   |          |
| peerAddrChangeEvent                                         | Флаг включения индикации об изменении состояния peer в ассоциации от ядра.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                                                                      | bool   | O   | P   |          |
| <a name="local-ifaces-component-diam">localInterfaces</a>   | Перечень локальных интерфейсов. См. [LocalAddress](#localandremoteaddress-diam). Формат:<br>`{ <ip:port>; <ip:port>; }`.<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                        | iort   | O   | P   |          |
| <a name="remote-ifaces-component-diam">remoteInterfaces</a> | Перечень удаленных интерфейсов. См. [RemoteAddress](#localandremoteaddress-diam). Формат:<br>`{ <ip:port>; <ip:port>; }`<br>**Примечание.** Только для **client, sctp**.                                                                                                                                                                                                                        | iort   | O   | P   |          |
| applTimeout                                                 | Максимальное время ожидания установления Diameter-соединения, в миллисекундах.<br>**Примечание.** Отсчитывается с момента отправки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer.<br>По умолчанию: значение [diameter.json::timers::applTimeout](../../diameter/#appl-timeout).                                                                    | int    | O   | P   | 4.1.8.39 |
| watchdogTimeout                                             | Максимальное время ожидания сообщений Diameter: Device-Watchdog-Request/Answer, в миллисекундах.<br>**Примечание.** Учитывается время прошедшее с момента посылки последнего сообщения, не обязательно Diameter: DWR.<br>По умолчанию: значение [diameter.json::timers::watchdogTimeout](../../diameter/#watchdog-timeout).                                                                     | int    | O   | P   | 4.1.8.39 |
| reconnectTimeout                                            | Максимальное время ожидания переустановления соединения, в миллисекундах.<br>**Примечание.** Учитывается время от разрушения соединения до очередной попытки восстановления.<br>По умолчанию: значение [diameter.json::timers::reconnectTimeout](../../diameter/#reconnect-timeout).                                                                                                            | int    | O   | P   | 4.1.8.39 |
| onBusyReconnectTimeout                                      | Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = BUSY`, в миллисекундах.<br>**Примечание.** Если 0, то соединение не переустанавливается.<br>По умолчанию: значение [diameter.json::timers::onBusyReconnectTimeout](../../diameter/#on-busy-reconnect-timeout).                                | int    | O   | P   | 4.1.8.43 |
| onShutdownReconnectTimeout                                  | Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU`, в миллисекундах.<br>**Примечание.** Если 0, то соединение не переустанавливается.<br>По умолчанию: значение [diameter.json::timers::onShutdownReconnect_Timeout](../../diameter/#on-shutdown-reconnect-timeout). | int    | O   | P   | 4.1.8.43 |
| responseTimeout                                             | Максимальное время ожидания ответа, в миллисекундах.<br>По умолчанию: значение [diameter.json::timers::responseTimeout](../../diameter/#response-timeout).                                                                                                                                                                                                                                      | int    | O   | P   | 4.1.8.39 |
| breakdownTimeout                                            | Продолжительность временной недоступности PCSM, в миллисекундах.<br>По умолчанию: значение [diameter.json::timers::breakdownTimeout](../../diameter/#breakdown-timeout).                                                                                                                                                                                                                        | int    | O   | P   | 4.1.8.39 |
| statisticTimeout                                            | Период записи статистики в лог-файлы, в миллисекундах.<br>По умолчанию: значение [diameter.json::timers::statisticTimeout](../../diameter/#statistic-timeout).                                                                                                                                                                                                                                  | int    | O   | P   | 4.1.8.39 |
| <a name="tr-man-interval">trafficManagerInterval</a>        | Период подсчета количества входящих и исходящих запросов, в миллисекундах.<br>По умолчанию: 1000.                                                                                                                                                                                                                                                                                               | int    | O   | P   | 4.1.8.60 |
| maxTransactions                                             | Максимальное количество запросов за период подсчета запросов, [trafficManagerInterval](#tr-man-interval).<br>**Примечание.** Если 0, то ограничение не проверяется.<br>На входящие запросы сверх лимита отправляется ответ с `Result-Code = TOO_BUSY (3002)`. На исходящие запросы сверх лимита в логику отправляется Pr_DIAM_SEND_DATA_REJ.<br>По умолчанию: 0.                                | int    | O   | P   | 4.1.8.60 |

#### Пример

```json
{
  "diam": {
    "peerTable": [
      {
        "hostIdentity": "hss02.mnc001.mcc001.3gppnetwork.org",
        "pcsm": "Sg.DIAM.PCSM.1"
      },
      {
        "hostIdentity": "hss.epc.mnc048.mcc250.3gppnetwork.org",
        "pcsm": "Sg.DIAM.PCSM.2"
      },
      {
        "hostIdentity": "eir.epc.mnc01.mcc250.3gppnetwork.org",
        "pcsm": "Sg.DIAM.PCSM.3"
      },
      {
        "hostIdentity": "scef.epc.mnc048.mcc250.3gppnetwork.org",
        "pcsm": "Sg.DIAM.PCSM.4"
      },
      {
        "hostIdentity": "scef1.epc.mnc048.mcc250.3gppnetwork.org",
        "pcsm": "Sg.DIAM.PCSM.5"
      },
      {
        "hostIdentity": "hss.ims.protei.ru",
        "pcsm": "Sg.DIAM.PCSM.6"
      }
    ],
    "routingTable": [
      {
        "realm": "epc.mnc048.mcc250.3gppnetwork.org",
        "route": [
          { "peer": "scef.epc.mnc048.mcc250.3gppnetwork.org" }
        ]
      },
      {
        "realm": "epc.mnc990.mcc999.3gppnetwork.org",
        "route": [
          { "peer": "hss02.mnc001.mcc001.3gppnetwork.org" }
        ]
      },
      {
        "realm": "test.protei.ru",
        "route": [
          { "peer": "hss.ims.protei.ru" }
        ]
      },
      {
        "realm": "protei.ru",
        "route": [
          { "peer": "hss.ims.protei.ru" }
        ]
      }
    ],
    "defaultPcsm": [
      { "pcsm": "Sg.DIAM.PCSM.2" }
    ]
  },
  "pcsm": [
    {
      "pcsmAddress": "Sg.DIAM.PCSM.1",
      "peerIp": "192.168.125.182",
      "peerPort": 13869,
      "transport": "sctp",
      "dscp": 46
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.2",
      "peerIp": "192.168.126.67",
      "peerPort": 13869,
      "transport": "sctp",
      "dscp": 46
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.3",
      "peerIp": "192.168.44.253",
      "peerPort": 3878,
      "transport": "sctp",
      "dscp": 46
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.4",
      "peerIp": "192.168.102.133",
      "peerPort": 3868,
      "transport": "sctp",
      "dscp": 46
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.5",
      "transport": "sctp"
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.6",
      "peerIp": "192.168.110.140",
      "peerPort": 3868,
      "dscp": 46
    }
  ]
}
```

### Конфигурация local- и remote-адресов {#localandremoteaddress-diam}

Для серверных компонент IP-адрес, порт и адреса для мультихоуминга задаются параметрами:

* [diameter.json::localAddress::localHost](../../diameter/#local_host);
* [diameter.json::localAddress::localPort](../../diameter/#local_port);
* [diameter.json::localAddress::local_interfaces](../../diameter/#local_interfaces).

Параметры из этого файла не используются.

Компонента является клиентской, если:

* задан [peerIp](#peer-ip-component-diam) для tcp;
* задан [peerIp](#peer-ip-component-diam) или [remoteInterfaces](#remote-ifaces-component-diam) для sctp.

#### Выбор адреса клиентских компонент

| Параметр     | ip                                                                                                                                               | port                                                                                            |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------|
| tcp, local   | [srcIp](#scr-ip);<br>[diameter.json::localAddress::localHost](../../diameter/#local_host)                                                        | [srcPort](#src-port);<br>Случайный выбор                                                        |
| tcp, remote  | [peerIp](#peer-ip-component-diam)                                                                                                                | [peerPort](#peer-port-component-diam)                                                           |
| sctp, local  | [srcIp](#scr-ip);<br>[localInterfaces](#local-ifaces-component-diam)[0];<br>[diameter.json::localAddress::localHost](../../diameter/#local_host) | [srcPort](#src-port);<br>[localInterfaces](#local-ifaces-component-diam)[0];<br>Случайный выбор |
| sctp, remote | [peerIp](#peer-ip-component-diam);<br>[remoteInterfaces](#remote-ifaces-component-diam)[0]                                                       | [peerPort](#peer-port-component-diam);<br>[remoteInterfaces](#remote-ifaces-component-diam)[0]  |

Для sctp, client адреса [local_interfaces](#local-ifaces-component-diam), [remote_interfaces](#remote-ifaces-component-diam) рассматриваются как набор из 1 основного и дополнительных.

#### Обязательность параметров клиентских компонент

| Параметр                                          | tcp | sctp                                                                                     |
|---------------------------------------------------|-----|------------------------------------------------------------------------------------------|
| [peerIp](#peer-ip-component-diam)                 | M   | M, если не задан IP-адрес в [remoteInterfaces](#remote-ifaces-component-diam)            |
| [peerPort](#peer-port-component-diam)             | M   | M, если не задан порт в [remoteInterfaces](#remote-ifaces-component-diam)                |
| [srcIp](#scr-ip)                                  | O   | O                                                                                        |
| [srcPort](#src-port)                              | O   | O                                                                                        |
| [localInterfaces](#local-ifaces-component-diam)   | --  | O                                                                                        |
| [remoteInterfaces](#remote-ifaces-component-diam) | --  | M, если не задан [peerIp](#peer-ip-component-diam)/[peerPort](#peer-port-component-diam) |
