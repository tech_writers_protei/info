---
title: "metrics.json"
description: "Параметры сбора метрик"
weight: 20
type: docs
---

В файле задаются настройки сбора метрик.

**Предупреждение.** При модификации файла новая конфигурация будет применена только после перезапуска приложения ММЕ.

### Используемые секции ###

* **[general](#general-metrics)** -- общие параметры сбора метрик;
* **[s1Interface](#s1Interface)** -- параметры сбора метрик S1;
* **[s1Attach](#s1Attach)** -- параметры сбора метрик S1, связанных с регистрацией абонента;
* **[s1Detach](#s1Detach)** -- параметры сбора метрик S1, связанных с дерегистрацией абонента;
* **[s1BearerActivation](#s1BearerActivation)** -- параметры сбора метрик S1, связанных с активацией bearer-служб;
* **[s1BearerDeactivation](#s1BearerDeactivation)** -- параметры сбора метрик S1, связанных с деактивацией bearer-служб;
* **[s1BearerModification](#s1BearerModification)** -- параметры сбора метрик S1, связанных с модификацией bearer-служб;
* **[s1Security](#s1Security)** -- параметры сбора метрик S1, связанных с аутентификацией абонента;
* **[s1Service](#s1Service)** -- параметры сбора метрик S1, связанных с процедурой Service Request;
* **[handover](#handover)** -- параметры сбора метрик, связанных с процедурой Handover;
* **[tau](#tau)** -- параметры сбора метрик, связанных с процедурой Tracking Area Update;
* **[sgsInterface](#sgsInterface)** -- параметры сбора метрик SGs;
* **[svInterface](#svInterface)** -- параметры сбора метрик Sv (SRVCC HO);
* **[resource](#resource)** -- параметры сбора метрик, связанных с ресурсами хоста;
* **[users](#users)** -- параметры сбора различных счётчиков;
* **[s11Interface](#s11Interface)** -- параметры сбора метрик S11;
* **[paging](#paging)** -- параметры сбора метрик, связанных с процедурой Paging;
* **[diameter](#diameter)** -- параметры сбора метрик, связанных с протоколом Diameter;
* **[s6aInterface](#s6a-interface)** -- параметры сбора метрик, связанных с интерфейсом S6a.

### Описание параметров ###

| Параметр                                                    | Описание                                                                                      | Тип                 | O/M | P/R | Версия   |
|-------------------------------------------------------------|-----------------------------------------------------------------------------------------------|---------------------|-----|-----|----------|
| **<a name="general-metrics">general</a>**                   | Общие параметры сбора метрик.                                                                 | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **{**                                                       |                                                                                               |                     |     |     |          |
| &nbsp;&nbsp;<a name="enable-metrics">enable</a>             | Флаг сбора метрик.<br>По умолчанию: false.                                                    | bool                | O   | P   | 1.45.0.0 |
| &nbsp;&nbsp;<a name="directory-metrics">directory</a>       | Директория для хранения метрик.<br>По умолчанию: ../metrics.                                  | string              | O   | P   | 1.45.0.0 |
| &nbsp;&nbsp;<a name="granularity">granularity</a>           | Гранулярность сбора метрик, в минутах.<br>По умолчанию: 5.                                    | int                 | O   | P   | 1.45.0.0 |
| &nbsp;&nbsp;<a name="node-name">nodeName</a>                | Имя узла, указываемое в названии файла с метриками.                                           | string              | O   | P   | 1.45.0.0 |
| &nbsp;&nbsp;<a name="hh-mm-separator">hhMmSeparator</a>     | Флаг включения разделителя между часами и минутами в формате `HH:MM`.<br>По умолчанию: false. | bool                | O   | P   | 1.49.0.0 |
| **}**                                                       |                                                                                               |                     |     |     |          |
| **<a name="s1Interface">s1Interface</a>**                   | Параметры для метрик S1.                                                                      | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="s1Attach">s1Attach</a>**                         | Параметры для метрик S1, связанных с регистрацией абонента.                                   | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="s1Detach">s1Detach</a>**                         | Параметры для метрик S1, связанных с дерегистрацией абонента.                                 | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="s1BearerActivation">s1BearerActivation</a>**     | Параметры для метрик S1, связанных с активацией bearer-службы.                                | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="s1BearerDeactivation">s1BearerDeactivation</a>** | Параметры для метрик S1, связанных с деактивацией bearer-службы.                              | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="s1BearerModification">s1BearerModification</a>** | Параметры для метрик S1, связанных с модификацией bearer-службы.                              | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="s1Security">s1Security</a>**                     | Параметры для метрик S1, связанных с аутентификацией абонента.                                | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="s1Service">s1Service</a>**                       | Параметры для метрик S1, связанных с процедурой Service Request.                              | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="handover">handover</a>**                         | Параметры для метрик, связанных с процедурой Handover.                                        | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="tau">tau</a>**                                   | Параметры для метрик, связанных с процедурой Tracking Area Update.                            | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="sgsInterface">sgsInterface</a>**                 | Параметры для метрик SGs.                                                                     | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="svInterface">svInterface</a>**                   | Параметры для метрик Sv (SRVCC HO).                                                           | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="resource">resource</a>**                         | Параметры для метрик, связанных с ресурсами хоста.                                            | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="users">users</a>**                               | Параметры сбора различных счётчиков.                                                          | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="s11Interface">s11Interface</a>**                 | Параметры для метрик S11.                                                                     | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="paging">paging</a>**                             | Параметры для метрик, связанных с процедурой Paging.                                          | [Metrics](#metrics) | O   | P   | 1.46.0.0 |
| **<a name="diameter">diameter</a>**                         | Параметры для метрик Diameter.                                                                | [Metrics](#metrics) | O   | P   | 1.45.0.0 |
| **<a name="s6a-interface">s6aInterface</a>**                | Параметры для метрик S6a.                                                                     | [Metrics](#metrics) | O   | P   | 1.45.0.0 |

#### Пример ####

```json
{
  "general": {
    "enable": true,
    "granularity": 30,
    "directory": "/usr/protei/Protei_MME/metrics",
    "nodeName": "MME-1"
  },
  "diameter": {
    "enable": true,
    "granularity": 15
  },
  "s6aInterface": {
    "enable": true,
    "granularity": 1
  }
}
```

### Тип Metrics {#metrics-type}

| Параметр      | Описание                                                                                                                                    | Тип    | O/M | P/R | Версия   |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| enable        | Флаг сбора метрик.<br>По умолчанию: значение [general::enable](#enable-metrics).                                                            | bool   | O   | P   | 1.45.0.0 |
| directory     | Директория для хранения метрик.<br>По умолчанию: значение [general::directory](#directory-metrics).                                         | string | O   | P   | 1.45.0.0 |
| granularity   | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [general::granularity](#granularity).                                      | int    | O   | P   | 1.45.0.0 |
| nodeName      | Имя узла, указываемое в названии файла с метриками.<br>По умолчанию: значение [general::nodeName](#node-name).                              | string | O   | P   | 1.45.0.0 |
| hhMmSeparator | Флаг включения разделителя между часами и минутами в формате `HH:MM`.<br>По умолчанию: значение [general::hhMmSeparator](#hh-mm-separator). | bool   | O   | P   | 1.49.0.0 |
