---
title: "dns.json"
description: "Параметры DNS"
weight: 20
type: docs
---

В файле задаются настройки DNS.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload dns**, см. [Управление](../../oam/system_management/).

## Используемые секции ##

* **[client](#client-dns)** -- параметры клиентской части;
* **[server](#server-dns)** -- параметры серверной части;
* **[clientPool](#client-pool)** -- параметры пула клиентов.

### Описание параметров ###

| Параметр                                 | Описание                                                                                                                                                        | Тип      | O/M | P/R | Version |
|------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|---------|
| **<a name="client-dns">client</a>**      | Параметры клиентской части.                                                                                                                                     | [object] | M   | P   |         |
| **{**                                    |                                                                                                                                                                 |          |     |     |         |
| &nbsp;&nbsp;id                           | Уникальный идентификатор направления.                                                                                                                           | int      | M   | P   |         |
| &nbsp;&nbsp;transport                    | Транспортный протокол.<br>`UDP` / `TCP`.<br>По умолчанию: UDP.                                                                                                  | string   | O   | P   |         |
| &nbsp;&nbsp;destAddress                  | IP-адрес сервера, куда направляются запросы.                                                                                                                    | ip       | M   | P   |         |
| &nbsp;&nbsp;destPort                     | Прослушиваемый порт сервера.<br>По умолчанию: 53.                                                                                                               | int      | O   | P   |         |
| &nbsp;&nbsp;srcAddress                   | Локальный IP-адрес.<br>По умолчанию: 0.0.0.0.                                                                                                                   | ip       | O   | P   |         |
| &nbsp;&nbsp;srcPort                      | Локальный порт.<br>По умолчанию: 0.                                                                                                                             | int      | O   | P   |         |
| &nbsp;&nbsp;responseTimeout              | Время ожидания ответа, в секундах.<br>По умолчанию: 60.                                                                                                         | int      | O   | P   |         |
| &nbsp;&nbsp;resendOverTcp                | Флаг перепосылки запроса по TCP для получения полного ответа.<br>По умолчанию: false.                                                                           | bool     | O   | P   |         |
| &nbsp;&nbsp;dscp                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>.<br>По умолчанию: 0.           | int      | O   | P   | 1.0.1.1 |
| &nbsp;&nbsp;ipMtuDiscover                | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: 0. | int      | O   | P   | 1.0.2.9 |
| **}**                                    |                                                                                                                                                                 |          |     |     |         |
| **<a name="server-dns">server</a>**      | Параметры серверной части.                                                                                                                                      | [object] | M   | P   |         |
| **{**                                    |                                                                                                                                                                 |          |     |     |         |
| &nbsp;&nbsp;id                           | Идентификатор направления.                                                                                                                                      | int      | M   | P   |         |
| &nbsp;&nbsp;transport                    | Транспортный протокол.<br> `UDP` / `TCP`.<br>По умолчанию: UDP.                                                                                                 | string   | O   | P   |         |
| &nbsp;&nbsp;address                      | Прослушиваемый IP-адрес.<br>По умолчанию: 0.0.0.0.                                                                                                              | ip       | O   | P   |         |
| &nbsp;&nbsp;port                         | Прослушиваемый порт.<br>По умолчанию: 53.                                                                                                                       | int      | O   | P   |         |
| &nbsp;&nbsp;dscp                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>.<br>По умолчанию: 0.           | int      | O   | P   | 1.0.1.1 |
| &nbsp;&nbsp;ipMtuDiscover                | Значение опции `IP_MTU_DISCOVER`.<br>Диапазон: 0-5.<br>**Примечание.** Значения 1, 2, 3 активируют флаг заголовка IP `Don't fragment (DF)`.<br>По умолчанию: 0. | int      | O   | P   | 1.0.2.9 |
| **}**                                    |                                                                                                                                                                 |          |     |     |         |
| **<a name="client-pool">clientPool</a>** | Параметры пула клиентов.                                                                                                                                        | object   | O   | P   | 1.0.2.0 |
| **{**                                    |                                                                                                                                                                 |          |     |     |         |
| &nbsp;&nbsp;id                           | Идентификатор пула клиентских направлений.                                                                                                                      | int      | M   | P   | 1.0.2.0 |
| &nbsp;&nbsp;directions                   | Перечень клиентских направлений, задействованных в балансировке.                                                                                                | int      | M   | P   | 1.0.2.0 |
| **}**                                    |                                                                                                                                                                 |          |     |     |         |

#### Пример ####

```json
{
  "client": [
    {
      "id": 1,
      "destAddress": "8.8.8.8",
      "resendOverTcp": true
    },
    {
      "id": 2,
      "destAddress": "127.0.0.1",
      "destPort": 1234,
      "responseTimeout": 10
    },
    {
      "id": 3,
      "destAddress": "127.0.0.1",
      "destPort": 53,
      "dscp": 56
    }
  ],
  "server": [
    {
      "id": 1,
      "address": "127.0.0.1",
      "port": 1234
    },
    {
      "id": 2
    }
  ],
  "clientPool": [
    {
      "id": 1,
      "directions": [
        2,
        3
      ]
    }
  ]
}
```