---
title: "diam_dest.cfg"
description: "Параметры IMSI, Destination-Host и Destination-Realm"
weight: 20
type: docs
---

В файле задаются правила, связывающие IMSI и параметры Destination-Host и Destination-Realm. Каждой связи соответствует одной секцией.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload diam_dest.cfg**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр                           | Описание                                                                                                                                                                               | Тип          | O/M | P/R | Версия   |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|-----|-----|----------|
| IMSI                               | Номер или маска IMSI.                                                                                                                                                                  | string/regex | M   | R   |          |
| DestHost                           | Хост назначения.                                                                                                                                                                       | string       | O   | R   |          |
| <a name="dest-realm">DestRealm</a> | Realm назначения.                                                                                                                                                                      | string       | O   | R   |          |
| FeatureList1                       | Битовая маска опций Feature-List-ID 1. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).<br>По умолчанию: 0x10000007. | binary       | O   | R   | 1.17.0.0 |
| FeatureList2                       | Битовая маска опций Feature-List-ID 2. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).<br>По умолчанию: 0x08000000. | binary       | O   | R   | 1.17.0.0 |

#### Пример ####

```ini
{
  IMSI = "001010000000315$";
  DestHost = "hss.epc.mnc02.mcc001.3gppnetwork.org";
  DestRealm = "epc.mnc02.mcc001.3gppnetwork.org";
  FeatureList1 = "0x10000207";
  FeatureList2 = "0x08000011";
}
```