---
title: "code_mapping_rules.cfg"
description: "Параметры Diameter: Result Code, Error Diagnostic и EMM Cause"
weight: 20
type: docs
---

В файле задаются правила, связывающие Diameter: Result-Code, Error-Diagnostic и EMM Cause.
Каждому правилу соответствует одна секция.
Имя секции является именем правила и может использоваться в качестве ссылки в параметре
[served_plmn.cfg::CodeMapping_rules](../served_plmn/#code-mapping-rules).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload served_plmn.cfg**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр        | Описание                                                                                                                                                                      | Тип     | O/M | P/R | Версия   |
|-----------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------|-----|-----|----------|
| ResultCode      | Значение Diameter: Result-Code. См. [RFC 6733](https://tools.ietf.org/html/rfc6733). Формат:<br>`"<code>,<code>"`.                                                            | \[int\] | M   | R   | 1.37.2.0 |
| ErrorDiagnostic | Значение `Error-Diagnostic`. См. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf). Формат:<br>`"<code>,<code>"`. | \[int\] | O   | R   | 1.48.1.0 |
| EMM_Cause       | Причина отбоя сетью EMM-запроса от UE, `EMM Cause`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).        | int     | M   | R   | 1.37.2.0 |

#### Пример ####

```ini
[Rule1]
ResultCode = 5421;
EMM_Cause = 123;

[Rule2]
ResultCode = "1,2,3";
EMM_Cause = 1;

[Rule3]
ResultCode = 5004;
ErrorDiagnostic = 2;
EMM_Cause = 15;

[Rule4]
ResultCode = 5004;
ErrorDiagnostic = "3,4";
EMM_Cause = 14;
```
