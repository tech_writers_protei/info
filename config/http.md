---
title: "http.cfg"
description: "Параметры HTTP"
weight: 20
type: docs
---

В файле задаются настройки HTTP-соединений.

**Примечание.** Наличие файла обязательно.

**Предупреждение.** При модификации файла новая конфигурация будет применена только после перезапуска приложения ММЕ.

### Используемые секции ###

* **[\[Server\]](#server-http)** -- параметры сервера;
  * **[\[SSL\]](#ssl-s)** -- параметры защищенного соединения;
  * **[\[Authorization\]](#auth-s)** -- параметры авторизации;
  * **[\[KeepAlive\]](#keep-alive-s)** -- параметры процедуры **keep-alive**;
* **[\[Client\]](#client-http)** -- параметры клиента;
  * **[\[SSL\]](#ssl-c)** -- параметры защищенного соединения;
  * **[\[Authorization\]](#auth-c)** -- параметры авторизации;
  * **[\[AdditionalDir\]](#add-dir)** -- параметры менеджера соединений;
  * **[\[KeepAlive\]](#keep-alive-c)** -- параметры процедуры **keep-alive**;

### Серверная сторона

| Параметр                                     | Описание                                                                                                                                                                                                                                                                                                                                                                                            | Тип    | O/M | P/R | Версия   |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| **<a name="server-http">\[Server\]</a>**     | Параметры серверной стороны.                                                                                                                                                                                                                                                                                                                                                                        | object | M   | R   |          |
| ID                                           | Идентификатор направления.                                                                                                                                                                                                                                                                                                                                                                          | int    | M   | R   |          |
| Address                                      | Прослушиваемый IP-адрес.<br>По умолчанию: 0.0.0.0.                                                                                                                                                                                                                                                                                                                                                  | string | O   | R   |          |
| Port                                         | Прослушиваемый порт.<br>По умолчанию: 80.                                                                                                                                                                                                                                                                                                                                                           | int    | O   | R   |          |
| MaxQueue                                     | Максимальная длина очереди запросов в одном соединении для [Persistent](#persistant)-режима.<br>По умолчанию: 10.                                                                                                                                                                                                                                                                                   | int    | O   | R   |          |
| <a name="maxbuf-s">MaxBufferSize</a>         | Максимальный размер буфера для приема сообщений, в битах.<br>**Примечание.** Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295<br>По умолчанию: -1.                                                                                                                                                                                                                                          | int    | O   | R   |          |
| RecvBufferSize                               | Минимальный размер буфера для приема сообщений, в битах.<br>**Примечание.** Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до [MaxBufferSize](#maxbuf-s).<br>В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS.<br>По умолчанию: 65&nbsp;536. | int    | O   | R   |          |
| ActivityTimer                                | Время ожидания активности соединения, в секундах.<br>**Примечание.** По истечении при отсутствии новых запросов соединение разрывается, объект **HTTP_ClientCL** уничтожается.<br>По умолчанию: 60.                                                                                                                                                                                                 | int    | O   | R   |          |
| StreamCount                                  | Количество потоков сервера при соединении по HTTP/2.<br>По умолчанию: 4&nbsp;294&nbsp;967&nbsp;295.                                                                                                                                                                                                                                                                                                 | int    | O   | R   | 1.3.0.17 |
| **<a name="ssl-s">\[SSL\]</a>**              | Параметры защищенного соединения.                                                                                                                                                                                                                                                                                                                                                                   | object | O   | R   |          |
| Version                                      | Версия SSL.<br>`1` -- TLSv1 / `2` -- SSLv2 / `3` -- SSLv3 / `4` -- SSLv23 / `5` -- TLSv1.1 / `6` -- TLSv1.2.<br>По умолчанию: 2.                                                                                                                                                                                                                                                                    | int    | O   | R   |          |
| CertificatePath                              | Путь к файлу сертификата с расширением \*.pem.                                                                                                                                                                                                                                                                                                                                                      | string | M   | R   |          |
| PrivateKeyPath                               | Путь к файлу, содержащему секретный ключ.                                                                                                                                                                                                                                                                                                                                                           | string | M   | R   |          |
| Ciphers                                      | Перечень поддерживаемых шифров. Формат:<br>`"<cipher>:<cipher>"`.<br>**Примечание.** Возможные значения см. [OpenSSL Cryptography and SSL/TLS Toolkit](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html).                                                                                                                                                                                    | string | O   | R   |          |
| PreferServerCiphers                          | Флаг приоритета шифров на стороне сервера вместо клиента.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                                       | bool   | O   | R   |          |
| **<a name="auth-s">\[Authorization\]</a>**   | Параметры авторизации.                                                                                                                                                                                                                                                                                                                                                                              | object | O   | R   |          |
| Type                                         | Тип авторизации.<br>`1` -- Basic / `2` -- Digest (пока не поддерживается).<br>По умолчанию: 1.                                                                                                                                                                                                                                                                                                      | int    | O   | R   |          |
| User                                         | Логин для авторизации.                                                                                                                                                                                                                                                                                                                                                                              | string | M   | R   |          |
| Password                                     | Пароль для авторизации.                                                                                                                                                                                                                                                                                                                                                                             | string | O   | R   |          |
| **<a name="keep-alive-s">\[KeepAlive\]</a>** | Параметры процедуры keep-alive.                                                                                                                                                                                                                                                                                                                                                                     | object | O   | R   |          |
| PingTimeout                                  | Время ожидания между процедурами.<br>По умолчанию: 10.                                                                                                                                                                                                                                                                                                                                              | int    | O   | R   |          |
| PingReqNumber                                | Количество Ping-запросов для разрыва соединения.<br>По умолчанию: 5.                                                                                                                                                                                                                                                                                                                                | int    | O   | R   |          |

### Клиентская сторона

| Параметр                                     | Описание                                                                                                                                                                                                                                                                                                                                                                                            | Тип                    | O/M | P/R | Версия   |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------|-----|-----|----------|
| **<a name="client-http">\[Client\]</a>**     | Параметры клиентской стороны.                                                                                                                                                                                                                                                                                                                                                                       | object                 | M   | R   |          |
| ID                                           | Идентификатор направления.<br>**Примечание.** Контроль за уникальностью идентификаторов возлагается на администратора системы.                                                                                                                                                                                                                                                                      | int                    | M   | R   |          |
| DestAddress                                  | IP-адрес сервера, которому должны быть предназначены запросы. Формат:<br>`"ip_address";port`<br>Начиная с v1.2.5.1, поддерживаются DNS-имена.                                                                                                                                                                                                                                                       | {string,int}<br>string | M   | R   |          |
| SrcAddress                                   | IP-адрес для отправки запроса, если сконфигурировано несколько сетевых интерфейсов.                                                                                                                                                                                                                                                                                                                 | string                 | O   | R   |          |
| <a name="persistant">Persistant</a>          | Флаг активации режима использования Persistant-соединений.<br>Если значение ненулевое, то после получения ответа соединение остается активным в течение [ActivityTimer](#activity_timer).<br>Разрешается отправка запросов конвейером: клиент отправляет запросы, не дожидаясь ответа на предыдущие.<br>По умолчанию: 0.                                                                            | bool                   | O   | R   |          |
| <a name="activity_timer">ActivityTimer</a>   | Время активности соединения при активации **Persistant**-режима, в секундах.<br>**Примечание.** По истечении, если от логики не поступило новых запросов, соединение разрывается, для всех запросов в очереди формируется оповещение логики об ошибке, затем объект **HTTP_ClientCL** уничтожается.<br>По умолчанию: 60.                                                                            | int                    | O   | R   |          |
| ResponseTimer                                | Время ожидания ответа от сервера, в секундах.<br>**Примечание.** По истечении, если не получен ответ, соединение разрывается, для всех запросов в очереди формируется оповещение логики об ошибке, затем объект **HTTP_ClientCL** уничтожается.<br>По умолчанию: 60.                                                                                                                                | int                    | O   | R   |          |
| MaxQueue                                     | Максимальное количество запросов в очереди в одном TCP-соединении.<br>**Примечание.** По достижении, интерфейс создает новый объект **HTTP_ClientCL** для нового TCP-соединения.<br>По умолчанию: 10.                                                                                                                                                                                               | int                    | O   | R   |          |
| MaxConnection                                | Максимальное количество одновременных соединений к серверу.<br>**Примечание.** По достижении, интерфейс отвечает HTTP_ERROR_IND с кодом ошибки `OVERLOADED`.<br>По умолчанию: 500.                                                                                                                                                                                                                  | int                    | O   | R   |          |
| <a name="maxbuf-c">MaxBufferSize</a>         | Максимальный размер буфера для приема сообщений, в битах.<br>**Примечание.** Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295.<br>По умолчанию: -1.                                                                                                                                                                                                                                         | int                    | O   | R   |          |
| RecvBufferSize                               | Минимальный размер буфера для приема сообщений, в битах.<br>**Примечание.** Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до [MaxBufferSize](#maxbuf-c).<br>В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS.<br>По умолчанию: 65&nbsp;536. | int                    | O   | R   |          |
| HTTP_Version                                 | Версия HTTP для создания соединения.<br>`1` – HTTP/1.0, HTTP/1.1 / `2` – HTTP/2.<br>**Примечание.** При использовании HTTP/2 необходимо активировать режим [Persistant](#persistant).<br>По умолчанию: 1.                                                                                                                                                                                           | int                    | O   | R   | 1.3.0.17 |
| StreamCount                                  | Количество потоков при соединении по HTTP/2.<br>По умолчанию: 4&nbsp;294&nbsp;967&nbsp;295.                                                                                                                                                                                                                                                                                                         | int                    | O   | R   | 1.3.0.17 |
| **<a name="ssl-c">\[SSL\]</a>**              | Параметры защищенного соединения.                                                                                                                                                                                                                                                                                                                                                                   | object                 | O   | R   |          |
| Version                                      | Версия SSL.<br>`1` -- TLSv1 / `2` -- SSLv2 / `3` -- SSLv3 / `4` -- SSLv23 / `5` -- TLSv1.1 / `6` -- TLSv1.2.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                    | int                    | O   | R   |          |
| SNI_Enabled                                  | Флаг использования <abbr title="Server Name Indication">SNI</abbr>.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                             | bool                   | O   | R   |          |
| ALPN_Enabled                                 | Флаг использования <abbr title="Application-Layer Protocol Negotiation">ALPN</abbr> для HTTP/2.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                 | bool                   | O   | R   | 1.3.0.17 |
| **<a name="auth-c">\[Authorization\]</a>**   | Параметры авторизации.                                                                                                                                                                                                                                                                                                                                                                              | object                 | O   | R   |          |
| User                                         | Логин для авторизации.                                                                                                                                                                                                                                                                                                                                                                              | string                 | M   | R   |          |
| Password                                     | Пароль для авторизации.                                                                                                                                                                                                                                                                                                                                                                             | string                 | O   | R   |          |
| **<a name="add-dir">\[AdditionalDir\]</a>**  | Параметры менеджера соединений.                                                                                                                                                                                                                                                                                                                                                                     | object                 | O   | R   | 1.3.0.20 |
| ID                                           | Список запасных направлений. Формат:<br>`{ <direction>; <direction> }`                                                                                                                                                                                                                                                                                                                              | \[int\]                | M   | R   |          |
| BreakDownTimeout                             | Время ожидания перезагрузки направлений, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                                                                                                                                                                                             | int                    | O   | R   |          |
| MaxErrorCount                                | Максимальное количество возможных ошибок.<br>По умолчанию: 1.                                                                                                                                                                                                                                                                                                                                       | int                    | O   | R   |          |
| Necromancy                                   | Флаг восстановления соединений.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                                                                 | bool                   | O   | R   |          |
| UseLoadSharing                               | Код типа распределения нагрузки между направлениями.<br>`0` -- не использовать;<br>`1` -- распределить между всеми;<br>`2` -- только между дополнительными при неактивном основном.<br>По умолчанию: 0.                                                                                                                                                                                             | int                    | O   | R   | 1.2.8.15 |
| **<a name="keep-alive-c">\[KeepAlive\]</a>** | Параметры для процедуры keep-alive.                                                                                                                                                                                                                                                                                                                                                                 | object                 | O   | R   |          |
| PingTimeout                                  | Время ожидания между процедурами.<br>По умолчанию: 10.                                                                                                                                                                                                                                                                                                                                              | int                    | O   | R   |          |
| PingReqNumber                                | Количество Ping-запросов для разрыва соединения.<br>По умолчанию: 5.                                                                                                                                                                                                                                                                                                                                | int                    | O   | R   |          |

#### Пример ####

```
[Server]
MaxBufferSize = 524626
ActivityTimer = 10
{
  ID = 0;
  Address = "192.168.228.2";
  Port = 80;
  SSL = {
    Version = 1;
    CertificatePath = "/home/usr/Projects/HTTP_Test/server/server-cert.pem";
    PrivateKeyPath = "/home/usr/Projects/HTTP_Test/server/server-key.pem";
    Ciphers = "HIGH:!MD5:!RC4:!aNULL:!eNULL";
    PreferServerCiphers = 1;
  }
}
{
  ID = 1;
  Address = "192.168.228.2";
  Port = 8080;
}

[Client]
{
  ID = 0;
  DestAddress = { "192.168.205.146"; 8080 };
  Persistant = 1;
  ActivityTimer = 120;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
  HTTP_Version = 1;
  AdditionalDir = {
    ID = { 1; 2 };
    MaxErrorCount = 3;
    BreakDownTimeout = 50000;
    Necromancy = 1;
    UseLoadSharing = 1;
  }
}
{
  ID = 1;
  DestAddress = { "192.168.1.118"; 8881 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
  SSL = { Version = 4; }
  ActivityTimer = 90;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
}
{
  ID = 2;
  DestAddress = { "192.168.1.118"; 8882 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
}
```