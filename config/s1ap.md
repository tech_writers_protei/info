---
title: "s1ap.cfg"
description: "Параметры компонента S1AP"
weight: 20
type: docs
---

В файле задаются настройки компонента S1AP.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload s1ap.cfg**, см. [Управление](../../oam/system_management/).

### Используемые секции ###

* **[\[LocalAddress\]](#local-address-s1ap)** -- параметры локального хоста;
* **[\[LocalInterfaces\]](#local-interfaces-s1ap)** -- параметры адресов для multihoming;
* **[\[Timers\]](#timers-s1ap)** -- параметры таймеров;
* **[\[Security\]](#security)** -- параметры безопасности обработки сообщений;
* **[\[Overload\]](#overload-s1ap)** -- параметры перегрузки;
* **[\[Balancer\]](#balancer)** -- параметры балансировщика;
* **[\[SCTP_AdditionalInfo\]](#sctp_additional_info)** -- дополнительные параметры SCTP.

### Описание параметров ###

| Параметр                                                       | Описание                                                                                                                                                                     | Тип            | O/M   | P/R   | Версия  |
|----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|-------|-------|---------|
| **<a name="local-address-s1ap">\[LocalAddress\]</a>**          | Параметры локального хоста.                                                                                                                                                  | object         | M     | P     |         |
| LocalHost                                                      | IP-адрес локального хоста.                                                                                                                                                   | string         | M     | P     |         |
| LocalPort                                                      | Прослушиваемый порт.<br>По умолчанию: 36412.                                                                                                                                 | int            | O     | P     |         |
| **<a name="local-interfaces-s1ap">\[LocalInterfaces\]</a>**    | Параметры адресов для multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>" }`                                                                                             | \[ip/ip:port\] | O     | P     |         |
| \<addresses\>                                                  | IP-адреса и порты для multihoming.                                                                                                                                           | ip/ip:port     | O     | P     |         |
| **<a name="timers-s1ap">\[Timers\]</a>**                       | Параметры таймеров.                                                                                                                                                          | object         | O     | P     |         |
| Reconnect_Timeout                                              | Время ожидания попытки переподключения после разрыва соединения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                              | int            | O     | P     |         |
| Response_Timeout                                               | Время ожидания ответного сообщения, в <br/>миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                      | int            | O     | R     |         |
| **<a name="security">\[Security\]</a>**                        | Параметры безопасности обработки сообщений.                                                                                                                                  | object         | O     | R     |         |
| NodeType                                                       | Код типа устройства.<br>`0` -- eNodeB / `1` -- MME.<br>По умолчанию: 1.                                                                                                      | int            | O     | R     |         |
| EncryptionAlg                                                  | Перечень [кодов](#codes) алгоритмов, используемых для шифрования/дешифрования сообщений, в порядке убывания приоритета. Формат:<br>`<algo>,<algo>`<br>По умолчанию: 3,2,1,0. | \[int\]        | O     | R     |         |
| IntegrityAlg                                                   | Перечень [кодов](#codes) алгоритмов, используемых для контроля целостности сообщений, в порядке убывания приоритета. Формат:<br>`<algo>,<algo>`<br>По умолчанию: 3,2,1,0.    | \[int\]        | O     | R     |         |
| **<a name="overload-s1ap">\[Overload\]</a>**                   | Параметры перегрузки.                                                                                                                                                        | object         | O     | R     |         |
| Enable                                                         | Флаг детектирования перегрузки.<br>По умолчанию: 0.                                                                                                                          | bool           | O     | R     |         |
| ~~Limit~~                                                      | ~~Максимальное количество сообщений Initial UE Message в секунду.~~ **Примечание.** Используется параметр [InitialUE](#initial-ue).<br>~~По умолчанию: 1&nbsp;000.~~         | ~~int~~        | ~~O~~ | ~~R~~ |         |
| <a name="initial-ue">InitialUE</a>                             | Максимальное количество сообщений Initial UE Message в секунду.<br>По умолчанию: 1&nbsp;000.                                                                                 | int            | O     | R     |         |
| AttachReq                                                      | Максимальное количество сообщений Attach Request в секунду.<br>По умолчанию: 1&nbsp;000.                                                                                     | int            | O     | R     |         |
| Paging                                                         | Максимальное количество сообщений Paging в секунду.<br>По умолчанию: 0, без ограничений.                                                                                     | int            | O     | R     | 1.2.0.4 |
| **<a name="balancer">\[Balancer\]</a>**                        | Параметры балансировщика.                                                                                                                                                    | object         | O     | P     | 1.1.0.0 |
| Enable                                                         | Флаг активации режима работы с балансировщиком.<br>По умолчанию: 0.                                                                                                          | bool           | O     | P     | 1.1.0.0 |
| **<a name="sctp_additional_info">\[SCTP_AdditionalInfo\]</a>** | [Дополнительные параметры SCTP](#sctp).                                                                                                                                      | object         | O     | P     |         |
| nodelay                                                        | Флаг активации SCTP nodelay.                                                                                                                                                 | bool           | 0     | O     | P       | 1.2.0.6 |

### Дополнительные параметры SCTP {#sctp}

{{< getcontent path="/Mobile/SharedDocs/SCTP_AdditionalInfo.md" >}}

#### Алгоритмы {#codes}

* 0 -- отсутствует: EEA0 для шифрования/дешифрования, EIA0 для проверки целостности;
* 1 -- SNOW3G, см. [3GPP Confidentiality and Integrity Algorithms UEA2 & UIA2. Document 2: SNOW 3G Specification](https://www.gsma.com/aboutus/wp-content/uploads/2014/12/snow3gspec.pdf);
* 2 -- AES-128, см. [FIPS 197; Advanced Encryption Standard (AES)](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197-upd1.pdf);
* 3 -- ZUC, см. [Specification of the 3GPP Confidentiality and Integrity Algorithms 128-EEA3 & 128-EIA3. Document 2: ZUC Specification](https://www.gsma.com/aboutus/wp-content/uploads/2014/12/eea3eia3zucv16.pdf).

#### Пример ####

```ini
[LocalAddress]
LocalHost = 192.168.100.1;
LocalPort = 29118;

[LocalInterfaces]
{ "192.168.100.10:29118"; "192.168.101.10:29118"; "192.168.102.10:29118" }

[Timers]
Reconnect_Timeout = 30000;
Response_Timeout = 30000;

[Security]
NodeType = 1;
EncryptionAlg = 3,2,1,0;
IntegrityAlg = 3,2,1,0;

[Overload]
Enable = 1;
InitialUE = 1000;

[Balancer]
Enable = 1;
```
