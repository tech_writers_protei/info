---
title: "served_plmn.cfg"
description: "Параметры обслуживаемых сетей"
weight: 20
type: docs
---

В файле задаются сети, обслуживаемые узлом ММЕ. Каждой сети соответствует одна секция **\[Served_PLMN\]**.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload served_plmn.cfg**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

#### Описание параметров сети PLMN ####

| Параметр                                                             | Описание                                                                                                                                                                                                                                                                                                                                             | Тип               | O/M | P/R | Версия   |
|----------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|-----|-----|----------|
| <a name="mcc">MCC</a>                                                | Мобильный код страны.                                                                                                                                                                                                                                                                                                                                | string            | M   | R   |          |
| <a name="mnc">MNC</a>                                                | Код мобильной сети.                                                                                                                                                                                                                                                                                                                                  | string            | M   | R   |          |
| <a name="mmegi">MMEGI</a>                                            | **Десятичное** значение `MME Group ID`.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                                                                                                                                                                    | int               | O   | R   |          |
| <a name="mmec">MMEC</a>                                              | **Десятичное** значение `MME Code`.<br>См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf).<br>По умолчанию: 0.                                                                                                                                                                        | int               | O   | R   |          |
| EquivalentPLMNs                                                      | Перечень эквивалентных PLMN. Формат:<br>`<mcc>-<mnc>,<mcc>-<mnc>`<br>См. [3GPP TS 29.292](https://www.etsi.org/deliver/etsi_ts/129200_129299/129292/17.00.00_60/ts_129292v170000p.pdf).                                                                                                                                                              | \[string\]        | O   | R   | 1.36.0.0 |
| <a name="relative-mme-capacity-served-plmn">RelativeMME_Capacity</a> | Относительная емкость узла MME, `Relative MME Capacity`.<br/><br/><br>См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf).<br>По умолчанию: не задано.<br>**Примечание.** Если не задано, то используется значение [mme.cfg::RelativeMME_Capacity](../mme/#relative-mme-capacity-mme). | int               | O   | R   |          |
| RelativeMME_CapacityForTAC                                           | Перечень связей [Relative MME Capacity](#relative-mme-capacity) и TAC. Формат:<br>`<tac>-<capacity>,<tac>-<capacity>`.                                                                                                                                                                                                                               | \[{int,int}\]     | O   | R   | 1.35.0.0 |
| Origin-Host                                                          | Значение `Origin-Host` для протокола Diameter и SGsAP.<br>По умолчанию: mmec[\<MMEC\>](#mmec).mmegi[\<MMEGI\>](#mmegi).<br>mme.epc.mnc[\<MNC\>](#mnc).mcc[\<MCC\>](#mcc).<br>3gppnetwork.org.<br>**Примечание.** Также `MME Name` в интерфейсе SGsAP.                                                                                                | string            | O   | R   |          |
| Origin-Realm                                                         | Значение `Origin-Realm` для протокола Diameter.<br>По умолчанию: epc.mnc[\<MNC\>](#mnc).<br>mcc[\<MCC\>](#mcc).3gppnetwork.org.                                                                                                                                                                                                                      | string            | O   | R   |          |
| UE_Usage_Types                                                       | Перечень `UE Usage Type`, связанных с текущим [MMEGI](#mmegi).<br>См. [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.08.00_60/ts_123401v170800p.pdf).                                                                                                                                                                 | \[int\]           | O   | R   | 1.35.0.0 |
| Foreign_UE_Usage_Types                                               | Перечень связей между `UE Usage Type` и другими `MMEGI`. Формат:<br>`<ue_usage_type>,<ue_usage_type>-<mmegi>`.                                                                                                                                                                                                                                       | \[{\[int\],int}\] | O   | R   |          |

#### Описание параметров EMM

| Параметр              | Описание                                                                                                                                                                           | Тип      | O/M | P/R | Версия |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| EMM_Info              | Флаг отправки сообщения EMM Information.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0. | bool     | O   | R   |        |
| NetName               | Краткое название сети.<br>По умолчанию: Protei.                                                                                                                                    | string   | O   | R   |        |
| NetFullname           | Полное название сети.<br>По умолчанию: Protei Network.                                                                                                                             | string   | O   | R   |        |
| [Timezone](#timezone) | Часовые пояса.<br>По умолчанию: UTC+3.                                                                                                                                             | timezone | O   | R   |        |

#### Описание параметров таймеров и счетчиков

| Параметр                          | Описание                                                                                                                                                                                                                                        | Тип    | O/M | P/R | Версия   |
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| T3324                             | Таймер T3324, в секундах.<br>См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>**Примечание.** Используется только при `PSM = 1`.<br>По умолчанию: значение T3324 абонента. | int    | O   | R   |          |
| T3402                             | Таймер T3402, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                                             | int    | O   | R   |          |
| T3402_Rej                         | Таймер T3402 для абонентов, не попавших в белый список IMSI, в секундах.<br>По умолчанию: 0.                                                                                                                                                    | int    | O   | R   |          |
| T3412                             | Таймер T3412, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 60.                                                                            | int    | O   | R   |          |
| <a name="t3412-ext">T3412_Ext</a> | Таймер T3412 Extended, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: не определено.                                                        | int    | O   | R   |          |
| T3412_Ext_Source                  | Приоритетный источник таймера [T3412 Extended](#t3412-ext).<br>`mme` / `ue`. По умолчанию: ue.                                                                                                                                                  | string | O   | R   |          |
| <a name="t3413">T3413</a>         | Таймер T3413, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 10.                                                                            | int    | O   | R   |          |
| T3413_SGs                         | Таймер T3413 для процедуры S1AP Paging, вызванного процедурой SGsAP Paging, в секундах.<br>По умолчанию: значение [T3413](#t3413).                                                                                                              | int    | O   | R   |          |
| T3422                             | Таймер T3422, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                             | int    | O   | R   | 1.36.0.0 |
| <a name="t3450">T3450</a>         | Таймер T3450, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                             | int    | O   | R   | 1.36.0.0 |
| T3450_RepeatCount                 | Количество повторных отправок по таймеру [T3450](#t3450).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                    | int    | O   | R   | 1.36.0.0 |
| <a name="t3460">T3460</a>         | Таймер T3460, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                             | int    | O   | R   | 1.36.0.0 |
| T3460_RepeatCount                 | Количество повторных отправок по таймеру [T3460](#t3460).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                    | int    | O   | R   | 1.36.0.0 |
| <a name="t3470">T3470</a>         | Таймер T3470, в секундах.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 6.                                                                             | int    | O   | R   | 1.36.0.0 |
| T3470_RepeatCount                 | Количество повторных отправок по таймеру [T3470](#t3470).<br>Диапазон: 0-5. По умолчанию: 4.                                                                                                                                                    | int    | O   | R   | 1.36.0.0 |
| PurgeDelay                        | Время задержки отправки сообщения Diameter: Purge-Request на узел HSS, в секундах.<br>**Примечание.** По умолчанию сообщение не отправляется, а данные не удаляются.<br>По умолчанию: не определено.                                            | int    | O   | R   |          |
| PagingRepeatCount                 | Количество отправок запросов Paging на каждую станцию eNodeB в рамках текущего шага используемой модели пейджинга.<br>Диапазон: 1-5.<br>По умолчанию: 4.                                                                                        | int    | O   | R   |          |
| UE_ActivityTimeout                | Время ожидания активности от устройства UE, в секундах.<br>По умолчанию: 600.                                                                                                                                                                   | int    | O   | R   |          |

#### Описание параметров флагов

| Параметр                        | Описание                                                                                                                                                                                                                                                                                                | Тип  | O/M | P/R | Версия    |
|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------|-----|-----|-----------|
| AllowDefaultAPN                 | Флаг разрешения использования APN по умолчанию.<br>По умолчанию: 1.                                                                                                                                                                                                                                     | bool | O   | R   |           |
| AllowOperatorQCI                | Флаг разрешения использования <abbr title="QoS Class Identifier">QCI</abbr> из диапазона 128-254.<br>По умолчанию: 0.                                                                                                                                                                                   | bool | O   | R   |           |
| AllowEmptyMSISDN                | Флаг разрешения регистрации абонентов без MSISDN.<br>По умолчанию: 0.                                                                                                                                                                                                                                   | bool | O   | R   |           |
| AllowExtDRX                     | Флаг разрешения использования Extended DRX.<br>См. [3GPP TS 25.300](https://www.etsi.org/deliver/etsi_ts/125300_125399/125300/17.00.00_60/ts_125300v170000p.pdf).<br>По умолчанию: 0.                                                                                                                   | bool | O   | R   |           |
| ResetOnSetup                    | Флаг отправки сообщения S1AP: RESET после запроса S1AP: S1 SETUP REQUEST.<br>По умолчанию: 0.                                                                                                                                                                                                           | bool | O   | R   |           |
| IndirectFwd                     | Флаг разрешения создания Indirect Data Forwarding Tunnel при хэндовере.<br>По умолчанию: 0.                                                                                                                                                                                                             | bool | O   | R   | 1.19.0.0  |
| PagingModel                     | Индикатор используемой модели для процедуры Paging.<br>`0` -- по умолчанию;<br>`1` -- начало с последней базовой станции.<br>По умолчанию: 0.                                                                                                                                                           | int  | O   | R   | 1.40.0.0  |
| SRVCC                           | Флаг поддержки <abbr title="Single Radio Voice Call Continuity">`SRVCC`</abbr>.<br>См. [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf).<br>По умолчанию: 0.                                                                               | bool | O   | R   |           |
| UseVLR                          | Флаг использования VLR.<br>По умолчанию: 0.<br>**Примечание.** По умолчанию SGs не используется. При запросе NAS ATTACH REQUEST от UE и значении `EPS attach type = combined EPS/IMSI attach, 2` узел MME в ответе NAS ATTACH ACCEPT задает значение `EPS attach result = combined EPS/IMSI attach, 2`. | bool | O   | R   |           |
| <a name="psm">PSM</a>           | Флаг разрешения режима Power Saving Mode.<br>См. [3GPP TS 23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/12.02.00_60/ts_123682v120200p.pdf).<br>По умолчанию: 0.                                                                                                                     | bool | O   | R   | 1.22.1.0  |
| MO_CSFB_Ind                     | Флаг отправки сообщения SGsAP-MO-CSFB-Indication при получении запроса Extended Service Request.<br>По умолчанию: 0.                                                                                                                                                                                    | bool | O   | R   |           |
| CP_CIoT_opt                     | Флаг разрешения оптимизации Control Plane CIoT EPS.<br>См. [3GPP TS 36.300](https://www.etsi.org/deliver/etsi_ts/136300_136399/136300/14.10.00_60/ts_136300v141000p.pdf).<br>По умолчанию: 0.                                                                                                           | bool | O   | R   | 1.15.19.0 |
| <a name="ims-vops">IMS_VoPS</a> | Значение `IMS Voice over PS Session Indicator`.<br>См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>По умолчанию: 0.                                                                                                               | bool | O   | R   |           |
| S11UliAlwaysSent                | Флаг отправки User Location Information в запросе GTP: Modify Bearer Request.<br>См. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf).<br>По умолчанию: 0.                                                                                 | bool | O   | R   | 1.40.0.0  |
| EmergencyAttach_wo_auth         | Флаг разрешения процедуры Emergency Attach без аутентификации.<br>По умолчанию: 0.                                                                                                                                                                                                                      | bool | O   | R   |           |
| E-UTRAN                         | Флаг поддержки сети E-UTRAN.<br>По умолчанию: 1.                                                                                                                                                                                                                                                        | bool | O   | R   |           |
| P_CSCF_Restoration              | Флаг поддержки процедуры P-CSCF Restoration.<br>По умолчанию: 1.                                                                                                                                                                                                                                        | bool | O   | R   | 1.44.2.0  |
| Authentication                  | Флаг включения аутентификации.<br>По умолчанию: 1.                                                                                                                                                                                                                                                      | bool | O   | R   |           |
| AuthVectorsNum                  | Количество запрашиваемых векторов аутентификации.<br>Диапазон: 1-7. По умолчанию: 1.                                                                                                                                                                                                                    | int  | O   | R   | 1.41.0.0  |
| NotifyOnPGW_Alloc               | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении динамического IP-адреса узла PGW для APN.<br>По умолчанию: 1.                                                                                                                                                     | bool | O   | R   |           |
| NotifyOnUE_SRVCC                | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении флага поддержки SRVCC на устройстве UE.<br>По умолчанию: 1.                                                                                                                                                       | bool | O   | R   |           |
| ForceIdentityReq                | Флаг отправки Identity Request вне зависимости от наличия сохраненного ранее IMSI.<br>По умолчанию: 0.                                                                                                                                                                                                  | bool | O   | R   |           |
| RequestIMEISV                   | Флаг требования IMEISV от абонента.<br>По умолчанию: 1.                                                                                                                                                                                                                                                 | bool | O   | R   | 1.15.18.0 |
| RejectForeignGUTI               | Флаг запрета перехода на MME абонентов, которые были зарегистрированы в других PLMN на сторонних MME при работе в условиях RAN Sharing.<br>По умолчанию: 1.<br>                                                                                                                                         | bool | O   | R   | 1.49.0.0  |
| RejectOnLU_Fail                 | Флаг отправки сообщения SGsAP-LOCATION-UPDATE-REJECT в случае неуспеха процедуры SGsAP-Location-Update.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 0.                                                       | bool | O   | R   |           |
| UseDefaultAPNonFailure          | Флаг использования APN из профиля по умолчанию при отсутствии известных адресов SGW/PGW для APN, запрошенного абонентом.<br>По умолчанию: 0.                                                                                                                                                            | bool | O   | R   | 1.49.0.0  |
| DefaultLACasTAC                 | Флаг использования LAC, равного текущему TAC, в случае отсутствия соответствия TAC к LAI в перечне ниже.<br>По умолчанию: 0.                                                                                                                                                                            | bool | O   | R   | 1.49.0.0  |

#### Описание параметров IMEI

| Параметр         | Описание                                                                                               | Тип     | O/M | P/R | Версия   |
|------------------|--------------------------------------------------------------------------------------------------------|---------|-----|-----|----------|
| CheckIMEI        | Флаг включения проверки IMEI на узле EIR.<br>По умолчанию: 0.                                          | bool    | O   | R   |          |
| AllowGreyIMEI    | Флаг разрешения подключения телефона с неизвестным IMEI.<br>По умолчанию: 1.                           | bool    | O   | R   |          |
| AllowUnknownIMEI | Флаг обслуживания устройств, неизвестных для EIR.<br>По умолчанию: 0.                                  | bool    | O   | R   | 1.40.0.0 |
| EIR-Host         | Значение `Destination-Host` для узла EIR.                                                              | string  | O   | R   |          |
| EIR-Realm        | Значение `Destination-Realm` для узла EIR.                                                             | string  | O   | R   |          |

#### Описание параметров правил

| Параметр                                               | Описание                                                                                                                                                                                                         | Тип             | O/M | P/R | Версия   |
|--------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-----|-----|----------|
| <a name="apn-rules">APN_rules</a>                      | Перечень [правил APN](../apn_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                                                                                                         | \[string\]      | O   | R   |          |
| <a name="tac_rules">TAC_rules</a>                      | Перечень [правил TAC](../tac_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                                                                                                         | \[string\]      | O   | R   |          |
| <a name="rac-rules">RAC_rules</a>                      | Перечень [правил RAC](../rac_rules/).<br>**Примечание.** Применяются только для домашних абонентов PLMN.                                                                                                         | \[string\]      | O   | R   |          |
| <a name="pgw_served_plmn">PGW_IP</a>                   | IP-адрес и FQDN узла PGW по умолчанию. Формат:<br>`"(<fqdn>)<ip>"`<br>**Примечание.** FQDN может отсутствовать.                                                                                                  | {string,ip}     | O   | R   |          |
| <a name="sgw_served_plmn">SGW_IP</a>                   | IP-адрес и FQDN узла SGW по умолчанию. Формат:<br>`"(<fqdn>)<ip>"`<br>**Примечание.** FQDN может отсутствовать.                                                                                                  | {string,ip}     | O   | R   |          |
| <a name="zc-rules">ZC_rules</a>                        | Перечень [правил ZoneCodes](../zc_rules/).                                                                                                                                                                       | \[string\]      | O   | R   | 1.36.0.0 |
| <a name="qos-rules">QoS_rules</a>                      | Перечень [правил QoS](../qos_rules/).                                                                                                                                                                            | \[string\]      | O   | R   | 1.37.0.0 |
| <a name="code-mapping-rules">CodeMapping_rules</a>     | Перечень [правил Diameter: Result-Code и EMM Cause](../code_mapping_rules/).                                                                                                                                     | \[string\]      | O   | R   | 1.37.2.0 |
| <a name="forbidden-imei-rules">ForbiddenIMEI_rules</a> | Перечень [правил запрещённых IMEI](../forbidden_imei_rules/).                                                                                                                                                    | \[string\]      | O   | R   |          |
| MME_IP                                                 | IP-адреса других узлов MME с соответствующими значениями `MMEGI` и `MMEC`. Формат:<br>`<mmegi>-<mmec>-<ip>,<mmec>-<ip>`<br>**Примечание.** Если MMEGI не задан, то используется значение, связанное с этой PLMN. | [{int,int,ip}\] | O   | R   |          |
| SGSN_IP                                                | IP-адреса узлов SGSN с соответствующими контроллерами RNC. Формат:<br>`<rnc>-<ip>,<rnc>-<ip>`.                                                                                                                   | \[{string,ip}\] | O   | R   |          |
| <a name="emergency-numbers">EmergencyNumbers</a>       | Перечень [типов и правил номеров экстренных служб](../emergency_numbers/).                                                                                                                                       | \[string\]      | O   | R   |          |
| <a name="emergency-pdn">EmergencyPDN</a>               | Перечень имен `PDN Connectivity`, применяемых в случае Emergency Attach.<br>**Примечание.** Именованные описания PDN Connectivity хранятся в файле [emergency_pdn.cfg](../emergency_pdn/).                       | \[string\]      | O   | R   |          |
| <a name="imsi-wl">IMSI_whitelist</a>                   | Перечень [правил TAC и IMSI](../imsi_rules/).<br>Если TAC не указан, то набор масок применяется для всей PLMN.                                                                                                   | \[string\]      | O   | R   |          |

#### Описание остальных параметров

| Параметр      | Описание                                                                                                                                                                                                                  | Тип        | O/M | P/R | Версия   |
|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|----------|
| VoPS_APN_NI   | Перечень значений `APN-NI`, которые могут быть использованы для VoPS.<br>По умолчанию: ims.                                                                                                                               | \[string\] | O   | R   | 1.40.0.0 |
| NRI_length    | Количество используемых бит идентификатора <abbr title="Network Resource Identifier">NRI</abbr> узла SGSN.<br>**Примечание.** Если не задано, то не используется в рамках соответствующей сети PLMN.                      | int        | O   | R   | 1.44.1.0 |
| LDN           | <abbr title="Local Distinguished Name">LDN</abbr>, отправляемый при активированном SRVCC.<br>По умолчанию: Protei_MME.                                                                                                    | string     | O   | R   |          |
| [eDRX](#edrx) | Значение <abbr title="Extended Discontinuous Reception">`eDRX`</abbr>. См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>По умолчанию: не определено. | int        | O   | R   | 1.41.0.0 |
| [PTW](#ptw)   | Значение `Paging Time Window` для Extended DRX. См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).<br>По умолчанию: не определено.                        | int        | O   | R   | 1.41.0.0 |

#### Описание параметров соответствий TAC-LAC

| Параметр          | Описание                                                                                                                                                                           | Тип               | O/M | P/R | Версия   |
|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------|-----|-----|----------|
| ForbiddenLAC      | Перечень кодов LAC, из которых переход абонентов запрещен.                                                                                                                         | \[int\]           | O   | R   | 1.33.0.0 |
| \<TAC\> = \<LAI\> | Перечень соответствий TAC к LAI. Формат:<br>`<tac> = [<plmn>-]<lac>;`<br>**Примечание.** Один TAC может быть связан только с одним LAI, один LAI может быть связан со многими TAC. | \[{int,int,int}\] | O   | R   |          |
| TAC               | Код зоны отслеживания.                                                                                                                                                             | int               | O   | R   |          |
| \<plmn\>          | Код сети PLMN.<br>По умолчанию: значение [\<MCC\>](#mcc)[\<MNC\>](#mnc).                                                                                                           | int               | O   | R   |          |
| \<lac\>           | Код локальной области LAC.                                                                                                                                                         | int               | M   | R   |          |

#### Пример ####

```ini
[Served_PLMN]
{
  MCC = 999;
  MNC = 99;
  MMEGI = 1;
  MMEC = 1;
  UTRAN = 0;
  T3413 = 1;
  T3412 = 1800;
  T3402 = 10;
  T3402_Rej = 10;
  T3450 = 1;
  T3460 = 1;
  T3470 = 1;
  CheckIMEI = 0;
  EmergencyNumbers = "911";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  UseVLR = 0;
  IMS_VoPS = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 0;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6;
  RelativeMME_Capacity = 255;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_999";
  NetName = "test99";
  Timezone = "UTC+3";
  eDRX = 3;
  PTW = 2;
}
{
  MCC = 001;
  MNC = 01;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  MME_IP = 1-2-192.168.125.182;
  T3324 = 60;
  T3413 = 1;
  T3412 = 120;
  T3412_Ext = 14400;
  T3412_Ext_Source = "mme";
  T3402 = 5;
  T3402_Rej = 10;
  T3450 = 5;
  T3450_RepeatCount = 4;
  T3460 = 6;
  T3460_RepeatCount = 3;
  T3470 = 1;
  UE_ActivityTimeout = 600;
  CheckIMEI = 0;
  RequestIMEISV = 1;
  AuthVectorsNum = 2
  EmergencyNumbers = "112,911";
  EmergencyPDN = "EMRG";
  Origin-Host = "mme1.epc.mnc001.mcc001.3gppnetwork.org";
  Origin-Realm = "epc.mnc001.mcc001.3gppnetwork.org";
  EIR-Host = "eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  VoPS_APN_NI = ims;
  AllowGreyIMEI = 0;
  UseVLR = 0;
  RejectOnLU_Fail = 0;
  ResetOnSetup = 0;
  PurgeDelay = 0;
  PagingModel = 1;
  IMSI_whitelist = WList1,WList5;
  APN_rules = FullRule,Rule_1,Rule_2,Rule_3;
  TAC_rules = Rule_1,Rule_2,Rule_3,Rule_4,Rule_5,Rule_6,Rule_7;
  CodeMapping_rules = Rule1, Rule2;
  RelativeMME_Capacity = 100;
  ForceIdentityReq = 1;
  EMM_Info = 1;
  NetFullname = "Protei_Network_001";
  NetName = "protei";
  Timezone = "TAC1_UTC+2";
  CP_CIoT_opt = 1;
  PSM = 1;
  1 = 1;
  7 = 1;
  9 = 25048-5;
  10 = 25048-7;
  UE_Usage_Types = 0,1;
  Foreign_UE_Usage_Types = 2-2;
  QoS_rules = Rules;
  AllowDefaultAPN = 1;
  eDRX = 3;
}
{
  MMEGI = 3;
  MMEC = 4;
}
{
  MCC = 208;
  MNC = 93;
  UTRAN = 0;
  MMEGI = 1;
  MMEC = 1;
  T3413 = 5;
  T3412_Ext = 600;
  RequestIMEISV = 0;
  CheckIMEI = 0;
  AllowEmptyMSISDN = 1;
  EIR-Host ="eir.epc.mnc01.mcc250.3gppnetwork.org";
  IMS_VoPS = 1;
  PSM = 1;
  AllowExtDRX = 1;
  AllowGreyIMEI = 0;
  ResetOnSetup = 1;
  UseDefaultAPNonFailure = 1;
  DefaultLACasTAC = 1;
  APN_rules = FullRule,Rule_1;
  TAC_rules = Rule_4;
  SGW_IP = "192.168.125.95";
  2 = 3;
  1 = 1;
  5 = 25048-4;
  CP_CIoT_opt = 1;
  PTW = 3;
}
```

### Значения eDRX {#edrx}

Значение eDRX задается 4 младшими битами октета 3.
В конфигурации используется десятичное (Dec) значение, полученное из конвертации двоичного (Bin).

Пример: для режима A/Gb необходимо использовать значение 195,84 сек. Согласно
[3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf),
разделу 10.5.5.32, или таблицам ниже, младшие биты должны быть 0111<sub>2</sub>, что соответствует 7<sub>10</sub>.

Итого: `eDRX = 7;`

#### Режим A/Gb

| Bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 1,88 сек                |
| 0001 | 1   | 3,76 сек                |
| 0010 | 2   | 7,53 сек                |
| 0011 | 3   | 12,24 сек               |
| 0100 | 4   | 24,48 сек               |
| 0101 | 5   | 48,96 сек               |
| 0110 | 6   | 97,92 сек               |
| 0111 | 7   | 195,84 сек              |
| 1000 | 8   | 391,68 сек              |
| 1001 | 9   | 783,36 сек              |
| 1010 | 10  | 1566,72 сек             |
| 1011 | 11  | 3133,44 сек             |

#### Режим Iu

| Bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 10,24 сек               |
| 0001 | 1   | 20,48 сек               |
| 0010 | 2   | 40,96 сек               |
| 0011 | 3   | 81,92 сек               |
| 0100 | 4   | 163,84 сек              |
| 0101 | 5   | 327,68 сек              |
| 0110 | 6   | 655,36 сек              |
| 0111 | 7   | 1310,72 сек             |
| 1000 | 8   | 1966,08 сек             |
| 1001 | 9   | 2621,44 сек             |

#### Режим S1

| Bin  | Dec | Длительность цикла eDRX |
|------|-----|-------------------------|
| 0000 | 0   | 5,12 сек                |
| 0001 | 1   | 10,24 сек               |
| 0010 | 2   | 20,48 сек               |
| 0011 | 3   | 40,96 сек               |
| 0100 | 4   | 61,44 сек               |
| 0101 | 5   | 81,92 сек               |
| 0110 | 6   | 102,4 сек               |
| 0111 | 7   | 122,88 сек              |
| 1000 | 8   | 143,36 сек              |
| 1001 | 9   | 163,84 сек              |
| 1010 | 10  | 327,68 сек              |
| 1011 | 11  | 655,36 сек              |
| 1100 | 12  | 1&nbsp;310,72 сек       |
| 1101 | 13  | 2&nbsp;621,44 сек       |
| 1110 | 14  | 5&nbsp;242,88 сек       |
| 1111 | 15  | 10&nbsp;485,76 сек      |

### Значения PTW {#ptw}

Значение PTW задается 4 старшими битами октета 3.
В конфигурации используется десятичное (Dec) значение, полученное из конвертации двоичного (Bin).

Пример: для режима NB-S1 необходимо использовать значение 15,36 сек. Согласно
[3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf),
разделу 10.5.5.32, или таблицам ниже, старшие биты должны быть 0101<sub>2</sub>, что соответствует 5<sub>10</sub>.

Итого: `PTW = 5;`

#### Режим Iu

| Bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 0 сек            |
| 0001 | 1   | 1 сек            |
| 0010 | 2   | 2 сек            |
| 0011 | 3   | 3 сек            |
| 0100 | 4   | 4 сек            |
| 0101 | 5   | 5 сек            |
| 0110 | 6   | 6 сек            |
| 0111 | 7   | 7 сек            |
| 1000 | 8   | 8 сек            |
| 1001 | 9   | 9 сек            |
| 1010 | 10  | 10 сек           |
| 1011 | 11  | 12 сек           |
| 1100 | 12  | 14 сек           |
| 1101 | 13  | 16 сек           |
| 1110 | 14  | 18 сек           |
| 1111 | 15  | 20 сек           |

#### Режим WB-S1

| Bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 1,28 сек         |
| 0001 | 1   | 2,56 сек         |
| 0010 | 2   | 3,84 сек         |
| 0011 | 3   | 5,12 сек         |
| 0100 | 4   | 6,4 сек          |
| 0101 | 5   | 7,68 сек         |
| 0110 | 6   | 8,96 сек         |
| 0111 | 7   | 10,24 сек        |
| 1000 | 8   | 11,52 сек        |
| 1001 | 9   | 12,8 сек         |
| 1010 | 10  | 14,08 сек        |
| 1011 | 11  | 15,36 сек        |
| 1100 | 12  | 16,64 сек        |
| 1101 | 13  | 17,92 сек        |
| 1110 | 14  | 19,20 сек        |
| 1111 | 15  | 20,48 сек        |

#### Режим NB-S1

| Bin  | Dec | Длительность PTW |
|------|-----|------------------|
| 0000 | 0   | 2,56 сек         |
| 0001 | 1   | 5,12 сек         |
| 0010 | 2   | 7,68 сек         |
| 0011 | 3   | 10,24 сек        |
| 0100 | 4   | 12,8 сек         |
| 0101 | 5   | 15,36 сек        |
| 0110 | 6   | 17,92 сек        |
| 0111 | 7   | 20,48 сек        |
| 1000 | 8   | 23,04 сек        |
| 1001 | 9   | 25,6 сек         |
| 1010 | 10  | 28,16 сек        |
| 1011 | 11  | 30,72 сек        |
| 1100 | 12  | 33,28 сек        |
| 1101 | 13  | 35,84 сек        |
| 1110 | 14  | 38,4 сек         |
| 1111 | 15  | 40,96 сек        |

### Формат Timezone {#timezone}

Формат записи часового пояса: `[TACX_]UTC<sign>HH:MM[_DST+Y]`

* Необязательная часть `TAC<X>_` -- задает ассоциацию между часовым поясом и TAC, при отсутствии ассоциируется со всеми TAC в данной PLMN:
  * X -- код TAC;
* Обязательная часть `UTC<sign><HH>:<MM>` -- задает часовой пояс через отклонение от UTC:
  * sign -- знак отклонения от UTC, `+` или `-`;
  * HH -- количество часов;
  * MM -- количество минут;
* Необязательная часть `_DST+<Y>` -- задает дополнительное отклонение при переводе часов на летнее время:
  * Y -- количество добавленных часов, диапазон: 0-2;

### Адрес PGW ####

Адрес PGW может быть получен из ряда источников.
Приоритеты источников в порядке убывания:

1. [apn_rules.cfg::PGW_IP](../apn_rules/#pgw_apn);
2. [dns.cfg](../dns/);
3. [PGW_IP](#pgw_served_plmn).

### Адрес SGW ####

Адрес SGW может быть получен из ряда источников.
В зависимости от процедуры SGW выбирается либо по APN, либо по TAC.

* В случае выбора по APN используется только [apn_rules.cfg::SGW_IP](../apn_rules/#sgw_apn);
* В случае выбора по TAC приоритеты источников в порядке убывания:

1. [tac_rules.cfg::SGW_IP](../tac_rules/#sgw_tac);
2. [dns.cfg](../dns/);
3. [SGW_IP](#sgw_served_plmn).