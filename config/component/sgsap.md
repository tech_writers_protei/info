---
title: "sgsap.cfg"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

**Примечание.** Наличие файла обязательно.

**Предупреждение.** При модификации файла новая конфигурация будет применена только после перезапуска приложения ММЕ.

### Описание параметров Sg.SGsAP {#sgsap}

| Параметр                 | Описание                                   | Тип        | O/M | P/R | Версия |
|--------------------------|--------------------------------------------|------------|-----|-----|--------|
| ComponentAddr            | Адрес компонента.                          | string     | M   | R   |        |
| ComponentType            | Тип компонента.<br>По умолчанию: Sg.SGsAP. | string     | O   | R   |        |
| Params                   | Параметры компонента.                      | object     | M   | R   |        |
| **{**                    |                                            |            |     |     |        |
| [PeerTable](#peer-table) | Таблица пиров.                             | \[object\] | O   | R   |        |
| DefaultPCSM              | Имя узла PCSM по умолчанию.                | \[str\]    | O   | R   |        |
| **}**                    |                                            |            |     |     |        |

#### Параметры элемента таблицы PeerTable {#peer-table}

* Для пира с одним IP-адресом:

| Параметр | Описание                                       | Тип    | O/M | P/R | Версия |
|----------|------------------------------------------------|--------|-----|-----|--------|
| PeerIP   | IP-адрес пира.                                 | ip     | M   | R   |        |
| GT       | Глобальный заголовок пира.                     | string | M   | R   |        |
| PCSM     | Компонентный адрес соответствующего узла PCSM. | string | M   | R   |        |
| PeerName | Имя пира.                                      | string | O   | R   |        |

* Для пира с несколькими IP-адресами:

| Параметр  | Описание                                       | Тип        | O/M | P/R | Версия  |
|-----------|------------------------------------------------|------------|-----|-----|---------|
| GT        | Глобальный заголовок пира.                     | string     | M   | R   | 1.1.0.0 |
| PeerName  | Имя пира.                                      | string     | O   | R   | 1.1.0.0 |
| PCSM_list | Набор параметров отдельных PCSM.               | \[object\] | M   | R   | 1.1.0.0 |
| **{**     |                                                |            |     |     |         |
| PeerIP    | IP-адрес пира.                                 | ip         | M   | R   | 1.1.0.0 |
| PCSM      | Компонентный адрес соответствующего узла PCSM. | string     | M   | R   | 1.1.0.0 |
| Priority  | Приоритет.<br>По умолчанию: 0 (высший).        | int        | O   | R   | 1.1.0.0 |
| Weight    | Вес.<br>По умолчанию: 1.                       | int        | O   | R   | 1.1.0.0 |
| **}**     |                                                |            |     |     |         |

### Описание параметров Sg.SGsAP.PCSM ###

| Параметр                         | Описание                                                                                                            | Тип         | O/M | P/R | Версия   |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|-------------|-----|-----|----------|
| ComponentAddr                    | Адрес компонента.                                                                                                   | string      | M   | R   |          |
| ComponentType                    | Тип компонента.<br>По умолчанию: Sg.SGsAP.PCSM.                                                                     | string      | O   | R   |          |
| Params                           | Параметры компонента.                                                                                               | object      | M   | R   |          |
| **{**                            |                                                                                                                     |             |     |     |          |
| <a name="peer-ip">PeerIP</a>     | IP-адрес подключения узла PCSM.                                                                                     | ip          | M   | R   |          |
| <a name="peer-port">PeerPort</a> | Порт подключения узла PCSM.                                                                                         | string      | M   | R   |          |
| <a name="src-ip">SrcIP</a>       | Локальный IP-адрес узла PCSM.<br>По умолчанию: [sgsap::\[LocalAddress\]::LocalHost](../../sgsap/#local-host-sgsap). | string      | O   | R   |          |
| <a name="src-port">SrcPort</a>   | Локальный порт узла PCSM.<br>По умолчанию: [sgsap::\[LocalAddress\]::LocalPort](../../sgsap/#local-port-sgsap).     | string      | O   | R   |          |
| RemoteInterfaces                 | Удаленные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }`                                    | \[ip:port\] | O   | R   |          |
| LocalInterfaces                  | Локальные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }`                                    | \[ip:port\] | O   | R   | 1.37.2.0 |
| SCTP_AdditionalInfo              | [Дополнительные параметры SCTP](#sctp-component-sgsap) для клиентской компоненты PCSM.                              |             | O   | R   |          |
| **}**                            |                                                                                                                     |             |     |     |          |

#### Дополнительные параметры SCTP {#sctp-component-sgsap}

{{< getcontent path="/Mobile/SharedDocs/SCTP_AdditionalInfo.md" >}}

#### Конфигурация local и remote адресов {#localandremoteaddress-sgsap}

Для серверных компонент IP-адрес, порт и `local_interfaces` используются соответствующие значения из файла [sgsap.cfg](../../sgsap/):
* [sgsap.cfg::\[LocalAddress\]::LocalHost](../../sgsap/#local-host-sgsap)
* [sgsap.cfg::\[LocalAddress\]::LocalPort](../../sgsap/#local-port-sgsap)
* [sgsap.cfg::\[LocalAddress\]::local_interfaces](../../sgsap/#local-interfaces-sgsap)

Параметры из этого файла для них игнорируются.

Компонента является клиентом, если:

* указан **PeerIP** для tcp;
* указан **PeerIP** или `remote_interfaces` для sctp.

Адреса клиентских компонент составляются следующим образом для TCP в порядке приоритетности:

* Для удаленных адресов, **RemoteAddr**:

  * ip - PeerIP;
  * port - PeerPort;

* Для локальных адресов, **LocalAddr**:

  * ip - SrcIP, [sgsap.cfg::\[LocalAddress\]::LocalHost](../../sgsap/#local-host-sgsap);
  * port - SrcPort;

Для клиентских SCTP-ассоциаций `remote_interfaces` и `local_interfaces` являются набором 1 основной + дополнительные
адреса.

Основной адрес для SCTP определяется следующим образом в порядке приоритетности:

* Для удаленных адресов, **RemoteAddr**:

  * ip - PeerIP, первый адрес в списке `remote_interfaces`;
  * port - PeerPort, первый порт в `remote_interfaces`;

* Для локальных адресов, **LocalAddr**:

  * ip - SrcIP, первый адрес в списке `local_interfaces`, [sgsap.cfg::\[LocalAddress\]::LocalHost](../../sgsap/#local-host-sgsap);
  * port - SrcPort, первый порт в `local_interfaces`;

#### Обязательность параметров клиентских компонент

| Параметр          | Клиент TCP      | Клиент SCTP                                               |
|-------------------|-----------------|-----------------------------------------------------------|
| PeerIP            | Обязательный    | Обязательный, если не указан `ip` в `remote_interfaces`   |
| PeerPort          | Обязательный    | Обязательный, если не указан `port` в `remote_interfaces` |
| SrcIP             | Опциональный    | Опциональный                                              |
| SrcPort           | Опциональный    | Опциональный                                              |
| local_interfaces  | Не используется | Опциональный                                              |
| remote_interfaces | Не используется | Обязательный, если не указан `PeerIP`/`PeerPort`          |

#### Пример ####

```
{
  ComponentAddr = Sg.SGsAP;
  ComponentType = Sg.SGsAP;
  Params = {
    PeerTable = {
      {
        PeerIP = "192.168.125.154";
        GT = "79216567568";
        PCSM = "Sg.SGsAP.PCSM.0";
      };
      {
        GT = "79216561234";
        PCSM_list = {
          {
            PeerIP = "192.168.126.155";
            PCSM = "Sg.SGsAP.PCSM.1";
            Weight = 1;
            Priority = 1;
          };
          {
            PeerIP = "192.168.126.156";
            PCSM = "Sg.SGsAP.PCSM.2";
            Weight = 2;
            Priority = 1;
          };
        };
        PeerName = "MultiIP_Peer";
      };
    };
    DefaultPCSM = {
     "Sg.SGsAP.PCSM.0";
    };
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.0;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.125.154";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
    dscp = 46;
    critical_tx_buf_size = 1;
    on_critical_tx_buf_size = 1;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.1;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.155";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}

{
  ComponentAddr = Sg.SGsAP.PCSM.2;
  ComponentType = Sg.SGsAP.PCSM;
  Params = {
    PeerIP = "192.168.126.156";
    PeerPort = 29118;
    SrcIP = "192.168.126.67";
    SrcPort = 29119;
  };
}
```