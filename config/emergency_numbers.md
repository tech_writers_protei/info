---
title: "emergency_numbers.cfg"
description: "Параметры номеров экстренных служб"
weight: 20
type: docs
---

В файле задаются связи между номерами экстренных служб и их типами. Имя секции может использоваться в качестве ссылки
в параметре [served_plmn.cfg::EmergencyNumbers](../served_plmn/#emergency-numbers).

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload served_plmn.cfg**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр       | Описание                                                                    | Тип    | O/M | P/R | Версия |
|----------------|-----------------------------------------------------------------------------|--------|-----|-----|--------|
| Number         | Номер.                                                                      | string | M   | R   |        |
| Police         | Флаг использования номера для полиции.<br>По умолчанию: 0.                  | bool   | O   | R   |        |
| Ambulance      | Флаг использования номера для cкорой помощи.<br>По умолчанию: 0.            | bool   | O   | R   |        |
| FireBrigade    | Флаг использования номера для пожарной охраны.<br>По умолчанию: 0.          | bool   | O   | R   |        |
| MarineGuard    | Флаг использования номера для береговой охраны.<br>По умолчанию: 0.         | bool   | O   | R   |        |
| MountainRescue | Флаг использования номера для горноспасательной службы.<br>По умолчанию: 0. | bool   | O   | R   |        |

#### Пример ####

```ini
[112]
Number = "112";
Police = 1;
Ambulance = 1;
FireBrigade = 1;
MarineGuard = 1;
MountainRescue = 1;

[911]
Number = "911";
Police = 1;
Ambulance = 1;
FireBrigade = 1;
MarineGuard = 1;
MountainRescue = 1;

[AmbulanceOnly]
Number = "03";
Ambulance = 1;
```
