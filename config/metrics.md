---
title: "metrics.cfg"
description: "Параметры сбора метрик"
weight: 20
type: docs
---

В файле задаются настройки сбора метрик.

**Предупреждение.** При модификации файла новая конфигурация будет применена только после перезапуска приложения ММЕ.

### Используемые секции ###

* **[\[General\]](#general-metrics)** -- общие параметры сбора метрик;
* **[\[s1Interface\]](#s1Interface)** -- параметры сбора метрик S1;
* **[\[s1Attach\]](#s1Attach)** -- параметры сбора метрик S1, связанных с регистрацией абонента;
* **[\[s1Detach\]](#s1Detach)** -- параметры сбора метрик S1, связанных с дерегистрацией абонента;
* **[\[s1BearerActivation\]](#s1BearerActivation)** -- параметры сбора метрик S1, связанных с активацией bearer-служб;
* **[\[s1BearerDeactivation\]](#s1BearerDeactivation)** -- параметры сбора метрик S1, связанных с деактивацией bearer-служб;
* **[\[s1BearerModification\]](#s1BearerModification)** -- параметры сбора метрик S1, связанных с модификацией bearer-служб;
* **[\[s1Security\]](#s1Security)** -- параметры сбора метрик S1, связанных с аутентификацией абонента;
* **[\[s1Service\]](#s1Service)** -- параметры сбора метрик S1, связанных с процедурой Service Request;
* **[\[handover\]](#handover)** -- параметры сбора метрик, связанных с процедурой Handover;
* **[\[tau\]](#tau)** -- параметры сбора метрик, связанных с процедурой Tracking Area Update;
* **[\[sgsInterface\]](#sgsInterface)** -- параметры сбора метрик SGs;
* **[\[svInterface\]](#svInterface)** -- параметры сбора метрик Sv (SRVCC HO);
* **[\[resource\]](#resource)** -- параметры сбора метрик, связанных с ресурсами хоста;
* **[\[users\]](#users)** -- параметры сбора различных счётчиков;
* **[\[s11Interface\]](#s11Interface)** -- параметры сбора метрик S11;
* **[\[paging\]](#paging)** -- параметры сбора метрик, связанных с процедурой Paging;
* **[\[Diameter\]](#diameter)** -- параметры сбора метрик, связанных с протоколом Diameter;
* **[\[S6a-interface\]](#s6a-interface)** -- параметры сбора метрик, связанных с интерфейсом S6a;

### Описание параметров ###

| Параметр                                                        | Описание                                                                                                   | Тип    | O/M | P/R | Версия   |
|-----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------|--------|-----|-----|----------|
| **<a name="general-metrics">\[General\]</a>**                   | Общие параметры сбора метрик.                                                                              |        | O   | P   | 1.45.0.0 |
| Enable                                                          | Флаг сбора метрик.<br>По умолчанию: 0.                                                                     | bool   | O   | P   | 1.45.0.0 |
| Directory                                                       | Директория для хранения метрик.<br>По умолчанию: ../metrics.                                               | string | O   | P   | 1.45.0.0 |
| <a name="granularity">Granularity</a>                           | Гранулярность сбора метрик, в минутах.<br>По умолчанию: 5.                                                 | int    | O   | P   | 1.45.0.0 |
| <a name="node-name">NodeName</a>                                | Имя узла, указываемое в названии файла с метриками.                                                        | string | O   | P   | 1.45.0.0 |
| HhMmSeparator                                                   | Флаг включения разделителя между часами и минутами в формате `HH:MM`.<br>По умолчанию: 0.                  | bool   | O   | P   | 1.49.0.0 |
| **<a name="s1Interface">\[s1Interface\]</a>**                   | Параметры для метрик S1.                                                                                   |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="s1Attach">\[s1Attach\]</a>**                         | Параметры для метрик S1, связанных с регистрацией абонента.                                                |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="s1Detach">\[s1Detach\]</a>**                         | Параметры для метрик S1, связанных с дерегистрацией абонента.                                              |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="s1BearerActivation">\[s1BearerActivation\]</a>**     | Параметры для метрик S1, связанных с активацией bearer-службы.                                             |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="s1BearerDeactivation">\[s1BearerDeactivation\]</a>** | Параметры для метрик S1, связанных с деактивацией bearer-службы.                                           |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="s1BearerModification">\[s1BearerModification\]</a>** | Параметры для метрик S1, связанных с модификацией bearer-службы.                                           |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="s1Security">\[s1Security\]</a>**                     | Параметры для метрик S1, связанных с аутентификацией абонента.                                             |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="s1Service">\[s1Service\]</a>**                       | Параметры для метрик S1, связанных с процедурой Service Request.                                           |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="handover">\[handover\]</a>**                         | Параметры для метрик, связанных с процедурой Handover.                                                     |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="tau">\[tau\]</a>**                                   | Параметры для метрик, связанных с процедурой Tracking Area Update.                                         |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="sgsInterface">\[sgsInterface\]</a>**                 | Параметры для метрик SGs.                                                                                  |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="svInterface">\[svInterface\]</a>**                   | Параметры для метрик Sv (SRVCC HO).                                                                        |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="resource">\[resource\]</a>**                         | Параметры для метрик, связанных с ресурсами хоста.                                                         |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="users">\[users\]</a>**                               | Параметры сбора различных счётчиков.                                                                       |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="s11Interface">\[s11Interface\]</a>**                 | Параметры для метрик S11.                                                                                  |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="paging">\[paging\]</a>**                             | Параметры для метрик, связанных с процедурой Paging.                                                       |        | O   | P   | 1.46.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.46.0.0 |
| **<a name="diameter">\[Diameter\]</a>**                         | Параметры для метрик Diameter.                                                                             |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |
| **<a name="s6a-interface">\[S6a-interface\]</a>**               | Параметры для метрик S6a.                                                                                  |        | O   | P   | 1.45.0.0 |
| Granularity                                                     | Гранулярность сбора метрик, в минутах.<br>По умолчанию: значение [\[General\]::Granularity](#granularity). | int    | O   | P   | 1.45.0.0 |

#### Пример ####

```ini
[General]
Enable = 1;
Granularity = 30;
Directory = "/usr/protei/Protei_MME/metrics";
NodeName = MME-1;

[Diameter]
Enable = 1;
Granularity = 15;

[S6a-interface]
Enable = 1;
Granularity = 1;
```