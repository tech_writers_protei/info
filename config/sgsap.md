---
title: "sgsap.cfg"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
---

В файле задаются настройки компонента SGsAP.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload sgsap.cfg**, см. [Управление](../../oam/system_management/).

### Используемые секции ###

* **[\[LocalAddress\]](#local-address-sgsap)** -- параметры локального хоста;
* **[\[LocalInterfaces\]](#local-interfaces-sgsap)** -- параметры адресов для multihoming;
* **[\[SCTP_AdditionalInfo\]](#sctp-config-sgsap)** -- дополнительные параметры SCTP;
* **[\[Timers\]](#timers-sgsap)** -- параметры таймеров;
* **[\[RetryCounters\]](#retry-counters)** -- параметры счетчиков повторных попыток;
* **[\[LAC_GT\]](#lac-gt)** -- параметры соответствий LAC и GT;
* **[\[MSC_Pool\]](#msc-pool)** -- параметры соответствий LAC и наборов GT с параметрами;
* **[\[Overload\]](#overload-sgsap)** -- параметры перегрузки.

### Описание параметров ###

| Параметр                                                     | Описание                                                                                                                                                                                                    | Тип                          | O/M | P/R | Версия |
|--------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|-----|-----|--------|
| **<a name="local-address-sgsap">\[LocalAddress\]</a>**       | Параметры локального хоста.                                                                                                                                                                                 | object                       | M   | P   |        |
| <a name="local-host-sgsap">LocalHost</a>                     | IP-адрес локального хоста.                                                                                                                                                                                  | string                       | M   | P   |        |
| <a name="local-port-sgsap">LocalPort</a>                     | Прослушиваемый порт.<br>По умолчанию: 29118.                                                                                                                                                                | int                          | O   | P   |        |
| **<a name="local-interfaces-sgsap">\[LocalInterfaces\]</a>** | Параметры адресов для multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>" }`.                                                                                                                           | \[ip/ip:port\]               | O   | P   |        |
| **<a name="sctp-config-sgsap">\[SCTP_AdditionalInfo\]</a>**  | [Дополнительные параметры SCTP](#sctp_additional) для серверной компоненты PCSM.                                                                                                                            | object                       | O   | P   |        |
| **<a name="timers-sgsap">\[Timers\]</a>**                    | Параметры таймеров.                                                                                                                                                                                         | object                       | O   | P   |        |
| Reconnect_Timeout                                            | Время ожидания попытки переподключения после разрыва соединения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                             | int                          | O   | P   |        |
| Response_Timeout                                             | Время ожидания ответного сообщения, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                          | int                          | O   | R   |        |
| Ts8                                                          | Таймер Ts8, в миллисекундах.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 4&nbsp;000.                             | int                          | O   | R   |        |
| Ts9                                                          | Таймер Ts9, в миллисекундах.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 4&nbsp;000.                             | int                          | O   | R   |        |
| **<a name="retry-counters">\[RetryCounters\]</a>**           | Параметры счетчиков повторных попыток.                                                                                                                                                                      | object                       | O   | R   |        |
| Ns8                                                          | Таймер Ns8.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 2.                                                       | int                          | O   | R   |        |
| Ns9                                                          | Таймер Ns9.<br>См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf).<br>По умолчанию: 2.                                                       | int                          | O   | R   |        |
| **<a name="lac-gt">\[LAC_GT\]</a>**                          | Параметры соответствий LAC и GT. Формат:<br>`[<plmn>-]<lac> = <gt>`.<br>Для LAC может быть указан PLMN.<br>**Примечание.** Первым задается GT по умолчанию для не упомянутых LAC в формате:<br>`DefaultGT`. | \[{int,int,string}\]         | O   | R   |        |
| \<plmn\>                                                     | Код сети PLMN.                                                                                                                                                                                              | int                          | O   | R   |        |
| \<lac\>                                                      | Код локальной области LAC.                                                                                                                                                                                  | int                          | M   | R   |        |
| \<gt\>                                                       | Глобальный заголовок.                                                                                                                                                                                       | string                       | O   | R   |        |
| **<a name="msc-pool">\[MSC_Pool\]</a>**                      | Параметры соответствий LAC и наборов GT с весами и приоритетами. Формат:<br>`[<plmn>-]<lac> = <gt>-<weight>-<priority>,<gt>-<weight>-<priority>`.<br>Для LAC может быть указан PLMN.                        | \[{int,int,string,int,int}\] | O   | R   |        |
| \<plmn\>                                                     | Код сети PLMN.                                                                                                                                                                                              | int                          | O   | R   |        |
| \<lac\>                                                      | Код локальной области LAC.                                                                                                                                                                                  | int                          | M   | R   |        |
| \<gt\>                                                       | Глобальный заголовок.                                                                                                                                                                                       | string                       | M   | R   |        |
| \<weight\>                                                   | Вес глобального заголовка.                                                                                                                                                                                  | int                          | O   | R   |        |
| \<priority\>                                                 | Приоритет глобального заголовка.                                                                                                                                                                            | int                          | O   | R   |        |
| **<a name="overload-sgsap">\[Overload\]</a>**                | Параметры перегрузки.                                                                                                                                                                                       | object                       | O   | R   |        |
| Enable                                                       | Флаг детектирования перегрузки. По умолчанию: 0.                                                                                                                                                            | bool                         | O   | R   |        |
| Limit                                                        | Максимальное количество сообщений SGsAP в секунду. По умолчанию: 1000.                                                                                                                                      | int                          | O   | R   |        |

#### Дополнительные параметры SCTP {#sctp_additional}

{{< getcontent path="/Mobile/SharedDocs/SCTP_AdditionalInfo.md" >}}

#### Пример ####

```ini
[LocalAddress]
LocalHost = 192.168.100.1;
LocalPort = 29118;

[LocalInterfaces]
{ "192.168.100.10:29118"; "192.168.101.10:29118"; "192.168.102.10:29118" }

[Timers]
Reconnect_Timeout = 30000;
Response_Timeout = 30000;
Ts8 = 4000;
Ts9 = 4000;

[RetryCounters]
Ns8 = 2;
Ns9 = 2;

[LAC_GT]
79211230000
5 = 79231234567
25048-7 = 79111234567

[MSC_Pool]
5 = 79231234567-1-1,79237654321-10-1
25048-7 = 79111234567-1-1,79117654321-10-1

[SCTP_AdditionalInfo]
critical_tx_buf_size = 1;
on_critical_tx_buf_size = 1;

[Overload]
Enable = 1;
Limit = 1000;
```
