if __name__ == '__main__':
    _dict = {
        "5G_EA": 61440,
        "5G_IA": 61440,
        "A-MSISDN": None,
        "ARD": None,
        "AWoPDN": False,
        "ActivityDetected": None,
        "CP_CIoT_opt": False,
        "CellID": 56633311,
        "DRX_param": "SPLIT PG CYCLE 16, non-DRX timer 4, SPLIT on CCCH 0, DRX cycle param 0",
        "ECM_State": "IDLE",
        "EEA": 240,
        "EIA": 240,
        "EMM_State": "REGISTERED",
        "ENB_IP": "192.168.46.244",
        "ENB_UE_ID": None,
        "EUTRAN_Trace_ID": None,
        "E_DRX_param": None,
        "EmergencyAttach": False,
        "GEA": 56,
        "GTP_C_TEID_SELF": 61463,
        "GUTI": "00101;8;1;1704",
        "GUTI_LastReallocTimestamp": "2024-09-13_15:23:17",
        "GUTI_ReallocTrigger_count": 6247,
        "HSS_Host": "hss.epc.mnc048.mcc250.3gppnetwork.org",
        "HSS_reset": False,
        "IMEISV": "3529235448289201",
        "IMSI": "001010123456789",
        "MME_UE_ID": None,
        "MSISDN": "0600000000",
        "MS_Classmark2": "2;1;1;7;1;1;1;0;0;0;1;0;0;0;1;1;0",
        "MS_Classmark3": "Tm_S1AP_NAS_MobileStationClassmark3: P-GSM 0, E-GSM or R-GSM 1, GSM 1800 1\nA5/4 - 1, A5/5 - 0, A5/6 - 0, A5/7 - 0\nUCS2 treatment - 0\nExtended Measurement Cap - 0\nGSM 850 Associated Radio cap - 4\nGSM 1900 Associated radio cap - 1\nUMTS FDD Radio Access Technology Cap - 1\nUMTS 3.84 Mcps TDD Radio Access Technology Cap - 0\nCDMA 2000 Radio Access Technology Cap - 0\nUMTS 1.28 Mcps TDD Radio Access Technology cap - 0\nGERAN Feature Package 1 - 1\nHigh Multislot Capability - 0\nGERAN Feature Package 2 - 0\nGMSK Multislot Power Profile 0\n8-PSK Multislot Power Profile 0\nDL Advanced ReceiverPerformance - phase I supported\nDTM Enhancements Cap - 0\nRepeated ACCH Cap - 1\nCiphering Mode Setting Cap - 0\nAdditional Positioning Caps - 0\nE-UTRA FDD support - 1\nE-UTRA TDD support - 1\nE-UTRA Measurement and Reporting support - 0\nPriority-based reselection support - 1\nUTRA CSG Cells Reporting - 0\nVAMOS not supported\nTIGHTER not supported\nSelective Ciphering of DL SACCH - 0",
        "NCC": None,
        "NEAF": None,
        "NH": None,
        "ODB": None,
        "PDN_Conn_restricted": False,
        "PDN_Manager": {
            "Barebone #1": {
                "Bearer": {
                    "ARP": "A&R priority level 10, shall not trigger preemption, preemptable",
                    "QCI": 6,
                    "id": "0",
                    "type": "default"
                },
                "Connectivity": {
                    "APN": "internet.mnc001.mcc001.gprs",
                    "APN AMBR": {
                        "DL": "100000000 b/sec",
                        "UL": "100000000 b/sec"
                    },
                    "Charging Characteristics": "32769",
                    "HSS Context": "1",
                    "PAA": "no IPv4, no IPv6",
                    "PDN Type": "ipv4",
                    "S11 UL F-TEID": "0:0.0.0.0-::",
                    "SGW FQDN": ""
                }
            },
            "Barebone #2": {
                "Bearer": {
                    "ARP": "A&R priority level 1, shall not trigger preemption, preemptable",
                    "QCI": 5,
                    "id": "0",
                    "type": "default"
                },
                "Connectivity": {
                    "APN": "ims.mnc001.mcc001.gprs",
                    "APN AMBR": {
                        "DL": "100000000 b/sec",
                        "UL": "100000000 b/sec"
                    },
                    "HSS Context": "2",
                    "PAA": "no IPv4, no IPv6",
                    "PDN Type": "ipv4",
                    "S11 UL F-TEID": "0:0.0.0.0-::",
                    "SGW FQDN": ""
                }
            },
            "Barebone #3": {
                "Bearer": {
                    "ARP": "A&R priority level 10, shall not trigger preemption, preemptable",
                    "QCI": 6,
                    "id": "0",
                    "type": "default"
                },
                "Connectivity": {
                    "APN": "internet-gy.mnc001.mcc001.gprs",
                    "APN AMBR": {
                        "DL": "100000000 b/sec",
                        "UL": "100000000 b/sec"
                    },
                    "HSS Context": "3",
                    "PAA": "no IPv4, no IPv6",
                    "PDN Type": "ipv4",
                    "S11 UL F-TEID": "0:0.0.0.0-::",
                    "SGW FQDN": ""
                }
            },
            "Barebone #4": {
                "Bearer": {
                    "ARP": "A&R priority level 10, shall not trigger preemption, preemptable",
                    "QCI": 9,
                    "id": "0",
                    "type": "default"
                },
                "Connectivity": {
                    "APN": "tester.mnc001.mcc001.gprs",
                    "APN AMBR": {
                        "DL": "100000000 b/sec",
                        "UL": "100000000 b/sec"
                    },
                    "HSS Context": "4, default",
                    "PAA": "no IPv4, no IPv6",
                    "PDN Type": "ipv4v6",
                    "S11 UL F-TEID": "0:0.0.0.0-::",
                    "SGW FQDN": ""
                }
            },
            "Connectivity #1": {
                "APN": "internet.mnc001.mcc001.gprs",
                "APN AMBR": {
                    "DL": "100000000 b/sec",
                    "UL": "100000000 b/sec"
                },
                "Bearer #1": {
                    "ARP": "A&R priority level 10, shall not trigger preemption, preemptable",
                    "QCI": 6,
                    "S1 UL F-TEID": "16091:83.149.63.3-::",
                    "S5 UL F-TEID": "3:83.149.63.10-::",
                    "id": "5",
                    "type": "default"
                },
                "Charging Characteristics": "32769",
                "HSS Context": "1",
                "PAA": "198.51.101.2, no IPv6",
                "PDN Type": "ipv4",
                "PGW FQDN": "topon.pgw1.s5s8.node1.epc.mnc001.mcc001.3gppnetwork.org",
                "S11 UL F-TEID": "16091:10.10.214.10-::",
                "S5/S8 UL F-TEID": "3:83.149.63.10-::",
                "SGW FQDN": "topon.sgw1.s11.node1.epc.mnc001.mcc001.3gppnetwork.org"
            }
        },
        "PLMN": "00101",
        "PPF": True,
        "RFSP_id": None,
        "ReachabilityTimeout": 360,
        "TAC": 6911,
        "TCE_IP": None,
        "UEA": 192,
        "UE_UsageType": None,
        "UE_radio_cap": "1681 bytes",
        "UE_radio_cap_for_paging": None,
        "UIA": 64,
        "dl_NAS_count": 373,
        "dl_UE_AMBR": 100000000,
        "knasenc_type": 0,
        "knasint_type": 2,
        "ul_NAS_count": 2516,
        "ul_UE_AMBR": 100000000
    }

    for k in _dict.get("PDN_Manager").get("Barebone #1"):
        print(f"| {k} | | | |")
