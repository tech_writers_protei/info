# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, Namespace, SUPPRESS
from base64 import b64decode, b64encode
from pathlib import Path
from re import Pattern, compile, findall
from string import hexdigits

_xml_pattern: Pattern = compile(r"image=data:image/svg\+xml,(.+?)[\\\";]+")
_attributes: tuple[str, ...] = ("fill", "stroke")
STOP: str = "Press any key to close the window ...\n"
NAME: str = Path(__file__).name
epilog: str = """examples:

  py %(prod)s Scratchpad.xml --rgb 70,14,117
  py %(prod)s Scratchpad.xml --cmyk 12,60,0,17
  py %(prod)s Scratchpad.xml --hex BA55D3
  py %(prod)s Scratchpad.xml --html mediumorchid

"""

colors: dict[str, str] = {
    "indianred": "CD5C5C",
    "lightcoral": "F08080",
    "salmon": "FA8072",
    "darksalmon": "E9967A",
    "lightsalmon": "FFA07A",
    "crimson": "DC143C",
    "red": "FF0000",
    "firebrick": "B22222",
    "darkred": "8B0000",
    "pink": "FFC0CB",
    "lightpink": "FFB6C1",
    "hotpink": "FF69B4",
    "deeppink": "FF1493",
    "mediumvioletred": "C71585",
    "palevioletred": "DB7093",
    "coral": "FF7F50",
    "tomato": "FF6347",
    "orangered": "ff4500",
    "darkorange": "ff8c00",
    "orange": "FFA500",
    "gold": "FFD700",
    "yellow": "FFFF00",
    "lightyellow": "ffffe0",
    "lemonchiffon": "FFFACD",
    "lightgoldenrodyellow": "FAFAD2",
    "papayawhip": "FFEFD5",
    "moccasin": "FFE4B5",
    "peachpuff": "FFDAB9",
    "palegoldenrod": "EEE8AA",
    "khaki": "F0E68C",
    "darkkhaki": "BDB76B",
    "lavender": "E6E6FA",
    "thistle": "D8BFD8",
    "plum": "DDA0DD",
    "violet": "EE82EE",
    "orchid": "DA70D6",
    "fuchsia": "FF00FF",
    "magenta": "FF00FF",
    "mediumorchid": "BA55D3",
    "mediumpurple": "9370DB",
    "amethyst": "9966CC",
    "blueviolet": "8A2BE2",
    "darkviolet": "9400D3",
    "darkorchid": "9932CC",
    "darkmagenta": "8B008B",
    "purple": "800080",
    "indigo": "4B0082",
    "slateblue": "6A5ACD",
    "darkslateblue": "483D8B",
    "mediumslateblue": "7B68EE",
    "greenyellow": "ADFF2F",
    "chartreuse": "7FFF00",
    "lawngreen": "7CFC00",
    "lime": "00FF00",
    "limegreen": "32CD32",
    "palegreen": "98FB98",
    "lightgreen": "90EE90",
    "mediumspringgreen": "00FA9A",
    "springgreen": "00FF7F",
    "mediumseagreen": "3CB371",
    "seagreen": "2E8B57",
    "forestgreen": "228B22",
    "green": "008000",
    "darkgreen": "006400",
    "yellowgreen": "9ACD32",
    "olivedrab": "6B8E23",
    "olive": "808000",
    "darkolivegreen": "556B2F",
    "mediumaquamarine": "66CDAA",
    "darkseagreen": "8FBC8F",
    "lightseagreen": "20B2AA",
    "darkcyan": "008B8B",
    "teal": "008080",
    "aqua": "00FFFF",
    "cyan": "00FFFF",
    "lightcyan": "E0FFFF",
    "paleturquoise": "AFEEEE",
    "aquamarine": "7FFFD4",
    "turquoise": "40E0D0",
    "mediumturquoise": "48D1CC",
    "darkturquoise": "00CED1",
    "cadetblue": "5F9EA0",
    "steelblue": "4682B4",
    "lightsteelblue": "B0C4DE",
    "powderblue": "B0E0E6",
    "lightblue": "ADD8E6",
    "skyblue": "87CEEB",
    "lightskyblue": "87CEFA",
    "deepskyblue": "00BFFF",
    "dodgerblue": "1E90FF",
    "cornflowerblue": "6495ED",
    "royalblue": "4169E1",
    "blue": "0000FF",
    "mediumblue": "0000CD",
    "darkblue": "00008B",
    "navy": "000080",
    "midnightblue": "191970",
    "cornsilk": "FFF8DC",
    "blanchedalmond": "FFEBCD",
    "bisque": "FFE4C4",
    "navajowhite": "FFDEAD",
    "wheat": "F5DEB3",
    "burlywood": "DEB887",
    "tan": "D2B48C",
    "rosybrown": "BC8F8F",
    "sandybrown": "F4A460",
    "goldenrod": "DAA520",
    "darkgoldenrod": "B8860B",
    "peru": "CD853F",
    "chocolate": "D2691E",
    "saddlebrown": "8B4513",
    "sienna": "A0522D",
    "brown": "A52A2A",
    "maroon": "800000",
    "white": "FFFFFF",
    "snow": "FFFAFA",
    "honeydew": "F0FFF0",
    "mintcream": "F5FFFA",
    "azure": "F0FFFF",
    "aliceblue": "F0F8FF",
    "ghostwhite": "F8F8FF",
    "whitesmoke": "F5F5F5",
    "seashell": "FFF5EE",
    "beige": "F5F5DC",
    "oldlace": "FDF5E6",
    "floralwhite": "FFFAF0",
    "ivory": "FFFFF0",
    "antiquewhite": "FAEBD7",
    "linen": "FAF0E6",
    "lavenderblush": "FFF0F5",
    "mistyrose": "FFE4E1",
    "gainsboro": "DCDCDC",
    "lightgrey": "D3D3D3",
    "silver": "C0C0C0",
    "darkgray": "A9A9A9",
    "gray": "808080",
    "dimgray": "696969",
    "lightslategray": "778899",
    "slategray": "708090",
    "darkslategray": "2F4F4F",
    "black": "000000"
}


def format_hex(value: str | float):
    return (hex(int(value)))[2:].zfill(2)


def is_hex(value: str):
    return value if all(_ in set(hexdigits) for _ in value) else None


def rgb_to_hex(r: str, g: str, b: str):
    if any(not _.isnumeric() for _ in (r, g, b)):
        print(f"RGB must be numerics")
        return

    if any(int(_) > 255 or int(_) < 0 for _ in (r, g, b)):
        print(f"RGB must be in range from 0 to 255")
        return

    return f"{format_hex(r)}{format_hex(g)}{format_hex(b)}"


def cmyk_to_hex(c: str, m: str, y: str, k: str):
    if any(not _.isnumeric() for _ in (c, m, y, k)):
        print(f"CMYK must be numerics")
        return

    if any(float(_) < 0 or float(_) > 100 for _ in (c, m, y, k)):
        print(f"CMYK must be in range from 0 to 100")
        return

    r: float = 255 * (1 - float(c) / 100) * (1 - float(k) / 100)
    g: float = 255 * (1 - float(m) / 100) * (1 - float(k) / 100)
    b: float = 255 * (1 - float(y) / 100) * (1 - float(k) / 100)

    return f"{format_hex(r)}{format_hex(g)}{format_hex(b)}"


def html_to_hex(html: str):
    if html not in colors:
        print(f"{html} is an unknown HTML color name")
        return
    else:
        return colors.get(html)


def parse():
    arg_parser: ArgumentParser = ArgumentParser(
        prog="fix_xml_image",
        usage=f"py {NAME} [ -h/--help | -v/--version | --names ] <FILE> ( --rgb | --cmyk | --hex | --html ) <COLOR>",
        description="Валидация YAML-файла для PDF",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        action="store",
        type=str,
        help="Path to file",
        dest="file"
    )

    group = arg_parser.add_mutually_exclusive_group(required=True)

    group.add_argument(
        "--rgb",
        action="store",
        default=SUPPRESS,
        help="Color in RGB format: '<0-255>,<0-255>,<0-255>'",
        dest="rgb"
    )

    group.add_argument(
        "--cmyk",
        action="store",
        default=SUPPRESS,
        help="Color in CMYK format: '<0-100>,<0-100>,<0-100>,<0-100>'",
        dest="cmyk"
    )

    group.add_argument(
        "--hex",
        action="store",
        default=SUPPRESS,
        help="Color in HEX format: '<0-9A-F>{,6}'",
        dest="hex"
    )

    group.add_argument(
        "--html",
        action="store",
        default=SUPPRESS,
        help="HTML color name: '<string>'",
        dest="html"
    )

    group.add_argument(
        "--names",
        action="store",
        default=SUPPRESS,
        help="Print all HTML color names",
        dest="names"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Show the script version and stop"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Show the help information and stop"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(STOP)
            return
        elif hasattr(args, "names"):
            _names: list[str] = [f"{k} = #{v}" for k, v in colors.items()]
            print("\n".join(_names))
            input()
            return

    except ArgumentError as exc:
        print(f"{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        input(STOP)
        exit(1)

    except KeyboardInterrupt:
        input("Script work has been interrupted ...")
        exit(2)

    else:
        print(args.__dict__)
        return args


def define_parameter(namespace: Namespace):
    if (_ := getattr(namespace, "rgb", None)) is not None:
        return _, "rgb"
    elif (_ := getattr(namespace, "hex", None)) is not None:
        return _, "hex"
    elif (_ := getattr(namespace, "cmyk", None)) is not None:
        return _, "cmyk"
    elif (_ := getattr(namespace, "html", None)) is not None:
        return _, "html"
    else:
        return None, ""


def convert_value(color: str | None, value_type: str) -> str | None:
    if value_type == "rgb":
        r, g, b = color.split(",")
        print(f"{rgb_to_hex(r, g, b)=}")
        return rgb_to_hex(r, g, b)
    elif value_type == "cmyk":
        c, m, y, k = color.split(",")
        return cmyk_to_hex(c, m, y, k)
    elif value_type == "html":
        return html_to_hex(color)
    elif value_type == "hex":
        return is_hex(color)
    else:
        return


def change_color(file: str | Path, color: str):
    with open(file, "rb") as fb:
        _content: str = fb.read().decode()

    for attribute in _attributes:
        _attribute_pattern: Pattern = compile(r"%s:#.{3,6}" % attribute)

        for part in findall(_xml_pattern, _content):
            _decoded: str = b64decode(part).decode()
            _string = _attribute_pattern.sub(f"{attribute}:#{color}", _decoded)
            _encoded: str = b64encode(_string.encode()).decode()
            _content = _content.replace(part, _encoded)

    _path: Path = Path(file)
    with open(_path.with_stem(f"{_path.stem}_{color}"), "wb") as fb:
        fb.write(_content.encode())

    print(f"File {file} updated")


def main():
    ns: Namespace = parse()
    user_input, tag = define_parameter(ns)
    print(f"{user_input=}")
    print(f"{tag=}")
    color: str | None = convert_value(user_input, tag)
    print(f"{color=}")
    change_color(getattr(ns, "file"), color)


if __name__ == '__main__':
    main()
