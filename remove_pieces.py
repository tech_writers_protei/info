# -*- coding: utf-8 -*-
from pathlib import Path
from re import compile, Pattern, sub

_FROM_PATTERN: Pattern = compile(r"---\n*Формат [^`]*```\w*\n+\s*\{\n[^`]+\s*```\s*#")

def change_file(path: str | Path):
    with open(path, "r", encoding="utf-8", errors="ignore") as fb:
        content: str = fb.read()

    content = sub(_FROM_PATTERN, "---\n\n#", content)

    with open(path, "w", encoding="utf-8") as f:
        f.write(content)

    print(f"File {path} has been changed")


if __name__ == '__main__':
    _path: str = r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common\oam\API\provisioning\entities"

    for file in Path(_path).iterdir():
        change_file(file)
    # file: str = r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common\oam\API\provisioning\entities\aucData.md"
    # change_file(file)
