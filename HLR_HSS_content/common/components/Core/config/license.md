
В файле задаются настройки лицензии узла PROTEI HLR/HSS.
Лицензия задает разрешеные функциональности и обрабатываемый трафик.
Данный конфигурационный файл выдается техподдержкой для конкретного компьютера и привязан к MAC-адресу.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload license.cfg**.

### Описание параметров ###

| Параметр                            | Описание                                                                                                                                                                                  | Тип    | O/M | P/R | Версия |
|-------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----|--------|
| **[[license]]
[License]** | Параметры лицензии узла PROTEI HLR/HSS.                                                                                                                                                   |        |     |     |        |
| HLR                                 | Флаг использования HLR части.pass:q[\<br\>]По умолчанию: 1.                                                                                                                                         | bool   | O   | R   |        |
| HSS                                 | Флаг использования интерфейсов Diameter.pass:q[\<br\>]По умолчанию: 0.                                                                                                                              | bool   | O   | R   |        |
| IMS                                 | Флаг использования интерфейсов IMS.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                   | bool   | O   | R   |        |
| LCS                                 | Флаг использования LCS сервисов.pass:q[\<br\>]По умолчанию:0.                                                                                                                                       | bool   | O   | R   |        |
| NBIOT                               | Флаг использования NB-IOT сервисов.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                   | bool   | O   | R   |        |
| TimeLicense                         | Флаг использования временной лицензии.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                | bool   | O   | P   |        |
| Statistics                          | Флаг ведения статистики по лицензии.pass:q[\<br\>]По умолчанию:0.                                                                                                                                   | bool   | O   | P   |        |
| Signature                           | Подпись лицензии.                                                                                                                                                                         | string | M   | P   |        |
| TrafficNominal                      | Номинальное количество транзакций в секунду по лицензии.pass:q[\<br\>]Диапазон: 1–10&nbsp;000. По умолчанию: 10.                                                                                    | int    | С   | P   |        |
| TrafficCriticalThreshold            | Максимальное количество транзакций TCAP и DIAMETER в секунду.pass:q[\<br\>]По умолчанию: 1.2 * TrafficNominal.                                                                                      | int    | C   | P   |        |
| TrafficThreshold                    | Количество транзакций в секунду, при достижении которого отключается авария по превышению трафика.pass:q[\<br\>]По умолчанию: 1.1 * TrafficNominal.                                                 | int    | C   | P   |        |
| TrafficThresholdInterval            | Время, в течение которого узел PROTEI HLR/HSS может обрабатывать транзакции TCAP и Diameter, превышающие максимальное число TrafficCriticalThreshold, в секундах.pass:q[\<br\>]По умолчанию: 600 с. | int    | C   | P   |
| TcapTrafficNominal                  | Номинальное количество транзакций TCAP в секунду по лицензии.pass:q[\<br\>]Диапазон: 1-10&nbsp;000. По умолчанию: 10.                                                                               | int    | C   | P   |        |
| TcapTrafficCriticalThreshold        | Максимальное количество транзакций TCAP в секунду.pass:q[\<br\>]По умолчанию: 1.2 * TcapTrafficNominal.                                                                                             | int    | C   | P   |        |
| TcapTrafficThreshold                | Количество транзакций TCAP в секунду, при котором отключается авария по превышению трафика.pass:q[\<br\>]По умолчанию: 1.1 * TcapTrafficNominal.                                                    | int    | C   | P   |        |
| TcapTrafficThresholdInterval        | Время, в течение которого PROTEI HLR/HSS может обрабатывать транзакции TCAP, превышающие максимальное число TcapTrafficCriticalThreshold, в секундах.pass:q[\<br\>]По умолчанию: 600.               | int    | C   | P   |
| DiampTrafficNominal                 | Номинальное общее количество транзакций Diameter в секунду по лицензии.pass:q[\<br\>]Диапазон: 1-10&nbsp;000. По умолчанию: 10.                                                                     | int    | C   | P   |        |
| DiamTrafficCriticalThreshold        | Критическая граница превышения DIAMETER трафика.pass:q[\<br\>]По умолчанию: 1.2 * DiamTrafficNominal.                                                                                               | int    | C   | P   |        |
| DiamTrafficThreshold                | Количество транзакций Diameter в секунду, при котором отключается авария по превышению трафика.pass:q[\<br\>]По умолчанию: 1.1 * TcapTrafficNominal.                                                | int    | C   | P   |        |
| DiamTrafficThresholdInterval        | Время, в течение которого PROTEI HLR/HSS может обрабатывать транзакции Diameter, превышающие максимальное число DiamTrafficCriticalThreshold, в секундах.pass:q[\<br\>]По умолчанию: 600.           | int    | C   | P   |

**Примечание.** Если файл лицензии нарушен или лицензионный ключ некорректный, то приложение будет работать с лицензией по умолчанию (параметр HLR = 1, параметр TrafficNominal  = 10).

#### Пример #### 

```ini
[License]
TrafficNominal = 10;
TrafficCriticalThreshold = 12;
TrafficThreshold = 11;
TrafficThresholdInterval = 600;
TcapTrafficNominal = 10;
TcapTrafficCriticalThreshold = 12;
TcapTrafficThreshold = 11;
TcapTrafficThresholdInterval = 600;
DiamTrafficNominal = 10;
DiamTrafficCriticalThreshold = 12;
DiamTrafficThreshold = 11;
DiamTrafficThresholdInterval = 600;
HSS = 1;
IMS = 0;
Statistics = 0;
TimeLicense = 0;
```