
В файле задаются настройки сбора метрик.

## Используемые секции ##


* [\[General\]](#general) - общие параметры сбора метрик;

* [\[Subscribers\]](#subscribers) - параметры сбора абонентских счётчиков;

* [\[Diameter\]](#diameter) - параметры сбора метрик, связанных с протоколом Diameter;

* [\[S6a-interface\]](#s6a-interface) - параметры сбора метрик, связанных с интерфейсом S6a;

* [\[S6t-interface\]](#s6t-interface) - параметры сбора метрик, связанных с интерфейсом S6t;

* [\[Cx-interface\]](#cx-interface) - параметры сбора метрик, связанных с интерфейсом Cx;

* [\[Sh-interface\]](#sh-interface) - параметры сбора метрик, связанных с интерфейсом Sh.

### Описание параметров ###

| Параметр                                          | Описание                                                                                        | Тип          | O/M | P/R     | Версия  |
|---------------------------------------------------|-------------------------------------------------------------------------------------------------|--------------|-----|---------|---------|
| **[[general]]\[General\]**             | Общие параметры сбора метрик.                                                                   |              | O   | P       | 2.3.8.0 |
| Enable                                            | Флаг сбора метрик.pass:q[\<br\>]По умолчанию: 0.                                                          | bool         | O   | P       | 2.3.8.0 |
| Directory                                         | Директория для хранения метрик.pass:q[\<br\>]По умолчанию: ../metrics.                                    | string       | O   | P       | 2.3.8.0 |
| [[granularity]]\[Granularity\]         | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: 5.                                                 | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| [[node-name]]\[NodeName\]              | Имя узла, указываемое в названии файла с метриками.                                             | string       | O   | P       | 2.3.8.0 |
| [[app-name]]\[AppName\]                | Имя приложения, обходимое для инициализации нужного набора метрик.                              | string       | O   | P       | 2.3.8.0 |
| HhMmSeparator                                     | Флаг включения разделителя между часами и минутами HH:MM.pass:q[\<br\>]По умолчанию: 0.                   | bool         | O   | P       | 2.3.8.0 |
| **[[diameter]]\[Diameter\]**           | Параметры для метрик Diameter.                                                                  | object     O | P   | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| **[[s6a-interface]]\[S6a-interface\]** | Параметры для метрик S6a.                                                                       | object       | O   | P       | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| **[[s6t-interface]]\[S6t-interface\]** | Параметры для метрик S6t.                                                                       | object       | O   | P       | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| **[[cx-interface]]\[Cx-interface\]**   | Параметры для метрик Cx.                                                                        | object       | O   | P       | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| **[[sh-interface]]\[Sh-interface\]**   | Параметры для метрик Sh.                                                                        | object       | O   | P       | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |
| **[[subscribers]]\[Subscribers\]**     | Параметры для метрик по абонентам.                                                              | object       | O   | P       | 2.3.8.0 |
| Granularity                                       | Гранулярность сбора метрик.pass:q[\<br\>]По умолчанию: значение [\[General\]::Granularity](#granularity). | intpass:q[\<br\>]мин   | O   | P       | 2.3.8.0 |

#### Пример ####

```ini
[General]
Enable = 1;
Granularity = 30;
Directory = "/usr/protei/Protei_HSS/metrics";
AppName=HSS;
NodeName = HSS-1;

[Diameter]
Enable = 1;
Granularity = 15;

[S6a-interface]
Enable = 1;
Granularity = 1;
```