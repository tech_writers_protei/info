
В файле задаются настройки драйвера платы.

### Описание параметров ###

| Параметр         | Описание                                                                                     | Тип    | O/M | P/R | Версия |
|------------------|----------------------------------------------------------------------------------------------|--------|-----|-----|--------|
| ComponentAddr    | Адрес компоненты. Формат: `Ph.Card.<id>`                                                       | string | M   | P   |        |
| ComponentType    | Тип компоненты                                                                               | string | M   | P   |        |
| Params           | Параметры компоненты                                                                         | object | M   | P   |        |
| Type             | Тип платы. pass:q[\<br\>]Возможные значения: ATP/TSP2/TSP3/TSP4/Consul/Consul7. pass:q[\<br\>]По умолчанию: TSP4 | string | O   | P   |        |
| IRQ              | Код аппаратного прерывания для платы. pass:q[\<br\>]По умолчанию: 11                                   | int    | O   | P   |        |
| HardwareRootPath | Путь до директории с драйверами устройств. pass:q[\<br\>]По умолчанию: /usr/protei/hardware            | string | O   | P   |        |
| ADSP             | Тип программы/прошивки, загружаемой в сигнальный процессор                                   | string | O   | P   |        |
| BPC              | Флаг использования BPC                                                                       | string | O   | P   |        |
| VoltageType      | Величина требуемого напряжения                                                               | int    | O   | P   |        |


#### Пример ####

```ini
{
  ComponentAddr = Ph.Card.0;
  ComponentType = Ph.Card;
  Params = {
    Type = "TSP4";
    IRQ = "11";
    HardwareRootPath = "/usr/protei/hardware/";
    ADSP = { "HDLC16"; "ECHO"; "DTMF"; "MIXER"; };
    UseBPC = "1";
    VoltageType = "48";
  };
}
{
  ComponentAddr = Ph.Card.0.ITC.19;
  ComponentType = Ph.ITC;
  Params = {
    Version = 1;
    IP = {
      { IP = 192.168.92.46/17; };
    };
  };
}
```