
Файл **<node_name>\_HSS-S6a-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам HSS для интерфейса S6a.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272) 
и [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301).

| Tx/Rx | Метрика                                                                         | Описание                                                           | Группа         | 
|-------|---------------------------------------------------------------------------------|--------------------------------------------------------------------|----------------|
| Rx    | [[authenticationInformationRequest]]authenticationInformationRequest | Количество сообщений Diameter: Authentication-Information-Request. | IMSIPLMN:Value | 
| Tx    | [[authenticationInformationAnswer]]authenticationInformationAnswer   | Количество сообщений Diameter: Authentication-Information-Answer.  | IMSIPLMN:Value | 
| Rx    | [[updateLocationRequest]]updateLocationRequest                       | Количество сообщений Diameter: Update-Location-Request.            | IMSIPLMN:Value | 
| Tx    | [[updateLocationAnswer]]updateLocationAnswer                         | Количество сообщений Diameter: Update-Location-Answer.             | IMSIPLMN:Value | 
| Tx    | [[resetRequest]]resetRequest                                         | Количество сообщений Diameter: Reset-Request.                      | IMSIPLMN:Value | 
| Rx    | [[resetAnswer]]resetAnswer                                           | Количество сообщений Diameter: Reset-Answer.                       | IMSIPLMN:Value | 
| Tx    | [[cancelLocationRequest]]cancelLocationRequest                       | Количество сообщений Diameter: Cancel-Location-Request.            | IMSIPLMN:Value | 
| Rx    | [[cancelLocationAnswer]]cancelLocationAnswer                         | Количество сообщений Diameter: Cancel-Location-Answer.             | IMSIPLMN:Value | 
| Tx    | [[insertSubscriberDataRequest]]insertSubscriberDataRequest           | Количество сообщений Diameter: Insert-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Rx    | [[insertSubscriberDataAnswer]]insertSubscriberDataAnswer             | Количество сообщений Diameter: Insert-Subscriber-Data-Answer.      | IMSIPLMN:Value | 
| Rx    | [[purgeUeRequest]]purgeUeRequest                                     | Количество сообщений Diameter: Purge-UE-Request.                   | IMSIPLMN:Value | 
| Tx    | [[purgeUeAnswer]]purgeUeAnswer                                       | Количество сообщений Diameter: Purge-UE-Answer.                    | IMSIPLMN:Value | 
| Rx    | [[notifyRequest]]notifyRequest                                       | Количество сообщений Diameter: Notify-Request.                     | IMSIPLMN:Value | 
| Tx    | [[notifyAnswer]]notifyAnswer                                         | Количество сообщений Diameter: Notify-Answer.                      | IMSIPLMN:Value | 
| Tx    | [[deleteSubscriberDataRequest]]deleteSubscriberDataRequest           | Количество сообщений Diameter: Delete-Subscriber-Data-Request.     | IMSIPLMN:Value | 
| Rx    | [[deleteSubscriberDataAnswer]]deleteSubscriberDataAnswer             | Количество сообщений Diameter: Delete-Subscriber-Data-Answer.      | IMSIPLMN:Value | 

В таблице ниже приведены возможные значения AVP `Result-Code` и AVP `Experimental-Result-Code` из перечисленных в
[3GPP TS 29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229) и
[3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272).

**Примечание.** Коды AVP `Experimental-Result-Code` отмечены буквой `e`.

| Код   | Описание                                        |
|-------|-------------------------------------------------|
| 2001  | DIAMETER_SUCCESS                                |
| 4181e | DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE        |
| 5001e | DIAMETER_ERROR_USER_UNKNOWN                     |
| 5003e | DIAMETER_ERROR_IDENTITY_NOT_REGISTERED          |
| 5004e | DIAMETER_ERROR_ROAMING_NOT_ALLOWED              |
| 5012e | DIAMETER_ERROR_SERVING_NODE_FEATURE_UNSUPPORTED |
| 5420e | DIAMETER_ERROR_UNKNOWN_EPS_SUBSCRIPTION         |
| 5421e | DIAMETER_ERROR_RAT_NOT_ALLOWED                  |
| 5423e | DIAMETER_ERROR_UNKNOWN_SERVING_NODE             |

При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP `Experimental-Result-Code` отмечены дополнительной буквой `e` в конце.

Пример: `updateLocationAnswer5420e`.

### Группа ###

| Название    | Описание                                                         | Тип |
|-------------|------------------------------------------------------------------|-----|
| IMSIPLMN    | Идентификатор сети PLMN на основе первых 5 цифр из номера IMSI.  | int |

#### Пример ####

```
IMSIPLMN:25099
```

#### Пример файла ####

```csv
tx,updateLocationAnswer,,71
rx,updateLocationAnswer2001,,71
tx,cancelLocationRequest,,2
tx,authenticationInformationAnswer,,134
tx,authenticationInformationAnswer2001,,134
tx,authenticationInformationAnswer4181e,,0
tx,insertSubscriberDataRequest,,0
tx,deleteSubscriberDataRequest,,0
tx,purgeUeAnswer,,47
tx,purgeUeAnswer2001,,47
tx,resetRequest,,0
tx,notifyAnswer,,118
tx,notifyAnswer2001,,90
tx,notifyAnswer5423e,,28
rx,updateLocationRequest,,71
rx,cancelLocationAnswer,,2
rx,cancelLocationAnswer2001,,2
rx,authenticationInformationRequest,,134
rx,insertSubscriberDataAnswer,,0
rx,deleteSubscriberDataAnswer,,0
rx,purgeUeRequest,,47
rx,resetAnswer,,0
rx,notifyRequest,,118
tx,updateLocationAnswer2001,IMSIPLMN:00101,59
tx,updateLocationAnswer,IMSIPLMN:00101,59
tx,cancelLocationRequest,IMSIPLMN:00101,2
tx,authenticationInformationAnswer2001,IMSIPLMN:00101,129
tx,authenticationInformationAnswer,IMSIPLMN:00101,129
tx,purgeUeAnswer2001,IMSIPLMN:00101,46
tx,purgeUeAnswer,IMSIPLMN:00101,46
tx,notifyAnswer2001,IMSIPLMN:00101,86
tx,notifyAnswer5423e,IMSIPLMN:00101,28
tx,notifyAnswer,IMSIPLMN:00101,114
tx,updateLocationAnswer2001,IMSIPLMN:20893,12
tx,updateLocationAnswer,IMSIPLMN:20893,12
tx,authenticationInformationAnswer2001,IMSIPLMN:20893,5
tx,authenticationInformationAnswer4181e,IMSIPLMN:20893,0
tx,authenticationInformationAnswer,IMSIPLMN:20893,5
tx,purgeUeAnswer2001,IMSIPLMN:20893,1
tx,purgeUeAnswer,IMSIPLMN:20893,1
tx,notifyAnswer2001,IMSIPLMN:20893,4
tx,notifyAnswer,IMSIPLMN:20893,4
tx,cancelLocationRequest,IMSIPLMN:99999,0
rx,updateLocationRequest,IMSIPLMN:00101,59
rx,cancelLocationAnswer2001,IMSIPLMN:00101,2
rx,cancelLocationAnswer,IMSIPLMN:00101,2
rx,authenticationInformationRequest,IMSIPLMN:00101,129
rx,purgeUeRequest,IMSIPLMN:00101,46
rx,notifyRequest,IMSIPLMN:00101,114
rx,updateLocationRequest,IMSIPLMN:20893,12
rx,authenticationInformationRequest,IMSIPLMN:20893,5
rx,purgeUeRequest,IMSIPLMN:20893,1
rx,notifyRequest,IMSIPLMN:20893,4
rx,cancelLocationAnswer2001,IMSIPLMN:99999,0
rx,cancelLocationAnswer,IMSIPLMN:99999,0
```