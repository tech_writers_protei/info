
Файл **<node_name>\_HSS-Cx-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам HSS для интерфейса Cx.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229).

| Tx/Rx | Метрика                                                                     | Описание                                                         | Группа | 
|-------|-----------------------------------------------------------------------------|------------------------------------------------------------------|--------|
| Rx    | [[userAuthorizationRequest]]userAuthorizationRequest             | Количество сообщений Diameter: User-Authorization-Request.       |        | 
| Tx    | [[userAuthorizationAnswer]]userAuthorizationAnswer               | Количество сообщений Diameter: User-Authorization-Answer.        |        | 
| Rx    | [[serverAssignmentRequest]]serverAssignmentRequest               | Количество сообщений Diameter: Server-Assignment-Request.        |        | 
| Tx    | [[serverAssignmentAnswer]]serverAssignmentAnswer                 | Количество сообщений Diameter: Server-Assignment-Answer.         |        | 
| Rx    | [[locationInfoRequest]]locationInfoRequest                       | Количество сообщений Diameter: Location-Info-Request.            |        | 
| Tx    | [[locationInfoAnswer]]locationInfoAnswer                         | Количество сообщений Diameter: Location-Info-Answer.             |        | 
| Rx    | [[multimediaAuthRequest]]multimediaAuthRequest                   | Количество сообщений Diameter: Multimedia-Auth-Request.          |        | 
| Tx    | [[multimediaAuthAnswer]]multimediaAuthAnswer                     | Количество сообщений Diameter: Multimedia-Auth-Answer.           |        | 
| Tx    | [[registrationTerminationRequest]]registrationTerminationRequest | Количество сообщений Diameter: Registration-Termination-Request. |        | 
| Rx    | [[registrationTerminationAnswer]]registrationTerminationAnswer   | Количество сообщений Diameter: Registration-Termination-Answer.  |        | 
| Tx    | [[pushProfileRequest]]pushProfileRequest                         | Количество сообщений Diameter: Push-Profile-Request.             |        | 
| Rx    | [[pushProfileAnswer]]pushProfileAnswer                           | Количество сообщений Diameter: Push-Profile-Answer.              |        | 


В таблице ниже приведены возможные значения AVP `Result-Code` и AVP `Experimental-Result-Code` из перечисленных в
[3GPP TS 29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229).

**Примечание.** Коды AVP `Experimental-Result-Code` отмечены буквой `e`.

| Код   | Описание                                                  |
|-------|-----------------------------------------------------------|
| 2001  | DIAMETER_SUCCESS                                          |
| 2001e | DIAMETER_FIRST_REGISTRATION                               |
| 2002e | DIAMETER_SUBSEQUENT_REGISTRATION                          |
| 2003e | DIAMETER_UNREGISTERED_SERVICE                             |
| 2004e | DIAMETER_SUCCESS_SERVER_NAME_NOT_STORED                   |
| 5001e | DIAMETER_ERROR_USER_UNKNOWN                               |
| 5002e | DIAMETER_ERROR_IDENTITIES_DONT_MATCH                      |
| 5003e | DIAMETER_ERROR_IDENTITY_NOT_REGISTERED                    |
| 5004e | DIAMETER_ERROR_ROAMING_NOT_ALLOWED                        |
| 5005e | DIAMETER_ERROR_IDENTITY_ALREADY_REGISTERED                |
| 5006e | DIAMETER_ERROR_AUTH_SCHEME_NOT_SUPPORTED                  |
| 5007e | DIAMETER_ERROR_IN_ASSIGNMENT_TYPE                         |
| 5008e | DIAMETER_ERROR_TOO_MUCH_DATA                              |
| 5009e | DIAMETER_ERROR_NOT_SUPPORTED_USER_DATA                    |
| 5011e | DIAMETER_ERROR_FEATURE_UNSUPPORTED                        |
| 5012e | DIAMETER_ERROR_SERVING_NODE_FEATURE_UNSUPPORTED           |


При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP `Experimental-Result-Code` отмечены дополнительной буквой `e` в конце.

Пример: `serverAssignmentAnswer2001e`.


#### Пример файла ####

```csv
rx,userAuthorizationRequest,,5
tx,userAuthorizationAnswer2001,,4
tx,userAuthorizationAnswer5001e,,1
rx,serverAssignmentRequest,,6
tx,serverAssignmentAnswer2001e,,4
tx,serverAssignmentAnswer2002e,,2
rx,locationInfoRequest,,3
tx,locationInfoAnswer2001,,3
rx,multimediaAuthRequest,,8
tx,multimediaAuthAnswer2001,,8
tx,registrationTerminationRequest,,8
rx,registrationTerminationAnswer2001,,8
tx,pushProfileRequest,,2
rx,pushProfileAnswer2001,,2
```