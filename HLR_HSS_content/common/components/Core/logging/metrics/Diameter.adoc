Файл *_HSS-Diameter__.csv* содержит статистическую информацию по метрикам HSS для процедур протокола Diameter: Base.

== Описание параметров

Подробную информацию см. https://www.ietf.org/rfc/rfc6733.txt.pdf[RFC 6733].

[width="100%",cols="4%,36%,54%,6%",options="header",]
|===
|Tx/Rx |Метрика |Описание |Группа
|Tx/Rx |[[deviceWatchdogRequest]]deviceWatchdogRequest |Количество сообщений Diameter: Device-Watchdog-Request, DWR. |REALM:Value
|Tx/Rx |[[deviceWatchdogAnswer]]deviceWatchdogAnswer |Количество сообщений Diameter: Device-Watchdog-Answer, DWA. |REALM:Value
|Tx/Rx |[[deviceWatchdogAnswer2001]]deviceWatchdogAnswer2001 |Количество сообщений Diameter: Device-Watchdog-Answer, DWA, c AVP `Result-Code = DIAMETER_SUCCESS (2001)`. |REALM:Value
|Tx/Rx |[[capabilitiesExchangeRequest]]capabilitiesExchangeRequest |Количество сообщений Diameter: Capabilities-Exchange-Request, CER. |REALM:Value
|Tx/Rx |[[capabilitiesExchangeAnswer]]capabilitiesExchangeAnswer |Количество сообщений Diameter: Capabilities-Exchange-Answer, CEA. |REALM:Value
|Tx/Rx |[[capabilitiesExchangeAnswer2001]]capabilitiesExchangeAnswer2001 |Количество сообщений Diameter: Capabilities-Exchange-Answer, CEA, c AVP `Result-Code = DIAMETER_SUCCESS (2001)`. |REALM:Value
|Tx/Rx |[[disconnectPeerRequest]]disconnectPeerRequest |Количество сообщений Diameter: Disconnect-Peer-Request, DPR. |REALM:Value
|Tx/Rx |[[disconnectPeerAnswer]]disconnectPeerAnswer |Количество сообщений Diameter: Disconnect-Peer-Answer, DPA. |REALM:Value
|Tx/Rx |[[disconnectPeerAnswer2001]]disconnectPeerAnswer2001 |Количество сообщений Diameter: Disconnect-Peer-Answer, DPA, c AVP `Result-Code = DIAMETER_SUCCESS (2001)`. |REALM:Value
|===

== Группа

[width="100%",cols="6%,90%,4%",options="header",]
|===
|Название |Описание |Тип
|REALM |Значение `Destination-Realm`, заданное параметром link:../../../config/diam_dest/#dest-realm/[diam_dest.cfg :: DestRealm]. Формат:pass:q[<br>]`<domain>.mnc<MNC>.mcc<MCC>.3gppnetwork.org` |string
|===

=== Пример

....
REALM:epc.mnc001.mcc001.3gppnetwork.org
....

=== Пример файла

[source,csv]
----
rx,capabilitiesExchangeRequest,,0
rx,capabilitiesExchangeAnswer,,0
rx,capabilitiesExchangeAnswer2001,,0
rx,deviceWatchdogRequest,,211
rx,deviceWatchdogAnswer,,87
rx,deviceWatchdogAnswer2001,,87
tx,capabilitiesExchangeRequest,,0
tx,capabilitiesExchangeAnswer,,0
tx,deviceWatchdogRequest,,87
tx,deviceWatchdogAnswer,,211
tx,deviceWatchdogAnswer2001,,211
----
