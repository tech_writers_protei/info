
Файл **<node_name>\_HSS-S6t-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам HSS для интерфейса S6t.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.336](https://www.etsi.org/deliver/etsi_ts/129300_129399/129336).

| Tx/Rx | Метрика                                                                       | Описание                                                         | Группа | 
|-------|-------------------------------------------------------------------------------|------------------------------------------------------------------|--------|
| Rx    | [[configurationInformationRequest]]configurationInformationRequest | Количество сообщений Diameter:Configuration-Information-Request. |        | 
| Tx    | [[configurationInformationAnswer]]configurationInformationAnswer   | Количество сообщений Diameter: Configuration-Information-Answer. |        | 
| Tx    | [[reportInformationRequest]]reportInformationRequest               | Количество сообщений Diameter:Report-Information-Request.        |        | 
| Rx    | [[reportInformationAnswer]]reportInformationAnswer                 | Количество сообщений Diameter: Report-Information-Answer.        |        | 
| Rx/Tx | [[niddInformationRequest]]niddInformationRequest                   | Количество сообщений Diameter:NIDD-Information-Request.          |        | 
| Rx/Tx | [[niddInformationAnswer]]niddInformationAnswer                     | Количество сообщений Diameter: NIDD-Information-Answer.          |        | 


В таблице ниже приведены возможные значения AVP `Result-Code` и AVP `Experimental-Result-Code` из перечисленных в
[3GPP TS 29.336](https://www.etsi.org/deliver/etsi_ts/129300_129399/129336).

**Примечание.** Коды AVP `Experimental-Result-Code` отмечены буквой `e`.

| Код   | Описание                                                  |
|-------|-----------------------------------------------------------|
| 2001  | DIAMETER_SUCCESS                                          |
| 4181e | DIAMETER_AUTHENTICATION_DATA_UNAVAILABLE                  |
| 5001e | DIAMETER_ERROR_USER_UNKNOWN                               |
| 5451e | DIAMETER_ERROR_USER_NO_APN_SUBSCRIPTION                   |
| 5510e | DIAMETER_ERROR_UNAUTHORIZED_REQUESTING_ENTITY             |
| 5511e | DIAMETER_ERROR_UNAUTHORIZED_SERVICE                       |
| 5512e | DIAMETER_ERROR_REQUESTED_RANGE_IS_NOT_ALLOWED             |
| 5513e | DIAMETER_ERROR_CONFIGURATION_EVENT_STORAGE_NOT_SUCCESSFUL |
| 5514e | DIAMETER_ERROR_CONFIGURATION_EVENT_NON_EXISTANT           |

При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP `Experimental-Result-Code` отмечены дополнительной буквой `e` в конце.

Пример: `configurationInformationAnswer5420e`.


#### Пример файла ####

```csv
rx,configurationInformationRequest,,71
tx,configurationInformationAnswer2001,,69
tx,configurationInformationAnswer5001e,,2
rx,niddInformationRequest,,15
tx,niddInformationAnswer2001,,15
tx,niddInformationRequest,,3
rx,niddInformationAnswer2001,,3
tx,reportInformationRequest,,8
rx,reportInformationAnswer2001,,8
```