
Файл **<node_name>\_HSS-Subscribers\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам HSS для интерфейса Cx.

### Описание параметров ###

| Tx/Rx | Метрика                                                                                               | Описание                                                                                                                                                                           | Группа | 
|-------|-------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|
|       | [[numberOfSubscriberWithNbiotService]]numberOfSubscriberWithNbiotService                   | Количество абонентов с привязанными non-IP EPS контекстами (активные флаги nonIpPdnTypeIndicator И nonIpDataDeliveryMechanism)                                                     |        | 
|       | [[numberOfActiveSubscriberWithoutNbiotArd]]numberOfActiveSubscriberWithoutNbiotArd         | Количество зарегистрированных абонентов с отсутствием запретов регистрации в NBIoT домене средствами ARD (nbIotNotAllowed 0)                                                       |        | 
|       | [[numberOfSubscriberWithoutNbiotArd]]numberOfSubscriberWithoutNbiotArd                     | Количество абонентов с отсутствием запретов регистрации в NBIoT домене средствами ARD (nbIotNotAllowed 0)                                                                          |        | 
|       | [[numberOfSubscriberWithActivatedNbiotService]]numberOfSubscriberWithActivatedNbiotService | Количество зарегистрированных абонентов с привязанными non-IP EPS контекстами (активные флаги nonIpPdnTypeIndicator И nonIpDataDeliveryMechanism) и RAT-Type: EUTRAN-NB-IoT (1006) |        | 

#### Пример файла ####

```csv
,numberOfSubscriberWithNbiotService,,15
,numberOfActiveSubscriberWithoutNbiotArd,,43
,numberOfSubscriberWithoutNbiotArd,,286
,numberOfSubscriberWithActivatedNbiotService,,7
```