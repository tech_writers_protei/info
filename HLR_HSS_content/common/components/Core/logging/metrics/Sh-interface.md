
Файл **<node_name>\_HSS-Sh-interface\_\<datetime\>\_\<granularity\>.csv** содержит статистическую информацию по метрикам HSS для интерфейса Sh.

### Описание параметров ###

Подробную информацию см. [3GPP TS 29.329](https://www.etsi.org/deliver/etsi_ts/129300_129399/129329).

| Tx/Rx | Метрика                                                                 | Описание                                                       | Группа | 
|-------|-------------------------------------------------------------------------|----------------------------------------------------------------|--------|
| Rx    | [[userDataRequest]]userDataRequest                           | Количество сообщений Diameter: User-Data-Request.              |        | 
| Tx    | [[userDataAnswer]]userDataAnswer                             | Количество сообщений Diameter: User-Data-Answer.               |        | 
| Rx    | [[subscribeNotificationRequest]]subscribeNotificationRequest | Количество сообщений Diameter: Subscribe-Notification-Request. |        | 
| Tx    | [[subscribeNotificationAnswer]]subscribeNotificationAnswer   | Количество сообщений Diameter: Subscribe-Notification-Answer.  |        | 
| Rx    | [[profileUpdateRequest]]profileUpdateRequest                 | Количество сообщений Diameter: Profile-Update-Request.         |        | 
| Tx    | [[profileUpdateAnswer]]profileUpdateAnswer                   | Количество сообщений Diameter: Profile-Update-Answer.          |        | 
| Tx    | [[pushNotificationRequest]]pushNotificationRequest           | Количество сообщений Diameter: Push-Notification-Request.      |        | 
| Rx    | [[pushNotificationAnswer]]pushNotificationAnswer             | Количество сообщений Diameter: Push-Notification-Answer.       |        | 


В таблице ниже приведены возможные значения AVP `Result-Code` и AVP `Experimental-Result-Code` из перечисленных в
[3GPP TS 29.329](https://www.etsi.org/deliver/etsi_ts/129300_129399/129329).

**Примечание.** Коды AVP `Experimental-Result-Code` отмечены буквой `e`.

| Код   | Описание                                                  |
|-------|-----------------------------------------------------------|
| 2001  | DIAMETER_SUCCESS                                          |
| 4100e | DIAMETER_USER_DATA_NOT_AVAILABLE                          |
| 4101e | DIAMETER_PRIOR_UPDATE_IN_PROGRESS                         |
| 5001e | DIAMETER_ERROR_USER_UNKNOWN                               |
| 5002e | DIAMETER_ERROR_IDENTITIES_DONT_MATCH                      |
| 5008e | DIAMETER_ERROR_TOO_MUCH_DATA                              |
| 5011e | DIAMETER_ERROR_FEATURE_UNSUPPORTED                        |
| 5100e | DIAMETER_ERROR_USER_DATA_NOT_RECOGNIZED                   |
| 5101e | DIAMETER_ERROR_OPERATION_NOT_ALLOWED                      |
| 5102e | DIAMETER_ERROR_USER_DATA_CANNOT_BE_READ                   |
| 5103e | DIAMETER_ERROR_USER_DATA_CANNOT_BE_MODIFIED               |
| 5104e | DIAMETER_ERROR_USER_DATA_CANNOT_BE_NOTIFIED               |
| 5105e | DIAMETER_ERROR_TRANSPARENT_DATA_OUT_OF_SYNC               |
| 5106e | DIAMETER_ERROR_SUBS_DATA_ABSENT                           |
| 5107e | DIAMETER_ERROR_NO_SUBSCRIPTION_TO_DATA                    |
| 5108e | DIAMETER_ERROR_DSAI_NOT_AVAILABLE                         |


При наступлении события формируется метрика с кодом AVP в конце.
Коды AVP `Experimental-Result-Code` отмечены дополнительной буквой `e` в конце.

Пример: `userDataAnswer5102e`.


#### Пример файла ####

```csv
rx,userDataRequest,,5
tx,userDataAnswer2001,,4
tx,userDataAnswer5102e,,1
rx,subscribeNotificationRequest,,6
tx,subscribeNotificationAnswer2001,,4
tx,subscribeNotificationAnswer5104e,,2
rx,profileUpdateRequest,,3
tx,profileUpdateAnswer2001,,3
tx,pushNotificationRequest,,8
rx,pushNotificationAnswer2001,,8
```