
Информация обо всех проводимых TCAP-транзакциях записывается в журнал hlr_cdr.log.
Log-файл: hlr_cdr.

### Формат CDR ###

DateTime; CommandName; OpCode; IMSI; CgPN; CdPN; TID; DB_Status;
TrStatus; ErrorCode; Version; VectorCount; MSISDN; VLR; MSC;
SGSN; SGSN_Number; AlertReason; SCA; GMSC; InterrogationType; Camel;
SuppressTCSI; MSRN; Full; SCF; SsCode; USSD; ISD_Count;



### Описание используемых полей ###

| N  | Поле              | Описание                                                                                                                                                                     | Тип      |
|----|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1  | DT                | Временная метка формирования записи в файле CDR                                                                                                                              | datetime |
| 2  | CommandName       | Название команды                                                                                                                                                             | string   |
| 3  | OpCode            | Код MAP-сообщения                                                                                                                                                            | int      |
| 4  | IMSI              | Номер IMSI                                                                                                                                                                   | string   |
| 5  | CgPN              | GT SCCP отправителя входящей транзакции                                                                                                                                      | string   |
| 6  | CdPN              | GT SCCP получателя входящей транзакции                                                                                                                                       | string   |
| 7  | TID               | Идентификатор входящей TCAP–транзакции                                                                                                                                       | int      |
| 8  | DB\_Status        | Код результата обработки запроса в базе данных. Cм. возможные значения в разделе [DB_status] (../add_info/db_status)                                                         | int      |
| 9  | TrStatus          | Код статуса завершения транзакции. Cм. возможные значения в разделе [TR_status] (../add_info/tr_status)                                                                      | int      |
| 10 | ErrorCode         | Код ошибки в сообщении TCAP_RETURN_ERROR_IND. Cм. возможные значения в разделе [TCAP_error_codes] (../add_info/Tcap_error_codes)  pass:q[\<br\>]Примечание - 0 -- завершение без ошибки | int      |
| 11 | Version           | Версия MAP                                                                                                                                                                   | int      |
| 12 | VectorCount       | Количество запрошенных векторов аутентификации                                                                                                                               | int      |
| 13 | MSISDN            | Номер MSISDN                                                                                                                                                                 | string   |
| 14 | VLR               | GT узла VLR                                                                                                                                                                  | string   |
| 15 | MSC               | GT узла MSC                                                                                                                                                                  | string   |
| 16 | SGSN              | Номер узла SGSN                                                                                                                                                              | string   |
| 17 | SGSNAddress       | GT узла SGSN.                                                                                                                                                                | string   |
| 18 | AlertReason       | Код причины ошибки                                                                                                                                                           | int      |
| 19 | SCA               | GT обслуживающего центра                                                                                                                                                     | string   |
| 20 | GMSC              | GT узла GMSC                                                                                                                                                                 | string   |
| 21 | InterrogationType | Код цели запроса Interrogation Type. pass:q[\<br\>]Возможные значения: 0 -- basicCall (базовый вызов); 1 -- forwarding (переадресация)                                                   | int      |
| 22 | Camel             | Код поддерживаемых фаз CAMEL на узле VLR. pass:q[\<br\>]Диапазон: 0–7                                                                                                                  | int      |
| 23 | SuppressTCsi      | Флаг получения Suppress_T_CSI в сообщении MAP_SRI                                                                                                                            | bool     |
| 24 | MSRN              | MSRN-номер                                                                                                                                                                   | string   |
| 25 | Full              | Флаг предоставления полной информации в ответе MAP_SRI_Resp                                                                                                                  | bool     |
| 26 | SCF               | GT узла SCF в сети GSM                                                                                                                                                       | string   |
| 27 | SsCode            | Идентификатор дополнительной услуги Supplementary Service                                                                                                                    | int      |
| 28 | USSD              | Текст USSD–сообщения                                                                                                                                                         | string   |
| 29 | IsdCount          | Количество сообщений ISD для передачи абонентского профиля                                                                                                                   | int      |



Примечание - appender = hlr\_cdr