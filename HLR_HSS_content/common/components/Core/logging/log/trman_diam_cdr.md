
Статистика TrafficManager по протоколу Diameter с периодичностью CheckInterval выводится в журнал Trman_diam_cdr.


### Формат CDR ###

DT; LastCheckTime; Speed; AllCalls; In; InLicenceReject; Out; OutLicenceReject; QueueOverload; OutReject; AboveLicense;

### Описание используемых полей ###

| N  | Поле             | Описание                                                       | Тип      |
|----|------------------|----------------------------------------------------------------|----------|
| 1  | DT               | Дата и время формирования записи                               | datetime |
| 2  | LastCheckTime    | Время последнего подсчета статистики TrafficManager.           | datetime |
| 3  | Speed            | Количество поступающих вызовов в секунду.                      | int      |
| 4  | AllCalls         | Количество всех запросов за последний период.                  | int      |
| 5  | In               | Количество полученных запросов.                                | int      |
| 6  | InLicenceReject  | Количество полученных запросов, отбитых ввиду лимита лицензии. | int      |
| 7  | Out              | Количество исходящих запросов.                                 | int      |
| 8  | OutLicenceReject | Количество исходящих запросов, отбитых ввиду лимита лицензии.  | int      |
| 9  | OutReject        | Количество исходящих запросов, отбитых на уровне Diameter.     | int      |
| 10 | AboveLicense     | Количество вызовов, превысивших лицензионный лимит.            | int      |



Примечание - Для записи CDR необходимо прописать в trace.cfg **TrMan_Diam_cdr*