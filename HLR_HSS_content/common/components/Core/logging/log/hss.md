
Информация обо всех проводимых Diameter-транзакциях записывается в журнал hss_cdr.log.
Log-файл: hss_cdr.

### Формат CDR ###

DateTime; CommandName; ApplicationId:OpCode; UserName; OrigHost; DestHost;
SessionId; DB_Status; ErrorCode; MSISDN; SgsnNumber; PrivateId;
PublicId; Sat; Uat; AuthScheme; Count; PlmnId;



### Описание используемых полей ###

| N  | Поле                 | Описание                                                                                                                                                                       | Тип      |
|----|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|
| 1  | DT                   | Временная метка формирования записи в файле CDR                                                                                                                                | datetime |
| 2  | CommandName          | Название команды                                                                                                                                                               | string   |
| 3  | ApplicationId:OpCode | Идентификатор приложения (ApplicationId) и код Diameter–сообщения (OpCode). pass:q[\<br\>] Формат: ApplicationId:OpCode. Cм. возможные значения в разделе [op_code] (../add_info/op_code | string   |
| 4  | UserName             | Имя абонента                                                                                                                                                                   | string   |
| 5  | OrigHost             | Хост отправителя                                                                                                                                                               | string   |
| 6  | DestHost             | Хост получателя                                                                                                                                                                | string   |
| 7  | SessionID            | Идентификатор сессии по протоколу Diameter                                                                                                                                     | string   |
| 8  | DB_Status            | Код результата обработки запроса в базе данных. Cм. возможные значения в разделе [DB_status] (../add_info/db_status)                                                           | int      |
| 9  | ErrorCode            | Код ошибки при обработке команды Diameter. Cм. возможные значения в разделе [DIAM_error_codes] (../add_info/Diam_error_codes)                                                  | int      |
| 10 | Msisdn               | Номер MSISDN пользователя                                                                                                                                                      | string   |
| 11 | SgsnNumber           | GT узла SGSN                                                                                                                                                                   | int      |
| 12 | PrivateId            | Идентификатор IMPI                                                                                                                                                             | string   |
| 13 | PublicId             | Идентификатор IMPU                                                                                                                                                             | string   |
| 14 | Sat                  | Значение Server-Assignment-Type. См. RFC 4740. Cм. возможные значения в разделе [server_assignment_types] (../add_info/server_assignment_types)                                | int      |
| 15 | Uat                  | Значение User-Athorization-Type. См. RFC 4740. Возможные значения: 0: REGISTRATION; 1: DE_REGISTRATION; 2: REGISTRATION_AND_CAPABILI                                           | int      |
| 16 | AuthScheme           | Схема аутентификации. Возможные значения: 4: Digest-AKAv1-MD5; 5: SIP Digest.                                                                                                  | int      |
| 17 | Count                | Количество векторов аутентификации                                                                                                                                             | int      |
| 18 | PlmnId               | Идентификатор сети                                                                                                                                                             | string   |


Примечание - appender = hss_cdr