
Файл **abonent_stat.log** содержит статистическую информацию по абонентам.

### Формат ###

```log
DateTime;GroupID;GroupName;RegCount;SubscriberActiveCount;SignallingActiveCount;ActiveCount;CurrentRegCount;UnblockCount;
```

### Описание параметров ###

| Название              | Описание                                                                                                                         | Тип      |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------|----------|
| DateTime              | Дата и время сбора статистики.                                                                                                   | datetime |
| GroupID               | Идентификатор группы.pass:q[\<br\>]`-1` -- суммарная статистика по всем группам; `0` -- статистика по абонентам, не привязанным к группам. | int      |
| GroupName             | Название группы.pass:q[\<br\>]**Примечание.** Значение `All` -- суммарная статистика по всем группам.                                      | string   |
| RegCount              | Количество абонентов, зарегистрировавшихся в период вывода статистики.                                                           | int      |
| SubscriberActiveCount | Количество уникальных абонентов, совершивших действие в период вывода статистики.                                                | int      |
| SignallingActiveCount | Количество уникальных абонентов с сигнальной активностью в период вывода статистики.                                             | int      |
| ActiveCount           | Количество уникальных активных абонентов в период вывода статистики.                                                             | int      |
| CurrentRegCount       | Текущее количество зарегистрированных абонентов.                                                                                 | int      |
| UnblockCount          | Текущее количество незаблокированных абонентов.                                                                                  | int      |


Примечание - appender = abonent\_stat