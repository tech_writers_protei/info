
Файл **abonent_operative_stat.log** содержит оперативную статистическую информацию по абонентам.

### Формат ###

```log
DataTime;GroupID;GroupName;RegCount;SubscriberActiveCount;SignallingActiveCount;ActiveCount;CurrentRegCount;UnblockCount;
```

### Описание параметров ###

| Название              | Описание                                                                                                                         | Тип      |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------|----------|
| DateTime              | Дата и время сбора статистики.                                                                                                   | datetime |
| GroupID               | Идентификатор группы.pass:q[\<br\>]`-1` -- суммарная статистика по всем группам; `0` -- статистика по абонентам, не привязанным к группам. | int      |
| GroupName             | Название группы.pass:q[\<br\>]**Примечание.** Значение `All` -- суммарная статистика по всем группам.                                      | string   |
| RegCount              | Количество абонентов, зарегистрировавшихся в период вывода статистики.                                                           | int      |
| SubscriberActiveCount | Количество уникальных абонентов, совершивших действие в период вывода статистики.                                                | int      |
| SignallingActiveCount | Количество уникальных абонентов с сигнальной активностью в период вывода статистики.                                             | int      |
| ActiveCount           | Количество уникальных активных абонентов в период вывода статистики.                                                             | int      |
| CurrentRegCount       | Текущее количество зарегистрированных абонентов.                                                                                 | int      |
| UnblockCount          | Текущее количество незаблокированных абонентов.                                                                                  | int      |




Примечание - appender = abonent\_operative\_stat



Список команд, которые относятся к абонентской активности:

1. MAP::SendAuthenticationInfo
2. MAP::UpdateLocation
3. MAP::InterrogateSs
4. MAP::ActivateSs
5. MAP::RegisterSs
6. MAP::EraseSs
7. MAP::DeactivateSs
8. MAP::ReadyForSm
9. MAP::RegisterPassword
10. Diameter::AuthenticalInformation
11. Diameter::UpdateLocation
12. Diameter::Notify
13. Diameter::MeIdentityCheck

Список команд, которые относятся к сигнальной активности:

1. MAP::AnyTimeInterrogation
2. MAP::AnyTimeModification
3. MAP::CancelLocation
4. MAP::InsertSubscriberData
5. MAP::DeleteSubscriberData
6. MAP::ProvideRoamingNumber
7. MAP::ProvideSubscriberInfo
8. MAP::PurgeMs
9. MAP::ReportSmDeliveryStatus
10. MAP::SendRoutingInfo
11. MAP::SendRoutingInfoForSm
12. MAP::SendRoutingInfoForLcs
13. Diameter::CancelLocation
14. Diameter::InsertSubscriberData
15. Diameter::DeleteSubscriberData
16. Diameter::PurgeUe
17. Diameter::LcsRoutingInfo