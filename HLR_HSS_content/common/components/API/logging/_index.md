
Модуль API ведет следующие журналы:


* log:
  * [bs_operation_journal.log](./log/bs_operation_journal/) -- журнал OMI-транзакций;
  * [http_statistics.log](./log/http_statistics/) -- журнал статистики HTTP-транзакций;
  * [omi_statistics.log](./log/omi_statistics/) -- журнал cтатистики OMI-транзакций;
  * [operation_journal.log](./log/operation_journal/) -- журнал HTTP-транзакций.