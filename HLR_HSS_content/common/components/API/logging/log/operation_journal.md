
В журнал `operation_journal.log` записывается информация об HTTP-транзакциях.

**Примечание.** Appender -- `operation_journal`.

Формат:

```log
DT;Operation;Status;ExecutionTime;SentToHlr;UserHost;UserName;MSISDN;IMSI;<param>=<value>;
```

### Описание используемых полей ###

| N  | Поле                   | Описание                                    | Тип      |
|----|------------------------|---------------------------------------------|----------|
| 1  | DT                     | Дата и время выполнения операции.           | datetime |
| 2  | Operation              | Название операции.                          | string   |
| 3  | [Status](#http-status) | Состояние выполнения операции.              | string   |
| 4  | ExecutionTime          | Время выполнения операции, в миллисекундах. | int      |
| 5  | SentToHlr              | Флаг запуска операции на узле HLR.Core.     | bool     |
| 6  | UserHost               | Адрес хоста пользователя.                   | ip       |
| 7  | UserName               | Логин пользователя.                         | string   |
| 8  | MSISDN                 | Номер MSISDN пользователя системы.          | string   |
| 9  | IMSI                   | Номер IMSI пользователя системы.            | string   |
| 10 | Params                 | Измененные параметры абонента.              | [object] |

### [[http-status]]Описание значений результата обработки запроса

| N  | Поле                           | Описание                                       |
|----|--------------------------------|------------------------------------------------|
| 1  | OK                             | Успешное завершение запроса                    |
| 2  | КО                             | Часть групповой операции завершилось неуспешно |
| 3  | NOT_FOUND                      | Объект не найден                               |
| 4  | NOT_AVAILABLE                  | Объект не доступен                             |
| 5  | PERMISSION_DENIED              | Доступ запрещен                                |
| 6  | INVALID_AUTH                   | Некорректная авторизация                       |
| 7  | INTERNAL_ERROR                 | Внутренняя ошибка                              |
| 8  | ALREADY_EXIST                  | Объект уже существует                          |
| 9  | INTEGRITY_VIOLATION            | Нарушение целостности данных                   |
| 10 | OBJECT_BUSY                    | Объект занят                                   |
| 11 | INCORRECT_PARAMS               | Некорректные параметры                         |
| 12 | TEMPORARY_FAILURE              | Временная ошибка                               |
| 13 | HLR_SERVER_ERROR               | Ошибка сервера HLR                             |
| 14 | HLR_API_NOT_AVAILABLE          | HLR API не доступен                            |
| 15 | INVALID_PROFILE                | Некорректный профиль                           |
| 16 | HLR_OBJECT_BUS                 | Объект HLR занят                               |
| 17 | SAME_IMSI                      | Одинаковый номер IMSI                          |
| 18 | NOTHING_CHANGED                | Нет изменений                                  |
| 19 | OBJECT_LINKED                  | Установлена связь с объектом                   |
| 20 | SUPPLEMENTARY_SERVICE_CONFLICT | Конфликт с дополнительной услугой              |
| 21 | REMOTE_HOST_UNREACHABLE        | Нет связи с удаленным хостом                   |