Модуль API ведет следующие журналы:

* log:
** link:./log/bs_operation_journal/[bs_operation_journal.log] -- журнал OMI-транзакций;
** link:./log/http_statistics/[http_statistics.log] -- журнал статистики HTTP-транзакций;
** link:./log/omi_statistics/[omi_statistics.log] -- журнал cтатистики OMI-транзакций;
** link:./log/operation_journal/[operation_journal.log] -- журнал HTTP-транзакций.
