
В файле задаются настройки работы приложения.


### Описание параметров ###

| Параметр                            | Описание                                                                                                                                                                                                   | Тип      | O/M | P/R | Версия   |
|-------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|----------|
| hlr.enable                          | Флаг взаимодействия с HLR Core.pass:q[\<br\>]По умолчанию: 1.                                                                                                                                                        | bool     | O   | R   |          |
| hlr.direction                       | Название направления для отправки команд с помощью OMI.                                                                                                                                                    | string   | O   | R   |          |
| hlr.list                            | Название соединения для отправки команд с помощью OMI.                                                                                                                                                     | [string] | O   | R   |          |
| hlr.auth.enabled                    | Флаг активации авторизации.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                                            | bool     | O   | R   |          |
| hlr.auth.pass.enabled               | Флаг активации авторизации по паролю.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                                  | int      | O   | P   |          |
| hlr.auth.system.login               | Логин для авторизации.                                                                                                                                                                                     | string   | O   | R   |          |
| hlr.licenceKey                      | Ключ лицензии.                                                                                                                                                                                             | string   | O   | P   |          |
| hlr.licenceParam                    | Параметр лицензии.                                                                                                                                                                                         | string   | O   | P   |          |
| db.countTryResolveDeadlock          | Количество попыток повторно отправить транзакцию в случае блокировки в базе данных.                                                                                                                        | int      | O   | R   |          |
| core.pool.init                      | Начальное количество потоков для обработки трафика.pass:q[\<br\>]По умолчанию: значение core.executor_core_thread_count в файле winter.properties.                                                                   | int      | M   | P   | 2.0.40.0 |
| core.pool.max                       | Максимальное количество потоков для обработки трафика.pass:q[\<br\>]По умолчанию: значение core.executor_max_thread_count в файле winter.properties.                                                                 | int      | M   | P   | 2.0.40.0 |
| core.queue                          | Размер очереди для обработки трафика.pass:q[\<br\>]По умолчанию: значение core.executor_queue_capacity в файле winter.properties.                                                                                    | int      | M   | P   | 2.0.40.0 |
| api.pool.init                       | Начальное количество потоков для обработки трафика от Provisioning.pass:q[\<br\>]По умолчанию: значение core.executor_core_thread_count  в файле winter.properties.                                                  | int      | M   | P   | 2.0.40.0 |
| api.pool.max                        | Максимальное количество потоков для обработки трафика от Provisioning.pass:q[\<br\>]По умолчанию: значение core.executor_max_thread_count в файле winter.properties.                                                 | int      | M   | P   | 2.0.40.0 |
| api.queue                           | Размер очереди для обработки трафика от Provisioning.pass:q[\<br\>]По умолчанию: значение core.executor_queue_capacity nt в файле winter.properties.                                                                 | int      | M   | P   | 2.0.40.0 |
| api.systemTransactionsLimit         | Максимальная скорость обработки системы, в транзакциях в секунду.pass:q[\<br\>]По умолчанию: 2<sup>31</sup> – 1.                                                                                                     | int      | O   | R   | 2.0.46.0 |
| api.userTransactionsLimit           | Максимальное скорость обработки для пользователя, в транзакциях в секунду.pass:q[\<br\>]По умолчанию: 2<sup>31</sup> – 1.                                                                                            | int      | O   | R   | 2.0.46.0 |
| api.usersTransactionsLimit          | Перечень пользователей с заданной вручную допустимой скоростью обработки.pass:q[\<br\>] Атрибуты: host, limit.                                                                                                       | [object] | O   | R   | 2.0.46.0 |
| {                                   |                                                                                                                                                                                                            |          |     |     |          |
| &nbsp;&nbsp;host                    | IP–адрес пользователя.                                                                                                                                                                                     | ip       | М   | R   | 2.0.46.0 |
| &nbsp;&nbsp;limit                   | Допустимая скорость обработки для пользователя, в транзакциях в секунду.                                                                                                                                   | int      | М   | R   | 2.0.46.0 |
| }                                   |                                                                                                                                                                                                            |          |     |     |          |
| api.disableCommandsToHlr            | Флаг отключения отправки команд CL/ISD/DSD, инициированных командами API.                                                                                                                                  | bool     | O   | R   | 2.0.56.8 |
| omi.trman.timer                     | Период подсчета статистики OMI-транзакций, в миллисекундах.pass:q[\<br\>]По умолчанию: 60&nbsp;000.                                                                                                                  | int      | O   | P   | 2.0.48.0 |
| http.trman.timer                    | Период подсчета статистики HTTP-транзакций, в миллисекундах.pass:q[\<br\>]По умолчанию: 60&nbsp;000.                                                                                                                 | int      | O   | P   | 2.0.48.0 |
| oss.OperatorCustomizedAlertingTone  | Код мелодии при заказе услуги RBT.                                                                                                                                                                         | int      | O   | R   | 2.0.43.0 |
| cache.<Field>.is_enabled            | Флаг активации кэша параметра <Field>.pass:q[\<br\>]По умолчанию: значение default pass:q[\<br\>] Примечание - Значение по умолчанию -- cache.default.is_enabled = false.                                                      | bool     | O   | P   | 2.0.55.0 |
| cache.<Field>.max_size              | Максимальный размер кэша параметра <Field>, в байтах.pass:q[\<br\>]По умолчанию: 10&nbsp;000.                                                                                                                        | int      | O   | P   | 2.0.55.0 | 	
| cache.<Field>.initial_capacity      | Начальный размер кэша параметра <Field>, в байтах.pass:q[\<br\>]По умолчанию: 10&nbsp;000.                                                                                                                           | int      | O   | P   | 2.0.55.0 |
| cache.<Field>.expire_after_write    | Очистка кэша параметра <Field> спустя определенное время после записи, в миллисекундах.pass:q[\<br\>]По умолчанию: значение default.                                                                                 | int      | C   | P   | 2.0.55.0 |
| cache.<Field>.expire_after_access   | Очистка кэша параметра <Field> спустя определенное время после последнего обращения, в миллисекундах.pass:q[\<br\>]По умолчанию: значение default.                                                                   | int      | C   | P   | 2.0.55.0 |
| cache.<Field>.is_statistics_enabled | Флаг вывода статистики HTTP-транзакций для параметра <Field>, в миллисекундах.pass:q[\<br\>]По умолчанию: значение default.  pass:q[\<br\>] Примечание - Значение по умолчанию -- cache.default.is_statistics_enabled = false. | int      | O   | P   | 2.0.55.0 |






Допустимые параметры для использования в <Field>:

* default - объекты, для которых не заданы конкретные настройки;

* config - конфигурация базы данных;

* aucC - параметр C профиля AUC;

* aucR - параметр R профиля AUC;

* aucOp - параметр OP профиля AUC;

* aucTk - параметр TK профиля AUC;

* oCsi - профиль O-CSI;

* tCsi - профиль T-CSI;

* smsCsi - профиль SMS-CSI;

* gprsCsi - профиль GPRS-CSI;

* dCsi - профиль D-CSI;

* mCsi - профиль M-CSI;

* ssCsi - профиль SS-CSI;

* ussdCsi - профиль USSD-CSI;

* oImCsi - профиль O-IM-CSI;

* vtCsi - профиль VT-CSI;

* dImCsi - профиль D-IM-CSI;

* pdpContext - профиль контекста PDP;

* epsContext - профиль контекста EPS;

* qosGprs - профиль QOS_GPRS;

* qosEps - профиль QOS_EPS;

* wl - белый перечень;

* bl - черный перечень;

* regional - код региональной зоны;

* as - профиль сервера приложений;

* preferredScscfSet - набор предпочитаемых S-CSCF;

* capabilitiesSet - набор Capabilities;

* chargingInformation - параметры тарификации Charging Information;

* group - группа пользователей;

* sIfcSet - группа пользователей.