
В файле задаются настройки записи лог-файлов. Полная информация о файле приведена в официальном руководстве Logback.

### Используемые секции ###


* [\[Configuration\]](#configuration) - конфигурация параметров записи лог-файлов.

### Описание параметров ###

| Параметр | Описание | Тип | O/M | P/R | Версия |
|-------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| **[[configuration]]\[Configuration\]** | . |  |  |  |  |
| scan | Флаг автоматической перезагрузки файла после изменения. | bool | O | R |  |
| scanPeriod | Период перезагрузки файла.pass:q[\<br\>]Примечание - Задается, если scan = true. | string | O | R |  |
| evaluator | Используемый фильтр при удовлетворении критериев. | object | O | R |  |
| property | Свойство родительского объекта.pass:q[\<br\>]Атрибуты: name, value, scope. | object | O | R |  |
| [[property]]\[Property\]                                                             |                                                                                                                                                                                                                                                              |          |     |     |        |  
| name | Название свойства. | string | O | R |  |
| value | Значение свойства. | string | O | R |  |
| scope | Область видимости свойства.pass:q[\<br\>] local/context/system. | string | O | R |  |
| appender | Аппендер; объект, ведущий записи событий.pass:q[\<br\>]Атрибуты: name, class, scope, filter, file, rollingPolicy, encoder. | object | M | R |  |
| [[appender]]\[Appender\] | . |  |  |  |  |
| name                                                                                            | Имя аппендера                                                                                                                                                                                                                                                | string   | M   | R   |        |  
| class                                                                                           | Класс аппендера.pass:q[\<br\>] ConsoleAppender (запись выводится в консольное окно); FileAppender (запись добавляется в конец файла); RollingFileAppender(запись добавляется в конец файла, файл обновляется по достижении определенного размера) | string   | M   | R   |        |  
| filter                                                                                          | Политика выбора необходимых логов                                                                                                                                                                                                                            | object   | O   | R   |        | 
| file                                                                                            | Имя файла. Примечание - Если файл не существует, то он генерируется автоматически                                                                                                                                                                            | string   | O   | R   |        |   
| rollingPolicy | Политика архивирования log-файлов при активации обновления.pass:q[\<br\>]Атрибуты: class, fileNamePattern, maxHistory, timeBasedFileNamingAndTriggeringPolicy,. |  |  |  |  |
| [[rollingPolicy]]\[RollingPolicy\]                                                   |                                                                                                                                                                                                                                                              |          |     |     |        |    |   
| class                                                                                           | Класс политики обновления файла                                                                                                                                                                                                                              | string   | O   | R   |        |  
| fileNamePattern                                                                                 | Шаблон имени log-файла. Содержит имя файла и спецификаторы изменения, определяющие типы выводимых данных                                                                                                                                                     | string   | O   | R   |        |  
| maxHistory                                                                                      | Максимальное время хранения файлов в архиве.pass:q[\<br\>]Примечание - Единицы времени задаются периодом обновления                                                                                                                                                   | int      | O   | R   |        |  
| timeBasedFileNamingAndTriggeringPolicy                                                          | Политика присвоения имен с привязкой ко времени и срабатывания триггеров, если log-файл достигает максимального размера до окончания времени хранения файла.pass:q[\<br\>]Атрибуты: class, maxFileSize                                                                | object   | O   | R   |        |  
| [[timeBasedFileNamingAndTriggeringPolicy]]\[TimeBasedFileNamingAndTriggeringPolicy\] |                                                                                                                                                                                                                                                              |          |     |     |        |    |   
| class                                                                                           | Класс политики присвоения имен с привязкой ко времени и срабатывания триггеров                                                                                                                                                                               | string   | O   | R   |        |  
| maxFileSize | Максимальный размер файла.pass:q[\<br\>]Формат:pass:q[\<br\>] maxFileSize="{value}{unit}". |  |  |  |  |
| string | O. | R |  |  |  |
| encoder | Политика обработки и вывода событий триггеров и логов. | object | M | R |  |
| logger | Логгер; объект, обрабатывающий события журналирования.pass:q[\<br\>]Атрибуты: name, level, additivity, appender-ref. | object | O | R |  |
| [[logger ]]\[Logger \] | . |  |  |  |  |
| name                                                                                            | Имя логгера                                                                                                                                                                                                                                                  | string   | O   | R   |        |  
| level                                                                                           | Наименьший уровень сообщений, записываемых в log-файл.pass:q[\<br\>] DEBUG/INFO/WARN/ERROR/FATAL                                                                                                                                                  | string   | O   | R   |        |  
| additivity                                                                                      | Флаг добавления логов в существующий аппендер                                                                                                                                                                                                                | bool     | O   | R   |        |  
| appender-ref                                                                                    | Имя аппендера, создающего записи в log-файла                                                                                                                                                                                                                 | [string] | O   | R   |        |  
| root | Параметры корневого логгера. | object | M | R |  |

#### Пример ####

```xml
<configuration>
  <appender name="STDOUT">
    <class>ch.qos.logback.core.ConsoleAppender</class>
    <encoder>
      <pattern>
        %d{dd.MM.yyyy HH:mm:ss.SSS} %-7level [%thread] %logger - %msg%n
      </pattern>
    </encoder>
  </appender>
  <appender name="FILE">
    <class>ch.qos.logback.core.rolling.RollingFileAppender</class>
    <file>logs/trace.log</file>
    <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
      <fileNamePattern>logs/arch/trace-%d{yyyy-MM-dd}.%i.gz</fileNamePattern>
      <maxHistory>365</maxHistory>
      <timeBasedFileNamingAndTriggeringPolicy>
        <class>ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP</class>
        <maxFileSize>400MB</maxFileSize>
        <maxHistory>3</maxHistory>
      </timeBasedFileNamingAndTriggeringPolicy>
    </rollingPolicy>
    <encoder>
      <pattern>
        %d{dd.MM.yyyy HH:mm:ss.SSS} %-7level [%thread] %logger - %msg%n
      </pattern>
    </encoder>
  </appender>
  <appender name="OPERATION_JOURNAL">
    <class>ch.qos.logback.core.rolling.RollingFileAppender</class>
    <file>cdr/operation_journal.cdr</file>
    <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
      <fileNamePattern>
        cdr/arch/operation_journal-%d{yyyy-MM-dd}.%i.gz
      </fileNamePattern>
      <maxHistory>365</maxHistory>
      <timeBasedFileNamingAndTriggeringPolicy>
        <class>ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP</class>
        <maxFileSize>400MB</maxFileSize>
      </timeBasedFileNamingAndTriggeringPolicy>
    </rollingPolicy>
    <encoder>
      <pattern>%msg%n</pattern>
    </encoder>
  </appender>
  <logger name="operation_journal" level="INFO">
    <appender-ref ref="OPERATION_JOURNAL"/>
  </logger>
  <root level="TRACE">
    <appender-ref ref="FILE" />
  </root>
</configuration>
```