
В файле задаются настройки взаимодействия с базой данных.

### Описание параметров ###

| Параметр | Описание | Тип | O/M | P/R | Версия |
|-------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|-----|-----|--------|
| **[[jdbc]]\[Jdbc\]** | . |  |  |  |  |
| name | Уникальное имя соединения с базой данных. | string | O | R |  |
| driver | Драйвер для работы с базой данных. | string | O | R |  |
| url | URL соединения с базой данных. | string | O | R |  |
| user | Логин для авторизации в базе данных. | string | O | R |  |
| password | Пароль для авторизации в базе данных. | string | O | R |  |
| initial-pool-size | Начальное количество соединений с базой данных.pass:q[\<br\>]По умолчанию: 0. | int | O | R |  |
| min-pool-size | Минимальное количество соединений с базой данных.pass:q[\<br\>]По умолчанию: 1. | int | O | R |  |
| max-pool-size | Максимальное количество соединений с базой данных.pass:q[\<br\>]По умолчанию: 10. | int | O | R |  |
| inactive-timeout | Максимальное время простоя до удаления соединения из пула, в секундах.pass:q[\<br\>]По умолчанию: 0 (функция отключена). | intpass:q[\<br\>]сек | O | R |  |
| connection-timeout | Максимальное время установления соединения с базой данных, в секундах.pass:q[\<br\>]По умолчанию: 0. | intpass:q[\<br\>]сек | O | R |  |
| validate-connection-on-borrow | Флаг проверки активности соединений.pass:q[\<br\>]По умолчанию: 0. | bool | O | R |  |
| sql-for-validate-connection | SQL-запрос для проверки активности соединения.pass:q[\<br\>]По умолчанию: 0.pass:q[\<br\>]Примечание - Для MySQL задано SELECT 1, для Oracle не задается. | string | M | R |  |
| liquibase-enabled | Флаг использования Liquibase.pass:q[\<br\>]По умолчанию: 0.pass:q[\<br\>]Примечание - Если значение true, при запуске используется файл liquibase/changelog.xml. | bool | O | R |  |
| statistic-period                                      | Период сбора статистики, в секундах.pass:q[\<br\>]По умолчанию: 0.pass:q[\<br\>]Примечание - Если не задан, статистика не собирается                                      | intpass:q[\<br\>]сек      | O   | R   |        | 
| detailed-statistic                                    | Флаг сбора подробной статистики.pass:q[\<br\>]По умолчанию: 0                                                                                                    | bool            | O   | R   |        |   
| statistic-slow-query-limit-ms                         | Минимальное время выполнения медленных запросов, в миллисекундах.pass:q[\<br\>]Примечание - Запросы, выполняемые дольше заданного значения, считаются медленными | intpass:q[\<br\>]миллисек | O   | R   |        |  
| primary                                               | Флаг использования соединения в качестве основного                                                                                                      | bool            | O   | R   |        | 
| alarm-enabled | Флаг отправления аварий. | bool | O | R |  |
| network-timeout | Максимальное время для отправки сетевого запроса к базе данных, в секундах.pass:q[\<br\>]По умолчанию: 600. | intpass:q[\<br\>]сек | O | R |  |
| connect-data-info | Дополнительные параметры соединения. | object | O | R |  |
| [[connect-data-info]]\[Сonnect-data-info\] |                                                                                                                                                         |                 |     |     |        |   
| min-results-to-switch                                 | Минимальное количество соединений с базой данных для поддержания ее в рабочем состоянии                                                                 | int             | O   | R   |        | 
| min-fail-percentage-to-switch | Максимальная доля ошибок подключений к базе данных к общему количеству попыток, чтобы база данных считалась активной.pass:q[\<br\>]Диапазон: 0-1. | int | O | R |  |
| check-interval-sec                                    | Период проверки доступности баз данных в порядке убывания их приоритетов, в секундах                                                                    | intpass:q[\<br\>]сек      | O   | R   |        | 
| window-sec                                            | Период подсчета общего количества соединений, в секундах                                                                                                | intpass:q[\<br\>]сек      | O   | R   |        | 
| always-try-inactive-connection                        | Флаг переподключения к базе данных, если все базы не активны.pass:q[\<br\>]По умолчанию: 1                                                                       | bool            | O   | R   |        | 
| connect-data                                          | Данные соединения.pass:q[\<br\>]Атрибуты: url, user, password, stay, properties                                                                                  | object          | O   | R   |        | 
| [[connect-data]]\[Сonnect-data\] | . |  |  |  |  |
| url | URL соединения с базой данных. | string | M | R |  |
| user                                                  | Логин для авторизации в базе данных                                                                                                                     | string          | O   | R   |        |  
| password                                              | Пароль для авторизации в базе данных                                                                                                                    | string          | O   | R   |        |  
| stay                                                  | Флаг запрета смены базы данных                                                                                                                          | bool            | O   | R   |        | 
| properties                                            | Параметры соединения.pass:q[\<br\>]Атрибуты: property                                                                                                            | [object]        | O   | R   |        | 
| [[properties]]\[Properties\] | . |  |  |  |  |
| property                                              | Параметры свойства соединения.pass:q[\<br\>]Атрибуты: name, value                                                                                                | [object]        | O   | R   |        | 
| [[property ]]\[Property \] | . |  |  |  |  |
| name                                                  | Имя свойства соединения                                                                                                                                 | string          | O   | R   |        |  
| value                                                 | Значение свойства соединения                                                                                                                            | string          | O   | R   |        |  
      

Примечание - Если значения URL, логина и пароля заданы только для одного подключения, система не осуществляет переход к другой базе данных.
Для использования нескольких баз данных необходимо задать несколько наборов параметров подключений (connect-data). Система осуществляет переход к базам данных по порядку.


#### Пример ####

```xml
<jdbc>
  <connection name="connName" driver="oracle.jdbc.driver.OracleDriver"
  url="jdbc:oracle:thin:@192.168.0.1:1520:ORCLTEST" user="loginAuth"
  password="passAuth" initial-pool-size="0" min-pool-size="1"
  max-pool-size="10" inactive-timeout="0" connection-timeout="0"
  validate-connection-on-borrow="false" sql-for-validate-connection="1" 
  liquibase-enabled="false" statistic-period="600" detailed-statistic="true" 
  statistic-slow-query-limit-ms="15000" primary="true" alarm-enabled="true" 
  network-timeout="600">
    <connect-data-info min-results-to-switch="10" 
    min-fail-percentage-to-switch="0.2" check-interval-sec="10" window-sec="60"
    always-try-inactive-connection="true">
      <connect-data url="jdbc:mysql://192.168.0.0:1433/DPDP" user="root"
      password="pass" stay="true">
      </connect-data>
    </connect-data-info>
  </connection>
</jdbc>
```