Файл содержит параметры профиля абонента. Файл с параметрами профилей расположен в директории /usr/protei/Protei_HLR_API/config и имеет название profiles.json

Изменения в файле считываются автоматически, результат считывания заносится в config.log.

Новые профили абонентов можно создавать и использовать в режиме реального времени.

Для создания нового профиля абонента используется команда cat.

== Запрос

[source,bash]
----
$ ./cat [directory]
[
  {
    "Profile_id": <int>,
    "SCA": <int>,
    "O_CSI": <int>,
    "T_CSI": <int>,
    "SMS_CSI": <int>,
    "USSD_CSI": <int>,
    "M_CSI": <int>,
    "GPRS_CSI": <int>,
    "D_CSI": <int>,
    "O_IM_CSI": <int>,
    "VT_IM_CSI": <int>,
    "PDP_DATA": [<list>],
    "TeleServices": [<list>],
    "Active_SS": [<list>],
    "NotActive_SS": [<list>],
    "SS_TeleServices": [<list>],
    "DefaultForwardingNumber": "<string>",
    "DefaultForwardingStatus": "<string>",
    "Forwarding": "<string>",
    "WL_DATA": [<list>],
    "BL_DATA": [<list>],
    "TK_ID": <int>,
    "AuCC_ID": <int>,
    "AuCR_ID": <int>,
    "EPS_DATA": {<object>},
    EPS_CONTEXTS": [<list>],
    "LCS_ID": <int>,
    "Group_ID": <int>,
    "deacticvatePsi": <bool>,
    "baocWithoutCamel": <bool>,     
    "qosGprsId": <int>,
    "qosEpsId": <int>,
    "roamingNotAllowed": <bool>,  
    "accessRestrictionData": <object>, 
    "amfUmts": "<string>",
    "amfLte": "<string>",
    "amfIms": "<string>"
  }
]
----

=== Поля запроса

[width="99%",cols="19%,72%,6%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|Profile_id |Идентификатор профиля. |int |M
|SCA |Адрес сервисного центра (см. 3GPP TS 23.040). |string |O
|O_CSI |Профиль O-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|T_CSI |Профиль T-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|SMS_CSI |Профиль SMS-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|USSD_CSI |Профиль USSD-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|M_CSI |Профиль M-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|GPRS_CSI |Профиль GPRS-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|D_CSI |Профиль D-CSI (см. 3GPP TS 23.008, 3GPP TS 23.078). |int |O
|O_IM_CSI |Профиль O_IM_CSI (см. (3GPP TS 23.278). |int |O
|VT_IM_CSI |Профиль VT_IM_CSI (см. (3GPP TS 23.278). |int |O
|D_IM_CSI |Профиль D_IM_CSI (см. (3GPP TS 23.278). |int |O
|PDP_DATA |Профиль PDP (см. (3GPP TS 23.278). См. [pdpData] (../API/provisioning/entities/pdpData/). |[object] |O
|TeleServices |Перечень основных телесервисов (см. 3GPP TS 22.003). |[int] |O
|Active_SS |Активные дополнительные услуги SS (см. 3GPP TS 22.003). |[int] |O
|NotActive_SS |Неактивные дополнительные услуги SS (см. 3GPP TS 22.003). |[int] |O
|Forced_SS |Принудительные дополнительные услуги SS (см. 3GPP TS 22.003). |[int] |O
|SS_TeleServices |Коды телесервисов, на базе которых работают дополнительные сервисы. |[int] |O
|DefaultForwardingNumber |Номер переадресации по умолчанию. |string |O
|DefaultForwardingStatus |Статус переадресации по умолчанию. |string |O
|Forwarding |Параметры переадресации. |string |O
|WL_DATA |Привязываемые белые списки. |[int] |O
|BL_DATA |Привязываемые черные списки. |[int] |O
|TK_ID |Идентификатор транспортного ключа. |int |O
|OP_ID |Идентификатор операторской константы. |int |O
|AuCC_ID |Идентификатор параметра C. |int |O
|AuCR_ID |Идентификатор параметра R. |int |O
|AuCR_ID |Идентификатор параметра R. |int |O
|EPS_DATA |Параметры EPS. См. link:../API/provisioning/entities/epsData/[epsData]. |object |O
|EPS_CONTEXTS |Параметры взаимодействия с контекстом EPS. См. link:../API/provisioning/entities/epsLink/[epsLink]. |[object] |O
|LCS_ID |Идентификатор профиля LCS. |int |O
|Group_ID |Идентификатор группы абонентов. |int |O
|deacticvatePsi |Отправка сообщений PSI в результате процедуры SRI. |bool |O
|baocWithoutCamel |Запрет исходящих вызовов, если версия CAMEL у абонента не совпадает с версией CAMEL на Protei HLR/HSS. |bool |O
|qosGprsId |Идентификатор профиля GPRS QoS. |int |O
|qosEpsId |Идентификатор профиля EPS QoS. |int |O
|roamingNotAllowed |Флаг запрета роуминга. |bool |O
|accessRestrictionData |Ограничения доступа. См. link:../API/provisioning/entities/accessRestrictionData/[accessRestrictionData]. |object |O
|amfUmts |Поле управления аутентификацией в сетях UMTS (см. 3GPP TS 33.102). |string |O
|amfLte |Поле управления аутентификацией в сетях LTE. |string |O
|amfIms |Поле управления аутентификацией в сетях IMS. |string |O
|===

=== Пример тела запроса

[source,ini]
----
$ ./cat /usr/protei/Protei_HLR_API/config/profiles.json
[
  {
    "Profile_id": 1,
    "TeleServices": [ 17, 18, 33, 34 ],
    "Active_SS": [ 17, 65, 66, 81 ],
    "NotActive_SS": [ 33, 41, 42, 43, 146, 154 ],
    "SS_TeleServices": [ 16 ],
    "PDP_DATA": [
      {
        "context-id": 1,
        "type": "f121"
      }
    ],
    "EPS_CONTEXTS": [
      { "context-id": 1 }
    ],
    "EPS_DATA": {
     "ueMaxDl": 100000000,
     "ueMaxUl": 100000000,
     "ratType": 1004,
     "defContextId": 1
    },
    "qosGprsId": 1,
    "qosEpsId": 1
  }
]
----
