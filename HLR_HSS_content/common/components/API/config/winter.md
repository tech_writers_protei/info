
В файле задаются следующие основные части:

* параметры ядра Core;

* параметры Alarm Processor;

* параметры HTTP-подключений;

* параметры HTTP-подключений.

### Описание параметров Alarm Processor ###

| Параметр                             | Описание                                                                                                                       | Тип             | O/M | P/R | Версия |
|--------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|-----------------|-----|-----|--------|
| alarm.app_name                       | Имя приложения, используемое как дополнительная переменная alarm.snmp.app_var_name в трапах.                                   | string          | O   | R   |        |
| alarm.http.batch.min.size            | Минимальное количество запросов для обработки.pass:q[\<br\>]По умолчанию: 10.                                                            | int             | O   | R   |        |
| alarm.http.batch.max.size            | Максимальное количество запросов для обработки.pass:q[\<br\>]По умолчанию: 50.                                                           | int             | O   | R   |        |
| alarm.http.batch.max.handle.interval | Период обработки набора запросов.pass:q[\<br\>]По умолчанию: 10000.                                                                      | intpass:q[\<br\>]сек      | O   | R   |        |
| alarm.http.trap.host                 | Хост HTTP для отправки траповой переменной.                                                                                    | string          | O   | R   |    
| alarm.http.trap.port                 | Порт HTTP отправителя.                                                                                                         | int             | O   | R   |   
| alarm.http.trap.url                  | URL для отправки трапов.                                                                                                       | string          | O   | R   |  
| alarm.snmp.listen_port               | Прослушиваемый порт для запросов SNMP-get/get-next.                                                                            | int             | O   | R   |  
| alarm.snmp.max_retries               | Максимальное количество попыток отправить ответное SNMP-сообщение.pass:q[\<br\>]По умолчанию: 3.                                         | int             | O   | R   |  
| alarm.snmp.retries_timeout           | Время ожидания между попытками отправить ответное SNMP-сообщение.pass:q[\<br\>]По умолчанию: 1000.                                       | intpass:q[\<br\>]миллисек | O   | R   |  
| alarm.mib.files                      | Перечень MIB-файлов (псевдо-MIB) в формате *.properties.                                                                       | [object]        | O   | R   |  
| alarm.snmp.version                   | Версия SNMP.                                                                                                                   | int             | O   | R   |        | 
| alarm.snmp.trap_host                 | Хост сервера для отправки трапов.                                                                                              | string          | O   | R   |        | 
| alarm.snmp.trap_port                 | Порт сервера для отправки трапов.pass:q[\<br\>]Примечание - Обязательный, если задано значение alarm.snmp.trap_host.                     | int             | O   | R   |        | 
| alarm.snmp.trap_addresses            | IP-адреса серверов для отправки трапов.                                                                                        | [string]        | O   | R   |        | 
| alarm.snmp.app_var_name              | Название переменной в MIB-файле properties для имени приложения.pass:q[\<br\>]Примечание - Значение берется из параметра alarm.app_name. | string          | O   | R   |        | 

**Примечания.**
Для отправки трапов по протоколу HTTP необходимо задать значения host, port и url.
Обязательно задать значение или alarm.snmp.trap_addresses, или alarm.snmp.trap_host и alarm.snmp.trap_port.


### Описание параметров ядра Core ###

| Параметр                               | Описание                                                                                                                      | Тип      | O/M | P/R | Версия |
|----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------|----------|-----|-----|--------|
| core.executor_core_thread_count        | Количество рабочих потоков в приложении.pass:q[\<br\>]По умолчанию: 4.                                                                  | int      | O   | R   |        |
| core.executor_max_thread_count         | Максимальное количество рабочих потоков в приложении.pass:q[\<br\>]По умолчанию: 4.                                                     | int      | O   | R   |        |
| core.executor_queue_capacity           | Размер очереди запросов.pass:q[\<br\>]По умолчанию: 10.                                                                                 | int      | O   | R   |        |
| core.scheduler_thread_count            | Количество потоков планировщика Spring.pass:q[\<br\>]По умолчанию: 2.                                                                   | int      | O   | R   |        |
| core.xstream.escape_char_replacement   | Символ для замены "_" в тегах и названиях атрибутов после конвертации XStream.pass:q[\<br\>]По умолчанию: __.                           | string   | O   | R   |        |
| core.xstream.additional_date_formats   | Дополнительные XML-форматы данных.pass:q[\<br\>]По умолчанию: " ".pass:q[\<br\>]Пример: yyyy-MM-dd HH:mm:ss X,yyyy-MM-dd HH:mm:ss.SSS X.          | [string] | O   | R   |        |
| core.use_jaxb                          | Флаг использования парсера JAXB вместо XStream.pass:q[\<br\>]По умолчанию: 0.                                                           | bool     | O   | R   |        |
| core.json_wrap_root_value              | Флаг вывода содержимого корневого объекта вместо его полного значения.pass:q[\<br\>]По умолчанию: 0.                                    | bool     | O   | R   |        |
| core.json_write_nulls                  | Флаг вывода параметров без заданных значений.pass:q[\<br\>]По умолчанию: 0.                                                             | bool     | O   | R   |        |
| core.json_std_date_format              | Флаг приведения дат к формату стандарта ISO-8601.pass:q[\<br\>]По умолчанию: 0.                                                         | bool     | O   | R   |        |
| core.executor_statistic_reset_interval | Период обнуления счетчиков выполненных или принятых задач (используется для статистики TaskExecutor)..pass:q[\<br\>]По умолчанию: 120s. | duration | O   | R   |        |
| core.executor_max_thread_work_time     | Максимальное время работы потока (используется для статистики TaskExecutor).pass:q[\<br\>]По умолчанию: 60s.                            | duration | O   | R   |        |

Алгоритм работы очереди:


* если количество занятых потоков меньше, чем значение core.executor_core_thread_count, куча очереди пуста;

* если количество занятых потоков превышает указанное значение, куча очереди начинается заполняться;

* если куча очереди заполнена, ее размер равен значению core.executor_max_thread_count, новые запросы не обрабатываются, пока какой-либо запрос не будет обработан или отменен;

* размер очереди должен превышать значение max_thread_count.

### Описание параметров HTTP-соединения ###

| Параметр                                     | Описание                                                                                                  | Тип        | O/M | P/R | Версия |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| http.max_client_connection                   | Максимальное количество одновременных HTTP-подключений к серверу.pass:q[\<br\>]По умолчанию: 2.                     | int        | O   | R   |        |
| http.max_client_connection_inactive_interval | Максимальное время простоя клиентского HTTP-соединения.pass:q[\<br\>]По умолчанию: 5.                               | intpass:q[\<br\>]сек | O   | R   |        |
| http.client_timeout                          | Время ожидания ответа сервера.                                                                            | intpass:q[\<br\>]сек | O   | R   |        |
| http.max_cached_time                         | Максимальное время хранения кэша статического контента.                                                   | intpass:q[\<br\>]сек | O   | R   |        |
| http.ignore_logging_urls                     | Адреса в запросах, не записываемые в подробный log-файл.                                                  | string     | O   | R   |        |
| http.static_content_folder                   | Путь к директории, в которой находится статический контент.pass:q[\<br\>]По умолчанию: ./static.                    | string     | O   | R   |        |
| http.use_jaxb                                | Флаг использования JAXB для конвертации XML.pass:q[\<br\>]По умолчанию: 0.                                          | bool       | O   | R   |        |
| http.auth_white_address_masks                | Маски разрешенных IP-адресов.pass:q[\<br\>]Пример: localhost,192.168.0.0/24.                                        | [string]   | O   | R   |        |
| http.port                                    | Прослушиваемый порт основного HTTP-сервера                                                                | int        | O   | R   |    
| http.host                                    | IP-адрес основного HTTP-сервера                                                                           | string     | O   | R   |  
| http.max_thread_count                        | Количество потоков, обрабатывающих ввод/вывод основного HTTP-сервера.pass:q[\<br\>]По умолчанию: 2                  | int        | O   | R   |    
| http.max_init_interval                       | Максимальное время ожидания первого запроса для подключения к основному HTTP-серверу.pass:q[\<br\>]По умолчанию: 60 | intpass:q[\<br\>]сек | O   | R   |    
| http.max_inactive_interval                   | Максимальное время простоя в сессии между запросами для основного HTTP-сервера.pass:q[\<br\>]По умолчанию: 120      | intpass:q[\<br\>]сек | O   | R   |   
| http.permitted_address_mask                  | Белый перечень адресов для подключения к HTTP-серверу.pass:q[\<br\>]По умолчанию: 120                               | [string]   | O   | R   |   
| http.auto_start                              | Флаг автозапуска основного HTTP-сервера.pass:q[\<br\>]По умолчанию: 0                                               | bool       | O   | R   | 
| http.max_receive_body_length                 | Максимальная длина тела входящего запроса.pass:q[\<br\>]По умолчанию: без ограничений                               | duration   | O   | R   |

### Описание параметров HTTPS-соединения ###

| Параметр                           | Описание                                                                                               | Тип                | O/M | P/R | Версия |
|------------------------------------|--------------------------------------------------------------------------------------------------------|--------------------|-----|-----|--------|
| secure.algorithm                   | Алгоритм шифрования.pass:q[\<br\>]По умолчанию: SunX509.                                                         | string             | O   | R   |        |
| secure.keystore.filename           | Путь до KeyStore.                                                                                      | string             | O   | R   |        |
| secure.keystore.password           | Пароль KeyStore.                                                                                       | string             | O   | R   |        |
| secure.truststore.filename         | Путь до TrustStore.                                                                                    | string             | O   | R   |        |
| secure.truststore.password         | Пароль TrustStore.                                                                                     | string             | O   | R   |        |
| https.port                         | Прослушиваемый порт основного HTTPS-сервера.                                                           | int                | O   | R   |        |
| https.host                         | Адрес основного HTTPS-сервера.                                                                         | string             | O   | R   |        |
| https.max_thread_count             | Количество потоков, обрабатывающих ввод/вывод основного HTTPS-сервера.pass:q[\<br\>]По умолчанию: 2.             | int                | O   | R   |        |
| https.max_init_interval            | Максимальное время ожидания для основного HTTPS-сервера.pass:q[\<br\>]По умолчанию: 60.                          | intpass:q[\<br\>]сек         | O   | R   |        |
| https.max_inactive_interval        | Максимальное время простоя в сессии между запросами для основного HTTPS-сервера.pass:q[\<br\>]По умолчанию: 120. | intpass:q[\<br\>]сек         | O   | R   |        |
| https.permitted_address_mask       | Белый перечень адресов для подключения к HTTPS-серверу.pass:q[\<br\>]По умолчанию: 120.                          | [string]/ip/regex. | O   | R   |        |
| https.auto_start                   | Флаг автозапуска основного HTTPS-серверау.pass:q[\<br\>]По умолчанию: 0.                                         | bool               | O   | R   |        |
| https.certificate.alias            | Альтернативное имя сертификата HTTPS-сервера в TrustStore.                                             | string             | M   | R   |        |
| https.certificate.password         | Пароль сертификата HTTPS-сервера.                                                                      | string             | M   | R   |        |
| https.client.auth                  | Флаг аутентификации сертификата клиента в TrustStore для HTTPS-сервера.pass:q[\<br\>]По умолчанию: 0.            | bool               | O   | R   |        |
| https.client.certificate.allow_DNs | Разрешенные строковые представления информации из клиентских сертификатов на HTTPS-сервере.            | [string]           | O   | R   |        |
| https.auth_white_address_masks     | Маски разрешенных IP-адресов.pass:q[\<br\>]Пример: localhost,192.168.0.0/24.                                     | [string]           | O   | R   |        |

Примечание - Запуск HTTPS-сервера возможен только при задании параметров secure.KeyStore.
Если secure.TrustStore.* отсутствует для доверенного сертификата, то используется KeyStore.

#### Пример ####

```ini
alarm.app_name=hlr_api
alarm.http.batch.min.size=10
alarm.http.batch.max.size=50
alarm.http.batch.max.handle.interval=10000
alarm.http.trap.host=192.168.0.0
alarm.http.trap.port=8080
alarm.http.trap.url=http://trap_url/
alarm.snmp.listen_port=161
alarm.snmp.max_retries=3
alarm.snmp.retries_timeout=1000
alarm.mib.files=*.properties
alarm.snmp.version=1
alarm.snmp.trap_host=127.0.0.1 
alarm.snmp.trap_port=162
alarm.snmp.trap_addresses=127.0.0.1:162,localhost:163

core.executor_core_thread_count=4
core.executor_max_thread_count=4
core.executor_queue_capacity=10
core.scheduler_thread_count=2
core.xstream.escape_char_replacement="__"
core.xstream.additional_date_formats=""
core.use_jaxb=false
core.json_wrap_root_value=false
core.json_write_nulls=false
core.json_std_date_format=false
core.executor_statistic_reset_interval=120s
core.executor_max_thread_work_time=60s

http.max_client_connection=2
http.max_client_connection_inactive_interval=5
http.max_cached_time=600
http.ignore_logging_urls="127.0.0.1"
http.static_content_folder="./static"
http.use_jaxb=false
http.auth_white_address_masks=<string> 
http.port=8000
http.host="127.0.0.1"
http.max_thread_count=2
http.max_init_interval=60
http.max_inactive_interval=120
http.permitted_address_masks="127.0.0.1"
http.auto_start=false
http.max_receive_body_length=4mb

secure.algorithm="SunX509"
secure.keystore.filename="keystore_filename"
secure.keystore.password="key_store_password"
secure.truststore.filename="truststore_filename"
secure.truststore.password="trust_store_password"
https.port=8080
https.host="127.0.0.1"
https.max_thread_count=0
https.max_init_interval=60
https.max_inactive_interval=120
https.permitted_address_masks="127.0.0.1,localhost"
https.auto_start=false
https.certificate.alias="certAlias"
https.certificate.password="certPassword"
https.client.auth=true
https.client.certificate.allow_DNs="cn=Name,ou=OrgUnit,o=Company,l=Spb,st=S,
c=ru;cn=Name2,ou=OrgUnit2,o=Company2,l=Spb,st=S,c=ru"
https.auth_white_address_masks="127.0.0.1,localhost"
```