
В файле задаются настройки протокола OMI.

### Описание параметров ###

| Параметр                              | Описание                                                                                | Тип    | O/M | P/R | Версия |
|---------------------------------------|-----------------------------------------------------------------------------------------|--------|-----|-----|--------|
| **[[omi]]\[OMI\]**         |                                                                                         |        |     |     |        |
| server                                | Параметры сервера.pass:q[\<br\>]Атрибуты: port, login, password, auto-start, max-thread-count.    | object | O   | R   |        |
| [[server]]\[Server\]       |                                                                                         |        |     |     |        |
| port                                  | Прослушиваемый порт сервера.                                                            | int    | M   | R   |        |
| login                                 | Логин для авторизации на сервере.                                                       | string | O   | R   |        |
| password                              | Пароль для авторизации на сервере.                                                      | string | O   | R   |        |
| auto-start                            | Флаг автозапуска.pass:q[\<br\>]По умолчанию: 0.                                                   | bool   | O   | R   |        |
| max-thread-count                      | Максимальное количество обрабатывающих потоков.pass:q[\<br\>]По умолчанию: 1.                     | int    | O   | R   |        |
| client                                | Параметры клиента.pass:q[\<br\>]Атрибуты: id, host, port, login, password,  max-response-timeout. | object | O   | R   |        |
| [[client]]\[Client\]       |                                                                                         |        |     |     |        |
| id                                    | Идентификатор клиента OMI.                                                              | string | O   | R   |        |
| host                                  | Адрес хоста порта.pass:q[\<br\>]По умолчанию: localhost.                                          | string | O   | R   |        |
| port                                  | Порт хоста сервера.                                                                     | int    | O   | R   |        |
| login                                 | Логин для авторизации на сервере.                                                       | string | O   | R   |        |
| password                              | Пароль для авторизации на сервере.                                                      | string | O   | R   |        |
| max-response-timeout                  | Время ожидания ответа, в секундах.pass:q[\<br\>]По умолчанию: 30.                                 | int    | O   | R   |        |
| direction                             | Параметры направления.pass:q[\<br\>]Атрибуты: id, primary-client, secondary-client.               | object | O   | R   |        |
| [[direction]]\[Direction\] |                                                                                         |        |     |     |        |
| id                                    | Идентификатор направления OMI.                                                          | string | M   | R   |        |
| primary-client                        | Идентификатор основного клиента.                                                        | string | M   | R   |        |
| secondary-client                      | Идентификатор резервного клиента.                                                       | string | M   | R   |        |

#### Пример ####

```xml
<omi>
  <client id="hlr1" host="192.168.115.233" port="4475" login="hlr_api"
  password="hlr_api" />
  <direction id="hlr">
    <primary-client>hlr1</primary-client>
  </direction>
</omi>
```