
В файле задаются настройки mib properties для узла HLR.

### Описание параметров ###

| Параметр | Описание | Полный адрес траповой переменной |
|---------------------------|------------------------------------------------------------------------------------------------------------------------|----------------------------------|
| protei_hlr_api | Корневой адрес PROTEI HLR/HSS API.pass:q[\<br\>]Пример: 1.3.6.1.4.1.20873.5387. | 1.3.6.1.4.1.20873.146 |
| trap | Тип траповой переменной.pass:q[\<br\>]Пример: protei_hlr_api.0. | 1.3.6.1.4.1.20873.146.0 |
| start_trap | Запуск PROTEI HLR/HSS API.pass:q[\<br\>]Пример: trap.0. | 1.3.6.1.4.1.20873.146.0.0 |
| stop_trap | Остановка PROTEI HLR/HSS API.pass:q[\<br\>]Пример: trap.1. | 1.3.6.1.4.1.20873.146.0.1 |
| jdbc_connection_trap | Подключение PROTEI HLR/HSS API к базе данных.pass:q[\<br\>]Пример: trap.2. | 1.3.6.1.4.1.20873.146.0.2 |
| winter_congestion_trap | Уровень нагрузки Congestion на PROTEI HLR/HSS API.pass:q[\<br\>]Пример: trap.3. | 1.3.6.1.4.1.20873.146.0.3 |
| value | Параметр.pass:q[\<br\>]Пример: protei_hlr_api.1. | 1.3.6.1.4.1.20873.146.1 |
| application_status | Текущее состояние приложения.pass:q[\<br\>]Пример: value.0. | 1.3.6.1.4.1.20873.146.1.0 |
| core_curr_thread_count | Текущее количество занятых потоков.pass:q[\<br\>]Пример: value.1. | 1.3.6.1.4.1.20873.146.1.1 |
| core_curr_queue_size | Текущий размер очереди задач.pass:q[\<br\>]Пример: value.2. | 1.3.6.1.4.1.20873.146.1.2 |
| core_max_thread_count | Максимальное количество рабочих потоков.pass:q[\<br\>]Пример: value.3. | 1.3.6.1.4.1.20873.146.1.3 |
| core_max_queue_size | Максимальный размер очереди задач.pass:q[\<br\>]Пример: value.4. | 1.3.6.1.4.1.20873.146.1.4 |
| core_hanged_thread_count | Текущее количество зависших потоков.pass:q[\<br\>]Пример: value.5. | 1.3.6.1.4.1.20873.146.1.5 |
| core_accepted_task_count | Количество принятых задач за период сбора статистики.pass:q[\<br\>]Пример: value.6. | 1.3.6.1.4.1.20873.146.1.6 |
| core_completed_task_count | Количество принятых задач за период сбора статистики.pass:q[\<br\>]Пример: value.7. | 1.3.6.1.4.1.20873.146.1.7 |
| jdbc_connection_id | Идентификатор соединения с базой данных.pass:q[\<br\>]Пример: value.8. | 1.3.6.1.4.1.20873.146.1.8 |
| jdbc_connection_name      | Название соединения с базой данных.pass:q[\<br\>]Пример: value.9                                                                | 1.3.6.1.4.1.20873.146.1.9        |     
| jdbc_connection_status | Состояние соединения с базой данных.pass:q[\<br\>]Пример: value.10. | 1.3.6.1.4.1.20873.146.1.10 |
| congestion_status | Состояние подсистемы нагрузки Congestion.pass:q[\<br\>]Normal, Minor, Major, Critical.pass:q[\<br\>]Пример: value.11. | 1.3.6.1.4.1.20873.146.1.11 |

#### Пример ####

```ini
#common
protei_hlr_api=1.3.6.1.4.1.20873.146
#trap
trap=protei_hlr_api.0
start_trap=trap.0
stop_trap=trap.1
jdbc_connection_trap=trap.2
winter_congestion_trap=trap.3
#value
value=protei_hlr_api.1
application_status=value.0
core_curr_thread_count=value.1
core_curr_queue_size=value.2
core_max_thread_count=value.3
core_max_queue_size=value.4
core_hanged_thread_count=value.5
core_accepted_task_count=value.6
core_completed_task_count=value.7
jdbc_connection_id=value.8
jdbc_connection_name=value.9
jdbc_connection_status=value.10
congestion_status = value.11
```