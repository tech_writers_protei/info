
{{<mermaid>}}
sequenceDiagram

participant G as GMSC
    participant H as HLR
    participant V as VLR/MSC
    autonumber

opt Входящий вызов по абоненту c CAMEL профилем
    G ->> H:  MAP-SEND-ROUTING-INFORMATION (22)
    Note right of G: SCCP:  CgPA: GT GMSC, CdPA: E164.MSISDN<br/>TCAP: begin: otid: TID1<br/>MAP: MSISDN, gmsc-Address, supportedCamelPhases
    H ->> V: MAP-PROVIDE-SUBSCRIBER-INFO (70)
    Note right of H: SCCP:  CgPA: GT HLR, CdPA: VLR<br/>TCAP: begin: otid: TID2<br/>MAP: IMSI, locationInformation, subscriberState
    V ->> H: MAP-PROVIDE-SUBSCRIBER-INFO (70) Response
    Note left of V: SCCP:  CgPA: GT VLR, CdPA: HLR<br/>TCAP: end: dtid: TID2<br/>MAP: vlr-Number, msc-Number, subscriberState
    H ->> G: MAP-SEND-ROUTING-INFORMATION (22) Response
    Note left of H: SCCP:  CgPA: GT HLR, CdPA: GMSC<br/>TCAP: end: dtid: TID1<br/>MAP: IMSI, O-CSI, T-CSI, vlr-Number, msc-Number, subscriberState, PLMN-specificSS
    G ->> H:  MAP-SEND-ROUTING-INFORMATION (22)
    Note right of G: SCCP:  CgPA: GT GMSC, CdPA: E164.MSISDN<br/>TCAP: begin: otid: TID3<br/>MAP: MSISDN, gmsc-Address, supportedCamelPhases,pass:q[\<br\>]suppress-T-CSI
    H ->> V:  MAP-PROVIDE-ROAMING-NUMBER (4)
    Note right of H: SCCP:  CgPA: GT HLR, CdPA: VLR<br/>TCAP: begin: otid: TID4<br/>MAP: IMSI, MSISDN, gmsc-Address
    V ->> H: MAP-PROVIDE-ROAMING-NUMBER (4) Response
    Note left of V: SCCP:  CgPA: GT VLR, CdPA: HLR<br/>TCAP: end: dtid: TID4<br/>MAP: RoamingNumber
    H ->> G: MAP-SEND-ROUTING-INFORMATION (22) Response
    Note left of H: SCCP:  CgPA: GT HLR, CdPA: GMSC<br/>TCAP: end: dtid: TID3<br/>MAP: IMSI, RoamingNumber, PLMN-specificSS
end

opt Входящий вызов по абоненту без CAMEL профиля
    G ->> H:  MAP-SEND-ROUTING-INFORMATION (22)
    Note right of G: SCCP:  CgPA: GT GMSC, CdPA: E164.MSISDN<br/>TCAP: begin: otid: TID1<br/>MAP: MSISDN, gmsc-Address, supportedCamelPhases
    H ->> V:  MAP-PROVIDE-ROAMING-NUMBER (4)
    Note right of H: SCCP:  CgPA: GT HLR, CdPA: VLR<br/>TCAP: begin: otid: TID2<br/>MAP: IMSI, MSISDN, gmsc-Address
    V ->> H: MAP-PROVIDE-ROAMING-NUMBER (4) Response
    Note left of V: SCCP:  CgPA: GT VLR, CdPA: HLR<br/>TCAP: end: dtid: TID3<br/>MAP: RoamingNumber
    H ->> G: MAP-SEND-ROUTING-INFORMATION (22) Response
    Note left of H: SCCP:  CgPA: GT HLR, CdPA: GMSC<br/>TCAP: end: dtid: TID4<br/>MAP: IMSI, RoamingNumber, PLMN-specificSS
end

opt Входящий вызов на неизвестного абонента (режим PseudoSRI)
    G ->> H:  MAP-SEND-ROUTING-INFORMATION (22)
    Note right of G: SCCP:  CgPA: GT GMSC, CdPA: E164.MSISDN<br/>TCAP: begin: otid: TID1<br/>MAP: MSISDN, gmsc-Address, supportedCamelPhases
    H ->> G: MAP-SEND-ROUTING-INFORMATION (22) Response
    Note left of H: SCCP:  CgPA: GT HLR, CdPA: GMSC<br/>TCAP: end: dtid: TID2<br/>MAP: RoamingNumber = MsrnPreffix(hlr.cfg) + MSISDN(international),pass:q[\<br\>]IMSI = SriImsi(hlr.cfg)
end

opt Входящий вызов на неизвестного абонента
    G ->> H:  MAP-SEND-ROUTING-INFORMATION (22)
    Note right of G: SCCP:  CgPA: GT GMSC, CdPA: E164.MSISDN<br/>TCAP: begin: otid: TID1<br/>MAP: MSISDN, gmsc-Address, supportedCamelPhases
    H ->> G: MAP-SEND-ROUTING-INFORMATION (22) Error
    Note left of H: SCCP:  CgPA: GT HLR, CdPA: GMSC<br/>TCAP: end: dtid: TID2<br/>MAP: ErrorCode = MAP_AbsentSubscriber (27)
end
{{</mermaid>}}


### PLMN specific supplementary services

Дополнительные услуги PLMN-specificSS c ssCode 240-255 передаются в ответе на запрос MAP_SEND_ROUTING_INFORMATION в SS-List.

PLMN-specificSS в SS-List в рамках услуги MAP-INSERT-SUBSCRIBER-DATA **не попадают**.

```
allPLMN-specificSS SS-Code ::= '11110000'B ::= 240
plmn-specificSS-1 SS-Code  ::= '11110001'B ::= 241
plmn-specificSS-2 SS-Code  ::= '11110010'B ::= 242
plmn-specificSS-3 SS-Code  ::= '11110011'B ::= 243
plmn-specificSS-4 SS-Code  ::= '11110100'B ::= 244
plmn-specificSS-5 SS-Code  ::= '11110101'B ::= 245
plmn-specificSS-6 SS-Code  ::= '11110110'B ::= 246
plmn-specificSS-7 SS-Code  ::= '11110111'B ::= 247
plmn-specificSS-8 SS-Code  ::= '11111000'B ::= 248
plmn-specificSS-9 SS-Code  ::= '11111001'B ::= 249
plmn-specificSS-A SS-Code  ::= '11111010'B ::= 250
plmn-specificSS-B SS-Code  ::= '11111011'B ::= 251
plmn-specificSS-C SS-Code  ::= '11111100'B ::= 252
plmn-specificSS-D SS-Code  ::= '11111101'B ::= 253
plmn-specificSS-E SS-Code  ::= '11111110'B ::= 254
plmn-specificSS-F SS-Code  ::= '11111111'B ::= 255
```