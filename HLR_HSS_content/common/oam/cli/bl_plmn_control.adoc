Команда `bl_plmn_control` позволяет добавить маску PLMN в черный перечень или удалить маску PLMN из черного списка.

== Запрос

[source,bash]
----
$ ./bl_plmn_control [name=%value%] [action=%value%] [PLMN_mask=%value%] [country=%value%] [network=%value%] 
----

=== Поля запроса

[width="100%",cols="16%,76%,5%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|NAME |Название черного списка. |string |M
|ACTION |Название действия. create (создание), delete(удаление). |string |M
|PLMN_MASK |Маска PLMN. |regex |M
|country/COUNTRY |Название страны. |string |O
|network/NETWORK |Название сети. |string |О
|===

=== Пример тела запроса

[source,bash]
----
$ ./bl_plmn_control BL Add '.(5)' country ALL network ANY
----

=== Ответ

....
{
  "status": "<string>"
}
....

=== Поля ответа

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус выполнения запроса. |string |M
|===

=== Пример ответа

....
{
  "status": "OK"
}
....
