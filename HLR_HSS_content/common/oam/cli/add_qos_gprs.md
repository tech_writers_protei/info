
Команда `add_qos_gprs` позволяет добавить данные QoS GPRS в профиль абонента.

### Запрос

```bash
$ ./ add_qos_gprs [imsi=%value%] [qos-id=%value%]
```

```bash
$ ./ add_qos_gprs [msisdn=%value%] [qos-id=%value%]
```

#### Поля запроса

| Поле   | Описание                       | Тип    | O/M |
|--------|--------------------------------|--------|-----|
| IMSI   | Номер IMSI абонента.           | string | C   |
| MSISDN | Номер MSISDN абонента.         | string | C   |
| QOS-ID | Идентификатор данных QoS GPRS. | int    | M   |

#### Пример тела запроса

```bash
$ ./add_qos_gprs  101010000001000 1
```

#### Ответ

```
{
  "status": "<string>"
}
```