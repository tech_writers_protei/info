Команда `ch_nam` позволяет изменять режим доступа к сети.

== Запрос

[source,bash]
----
$ ./ch_nam [imsi=%value%] [msisdn=%value%] [nam=%value%] 
----

=== Поля запроса

[width="99%",cols="6%,88%,4%,2%",options="header",]
|===
|Поле |Описание |Тип |O/M
|IMSI |Номер IMSI абонента. |string |C
|MSISDN |Номер MSISDN абонента. |string |C
|NAM |Режим доступа к сети (см. 3GPP TS 23.008). 0 - оба типа сети (PS+CS), 1 - только домен CS (non-GPRS/EPS), 2 - только домен PS (GPRS/EPS). |string |M
|===

=== Пример тела запроса

Доступ только для CS домена

[source,bash]
----
$ ch_nam 101010000001000 1
----

=== Ответ

....
{
  "status": "<string>"
}
....

=== Поля ответа

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус выполнения запроса. |string |M
|===

=== Пример ответа

....
{
  "status": "OK"
}
....
