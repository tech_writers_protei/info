
Команда `bl_vlr_control` позволяет добавить маску VLR в черный перечень или удалить маску VLR из черного списка.

### Запрос

```bash
$ ./control_csi [name=%value%] [action=%value%] [vlr_mask=%value%] [country=%value%] [network=%value%]
```


#### Поля запроса

| Поле            | Описание                                                | Тип    | O/M |
|-----------------|---------------------------------------------------------|--------|-----|
| NAME            | Название черного списка.                                | string | M   |
| ACTION          | Название действия. create (создание), delete(удаление). | string | M   |
| VLR_MASK        | Маска PLMN.                                             | regex  | M   |
| country/COUNTRY | Название страны.                                        | string | O   |
| network/NETWORK | Название сети.                                          | string | О   |

#### Пример тела запроса

```bash
$ ./bl_vlr_control BL Add '.(0,22)' country ALL network ANY
```

#### Ответ

```
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле   | Описание                   | Тип    | O/M |
|--------|----------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M   |


#### Пример ответа

```json
{
  "status": "OK"
}
```