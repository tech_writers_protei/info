
Команда `ch_msisdn` позволяет изменять номер MSISDN существующему абоненту.

### Запрос

```bash
$ ./odb_control [imsi=%value%] [msisdn=%value%] [new_msisdn=%value%] 
```

```bash
$ ./odb_control [imsi=%value%] [new_msisdn=%value%] 
```

#### Поля запроса

| Поле       | Описание                     | Тип    | O/M |
|------------|------------------------------|--------|-----|
| IMSI       | Номер IMSI абонента.         | string | C   |
| MSISDN     | Номер MSISDN абонента.       | string | C   |
| NEW_MSISDN | Новый номер MSISDN абонента. | string | О   |

#### Пример тела запроса

```bash
$ ch_msisdn 101010000001000 1010100124 1010100100
```

Добавление номера MSISDN абоненту, у которого не привязан номер: MSISDN
 
```bash
$ ch_msisdn 101010000001000 1010100100
```

#### Ответ

```
{
  "status": "<string>"
}
```