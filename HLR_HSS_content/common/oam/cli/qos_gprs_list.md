
Команда `qos_gprs_list` позволяет просмотреть профиль GPRS QoS.

### Запрос

```bash
$ ./qos_gprs_list [qos-gprs-id=%value%] 
```

#### Поля запроса

| Поле | Описание | Тип | O/M |
|-------------|---------------------------------|-----|-----|
| qos-gprs-id | Идентификатор профиля QOS GPRS. | int | M |

#### Пример тела запроса

```bash
$ ./$ qos_gprs_list 1
```

#### Ответ

```
{
  "status": "<string>",
  "qosGprs": {<object>}
}

```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|---------|--------------------------------------------------------------------------------------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |
| qosGprs | Профиль QoS GPRS. См. [QoS_gprs] (../API/provisioning/entities/qosGprs/). | object | M |


#### Пример ответа

```
{
  "status": "OK",
  "qosGprs": {
    "id": 1,
    "qosSubscribed": "0000000000",
    "extQosSubscribed": "0000000001",
    "ext2QosSubscribed": "0000000002",
    "ext3QosSubscribed": "0000000003",
    "ext4QosSubscribed": "0000000004"
  }
}
```