Команда `del_eps` позволяет удалить данные QoS EPS из профиля абонента.

== Запрос

[source,bash]
----
$ ./del_qos_eps [imsi=%value%] 
----

[source,bash]
----
$ ./del_qos_eps [msisdn=%value%] 
----

=== Поля запроса

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|IMSI |Номер IMSI абонента. |string |C
|MSISDN |Номер MSISDN абонента. |string |C
|===

=== Пример тела запроса

[source,bash]
----
$ ./del_qos_eps  101010000001000 1
----

=== Ответ

[source,text]
----
{
  "status": "<string>"
}
----

=== Поля ответа

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус выполнения запроса. |string |M
|===

=== Пример ответа

....
{
  "status": "OK"
}
....
