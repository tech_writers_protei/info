
Команда `qos_eps_control` позволяет создать/изменить/удалить профиль EPS QoS.

### Запрос

```bash
$ ./qos_eps_control [qos-eps-id=%value%] [command=%value%] [qosClassId/ID=%value%] [allocateRetPriority/ARP=%value%] [maxDl/DL=%value%]
[maxUl/UL=%value%] [extMaxDl/DL=%value%] [extMaxUl/UL=%value%] [preEmptionCapability/CAPABILITY=%value%] [preEmptionVulnerability/VULNERABILITY=%value%]
```


#### Поля запроса

| Поле | Описание | Тип | O/M |
|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| QOS-EPS-ID | Идентификатор профиля QOS EPS. | int | M |
| COMMAND | Название команды.pass:q[\<br\>]create (создание)/modify (изменение)/delete (удаление).pass:q[\<br\>]**Примечание.** При использовании команды modify необходимо указать все параметры (как и при команде create). Все неуказаные параметры будут null. | string | М |
| qosClassId/ID | Идентификатор класса QoS (см. [3GPP TS 23.203](https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf), таблица 6.1.7).pass:q[\<br\>]Диапазон: 5-9. | int | О |
| allocateRetPriority/ARP | Значение Allocation Retention Priority (см. [3GPP TS 29.212](https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf)). | int | O |
| maxDl/DL | Максимальная запрашиваемая полоса пропускания в направлении DL. | int | O |
| maxUl/UL | Максимальная запрашиваемая полоса пропускания в направлении UL. | int | O |
| extMaxDl/DL | Расширенная максимальная запрашиваемая полоса пропускания в направлении DL. | int | O |
| extMaxUl/UL | Расширенная максимальная запрашиваемая полоса пропускания в направлении UL. | int | O |
| preEmptionCapability/CAPABILITY | Значение Pre-emption-Capability IE, указывает на возможность предварительного освобождения запроса на других E-RAB (см. 3GPP TS 29.060). | int | O |
| preEmptionVulnerability/VULNERABILITY | Значение Pre-emption-Vulnerability IE указывает на уязвимость E-RAB к преимущественному использованию других E-RAB (см. 3GPP TS 29.060). | int | O |


#### Пример тела запроса

```bash
$ ./qos_eps_control 1 create  qosClassId 6 allocateRetPriority 13 maxDl 100000000 maxUl 100000000
```

#### Ответ

```
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|-------|---------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```
{
  "status": "OK"
}
```