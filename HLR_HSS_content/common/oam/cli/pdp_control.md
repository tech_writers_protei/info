
Команда `pdp_control` позволяет создать/изменить/удалить контекст PDP.

### Запрос

```bash
$ ./pdp_control [pdp-id=%value%] [command=%value%] [apn=%value%] [vplmnAddressAllowed=%value%] [extPdpAddress=%value%]
[extPdpType=%value%] [pdpChargingCharacteristics=%value%] [qosSubscribed=%value%] [extQosSubscribed=%value%] [ext2QosSubscribed=%value%] [ext3QosSubscribed=%value%] [ext4QosSubscribed=%value%]
```


#### Поля запроса

| Поле | Описание | Тип | O/M |
|----------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| PDP-ID | Идентификатор контекста PDP. | int | M |
| COMMAND | Название команды.pass:q[\<br\>]create (создание)/modify (изменение)/delete (удаление).pass:q[\<br\>]**Примечание.** При использовании команды modify необходимо указать все параметры (как и при команде create). Все неуказаные параметры будут null. | string | М |
| apn | Идентификатор сети APN (см. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf), пункты 9.1 и 9.1.1) или значение wild card (см. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf), пункт 9.2.1, и 3GPP TS 23.008, пункт 2.13.6). | string | С |
| vplmnAddressAllowed | Параметр указывает, что оборудованию пользователя разрешено использовать точку доступа APN в домене сети VPLMN.pass:q[\<br\>]**Примечание.** Если значение не задано, но считается, что не разрешено ). | bool | O |
| extPdpAddress | Расширенный адрес PDP (может содержать адрес IPv4 или IPv6). | int | O |
| extPdpType | Расширенный тип PDP (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf), пункт 7.6.2.44A).pass:q[\<br\>]Пример: f18d. | int | O |
| pdpChargingCharacteristics | Значение Charging Characteristics для контекста PDP (См. [3GPP TS 32.215](https://www.etsi.org/deliver/etsi_ts/132200_132299/132215/05.09.00_60/ts_132215v050900p.pdf), пункт 5.6). | int | O |
| qosSubscribed | Значение QoS-Subscribed (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) version 15.6.0). | int | O |
| extQosSubscribed | Значение Ext-QoS-Subscribed (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) version 15.6.0). | int | O |
| ext2QosSubscribed | Значение Ext2-QoS-Subscribed (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) version 15.6.0). | int | O |
| ext3QosSubscribed | Значение Ext3-QoS-Subscribed (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) version 15.6.0). | int | O |
| ext4QosSubscribed | Значение Ext4-QoS-Subscribed (см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf) version 15.6.0). | int | O |

#### Пример тела запроса

```bash
$ ./pdp_control 10 create apn internet_test vplmnAddressAllowed 1 qosSubscribed 239412 extQosSubscribed 0x027196fefe7482ffff
```

#### Ответ

```
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|--------|---------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```
{
  "status": "OK"
}