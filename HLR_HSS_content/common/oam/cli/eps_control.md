
Команда `eps_control` позволяет создать/изменить/удалить контекст EPS.

### Запрос

```bash
$ ./eps_control [eps_id=%value%] [command=%value%] [service-selection=%value%] [pdn-type=%value%] [vplmnDynamicAddressAllowed/ISALLOWED=%value%]  
[qosClassId/ID=%value%] [allocateRetPriority/ARP=%value%] [pdnGwType/GW-TYPE=%value%] [mip6Ip4/ADDRESS] [mip6Ip6/ADDRESS] [mip6AgentHost/HOST] 
[mip6AgentRealm/REALM] [visitedNetworkId/ID] [chargingCharacteristics/CC] [apnOiReplacement/APN] [maxDl/DL] [maxUl/UL] [extMaxDl/DL] [extMaxUl/UL]   
[preEmptionCapability/CAPABILITY] [preEmptionVulnerability/VULNERABILITY] [mpsPriority/MSP]          

```


#### Поля запроса

| Поле | Описание | Тип | O/M |
|---------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| EPS-ID | Идентификатор контекста EPS. | int | M |
| COMMAND | Название команды. возможные значения: create/modify/delete.pass:q[\<br\>]**Примечание.** При использовании команды modify необходимо указать все параметры (как при команде create). Все неуказаные параметры будут null. | string | M |
| SERVICE-SELECTION | Идентификатор сети APN (см. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf), пункты 9.1 и 9.1.1) или значение wild card (см. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf), пункт 9.2.1, и 3GPP TS 23.008, пункт 2.13.6). | string | С |
| PDN-TYPE | Тип адреса сети PDN. 0 - IPv4, 1 - IPv6, 2 - IPv4v6, 3 - IPv4_OR_IPv6. | int | C |
| vplmnDynamicAddressAllowed/ISALLOWED | Параметр указывает, что оборудованию пользователя разрешено использовать шлюз PDN GW в домене сети VPLMN.pass:q[\<br\>]**Примечание.** Если значение не задано, но считается, что не разрешено. | bool | O |
| qosClassId/ID | Идентификатор класса QoS (см. [3GPP TS 23.203](https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf), таблица 6.1.7).pass:q[\<br\>]Диапазон: 5-9. | int | O |
| allocateRetPriority/ARP | Значение Allocation Retention Priority (см. [3GPP TS 29.212](https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf)). | int | O |
| pdnGwType/GW-TYPE | Тип шлюза PDN GW.      0 - STATIC, 1 - DYNAMIC. | int | M |
| mip6Ip4/ADDRESS | IPv4 адрес шлюза PDN GW. | ip | O |
| mip6Ip6/ADDRESS | IPv6 адрес шлюза PDN GW. | ip | O |
| mip6AgentHost/HOST | Название хоста шлюза PDN GW (см. [3GPP TS 29.303](https://www.etsi.org/deliver/etsi_ts/129300_129399/129303/18.00.00_60/ts_129303v180000p.pdf), пункт 4.3.2). | string | О |
| mip6AgentRealm/REALM | Область назначения (Destination-Realm) шлюза PDN GW.pass:q[\<br\>]Пример: epc.mnc.mcc.3gppnetwork.org. | string | C |
| visitedNetworkId/ID | Сеть PLMN, в которой расположен шлюз PDN GW. | int | O |
| chargingCharacteristics/CC | Данные сети EPS PDN Connection Charging Characteristics. | string | О |
| apnOiReplacement/APN | Название домена для замены значения APN OI (см. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf) и [3GPP TS 29.303](https://www.etsi.org/deliver/etsi_ts/129300_129399/129303/18.00.00_60/ts_129303v180000p.pdf).). | string | О |
| maxDl/DL | Максимальная запрашиваемая полоса пропускания в направлении DL. | int | O |
| maxUl/UL | Максимальная запрашиваемая полоса пропускания в направлении UL. | int | O |
| extMaxDl/DL | Расширенная максимальная запрашиваемая полоса пропускания в направлении DL. | int | O |
| extMaxUl/UL | Расширенная максимальная запрашиваемая полоса пропускания в направлении UL. | int | O |
| preEmptionCapability/CAPABILITY | Значение Pre-emption-Capability IE, указывает на возможность предварительного освобождения запроса на других E-RAB (см. 3GPP TS 29.060). | int | O |
| preEmptionVulnerability/VULNERABILITY | Значение Pre-emption-Vulnerability IE указывает на уязвимость E-RAB к преимущественному использованию других E-RAB (см. 3GPP TS 29.060). | int | O |
| mpsPriority/MSP | Маска битов приоритетной службы мультимедиа. 0 - значение MPS-CS-Priority указывает, что оборудование пользователя (UE) подписано на приоритетную услугу приоритета eMLPP или 1x RTT в домене CS;  1 - значение MPS-EPS-Priority указывает, что оборудование пользователя (UE) подписано на приоритетную услугу MPS в домене EPS. | int | O |

   




#### Пример тела запроса

```bash
$ ./eps_control 1 create APN 0 vplmnDynamicAddressAllowed 1 qosClassId 6 allocateRetPriority 13 pdnGwType 1 maxDl 100000000 maxUl 100000000 
```


#### Ответ

```
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|--------|---------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```
{
  "status": "OK"
}
```