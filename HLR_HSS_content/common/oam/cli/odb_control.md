
Команда `odb_control` позволяет установить/снять операторские ограничения на услуги.

### Запрос

```bash
$ ./odb_control [imsi=%value%] [subscriber_status=%value%] [action=%value%] [ODB-GeneralData=%value%]  
```


```bash
$ ./odb_control [msisdn=%value%] [subscriber_status=%value%] [action=%value%] [ODB-GeneralData=%value%]  
```

#### Поля запроса

| Поле | Описание | Тип | O/M |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| IMSI | Номер IMSI абонента. | string | C |
| MSISDN | Номер MSISDN абонента. | string | C |
| Subscriber_status | Статус абонента. 0 - Service Granted (обеспечение услуг), 1 - ODB ограничения активны. | int | М |
| Action | Флаг активности запрета. | bool | О |
| ODB-GeneralData | Категория, на которую можно установить запрет. См. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf). См. [ODB] (../API/provisioning/add_info/odb/). | int | O |




#### Пример тела запроса
 
```bash
$ odb_control 101010000001000 1 1 0
```

#### Ответ

```text
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|-------|---------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```json
{
  "status": "OK"
}
```