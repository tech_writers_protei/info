
Команда `get_csi` позволяет просмотреть CSI-профиль.

### Запрос

```bash
$ ./get_csi [csi-type=%value%] [csi-id=%value%] 
```


#### Поля запроса

| Поле | Описание | Тип | O/M |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------|--------|-----|
| CSI-TYPE | Тип CSI-профиля. oCsi, tCsi, mCsi. | string | M |
| CSI-ID | Идентификатор CSI-профиля. | int | М |


#### Пример тела запроса

```bash
$ ./get_csi ocsi 1
```

#### Ответ

```
{
  "typeId": <int>,
  "profileId": <int>
}
{
  "status": "<string>",
  "oCsi": {
    "id": <int>,
    "name": "<string>",
    "camelCapabilityHandling": <int>,
    "notificationToCse": <bool>,
    "csiActive": <bool>,
    "csiTdps": [
      {
        "tdpId": <int>,
        "serviceKey": <int>,
        "gsmScfAddress": "<string>",
        "defaultHandling": <int>
      }
    ]
  }
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------|--------|-----|
| typeId | Идентификатор типа CSI-профиля. | int | M |
| profileId | Идентификатор CSI-профиля. | int | M |
| status | Статус выполнения запроса. | string | M |
| id | Идентификатор CSI-профиля. | int | М |
| name | Название CSI-профиля. | string | M |
| camelCapabilityHandling | Фаза Camel. 1,2,3. | int | O |
| notificationToCse | Флаг отправки уведомления. | bool | O |
| csiActive | Флаг активного CSI-профиля. | bool | O |
| tdpId | Идентификатор TDP. | int | O |
| serviceKey | Идентификатор службы, `ServiceKey`. | int | O |
| gsmScfAddress | GT SCF. | string | O |
| defaultHandling | Параметр обработки запроса. 0 - continue (продолжить обработку), 1 - release (остановить обработку). | int | O |
| mmCode | Код MM. | int | O |



#### Пример ответа

```
{
  "typeId": 0,
  "profileId": 1
}
{
  "status": "OK",
  "oCsi": {
    "id": 1,
    "name": "OCSI1",
    "camelCapabilityHandling": 2,
    "notificationToCse": 1,
    "csiActive": 1,
    "csiTdps": [
      {
        "tdpId": 12,
        "serviceKey": 10,
        "gsmScfAddress": "79006199782",
        "defaultHandling": 1
      }
    ]
  }
}
```