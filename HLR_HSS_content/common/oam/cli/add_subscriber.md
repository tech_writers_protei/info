
Команда `add_subscriber` позволяет загрузить данные нового абонента на узел HLR.

### Запрос

```bash
$ ./add_subscriber [imsi=%value_1%] [ki=%value_N%] [algorithm=%value_N%] [status=%value_N%] [hlr_profile_id=%value_N%] [msisdn=%value_N%] [group_id=%value_N%] [opc=%value_N%]
```

#### Поля запроса

| Поле           | Описание                                                                                                        | Тип    | O/M |
|----------------|-----------------------------------------------------------------------------------------------------------------|--------|-----|
| IMSI           | Номер IMSI абонента.                                                                                            | string | M   |
| Ki             | Ключ аутентификации (см. TS 33.102).                                                                            | string | M   |
| algorithm      | Алгоритм шифрования (см. TS 33.102).pass:q[\<br\>]Доступные значения: 1-4.                                                | int    | M   |
| status         | Статус профиля абонента. См. [Subscriber_status_codes] (../API/provisioning/add_info/subscriber_status_codes/). | int    | M   |
| hlr_profile_id | Идентификатор профиля абонента.                                                                                 | int    | О   |
| MSISDN         | Номер MSISDN абонента.                                                                                          | string | О   |
| Group_ID       | Идентификатор группы абонентов.                                                                                 | string | О   |
| opc            | Параметр вектора аутентификации, используется в алгоритме Milenage (см. TS 43.020).                             | string | О   |

#### Пример тела запроса

```bash
$ ./add_subscriber 101010000001000 ki 7D25499BD1817AAF49BF42939343B299 alg 4 status 2 profile_id 1 msisdn 1010100100
```

#### Ответ

```
{
  "status": "<string>>"
}
```