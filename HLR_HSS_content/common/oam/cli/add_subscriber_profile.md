
Команда `add_subscriber_profile` позволяет добавить профиль абонента к карте, ранее загруженной в центр аутентификации AuC.

### Запрос

```bash
$ ./add_subscriber_profile [imsi%=%value%] [msisdn=%value%] [status=%value%] [hlr_profile_id=%value%]
```

#### Поля запроса

| Поле | Описание | Тип | O/M |
|----------------|---------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| IMSI | Номер IMSI абонента. | string | M |
| MSISDN | Номер MSISDN абонента. | string | О |
| status | Статус профиля абонента. См. возможные значения в разделе [subscriber_status_codes](../API/provisioning/add_info/subbscriber_status_codes/). | string | М |
| hlr_profile_id | Идентификатор профиля абонента из profiles.json (см. описание в разделе (../config/API/profiles/). | string | О |

#### Пример тела запроса

```bash
add_subscriber_profile 00101001000003 msisdn 70003332211 status 2 hlr_profile_id 1
```