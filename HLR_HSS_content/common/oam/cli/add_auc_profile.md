
Команда `add_auc_profile` позволяет загрузить профиль AuC для карты в БД HLR/AuC.





### Запрос



```bash

$ ./add_auc_profile [imsi %value%] [ki %value%] [algorithm %value%] [opc %value%] 

```



#### Поля запроса



| Поле      | Описание                                                                                                                    | Тип    | O/M |

|-----------|-----------------------------------------------------------------------------------------------------------------------------|--------|-----|

| IMSI      | Номер IMSI карты.                                                                                                           | string | M   |

| Ki        | Ki карты.                                                                                                                   | string | M   |

| algorithm | Алгоритм аутентификации. См. [Authentication_algorithm_codes](../API/provisioning/add_info/authentication_algorithm_codes). | int    | M   |

| opc       | Параметр вектора аутентификации, используется в алгоритме Milenage (TS 43.020).                                             | string | O   |



#### Пример тела запроса



```bash

add_auc_profile 00101001000003 ki 7D25499BD1817AAF49BF42939343B299 algorithm 4 opc 7D25499BD1817AAF49BF42939343B299

```