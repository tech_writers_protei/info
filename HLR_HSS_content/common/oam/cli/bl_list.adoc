Команда `bl_list` позволяет просмотреть маски VLR и PLMN, привязанных к черному списку.

== Запрос

[source,bash]
----
$ ./bl_list [id=%value%] [name=%value%] 
----

=== Поля запроса

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|ID |Идентификатор черного списка. |int |C
|NAME |Название черного списка. |string |C
|===

=== Пример тела запроса

[source,bash]
----
$ ./bl_list BL
----

=== Ответ

....
{
  "status": "<string>",
  "blackList": {
    "id": <int>,
    "name": "<string>",
    "vlr_masks": [<list>],
    "plmn_masks": [<list>]
  }
}
....

=== Поля ответа

[width="100%",cols="9%,70%,18%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус выполнения запроса. |string |M
|id |Идентификатор черного списка. |string |M
|name |Название черного списка. |string |M
|vlr_masks |Маска номера узла VLR. Описание параметров маски номера узла VLR см. в разделе link:../entities/blVlrMask[BLVlrmask]. |[object] |O
|plmn_masks |Маска сети PLMN. Описание параметров сети PLMN см. в разделе link:../entities/blPlmnMask[BLPlmnmask]. |[object] |O
|===

=== Пример ответа

....
{
  "status": "OK",
  "blackList": {
    "id": 1,
    "name": "BL",
    "vlr_masks": [
      {
        "mask": ".(0,22)",
        "country": "ALL",
        "network": "ANY"
      }
    ],
    "plmn_masks": [
      {
        "mask": ".(5)",
        "country": "ALL",
        "network": "ANY"
      }
    ]
  }
}
....
