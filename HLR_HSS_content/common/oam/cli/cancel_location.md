
Команда `cancel_location` позволяет отправить запрос Cancel_Location на текущие узлы VLR/SGSN/MME абонента.

### Запрос

```bash
$ ./cancel_location [imsi=%value%] [domain=%value%] [reattachRequired=%value%]
```

```bash
$ ./cancel_location [msisdn=%value%] [domain=%value%] [reattachRequired=%value%]
```

#### Поля запроса

| Поле | Описание | Тип | O/M |
|------------------|---------------------------------------------------------------------------------------------------|--------|-----|
| IMSI | Номер IMSI абонента. | string | C |
| MSISDN | Номер MSISDN абонента. | string | C |
| Domain | Название домена. CS, PS, EPS.pass:q[\<br\>]По умолчанию: идет отправка по всем доменам. | string | О |
| reattachRequired | Флаг необходимости проведения процедуры Reattach. | string | О |

#### Пример тела запроса
Отправка запроса Cancel_location в EPS домене с необходимостью Reattach

```bash
$ ./cancel_location 101010000001000 EPS reattach
```

Отправка запроса Cancel_location по всем доменам
```bash
$ ./cancel_location 101010000001000
```

#### Ответ

```
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|--------|---------------------------|--------|-----|
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```
{
  "status": "OK"
}