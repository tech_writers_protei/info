
Команда `get_profile` позволяет просмотреть текущий профиль абонента.

### Запрос

```bash
$ ./get_profile [imsi=%value%] 
```

```bash
$ ./get_profile [msisdn=%value%] 
```

#### Поля запроса

| Поле | Описание | Тип | O/M |
|--------|-----------------------|--------|-----|
| IMSI | Номер IMSI абонента. | string | C |
| MSISDN | Номер MSISDN абонента. | string | C |


#### Пример тела запроса

```bash
$ get_profile 101010000001000 
```
#### Ответ
```json
{subscriberProfileRequested}
```

#### Поля ответа

Описание параметров объекта subscriberProfileRequested в разделе [Subscriber_profile_requested] (../API/provisioning/entities/subscriberProfileRequested/)  

#### Пример ответа


```json
{
  "status": "OK",
  "imsi": "101010000001000",
  "msisdn": "1010100100",
  "administrative_state": 2,
  "forbid_reg": 0,
  "roaming_not_allowed": 0,
  "pdp-data": [],
  "eps-context-data": [
    {
      "eps_context_id": 1,
      "service_selection": "internet",
      "vplmnDynamicAddressAllowed": 0,
      "qosClassId": 9,
      "pdnGwType": 1,
      "pdnType": 2
    },
    {
      "eps_context_id": 2,
      "service_selection": "ims",
      "vplmnDynamicAddressAllowed": 0,
      "qosClassId": 5,
      "allocateRetPriority": 9,
      "pdnGwType": 1,
      "apnOiReplacement": "mnc001.mcc001.gprs",
      "maxDl": 100000000,
      "maxUl": 100000000,
      "pdnType": 2
    },
    {
      "eps_context_id": 3,
      "service_selection": "internet3",
      "vplmnDynamicAddressAllowed": 0,
      "pdnGwType": 1,
      "pdnType": 2
    }
  ],
  "eps-data": [
    {
      "ueMaxDl": 100000000,
      "ueMaxUl": 100000000,
      "ratFreqPriorId": 6,
      "defContextId": 1
    }
  ],
  "csi-list": [],
  "ssData": [],
  "ssForw": [],
  "ssBarring": [],
  "lcsPrivacy": [],
  "lcsMolr": [],
  "teleserviceList": [],
  "bearerserviceList": [],
  "roaming-sgsn-info": {
    "version": 0,
    "dtregistered": "2020-06-01_11:23:33",
    "ispurged": 0,
    "dmMmeHost": "mmec1.mmegi2.mme.epc.mnc1.mcc1.3gppnetwork.org",
    "dmMmeRealm": "epc.mnc1.mcc1.3gppnetwork.org",
    "is-purged-eps-mme": 0,
    "is-purged-eps-sgsn": 0,
    "srvccMme": 0,
    "voImsMme": 0,
    "vplmn": "00101",
    "sf1Mme": 0,
    "sf2Mme": 134217728,
    "supportedSgsnCamel": 0,
    "supportedSgsnLcs": 0,
    "combinedMmeSgsn": 1
  },
  "defaultForwStatus": 0,
  "whiteLists": [],
  "blackLists": [],
  "lcsId": 0,
  "deactivatePsi": 0,
  "ipSmGw": "",
  "networkAccessMode": 0,
  "qosEpsId": 1
}
```