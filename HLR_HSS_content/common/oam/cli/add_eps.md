
Команда `add_eps` позволяет привязать контекст EPS и добавить/изменить связанный с ним статический IP-адрес.

### Запрос

```bash
$ ./add_eps [imsi=%value%] [eps-id=%value%] [ipv4=%value%] [ipv6=%value%] [plmnId=%value%]
```

```bash
$ ./add_eps [msisdn=%value%] [eps-id=%value%] [ipv4=%value%] [ipv6=%value%] [plmnId=%value%]
```

#### Поля запроса

| Поле   | Описание                     | Тип    | O/M |
|--------|------------------------------|--------|-----|
| IMSI   | Номер IMSI абонента.         | string | C   |
| MSISDN | Номер MSISDN абонента.       | string | C   |
| EPS-ID | Идентификатор контекста EPS. | int    | M   |
| ipv4   | Статический IPv4-адрес.      | ip     | O   |
| ipv6   | Статический IPv6-адрес.      | ip     | O   |
| plmnId | Идентификатор сети PLMN.     | int    | O   |

#### Пример тела запроса

```bash
$ ./add_eps 101010000001000 1 ipv4 1.1.1.1 ipv6 2222:2222:2222:2222:2222:2222:2222:2222 plmnId 1
```

#### Ответ

```
{
  "status": "<string>"
}
```