
Команда `control_csi` позволяет создать/изменить/удалить CSI-профиль.

### Запрос

```bash
$ ./control_csi [action=%value%] [csi-type=%value%] [csi-id=%value%] [name=%value%] [camelCapabilityHandling=%value%]
[notificationToCse=%value%] [csiActive=%value%] [tdpId=%value%] [serviceKey =%value%] [gsmScfAddress =%value%] [defaultHandling=%value%] [mmCode=%value%] 
```


#### Поля запроса

| Поле | Описание | Тип | O/M |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------|--------|-----|
| ACTION | Название действия. create (создание), modify (изменение), delete (удаление). | string | M |
| CSI-TYPE | Тип CSI-профиля. oCsi, tCsi, mCsi. | string | M |
| CSI-ID | Идентификатор CSI-профиля. | int | М |
| name | Название CSI-профиля. | string | M |
| camelCapabilityHandling | Фаза Camel. 1,2,3. | int | O |
| notificationToCse | Флаг отправки уведомления. | bool | O |
| csiActive | Флаг активного CSI-профиля. | bool | O |
| tdpId | Идентификатор TDP. | int | O |
| serviceKey | Идентификатор службы, `ServiceKey`. | int | O |
| gsmScfAddress | GT SCF. | int | O |
| defaultHandling | Параметр обработки запроса. 0 - continue (продолжить обработку), 1 - release (остановить обработку). | int | O |
| mmCode | Код MM. | int | O |


#### Пример тела запроса

```bash
$ ./control_csi create ocsi 1 name OCSI1 camelCapabilityHandling 2 notificationToCse 1 csiActive 1 tdpId 12 serviceKey 10 gsmScfAddress 79006199782 defaultHandling 1
```

#### Ответ

```
{
  "action": "<string>",
  "oCsi": {<object>}
}
{
  "status": "<string>"
}
```

#### Поля ответа

| Поле | Описание | Тип | O/M |
|--------|-------------------------------------------------------------------------------------------------|--------|-----|
| action | Название действия. create (создание), modify (изменение), delete (удаление) | string | M   | 
| <Csi> | CSI-профиль. Параметры профиля см. выше в запросе. | object | M |
| status | Статус выполнения запроса. | string | M |


#### Пример ответа

```
{
  "action": "create",
  "oCsi": {
    "id": "1",
    "name": "OCSI1",
    "camelCapabilityHandling": "2",
    "notificationToCse": "1",
    "csiActive": "1",
    "csiTdps": [
      {
        "tdpId": "12",
        "serviceKey": "10",
        "gsmScfAddress": "79006199782",
        "defaultHandling": "1"
      }
    ]
  }
}
{
  "status": "OK"
}
```