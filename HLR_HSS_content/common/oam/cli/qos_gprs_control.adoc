Команда `qos_gprs_control` позволяет создать/изменить/удалить профиль GPRS QoS.

== Запрос

[source,bash]
----
$ ./qos_gprs_control [qos-gprs-id=%value%] [command=%value%] [qos=%value%] [extqos=%value%] [ext2qos=%value%]
[ext3qos=%value%] [ext4qos=%value%]
----

=== Поля запроса

[width="100%",cols="7%,90%,2%,1%",options="header",]
|===
|Поле |Описание |Тип |O/M
|QOS-GPRS-ID |Идентификатор профиля QOS GPRS. |int |M
|COMMAND |Название команды.pass:q[<br>]create (создание)/modify (изменение)/delete (удаление).pass:q[<br>]*Примечание.* При использовании команды modify необходимо указать все параметры (как и при команде create). Все неуказаные параметры будут null. |string |М
|qos |октет QoS-Subscribed. |string |О
|extqos |Октет Ext-QoS-Subscribed. |string |O
|ext2qos |Октет Ext2-QoS-Subscribed. |string |O
|ext3qos |Октет Ext3-QoS-Subscribed. |string |O
|ext4qos |Октет Ext3-QoS-Subscribed. |string |O
|===

=== Пример тела запроса

[source,bash]
----
$ ./qos_gprs_control 1 create qos 0000000000 extqos 0000000001 ext2qos 0000000002 ext3qos 0000000003 ext4qos 0000000004
----

=== Ответ

....
{
  "status": "<string>"
}
....

=== Поля ответа

[cols=",,,",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус выполнения запроса. |string |M
|===

=== Пример ответа

``` \{ "status": "OK" }
