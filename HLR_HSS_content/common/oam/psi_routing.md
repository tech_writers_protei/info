
Для настройки PSI Routing необходимо

1. Создать [SCC_AS](../../interface/API/provisioning/controlAs/).
2. Создать [псевдо абонента](../../interface/API/provisioning/addSubscriberProfile/) с подпиской IMS , содержащей [PSI](../../interface/API/provisioning/entities/impu) и привязанный SCC_AS:
```json
{
  "imsi": "000000000012345",
  "msisdn": "000000000012345",
  "ki": "ffffffffffffffffffffffffffffffff",
  "opc": "ffffffffffffffffffffffffffffffff",
  "algorithm": 4,
  "status": 2,
  "imsProfile": {
    "impi":"000000000012345@ims.protei.ru",
    "authScheme":4,
    "impus":
    [
      {
        #см. пункт 3
      }
    ]
  },
  "imsSubscription": {
    "serviceProfiles":
    [
      {
        "Name":"sp",
        "CoreNetworkServiceAuthorization":1
      }
    ],
    "sccAsName": "scc_as"
  }
}

``` 
3. Поддерживаются два типа PSI  
3a. В случае STN-SR Routing (SRVCC) необходимо создать Distinct PSI:
```json
{
  "Identity": "sip:+12345",
  "Type": 1,
  "BarringIndication": 0,
  "CanRegister": 1,
  "PsiActivation": 1,
  "ServiceProfileName":"sp" 
}
```
3b. В случае IMRN Routing необходимо создать Wilcarded PSI:
```json
{
  "Identity": "sip:+12345!.*!",
  "Type": 2,
  "BarringIndication": 0,
  "CanRegister": 1,
  "PsiActivation": 1,
  "ServiceProfileName": "sp" 
}
```