
Команда `GetAudit` позволяет получать информацию о действиях пользователей.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetAudit?offset=<OFFSET>&limit=<LIMIT> \
  -d '{
  "filter": <oject>,
  "all": <bool>
}
```

### Поля запроса query ###

| Поле   | Описание                                            | Тип | O/M |
|--------|-----------------------------------------------------|-----|-----|
| offset | Сдвиг первой требуемой записи. По умолчанию: 0.     | int | О   |
| limit  | Количество отображаемых записей. По умолчанию: все. | int | О   |

### Поля тела запроса ###

| Поле   | Описание                                        | Тип                                     | O/M | Версия   |
|--------|-------------------------------------------------|-----------------------------------------|-----|----------|
| filter | Параметры фильтра для записей журнала действий. | [AuditFilter](../entities/auditFilter/) | M   | 2.0.54.2 |

### Пример тела запроса ###

```json
{
  "filter": { "sortDirection": "DESC" }
}
```

### Ответ ###

В ответе передаются статус запроса и параметры записей.

```
{
  "status": "<string>",
  "data": [ <Audit> ],
  "total": <int>
}
```

### Поля ответа ###

| Поле   | Описание                                | Тип                           | O/M |
|--------|-----------------------------------------|-------------------------------|-----|
| status | Статус запроса.                         | string                        | M   |
| data   | Перечень записей журнала действий.      | [[Audit](../entities/audit/)] | M   |
| total  | Общее количество записей после фильтра. | int                           | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "data": [
    {
      "dt": 1658160802312,
      "operation": "GET_PROFILE",
      "status": "OK",
      "executionTime": 5,
      "sentToHlr": false,
      "userHost": "10.200.110.21",
      "host": "CentOS8_103",
      "userName": "null",
      "msisdn": "380725387725",
      "imsi": "255995005387725"
    },
    {
      "dt": 1658160829710,
      "operation": "CHANGE_PROFILE",
      "status": "ALREADY_EXIST",
      "executionTime": 10,
      "sentToHlr": false,
      "userHost": "10.200.110.12",
      "host": "CentOS8_103",
      "userName": "null",
      "msisdn": "380722291555",
      "imsi": "255995002291555",
      "params": "TELE_SERVICES_ADD=[34]"
    },
    {
      "dt": 1658160829805,
      "operation": "CHANGE_PROFILE",
      "status": "HLR_OBJECT_BUSY",
      "executionTime": 41,
      "sentToHlr": false,
      "userHost": "10.200.110.12",
      "host": "CentOS8_103",
      "userName": "null",
      "msisdn": "380722291555",
      "imsi": "255995002291555",
      "params": "SS_FORW"
    }
  ],
  "total": 503
}
```