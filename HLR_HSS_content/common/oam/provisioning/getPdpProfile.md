
Команда `GetPdpProfile` позволяет получать профиль PDP.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetPdpProfile \
  -d '{ "context-id": <int> }'
```

### Поля тела запроса ###

| Поле       | Описание                     | Тип | O/M |
|------------|------------------------------|-----|-----|
| context-id | Идентификатор контекста PDP. | int | M   |

### Пример тела запроса ###

```json
{ "context-id": 1 }
```

### Ответ ###

В ответе передаются статус запроса и профиль PDP.

```
{
  "status": "<string>",
  "pdpProfile": <PdpProfile>
}
```

### Поля ответа ###

| Поле       | Описание        | Тип                                   | O/M |
|------------|-----------------|---------------------------------------|-----|
| status     | Статус запроса. | string                                | M   |
| pdpProfile | Профиль PDP.    | [PdpProfile](../entities/pdpProfile/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "pdpProfile": {}
}
```