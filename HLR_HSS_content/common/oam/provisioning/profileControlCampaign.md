
Команда `ProfileControl` позволяет изменять AuC профили из файла.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileServiceCampaign/ProfileControl?jsonData=<AucProfile>
```

### Поля запроса query ###

| Поле     | Описание     | Тип                                   | O/M |
|----------|--------------|---------------------------------------|-----|
| jsonData | Профиль AuC. | [AucProfile](../entities/aucProfile/) | M   |

В теле запроса передается файл формата CSV.

Формат файла:

```csv
imsi;msisdn;EPSIpv4;EPSIpv6;
```

### Поля файла ###

| Поле    | Описание                | Тип    | O/M |
|---------|-------------------------|--------|-----|
| imsi    | Номер IMSI.             | string | О   |
| msisdn  | Номер MSISDN.           | string | О   |
| EPSIpv4 | IPv4-адрес профиля EPS. | ip     | О   |
| EPSIpv6 | IPv6-адрес профиля EPS. | string | О   |

### Пример тела запроса ###

```bash
$ curl -X https://<host>:<port>/ProfileServiceCampaign/AddAucProfile? \
jsonData=%7B%22eps-data%22%3A%7B%22data%22%3A%7B%22 \
defContextId%22%3A5%2C%22ueMaxUl%22%3A123000%7D%7D%2C%22 \
qosEpsId%22%3A%7B%22value%22%3A3%7D%2C%22epsContexts%22%3A%5B%7B%22 \
context-id%22%3A5%7D%5D%2C%22imsProfile%22%3A%7B%22 \
impi%22%3A%2221312%22%2C%22authScheme%22%3A5%2C%22sipDigest%22%3A%7B%22 \
password%22%3A%2223123%22%7D%2C%22impus%22%3A%5B%7B%22 \
Type%22%3A2%2C%22BarringIndication%22%3A1%2C%22 \
Identity%22%3A%22435345%22%2C%22CanRegister%22%3A1%2C%22 \
ServiceProfileName%22%3A%223454%22%2C%22WildcardPsi%22%3A%22443534%22%2C%22 \
PsiActivation%22%3A345345%2C%22Default%22%3Atrue%2C%22 \
delete%22%3Afalse%7D%2C%7B%22Type%22%3A3%2C%22BarringIndication%22%3A0%2C%22 \
Identity%22%3A%22fsdf%22%2C%22CanRegister%22%3A1%2C%22 \
ServiceProfileName%22%3A%22dsfsd%22%2C%22WildcardPsi%22%3A%223423%22%2C%22 \
PsiActivation%22%3A2%2C%22Default%22%3Afalse%2C%22delete%22%3Atrue%7D%5D%2C%22 \
implicitlySets%22%3A%5B%5D%7D%2C%22 \
imsSubscription%22%3A%7B%22name%22%3A%22qwe23%22%2C%22 \
capabilitySetId%22%3A45435%2C%22prefferedScscfSetId%22%3A1242135%2C%22 \
chargingInformationName%22%3A%22rgtre%22%2C%22sccAsName%22%3A%22dsfd%22%2C%22 \
serviceProfiles%22%3A%5B%7B%22Name%22%3A%2221312%22%2C%22 \
CoreNetworkServiceAuthorization%22%3A123%2C%22Ifcs%22%3A%5B%7B%22 \
Name%22%3A%22324324%22%2C%22Priority%22%3A23%2C%22 \
ApplicationServerName%22%3A%221123%22%2C%22ProfilePartIndicator%22%3A3%2C%22 \
TriggerPoint%22%3A%7B%22ConditionTypeCNF%22%3A0%2C%22Spt%22%3A%5B%7B%22 \
ConditionNegated%22%3A0%2C%22Group%22%3A3%2C%22RequestUri%22%3A%22%22%2C%22 \
Method%22%3A%22%22%2C%22Header%22%3A%22%22%2C%22Content%22%3A%22%22%2C%22 \
SdpLine%22%3A%22%22%2C%22SdpLineContent%22%3A%22%22%2C%22 \
Delete%22%3Atrue%7D%5D%7D%7D%5D%2C%22SIfcs%22%3A%5B%7B%22 \
Id%22%3A21212%7D%5D%7D%5D%2C%22implicitlyRegisteredSet%22%3A%5B%5D%7D%7D
```

### Пример тела запроса в формате JSON ###

```json
{
  "eps-data": {
    "data": {
      "defContextId": 5,
      "ueMaxUl": 123000
    }
  },
  "qosEpsId": { "value": 3 },
  "epsContexts": [
    { "context-id": 5 }
  ],
  "imsProfile": {
    "impi": "21312",
    "authScheme": 5,
    "sipDigest": { "password": "23123" },
    "impus": [
      {
        "Type": 2,
        "BarringIndication": 1,
        "Identity": "435345",
        "CanRegister": 1,
        "ServiceProfileName": "3454",
        "WildcardPsi": "443534",
        "PsiActivation": 345345,
        "Default": true,
        "delete": false
      },
      {
        "Type": 3,
        "BarringIndication": 0,
        "Identity": "fsdf",
        "CanRegister": 1,
        "ServiceProfileName": "dsfsd",
        "WildcardPsi": "3423",
        "PsiActivation": 2,
        "Default": false,
        "delete": true
      }
    ],
    "implicitlySets": []
  },
  "imsSubscription": {
    "name": "qwe23",
    "capabilitySetId": 45435,
    "prefferedScscfSetId": 1242135,
    "chargingInformationName": "rgtre",
    "sccAsName": "dsfd",
    "serviceProfiles": [
      {
        "Name": "21312",
        "CoreNetworkServiceAuthorization": 123,
        "Ifcs": [
          {
            "Name": "324324",
            "Priority": 23,
            "ApplicationServerName": "1123",
            "ProfilePartIndicator": 3,
            "TriggerPoint": {
              "ConditionTypeCNF": 0,
              "Spt": [
                {
                  "ConditionNegated": 0,
                  "Group": 3,
                  "RequestUri": "",
                  "Method": "",
                  "Header": "",
                  "Content": "",
                  "SdpLine": "",
                  "SdpLineContent": "",
                  "Delete": true
                }
              ]
            }
          }
        ],
        "SIfcs": [
          { "Id": 21212 }
        ]
      }
    ],
    "implicitlyRegisteredSet": []
  }
}
```

### Пример файла формата CSV ###

```csv
310170845466094;;;;
8801500121121;;;;
```

### Ответ ###

В ответе передаются статус запроса и параметры операции.

```
{
  "status": "<string>",
  "campaignCounter": <CampaignCounter>,
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "startDate": "<datetime>",
  "finishDate": "<datetime>"
}
```

### Поля ответа ###

| Поле            | Описание                             | Тип                                             | O/M |
|-----------------|--------------------------------------|-------------------------------------------------|-----|
| status          | Статус запроса.                      | string                                          | M   |
| campaignCounter | Счетчик операций.                    | [CampaignCounter](../entities/campaignCounter/) | M   |
| campaignStatus  | Cостояние операции.                  | string                                          | M   |
| campaignType    | Тип операции.                        | string                                          | M   |
| id              | Идентификатор операции.              | int                                             | M   |
| creationDate    | Дата создания операции.              | datetime                                        | O   |
| startDate       | Дата начала выполнения операции.     | datetime                                        | O   |
| finishDate      | Дата завершения выполнения операции. | datetime                                        | O   |

### Пример тела запроса ###

```json
{
  "status": "OK",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "CHANGE_PROFILE",
  "id": 49,
  "creationDate": "2022-07-28T12:50:37",
  "startDate": "2022-07-28T12:50:37"
}
```