
Команда `DeleteSubscriber` позволяет удалять несколько абонентов из файла.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/DeleteSubscriber \
  -H 'Content-Type: text/csv' \
  -d @<path_to_file>.csv
```

В теле запроса передается файл формата CSV.

Формат файла:

```csv
imsi;msisdn;
```

### Поля файла

| Поле   | Описание               | Тип    | O/M |
|--------|------------------------|--------|-----|
| imsi   | Номер IMSI абонента.   | string | M   |
| msisdn | Номер MSISDN абонента. | string | О   |

### Пример файла

```csv
250010000000004;79110000004;
250010000000005;79110000005;
```

### Ответ ###

В ответе передаются параметры операции Campaign.

```
{
  "status": "<string>",
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "finishDate": "<datetime>",
  "startDate": "<datetime>",
  "campaignCounter": <CampaignCounter>
}
```

### Поля ответа ###

| Поле                                                          | Описание                                                   | Тип                                             | O/M | Версия   |
|---------------------------------------------------------------|------------------------------------------------------------|-------------------------------------------------|-----|----------|
| status                                                        | Статус запроса.                                            | string                                          | M   |          |
| campaignCounter                                               | Счетчик операций.                                          | [СampaignCounter](../entities/campaignCounter/) | M   | 2.0.56.0 |
| [campaignStatus](#campaign-status-delete-subscriber-campaign) | Статус операции.                                           | string                                          | M   | 2.0.56.0 |
| [campaignType](#campaign-type-delete-subscriber-campaign)     | Тип операции.                                              | string                                          | M   | 2.0.56.0 |
| id                                                            | Идентификатор  операции.                                   | int                                             | M   | 2.0.56.0 |
| creationDate                                                  | Дата создания операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`.  | datetime                                        | O   | 2.0.56.0 |
| startDate                                                     | Дата запуска операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`.   | datetime                                        | O   | 2.0.56.0 |
| finishDate                                                    | Дата окончания операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`. | datetime                                        | O   | 2.0.56.0 |

#### [[campaign-status-delete-subscriber-campaign]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

#### [[campaign-type-delete-subscriber-campaign]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |

### Пример ответа ###

```json
{
  "status": "OK",
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "DELETE_SUBSCRIBER",
  "id": 49,
  "creationDate": "2022-01-01T12:50:37",
  "startDate": "2022-01-01T12:50:37",
  "campaignCounter": {
    "total": 2,
    "success": 0,
    "error": 0
  }
}
```