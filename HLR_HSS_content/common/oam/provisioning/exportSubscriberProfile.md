
Команда `ExportSubscriberProfile` позволяет асинхронно экспортировать профили абонентов в файл.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/ExportSubscriberProfile \
  -d '{
  "filter": <SubscriberFilter>,
  "status": <bool>,
  "forbid_reg": <bool>,
  "odb-param": <bool>,
  "pdp-data": <bool>,
  "eps-context-data": <bool>,
  "eps-data": <bool>,
  "csi-list": <bool>,
  "ssData": <bool>,
  "ssForw": <bool>,
  "ssBarring": <bool>,
  "teleserviceList": <bool>,
  "bearerserviceList": <bool>,
  "roaming-info": <bool>,
  "roaming-sgsn-info": <bool>,
  "defaultForw": <bool>,
  "whiteLists": <bool>,
  "blackLists": <bool>,
  "lcs": <bool>,
  "tk": <bool>,
  "group": <bool>,
  "deactivatePsi": <bool>,
  "baocWithoutCamel": <bool>,
  "ipSmGw": <bool>,
  "networkAccessMode": <bool>,
  "fullWBL": <bool>,
  "fullCSI": <bool>,
  "qosGprsId": <bool>,
  "qosEpsId": <bool>,
  "accessRestrictionData": <bool>,
  "chargingCharacteristics": <bool>,
  "ims": <bool>,
  "imsFull": <bool>,
  "aMsisdn": <bool>,
  "periodicLauTimer": <bool>,
  "periodicRauTauTimer": <bool>,
  "HSM_ID": <bool>,
  "opc": <bool>,
  "ueUsageType": <bool>,
  "lastActivity": <bool>,
  "SCA": <bool>,
  "category": <bool>,
  "icsIndicator": <bool>,
  "comment": <bool>,
  "all": <bool>
}'
```

### Поля тела запроса ###

| Поле                    | Описание                                                                                                                             | Тип    | O/M | Версия  |
|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------|--------|-----|---------|
| filter                  | Фильтр абонентов. См. [SubscriberFilter](../entities/subscriberFilter/).                                                             | object | M   | 2.1.7.0 |
| status                  | Флаг запроса состояния абонента.                                                                                                     | bool   | O   | 2.1.7.0 |
| forbid_reg              | Флаг запроса значения параметра [forbidRegistration_WithoutCamel](../entities/subscriberProfile/#forbid-registration-without-camel). | bool   | O   | 2.1.7.0 |
| odb-param               | Флаг запроса параметров запретов, установленных оператором связи.                                                                    | bool   | O   | 2.1.7.0 |
| pdp-data                | Флаг запроса параметров PDP.                                                                                                         | bool   | O   | 2.1.7.0 |
| eps-context-data        | Флаг запроса параметров контекста EPS.                                                                                               | bool   | O   | 2.1.7.0 |
| eps-data                | Флаг запроса параметров EPS.                                                                                                         | bool   | O   | 2.1.7.0 |
| csi-list                | Флаг запроса параметров CSI-профиля.                                                                                                 | bool   | O   | 2.1.7.0 |
| ssData                  | Флаг запроса параметров дополнительной услуги, SS.                                                                                   | bool   | O   | 2.1.7.0 |
| ssForw                  | Флаг запроса параметров дополнительных услуг SS переадресации для абонента.                                                          | bool   | O   | 2.1.7.0 |
| ssBarring               | Флаг запроса параметров дополнительных услуг SS запрета для абонента.                                                                | bool   | O   | 2.1.7.0 |
| teleserviceList         | Флаг запроса списка телеуслуг абонента.                                                                                              | bool   | O   | 2.1.7.0 |
| bearerserviceList       | Флаг запроса списка служб передачи данных абонента.                                                                                  | bool   | O   | 2.1.7.0 |
| roaming-info            | Флаг запроса параметров роуминга абонента.                                                                                           | bool   | O   | 2.1.7.0 |
| roaming-sgsn-info       | Флаг запроса параметров роумингового узла SGSN абонента.                                                                             | bool   | O   | 2.1.7.0 |
| defaultForw             | Флаг запроса параметров переадресации по умолчанию для абонента.                                                                     | bool   | O   | 2.1.7.0 |
| whiteLists              | Флаг запроса белых списков абонента.                                                                                                 | bool   | O   | 2.1.7.0 |
| blackLists              | Флаг запроса черных списков абонента.                                                                                                | bool   | O   | 2.1.7.0 |
| lcs                     | Флаг запроса профиля LCS.                                                                                                            | bool   | O   | 2.1.7.0 |
| tk                      | Флаг запроса значения транспортного ключа.                                                                                           | bool   | O   | 2.1.7.0 |
| group                   | Флаг запроса параметров группы.                                                                                                      | bool   | O   | 2.1.7.0 |
| deactivatePsi           | Флаг запроса значения параметра `deactivatePsi`.                                                                                     | bool   | O   | 2.1.7.0 |
| baocWithoutCamel        | Флаг запроса значения параметра `baocWithoutCamel`.                                                                                  | bool   | O   | 2.1.7.0 |
| ipSmGw                  | Флаг запроса параметров узла IP-SM-GW.                                                                                               | bool   | O   | 2.1.7.0 |
| networkAccessMode       | Флаг запроса режима доступа к сети.                                                                                                  | bool   | O   | 2.1.7.0 |
| fullWBL                 | Флаг запроса всех белых и черных списков.                                                                                            | bool   | O   | 2.1.7.0 |
| fullCSI                 | Флаг запроса всех параметров CSI-профиля.                                                                                            | bool   | O   | 2.1.7.0 |
| qosGprsId               | Флаг запроса идентификатора профиля QoS GPRS.                                                                                        | bool   | O   | 2.1.7.0 |
| qosEpsId                | Флаг запроса идентификатора профиля QoS EPS.                                                                                         | bool   | O   | 2.1.7.0 |
| accessRestrictionData   | Флаг запроса прав доступа.                                                                                                           | bool   | O   | 2.1.7.0 |
| chargingCharacteristics | Флаг запроса значения `ChargingCharacteristics`.                                                                                     | bool   | O   | 2.1.7.0 |
| ims                     | Флаг запроса подписки в сетях IMS.                                                                                                   | bool   | O   | 2.1.7.0 |
| imsFull                 | Флаг запроса значений всех параметров подписки в сетях IMS.                                                                          | bool   | O   | 2.1.7.0 |
| aMsisdn                 | Флаг запроса дополнительных номеров MSISDN.                                                                                          | bool   | O   | 2.1.7.0 |
| periodicLauTimer        | Флаг запроса значения параметра `periodicLauTimer`.                                                                                  | bool   | O   | 2.1.7.0 |
| periodicRauTauTimer     | Флаг запроса значения параметра `periodicRauLauTimer`.                                                                               | bool   | O   | 2.1.7.0 |
| HSM_ID                  | Флаг запроса идентификатора модуля HSM.                                                                                              | bool   | O   | 2.1.7.0 |
| opc                     | Флаг запроса кода точки отправления OPC.                                                                                             | bool   | O   | 2.1.7.0 |
| ueUsageType             | Флаг запроса значения параметра ueUsageType.                                                                                         | bool   | O   | 2.1.7.0 |
| lastActivity            | Флаг запроса даты и времени последней активности.                                                                                    | bool   | O   | 2.1.7.0 |
| SCA                     | Флаг запроса адреса сервисного центра.                                                                                               | bool   | O   | 2.1.7.0 |
| category                | Флаг запроса категории абонента.                                                                                                     | bool   | O   | 2.1.7.0 |
| icsIndicator            | Флаг запроса значения параметра `icsIndicator`.                                                                                      | bool   | O   | 2.1.7.0 |
| comment                 | Флаг запроса информации об абоненте.                                                                                                 | bool   | O   | 2.1.9.0 |
| all                     | Флаг запроса значений всех параметров.                                                                                               | bool   | O   | 2.1.7.0 |

### Пример тела запроса ###

```json
{
  "filter": {
    "imsiMask": "25001%01",
    "sortDirection": "ASC",
    "sortField": "msisdn"
  },
  "all": true
}
```

### Ответ ###

В ответе передаются статус запроса и параметры операции.

```
{
  "status": "<string>",
  "campaignCounter": <CampaignCounter>,
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "startDate": "<datetime>"
}
```

### Поля ответа ###

| Поле                                                         | Описание                 | Тип                                            | O/M | Версия  |
|--------------------------------------------------------------|--------------------------|------------------------------------------------|-----|---------|
| status                                                       | Статус запроса.          | string                                         | M   | 2.1.7.0 |
| campaignCounter                                              | Счетчик операций.        | [СampaignCounter](../entities/campaignCounter) | M   | 2.1.7.0 |
| [campaignStatus](#campaign-status-export-subscriber-profile) | Статус операции.         | string                                         | M   | 2.1.7.0 |
| [campaignType](#campaign-type-export-subscriber-profile)     | Тип операции.            | string                                         | M   | 2.1.7.0 |
| id                                                           | Идентификатор  операции. | int                                            | M   | 2.1.7.0 |
| creationDate                                                 | Дата создания операции.  | datetime                                       | O   | 2.1.7.0 |
| startDate                                                    | Дата запуска операции.   | datetime                                       | O   | 2.1.7.0 |
| finishDate                                                   | Дата окончания операции. | datetime                                       | O   | 2.1.7.0 |

#### [[campaign-status-export-subscriber-profile]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

#### [[campaign-type-export-subscriber-profile]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |

### Пример ответа ###

```json
{
  "status": "OK",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "NEW",
  "campaignType": "EXPORT_SUBSCRIBER_PROFILE",
  "id": 1
}
```