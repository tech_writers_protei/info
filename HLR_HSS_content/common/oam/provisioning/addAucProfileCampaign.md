
Команда `AddAucProfile` позволяет добавить набор профилей AuC в базу данных HLR/HSS из файла.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileServiceCampaign/AddAucProfile?jsonData=%7B%22opId%22%3A<OpId>%2C%22cId%22%3A<CId>%7D
```

### Поля запроса query ###

| Поле                                              | Описание                                                    | Тип    | O/M |
|---------------------------------------------------|-------------------------------------------------------------|--------|-----|
| [[json-data-add-auc-campaign]]jsonData | Параметры запроса, передаваемые в открытом виде.            | object | M   |
| **{**                                             |                                                             |        |     |
| &nbsp;&nbsp;opId                                  | Идентификатор ключа оператора для алгоритма MILENAGE.       | int    | M   |
| &nbsp;&nbsp;cId                                   | Идентификатор аддитивной постоянной для алгоритма MILENAGE. | int    | M   |
| **}**                                             |                                                             |        |     |

**Примечание.** В поле [jsonData](#json-data-add-auc-campaign) передаются любые опциональные поля профиля AuC, [AucProfile](../entities/aucProfile/).

### Запрос в формате JSON ###

```bash
$ curl -X POST https://<host>:<port>/ProfileServiceCampaign/AddAucProfile \
  -d '{
  "opId": <int>,
  "cId": <int>
}'
```

В теле запроса передается файл формата CSV.

Формат файла:

```csv
imsi;ki;iccid;opc;
```

### Поля файла ###

| Поле  | Описание                                                                                                                                                            | Тип    | O/M |
|-------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi  | Номер IMSI абонента.                                                                                                                                                | string | M   |
| ki    | Секретный ключ Ki для аутентификации.                                                                                                                               | int    | M   |
| iccid | Идентификатор смарт-карты абонента.                                                                                                                                 | string | O   |
| opc   | Значение поля конфигурации алгоритма оператором. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf). | string | O   |

### Пример файла формата CSV

```csv
250010000000004;2cf6bd9062d921cc79f43b523d085233;
250010000000005;2cf6bd9062d921cc79f43b523d085233;
250010000000006;2cf6bd9062d921cc79f43b523d085233;
```

### Ответ ###

В ответе передаются статус операции и параметры операции Campaign.

```
{
  "status": "<string>",
  "campaignCounter": <CampaignCounter>,
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "startDate": "<datetime>",
  "finishDate": "<datetime>"
}
```

### Поля ответа ###

| Поле                                                | Описание                             | Тип                                             | O/M |
|-----------------------------------------------------|--------------------------------------|-------------------------------------------------|-----|
| status                                              | Статус запроса.                      | string                                          | M   |
| campaignCounter                                     | Счетчик операций.                    | [CampaignCounter](../entities/campaignCounter/) | M   |
| [campaignStatus](#campaign-status-add-auc-campaign) | Статус операции.                     | string                                          | M   |
| [campaignType](#campaign-type-add-auc-campaign)     | Тип операции.                        | string                                          | M   |
| id                                                  | Идентификатор операции.              | int                                             | M   |
| creationDate                                        | Дата создания операции.              | datetime                                        | O   |
| startDate                                           | Дата начала выполнения операции.     | datetime                                        | O   |
| finishDate                                          | Дата завершения выполнения операции. | datetime                                        | O   |

#### [[campaign-status-add-auc-campaign]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

#### [[campaign-type-add-auc-campaign]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |

### Пример ответа ###

```json
{
  "status": "OK",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "ADD_SUBSCRIBER_PROFILE",
  "id": 49,
  "creationDate": "2022-01-01T12:50:37",
  "startDate": "2022-01-01T12:50:37"
}
```