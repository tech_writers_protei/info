
Команда `ChangeStatus` позволяет изменять статус абонента на узле HLR/HSS.

При наличии активной регистрации на узлах VLR, SGSN или MME у абонента в состоянии Provisioned/unlocked она прекращается запросом MAP-CL или Diameter S6: CLR.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeStatus \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>",
  "status": <int>
}'
```

### Поля тела запроса ###

| Поле                                      | Описание                | Тип    | O/M |
|-------------------------------------------|-------------------------|--------|-----|
| [[imsi-change-status]]imsi     | Номер IMSI абонента.    | string | C   |
| [[msisdn-change-status]]msisdn | Номер MSISDN.           | string | C   |
| [status](#status-change-status)           | Код состояния абонента. | int    | M   |

**Примечание.** Если состояние меняется с "UNLOCKED" на любой другой, то с узла HLR отправляется запрос Send–Cancel–Location.

**Примечание.** Задается только один из параметров [imsi](#imsi-change-status) или [msisdn](#msisdn-change-status).

#### [[status-change-status]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | provisioned/unlocked  | В эксплуатации                                  |
| 3 | provisioned/suspended | Обслуживание приостановлено                     |
| 4 | terminated            | Абонент удален                                  |

### Пример тела запроса ###

```json
{
  "imsi": "250010000001",
  "status": 1
}
```

### Ответ ###

В ответе передается только статус запроса.