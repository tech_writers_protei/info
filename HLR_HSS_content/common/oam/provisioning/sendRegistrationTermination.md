
Команда `SendRegistrationTermination` позволяет отменять регистрацию в сетях IMS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SendRegistrationTermination \
  -d '{
  "impi": "<string>",
  "impu": "<string>"
}'
```

### Поля тела запроса ###

| Поле                                                  | Описание                                         | Тип    | O/M |
|-------------------------------------------------------|--------------------------------------------------|--------|-----|
| [[impi-send-registration-termination]]impi | Приватный идентификатор пользователя в сети IMS. | string | C   |
| [[impu-send-registration-termination]]impu | Публичный идентификатор пользователя в сети IMS. | string | C   |

**Примечание.** Задается только один из параметров [impi](#impi-send-registration-termination) и [impu](#impu-send-registration-termination).

### Пример тела запроса ###

```json
{ "impi": "250010000001@ims.protei.ru" }
```

### Ответ ###

В ответе передается только статус запроса.