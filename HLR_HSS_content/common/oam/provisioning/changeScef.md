
Команда `ChangeScef` позволяет изменять параметры узла SCEF.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeScef \
  -d '{
  "action": "<string>",
  "scef": <Scef>
}'
```

### Поля тела запроса ###

| Поле   | Описание                                                                         | Тип                       | O/M |
|--------|----------------------------------------------------------------------------------|---------------------------|-----|
| action | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                    | M   |
| scef   | Узел SCEF.                                                                       | [Scef](../entities/scef/) | M   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "scef": {
    "name": "scef1",
    "host": "scef.protei.ru",
    "realm": "protei.ru"
  }
}
```

### Ответ ###

В ответе передается только статус запроса.