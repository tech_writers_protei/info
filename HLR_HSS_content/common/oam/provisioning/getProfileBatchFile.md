
Команда `GetProfileBatchFile` позволяет получать перечень профилей абонентов.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/CustomService/GetProfileBatchFile \
  -d '{
  "filter": <SubscriberFilter>,
  "status": <bool>,
  "forbid_reg": <bool>,
  "odb-param": <bool>,
  "pdp-data"  <bool>,
  "eps-context-data": <bool>,
  "eps-data": <bool>,
  "csi-list": <bool>,
  "ssData": <bool>,
  "ssForw": <bool>,
  "ssBarring": <bool>,
  "teleserviceList": <bool>,
  "bearerserviceList": <bool>,
  "roaming-info": <bool>,
  "roaming-sgsn-info": <bool>,
  "defaultForw": <bool>,
  "whiteLists": <bool>,
  "blackLists": <bool>,
  "lcs": <bool>,
  "tk": <bool>,
  "group": <bool>,
  "deactivatePsi": <bool>,
  "baocWithoutCamel": <bool>,
  "ipSmGw": <bool>,
  "networkAccessMode": <bool>,
  "fullWBL": <bool>,
  "fullCSI": <bool>,
  "qosGprsId": <bool>,
  "qosEpsId": <bool>,
  "accessRestrictionData": <bool>,
  "chargingCharacteristics": <bool>,
  "ims": <bool>,
  "imsFull": <bool>,
  "aMsisdn": <bool>,
  "periodicLauTimer": <bool>,
  "periodicRauTauTimer": <bool>,
  "HSM_ID": <bool>,
  "opc": <bool>,
  "ueUsageType": <bool>,
  "lastActivity": <bool>,
  "SCA": <bool>,
  "category": <bool>,
  "icsIndicator": <bool>,
  "csToPsSrvccAllowedIndicator": <bool>,
  "nssai": <bool>,
  "externalId": <bool>,
  "comment": <bool>,
  "externalGroupIds": <bool>,
  "all": <bool>
}'
```

### Поля тела запроса ###

| Поле                        | Описание                                                                                                                             | Тип                                               | O/M | Версия   |
|-----------------------------|--------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------|-----|----------|
| filter                      | Параметры фильтра абонентов.                                                                                                         | [SubscriberFilter](../entities/subscriberFilter/) | M   | 2.0.56.3 |
| status                      | Флаг запроса статуса абонента.                                                                                                       | bool                                              | O   |          |
| forbid_reg                  | Флаг запроса значения параметра [forbidRegistration_WithoutCamel](../entities/subscriberProfile/#forbid-registration-without-camel). | bool                                              | O   |          |
| odb-param                   | Флаг запроса параметров запретов, установленных оператором связи.                                                                    | bool                                              | O   |          |
| pdp-data                    | Флаг запроса параметров PDP.                                                                                                         | bool                                              | O   |          |
| eps-context-data            | Флаг запроса параметров контекста EPS.                                                                                               | bool                                              | O   |          |
| eps-data                    | Флаг запроса параметров EPS.                                                                                                         | bool                                              | O   |          |
| csi-list                    | Флаг запроса параметров CSI-профиля.                                                                                                 | bool                                              | O   |          |
| ssData                      | Флаг запроса параметров дополнительной услуги, SS.                                                                                   | bool                                              | O   |          |
| ssForw                      | Флаг запроса параметров дополнительных услуг SS переадресации для абонента.                                                          | bool                                              | O   |          |
| ssBarring                   | Флаг запроса параметров дополнительных услуг SS запрета для абонента.                                                                | bool                                              | O   |          |
| teleserviceList             | Флаг запроса списка телеуслуг абонента.                                                                                              | bool                                              | O   |          |
| bearerserviceList           | Флаг запроса списка служб передачи данных абонента.                                                                                  | bool                                              | O   |          |
| roaming-info                | Флаг запроса параметров роуминга абонента.                                                                                           | bool                                              | O   |          |
| roaming-sgsn-info           | Флаг запроса параметров роумингового узла SGSN абонента.                                                                             | bool                                              | O   |          |
| defaultForw                 | Флаг запроса параметров переадресации по умолчанию для абонента.                                                                     | bool                                              | O   |          |
| whiteLists                  | Флаг запроса белых списков абонента.                                                                                                 | bool                                              | O   |          |
| blackLists                  | Флаг запроса черных списков абонента.                                                                                                | bool                                              | O   |          |
| lcs                         | Флаг запроса профиля LCS.                                                                                                            | bool                                              | O   |          |
| tk                          | Флаг запроса значения транспортного ключа.                                                                                           | bool                                              | O   |          |
| group                       | Флаг запроса параметров группы.                                                                                                      | bool                                              | O   |          |
| deactivatePsi               | Флаг запроса значения параметра `baocWithoutCamel`.                                                                                  | bool                                              | O   |          |
| baocWithoutCamel            | Флаг разрешения регистрации на узле VLR без поддержки CAMEL при наличии запрета роуминга.                                            | bool                                              | O   |          |
| ipSmGw                      | Флаг запроса параметров узла IP-SM-GW.                                                                                               | bool                                              | O   |          |
| networkAccessMode           | Флаг запроса режима доступа к сети.                                                                                                  | bool                                              | O   |          |
| fullWBL                     | Флаг запроса всех белых и черных списков.                                                                                            | bool                                              | O   |          |
| fullCSI                     | Флаг запроса всех параметров CSI-профиля.                                                                                            | bool                                              | O   |          |
| qosGprsId                   | Флаг запроса идентификатора профиля QoS GPRS.                                                                                        | bool                                              | O   |          |
| qosEpsId                    | Флаг запроса идентификатора профиля QoS EPS.                                                                                         | bool                                              | O   |          |
| accessRestrictionData       | Флаг запроса прав доступа.                                                                                                           | bool                                              | O   |          |
| chargingCharacteristics     | Флаг запроса значения `ChargingCharacteristics`.                                                                                     | bool                                              | O   |          |
| ims                         | Флаг запроса подписки в сетях IMS.                                                                                                   | bool                                              | O   |          |
| imsFull                     | Флаг запроса значений всех параметров подписки в сетях IMS.                                                                          | bool                                              | O   |          |
| aMsisdn                     | Флаг запроса дополнительных номеров MSISDN.                                                                                          | bool                                              | O   |          |
| periodicLauTimer            | Флаг запроса значения параметра `periodicLauTimer`.                                                                                  | bool                                              | O   |          |
| periodicRauTauTimer         | Флаг запроса значения параметра `periodicRauLauTimer`.                                                                               | bool                                              | O   |          |
| HSM_ID                      | Флаг запроса идентификатора модуля HSM.                                                                                              | bool                                              | O   |          |
| opc                         | Флаг запроса кода пункта отправления OPC.                                                                                            | bool                                              | O   |          |
| ueUsageType                 | Флаг запроса значения параметра `ueUsageType`.                                                                                       | bool                                              | O   |          |
| lastActivity                | Флаг запроса даты и времени последней активности.                                                                                    | bool                                              | O   |          |
| SCA                         | Флаг запроса адреса сервисного центра.                                                                                               | bool                                              | O   |          |
| category                    | Флаг запроса категории абонента.                                                                                                     | bool                                              | O   |          |
| icsIndicator                | Флаг запроса значения параметра `icsIndicator`.                                                                                      | bool                                              | O   |          |
| csToPsSrvccAllowedIndicator | Флаг запроса значения параметра `csToPsSrvccIndicator`.                                                                              | bool                                              | O   | 2.0.35.0 |
| nssai                       | Флаг запроса значения параметра NSSAI.                                                                                               | bool                                              | O   | 2.1.4.0  |
| externalId                  | Флаг запроса внешнего идентификатора.                                                                                                | bool                                              | O   | 2.1.7.0  |
| comment                     | Флаг запроса дополнительной информации.                                                                                              | bool                                              | O   | 2.1.9.0  |
| externalGroupIds            | Флаг запроса идентификатора внешних групп.                                                                                           | bool                                              | O   | 2.1.10.0 |
| all                         | Флаг запроса значений всех параметров.                                                                                               | bool                                              | O   |          |

### Ответ ###

В ответе передается только файл в формате CSV.

Формат файла:

```csv
Imsi;Msisdn;AdministrativeState;LastActivity;RoamingNotAllowed;LcsId;TkId;
GroupId;QosGprsId;QosEpsId;Opc;
{SS_Id;SS_SsStatus;SS_ForwardingAddress;SS_ForwardingOptions;SS_SubscriberId;
SS_ServiceType;SS_BasicService;SS_SsCode;SS_ForwardedToNumber;
SS_ForwardingAddress;SS_ForwardingOptions;SS_NoreplyConditionTime;
SS_LongForwardingNumber;SS_IsUpdate;SS_NewTeleservice;};
{Barring_getId;Barring_getSubscriberId;Barring_getServiceType;
Barring_getBasicService;Barring_getSsCode;Barring_getSsStatus;
Barring_getIsUpdate;Barring_getNewTeleService;};
{SSDATA_SubscriberId;SSDATA_SsCode;SSDATA_SsStatus;SSDATA_Type;
SSDATA_SubscriptionOption;{teleService;};{bearerService;};
SSDATA_NewTeleService;SSDATA_ImsSubscriptionOption;};
```

### Поля ответа ###

| Поле                                      | Описание                                                                                                                                                                       | Тип      | O/M |
|-------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|
| Imsi                                      | Номер IMSI абонента.                                                                                                                                                           | string   | M   |
| Msisdn                                    | Номер MSISDN абонента.                                                                                                                                                         | string   | O   |
| AdministrativeState                       | Код состояния абонента.                                                                                                                                                        | string   | O   |
| LastActivity                              | Дата и время последней активности абонента.                                                                                                                                    | datetime | O   |
| RoamingNotAllowed                         | Флаг запрета роуминга.                                                                                                                                                         | bool     | O   |
| LcsId                                     | Идентификатор профиля LCS.                                                                                                                                                     | int      | O   |
| TkId                                      | Идентификатор транспортного ключа для алгоритма MILENAGE.                                                                                                                      | int      | O   |
| GroupId                                   | Идентификатор группы абонентов.                                                                                                                                                | int      | O   |
| QosGprsId                                 | Идентификатор профиля QoS GPRS.                                                                                                                                                | string   | O   |
| QosEpsId                                  | Идентификатор профиля QoS EPS.                                                                                                                                                 | string   | O   |
| Opc                                       | Код сигнальной точки отправления.                                                                                                                                              | string   | O   |
| **{**                                     |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;SS_Id                         | Идентификатор дополнительной услуги SS.                                                                                                                                        | int      | O   |
| &nbsp;&nbsp;SS_Status                     | Статус дополнительной услуги SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                            | string   | O   |
| &nbsp;&nbsp;SS_ForwardingAddress          | Адрес для перенаправления дополнительной услуги SS.                                                                                                                            | string   | O   |
| &nbsp;&nbsp;SS_ForwardingOptions          | Параметры переадресации. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                    | int      | O   |
| &nbsp;&nbsp;SS_SubscriberId               | Идентификатор абонента дополнительной услуги SS.                                                                                                                               | int      | O   |
| &nbsp;&nbsp;SS_ServiceType                | Тип дополнительной услуги SS.                                                                                                                                                  | string   | O   |
| &nbsp;&nbsp;SS_BasicService               | Базовая дополнительная услуга SS.                                                                                                                                              | string   | O   |
| &nbsp;&nbsp;SS_SsCode                     | Код дополнительной услуги SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                               | int      | O   |
| &nbsp;&nbsp;SS_ForwardedToNumber          | Номер для перенаправления дополнительной услуги SS.                                                                                                                            | string   | O   |
| &nbsp;&nbsp;SS_NoreplyConditionTime       | Флаг ограничения времени ожидания ответа.                                                                                                                                      | bool     | O   |
| &nbsp;&nbsp;SS_LongForwardingNumber       | Длинный номер для перенаправления дополнительной услуги SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf). | string   | O   |
| &nbsp;&nbsp;SS_IsUpdate                   | Флаг обновления дополнительной услуги SS.                                                                                                                                      | bool     | O   |
| &nbsp;&nbsp;SS_NewTeleservice             | Идентификатор новой телеуслуги.                                                                                                                                                | int      | O   |
| **}**                                     |                                                                                                                                                                                |          |     |
| **{**                                     |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;Barring_getId                 | Идентификатор дополнительной услуги запрета SS_Barring.                                                                                                                        | int      | O   |
| &nbsp;&nbsp;Barring_getSubscriberId       | Идентификатор абонента для дополнительной услуги запрета SS_Barring.                                                                                                           | int      | O   |
| &nbsp;&nbsp;Barring_getServiceType        | Тип дополнительной услуги запрета SS_Barring.                                                                                                                                  | string   | O   |
| &nbsp;&nbsp;Barring_getBasicService       | Базовая дополнительная услуга запрета SS_Barring.                                                                                                                              | string   | O   |
| &nbsp;&nbsp;Barring_getSsCode             | Код дополнительной услуги запрета SS_Barring.                                                                                                                                  | int      | O   |
| &nbsp;&nbsp;Barring_getSsStatus           | Статус дополнительной услуги запрета SS_Barring.                                                                                                                               | string   | O   |
| &nbsp;&nbsp;Barring_getIsUpdate           | Флаг обновления дополнительной услуги запрета SS_Barring.                                                                                                                      | bool     | O   |
| &nbsp;&nbsp;Barring_getNewTeleService     | Идентификатор новой телеуслуги.                                                                                                                                                | int      | O   |
| **}**                                     |                                                                                                                                                                                |          |     |
| **{**                                     |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;SSDATA_SubscriberId           | Идентификатор абонента.                                                                                                                                                        | int      | O   |
| &nbsp;&nbsp;SSDATA_SsCode                 | Код дополнительной услуги SS.                                                                                                                                                  | int      | O   |
| &nbsp;&nbsp;SSDATA_SsStatus               | Статус дополнительной услуги SS.                                                                                                                                               | string   | O   |
| &nbsp;&nbsp;SSDATA_Type                   | Тип дополнительной услуги SS.                                                                                                                                                  | string   | O   |
| &nbsp;&nbsp;SSDATA_SubscriptionOption     | Параметр подписки.                                                                                                                                                             | int      | O   |
| &nbsp;&nbsp;**{**                         |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;&nbsp;&nbsp;teleService       | Идентификатор телеуслуги.                                                                                                                                                      | int      | O   |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;{                             |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;&nbsp;&nbsp;bearerService     | Идентификатор службы передачи данных.                                                                                                                                          | int      | O   |
| &nbsp;&nbsp;**}**                         |                                                                                                                                                                                |          |     |
| &nbsp;&nbsp;SSDATA_NewTeleService         | Идентификатор новой телеуслуги.                                                                                                                                                | int      | O   |
| &nbsp;&nbsp;SSDATA_ImsSubscriptionOptione | Параметр подписки IMS.                                                                                                                                                         | int      | O   |
| **}**                                     |                                                                                                                                                                                |          |     |

### Пример ответа ###

```csv
250000000000001;79000000001;null;null;null;null;null;null;null;null;null;;;;
250000000000002;79000000002;null;null;null;null;null;null;null;null;null;;;;
```