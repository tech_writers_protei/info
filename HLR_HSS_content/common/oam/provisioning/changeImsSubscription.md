
Команда `ChangeImsSubscription` позволяет изменять подписку в сетях IMS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeImsSubscription \
  -d '{
  "action": "<string>",
  "imsSubscription": <ImsSubscription>
}'
```

### Поля тела запроса ###

| Поле            | Описание                                                                         | Тип                                             | O/M |
|-----------------|----------------------------------------------------------------------------------|-------------------------------------------------|-----|
| action          | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                          | M   |
| imsSubscription | Подписка IMS.                                                                    | [ImsSubscription](../entities/imsSubscription/) | O   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "imsSubscription": {
    "name": "imsu",
    "capabilitySetId": 1,
    "prefferedScscfSetId": 1,
    "chargingInformationName": "ci",
    "implicitlyRegisteredSet": [
      {
        "name": "implSet1",
        "impus": [
          {
            "Identity": "sip:protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp",
            "Default": true
          },
          {
            "Identity": "sip:ntc_protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp"
          }
        ]
      }
    ],
    "serviceProfiles": [
      {
        "Name": "sp",
        "CoreNetworkServiceAuthorization": 1,
        "Ifcs": [
          {
            "Name": "ifc1",
            "Priority": 1,
            "ApplicationServerName": "as",
            "ProfilePartIndicator": 1,
            "TriggerPoint": {
              "ConditionTypeCNF": 1,
              "Spt": [
                {
                  "Group": 1,
                  "Method": "INVITE",
                  "SessionCase": 1,
                  "ConditionNegated": 2,
                  "Type": 3,
                  "RequestUri": "http://ims.protei.ru/spt1",
                  "Header": "header",
                  "Content": "headerContent",
                  "SdpLine": "sdpLine",
                  "SdpLineContent": "sdpLineContent",
                  "RegistrationType": 1
                }
              ]
            }
          }
        ]
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.