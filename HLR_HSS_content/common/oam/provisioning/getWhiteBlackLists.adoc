Команда `GetWhiteBlackLists` позволяет получать белые и черные списки.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetWhiteBlackLists \
  -d '{
  "type": "<string>",
  "name": "<string>"
}'
----

== Поля тела запроса

[width="100%",cols="47%,39%,9%,5%",options="header",]
|===
|Поле |Описание |Тип |O/M
|type |Тип списка. `white` / `black`. |string |M
|[[id-get-wb-lists]]id |Идентификационный номер списка. |int |C
|[[name-get-wb-lists]]name |Название списка. |string |C
|===

*Примечание.* Задается только один из параметров link:#id-get-wb-lists[id] и link:#name-get-wb-lists[name].

== Пример тела запроса

[source,json]
----
{
  "type": "white",
  "name": "wl_rus"
}
----

== Ответ

В ответе передаются статус запроса и белый или черный список.

....
{
  "status": "<string>",
  "blackList": <BlackList>,
  "whiteList": <WhiteList>
}
....

== Поля ответа

[width="100%",cols="17%,24%,52%,7%",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|blackList |Черный список. |link:../entities/blackList/[BlackList] |С
|whiteList |Белый список. |link:../entities/whiteList/[WhiteList] |С
|===

== Пример ответа

[source,json]
----
{
  "status": "OK",
  "blackList": {}
}
----
