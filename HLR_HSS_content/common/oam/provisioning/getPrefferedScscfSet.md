
Команда `GetPrefferedScscfSet` позволяет получать набор предпочитаемых узлов S-CSCF.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetPrefferedScscfSet \
  -d '{ "SetId": <int> }'
```

### Поля тела запроса ###

| Поле  | Описание                                          | Тип | O/M |
|-------|---------------------------------------------------|-----|-----|
| SetId | Идентификатор набора предпочитаемых узлов S-CSCF. | int | M   |

### Пример тела запроса ###

```json
{ "SetId": 1 }
```

### Ответ ###

В ответе передаются статус запроса и набор предпочитаемых узлов S-CSCF.

```
{
  "status": "<string>",
  "PrefferedScscfSet": <PrefferedScscfSet>
}
```

### Поля ответа ###

| Поле              | Описание                           | Тип                                                 | O/M |
|-------------------|------------------------------------|-----------------------------------------------------|-----|
| status            | Статус запроса.                    | string                                              | M   |
| PrefferedScscfSet | Набор предпочитаемых узлов S-CSCF. | [PrefferedScscfSet](../entities/prefferedScscfSet/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "PrefferedScscfSet": {}
}
```