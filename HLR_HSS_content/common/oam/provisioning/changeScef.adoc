Команда `ChangeScef` позволяет изменять параметры узла SCEF.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeScef \
  -d '{
  "action": "<string>",
  "scef": <Scef>
}'
----

== Поля тела запроса

[width="100%",cols="7%,67%,22%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M
|action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|scef |Узел SCEF. |link:../entities/scef/[Scef] |M
|===

== Пример тела запроса

[source,json]
----
{
  "action": "create",
  "scef": {
    "name": "scef1",
    "host": "scef.protei.ru",
    "realm": "protei.ru"
  }
}
----

== Ответ

В ответе передается только статус запроса.
