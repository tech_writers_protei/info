
Команда `ControlAs` позволяет изменять параметры сервера приложений.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ControlAs \
  -d '{
  "Action": "<string>",
  "As": <ApplicationServer>
}
```

### Поля тела запроса ###

| Поле   | Описание                                                                         | Тип                                                 | O/M |
|--------|----------------------------------------------------------------------------------|-----------------------------------------------------|-----|
| Action | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                              | M   |
| As     | Параметры сервера приложений.                                                    | [ApplicationServer](../entities/applicationServer/) | M   |

### Пример тела запроса ###

```json
{
  "Action": "create",
  "As": {
    "ServerName": "as",
    "DefaultHandling": 1,
    "ServiceInfo": "serviceInfo",
    "Host": "as.ims.protei.ru",
    "Realm": "ims.protei.ru",
    "RepositoryDataSizeLimit": 1000,
    "Udr": 1,
    "Pur": 1,
    "Snr": 1,
    "UdrRepositoryData": 1,
    "UdrImpu": 1,
    "UdrImsUserState": 1,
    "UdrScscfName": 1,
    "UdrIfc": 1,
    "UdrLocation": 1,
    "UdrUserState": 1, 
    "UdrChargingInfo": 1,
    "UdrMsisdn": 1,
    "UdrPsiActivation": 1,
    "UdrDsai": 1,
    "UdrAliasesRepositoryData": 1,
    "PurRepositoryData": 0,
    "PurPsiActivation": 0,
    "PurDsai": 0,
    "PurAliasesRepositoryData": 0,
    "SnrRepositoryData": 0,
    "SnrImpu": 0,
    "SnrImsUserState": 0, 
    "SnrScscfName": 0,
    "SnrIfc": 0,
    "SnrPsiActivation": 0,
    "SnrDsai": 0,
    "SnrAliasesRepositoryData": 0,
    "IncludeRegisterResponse": 1,
    "IncludeRegisterRequest": 1
  }
}
```

### Ответ ###

В ответе передается только статус запроса.