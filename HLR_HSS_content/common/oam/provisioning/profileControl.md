
Команда `ProfileControl` позволяет изменять профиль в базе данных HLR/HSS.
При наличии активной регистрации на узле VLR или SGSN могут быть отправлены запросы MAP-ISD, MAP-DSD или MAP-CL ввиду изменения данных.
При наличии активной регистрации на узле MME или SGSN и изменении параметров EPS могут быть отправлены запросы Diameter S6: IDR, DSR или CLR.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ProfileControl \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>",
  "newMsisdn": "<string>",
  "status": "<string>",
  "category": <int>,
  "networkAccessMode": <int>,
  "DefaultForwardingNumber": "<string>",
  "DefaultForwardingStatus": <int>,
  "groupId": <int>,
  "ipSmGwNumber": "<string>",
  "ipSmGwHost": "<string>",
  "ipSmGwRealm": "<string>",
  "deactivatePsi": <bool>,
  "baocWithoutCamel": <bool>,
  "forbid_reg": <bool>,
  "qosGprsId": <int>,
  "qosEpsId": <int>,
  "roamingNotAllowed": <bool>,
  "accessRestrictionData": <object>,
  "periodicLauTimer": <int>,
  "periodicRauTauTimer": <int>,
  "icsIndicator": <bool>,
  "amfUmts": "<string>",
  "amfLTE": "<string>",
  "amfIms": "<string>",
  "SCA": "<string>",
  "ueUsageType": <int>,
  "imrn": "<string>",
  "voLte": <bool>,
  "opc": "<string>",
  "HSM_ID": <int>,
  "ssData": <list>,
  "ssForw": <list>,
  "ssForw": <list>,
  "ssBarring": <list>,
  "ssCugFeat": <list>,
  "ssCugSub": <list>,
  "EpsData": <object>,
  "lcsId": <int>,
  "PdpData": <object>,
  "link-eps-data": <list>,
  "csi-list": <list>,
  "teleserviceList": <list>,
  "teleservicesToAdd": <list>,
  "teleservicesToDelete": <list>,
  "bearerserviceList": <list>,
  "bearerservicesToAdd": <list>,
  "bearerservicesToDelete": <list>,
  "whiteListIds": <list>,
  "whiteListIdsToAdd": <list>,
  "whiteListIdsToDelete": <list>,
  "blackListIds": <list>,
  "blackListIdsToAdd": <list>,
  "blackListIdsToDelete": <list>,
  "whiteListNames": <list>,
  "whiteListNamesToAdd": <list>,
  "whiteListNamesToDelete": <list>,
  "blackListNames": <list>,
  "blackListNamesToAdd": <list>,
  "blackListNamesToDelete": <list>,
  "ODB": <object>,
  "chargingCharacteristics": "<string>",
  "aMsisdn": "<string}",
  "imsProfile": <object>,
  "imsSubscription": <object>,
  "regionalZoneCodes": <list>,
  "csToPsSrvccIndicator": <bool>,
  "singleNssais": <list>
}'
```

### Поля тела запроса ###

| Поле                                               | Описание                                                                                                                                                                                                           | Тип                                                         | O/M |
|----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------|-----|
| imsi                                               | Номер IMSI абонента.                                                                                                                                                                                               | string                                                      | C   |
| msisdn                                             | Текущий номер MSISDN.                                                                                                                                                                                              | string                                                      | С   |
| newMsisdn                                          | Новый номер MSISDN.                                                                                                                                                                                                | string                                                      | О   |
| [status](#status-profile-control)                  | Код состояния абонента.                                                                                                                                                                                            | int                                                         | О   |
| category                                           | Категория вызывающего абонента, `CgPC`. См. [Recommendation&nbsp;ITU-T&nbsp;Q.767](https://www.itu.int/rec/T-REC-Q.767-199102-I/en).pass:q[\<br\>]По умолчанию: 10.                                                          | int                                                         | О   |
| networkAccessMode                                  | Код режима работы.pass:q[\<br\>]`0` -- MSC + SGSN; `1` -- только MSC; `2` -- только SGSN.                                                                                                                                    | int                                                         | О   |
| DefaultForwardingNumber                            | Номер переадресации по умолчанию.                                                                                                                                                                                  | string                                                      | О   |
| [DefaultForwardingStatus](#status-profile-control) | Код состояния для переадресации.                                                                                                                                                                                   | int                                                         | О   |
| groupId                                            | Идентификатор абонентской группы.                                                                                                                                                                                  | [ValueControl\<int\>](#value-control)                       | О   |
| ipSmGwNumber                                       | Номер узла IP–SM–GW.                                                                                                                                                                                               | string                                                      | О   |
| ipSmGwHost                                         | Хост узла IP–SM–GW.                                                                                                                                                                                                | string                                                      | О   |
| ipSmGwRealm                                        | Реалм узла IP–SM–GW.                                                                                                                                                                                               | string                                                      | О   |
| deactivatePsi                                      | Флаг отказа от отправки сообщения MAP-PSI при получении запроса MAP-SRI.pass:q[\<br\>]По умолчанию: 0.                                                                                                                       | bool                                                        | О   |
| baocWithoutCamel                                   | Флаг разрешения регистрации на узле VLR без поддержки CAMEL при наличии запрета роуминга.                                                                                                                          | bool                                                        | О   |
| forbid_reg                                         | Флаг запрета регистрации на узле VLR/SGSN без поддержки фаз CAMEL.                                                                                                                                                 | bool                                                        | О   |
| qosGprsId                                          | Идентификатор профиля QoS для сетей GPRS. Формат параметра приведен ниже.                                                                                                                                          | [ValueControl\<int\>](#value-control)                       | О   |
| qosEpsId                                           | Идентификатор профиля QoS для сетей EPS.  Формат параметра приведен ниже.                                                                                                                                          | [ValueControl\<int\>](#value-control)                       | О   |
| roamingNotAllowed                                  | Флаг запрета роуминга.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                                                         | bool                                                        | О   |
| accessRestrictionData                              | Ограничения доступа.                                                                                                                                                                                               | [AccessRestrictionData](../entities/accessRestrictionData/) | О   |
| periodicLauTimer                                   | Период между отправками запросов Location–Area–Update, в секундах.                                                                                                                                                 | [ValueControl\<int\>](#value-control)                       | О   |
| periodicRauTauTimer                                | Период между отправками запросов Routing–Area–Update и Tracking–Area–Update, в секундах.                                                                                                                           | [ValueControl\<int\>](#value-control)                       | О   |
| icsIndicator                                       | Индикатор централизованных служб, Centralized Services, сетей IMS.                                                                                                                                                 | bool                                                        | О   |
| amfUmts                                            | Поле управления аутентификацией в сетях UMTS.                                                                                                                                                                      | [ValueControl\<string\>](#value-control)                    | О   |
| amfLte                                             | Поле управления аутентификацией в сетях LTE.                                                                                                                                                                       | [ValueControl\<string\>](#value-control)                    | О   |
| amfIms                                             | Поле управления аутентификацией в сетях IMS.                                                                                                                                                                       | [ValueControl\<string\>](#value-control)                    | О   |
| SCA                                                | Адрес сервисного центра.                                                                                                                                                                                           | string                                                      | О   |
| ueUsageType                                        | Значение `UE-Usage-Type` для выбора выделенной опорной сети. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).pass:q[\<br\>]Диапазон: 0–127. | int                                                         | О   |
| imrn                                               | Номер маршрутизации в сетях IMS.                                                                                                                                                                                   | string                                                      | О   |
| voLte                                              | Флаг вызова с помощью Voice over LTE.                                                                                                                                                                              | bool                                                        | О   |
| opc                                                | Код сигнальной точки отправления.                                                                                                                                                                                  | string                                                      | О   |
| HSM_ID                                             | Идентификатор модуля HSM.                                                                                                                                                                                          | int                                                         | О   |
| ssData                                             | Перечень дополнительных услуг.                                                                                                                                                                                     | [[SsData](../entities/ssData/)]                             | О   |
| ssForw                                             | Перечень дополнительных услуг переадресации.                                                                                                                                                                       | [[SsForwarding](../entities/ssForwarding/)]                 | О   |
| ssBarring                                          | Перечень дополнительных услуг запрета.                                                                                                                                                                             | [[SsBarring](../entities/ssBarring/)]                       | О   |
| ssCugFeat                                          | Перечень свойств дополнительной услуги CUG.                                                                                                                                                                        | [[SsCugFeat](../entities/ssCugFeat/)]                       | О   |
| ssCugSub                                           | Перечень подписок на дополнительную услугу CUG.                                                                                                                                                                    | [[SsCugSub](../entities/ssCugSub/)]                         | О   |
| eps-data                                           | Измененные параметры EPS.                                                                                                                                                                                          | [ChangeEpsData](../entities/ChangeEpsData/)                 | О   |
| lcsId                                              | Идентификатор профиля LCS.                                                                                                                                                                                         | [ValueControl\<int\>](#value-control)                       | О   |
| pdp-data                                           | Параметры PDP.                                                                                                                                                                                                     | [PdpData](../entities/PdpData/)                             | О   |
| pdpContexts                                        | Перечень параметров контекста PDP.                                                                                                                                                                                 | [[PdpData](../entities/PdpData/)]                           | О   |
| link-eps-data                                      | Параметры контекста EPS.                                                                                                                                                                                           | [LinkEpsData](../entities/linkEpsData/)                     | О   |
| epsContexts                                        | Перечень параметров контекста EPS.                                                                                                                                                                                 | [[LinkEpsData](../entities/linkEpsData/)]                   | О   |
| csi-list                                           | Перечень профилей CSI.                                                                                                                                                                                             | [[CSI](../entities/csiLink/)]                               | О   |
| teleserviceList                                    | Перечень идентификаторов телеуслуг. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                                              | [int]                                                       | О   |
| teleservicesToAdd                                  | Перечень идентификаторов телеуслуг, добавляемых абоненту. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                        | [int]                                                       | О   |
| teleservicesToDelete                               | Перечень идентификаторов телеуслуг, удаляемых у абонента. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                        | [int]                                                       | О   |
| bearerserviceList                                  | Перечень идентификаторов служб передачи данных. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                                  | [int]                                                       | О   |
| bearerservicesToAdd                                | Перечень идентификаторов служб передачи данных, добавляемых абоненту. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).            | [int]                                                       | О   |
| bearerservicesToDelete                             | Перечень идентификаторов служб передачи данных, удаляемых у абонента. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).            | [int]                                                       | О   |
| whiteListIds                                       | Перечень идентификаторов белых списков.                                                                                                                                                                            | [int]                                                       | О   |
| whiteListIdsToAdd                                  | Перечень идентификаторов белых списков, добавляемых абоненту.                                                                                                                                                      | [int]                                                       | О   |
| whiteListIdsToDelete                               | Перечень идентификаторов белых списков, удаляемых у абонента.                                                                                                                                                      | [int]                                                       | О   |
| blackListIds                                       | Перечень идентификаторов черных списков.                                                                                                                                                                           | [int]                                                       | О   |
| blackListIdsToAdd                                  | Перечень идентификаторов черных списков, добавляемых абоненту.                                                                                                                                                     | [int]                                                       | О   |
| blackListIdsToDelete                               | Перечень идентификаторов черных списков, удаляемых у абонента.                                                                                                                                                     | [int]                                                       | О   |
| whiteListNames                                     | Перечень имен в белом списке.                                                                                                                                                                                      | [string]                                                    | О   |
| whiteListNamesToAdd                                | Перечень имен, которые необходимо добавить в белый список абонента.                                                                                                                                                | [string]                                                    | О   |
| whiteListNamesToDelete                             | Перечень имен, которые необходимо удалить из белого списка абонента.                                                                                                                                               | [string]                                                    | О   |
| blackListNames                                     | Перечень имен в черном списке.                                                                                                                                                                                     | [string]                                                    | О   |
| blackListNamesToAdd                                | Перечень имен, которые необходимо добавить в черный список абонента.                                                                                                                                               | [string]                                                    | О   |
| blackListNamesToDelete                             | Перечень имен, которые необходимо удалить из черного списка абонента.                                                                                                                                              | [string]                                                    | О   |
| ODB                                                | Запреты, установленные оператором связи.                                                                                                                                                                           | [ODB](../entities/odb/)                                     | О   |
| chargingCharacteristics                            | Значение параметра `ChargingCharacteristics`.                                                                                                                                                                      | ValueControl\<string\>                                      | О   |
| aMsisdn                                            | Перечень дополнительных номеров MSISDN.                                                                                                                                                                            | [ValueControl\<string\>]                                    | О   |
| imsProfile                                         | Профиль IMS.                                                                                                                                                                                                       | [ImsProfile](../entities/ImsProfile/)                       | О   |
| imsSubscription                                    | Подписка IMS.                                                                                                                                                                                                      | [ImsSubscription](../entities/ImsSubscription/)             | О   |
| regionalZoneCodes                                  | Перечень региональных зон, связанных с профилем.                                                                                                                                                                   | [[RegionalZoneCodeLink](../entities/RegionalZoneCodeLink/)] | О   |
| csToPsSrvccIndicator                               | Флаг передачи голосовой части из домена CS в домен PS.                                                                                                                                                             | bool                                                        | О   |
| singleNssais                                       | Перечень подписок S-NSSAI.                                                                                                                                                                                         | [[SubSnssai](../entities/subSnssai/)]                       |
| externalId                                         | Идентификатор для IOT устройств.                                                                                                                                                                                   | string                                                      |
| comment                                            | Комментарий к профилю абонента.                                                                                                                                                                                    | string                                                      |
| externalGroupIds                                   | Перечень групповых идентификаторов IoT-устройств.                                                                                                                                                                  | [string]                                                    |

#### [[value-control]]Формат ValueControl

| Поле   | Описание                          | Тип        | O/M |
|--------|-----------------------------------|------------|-----|
| value  | Значение параметра.               | int/string | О   |
| delete | Флаг удаления значения параметра. | bool       | О   |

#### [[status-profile-control]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | provisioned/unlocked  | В эксплуатации                                  |
| 3 | provisioned/suspended | Обслуживание приостановлено                     |
| 4 | terminated            | Абонент удален                                  |

### Пример тела запроса ###

```json
{
  "imsi": "250010000001",
  "status": 2,
  "category": 10,
  "networkAccessMode": 0,
  "DefaultForwardingNumber": "867349752",
  "DefaultForwardingStatus": 7,
  "ipSmGwNumber": "23525252",
  "EpsData": {
    "defContextId": 1,
    "ueMaxDl": 10000,
    "ueMaxUl": 10000
  },
  "link-eps-data": [
    {
      "context-id": 2,
      "ipv4": "192.168.1.22",
      "plmnId": "25001"
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 0, 16, 32 ]
    },
    {
      "ss_Code": 65,
      "ss_Status": 5
    }
  ],
  "ssForw": [
    {
      "ss_Code": 42,
      "ss_Status": 7,
      "forwardedToNumber": "42513466754",
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "PdpData": [
    {
      "context-id": 1,
      "type": "0080"
    }
  ],
  "ODB": {
    "generalSetList": [ 1, 6, 19 ],
    "generalUnsetList": [ 2, 5 ],
    "SubscriberStatus": 1
  },
  "imsProfile": {
    "impi": "250010000001@ims.protei.ru",
    "authScheme": 5,
    "sipDigest": {
      "password": "pass"
    },
    "impus": [
      {
        "Identity": "sip:79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 0,
        "CanRegister": 1,
        "ServiceProfileName": "sp"
      },
      {
        "Identity": "tel:+79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 1,
        "CanRegister": 1,
        "WildcardPsi": "tel:+79000000001!.*!",
        "PsiActivation": 1,
        "ServiceProfileName": "sp"
      }
    ],
    "implicitlySets": [
      { "name": "implSet1" }
    ]
  },
  "imsSubscription": {
    "name": "250010000001",
    "capabilitySetId": 1,
    "prefferedScscfSetId": 1,
    "chargingInformationName": "ci",
    "serviceProfiles": [
      {
        "Name": "sp",
        "CoreNetworkServiceAuthorization": 1,
        "Ifcs": [
          {
            "Name": "ifc1",
            "Priority": 1,
            "ApplicationServerName": "as",
            "ProfilePartIndicator": 1,
            "TriggerPoint": {
              "ConditionTypeCNF": 1,
              "Spt": [
                {
                  "Group": 1,
                  "Method": "INVITE",
                  "SessionCase": 1,
                  "ConditionNegated": 2,
                  "Type": 3,
                  "RequestUri": "http://ims.protei.ru/spt1",
                  "Header": "header",
                  "Content": "headerContent",
                  "SdpLine": "sdpLine",
                  "SdpLineContent": "sdpLineContent",
                  "RegistrationType": 1
                }
              ]
            }
          }
        ]
      }
    ],
    "implicitlyRegisteredSet": [
      {
        "name": "implSet1",
        "impus": [
          {
            "Identity": "sip:protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp",
            "Default": true
          },
          {
            "Identity": "sip:ntc_protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp"
          }
        ]
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.