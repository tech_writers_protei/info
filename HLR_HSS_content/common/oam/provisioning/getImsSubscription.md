
Команда `GetImsSubscription` позволяет получать подписку в сетях IMS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetImsSubscription \
  -d '{
  "id": <int>,
  "name": "<string>",
  "impi": "<string>",
  "impu": "<string>",
  "imsi": "<string>",
  "msisdn": "<string>"
  "full": <bool>
}'
```

### Поля тела запроса ###

| Поле                                             | Описание                                         | Тип    | O/M |
|--------------------------------------------------|--------------------------------------------------|--------|-----|
| [[id-get-ims-subscription]]id         | Идентификатор подписки.                          | int    | C   |
| [[name-get-ims-subscription]]name     | Название подписки.                               | string | C   |
| [[impi-get-ims-subscription]]impi     | Приватный идентификатор пользователя в сети IMS. | string | C   |
| [[impu-get-ims-subscription]]impu     | Публичный идентификатор пользователя в сети IMS. | string | C   |
| [[imsi-get-ims-subscription]]imsi     | Номер IMSI пользователя.                         | string | C   |
| [[msisdn-get-ims-subscription]]msisdn | Номер MSISDN пользователя.                       | string | C   |
| full                                             | Флаг запроса всей информации.                    | bool   | O   |

**Примечание.** Задается только один из параметров [id](#id-get-ims-subscription), [name](#name-get-ims-subscription), [impi](#impi-get-ims-subscription), [impu](#impu-get-ims-subscription), [imsi](#imsi-get-ims-subscription) и [msisdn](#msisdn-get-ims-subscription).

### Пример тела запроса ###

```json
{
  "name": "imsu",
  "full": true
}
```

### Ответ ###

В ответе передаются статус запроса и параметры подписки IMS.

```
{
  "status": "<string>",
  "name": "<string>",
  "prefferedScscsSetId": <int>,
  "capabilitySetId": <int>,
  "chargingInformationName": "<string>",
  "impis": [ "<string>" ],
  "impus": [ "<string>" ],
  "implicitlyRegisteredSet": [ <ImplicitlyRegisteredSet> ],
  "prefferedScscsSet": <PrefferedScscsSet>,
  "capabilitySet": <CapabilitySet>,
  "chargingInformation": <ChargingInformation>,
  "imsProfiles": [ <ImsProfile> ],
  "serviceProfiles": [ <ServiceProfile> ],
  "sccAsName": "<string>"
}
```

### Поля ответа ###

| Поле                    | Описание                                                                                         | Тип                                                             | O/M |
|-------------------------|--------------------------------------------------------------------------------------------------|-----------------------------------------------------------------|-----|
| status                  | Индикатор статуса запроса.                                                                       | int                                                             | M   |
| name                    | Название подписки.                                                                               | string                                                          | M   |
| prefferedScscfSetId     | Идентификатор набора предпочитаемых узлов S-CSCF.pass:q[\<br\>]**Примечание.** Активно при `full = false`. | int                                                             | C   |
| capabilitySetId         | Идентификатор набора CapabilitiesSet.pass:q[\<br\>]**Примечание.** Активно при `full = false`.             | int                                                             | C   |
| chargingInformationName | Имя профиля Charging Information.pass:q[\<br\>]**Примечание.** Активно при `full = false`.                 | string                                                          | C   |
| impis                   | Значение IMPI.pass:q[\<br\>]**Примечание.** Активно при `full = false`.                                    | [string]                                                        | C   |
| impus                   | Значение IMPU.pass:q[\<br\>]**Примечание.** Активно при `full = false`.                                    | [string]                                                        | C   |
| implicitlyRegisteredSet | Набор неявно зарегистрированных IMPU.pass:q[\<br\>]**Примечание.** Активно при `full = false`.             | [ImplicitlyRegisteredSet](../entities/implicitlyRegisteredSet/) | C   |
| prefferedScscfSet       | Набор предпочитаемых узлов S-CSCF.pass:q[\<br\>]**Примечание.** Активно при `full = true`.                 | [PrefferedScscfSet](../entities/prefferedScscfSet/)             | C   |
| capabilitySet           | Набор Capability.pass:q[\<br\>]**Примечание.** Активно при `full = true`.                                  | [CapabilitySet](../entities/capabilitySet/)                     | C   |
| chargingInformation     | Профиль тарификации.pass:q[\<br\>]**Примечание.** Активно при `full = true`.                               | [chargingInformation](../entities/chargingInformation/)         | C   |
| imsProfiles             | Перечень профилей IMS.pass:q[\<br\>]**Примечание.** Активно при `full = true`.                             | [[imsProfile](../entities/imsProfile/)]                         | C   |
| serviceProfiles         | Перечень профилей услуг.pass:q[\<br\>]**Примечание.** Активно при `full = true`.                           | [[serviceProfile](../entities/serviceProfile/)]                 | C   |
| sccAsName               | Имя узла SCC-AS.                                                                                 | string                                                          | O   |