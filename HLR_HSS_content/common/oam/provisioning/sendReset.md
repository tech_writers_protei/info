
Команда `SendReset` позволяет уведомлять узел VLR/SGSN/MME о незапланированной перезагрузке.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SendReset \
  -d '{
  "mmeSgsnMask": "<string>",
  "vlrSgsnMask": "<string>",
  "imsiPrefix": "<string>",
  "prefixLength": <int>,
  "groupIds": [ <int> ],
  "groupNames": [ "<string>" ],
  "vlrNumber": "<string>",
  "version": <int>
}'
```

### Поля тела запроса ###

| Поле                                                | Описание                                                                                                                       | Тип      | O/M | Версия   |
|-----------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|----------|-----|----------|
| [[mme-sgsn-mask-send-reset]]mmeSgsnMask  | Префикс имени узла MME/SGSN 4G.                                                                                                | string   | C   |          |
| [[vlr-sgsn-mask-send-reset]]vlrSgsnMask  | Префикс номера узла VLR/SGSN.                                                                                                  | string   | C   |          |
| [[imsi-prefix-send-reset]]imsiPrefix     | Префикс номера IMSI для отправки запроса.                                                                                      | string   | C   |          |
| [[prefix-length-send-reset]]prefixLength | Длина префикса номера IMSI. Примечание - Все номера IMSI  группируются по префиксам.                                           | int      | С   |          |
| [[group-ids-send-reset]]groupIds         | Перечень идентификаторов группы для отправки запроса.                                                                          | [int]    | С   |          |
| [[group-names-send-reset]]groupNames     | Перечень названий группы для отправки запроса.                                                                                 | [string] | С   |          |
| [[vlr-number-send-reset]]vlrNumber       | Номер узла VLR.pass:q[\<br\>]**Примечание.** Используется только с параметром [version](#version-send-reset).                            | string   | C   | 2.0.37.4 |
| [[version-send-reset]]version            | Версия MAP, поддерживаемая узлом VLR.pass:q[\<br\>]**Примечание.** Используется только с параметром [vlrNumber](#vlr-number-send-reset). | int      | C   | 2.0.37.4 |

**Примечание.** Задается один из параметров [mmeSgsnMask](#mme-sgsn-mask-send-reset), [vlrSgsnMask](#vlr-sgsn-mask-send-reset) и [vlrNumber](#vlr-number-send-reset).

**Примечание.** Задается один из параметров [imsiPrefix](#imsi-prefix-send-reset), [prefixLength](#prefix-length-send-reset), [groupIds](#group-ids-send-reset) и [groupNames](#group-names-send-reset) вместе с одним из параметров [mmeSgsnMask](#mme-sgsn-mask-send-reset) и [vlrSgsnMask](#vlr-sgsn-mask-send-reset).

Комбинации параметров:

`[ mmeSgsnMask | vlrSgsnMask ] [ imsiPrefix | prefixLength | groupIds | groupNames ]`

или

` vlrNumber | version`

### Пример тела запроса ###

```json
{
  "vlrSgsnMask": "7697",
  "imsiPrefix": "25001"
}
```

```json
{
  "vlrNumber": "769735354353",
  "version": 3
}
```

### Ответ ###

В ответе передается только статус запроса.