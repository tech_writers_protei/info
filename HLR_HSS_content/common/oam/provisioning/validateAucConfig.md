
Команда `ValidateAucConfig` позволяет валидировать параметры центра аутентификации AuC.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ValidateAucConfig \
  -d '{
  "opParam": <object>,
  "tkParam": <object>,
  "cParam": <object>,
  "rParam": <object>,
}'
```

### Поля тела запроса ###

| Поле    | Описание      | Тип                                     | O/M |
|---------|---------------|-----------------------------------------|-----|
| opParam | OP параметры. | [OpParam](../entities/validateOpParam/) | O   |
| tkParam | ТК параметры. | [TkParam](../entities/validateTkParam/) | O   |
| cParam  | С параметры.  | [CParam](../entities/validateCParam/)   | O   |
| rParam  | R параметры.  | [RParam](../entities/validateRParam/)   | O   |

### Пример тела запроса ###

```json
{
  "opParam": {
    "id": 1,
    "op": "12345678900987654321123456789009"
  }
}
```

### Ответ ###

В ответе передается результаты проверки параметров центра аутентификации AuC.

```json
{
  "opParam": { "op": <bool> },
  "tkParam": { "tk": <bool> },
  "cParam": { "c": <bool> },
  "rParam": { "r": <bool> }
}
```

### Поля ответа ###

| Поле | Описание                         | Тип  | O/M |
|------|----------------------------------|------|-----|
| op   | Результат проверки параметра Op. | bool | M   |
| tk   | Результат проверки параметра Tk. | bool | M   |
| c    | Результат проверки параметра C.  | bool | M   |
| r    | Результат проверки параметра R.  | bool | M   |

### Пример ответа ###

```json
{
  "opParam": { "op": true },
  "tkParam": { "tk": true },
  "cParam": { "c": true },
  "rParam": { "r": true }
}
```