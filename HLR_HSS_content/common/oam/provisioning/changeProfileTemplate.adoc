Команда `ChangeProfileTemplate` позволяет изменять шаблон профиля.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeProfileTemplate \
  -d '{
  "id": <int>,
  "name": "<string>",
  "action": "<string>",
  "template": <ProfileTemplate>
}'
----

== Поля тела запроса

[width="100%",cols="11%,71%,6%,4%,8%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M |2.0.49.0
|id |Идентификатор шаблона.pass:q[<br>]*Примечание.* Обязателен при изменении и удалении. |int |C |2.0.49.0
|name |Название шаблона.pass:q[<br>]*Примечание.* Обязателен при создании. |string |C |2.0.49.0
|template |Тело шаблона. См. link:../entities/profileTemplate/[ProfileTemplate]. |object |O |2.0.49.0
|===

== Пример тела запроса

[source,json]
----
{
  "name": "template_1",
  "action": "create",
  "template": {
    "category": 10,
    "defaultForwardingNumber": "867349752",
    "defaultForwardingStatus": 7,
    "epsData": {
      "defContextId": 1,
      "ueMaxDl": 10000,
      "ueMaxUl": 10000
    },
    "epsContexts": [
      {
        "context-id": 2,
        "ipv4": "192.168.1.22",
        "plmnId": "25001"
      }
    ],
    "pdpData": [
      {
        "context-id": 1,
        "type": "0080"
      }
    ]
  }
}
----

== Ответ

В ответе передается только статус запроса.
