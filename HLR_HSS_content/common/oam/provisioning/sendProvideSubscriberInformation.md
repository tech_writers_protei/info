
Команда `SendProvideSubscriberInfo` позволяет получать информацию о регистрации абонента.

Используется для интеграции с внешними платформами.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SendProvideSubscriberInfo \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>"
}'
```

### Поля тела запроса ###

| Поле                                                            | Описание               | Тип    | O/M |
|-----------------------------------------------------------------|------------------------|--------|-----|
| [[imsi-send-provide-subscriber-information]]imsi     | Номер IMSI абонента.   | string | C   |
| [[msisdn-send-provide-subscriber-information]]msisdn | Номер MSISDN абонента. | string | C   |

**Примечание.** Задается только один из параметров [imsi](#imsi-send-provide-subscriber-information) и [msisdn](#msisdn-send-provide-subscriber-information).

### Пример тела запроса ###

```json
{ "imsi": "250010000001" }
```

### Ответ ###

В ответе передается статус запроса, состояние активности абонента и информация о местоположении абонента.

```
{
  "status": "<string>",
  "subscriberState": <bool>,
  "locationInformation": {<object>}
}
```

### Поля ответа ###

| Поле                | Описание                              | Тип                                                     | O/M |
|---------------------|---------------------------------------|---------------------------------------------------------|-----|
| status              | Статус запроса.                       | string                                                  | M   |
| subscriberState     | Флаг активности абонента.             | bool                                                    | O   |
| locationInformation | Информация о местоположении абонента. | [LocationInformation](../entities/locationInformation/) | O   |

### Пример тела запроса ###

```json
{
  "status": "OK",
  "subscriberState": 1,
  "locationInformation": {}
}
```