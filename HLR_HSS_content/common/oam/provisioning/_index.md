
### Общая информация HTTP API ###

Все запросы обрабатываются в синхронном режиме.
Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Все запросы и ответы должны быть представлены в формате JSON.

Все запросы и ответы JSON имеют вид согласно JSON-схемам, представленным ниже.
В каждом запросе должна использоваться только одна операция.

Каждый JSON-запрос и ответ должен иметь кодировку UTF-8, чтобы поддерживать многоязычность.

Приложение должно использовать один адрес URL для отправки запросов PROTEI HLR/HSS:

```
http(s)://<host>:<port>/<web_hook>
```

| Поле     | Описание                                 | Тип       | O/M |
|----------|------------------------------------------|-----------|-----|
| host     | IP-адрес или DNS-имя PROTEI HLR/HSS API. | string/ip | M   |
| port     | Порт для установления соединения.        | int       | M   |
| web_hook | URL для отправки запроса.                | string    | M   |

### Ответ от PROTEI HLR/HSS ###

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```json
{ "status": "<string>" }
```

### [[request_status]]Статус запроса

| Код | Поле                | Описание                                                                            |
|-----|---------------------|-------------------------------------------------------------------------------------|
| 0   | SUCCESS             | Действие выполнено                                                                  |
| 201 | TEMPORARYERROR      | Не описанная временная ошибка                                                       |
| 101 | INVPAYLOAD          | Ошибка при попытке использования payload                                            |
| 102 | INVEXTID            | Ошибка при задании внешнего идентификатора                                          |
| 103 | INVSCSID            | Ошибка узла SCS                                                                     |
| 104 | INVPERIOD           | Ошибка срока действия                                                               |
| 105 | NOTAUTHORIZED       | Узел SCS не имеет соответствующих прав для выполнения действия                      |
| 106 | SERVICEUNAVAILABLE  | Указанная услуга не доступна                                                        |
| 107 | PERMANENTERROR      | Постоянная ошибка                                                                   |
| 108 | QUOTAEXCEEDED       | Узел SCS исчерпал выделенную квоту                                                  |
| 109 | RATEEXCEEDED        | Количество пользовательских запросов с узла SCS превысило установленное ограничение |
| 110 | REPLACEFAIL         | Не удалось заменить триггер устройства                                              |
| 111 | RECALLFAIL          | Не удалось повторно вызвать триггер устройства                                      |
| 112 | ORIGINALMESSAGESENT | Сообщение, которое должно быть повторно отправлено или изменено, уже отправлено     |

### Запросы API


* [/CampaignService/GetStatsCampaign](./getStatsCampaign/) -- получить статистики по операциям;

* [/CampaignService/StopCampaign](./stopCampaign/) -- остановить операцию;

* [/CustomService/GetCampaignStatDetail](./getCampaignStatDetail/) -- получить данные статистики операции;

* [/CustomService/GetExportFile](./getExportFile/) -- получить файл с профилями абонентов;

* [/CustomService/GetImsiMsisdnInfo](./getImsiMsisdnInfo/) -- получить cвязки IMSI-MISISDN;

* [/CustomService/GetProfileBatchFile](./getProfileBatchFile/) -- получить списка профилей абонентов в файл;

* [/InternalService/GetJdbcConfig](./getJdbcConfig/) -- получить параметры подключения к базе данных;

* [/InternalService/GetProperties](./getProperties/) -- получить настройки базы данных, Core и API;

* [/ProfileService/AddAucProfile](./addAucProfile/) -- добавить профиль AuC;

* [/ProfileService/AddAucProfileBatch](./addAucProfileBatch/) -- добавить профили AuC;

* [/ProfileService/ChangeAucConfig](./changeAucConfig/) -- изменить параметры центра аутентификации AuC;

* [/ProfileService/ChangeAucProfile](./changeAucProfile/) -- изменить профиль AuC;

* [/ProfileService/ChangeCapabilitiesSet](./changeCapabilitiesSet/) -- изменить набор возможностей CapabilitiesSet;

* [/ProfileService/ChangeExternalGroupIdentifier](./changeExternalGroupIdentifier/) -- изменить внешний идентификатор группы;

* [/ProfileService/ChangeGroup](./changeGroup/) -- изменить группу абонента;

* [/ProfileService/ChangeProfileTemplate](./changeProfileTemplate/) -- изменить шаблон профиля;

* [/ProfileService/ChangeQosProfile](./changeQosProfile/) -- изменить профиль QoS;

* [/ProfileService/ChangeScef](./changeScef/) -- изменить параметры узла SCEF;

* [/ProfileService/ChangeSnssai](./changeSnssai/) -- изменить данные Single Network Slice Selection Assistance Information;

* [/ProfileService/ChangeWhiteBlackLists](./changeWhiteBlackLists/) -- изменить белые/черные списки;

* [/ProfileService/ControlAs](./controlAs/) -- управление сервером приложений;

* [/ProfileService/ControlConfig](./controlConfig/) -- изменить конфигурацию базы данных;

* [/ProfileService/ControlCsiProfile](./controlCsiProfile/) -- изменить CAMEL-профиль;

* [/ProfileService/ControlEpsProfile](./controlEpsProfile/) -- изменить профиль EPS;

* [/ProfileService/ControlPdpProfile](./controlPdpProfile/) -- изменить профиль PDP;

* [/ProfileService/ControlPrefferedScscfSet](./controlPrefferedScscfSet/) -- изменить набор предпочитаемых узлов S-CSCF;

* [/ProfileService/ControlRoamingAgreement](./controlRoamingAgreement/) -- изменить роуминговое соглашение о поколении CAMEL;

* [/ProfileService/ControlUser](./controlUser/) -- изменить параметры пользователя системы;

* [/ProfileService/GetAllBlackLists](./getAllBlackLists/) -- получить все черные списки;

* [/ProfileService/GetAllWhiteLists](./getAllWhiteLists/) -- получить все белые списки;

* [/ProfileService/GetAs](./getAs/) -- получить параметры сервера приложений;

* [/ProfileService/GetAucConfig](./getAucConfig/) -- получить параметры центра аутентификации AuC;

* [/ProfileService/GetAucProfile](./getAucProfile/) -- получить профиля AuC;

* [/ProfileService/GetAucProfileBatch](./getAucProfileBatch/) -- получить перечень карт согласно фильтру;

* [/ProfileService/GetAudit](./getAudit/) -- получить информацию о действиях пользователей;

* [/ProfileService/GetCapabilitiesSet](./getCapabilitiesSet/) -- получить набор возможностей Capability;

* [/ProfileService/GetChargingInformation](./getChargingInformation/) -- получить информацию о тарификации;

* [/ProfileService/GetConfig](./getConfig/) -- получить конфигурацию базы данных;

* [/ProfileService/GetCsiProfile](./getCsiProfile/) -- получить профиль CAMEL;

* [/ProfileService/GetEpsProfile](./getEpsProfile/) -- получить профиля EPS;

* [/ProfileService/GetGroup](./getGroup/) -- получить группу;

* [/ProfileService/GetGroups](./getGroups/) -- получить группы;

* [/ProfileService/GetImsSubscription](./getImsSubscription/) -- получить подписку IMS;

* [/ProfileService/GetLcsProfile](./getLcsProfile/) -- получить профиль услуг LCS;

* [/ProfileService/GetPdpProfile](./getPdpProfile/) -- получить профиль PDP;

* [/ProfileService/GetPrefferedScscfSet](./getPrefferedScscfSet/) -- получить набор предпочитаемых S-CSCF;

* [/ProfileService/GetProfile](./getProfile/) -- получить профиль абонента;

* [/ProfileService/GetProfileBatchFile](./getProfileBatchFile/) -- получить списка профилей абонентов по фильтру;

* [/ProfileService/GetProfilesIdentity](./getProfilesIdentity/) -- получить список идентификаторов общих сущностей;

* [/ProfileService/GetProfilesValue](./getProfilesValue/) -- получить список значений общих сущностей;

* [/ProfileService/GetProfileTemplate](./getProfileTemplate/) -- получить шаблона профиля;

* [/ProfileService/GetQosProfile](./getQosProfile/) -- получить профиль QoS;

* [/ProfileService/GetRegionalZoneCodeIdentity](./getRegionalZoneCodeIdentity/) -- получить код региональной зоны;

* [/ProfileService/GetRoamingAgreement](./getRoamingAgreement/) -- получить роуминговые соглашения о поколении CAMEL;

* [/ProfileService/GetScef](./getScef/) -- получить параметры узла SCEF;

* [/ProfileService/GetUser](./getUser/) -- получить параметры пользователя системы;

* [/ProfileService/GetUserBatch](./getUserBatch/) -- получить список пользователей по фильтру;

* [/ProfileService/GetWhiteBlackLists](./getWhiteBlackLists/) -- получить белых/черных списков;

* [/ProfileService/ProfileControl](./profileControl/) -- изменить профиль в базе данных HLR/HSS;

* [/ProfileService/ValidateAucConfig](./validateAucConfig/) -- проверить параметры AuC;

* [/ProfileServiceCampaign/AddAucProfile](./addAucProfileCampaign/) -- добавить набор профилей AUC в базу данных HLR/HSS из файла;

* [/ProfileServiceCampaign/ProfileControl](./profileControlCampaign/) -- изменить AuC-профили из файла;

* [/SubscriberService/AddAndActivateEmptySubscribers](./addAndActivateEmptySubscribers/) -- создать и активировать пустые профили абонентов;

* [/SubscriberService/AddSubscriber](./addSubscriber/) -- создать профиль абонента;

* [/SubscriberService/AddSubscriberBatch](./addSubscriberBatch/) -- добавить список абонентов;

* [/SubscriberService/AddSubscriberProfile](./addSubscriberProfile/) -- добавить профиля абонента к ранее загруженной карте;

* [/SubscriberService/ChangeIMSI](./ChangeImsi/) -- сменить номер IMSI;

* [/SubscriberService/ChangeImsSubscription](./changeImsSubscription/) -- изменить IMS-подписку;

* [/SubscriberService/ChangeLcsProfile](./changeLcsProfile/) -- изменить профилем LCS;

* [/SubscriberService/ChangeNiddAuthorization](./changeNiddAuthorization/) -- изменить авторизацию NIDD;

* [/SubscriberService/ChangeRegionalZoneCodeIdentity](./changeRegionalZoneCodeIdentity/) -- изменить код региональной зоны;

* [/SubscriberService/ChangeSifcSet](./changeSifcSet/) -- изменить набор общих iFC;

* [/SubscriberService/ChangeStatus](./changeStatus/) -- изменить статус абонента;

* [/SubscriberService/ControlChargingInformation](./controlChargingInformation/) -- изменить настройки тарификации;

* [/SubscriberService/DeleteSubscriber](./deleteSubscriber/) -- удалить профиль абонента из базы данных HLR/HSS с параметрами аутентификации карты;

* [/SubscriberService/DeleteSubscriberProfile](./deleteSubscriberProfile/) -- удалить профиля абонента из базы данных HLR/HSS без параметров аутентификации карты;

* [/SubscriberService/GetSifcSet](./getSifcSet/) -- получить набор общих iFC;

* [/SubscriberService/GetUserLocation](./getUserLocation/) -- получить местоположение абонента;

* [/SubscriberService/SendAlertSc](./sendAlertSc/) -- уведомить обслуживающий центр об активности абонента в сети;

* [/SubscriberService/SendCancelLocation](./sendCancelLocation/) -- отменить регистрацию в сетях 2G/3G/4G;

* [/SubscriberService/SendProvideRoamingNumber](./sendProvideRoamingNumber/) -- получить роуминговый номер абонента;

* [/SubscriberService/sendProvideSubscriberInformation](./sendProvideSubscriberInformation/) -- получить информацию о регистрации абонента;

* [/SubscriberService/SendRegistrationTermination](./sendRegistrationTermination/) -- отменить регистрацию в сетях IMS;

* [/SubscriberService/SendReset](./sendReset/) -- оповестить узел VLR/SGSN/MME об аварийной перезагрузке;

* [/SubscriberService/SetMSISDN](./setMsisdn/) -- изменить номер MSISDN абонента;

* [/SubscriberService/SetMSISDNBatch](./setMsisdnNBatch/) -- изменить номера MSISDN абонентов;

* [/SubscriberServiceCampaign/AddSubscriberProfile](./addSubscriberProfileCampaign/) -- добавить абонентские профили из файла;

* [/SubscriberServiceCampaign/ChangeIMSI](./changeImsiCampaign/) -- изменить набора номеров IMSI из файла;

* [/SubscriberServiceCampaign/DeleteSubscriber](./deleteSubscriberCampaign/) -- удалить абонентов, указанных в файле;

* [/SubscriberServiceCampaign/DeleteSubscriberProfile](./deleteSubscriberProfileCampaign/) -- удалить набор профилей абонентов без удаления карт из файла;

* [/SubscriberServiceCampaign/ExportSubscriberProfile](./exportSubscriberProfile/) -- экспортировать профили абонентов в файл;

* [/SubscriberServiceCampaign/SetMSISDN](./setMsisdnCampaign/) -- смена набора номеров MSISDN из файла.