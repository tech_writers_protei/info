
Команда `GetStatsCampaign` позволяет получать статистики по операциям.

### Запрос ###

```bash
$ curl -X GET https://<host>:<port>/CampaignService/GetStats?offset=<OFFSET>&limit=<LIMIT> \
  -d '{ "filter": <CampaignFilter> }'
```

### Поля запроса query ###

| Поле   | Описание                                               | Тип | O/M |
|--------|--------------------------------------------------------|-----|-----|
| offset | Сдвиг для первой требуемой записи.pass:q[\<br\>]По умолчанию: 0. | int | O   |
| limit  | Количество отображаемых записей.pass:q[\<br\>]По умолчанию: все. | int | O   |

### Поля тела запроса ###

| Поле   | Описание         | Тип                                           | O/M |
|--------|------------------|-----------------------------------------------|-----|
| filter | Фильтр операций. | [CampaignFilter](../entities/campaignFilter/) | M   |

### Пример тела запроса ###

```json
{
  "filter": {
    "sortDirection": "DESC"
  }
}
```

### Ответ ###

В ответе передается статус запроса, данные статистики и общее количество операций.

```json
{
  "status": 1,
  "stats": [
    {
      "campaignCounter": {
        "total": 10,
        "success": 10,
        "error": 0
      },
      "campaignStatus": "SUCCEED",
      "campaignType": "SIMCARDS_INFORMATION_LOAD",
      "id": 49,
      "creationDate": "2022-07-28T12:50:37",
      "startDate": "2022-07-28T12:50:37",
      "finishDate": "2022-07-28T12:50:37"
    }
  ],
  "total": 43
}
```

### Поля ответа ###

| Поле   | Описание                   | Тип                                 | O/M |
|--------|----------------------------|-------------------------------------|-----|
| status | Статус запроса.            | string                              | M   |
| stats  | Статистика по операциям.   | [[Campaign](../entities/campaign/)] | M   |
| total  | Общее количество операций. | int                                 | M   |