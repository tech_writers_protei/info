
Команда `SendProvideRoamingNumber` позволяет получать роуминговый номер.

Используется для интеграции с внешними платформами.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SendProvideRoamingNumber \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>",
  "gmscAddress": "<string>",
  "callReferenceNumber": "<string>"
}'
```

### Поля тела запроса ###

| Поле                                                    | Описание                                                     | Тип    | O/M |
|---------------------------------------------------------|--------------------------------------------------------------|--------|-----|
| [[imsi-send-provide-roaming-number]]imsi     | Номер IMSI абонента.                                         | string | C   |
| [[msisdn-send-provide-roaming-number]]msisdn | Номер MSISDN абонента.                                       | string | C   |
| gmscAddress                                             | Адрес узла GMSC.                                             | string | M   |
| callReferenceNumber                                     | Номер вызова, назначенный управлением соединениями узла MSC. | string | M   |

**Примечание.** Задается только один из параметров [imsi](#imsi-send-provide-roaming-number) и [msisdn](#msisdn-send-provide-roaming-number).

### Пример тела запроса ###

```json
{
  "imsi": "250010000001",
  "gmscAddress": "123456789",
  "callReferenceNumber": "9876543210"
}
```

### Ответ ###

В ответе передаются статус запроса и номер мобильного абонента в роуминге.

```
{
  "status": "<string>",
  "msrn": "<string>"
}
```

### Поля ответа ###

| Поле   | Описание                              | Тип    | O/M |
|--------|---------------------------------------|--------|-----|
| status | Статус запроса.                       | string | M   | 
| msrn   | Номер мобильного абонента в роуминге. | string | O   |

### Пример ответа ###

```json
{
  "status": "OK",
  "msrn": "123456789"
}
```