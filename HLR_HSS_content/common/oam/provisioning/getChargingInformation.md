
Команда `GetChargingInformation` позволяет получать информацию Charging Information.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetChargingInformation \
  -d '{ "Name": "<string>" }'
```

### Поля тела запроса ###

| Поле | Описание                                  | Тип    | O/M |
|------|-------------------------------------------|--------|-----|
| Name | Название параметров Charging Information. | string | M   |

### Пример тела запроса ###

```json
{ "Name": "ci" }
```

### Ответ ###

В ответе передаются статус запроса и параметры Charging Information.

```
{
  "status": "<string>",
  "ChargingInformation": <ChargingInformation>
}
```

### Поля ответа ###

| Поле                | Описание                           | Тип                                                     | O/M |
|---------------------|------------------------------------|---------------------------------------------------------|-----|
| status              | Статус запроса.                    | string                                                  | M   |
| ChargingInformation | Параметры тарификации в сетях IMS. | [ChargingInformation](../entities/chargingInformation/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "ChargingInformation": {}
}
```