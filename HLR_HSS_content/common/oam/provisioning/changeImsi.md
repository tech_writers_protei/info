
Команда `ChangeIMSI` позволяет изменять номер IMSI абонента.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeIMSI \
  -d '{
  "oldimsi": "<string>",
  "imsi": "<string>",
  "msisdn": "<string>",
  "status": "<string>",
  "deleteOldImsi": <bool>
}'
```

### Поля тела запроса ###

| Поле                                      | Описание                     | Тип    | O/M | Версия   |
|-------------------------------------------|------------------------------|--------|-----|----------|
| [[oldimsi-change-imsi]]oldimsi | Текущий номер IMSI.          | string | C   |          |
| [[msisdn-change-imsi]]msisdn   | Текущий номер MSISDN.        | string | C   |          |
| imsi                                      | Новый номер IMSI.            | string | М   |          |
| [status](#status-change-imsi)             | Код состояния абонента.      | int    | O   |          |
| deleteOldImsi                             | Флаг удаления текущей карты. | bool   | O   | 2.0.54.0 |

**Примечание.** Задается только один из параметров [oldimsi](#oldimsi-change-imsi) и [msisdn](#msisdn-change-imsi).

#### [[status-change-imsi]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

### Пример тела запроса ###

```json
{
  "oldimsi": "250010000001",
  "imsi": "250010000002"
}
```

### Ответ ###

В ответе передается только статус запроса.

**Примечание.** Состояние нового номера IMSI определяется значением в запросе или на основе старого.
Профиль абонента привязывается к новому номеру IMSI.