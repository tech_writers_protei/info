
Команда `GetEpsProfile` позволяет получать профиль EPS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetEpsProfile \
  -d '{ "context-id": <int> }'
```

### Поля тела запроса ###

| Поле       | Описание                     | Тип | O/M |
|------------|------------------------------|-----|-----|
| context-id | Идентификатор контекста EPS. | int | M   |

### Пример тела запроса ###

```json
{ "context-id": 1 }
```

### Ответ ###

В ответе передается cтатус запроса и профиль EPS.

```
{
  "status": "<string>",
  "epsProfile": {<object>}
}
```

### Поля ответа ###

| Поле       | Описание        | Тип                                   | O/M |
|------------|-----------------|---------------------------------------|-----|
| status     | Статус запроса. | string                                | M   |
| epsProfile | Профиль EPS.    | [EpsProfile](../entities/epsProfile/) | M   |

### Пример тела запроса ###

```json
{
  "status": "OK",
  "epsProfile": {}
}
```