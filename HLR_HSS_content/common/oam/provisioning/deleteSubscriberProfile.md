
Команда `DeleteSubscriberProfile` позволяет удалять абонента из базы данных HLR/HSS без параметров аутентификации.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/DeleteSubscriber \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>"
}'
```

### Поля тела запроса ###

| Поле                                                  | Описание               | Тип    | O/M | Версия   |
|-------------------------------------------------------|------------------------|--------|-----|----------|
| [[imsi-delete-subscriber-profile]]imsi     | Номер IMSI абонента.   | string | C   | 2.0.41.0 |
| [[msisdn-delete-subscriber-profile]]msisdn | Номер MSISDN абонента. | string | C   | 2.0.41.0 |

**Примечание.** Задается только один из параметров [imsi](#imsi-delete-subscriber-profile) и [msisdn](#msisdn-delete-subscriber-profile).

### Пример тела запроса ###

```json
{ "imsi": "250010000001" }
```

### Ответ ###

В ответе передается только статус запроса.