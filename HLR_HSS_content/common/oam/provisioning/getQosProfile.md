
Команда `GetQosProfile` позволяет получать профиль QoS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetQosProfile \
  -d '{
  "qosEpsId": <int>,
  "qosGprsId": <int>
}'
```

### Поля тела запроса ###

| Поле                                | Описание                        | Тип | O/M |
|-------------------------------------|---------------------------------|-----|-----|
| [[qos-gprs-id]]qosGprsId | Идентификатор профиля QoS GPRS. | int | С   |
| [[qos-eps-id]]qosEpsId   | Идентификатор профиля QoS EPS.  | int | С   |

**Примечание.** Задается только один из параметров [qosGprsId](#qos-gprs-id) и [qosEpsId](#qos-eps-id).

### Пример тела запроса ###

```json
{ "qosEpsId": 1 }
```

### Ответ ###

В ответе передаются статус запроса и профиль QoS EPS или QoS GPRS.

```
{
  "status": "<string>",
  "qosGprs": <QosGprs>,
  "qosEps": <QosEps>
}
```

### Поля ответа ###

| Поле    | Описание          | Тип                             | O/M |
|---------|-------------------|---------------------------------|-----|
| status  | Статус запроса.   | string                          | M   |
| qosGprs | Профиль QoS GPRS. | [QosGprs](../entities/qosGprs/) | C   |
| qosEps  | Профиль QoS EPS.  | [QosEps](../entities/qosEps/)   | C   |

### Пример ответа ###

```json
{
  "status": "OK",
  "qosGprs": {}
}
```