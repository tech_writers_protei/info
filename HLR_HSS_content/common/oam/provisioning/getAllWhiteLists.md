
Команда `GetAllWhiteLists` позволяет получать все белые списки в системе.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetAllWhiteLists
```
### Ответ ###

В ответе передаются статус запроса и параметры белых списков.

```
{
  "status": "<string>",
  "whiteList": [ <WhiteList> ]
}
```

### Поля ответа ###

| Поле      | Описание                | Тип                                   | O/M |
|-----------|-------------------------|---------------------------------------|-----|
| status    | Статус запроса.         | string                                | M   |
| whiteList | Перечень белых списков. | [[WhiteList](../entities/whiteList/)] | M   |