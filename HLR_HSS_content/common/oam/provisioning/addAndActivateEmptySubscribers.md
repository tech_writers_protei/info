
Команда `AddAndActivateEmptySubscribers` позволяет задавать и активировать абонента без каких-либо заданных параметров.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/AddAndActivateEmptySubscribers \
  -d '{ "subscribers": [ <EmptySubscriber> ] }'
```

### Поля тела запроса ###

| Поле        | Описание                            | Тип                                               | O/M |
|-------------|-------------------------------------|---------------------------------------------------|-----|
| subscribers | Перечень пустых профилей абонентов. | [[emptySubscriber](../entities/emptySubscriber/)] | M   |

### Пример тела запроса ###

```json
{
  "subscribers": [
    {
      "imsi": "250010000001",
      "msisdn": "79000000001",
      "smsc": "1234567890",
      "teleServices": "16,17,21,22"
    },
    {
      "imsi": "250010000100",
      "msisdn": "79000000100",
      "smsc": "1234567890",
      "teleServices": "16,17,21,22"
    }
  ]
}
```

### Ответ ###

В ответе передается только статус запроса.