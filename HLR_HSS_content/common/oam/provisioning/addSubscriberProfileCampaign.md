
Команда `AddSubscriberProfile` позволяет добавлять набор абонентов в базу данных HLR из файла.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/AddSubscriberProfile?jsonData=<JSON_DATA> \
  -H 'Content-Type: text/csv' \
  -d @<path_to_file>.csv
```

### Поля запроса query ###

| Поле                                                  | Описание                                         | Тип                                             | O/M | Версия    |
|-------------------------------------------------------|--------------------------------------------------|-------------------------------------------------|-----|-----------|
| jsonData                                              | Параметры запроса, передаваемые в открытом виде. | object                                          | M   |           |
| **{**                                                 |                                                  |                                                 |     |           |
| &nbsp;&nbsp;templateId                                | Идентификатор шаблона абонентского профиля.      | [ProfileTemplate](../entities/profileTemplate/) | M   |           |
| &nbsp;&nbsp;[status](#status-add-subscriber-campaign) | Статус абонента.                                 | string                                          | О   | 2.0.56.20 |
| **}**                                                 |                                                  |                                                 |     |           |

#### [[status-add-subscriber-campaign]]Коды состояния абонента

| N | Статус                | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

### Пример тела запроса в формате URI ##

```bash
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/AddSubscriberProfile?jsonData=%7B%22templateId%22%3A1%2C%22status%22%3A2%7D
```

### Пример тела запроса в формате JSON ###

```bash
$ curl -X POST https://<host>:<port>/ProfileServiceCampaign/AddSubscriberProfile \
  -d '{
  "templateId": 1,
  "status": 2
}' \
  -H 'Content-Type: text/csv' \
  -d @<path_to_file>.csv
```

В теле запроса передается файл формата CSV.

Формат файла:

```csv
imsi;msisdn;epsContextId;staticIpv4;staticIpv6;
```

### Поля файла ###

| Поле         | Описание                                             | Тип    | O/M |
|--------------|------------------------------------------------------|--------|-----|
| imsi         | Номер IMSI абонента.                                 | string | M   |
| msisdn       | Номер MSISDN абонента.                               | string | O   |
| epsContextId | Идентификатор контекста EPS для привязки IP-адресов. | string | O   |
| staticIpv4   | Статический IPv4-адрес абонента.                     | ip     | O   |
| staticIpv6   | Статический IPv6-адрес абонента.                     | string | O   |

### Пример файла ### 

```csv
250010000000004;79110000004;
250010000000005;79110000005;
250010000000006;79110000006;
```

### Ответ ###

В ответе передаются статус запроса и параметры операции.

```
{
  "status": "<string>",
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "finishDate": "<datetime>",
  "startDate": "<datetime>",
  "campaignCounter": <СampaignCounter>
}
```

### Поля ответа ###

| Поле                                                       | Описание                 | Тип                                             | O/M | Версия   |
|------------------------------------------------------------|--------------------------|-------------------------------------------------|-----|----------|
| status                                                     | Статус запроса.          | string                                          | M   |          |
| campaignCounter                                            | Счетчик операций.        | [СampaignCounter](../entities/campaignCounter/) | M   | 2.0.50.0 |
| [campaignStatus](#campaign-status-add-subscriber-campaign) | Статус операции.         | string                                          | M   | 2.0.50.0 |
| [campaignType](#campaign-type-add-subscriber-campaign)     | Тип операции.            | string                                          | M   | 2.0.50.0 |
| id                                                         | Идентификатор операции.  | int                                             | M   | 2.0.50.0 |
| creationDate                                               | Дата создания операции.  | datetime                                        | O   | 2.0.50.0 |
| startDate                                                  | Дата запуска операции.   | datetime                                        | O   | 2.0.50.0 |
| finishDate                                                 | Дата окончания операции. | datetime                                        | O   | 2.0.50.0 |

#### [[campaign-status-add-subscriber-campaign]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

#### [[campaign-type-add-subscriber-campaign]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |

### Пример ответа ###

```json
{
  "status": "OK",
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "ADD_SUBSCRIBER_PROFILE",
  "id": 49,
  "creationDate": "2022-01-01T12:50:37",
  "startDate": "2022-01-01T12:50:37",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  }
}
```