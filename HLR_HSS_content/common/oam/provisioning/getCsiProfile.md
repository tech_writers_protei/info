
Команда `GetCsiProfile` позволяет получать профиль CAMEL Subscription Information.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetCsiProfile \
  -d '{
  "typeId": <int>,
  "profileId": <int>
}'
```

### Поля тела запроса ###

| Поле                               | Описание                   | Тип | O/M |
|------------------------------------|----------------------------|-----|-----|
| [typeId](#type-id-get-csi-profile) | Код типа CSI.              | int | M   |
| profileId                          | Идентификатор профиля CSI. | int | M   |

#### [[type-id-get-csi-profile]]Идентификаторы типа CSI

| N  | Поле      | Описание          |
|----|-----------|-------------------|
| 0  | O-CSI     | Профиль O-CSI     |
| 1  | T-CSI     | Профиль T-CSI     |
| 2  | SMS-CSI   | Профиль SMS-CSI   |
| 3  | GPRS-CSI  | Профиль GPRS-CSI  |
| 4  | M-CSI     | Профиль M-CSI     |
| 5  | D-CSI     | Профиль D-CSI     |
| 6  | TIF-CSI   | Профиль TIF-CSI   |
| 7  | SS-CSI    | Профиль SS-CSI    |
| 8  | USSD-CSI  | Профиль USSD-CSI  |
| 9  | O-IM-CSI  | Профиль O-IM-CSI  |
| 10 | VT-IM-CSI | Профиль VT-IM-CSI |
| 11 | D-IM-CSI  | Профиль D-IM-CSI  |

### Пример тела запроса ###

```json
{
  "typeId": 0,
  "profileId": 1
}
```

### Ответ ###

В ответе передаются статус запроса и профиль CSI.

```
{
  "status": "<string>",
  "oCsi": <OCsi>,
  "tCsi": <TCsi>,
  "smsCsi": <SmsCsi>,
  "gprsCsi": <GprsCsi>,
  "mCsi": <MCsi>,
  "dCsi": <DCsi>,
  "ssCsi": <SsCsi>,
  "ussdCsi": <UssdCsi>,
  "oImCsi": <OImCsi>,
  "vtCsi": <VtCsi>,
  "dImCsi": <DImCsi>
}
```

### Поля ответа ###

| Поле    | Описание                                                                            | Тип                             | O/M |
|---------|-------------------------------------------------------------------------------------|---------------------------------|-----|
| status  | Статус запроса.                                                                     | string                          | M   |
| oCsi    | Профиль Originating-CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 0`.               | [OCsi](../entities/oCsi/)       | С   |
| tCsi    | Профиль Terminating CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 1`.               | [TCsi](../entities/tCsi/)       | С   |
| smsCsi  | Профиль SMS-CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 2`.                       | [SmsCsi](../entities/smsCsi/)   | С   |
| gprsCsi | Профиль GPRS-CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 3`.                      | [GprsCsi](../entities/gprsCsi/) | С   |
| mCsi    | Профиль Mobile Management Event CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 4`.   | [MCsi](../entities/mCsi/)       | С   |
| dCsi    | Профиль Dialled CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 5`.                   | [DCsi](../entities/dCsi/)       | С   |
| ssCsi   | Профиль Supplementary Services CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 7`.    | [SsCsi](../entities/ssCsi/)     | С   |
| ussdCsi | Профиль USSD CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 8`.                      | [UssdCsi](../entities/ussdCsi/) | С   |
| oImCsi  | Профиль Originating IP Multimedia CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 9`. | [OCsi](../entities/oCsi/)       | С   |
| vtCsi   | Профиль VMSC Terminating CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 10`.         | [TCsi](../entities/tCsi/)       | С   |
| dImCsi  | Профиль Dialled IP Multimedia CSI.pass:q[\<br\>]**Примечание.** Активен при `typeId = 11`.    | [DCsi](../entities/dCsi/)       | С   |

### Пример ответа ###

```json
{
  "status": "OK",
  "oCsi": {}
}
```