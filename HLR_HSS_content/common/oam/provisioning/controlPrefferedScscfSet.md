
Команда `ControlPrefferedScscfSet` позволяет изменять набор предпочитаемых узлов S-CSCF.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangePrefferedScscfSet \
  -d '{
  "Action": "<string>",
  "PrefferedScscfSet": <PrefferedScscfSet>
}'
```

### Поля тела запроса ###

| Поле              | Описание                                                                         | Тип                                                 | O/M |
|-------------------|----------------------------------------------------------------------------------|-----------------------------------------------------|-----|
| Action            | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                              | M   |
| PrefferedScscfSet | Набор предпочитаемых узлов S-CSCF.                                               | [PrefferedScscfSet](../entities/prefferedScscfSet/) | M   |

### Пример тела запроса ###

```json
{
  "Action": "create",
  "PrefferedScscfSet": {
    "SetId": 1,
    "Scscfs": [
      {
        "ScscfName": "scscf1",
        "Priority": 1
      },
      {
        "ScscfName": "scscf2",
        "Priority": 2
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.