
Команда `ChangeRegionalZoneCodeIdentity` позволяет изменять код региональной зоны.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/AddAndActivateEmptySubscribers \
  -d '{
  "action": "<string>",
  "regional": <Regional>
}
```

### Поля тела запроса ###

| Поле     | Описание                                                                         | Тип                               | O/M |
|----------|----------------------------------------------------------------------------------|-----------------------------------|-----|
| action   | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                            | M   |
| regional | Параметры региональной зоны.                                                     | [Regional](../entities/regional/) | M   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "regional": {
    "name": "rus",
    "plmns": [
      {
        "plmn": "25001",
        "zoneCodes": [
          { "zoneCode": 1 },
          { "zoneCode": 2 }
        ]
      }
    ],
    "ccndcs": [
      {
        "ccndc": "7901",
        "zoneCodes": [
          { "zoneCode": 2 },
          { "zoneCode": 3 }
        ]
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.