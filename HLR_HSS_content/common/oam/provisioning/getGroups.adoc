Команда `GetGroups` позволяет получать пользовательские группы в системе.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetGroups \
  -d '{ "full": <bool> }'
----

== Поля тела запроса

[width="99%",cols="10%,75%,8%,7%",options="header",]
|===
|Поле |Описание |Тип |O/M
|full |Флаг запроса всей информации. По умолчанию: false. |bool |O
|===

== Пример тела запроса

[source,json]
----
{ "full": true }
----

== Ответ

В ответе передаются статус запроса и пользовательские группы.

....
{
  "status": "<string>",
  "groups": [ <Group> ]
}
....

== Поля ответа

[width="100%",cols="12%,43%,39%,6%",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|groups |Перечень пользовательских групп. |[link:../entities/group/[Group]] |M
|===

== Пример тела запроса

[source,json]
----
{
  "status": "OK",
  "groups": []
}
----
