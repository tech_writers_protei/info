
Команда `GetProfileTemplate` позволяет получать шаблон профиля.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetProfileTemplate \
  -d '{
  "name": "<string>",
  "id": <int>
}'
```

### Поля тела запроса ###

| Поле                                         | Описание                       | Тип    | O/M |
|----------------------------------------------|--------------------------------|--------|-----|
| [[id-get-profile-template]]id     | Идентификатор шаблона профиля. | int    | C   |
| [[name-get-profile-template]]name | Имя шаблона.                   | string | C   |

**Примечание.** Задается только один из параметров [id](#id-get-profile-template) или [name](#name-get-profile-template).

### Пример тела запроса ###

```json
{
  "name": "template_1"
}
```

### Ответ ###

В ответе передаются статус и параметры шаблона профиля.

```
{
  "status": "<string>",
  "id": <int>,
  "name": "<string>",
  "template": <ProfileTemplate>
}
```

### Поля ответа ###

| Поле     | Описание                       | Тип                                             | O/M |
|----------|--------------------------------|-------------------------------------------------|-----|
| status   | Статус запроса.                | string                                          | M   |
| id       | Идентификатор шаблона профиля. | int                                             | M   |
| name     | Имя шаблона профиля.           | string                                          | M   |
| template | Параметры шаблона профиля.     | [ProfileTemplate](../entities/profileTemplate/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "id": 1,
  "name": "template_1",
  "template": {
    "category": 10,
    "defaultForwardingNumber": "867349752",
    "defaultForwardingStatus": 7,
    "epsData": {
      "defContextId": 1,
      "ueMaxDl": 10000,
      "ueMaxUl": 10000
    },
    "epsContexts": [
      {
        "context-id": 2,
        "ipv4": "192.168.1.22",
        "plmnId": "25001"
      }
    ],
    "pdpData": [
      {
        "context-id": 1,
        "type": "0080"
      }
    ]
  }
}
```