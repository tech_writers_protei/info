
## Параметры приватности профиля LCS для внешнего абонента LcsPrivacyEc

| Поле                 | Описание                                  | Тип    | O/M |
|----------------------|-------------------------------------------|--------|-----|
| gmlcRestriction      | Флаг разрешения регистрации на узле GMLC. | int    | O   |
| notificationToMsUser | Флаг оповещения о внесенных изменениях.   | int    | O   |
| externalAddress      | Адрес внешнего пользователя.              | string | M   |
| delete               | Флаг удаления значения.                   | bool   | O   |

### Пример ###

```json
{
  "gmlcRestriction": 0,
  "notificationToMsUser": 1,
  "externalAddress": "1245135"
}
```