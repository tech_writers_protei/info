
## Параметры неявно зарегистрированных IMPU ImplicitlyRegisteredSet

| Поле   | Описание                                                  | Тип                | O/M |
|--------|-----------------------------------------------------------|--------------------|-----|
| name   | Имя набора.                                               | string             | M   |
| impus  | Перечень публичных идентификаторов пользователя сети IMS. | [[Impu](../impu/)] | O   |
| delete | Флаг удаления записи.                                     | bool               | O   |

### Пример ###

```json
{
  "impus": [
    {
      "BarringIndication": 0,
      "CanRegister": 1,
      "Identity": "simpu1@ims.protei.ru",
      "ServiceProfileName": "sp",
      "Type": 0
    },
    {
      "Barring": 1,
      "BarringIndication": 0,
      "CanRegister": 1,
      "Identity": "simpu2@ims.protei.ru",
      "ServiceProfileName": "sp",
      "Type": 0
    }
  ],
  "name": "implicitlyRegisteredSet_1"
}
```