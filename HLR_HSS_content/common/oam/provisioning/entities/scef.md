
## Параметры узла Service Capability Exposure Function Scef

| Поле                               | Описание                                                                                                                               | Тип    | O/M | Версия  |
|------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|--------|-----|---------|
| name                               | Название узла SCEF. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/17.03.00_60/ts_123682v170300p.pdf). | string | М   |         |
| host                               | Хост узла SCEF. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/17.03.00_60/ts_123682v170300p.pdf).     | string | O   |         |
| realm                              | Realm узла SCEF.                                                                                                                       | string | O   |         |
| monte                              | Флаг мониторинга событий MONTE.                                                                                                        | bool   | O   | 2.1.0.0 |
| aeseCommunicationPattern           | Флаг шаблона взаимодействия AESE.                                                                                                      | bool   | O   | 2.1.0.0 |
| niddAuthorization                  | Флаг авторизации NIDD.                                                                                                                 | bool   | O   | 2.1.0.0 |
| enhancedCoverageRestrictionControl | Флаг применения политики управления улучшенным контролем покрытия сети.                                                                | bool   | O   | 2.1.0.0 |
| niddAuthorizationUpdate            | Флаг обновления параметров авторизации NIDD.                                                                                           | bool   | O   | 2.1.0.0 |
| reportEfnmonte                     | Флаг создания отчета с наблюдаемыми событиями cети.                                                                                    | bool   | O   | 2.1.0.0 |
| eventCancellationReport            | Флаг создания отчета с событиями CANCELLATION EVENT.                                                                                   | bool   | O   | 2.1.0.0 |
| configEfncp                        | Флаг настройки сетевого подключения EFNCP.                                                                                             | bool   | O   | 2.1.0.0 |
| configEfnnp                        | Флаг настройки параметров EFNNP.                                                                                                       | bool   | O   | 2.1.0.0 |
| extendedReferenceIos               | Флаг списка расширенных ссылок.                                                                                                        | bool   | O   | 2.1.0.0 |
| dynamicGroupEventMonitoring        | Флаг динамического мониторинга группы событий.                                                                                         | bool   | O   | 2.1.0.0 |

### Пример ###

```json
{
  "name": "scef1",
  "host": "scef.protei.ru",
  "realm": "protei.ru",
  "monte": 1,
  "niddAuthorization": 1
}
```