
## Параметры профиля M-CSI-MT MCsiMt

| Поле                     | Описание                       | Тип  | O/M |
|--------------------------|--------------------------------|------|-----|
| [mmCode](#mmcode-csi-mt) | Код события в мобильных сетях. | int  | M   |
| delete                   | Флаг удаления записи.          | bool | O   |

### Пример ###

```json
{
  "mmCode": 2
}
```

#### [[mmcode-csi-mt]]Коды событий Mobility Management

| Bin      | Dec | Событие                       |
|----------|-----|-------------------------------|
| 00000000 | 0   | Location-update-in-same-VLR   |
| 00000001 | 1   | Location-update-to-other-VLR  |
| 00000010 | 2   | IMSI-Attach                   |
| 00000011 | 3   | MS-initiated-IMSI-Detach      |
| 00000100 | 4   | Network-initiated-IMSI-Detach |