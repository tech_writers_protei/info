
## Параметры абонентской группы Group

| Поле        | Описание                                                                                                 | Тип                      | O/M |
|-------------|----------------------------------------------------------------------------------------------------------|--------------------------|-----|
| id          | Идентификатор группы.pass:q[\<br\>]**Примечание.** Генерируется при создании.                                      | int                      | O   |
| name        | Имя группы.                                                                                              | string                   | М   |
| mapGt       | Глобальный заголовок MAP GT узла HLR.                                                                    | string                   | О   |
| diamRealm   | Realm узла HSS для протокола Diameter.                                                                   | string                   | О   |
| mapError    | Код ошибки MAP для завершения запроса MAP-Update-Location.                                               | string                   | O   |
| diamError   | Код ошибки Diameter для завершения запроса Diameter: Update-Location-Request.                            | int                      | O   |
| gtList      | Перечень настоящих глобальных заголовков узла HLR.                                                       | [[GroupGt](../groupGt/)] | O   |
| noPsiGtList | Перечень глобальных заголовков, для которых в ответ на запрос MAP-SRI не отправляется сообщение MAP-PSI. | [[NoPsiGt](../noPsiGt/)] | O   |

### Пример ###

```json
{
  "action": "create",
  "group": {
    "diamError": 5444,
    "diamRealm": "protei.ru",
    "gtList": [
      {
        "diamHost": "hss1.protei.ru",
        "hlrId": 1
      },
      {
        "diamHost": "hss2.protei.ru",
        "hlrId": 2
      }
    ],
    "name": "gr"
  }
}
```