== Параметры записи в журнале действий Audit

[width="100%",cols="34%,44%,9%,4%,9%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|dt |Дата выполнения команды в формате Unix. |unixtime |М |2.0.54.2
|link:#operation-audit[operation] |Тип операции. |string |O |2.0.54.2
|link:#status-audit[status] |Код состояния операции. |string |O |2.0.54.2
|executionTime |Время выполнения действия, в миллисекундах. |int |O |2.0.54.2
|sentToHlr |Флаг запуска операции на узле HLR.Core. |bool |O |2.0.54.2
|userHost |Адрес хоста, обработавшего запрос. |ip |O |2.0.54.2
|host |Имя хоста, обработавшего запрос. |string |O |2.0.54.2
|userName |Логин пользователя. |string |O |2.0.54.2
|msisdn |Номер MSISDN абонента. |string |O |2.0.54.2
|params |Измененные параметры абонента. |string |O |2.0.54.2
|===

=== Пример

[source,json]
----
{
  "dt": 1658160802312,
  "operation": "DELETE_SUBSCRIBER",
  "status": "OK",
  "executionTime": 5,
  "sentToHlr": false,
  "userHost": "10.200.110.21",
  "host": "CentOS8_103",
  "userName": "test",
  "msisdn": "380725387725",
  "imsi": "255995005387725"
}
----

=== [[operation-audit]]Действия пользователя

[width="100%",cols="36%,64%",options="header",]
|===
|Значение |Описание
|ADD_AND_ACTIVATE_EMPTY_SUBSCRIBER |Добавление пустого профиля подписчика
|ADD_AUC_CAMPAIGN_TASK |Добавление карты в центр аутентификации AUC с помощью операции
|ADD_AUC_PROFILE_BATCH |Добавление набора карт в центр аутентификации AUC
|ADD_AUC_PROFILE_CAMPAIGN |Добавление профиля центра аутентификации AUC подписчика
|ADD_SUBSCRIBER |Добавление подписчика
|ADD_SUBSCRIBER_BATCH |Добавление набора подписчиков
|ADD_SUBSCRIBER_PROFILE |Добавление профиля подписчика
|ADD_SUBSCRIBER_PROFILE_CAMPAIGN |Добавление профиля подписчика из файла
|CHANGE_AS |Изменение сервера приложений
|CHANGE_AUC_CONFIG |Изменение конфигурации центра аутентификации AUC
|CHANGE_CAPABILITY_SET |Изменение набора CapabilitySet
|CHANGE_CHARGING_INFO |Изменение набора ChargingInformation
|CHANGE_CSI |Изменение CSI-профиля
|CHANGE_EPS |Изменение профиля EPS
|CHANGE_IFC |Изменение набора iFC
|CHANGE_IMPL_REG_SET |Изменение набора ImplicitlyRegisteredSet
|CHANGE_IMS_SUBSCRIPTION |Изменение подписки в сетях IMS
|CHANGE_PDP |Изменение профиля PDP
|CHANGE_PREFFERED_SCSCF_SET |Изменение набора предпочитаемых узлов S-CSCF
|CHANGE_PROFILE |Изменение профиля абонента
|CHANGE_PROFILE_CAMPAIGN |Добавление профилей подписчиков из файла
|CHANGE_PROFILE_IMSI |Изменение номера IMSI профиля
|CHANGE_PROFILE_IMSI_CAMPAIGN |Изменение набора номеров IMSI профиля
|CHANGE_PROFILE_MSISDN |Изменение номера MSISDN профиля
|CHANGE_PROFILE_MSISDN_BATCH |Изменение набора номеро MSISDNв профиля
|CHANGE_PROFILE_MSISDN_CAMPAIGN |Изменение набора номеро MSISDNв абонентов
|CHANGE_QOS |Изменение профиля QoS
|CHANGE_RA |Изменение соглашения о роуминге
|CHANGE_SCEF |Изменение узла SCEF
|CHANGE_SERVICE_PROFILE |Изменение профиля услуги
|CHANGE_SIFCS |Изменение набора общих iFC
|CHANGE_SUBSCRIBER_STATUS |Изменение состояния подписчика
|CHANGE_WBL |Изменение черного/белого списка
|DELETE_SUBSCRIBER |Удаление подписчика
|DELETE_SUBSCRIBER_CAMPAIGN |Удаление набора подписчиков
|DELETE_SUBSCRIBER_PROFILE |Удаление профиля подписчика
|DELETE_SUBSCRIBER_PROFILE_CAMPAIGN |Удаление профилей набора подписчиков
|GET_ALL_BL |Получение всех черных списков
|GET_ALL_WL |Получение всех белых списков
|GET_AS |Получение параметров сервера приложений
|GET_AUC_CONFIG |Получение конфигурации центра аутентификации AUC
|GET_AUC_PROFILE |Получение карты из центра аутентификации AUC
|GET_AUC_PROFILE_BATCH |Получение карт из центра аутентификации AUC после фильтрации
|GET_AUDIT |Получение информации о действиях пользователя
|GET_CAPABILITY_SET |Получение набора CapabilitySet
|GET_CHARGING_INFO |Получение профиля ChargingInformation
|GET_CSI |Получение CSI-профиля
|GET_EPS |Получение профиля EPS
|GET_GROUPS |Получение списка всех групп
|GET_IFC |Получение набора iFC
|GET_IMPL_REG_SET |Получение набора ImplicitlyRegisteredSet
|GET_IMS_SUBSCRIPTION |Получение подписки в сетях IMS
|GET_IMSI_MSISDN |Получение связок IMSI - MSISDN из файла
|GET_PDP |Получение профиля PDP
|GET_PREFFERED_SCSCF_SET |Получение набора предпочитаемых узлов S-CSCF
|GET_PROFILE |Получение профиля абонента
|GET_PROFILE_BATCH |Получение набора профилей абонентов
|GET_PROFILE_BATCH_FILE |Получение набора профилей абонентов из файла
|GET_PROFILES_IDENTITY |Получение идентификаторов общего объекта
|GET_PROPERTIES |Получение настроек ядра, базы данных и API
|GET_RA |Получение соглашения о роуминге
|GET_SCEF |Получение параметров узла SCEF
|GET_SERVICE_PROFILE |Получение профиля услуги
|GET_SIFCS |Получение набора общих iFC
|GET_STAT_DETAIL_CAMPAIGN |Получение подробной статистики по всем операциям
|GET_STATS_CAMPAIGN |Изменение статистики по операции
|GET_USER_LOCATION |Получение местоположения пользователя
|GET_WBL |Получение черного/белого списка
|RESET |Отправка сообщения Reset
|SEND_ALERT_SC |Отправка сообщения MAP-Alert-SC
|SEND_CANCEL_LOCATION |Отправка сообщения CancelLocation
|SEND_PROVIDE_ROAMING_NUMBER |Отправка сообщения MAP-Provide-Roaming-Number
|SEND_PROVIDE_SUBSCRIBER_INFO |Отправка сообщения MAP-Provide-Subscriber-Info
|SEND_REGISTRATION_TERMINATE |Отправка сообщения Diameter: Registration-Terminate-Request
|SEND_RESET |Отправка сообщения Reset
|STOP_CAMPAIGN |Остановка операции
|===

=== [[status-audit]]Коды состояния запроса

[width="100%",cols="6%,18%,76%",options="header",]
|===
|N |Поле |Описание
|0 |SUCCESS |Действие выполнено
|201 |TEMPORARYERROR |Не описанная временная ошибка
|101 |INVPAYLOAD |Ошибка при попытке использования payload
|102 |INVEXTID |Ошибка при задании внешнего идентификатора
|103 |INVSCSID |Ошибка узла SCS
|104 |INVPERIOD |Ошибка срока действия
|105 |NOTAUTHORIZED |Узел SCS не имеет соответствующих прав для выполнения действия
|106 |SERVICEUNAVAILABLE |Указанная услуга не доступна
|107 |PERMANENTERROR |Постоянная ошибка
|108 |QUOTAEXCEEDED |Узел SCS исчерпал выделенную квоту
|109 |RATEEXCEEDED |Количество пользовательских запросов с узла SCS превысило установленное ограничение
|110 |REPLACEFAIL |Не удалось заменить триггер устройства
|111 |RECALLFAIL |Не удалось повторно вызвать триггер устройства
|112 |ORIGINALMESSAGESENT |Сообщение, которое должно быть повторно отправлено или изменено, уже отправлено
|===
