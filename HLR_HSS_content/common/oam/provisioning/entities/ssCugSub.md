
## Параметры подписки на закрытую группу CUG SsCugSub

| Поле             | Описание                                                                                                                                                                                              | Тип                            | O/M |
|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-----|
| index            | Идентификатор CUG. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                 | int                            | M   |
| interLock        | Код блокировки CUG. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                | string                         | M   |
| intraOption      | Код режима обработки входящих и исходящих вызовов для CUG.pass:q[\<br\>]`0` -- нет запретов на вызовы;pass:q[\<br\>]`1` -- запрет на входящие вызовы участникам CUG;pass:q[\<br\>]`2` -- запрет на исходящие вызовы участникам CUG. | int                            | O   |
| basicServiceList | Перечень базовых услуг.                                                                                                                                                                               | [[SsCugSubBs](../ssCugSubBs/)] | O   |
| delete           | Флаг удаления значения параметра.                                                                                                                                                                     | bool                           | O   |

### Пример ###

```json
{
  "index": 1,
  "interLock": "00001fa5",
  "intraOption": 1,
  "basicServiceList": [
    {
      "serviceType": 1,
      "basicService": 17
    },
    {
      "serviceType": 1,
      "basicService": 18
    }
  ]
}
```