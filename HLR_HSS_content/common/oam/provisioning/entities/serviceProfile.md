
## Параметры профиля услуг ServiceProfile

| Поле                            | Описание                                                 | Тип              | O/M |
|---------------------------------|----------------------------------------------------------|------------------|-----|
| name                            | Имя профиля.                                             | string           | M   |
| CoreNetworkServiceAuthorization | Идентификатор услуги авторизации в cети Core.            | int              | O   |
| Ifcs                            | Перечень iFC, связанных с услугой.                       | [[Ifc](../ifc/)] | О   |
| SIfcs                           | Перечень идентификаторов общих iFC, связанных с услугой. | [object]         | O   |
| **{**                           |                                                          |                  |     |
| &nbsp;&nbsp;Id                  | Идентификатор SIfc.                                      | int              | M   |
| **}**                           |                                                          |                  |     |
| Delete                          | Флаг удаления записи.                                    | bool             | O   |

### Пример ###

```json
{
  "CoreNetworkServiceAuthorization": 1,
  "Ifcs": [
    {
      "ApplicationServerName": "as",
      "Name": "ifc1",
      "Priority": 1,
      "ProfilePartIndicator": 1,
      "TriggerPoint": {
        "ConditionTypeCNF": 1,
        "Spt": [
          {
            "ConditionNegated": 2,
            "Content": "headerContent",
            "Group": 1,
            "Header": "header",
            "Method": "INVITE",
            "RegistrationType": 1,
            "RequestUri": "http://ims.protei.ru/spt1",
            "SdpLine": "sdpLine",
            "SdpLineContent": "sdpLineContent",
            "SessionCase": 1,
            "Type": 3
          }
        ]
      }
    }
  ],
  "Name": "sp",
  "SIfcs": [
    { "Id": 1 }
  ]
}
```