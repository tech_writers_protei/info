
## Параметры сервиса RepositoryData

| Поле              | Описание                                                                               | Тип    | O/M |
|-------------------|----------------------------------------------------------------------------------------|--------|-----|
| id                | Ключевой идентификатор сервиса.pass:q[\<br\>]**Примечание.** Обязательный при изменении объекта. | int    | C   |
| sqn               | Счетчик изменений тела сервиса.pass:q[\<br\>]**Примечание.** Только для чтения.                  | int    | C   |
| publicIdentity    | Публичный идентификатор, к которому привязан сервис.                                   | string | M   |
| serviceIndication | Идентификатор сервиса.                                                                 | string | M   |
| repositoryData    | Тело сервиса.                                                                          | string | M   |

### Пример ###

```json
{
  "id": 1,
  "publicIdentity": "tel:+79000000001@ims.protei.ru",
  "sqn": 0,
  "serviceIndication": "Service-Specific",
  "repositoryData": "<ServiceSpecificData><Parameter1>1</Parameter1><Parameter2>two</Parameter2></ServiceSpecificData>"
}
```