
## Параметры черного списка BlackList

| Поле       | Описание                                                                         | Тип                            | O/M |
|------------|----------------------------------------------------------------------------------|--------------------------------|-----|
| action     | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                         | M   |
| name       | Название списка.                                                                 | string                         | M   |
| vlr_masks  | Перечень масок номера узла VLR.                                                  | [[BLVlrMask](../blVlrMask/)]   | O   |
| plmn_masks | Перечень масок сети PLMN.                                                        | [[BLPlmnMask](../blPlmnMask/)] | O   |

### Пример ###

```json
{
  "action": "create",
  "name": "blacklist",
  "vlr_masks": [
    {
      "country": "rus",
      "mask": "7689",
      "network": "supertelecom"
    }
  ]
}
```