
## Параметры местоположения абонента на узле SGSN SgsnLocationInformation

| Поле                     | Описание                                                                                                                                                                                      | Тип                                          | O/M |
|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------|-----|
| ageOfLocationInformation | Значение `Age-Of-Location-Information`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272).                                                       | int                                          | O   |
| currentLocationRetrieved | Флаг предоставления текущего местоположения абонента, `Current-Location-Retrieved`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272).           | bool                                         | O   |
| cellGlobalIdentity       | Глобальный идентификатор соты в сетях E-UTRAN, `Cell-Global-Identity`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272).                        | string                                       | O   |
| geodeticInformation      | Геодезическое местоположение отправки вызова, `Geodetic-Information`. См. [Recommendation&nbsp;ITU-T&nbsp;Q.763](https://www.itu.int/rec/T-REC-Q.763-199912-I/en).                                      | string                                       | O   |
| geographicalInformation  | Описание географической области, `Geographical-Information`. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.032](https://www.etsi.org/deliver/etsi_ts/123000_123099/123032/18.01.00_60/ts_123032v180100p.pdf). | string                                       | O   |
| locationAreaIdentity     | Область отслеживания, `Location-Area-Identity`.                                                                                                                                               | hex                                          | O   |
| routingAreaIdentity      | Область маршрутизации, `Routing-Area-Identity`.                                                                                                                                               | hex                                          | O   |
| serviceAreaIdentity      | Область обслуживания, `Service-Area-Identity`.                                                                                                                                                | hex                                          | O   |
| userCsgInformation       | Информация о доступе к ячейке закрытой группы, CSG.                                                                                                                                           | [UserCsgInformation](../userCsgInformation/) | O   |

### Пример ###

```json
{
  "ageOfLocationInformation": 1,
  "cellGlobalIdentity": "00f1102b2d1010",
  "routingAreaIdentity": "00f1100001"
}
```