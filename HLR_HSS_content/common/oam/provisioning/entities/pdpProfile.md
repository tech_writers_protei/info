
## Параметры профиля PDP PdpProfile

| Поле                       | Описание                                                                                                                                                                                                                                                   | Тип    | O/M |
|----------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| context-id                 | Идентификатор контекста PDP.                                                                                                                                                                                                                               | string | М   |
| qosSubscribed              | QoS-Subscribed, октеты 3-5. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                                                             | string | O   |
| address                    | Информация о сервисе.pass:q[\<br\>]**Примечание.** Используется только в команде [getProfile](../../getProfile/).                                                                                                                                                    | string | O   |
| type                       | Тип контекста PDP. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.060](https://www.etsi.org/deliver/etsi_ts/123000_123099/123060/17.00.00_60/ts_123060v170000p.pdf).pass:q[\<br\>]`F121` -- IPv4; `F157` -- IPv6.pass:q[\<br\>]**Примечание.** Используется только в команде [getProfile](../../getProfile/). | string | О   |
| vplmnAddressAllowed        | Флаг разрешения оборудованию пользователя использовать динамический адрес в домене VPLMN.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                              | bool   | O   |
| apn                        | Имя точки доступа.pass:q[\<br\>]**Примечание.** Необязателен при удалении.                                                                                                                                                                                           | string | C   |
| extQosSubscribed           | Ext-QoS-Subscribed, приоритет распределения и хранения, октеты 6-13. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                    | string | O   |
| pdpChargingCharacteristics | Параметры тарификации, `ChargingCharacteristics`. См.&nbsp;[3GPP&nbsp;TS&nbsp;32.215](https://www.etsi.org/deliver/etsi_ts/132200_132299/132215/05.09.00_60/ts_132215v050900p.pdf).                                                                                       | string | O   |
| extPdpAddress              | Расширенный адрес контекста PDP.                                                                                                                                                                                                                           | string | O   |
| extPdpType                 | Расширенный тип контекста PDP. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.060](https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf).                                                                                                          | string | O   |
| ext2QosSubscribed          | Ext2-QoS-Subscribed, октеты 14-16. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                                                      | string | O   |
| ext3QosSubscribed          | Ext3-QoS-Subscribed, октеты 17-18. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                                                      | string | O   |
| ext4QosSubscribed          | Ext4-QoS-Subscribed, индикаторы `ARP`, `PL`, `PCI` и `PVI`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                                                             | string | O   |

### Пример ###

```json
{
  "context-id": 1,
  "qosSubscribed": "apn.replace.protei.ru",
  "vplmnAddressAllowed": 1,
  "apn": "internet.protei.ru",
  "extQosSubscribed": "035F38",
  "pdpChargingCharacteristics": "0800",
  "ext2QosSubscribed": "035F3834ACEC"
}
```