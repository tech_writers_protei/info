== Параметры разрешений Permission

[width="100%",cols="41%,47%,8%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M
|link:#permissions[permissions] |Права доступа пользователя. Формат:pass:q[<br>]`ADERW`. |[string] |М
|[[group-id-permission]]groupId |Идентификатор группы, к которой относится пользователь. |int |C
|[[group-name-permission]]groupName |Название группы, к которой относится пользователь. |string |C
|delete |Флаг удаления значения параметра. |bool |O
|===

*Примечание.* Задается только один из параметров link:#group-id-permission[groupId] и link:#group-name-permission[groupName].

=== Пример

[source,json]
----
{
  "permissions": "RW",
  "groupName": "gr1"
}
----

=== [[permissions]]Описание прав доступа

[cols=",",options="header",]
|===
|Код |Описание
|A |add, добавление профиля
|C |config AUC, управление конфигурацией AUC
|D |delete, удаление профиля
|E |external, управление внешними узлами
|N |network, управление профилем со стороны сети
|R |read, чтение данных профиля
|S |sim, управление профилями AUC
|U |user, управление пользователями
|W |write, изменение данных профиля
|===
