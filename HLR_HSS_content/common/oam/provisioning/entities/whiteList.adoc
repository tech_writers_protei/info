== Параметры белого списка WhiteList

[width="100%",cols="11%,61%,25%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|action |Тип действия. `create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|name |Название списка. |string |M
|vlr_masks |Перечень масок номера узла VLR. |[link:../wlVlrMask/[WLVlrMask]] |O
|plmn_masks |Перечень масок сети PLMN. |[link:../wlPlmnMask/[WLPlmnMask]] |O
|===

=== Пример

[source,json]
----
{
  "action": "create",
  "name": "whitelist",
  "vlr_masks": [
    {
      "mask": "7689",
      "country": "rus",
      "network": "supertelecom"
    }
  ]
}
----
