
## Параметры местоположения абонента c узла VLR/SGSN CsPsLocationInformation

| Поле                             | Описание                                                                | Тип                                                                                                            | O/M |
|----------------------------------|-------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|-----|
| ageOfLocationInformation         | Значение Age of Location.                                               | int                                                                                                            | O   |
| cellGlobalIdOrServiceAreaIdOrLAI | Идентификатор глобальной соты, зоны обслуживания или локальной области. | string                                                                                                         | O   |
| mscNumber                        | Номер узла MSC.                                                         | string                                                                                                         | O   |
| locationInformationEps           | Информация о местоположении в EPS.                                      | [MmeLocationInformation](../mmeLocationInformation/)pass:q[\<br\>][SgsnLocationInformation](../sgsnLocationInformation/) | O   |

### Пример ###

```json
{
  "ageOfLocationInformation":1,
  "cellGlobalIdOrServiceAreaIdOrLAI":"00f1102b2d1010",
  "mscNumber":"97572782157",
  "locationInformationEps": {
    "ageOfLocationInformation":1,
    "eUtranCellGlobalIdentity":"00f1168b2d1234",
    "trackingAreaIdentity":"00f1100001"
  }
}
```