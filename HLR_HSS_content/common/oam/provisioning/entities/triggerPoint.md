
## Параметры триггерной точки TriggerPoint

| Поле             | Описание                                                                                                  | Тип                                              | O/M |
|------------------|-----------------------------------------------------------------------------------------------------------|--------------------------------------------------|-----|
| ConditionTypeCNF | Тип отношения между элементами.pass:q[\<br\>]`0` -- конъюнкция, логическое И;pass:q[\<br\>]`1` -- дизъюнкция, логическое ИЛИ. | int                                              | M   |
| Spt              | Перечень триггерных точек триггера.                                                                       | [[ServicePointTrigger](../servicePointTrigger/)] | M   |

### Пример ###

```json
{
  "ConditionTypeCNF": 1,
  "Spt": [
    {
      "Group": 1,
      "Method": "INVITE",
      "SessionCase": 1,
      "ConditionNegated": 1,
      "Type": 3,
      "RequestUri": "http://ims.protei.ru/spt1",
      "Header": "header",
      "Content": "headerContent",
      "SdpLine": "sdpLine",
      "SdpLineContent": "sdpLineContent",
      "RegistrationType": 1
    }
  ]
}
```