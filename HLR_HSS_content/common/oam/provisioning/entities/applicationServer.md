
## Параметры сервера приложений ApplicationServer

| Поле                     | Описание                                                                                         | Тип    | O/M |
|--------------------------|--------------------------------------------------------------------------------------------------|--------|-----|
| ServerName               | Имя сервера.                                                                                     | string | M   |
| DefaultHandling          | Действие при обработке вызова по умолчанию.pass:q[\<br\>]`0` -- продолжить вызов; `1` -- отбить вызов.     | int    | O   |
| ServiceInfo              | Информация о сервисе.                                                                            | string | O   |
| Host                     | Хост сервера приложений.                                                                         | string | O   |
| Realm                    | Реалм сервера приложений.                                                                        | string | O   |
| RepositoryDataSizeLimit  | Максимальный размер хранилища, в байтах.                                                         | int    | O   |
| Udr                      | Флаг обработки команды Diameter Sh: UDR от сервера приложений.                                   | bool   | O   |
| Pur                      | Флаг обработки команды Diameter Sh: PUR от сервера приложений.                                   | bool   | O   |
| Snr                      | Флаг обработки команды Diameter Sh: SNR от сервера приложений.                                   | bool   | O   |
| UdrRepositoryData        | Флаг обработки параметра RepositoryData в команде Diameter Sh: UDR от сервера приложений.        | bool   | O   |
| UdrImpu                  | Флаг обработки параметра Impu в команде Diameter Sh: UDR от сервера приложений.                  | bool   | O   |
| UdrImsUserState          | Флаг обработки параметра Impu в команде Diameter Sh: UDR от сервера приложений.                  | bool   | O   |
| UdrScscfName             | Флаг обработки параметра ScscfName в команде Diameter Sh: UDR от сервера приложений.             | bool   | O   |
| UdrIfc                   | Флаг обработки параметра iFC в команде Diameter Sh: UDR от сервера приложений.                   | bool   | O   |
| UdrLocation              | Флаг обработки параметра Location в команде Diameter Sh: UDR от сервера приложений.              | bool   | O   |
| UdrUserState             | Флаг обработки параметра UserState в команде Diameter Sh: UDR от сервера приложений.             | bool   | O   |
| UdrChargingInfo          | Флаг обработки параметра ChargingInfo в команде Diameter Sh: UDR от сервера приложений.          | bool   | O   |
| UdrMsisdn                | Флаг обработки параметра MSISDN в команде Diameter Sh: UDR от сервера приложений.                | bool   | O   |
| UdrPsiActivation         | Флаг обработки параметра PsiActivation в команде Diameter Sh: UDR от сервера приложений.         | bool   | O   |
| UdrDsai                  | Флаг обработки параметра Dsai в команде Diameter Sh: UDR от сервера приложений.                  | bool   | O   |
| UdrAliasesRepositoryData | Флаг обработки параметра AliasesRepositoryData в команде Diameter Sh: UDR от сервера приложений. | bool   | O   |
| PurRepositoryData        | Флаг обработки параметра RepositoryData в команде Diameter Sh: PUR от сервера приложений.        | bool   | O   |
| PurPsiActivation         | Флаг обработки параметра PsiActivation в команде Diameter Sh: PUR от сервера приложений.         | bool   | O   |
| PurDsai                  | Флаг обработки параметра Dsai в команде Diameter Sh: PUR от сервера приложений.                  | bool   | O   |
| PurAliasesRepositoryData | Флаг обработки параметра AliasesRepositoryData в команде Diameter Sh: PUR от сервера приложений. | bool   | O   |
| SnrRepositoryData        | Флаг обработки параметра RepositoryData в команде Diameter Sh: SNR от сервера приложений.        | bool   | O   |
| SnrImpu                  | Флаг обработки параметра Impu в команде Diameter Sh: SNR от сервера приложений.                  | bool   | O   |
| SnrImsUserState          | Флаг обработки параметра ImsUserState в команде Diameter Sh: SNR от сервера приложений.          | bool   | O   |
| SnrScscfName             | Флаг обработки параметра ScscfName в команде Diameter Sh: SNR от сервера приложений.             | bool   | O   |
| SnrIfc                   | Флаг обработки параметра IFC в команде Diameter Sh: SNR от сервера приложений.                   | bool   | O   |
| SnrPsiActivation         | Флаг обработки параметра PsiActivation в команде Diameter Sh: SNR от сервера приложений.         | bool   | O   |
| SnrDsai                  | Флаг обработки параметра Dsai в команде Diameter Sh: SNR от сервера приложений.                  | bool   | O   |
| SnrAliasesRepositoryData | Флаг обработки параметра AliasesRepositoryData в команде Diameter Sh: SNR от сервера приложений. | bool   | O   |
| IncludeRegisterRequest   | Флаг отправки запроса абонента к серверу приложений для регистрации на другом узле S-CSCF.       | bool   | O   |
| IncludeRegisterResponse  | Флаг оповещения сервера приложений о регистрации абонента на другом узле S-CSCF.                 | bool   | O   |

### Пример ###

```json
{
  "ServerName": "as",
  "DefaultHandling": 1,
  "ServiceInfo": "serviceInfo",
  "Host": "as.ims.protei.ru",
  "Realm": "ims.protei.ru",
  "RepositoryDataSizeLimit": 1000,
  "Udr": 0,
  "Pur": 0,
  "Snr": 0,
  "UdrRepositoryData": 1,
  "UdrImpu": 1,
  "UdrImsUserState": 1,
  "UdrScscfName": 1,
  "UdrIfc": 1,
  "UdrLocation": 1,
  "UdrUserState": 1, 
  "UdrChargingInfo": 1,
  "UdrMsisdn": 1,
  "UdrPsiActivation": 1,
  "UdrDsai": 1,
  "UdrAliasesRepositoryData": 1,
  "PurRepositoryData": 0, 
  "PurPsiActivation": 0,
  "PurDsai": 0,
  "PurAliasesRepositoryData": 0,
  "SnrRepositoryData": 0,
  "SnrImpu": 0,
  "SnrImsUserState": 0, 
  "SnrScscfName": 0,
  "SnrIfc": 0,
  "SnrPsiActivation": 0,
  "SnrDsai": 0,
  "SnrAliasesRepositoryData": 0, 
  "IncludeRegisterResponse": 1,
  "IncludeRegisterRequest": 1
}
```