== Параметры профиля EPS EpsProfile

[width="99%",cols="8%,88%,1%,1%,2%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|eps_context_id |Идентификатор контекста EPS. |int |M |
|service_selection |Имя услуги или внешней сети, связанных с указанной службой.pass:q[<br>]*Примечание.* Обязателен при создании. |string |C |
|vplmnDynamicAddressAllowed |Флаг разрешения оборудованию пользователя использовать PGW в домене VPLMN. См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123060/17.00.00_60/ts_123060v170000p.pdf[3GPP TS 23.060].pass:q[<br>]По умолчанию: 0. |bool |O |
|qosClassId |Идентификатор класса QoS. См. https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf[3GPP TS 23.203].pass:q[<br>]Диапазон: 5-9.pass:q[<br>]*Примечание.* Возможные значения соответствуют службам non-GBR. |int |O |
|allocateRetPriority |Приоритет при распределении и хранении. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf[3GPP TS 29.212]. |int |O |
|pdnGwType |Индикатор метода задания адреса PGW в параметре MIP6-Agent-Info.pass:q[<br>]`0` -- статический; `1` -- динамический. |int |O |
|mip6Ip4 |IPv4-адрес шлюза PDN. |ip |O |
|mip6Ip6 |IPv6-адрес шлюза PDN. |string |O |
|mip6AgentHost |Имя хоста домашнего агента шлюза PDN. См. https://datatracker.ietf.org/doc/html/rfc5447[RFC 5447]. |string |O |
|mip6AgentRealm |Значение `Destination-Realm` для шлюза PDN.pass:q[<br>]Формат:pass:q[<br>]`epc.mnc<MNC>.mcc<MCC>.3gppnetwork.org`. |string |O |
|visitedNetworkId |Идентификатор сети PLMN шлюза PDN. |string |O |
|chargingCharacteristics |Значение `Charging Characteristics`. |string |O |
|apnOiReplacement |Доменное имя для подмены APN OI. См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf[3GPP TS 23.003] и https://www.etsi.org/deliver/etsi_ts/129300_129399/129303/18.00.00_60/ts_129303v180000p.pdf[3GPP TS 29.303].pass:q[<br>]*Примечание.* Значение должно быть символьной строкой с одним или несколькими именами, разделенными точками. |string |O |
|maxDl |Максимальная запрашиваемая ширина полосы нисходящего канала, байты в секунду.pass:q[<br>]Диапазон: 1-(232 - 1). |int |O |
|maxUl |Максимальная запрашиваемая ширина полосы восходящего канала, байты в секунду.pass:q[<br>]Диапазон: 1-(232 - 1). |int |O |
|extMaxDl |Максимальная ширина полосы нисходящего канала со внешними системами, килобиты в секунду.pass:q[<br>]Диапазон: 4 294 968-(232 - 1). |string |O |
|extMaxUl |Максимальная ширина полосы восходящего канала внешних систем, килобиты в секунду.pass:q[<br>]Диапазон: 4 294 968-(232 - 1). |int |O |
|pdnType |Индикатор типа адреса PDN.pass:q[<br>]`0` -- IPv4, только IP версии 4;`1` -- IPv6, только IP версии 6;`2` -- IPv4v6, IP версии 4 и версии 6;`3` -- IPv4_OR_IPv6, только IP версии 4 или версии 6. |int |O |
|preEmptionCapability |Значение `Pre-emption Capability`. |int |O |
|preEmptionVulnerability |Значение `Pre-emption Vulnerability`. |int |O |
|ipv4 |Статический IPv4-адрес, присвоенный устройству абонента. |string |O |2.0.50.0
|ipv6 |Статический IPv6-адрес, присвоенный устройству абонента. |string |O |2.0.50.0
|plmnId |Идентификатор сети PLMN. |string |O |
|interworking5GSIndicator |Флаг подписки для взаимодействия сетей 5GS и EPS. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272]. |bool |O |2.0.35.0
|nonIpPdnTypeIndicator |Флаг индикации не-IP типа PDN точки доступа. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272]. |bool |O |2.0.36.0
|nonIpDataDeliveryMechanism |Код механизма передачи не-IP данных. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272].pass:q[<br>]`0` -- SGi-BASED-DATA-DELIVERY; `1` -- SCEF-BASED-DATA-DELIVERY. |int |O |2.0.36.0
|preferredDataMode |Битовая маска способа передачи трафика.pass:q[<br>]`0` -- Data over User Plane Preferred; `1` -- Data over Control Plane Preferred. |int |O |2.0.36.0
|pdnConnectionContinuity |Код метода обработки данных PDN-соединения при переходе между широкополосными и узкополосными сетями. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf[3GPP TS 29.272].pass:q[<br>]`0` -- MAINTAIN-PDN-CONNECTION;pass:q[<br>]`1` -- DISCONNECT-PDN-CONNECTION-WITH-REACTIVATION-REQUEST;pass:q[<br>]`2` -- DISCONNECT-PDN-CONNECTION-WITHOUT-REACTIVATION-REQUEST. |int |O |2.0.36.0
|rdsIndicator |Флаг использования службы надежной передачи данных. |bool |O |2.0.36.0
|scefName |Название узла SCEF. |string |O |2.0.36.0
|===

=== Пример

[source,json]
----
{
  "allocateRetPriority": 4,
  "eps_context_id": 1,
  "interworking5GSIndicator": 1,
  "maxDl": 1000,
  "maxUl": 1000,
  "nonIpDataDeliveryMechanism": 1,
  "nonIpPdnTypeIndicator": 1,
  "pdnConnectionContinuity": 1,
  "pdnType": 1,
  "preEmptionCapability": 1,
  "preEmptionVulnerability": 1,
  "preferredDataMode": 1,
  "qosClassId": 6,
  "rdsIndicator": 1,
  "scefName": "scef1",
  "service_selection": "test.protei.ru",
  "vplmnDnamicAddressAllowed": 1
}
----
