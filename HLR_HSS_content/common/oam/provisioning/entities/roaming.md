
## Параметры роуминга Roaming

| Поле              | Описание                                                                                                                                               | Тип      | O/M |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|
| vlr               | Номер узла VLR.                                                                                                                                        | string   | O   |
| msc               | Номер узла MSC.                                                                                                                                        | string   | O   |
| mscAreaRestricted | Флаг активации запрета области.                                                                                                                        | bool     | O   |
| vGmlc             | Номер узла GMLC.                                                                                                                                       | string   | O   |
| supportedVlrCamel | Поддерживаемые фазы CAMEL.                                                                                                                             | int      | O   |
| supportedVlrLcs   | Идентификатор `Supported-Features`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229/17.02.00_60/ts_129229v170200p.pdf). | [int]    | O   |
| ueLcsNotSupported | Флаг отсутствия поддержки услуг LCS пользовательским оборудованием.                                                                                    | bool     | O   |
| version           | Версия MAP.                                                                                                                                            | int      | O   |
| dtregistered      | Дата и время регистрации абонента.                                                                                                                     | datetime | O   |
| ispurged          | Флаг удаления данных абонента на узле VLR.                                                                                                             | bool     | O   |
| dtpurged          | Дата и время удаления данных абонента на узле VLR.                                                                                                     | datetime | O   |
| isRoaming         | Флаг регистрации в роуминговой сети.                                                                                                                   | bool     | O   |