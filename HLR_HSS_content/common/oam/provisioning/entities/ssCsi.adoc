== Параметры профиля SS-CSI SsCsi

[width="100%",cols="12%,64%,18%,2%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|id |Идентификатор профиля SS-CSI.pass:q[<br>]*Примечание.* При создании профиля SS-CSI его идентификатор можно не указывать, если указано имя. |int |С |
|name |Имя профиля SS-CSI. |string |С |2.0.55.0
|ssNotificationToCse |Флаг уведомления об изменениях в профиле SS-CSI. |bool |О |
|ssCsiActive |Идентификатор активного профиля SS-CSI. |int |O |
|notificationToCse |Флаг оповещения о внесенных изменениях профиля. |bool |О |
|csiActive |Идентификатор активного профиля CSI. |int |O |
|gsmScfAddress |Адрес узла IM-SSF. |string |O |
|csiCriterias |Перечень критериев SS-CSI. |[link:../ssCsiCriteria/[SsCsiCriteria]] |O |
|===

=== Пример

[source,json]
----
{
  "id": 1,
  "name": "csi1",
  "camelCapabilityHandling": 3,
  "csiActive": 1,
  "gsmScfAddress": "78924813183138",
  "csiCriterias": [
    { "ssCode": 16 }
  ]
}
----
