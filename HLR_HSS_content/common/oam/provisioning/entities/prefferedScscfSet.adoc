== Параметры профиля аутентификации AucProfile

[width="100%",cols="10%,38%,39%,4%,9%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|SetId |Идентификатор набора. |int |М |
|Name |Имя набора. |string |O |2.0.59.0
|Scscfs |Перечень предпочитаемых узлов S-CSCF. |[link:../prefferedScscf/[PrefferedScscf]] |М |
|===

=== Пример

[source,json]
----
{
  "SetId": 1,
  "Name": "set1",
  "Scscfs": [
    {
      "ScscfName": "scscf1",
      "Priority": 1
    }
  ]
}
----
