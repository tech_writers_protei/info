
## Параметры профиля D-CSI DCsi

| Поле                    | Описание                                                                                                                                          | Тип                                | O/M | Версия   |
|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------|-----|----------|
| id                      | Идентификатор CAMEL Subscriber Information.pass:q[\<br\>]**Примечание.** При создании профиля D-СSI можно не указывать его идентификатор, если указано имя. | int                                | C   |          |
| name                    | Имя CSI-профиля.                                                                                                                                  | string                             | C   | 2.0.55.0 |
| camelCapabilityHandling | Поддерживаемые фазы CAMEL.                                                                                                                        | int                                | O   |          |
| notificationToCse       | Флаг оповещения о внесенных изменениях профиля.                                                                                                   | bool                               | O   |          |
| csiActive               | Идентификатор активного профиля CSI.                                                                                                              | int                                | O   |          |
| csiCriterias            | Перечень критериев D-CSI для соответствия.                                                                                                        | [[DCsiCriteria](../dCsiCriteria/)] | O   |          |

### Пример ###

```json
{
  "camelCapabilityHandling": 3,
  "csiActive": 1,
  "csiCriterias": [
    {
      "defaultCallHandling": 2,
      "dialledNumber": "111",
      "gsmScfAddress": "78924813183138",
      "serviceKey": 1
    }
  ],
  "id": 1,
  "name": "csi1"
}
```