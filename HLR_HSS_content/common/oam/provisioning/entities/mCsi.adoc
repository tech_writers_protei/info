== Параметры профиля M-CSI MCsi

[width="100%",cols="12%,69%,12%,2%,5%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|id |Идентификатор профиля M-CSI.pass:q[<br>]*Примечание.* При создании профиля M-CSI его идентификатор можно не указывать, если указано имя. |int |С |
|name |Имя профиля M-CSI. |string |С |2.0.55.0
|serviceKey |Идентификатор службы, `ServiceKey`. |int |M |
|gsmScfAddress |Адрес узла IM-SSF. |string |M |
|notificationToCse |Флаг оповещения о внесенных изменениях профиля. |bool |О |
|csiActive |Идентификатор активного профиля CSI. |int |O |
|csiMts |Перечень профилей М-CSI МТ. |[link:../mCsiMt/[mCsiMt]] |М |
|===

=== Пример

[source,json]
----
{
  "csiActive": 1,
  "csiMts": [
    { "mmCode": 2 }
  ],
  "gsmScfAddress": "78924813183138",
  "id": 1,
  "name": "csi1",
  "serviceKey": 1
}
----
