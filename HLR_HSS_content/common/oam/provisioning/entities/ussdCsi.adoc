== Параметры профиля USSD-CSI

[width="100%",cols="8%,66%,20%,2%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|id |Идентификатор профиля USSD-CSI.pass:q[<br>]*Примечание.* При создании профиля USSD-CSI его идентификатор можно не указывать, если указано имя. |int |С |
|name |Имя профиля USSD-CSI. |string |С |2.0.55.0
|csiActive |Идентификатор активного профиля CSI. |int |O |
|csiCriterias |Перечень критериев USSD-CSI. |[link:../ussdCsiCriteria/[UssdCsiCriteria]] |O |
|===

=== Пример

[source,json]
----
{
  "id": 1,
  "name": "csi1",
  "csiActive": 1,
  "csiCriterias": [
    {
      "ussd": "*100#",
      "gsmScfAddress": "32147642342",
      "destinationReference": 1
    }
  ]
}
----
