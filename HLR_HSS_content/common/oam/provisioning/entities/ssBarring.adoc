== Параметры дополнительных услуг запрета SsBarring

[width="100%",cols="17%,80%,2%,1%",options="header",]
|===
|Поле |Описание |Тип |O/M
|link:#ss-code-ss-barring[ss_Code] |Код дополнительной услуги SS, `SS-Code`. См. https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf[3GPP TS 29.002].pass:q[<br>]*Внимание.* Допустимые значения: `17`, `18`, `65`, `66`, `81`, `245`. |int |M
|ss_Status |Статус дополнительной услуги SS, `SS-Status`. См. https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf[3GPP TS 29.002]. |int |M
|link:#tele-service-ss-barring[tele_service] |Перечень идентификаторов телеуслуг.pass:q[<br>]*Внимание.* Допустимые значения: `0`, `16`, `32`, `96`, `112`, `128`, `144`. |[int] |O
|link:#bearer-service-ss-barring[bearer_service] |Перечень идентификаторов служб передачи данных.pass:q[<br>]*Внимание.* Допустимые значения: `-1`, `0`, `16`, `32`, `96`, `112`, `128`, `144`. |[int] |O
|===

=== Пример

[source,json]
----
{
  "ss_Code": 154,
  "ss_Status": 5,
  "tele_service": [ 0, 16, 32 ]
}
----

==== [[ss-code-ss-barring]]Коды дополнительных услуг

[source,text]
----
SS-Code ::= OCTET STRING (SIZE (1))
-- This type is used to represent the code identifying a single
-- supplementary service, a group of supplementary services, or
-- all supplementary services. The services and abbreviations
-- used are defined in GSM 02.04. The internal structure is 
-- defined as follows:

-------------


-- bits 87654321: group (bits 8765), and specific service
-- (bits 4321)

allSS   SS-Code ::= '00000000'B-'0'Dec
-- reserved for possible future use
-- all SS

allLineIdentificationSS SS-Code ::= '00010000'B-'16'Dec
-- reserved for possible future use
-- all line identification SS
clip    SS-Code ::= '00010001'B-'17'Dec
-- calling line identification presentation
clir    SS-Code ::= '00010010'B-'18'Dec
-- calling line identification restriction
colp    SS-Code ::= '00010011'B-'19'Dec
-- connected line identification presentation
colr    SS-Code ::= '00010100'B-'20'Dec
-- connected line identification restriction
mci SS-Code ::= '00010101'B-'21'Dec
-- reserved for possible future use
-- malicious call identification
allNameIdentificationSS SS-Code ::= '00011000'B-'24'Dec
-- all name identification SS
cnap    SS-Code ::= '00011001'B-'27'Dec
-- calling name presentation

-- SS-Codes '00011010'B to '00011111'B are reserved for future 
-- NameIdentification Supplementary Service use.

allForwardingSS SS-Code ::= '00100000'B-'32'Dec
-- all forwarding SS
cfu SS-Code ::= '00100001'B-'33'Dec

-- call forwarding unconditional
allCondForwardingSS SS-Code ::= '00101000'B-'40'Dec
-- all conditional forwarding SS
cfb SS-Code ::= '00101001'B-'41'Dec
-- call forwarding on mobile subscriber busy
cfnry   SS-Code ::= '00101010'B-'42'Dec
-- call forwarding on no reply
cfnrc   SS-Code ::= '00101011'B-'43'Dec
-- call forwarding on mobile subscriber not reachable
cd  SS-Code ::= '00100100'B-'36'Dec
-- call deflection

allCallOfferingSS   SS-Code ::= '00110000'B-'48'Dec
-- reserved for possible future use
-- all call offering SS includes also all forwarding SS
ect SS-Code ::= '00110001'B-'49'Dec
-- explicit call transfer
mah SS-Code ::= '00110010'B-'50'Dec
-- reserved for possible future use
-- mobile access hunting
----

==== [[tele-service-ss-barring]]Идентификаторы телеуслуг

[source,text]
----
MAP-TS-Code {
  itu-t identified-organization (4) etsi (0) mobileDomain (0)
  gsm-Network (1) modules (3) map-TS-Code (19) version15 (15)}

DEFINITIONS

::=

BEGIN

TeleserviceCode ::= OCTET STRING (SIZE (1))
-- This type is used to represent the code identifying a single
-- teleservice, a group of teleservices, or all teleservices. The
-- services are defined in GSM 22.003. 
-- The internal structure is defined as follows:
-- bits 87654321: group (bits 8765) and specific service
-- (bits 4321)

Ext-TeleserviceCode ::= OCTET STRING (SIZE (1..5))
-- This type is used to represent the code identifying a single
-- teleservice, a group of teleservices, or all teleservices. The
-- services are defined in GSM 22.003. 
-- The internal structure is defined as follows:

-- OCTET 1:
-- bits 87654321: group (bits 8765) and specific service
-- (bits 4321)

-- OCTETS 2-5: reserved for future use. If received the
-- Ext-TeleserviceCode shall be
-- treated according to the exception handling defined for the
-- operation that uses this type.

-- Ext-TeleserviceCode includes all values defined for TeleserviceCode.

allTeleservices TeleserviceCode ::= '00000000'B = '0'Dec
allSpeechTransmissionServices   TeleserviceCode ::= '00010000'B = '16'Dec
telephony   TeleserviceCode ::= '00010001'B = '17'Dec
emergencyCalls  TeleserviceCode ::= '00010010'B = '18'Dec

allShortMessageServices TeleserviceCode ::= '00100000'B = '32'Dec
shortMessageMT-PP   TeleserviceCode ::= '00100001'B = '33'Dec
shortMessageMO-PP   TeleserviceCode ::= '00100010'B = '34'Dec

allFacsimileTransmissionServices    TeleserviceCode ::= '01100000'B = '96'Dec
facsimileGroup3AndAlterSpeech   TeleserviceCode ::= '01100001'B = '97'Dec
automaticFacsimileGroup3    TeleserviceCode ::= '01100010'B = '98'Dec
facsimileGroup4 TeleserviceCode ::= '01100011'B = '99'Dec

-- The following non-hierarchical Compound Teleservice Groups
-- are defined in 3GPP TS 22.030: 
allDataTeleservices TeleserviceCode ::= '01110000'B = '112'Dec
-- covers Teleservice Groups 'allFacsimileTransmissionServices'
-- and 'allShortMessageServices'
allTeleservices-ExeptSMS    TeleserviceCode ::= '10000000'B = '128'Dec
-- covers Teleservice Groups 'allSpeechTransmissionServices' and
-- 'allFacsimileTransmissionServices'

-------------


-- Compound Teleservice Group Codes are only used in call
-- independent supplementary service operations, i.e. they
-- are not used in InsertSubscriberData or in
-- DeleteSubscriberData messages.

allVoiceGroupCallServices   TeleserviceCode ::= '10010000'B = '144'Dec
voiceGroupCall  TeleserviceCode ::= '10010001'B = '145'Dec
voiceBroadcastCall  TeleserviceCode ::= '10010010'B = '146'Dec
allPLMN-specificTS  TeleserviceCode ::= '11010000'B = '208'Dec
plmn-specificTS-1   TeleserviceCode ::= '11010001'B = '209'Dec
plmn-specificTS-2   TeleserviceCode ::= '11010010'B = '210'Dec
plmn-specificTS-3   TeleserviceCode ::= '11010011'B = '211'Dec
plmn-specificTS-4   TeleserviceCode ::= '11010100'B = '212'Dec
plmn-specificTS-5   TeleserviceCode ::= '11010101'B = '213'Dec
plmn-specificTS-6   TeleserviceCode ::= '11010110'B = '214'Dec
plmn-specificTS-7   TeleserviceCode ::= '11010111'B = '215'Dec
plmn-specificTS-8   TeleserviceCode ::= '11011000'B = '216'Dec
plmn-specificTS-9   TeleserviceCode ::= '11011001'B = '217'Dec
plmn-specificTS-A   TeleserviceCode ::= '11011010'B = '218'Dec
plmn-specificTS-B   TeleserviceCode ::= '11011011'B = '219'Dec
plmn-specificTS-C   TeleserviceCode ::= '11011100'B = '220'Dec
plmn-specificTS-D   TeleserviceCode ::= '11011101'B = '221'Dec
plmn-specificTS-E   TeleserviceCode ::= '11011110'B = '222'Dec
plmn-specificTS-F   TeleserviceCode ::= '11011111'B = '223'Dec
----

==== [[bearer-service-ss-barring]]Идентификаторы служб передачи данных

[source,text]
----
MAP-BS-Code {
  itu-t identified-organization (4) etsi (0) mobileDomain (0)
  gsm-Network (1) modules (3) map-BS-Code (20) version15 (15)}

DEFINITIONS

::=

BEGIN
BearerServiceCode ::= OCTET STRING (SIZE (1))
-- This type is used to represent the code identifying a single
-- bearer service, a group of bearer services, or all bearer
-- services. The services are defined in 3GPP TS 22.002. 
-- The internal structure is defined as follows:

-------------


-- plmn-specific bearer services:
-- bits 87654321: defined by the HPLMN operator

-- rest of bearer services:
-- bit 8: 0 (unused)
-- bits 7654321: group (bits 7654), and rate, if applicable
-- (bits 321)

Ext-BearerServiceCode ::= OCTET STRING (SIZE (1..5))
-- This type is used to represent the code identifying a single
-- bearer service, a group of bearer services, or all bearer
-- services. The services are defined in 3GPP TS 22.002. 
-- The internal structure is defined as follows:

-------------


-- OCTET 1:
-- plmn-specific bearer services:
-- bits 87654321: defined by the HPLMN operator

-------------


-- rest of bearer services:
-- bit 8: 0 (unused)
-- bits 7654321: group (bits 7654), and rate, if applicable
-- (bits 321)


-- OCTETS 2-5: reserved for future use. If received the
-- Ext-TeleserviceCode shall be
-- treated according to the exception handling defined for the
-- operation that uses this type. 

-- Ext-BearerServiceCode includes all values defined for BearerServiceCode.

allBearerServices   BearerServiceCode ::= '00000000'B = '0'Dec

allDataCDA-Services BearerServiceCode ::= '00010000'B = '16'Dec
dataCDA-300bps  BearerServiceCode ::= '00010001'B = '17'Dec
dataCDA-1200bps BearerServiceCode ::= '00010010'B = '18'Dec
dataCDA-1200-75bps  BearerServiceCode ::= '00010011'B = '19'Dec
dataCDA-2400bps BearerServiceCode ::= '00010100'B = '20'Dec
dataCDA-4800bps BearerServiceCode ::= '00010101'B = '21'Dec
dataCDA-9600bps BearerServiceCode ::= '00010110'B = '22'Dec
general-dataCDA BearerServiceCode ::= '00010111'B = '23'Dec

allDataCDS-Services BearerServiceCode ::= '00011000'B = '24'Dec
dataCDS-1200bps BearerServiceCode ::= '00011010'B = '26'Dec
dataCDS-2400bps BearerServiceCode ::= '00011100'B = '28'Dec
dataCDS-4800bps BearerServiceCode ::= '00011101'B = '29'Dec
dataCDS-9600bps BearerServiceCode ::= '00011110'B = '30'Dec
general-dataCDS BearerServiceCode ::= '00011111'B = '31'Dec

allPadAccessCA-Services BearerServiceCode ::= '00100000'B = '32'Dec
padAccessCA-300bps  BearerServiceCode ::= '00100001'B = '33'Dec
padAccessCA-1200bps BearerServiceCode ::= '00100010'B = '34'Dec
padAccessCA-1200-75bps  BearerServiceCode ::= '00100011'B = '35'Dec
padAccessCA-2400bps BearerServiceCode ::= '00100100'B = '36'Dec
padAccessCA-4800bps BearerServiceCode ::= '00100101'B = '37'Dec
padAccessCA-9600bps BearerServiceCode ::= '00100110'B = '38'Dec
general-padAccessCA BearerServiceCode ::= '00100111'B = '39'Dec

allDataPDS-Services BearerServiceCode ::= '00101000'B = '40'Dec
dataPDS-2400bps BearerServiceCode ::= '00101100'B = '44'Dec
dataPDS-4800bps BearerServiceCode ::= '00101101'B = '45'Dec
dataPDS-9600bps BearerServiceCode ::= '00101110'B = '46'Dec
general-dataPDS BearerServiceCode ::= '00101111'B = '47'Dec

allAlternateSpeech-DataCDA  BearerServiceCode ::= '00110000'B = '48'Dec

allAlternateSpeech-DataCDS  BearerServiceCode ::= '00111000'B = '56'Dec

allSpeechFollowedByDataCDA  BearerServiceCode ::= '01000000'B = '64'Dec

allSpeechFollowedByDataCDS  BearerServiceCode ::= '01001000'B = '72'Dec

-- The following non-hierarchical Compound Bearer Service
-- Groups are defined in 3GPP TS 22.030: 
allDataCircuitAsynchronous  BearerServiceCode ::= '01010000'B = '80'Dec
-- covers "allDataCDA-Services", "allAlternateSpeech-DataCDA" and
-- "allSpeechFollowedByDataCDA"
allAsynchronousServices BearerServiceCode ::= '01100000'B = '96'Dec
-- covers "allDataCDA-Services", "allAlternateSpeech-DataCDA",
-- "allSpeechFollowedByDataCDA" and "allPadAccessCDA-Services"
allDataCircuitSynchronous   BearerServiceCode ::= '01011000'B = '88'Dec
-- covers "allDataCDS-Services", "allAlternateSpeech-DataCDS" and
-- "allSpeechFollowedByDataCDS"
allSynchronousServices  BearerServiceCode ::= '01101000'B = '104'Dec
-- covers "allDataCDS-Services", "allAlternateSpeech-DataCDS",
-- "allSpeechFollowedByDataCDS" and "allDataPDS-Services"

-------------


-- Compound Bearer Service Group Codes are only used in call
-- independent supplementary service operations, i.e. they
-- are not used in InsertSubscriberData or in
-- DeleteSubscriberData messages.

allPLMN-specificBS  BearerServiceCode ::= '11010000'B = '208'Dec
plmn-specificBS-1   BearerServiceCode ::= '11010001'B = '209'Dec
plmn-specificBS-2   BearerServiceCode ::= '11010010'B = '210'Dec
plmn-specificBS-3   BearerServiceCode ::= '11010011'B = '211'Dec
plmn-specificBS-4   BearerServiceCode ::= '11010100'B = '212'Dec
plmn-specificBS-5   BearerServiceCode ::= '11010101'B = '213'Dec
plmn-specificBS-6   BearerServiceCode ::= '11010110'B = '214'Dec
plmn-specificBS-7   BearerServiceCode ::= '11010111'B = '215'Dec
plmn-specificBS-8   BearerServiceCode ::= '11011000'B = '216'Dec
plmn-specificBS-9   BearerServiceCode ::= '11011001'B = '217'Dec
plmn-specificBS-A   BearerServiceCode ::= '11011010'B = '218'Dec
plmn-specificBS-B   BearerServiceCode ::= '11011011'B = '219'Dec
plmn-specificBS-C   BearerServiceCode ::= '11011100'B = '220'Dec
plmn-specificBS-D   BearerServiceCode ::= '11011101'B = '221'Dec
plmn-specificBS-E   BearerServiceCode ::= '11011110'B = '222'Dec
plmn-specificBS-F   BearerServiceCode ::= '11011111'B = '223'Dec
----
