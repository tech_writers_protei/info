
## Параметры статуса абонента EPS EpsUserState

| Поле                                   | Описание                                                                                                     | Тип | O/M |
|----------------------------------------|--------------------------------------------------------------------------------------------------------------|-----|-----|
| [mmeUserState](#state-eps-user-state)  | Значение `MME-User-State`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272).  | int | O   |
| [sgsnUserState](#state-eps-user-state) | Значение `SGSN-User-State`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272). | int | O   |

### Пример ###

```json
{
  "mmeUserState": 1
}
```

#### [[state-eps-user-state]]Коды статуса абонента

| Код | Состояние                          |
|-----|------------------------------------|
| 0   | DETACHED                           |
| 1   | ATTACHED_NOT_REACHABLE_FOR_PAGING  |
| 2   | ATTACHED_REACHABLE_FOR_PAGING      |
| 3   | CONNECTED_NOT_REACHABLE_FOR_PAGING |
| 4   | CONNECTED_REACHABLE_FOR_PAGING     |