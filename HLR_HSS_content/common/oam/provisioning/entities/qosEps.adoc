== Параметры профиля QoS EPS

[width="100%",cols="12%,85%,2%,1%",options="header",]
|===
|Поле |Описание |Тип |O/M
|qosClassId |Идентификатор класса QoS. См. https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf[3GPP TS 23.203].pass:q[<br>]Диапазон: 5-9.pass:q[<br>]*Примечание.* Возможные значения соответствуют службам non-GBR. |int |O
|allocateRetPriority |Приоритет при распределении и хранении. См. https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf[3GPP TS 29.212]. |int |O
|maxDl |Максимальная запрашиваемая ширина полосы нисходящего канала, биты в секунду.pass:q[<br>]Диапазон: 1-(232 - 1). |int |М
|maxUl |Максимальная запрашиваемая ширина полосы восходящего канала, биты в секунду.pass:q[<br>]Диапазон: 1-(232 - 1). |string |М
|extMaxDl |Максимальная ширина полосы нисходящего канала со внешними системами, килобиты в секунду.pass:q[<br>]Диапазон: 4 294 968-(232 - 1). |string |O
|extMaxUl |Максимальная ширина полосы восходящего канала внешних систем, килобиты в секунду.pass:q[<br>]Диапазон: 4 294 968-(232 - 1). |int |O
|preEmptionCapability |Значение `Pre-emption Capability`. |int |O
|preEmptionVulnerability |Значение `Pre-emption Vulnerability`. |int |O
|name |Имя профиля. |string |O
|===

=== Пример

[source,json]
----
{
  "qosClassId": 6,
  "allocateRetPriority": 4,
  "maxDl": 1000,
  "maxUl": 1000,
  "preEmptionCapability": 1,
  "preEmptionVulnerability": 1
}
----
