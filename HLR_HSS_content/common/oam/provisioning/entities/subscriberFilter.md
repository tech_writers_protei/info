
## Параметры фильтра по абонентам SubscriberFilter

| Поле                                | Описание                                                              | Тип    | O/M | Версия   |
|-------------------------------------|-----------------------------------------------------------------------|--------|-----|----------|
| imsiMask                            | Маска номера IMSI абонента.                                           | regex  | O   | 2.0.46.0 |
| msisdnMask                          | Маска номера MSISDN абонента.                                         | regex  | O   | 2.0.46.0 |
| qosEpsId                            | Идентификатор профиля QoS EPS.                                        | int    | O   | 2.0.46.0 |
| [status](#status-subscriber-filter) | Состояние пользователя.                                               | int    | O   | 2.0.46.0 |
| epsId                               | Идентификатор профиля EPS.                                            | int    | O   | 2.0.46.0 |
| vlrMask                             | Маска VLR.                                                            | string | O   | 2.1.6.0  |
| sortField                           | Ключ сортировки.pass:q[\<br\>]`imsi` / `msisdn`. pass:q[\<br\>]По умолчанию: imsi.        | string | O   | 2.0.46.0 |
| sortDirection                       | Порядок сортировки значений.pass:q[\<br\>]`ASC` / `DESC`.pass:q[\<br\>]По умолчанию: ASC. | string | O   | 2.0.46.0 |

### Пример ###

```json
{
  "imsiMask": "25001%01",
  "sortField": "msisdn",
  "sortDirection": "DESC"
}
```

#### [[status-subscriber-filter]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |