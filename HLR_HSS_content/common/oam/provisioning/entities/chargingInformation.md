
## Параметры тарификации в сетях IMS ChargingInformation

| Поле   | Описание                                                | Тип    | O/M |
|--------|---------------------------------------------------------|--------|-----|
| Name   | Имя профиля учета стоимости в сетях IMS.                | string | M   |
| PriEcf | Имя основной функции учета расходов на основе событий.  | string | O   |
| SecEcf | Имя резервной функции учета расходов на основе событий. | string | O   |
| PriCcf | Имя основной функции сбора тарификационных данных.      | string | O   |
| SecCcf | Имя резервной функции сбора тарификационных данных.     | string | O   |

### Пример ###

```json
{
  "Name": "ci",
  "PriEcf": "priecf@ims.protei.ru",
  "SecEcf": "sececf@ims.protei.ru",
  "PriCcf": "priccf@ims.protei.ru",
  "SecCcf": "secccf@ims.protei.ru"
}
```