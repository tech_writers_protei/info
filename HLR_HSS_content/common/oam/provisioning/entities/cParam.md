
## Параметры CParam

| Поле                        | Описание                                                                                                                                                                                                                  | Тип    | O/M | Версия   |
|-----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|----------|
| id                          | Идентификатор аддитивной постоянной. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).pass:q[\<br\>]**Примечание.** Обязателен для удаления и изменения параметра. | int    | C   |          |
| name                        | Название аддитивной постоянной.pass:q[\<br\>]**Примечание.** Используется для создания параметра.                                                                                                                                   | string | М   | 2.0.48.0 |
| [[c-c-param]]c   | Значение аддитивной постоянной.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                      | string | С   |          |
| [[c1-c-param]]c1 | Значение аддитивной постоянной 1.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                    | string | С   | 2.0.35.0 |
| [[c2-c-param]]c2 | Значение аддитивной постоянной 2.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                    | string | С   | 2.0.35.0 |
| [[c3-c-param]]c3 | Значение аддитивной постоянной 3.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                    | string | С   | 2.0.35.0 |
| [[c4-c-param]]c4 | Значение аддитивной постоянной 4.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                    | string | С   | 2.0.35.0 |
| [[c5-c-param]]c5 | Значение аддитивной постоянной 5.pass:q[\<br\>]**Примечание.** Длина -- 128 бит.                                                                                                                                                    | string | С   | 2.0.35.0 |
| delete                      | Флаг удаления значения.                                                                                                                                                                                                   | bool   | O   |          |
| force                       | Флаг принудительного удаления при наличии связи с профилем абонента.                                                                                                                                                      | bool   | O   |          |
| encrypted                   | Флаг шифрования значений.pass:q[\<br\>]По умолчанию: true.                                                                                                                                                                          | bool   | O   | 2.0.48.0 |

**Примечание.** Задается только [c](#c-c-param) или набор [c1](#c1-c-param), [c2](#c2-c-param), [c3](#c3-c-param), [c4](#c4-c-param), [c5](#c5-c-param).

### Пример ###

```json
{
  "name": "c",
  "с": "123456789009"
}
```

или

```json
{
  "name": "full_c",
  "с1": "11111111111111111111111111111111",
  "с2": "22222222222222222222222222222222",
  "с3": "33333333333333333333333333333333",
  "с4": "44444444444444444444444444444444",
  "с5": "55555555555555555555555555555555"
}
```