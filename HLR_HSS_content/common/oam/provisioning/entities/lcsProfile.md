
## Параметры профиля LCS LcsProfile

| Поле        | Описание                                       | Тип                            | O/M |
|-------------|------------------------------------------------|--------------------------------|-----|
| id          | Идентификатор профиля LCS.                     | int                            | М   |
| gmlcList    | Перечень узлов GMLC, связанных с профилем LCS. | [[LcsGmlc](../lcsGmlc/)]       | O   |
| privacyList | Перечень параметров приватности, Privacy.      | [[LcsPrivacy](../lcsPrivacy/)] | O   |
| molrList    | Перечень параметров запроса LCS MOLR.          | [[LcsMolr](../lcsMolr/)]       | O   |

### Пример ###

```json
{
  "gmlcList": [
    { "gmlcNumber": "2364657" }
  ],
  "id": 1,
  "molrList": [
    {
      "ssCode": 15,
      "ssStatus": 2
    }
  ],
  "privacyList": [
    {
      "ecList": [
        {
          "externalAddress": "1245135",
          "gmlcRestriction": 0,
          "notificationToMsUser": 1
        }
      ],
      "notificationToMsUser": 1,
      "plmnList": [
        { "clientExternalId": 1 }
      ],
      "ssCode": 16,
      "ssStatus": 5
    }
  ]
}
```