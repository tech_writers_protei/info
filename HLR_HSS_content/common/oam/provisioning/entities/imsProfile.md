
## Параметры профиля IMS ImsProfile

| Поле                                 | Описание                                                                                              | Тип                                    | O/M | Версия   |
|--------------------------------------|-------------------------------------------------------------------------------------------------------|----------------------------------------|-----|----------|
| impi                                 | Приватный идентификатор пользователя сети IMS.                                                        | string                                 | M   |          |
| [[auth-scheme]]authScheme | Код схемы аутентификации.pass:q[\<br\>]`4` -- Digest-AKAv1-MD5 (MILENAGE);pass:q[\<br\>]`5` -- SIP Digest;pass:q[\<br\>]`6` -- XOR. | int                                    | M   |          |
| sipDigest                            | Дайджест-аутентификация SIP.pass:q[\<br\>]**Примечание.** Обязателен, если [authScheme](#auth-scheme) = 5.      | [SipDigest](../sipDigest/)             | C   |          |
| impus                                | Публичный идентификатор пользователя сети IMS.                                                        | [[Impu](../impu/)]                     | O   |          |
| implicitlySets                       | Перечень неявно зарегистрированных IMPU.                                                              | [object]                               | O   |          |
| **{**                                |                                                                                                       |                                        |     |          |
| &nbsp;&nbsp;name                     | Имя набора неявно зарегистрированных IMPU.                                                            | string                                 |     |          |
| **}**                                |                                                                                                       |                                        |     |          |
| repositoryData                       | Перечень прозрачных данных.                                                                           | [[RepositoryData](../repositoryData/)] | O   | 2.1.13.0 |
| delete                               | Флаг удаления значения параметра.                                                                     | bool                                   | O   |          |

### Пример ###

```json
{
  "impi": "250010000001@ims.protei.ru",
  "authScheme": 5,
  "sipDigest": {
    "password": "elephant"
  },
  "impus": [
    {
      "Identity": "sip:79000000001@ims.protei.ru",
      "BarringIndication": 0,
      "Type": 0,
      "CanRegister": 1,
      "ServiceProfileName": "sp"
    },
    {
      "Identity": "tel:+79000000001@ims.protei.ru",
      "BarringIndication": 0,
      "Type":1,
      "CanRegister": 1,
      "WildcardPsi": "tel:+79000000001!.*!",
      "PsiActivation": 1,
      "ServiceProfileName": "sp"
    }
  ],
  "implicitlySets": [
    { "name": "implSet1" }
  ],
  "repositoryData": [
    {
      "id": 1,
      "publicIdentity": "tel:+79000000001@ims.protei.ru",
      "sqn": 0,
      "serviceIndication": "Service-Specific",
      "repositoryData": "<ServiceSpecificData><Parameter1>1</Parameter1><Parameter2>two</Parameter2></ServiceSpecificData>"
    }
  ]
}
```