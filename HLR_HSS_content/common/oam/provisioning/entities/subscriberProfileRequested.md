
## Параметры профиля абонента при запросе SubscriberProfileRequested

| Поле                                                  | Описание                                                                                                                                                                                                           | Тип                                                | O/M | Версия   |
|-------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|-----|----------|
| imsi                                                  | Номер IMSI абонента.                                                                                                                                                                                               | string                                             | М   |          |
| msisdn                                                | Номер MSISDN абонента.                                                                                                                                                                                             | string                                             | O   |          |
| [administrative_state](#administrative-state-profile) | Код состояния абонента.                                                                                                                                                                                            | int                                                | O   |          |
| forbid_reg                                            | Флаг запрета регистрации на VLR/SGSN без поддержки фаз CAMEL.                                                                                                                                                      | bool                                               | O   |          |
| roaming_not_allowed                                   | Флаг запрета роуминга. По умолчанию: 0.                                                                                                                                                                            | bool                                               | O   |          |
| odb-param                                             | Запреты, установленные оператором связи.                                                                                                                                                                           | [Odb](../odb/)                                     | O   |          |
| pdp-data                                              | Перечень профилей PDP.                                                                                                                                                                                             | [[PdpProfile](../pdpProfile/)]                     | O   |          |
| eps-context-data                                      | Перечень профилей EPS.                                                                                                                                                                                             | [[EpsProfile](../epsProfile/)]                     | O   |          |
| eps-data                                              | Параметры EPS.                                                                                                                                                                                                     | [EpsData](../epsData/)                             | O   |          |
| csi-list                                              | Перечень профилей CSI Link.                                                                                                                                                                                        | [[CsiLink](../csiLink/)]                           | O   |          |
| oCsi-list                                             | Перечень профилей O-CSI.                                                                                                                                                                                           | [[OCsi](../oCsi/)]                                 | O   |          |
| tCsi-list                                             | Перечень профилей T-CSI.                                                                                                                                                                                           | [[TCsi](../tCsi/)]                                 | O   |          |
| smsCsi-list                                           | Перечень профилей SMS-CSI.                                                                                                                                                                                         | [[SmsCsi](../smsCsi/)]                             | O   |          |
| gprsCsi-list                                          | Перечень профилей GPRS-CSI.                                                                                                                                                                                        | [[GprsCsi](../gprsCsi/)]                           | O   |          |
| mCsi-list                                             | Перечень профилей M-CSI.                                                                                                                                                                                           | [[MCsi](../mCsi/)]                                 | O   |          |
| dCsi-list                                             | Перечень профилей D-CSI.                                                                                                                                                                                           | [[DCsi](../dCsi/)]                                 | O   |          |
| ssCsi-list                                            | Перечень профилей D-CSI.                                                                                                                                                                                           | [[SsCsi](../ssCsi/)]                               | O   |          |
| oImCsi-list                                           | Перечень профилей Originating IP Multimedia CSI.                                                                                                                                                                   | [[OCsi](../oCsi/)]                                 | O   |          |
| vtCsi-list                                            | Перечень профилей VMSC Terminating CSI.                                                                                                                                                                            | [[TCsi](../tCsi/)]                                 | O   |          |
| dImCsi-list                                           | Перечень профилей Dialled IP Multimedia CSI.                                                                                                                                                                       | [[DCsi](../dCsi/)]                                 | O   |          |
| ssData                                                | Перечень дополнительных услуг SS.                                                                                                                                                                                  | [[SsData](../ssData/)]                             | O   |          |
| ssForw                                                | Перечень дополнительных услуг SS переадресации.                                                                                                                                                                    | [[SsForw](../ssForwarding/)]                       | O   |          |
| ssBarring                                             | Перечень дополнительных услуг SS запрета.                                                                                                                                                                          | [[SsBarring](../ssBarring/)]                       | O   |          |
| lcsPrivacy                                            | Перечень профилей LCS Privacy.                                                                                                                                                                                     | [[LcsPrivacy](../lcsPrivacy/)]                     | O   |          |
| lcsMolr                                               | Перечень профилей LCS MOLR.                                                                                                                                                                                        | [[LcsMolr](../lcsMolr/)]                           | O   |          |
| teleserviceList                                       | Перечень идентификаторов телеуслуг.                                                                                                                                                                                | [int]                                              | O   |          |
| bearerserviceList                                     | Перечень идентификаторов служб передачи данных.                                                                                                                                                                    | [int]                                              | O   |          |
| roaming-info                                          | Параметры роуминга.                                                                                                                                                                                                | [RoamingInfo](../roaming/)                         | O   |          |
| roaming-sgsn-info                                     | Параметры роумингового узла SGSN.                                                                                                                                                                                  | [RoamingSgsnInfo](../roamingSgsn/)                 | O   |          |
| defaultForwNumber                                     | Номер переадресации по умолчанию.                                                                                                                                                                                  | string                                             | O   |          |
| defaultForwStatus                                     | Код состояния для переадресации.                                                                                                                                                                                   | int                                                | O   |          |
| whiteLists                                            | Перечень идентификаторов белых списков, [WhiteList](../whiteList/).                                                                                                                                                | [int]                                              | O   |          |
| blackLists                                            | Перечень идентификаторов черных списков, [BlackList](../blackList/).                                                                                                                                               | [int]                                              | O   |          |
| lcsId                                                 | Идентификатор профиля LCS.                                                                                                                                                                                         | int                                                | O   |          |
| tkId                                                  | Идентификатор транспортного ключа.                                                                                                                                                                                 | int                                                | O   |          |
| groupId                                               | Идентификатор абонентской группы.                                                                                                                                                                                  | int                                                | O   |          |
| deactivatePsi                                         | Флаг отказа от отправки сообщения MAP-PSI при получении запроса MAP-SRI.pass:q[\<br\>]По умолчанию: 0.                                                                                                                       | bool                                               | O   |          |
| baocWithoutCamel                                      | Флаг разрешения регистрации на VLR без поддержки CAMEL при наличии запрета роуминга.                                                                                                                               | bool                                               | O   |          |
| ipSmGwNumber                                          | Номер узла IP–SM–GW.                                                                                                                                                                                               | string                                             | O   |          |
| ipSmGwHost                                            | Хост узла IP–SM–GW.                                                                                                                                                                                                | string                                             | O   |          |
| ipSmGwRealm                                           | Realm узла IP–SM–GW.                                                                                                                                                                                               | string                                             | O   |          |
| smsRegistrationInfo                                   | Перечень регистраций на узле IP–SM–GW.                                                                                                                                                                             | [[SmsRegistrationInfo](../smsRegistrationInfo/)]   | O   |          |
| networkAccessMode                                     | Код режима работы.pass:q[\<br\>]`0` -- MSC + SGSN; `1` -- только MSC;`2` -- только SGSN.                                                                                                                                     | int                                                | O   |          |
| qosGprsId                                             | Идентификатор профиля QoS GPRS.                                                                                                                                                                                    | int                                                | O   |          |
| qosEpsId                                              | Идентификатор профиля QoS EPS.                                                                                                                                                                                     | int                                                | O   |          |
| accessRestrictionData                                 | Ограничения доступа.                                                                                                                                                                                               | [AccessRestrictionData](../accessRestrictionData/) | O   |          |
| chargingCharacteristics                               | Значение `ChargingCharacteristics`.                                                                                                                                                                                | string                                             | O   |          |
| imsSubscription                                       | Подписка IMS.                                                                                                                                                                                                      | [ImsSubscription](../imsSubscription/)             | O   |          |
| imsUserData                                           | Параметры данных пользователя IMS.                                                                                                                                                                                 | [ImsUserData](../imsUserData/)                     | O   |          |
| roaming-scscf-info                                    | Перечень параметров роуминговых узлов S-CSCF.                                                                                                                                                                      | [[RoamingScscfInfo](../roamingScscfInfo/)]         | O   |          |
| aMsisdn                                               | Дополнительные номера MSISDN.                                                                                                                                                                                      | [string]                                           | O   |          |
| periodicLauTimer                                      | Период между отправками запросов Location–Area–Update, в секундах.                                                                                                                                                 | int                                                | O   |          |
| periodicRauTauTimer                                   | Период между отправками запросов Routing–Area–Update и Tracking–Area–Update, в секундах.                                                                                                                           | int                                                | O   |          |
| HSM_ID                                                | Идентификатор модуля HSM.                                                                                                                                                                                          | int                                                | O   |          |
| opc                                                   | Код сигнальной точки отправления.                                                                                                                                                                                  | string                                             | O   |          |
| ueUsageType                                           | Значение `UE-Usage-Type` для выбора выделенной опорной сети. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).pass:q[\<br\>]Диапазон: 0–127. | int                                                | O   |          |
| lastActivity                                          | Дата и время последней активности.                                                                                                                                                                                 | datetime                                           | O   |          |
| SCA                                                   | Адрес сервисного центра.                                                                                                                                                                                           | datetime                                           | O   |          |
| category                                              | Категория вызывающего абонента CgPC. См. [Recommendation&nbsp;ITU-T&nbsp;Q.767](https://www.itu.int/rec/T-REC-Q.767-199102-I/en).                                                                                           | int                                                | O   |          |
| RSD                                                   | Перечень дескрипторов выбора маршрута. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.212](https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf).                                           | [string]                                           | O   |          |
| icsIndicator                                          | Индикатор централизованных служб, Centralized Services, сетей IMS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).               | bool                                               | O   |          |
| csToPsSrvccAllowedIndicator                           | Флаг разрешения передачи голосовой части сессии из домена CS в домен PS.                                                                                                                                           | bool                                               | O   | 2.0.35.0 |
| singleNssais                                          | Параметры подписок Single NSSAI.                                                                                                                                                                                   | [SubSnssai](../subSnssai/)                         | O   | 2.1.14.0 |
| externalId                                            | Идентификатор для внешних устройств IoT.                                                                                                                                                                           | string                                             | O   | 2.1.7.0  |
| comment                                               | Комментарий к профилю абонента.                                                                                                                                                                                    | string                                             | O   | 2.1.9.0  |
| externalGroupIds                                      | Перечень групповых идентификаторов устройств IoT.                                                                                                                                                                  | [string]                                           | O   | 2.1.10.0 |
| niddAuthorizations                                    | Перечень авторизаций NIDD.                                                                                                                                                                                         | [[NiddAuthorization](../niddAuthorization/)]       | O   | 2.1.10.0 |
| imei                                                  | Номер IMEI устройства.                                                                                                                                                                                             | string                                             | O   | 2.1.10.0 |

### [[administrative-state-profile]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

### Пример ###

```json
{
  "status": "OK",
  "imsi": "250510100168217",
  "msisdn": "79585179068217",
  "administrative_state": 2,
  "forbid_reg": 0,
  "roaming_not_allowed": 0,
  "odb-param": {
    "SubscriberStatus": 0,
    "general-list": 0,
    "hplmn-list": 0,
    "hl-general-list": [],
    "hl-hplmn-list": []
  },
  "pdp-data": [
    {
      "context-id": 1,
      "type": "f121",
      "vplmnAddressAllowed": 0,
      "apn": "iot2"
    },
    {
      "context-id": 4,
      "type": "f121",
      "vplmnAddressAllowed": 0,
      "apn": "iot"
    }
  ],
  "eps-context-data": [
    {
      "eps_context_id": 1,
      "service_selection": "iot2",
      "vplmnDynamicAddressAllowed": 0,
      "pdnGwType": 1,
      "pdnType": 0,
      "specificApns": []
    },
    {
      "eps_context_id": 4,
      "service_selection": "iot",
      "vplmnDynamicAddressAllowed": 0,
      "pdnGwType": 1,
      "pdnType": 0,
      "specificApns": []
    }
  ],
  "eps-data": [
    {
      "ueMaxDl": 100000000,
      "ueMaxUl": 100000000,
      "ratType": 1004,
      "defContextId": 1
    }
  ],
  "csi-list": [
    {
      "type-id": 4,
      "profile-id": 1
    }
  ],
  "mCsi-list": [
    {
      "id": 1,
      "serviceKey": 1,
      "gsmScfAddress": "79585340009",
      "notificationToCse": 1,
      "csiActive": 1,
      "csiMts": [
        {
          "id": 6,
          "csiId": 1,
          "mmCode": 0
        },
        {
          "id": 9,
          "csiId": 1,
          "mmCode": 1
        },
        {
          "id": 12,
          "csiId": 1,
          "mmCode": 2
        }
      ]
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 16 ]
    }
  ],
  "ssForw": [],
  "ssBarring": [
    {
      "ss_Code": 146,
      "ss_Status": 1,
      "tele_service": 16
    }
  ],
  "lcsPrivacy": [],
  "lcsMolr": [],
  "teleserviceList": [ 17, 33, 34 ],
  "bearerserviceList": [],
  "roaming-info": {
    "mscAreaRestricted": -1,
    "supportedVlrCamel": 3,
    "supportedVlrLcs": 15,
    "ueLcsNotSupported": 0,
    "version": 0,
    "dtregistered": 1620118895000,
    "ispurged": 0
  },
  "roaming-sgsn-info": {
    "dtregistered": 1620118895000
  },
  "defaultForwStatus": 0,
  "whiteLists": [
    {
      "id": 22,
      "name": "MegaFon"
    }
  ],
  "blackLists": [],
  "tkId": 1,
  "groupId": 622,
  "deactivatePsi": 0,
  "networkAccessMode": 0,
  "qosGprsId": 1,
  "qosEpsId": 1,
  "accessRestrictionData": {},
  "category": 10
}
```