== Параметры запроса LCS_MOLR LcsMolr

[width="100%",cols="8%,87%,3%,2%",options="header",]
|===
|Поле |Описание |Тип |O/M
|ssCode |Код дополнительной услуги SS. См. https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf[3GPP TS 29.002]. |int |М
|ssStatus |Статус дополнительной услуги SS. См. https://www.etsi.org/deliver/etsi_ts/123000_123099/123011/18.00.00_60/ts_123011v180000p.pdf[3GPP TS 23.011]. |int |M
|delete |Флаг удаления значения. |bool |O
|===

=== Пример

[source,json]
----
{
  "ssCode": 15,
  "ssStatus": 2
}
----
