== Параметры профиля GPRS-CSI GprsCsi

[width="100%",cols="14%,65%,15%,2%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|id |Идентификатор профиля GPRS-CSI.pass:q[<br>]*Примечание.* При создании профиля GPRS-CSI его идентификатор можно не указывать, если указано имя. |int |С |
|name |Имя профиля GPRS-CSI. |string |С |2.0.55.0
|camelCapabilityHandling |Поддерживаемые фазы CAMEL. |int |О |
|notificationToCse |Флаг оповещения о внесенных изменениях профиля. |bool |О |
|csiActive |Идентификатор активного профиля CSI. |int |O |
|csiTdps |Перечень триггерных точек GPRS-CSI. |[link:../gprsCsiTdp/[GprsCsiTdp]] |O |
|===

=== Пример

[source,json]
----
{
  "camelCapabilityHandling": 3,
  "csiActive": 1,
  "csiTdps": [
    {
      "defaultHandling": 2,
      "gsmScfAddress": "78924813183138",
      "serviceKey": 1,
      "tdpId": 1
    }
  ],
  "id": 1,
  "name": "csi1"
}
----
