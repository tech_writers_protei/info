
## Параметры подписки IMS ImsSubscription

| Поле                    | Описание                                                    | Тип                                                      | O/M |
|-------------------------|-------------------------------------------------------------|----------------------------------------------------------|-----|
| name                    | Номер подписки IMS.                                         | string                                                   | M   |
| capabilitySetId         | Идентификатор набора Capability.                            | int                                                      | O   |
| prefferedScscfSetId     | Идентификатор набора предпочитаемых узлов S-CSCF.           | int                                                      | O   |
| chargingInformationName | Имя профиля Charging Information.                           | string                                                   | O   |
| sccAsName               | Имя узла SCC-AS.                                            | string                                                   | O   |
| impis                   | Перечень приватных идентификаторов пользователя в сети IMS. | [[IMPI](../impi/)]                                       | O   |
| serviceProfiles         | Перечень профилей услуги.                                   | [[ServiceProfile](../serviceProfile/)]                   | O   |
| implicitlyRegisteredSet | Перечень наборов неявно зарегистрированных IMPU.            | [[ImplicitlyRegisteredSet](../implicitlyRegisteredSet/)] | O   |


### Пример ###

```json
{
  "name": "250010000001",
  "capabilitySetId": 1,
  "prefferedScscfSetId": 1,
  "chargingInformationName": "ci",
  "serviceProfiles": [
    {
      "Name": "sp",
      "CoreNetworkServiceAuthorization": 1,
      "Ifcs": [
        {
          "Name": "ifc1",
          "Priority": 1,
          "ApplicationServerName": "as",
          "ProfilePartIndicator": 1,
          "TriggerPoint": {
            "ConditionTypeCNF": 1,
            "Spt": [
              {
                "Group": 1,
                "Method": "INVITE",
                "SessionCase": 1,
                "ConditionNegated": 2,
                "Type": 3,
                "RequestUri": "http://ims.protei.ru/spt1",
                "Header": "header",
                "Content": "headerContent",
                "SdpLine": "sdpLine",
                "SdpLineContent": "sdpLineContent",
                "RegistrationType": 1
              }  
            ]
          }
        }
      ]
    }
  ],
  "implicitlyRegisteredSet": [
    {
      "name": "implSet1",
      "impus": [
        {
          "Identity": "sip:protei@ims.protei.ru",
          "BarringIndication": 0,
          "Type": 0,
          "CanRegister": 1,
          "ServiceProfileName": "sp",
          "Default":true
        },
        {
          "Identity": "sip:ntc_protei@ims.protei.ru",
          "BarringIndication": 0,
          "Type": 0,
          "CanRegister": 1,
          "ServiceProfileName": "sp"
        }
      ]
    }
  ]
}
```