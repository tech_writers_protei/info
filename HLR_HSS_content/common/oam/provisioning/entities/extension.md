
## Параметры расширения идентификатора для определения подписки абонента Extension

| Поле                                    | Описание                                                                                                       | Тип    | O/M |
|-----------------------------------------|----------------------------------------------------------------------------------------------------------------|--------|-----|
| [[e164-extension]]E164       | Номер в формате E.164. См. [Recommendation&nbsp;ITU-T&nbsp;E.164](https://www.itu.int/rec/T-REC-E.164-201011-I/en).      | string | С   |
| [[imsi-extension]]IMSI       | Номер IMSI в формате E.212. См. [Recommendation&nbsp;ITU-T&nbsp;E.212](https://www.itu.int/rec/T-REC-E.212-201609-I/en). | string | С   |
| [[sip-uri-extension]]SIP-URI | Идентификатор в формате SIP-URI.                                                                               | string | С   |
| [[nai-extension]]NAI         | Идентификатор в формате NAI.                                                                                   | string | С   |
| [[private-extension]]Private | Приватный идентификатор сервера Credit-Control.                                                                | string | С   |

**Примечание.** Задается только один из параметров [E164](#e164-extension), [IMSI](#imsi-extension), [SIP-URI](#sip-uri-extension), [NAI](#nai-extension), [Private](#private-extension).

### Пример ###

```json
{
  "IMSI": "001010000000306"
}
```