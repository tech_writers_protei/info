
## Параметры местоположения абонента LocationInformation

| Поле                   | Описание                          | Тип                                                    | O/M |
|------------------------|-----------------------------------|--------------------------------------------------------|-----|
| epsUserState           | Статус абонента в сети EPS.       | [epsUserState](../epsUserState/)                       | O   |
| epsLocationInformation | Местоположение в сети EPS.        | [epsLocationInformation](../epsLocationInformation/)   | O   |
| psSubscriberState      | Код статуса абонента в домене PS. | int                                                    | O   |
| csSubscriberState      | Код статуса абонента в домене CS. | int                                                    | O   |
| csLocationInformation  | Местоположение в домене CS.       | [CsPsLocationInformation](../csPsLocationInformation/) | O   |
| psLocationInformation  | Местоположение в домене PS.       | [CsPsLocationInformation](../csPsLocationInformation/) | O   |

### Пример ###

```json
{
  "epsLocationInformation": {
    "mmeLocationInformation": {
      "ageOfLocationInformation": 22,
      "currentLocationRetrieved": 1,
      "eUtranCellGlobalIdentity": "aaaa",
      "geodeticInformation": "cccc",
      "geographicalInformation": "bbbb",
      "trackingAreaIdentity": "1a2f",
      "userCsgInformation": {
        "csgAccessMode": 1,
        "csgId": 1,
        "csgMembershipIndication": 1
      }
    }
  },
  "epsUserState": {
    "mmeUserState": 1
  }
}
```