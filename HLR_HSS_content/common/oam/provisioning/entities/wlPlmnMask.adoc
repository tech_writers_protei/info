== Параметры маски PLMN для белого списка WlPlmnMask

[width="100%",cols="9%,83%,5%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|mask |Маска номера в формате E.212. См. https://www.itu.int/rec/T-REC-E.212-201609-I/en[Recommendation ITU-T E.212]. |regex |M
|country |Название страны. |string |M
|network |Имя сети. |string |M
|action |Код действия со списком.pass:q[<br>]`0` -- create, создать, или modify, изменить;pass:q[<br>]`1` -- delete, удалить. |int |O
|===

=== Пример

[source,json]
----
{
  "mask": "25001",
  "country": "rus",
  "network": "supertelecom"
}
----
