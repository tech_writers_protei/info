
## Параметры критериев профиля SS-CSI SsCsiCriteria

| Поле                               | Описание                                                                                                                                          | Тип  | O/M |
|------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|------|-----|
| [ssCode](#ss-code-ss-csi-criteria) | Код дополнительной услуги, SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf). | int  | М   |
| delete                             | Флаг удаления значения параметра.                                                                                                                 | bool | О   |

### Пример ###

```json
{
    "ssCode": 16
}
```

#### [[ss-code-ss-csi-criteria]]Коды дополнительных услуг

```text
SS-Code ::= OCTET STRING (SIZE (1))
-- This type is used to represent the code identifying a single
-- supplementary service, a group of supplementary services, or
-- all supplementary services. The services and abbreviations
-- used are defined in GSM 02.04. The internal structure is 
-- defined as follows:

-------------


-- bits 87654321: group (bits 8765), and specific service
-- (bits 4321)

allSS	SS-Code ::= '00000000'B-'0'Dec
-- reserved for possible future use
-- all SS

allLineIdentificationSS	SS-Code ::= '00010000'B-'16'Dec
-- reserved for possible future use
-- all line identification SS
clip	SS-Code ::= '00010001'B-'17'Dec
-- calling line identification presentation
clir	SS-Code ::= '00010010'B-'18'Dec
-- calling line identification restriction
colp	SS-Code ::= '00010011'B-'19'Dec
-- connected line identification presentation
colr	SS-Code ::= '00010100'B-'20'Dec
-- connected line identification restriction
mci	SS-Code ::= '00010101'B-'21'Dec
-- reserved for possible future use
-- malicious call identification
allNameIdentificationSS	SS-Code ::= '00011000'B-'24'Dec
-- all name identification SS
cnap	SS-Code ::= '00011001'B-'27'Dec
-- calling name presentation

-- SS-Codes '00011010'B to '00011111'B are reserved for future 
-- NameIdentification Supplementary Service use.

allForwardingSS	SS-Code ::= '00100000'B-'32'Dec
-- all forwarding SS
cfu	SS-Code ::= '00100001'B-'33'Dec

-- call forwarding unconditional
allCondForwardingSS	SS-Code ::= '00101000'B-'40'Dec
-- all conditional forwarding SS
cfb	SS-Code ::= '00101001'B-'41'Dec
-- call forwarding on mobile subscriber busy
cfnry	SS-Code ::= '00101010'B-'42'Dec
-- call forwarding on no reply
cfnrc	SS-Code ::= '00101011'B-'43'Dec
-- call forwarding on mobile subscriber not reachable
cd	SS-Code ::= '00100100'B-'36'Dec
-- call deflection

allCallOfferingSS	SS-Code ::= '00110000'B-'48'Dec
-- reserved for possible future use
-- all call offering SS includes also all forwarding SS
ect	SS-Code ::= '00110001'B-'49'Dec
-- explicit call transfer
mah	SS-Code ::= '00110010'B-'50'Dec
-- reserved for possible future use
-- mobile access hunting
```