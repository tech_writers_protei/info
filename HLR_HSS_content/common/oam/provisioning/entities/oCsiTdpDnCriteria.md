
## Параметры критерия номера назначения триггерной точки обнаружения O-CSI OCsiTdpDnCriteria

| Поле                 | Описание                                                                       | Тип                                                | O/M |
|----------------------|--------------------------------------------------------------------------------|----------------------------------------------------|-----|
| matchType            | Тип соответствия номера назначения.pass:q[\<br\>]`0` -- запрещающий; `1` -- разрешающий. | int                                                | М   |
| csiTdpDnCriteriaDns  | Перечень критериев номера назначения.                                          | [[OCsiTdpDnCriteriaDn](../oCsiTdpDnCriteriaDn/)]   | O   |
| csiTdpDnCriteriaDnls | Перечень критериев длины номера назначения.                                    | [[OCsiTdpDnCriteriaDnl](../oCsiTdpDnCriteriaDnl/)] | О   |
| delete               | Флаг удаления значения.                                                        | bool                                               | О   |

### Пример ###

```json
{
  "csiTdpDnCriteriaDnls": [
    { "length": 9 }
  ],
  "csiTdpDnCriteriaDns": [
    { "destNumber": "123456789" }
  ],
  "matchType": 1
}
```