
## Параметры набора Capabilities CapabilitiesSet

| Поле         | Описание                          | Тип                                     | O/M |
|--------------|-----------------------------------|-----------------------------------------|-----|
| SetId        | Идентификатор набора.             | int                                     | M   |
| Name         | Название набора.                  | string                                  | O   |
| Capabilities | Перечень параметров Capabilities. | [[Capability](/capability/)] | M   |

### Пример ###

```json
{
  "Capabilities": [
    {
      "Code": 1,
      "Mandatory": 1
    },
    {
      "Code": 2,
      "Mandatory": 0
    }
  ],
  "Name": "set1",
  "SetId": 1
}
```