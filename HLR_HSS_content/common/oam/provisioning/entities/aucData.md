
## Параметры профиля аутентификации AucData

| Поле             | Описание                                                                                                                                                                           | Тип    | O/M | Версия    |
|------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|-----------|
| opId             | Идентификатор ключа оператора для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).           | int    | O   |           |
| tkId             | Идентификатор транспортного ключа для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).       | int    | O   |           |
| opcTkId          | Идентификатор транспортного ключа OPC. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).                          | int    | O   | 2.0.35.0  |
| cId              | Идентификатор аддитивной постоянной для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).     | int    | O   |           |
| rId              | Идентификатор постоянной поворота для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).       | int    | O   |           |
| amfUmts          | Поле управления аутентификацией в сетях UMTS.                                                                                                                                      | string | O   |           |
| amfLte           | Поле управления аутентификацией в сетях LTE.                                                                                                                                       | string | O   |           |
| amfIms           | Поле управления аутентификацией в сетях IMS.                                                                                                                                       | string | O   |           |
| resSize          | Длина ответа пользователя для алгоритма MILENAGE, в байтах. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).     | int    | O   | 2.0.33.10 |
| ckSize           | Длина ключа конфиденциальности для алгоритма MILENAGE, в битах. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf). | int    | O   | 2.0.33.10 |
| ikSize           | Длина ключа целостности для алгоритма MILENAGE, в битах. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).        | int    | O   | 2.0.33.10 |
| macSize          | Код сетевой аутентификации для алгоритма MILENAGE, в битах. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).     | int    | O   | 2.0.33.10 |
| keccakIterations | Количество раундов алгоритма хэширования SHA-3 (Keccak). См. [NIST FIPS PUB 202](https://nvlpubs.nist.gov/nistpubs/fips/nist.fips.202.pdf).                                        | int    | O   | 2.0.33.10 |
| sqnType          | Код способа выбора последовательности SQN.pass:q[\<br\>]`0` -- не зависимый от времени; `1` -- основанный на времени.                                                                        | int    | O   | 2.0.37.0  |

### Пример ###

```json
{
  "opId": 1,
  "tkId": 1,
  "cId": 1,
  "rId": 1,
  "amfUmts": "0000",
  "amfLte": "8000",
  "amfIms": "8001"
}
```