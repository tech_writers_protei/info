
## Параметры местоположения абонента по EPS-домену EpsLocationInformation

| Поле                    | Описание                              | Тип                                                    | O/M |
|-------------------------|---------------------------------------|--------------------------------------------------------|-----|
| mmeLocationInformation  | Местоположение абонента на узле MME.  | [MmeLocationInformation](../mmeLocationInformation/)   | O   |
| sgsnLocationInformation | Местоположение абонента на узле SGSN. | [SgsnLocationInformation](../sgsnLocationInformation/) | O   |

### Пример ###

```json
{
  "mmeLocationInformation": {
    "ageOfLocationInformation": 1,
    "eUtranCellGlobalIdentity": "00f1102b2d1010",
    "trackingAreaIdentity": "00f1100001"
  }
}
```