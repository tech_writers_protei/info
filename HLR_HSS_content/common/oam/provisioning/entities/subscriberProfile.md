
## Параметры профиля абонента при добавлении SubscriberProfile

| Поле                                                                             | Описание                                                                                                                                                                                            | Тип                                                | O/M | Версия   |
|----------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------|-----|----------|
| imsi                                                                             | Номер IMSI абонента.                                                                                                                                                                                | string                                             | M   | 2.0.39.0 |
| msisdn                                                                           | Номер MSISDN абонента.                                                                                                                                                                              | string                                             | M   | 2.0.39.0 |
| [status](#subscriber-status-code)                                                | Код состояния абонента.                                                                                                                                                                             | int                                                | M   | 2.0.39.0 |
| category                                                                         | Категория вызывающего абонента, `CgPC`. См. [ITU-T Recommendation Q.767](https://www.itu.int/rec/T-REC-Q.767-199102-I/en).pass:q[\<br\>]По умолчанию: 10.                                                     | int                                                | O   | 2.0.39.0 |
| networkAccessMode                                                                | Код режима работы.pass:q[\<br\>]`0` -- MSC + SGSN;pass:q[\<br\>]`1`-- только MSC;pass:q[\<br\>]`2` -- только SGSN.                                                                                                                | int                                                | O   | 2.0.39.0 |
| defaultForwNumber                                                                | Номер переадресации по умолчанию.                                                                                                                                                                   | string                                             | O   | 2.0.60.1 |
| [defaultForwStatus](#subscriber-status-code)                                     | Код состояния для переадресации.                                                                                                                                                                    | int                                                | O   | 2.0.60.1 |
| groupId                                                                          | Идентификатор абонентской группы.                                                                                                                                                                   | int                                                | O   | 2.0.39.0 |
| ipSmGwNumber                                                                     | Номер узла IP-SM-GW.                                                                                                                                                                                | string                                             | O   | 2.0.39.0 |
| ipSmGwHost                                                                       | Хост узла IP-SM-GW.                                                                                                                                                                                 | string                                             | O   | 2.0.39.0 |
| ipSmGwRealm                                                                      | Realm узла IP-SM-GW.                                                                                                                                                                                | string                                             | O   | 2.0.39.0 |
| deactivatePsi                                                                    | Флаг отказа от отправки сообщения MAP-PSI при получении запроса MAP-SRI.pass:q[\<br\>]По умолчанию: 0.                                                                                                        | bool                                               | O   | 2.0.39.0 |
| baocWithoutCamel                                                                 | Флаг разрешения регистрации на VLR без поддержки CAMEL при наличии запрета роуминга.                                                                                                                | bool                                               | O   | 2.0.39.0 |
| [[forbid-registration-without-camel]]ForbidRegistration_WithoutCamel  | Флаг запрета регистрации на VLR/SGSN без поддержки фаз CAMEL.                                                                                                                                       | bool                                               | O   | 2.0.39.0 |
| qosGprsId                                                                        | Идентификатор профиля QoS для сетей GPRS.                                                                                                                                                           | int                                                | O   | 2.0.39.0 |
| qosEpsId                                                                         | Идентификатор профиля QoS для сетей EPS.                                                                                                                                                            | int                                                | O   | 2.0.39.0 |
| roamingNotAllowed                                                                | Флаг запрета роуминга.pass:q[\<br\>]По умолчанию: 0.                                                                                                                                                          | bool                                               | O   | 2.0.39.0 |
| accessRestrictionData                                                            | Ограничения доступа.                                                                                                                                                                                | [AccessRestrictionData](../accessRestrictionData/) | O   | 2.0.39.0 |
| periodicLauTimer                                                                 | Период между отправками запросов Location-Area-Update, в секундах.                                                                                                                                  | int                                                | O   | 2.0.39.0 |
| periodicRauTauTimer                                                              | Период между отправками запросов Routing-Area-Update и Tracking-Area-Update, в секундах.                                                                                                            | int                                                | O   | 2.0.39.0 |
| SCA                                                                              | Адрес сервисного центра.                                                                                                                                                                            | string                                             | O   | 2.0.39.0 |
| ueUsageType                                                                      | Значение `UE-Usage-Type` для выбора выделенной опорной сети. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).pass:q[\<br\>]Диапазон: 0-127. | int                                                | O   | 2.0.39.0 |
| imrn                                                                             | Номер маршрутизации в сетях IMS.                                                                                                                                                                    | string                                             | O   | 2.0.39.0 |
| voLte                                                                            | Флаг использования VoLTE.                                                                                                                                                                           | bool                                               | O   | 2.0.39.0 |
| icsIndicator                                                                     | Индикатор централизованных служб, Centralized Services, сетей IMS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).               | bool                                               | O   | 2.0.39.0 |
| hlr_profile_id                                                                   | Идентификатор шаблона профиля в файле `profiles.json`.                                                                                                                                              | int                                                | O   | 2.0.39.0 |
| [[algo-subscriber-profile]][algorithm](#algorithm-subscriber-profile) | Код алгоритма аутентификации.                                                                                                                                                                       | int                                                | M   | 2.0.39.0 |
| ki                                                                               | Секретный ключ Ki для аутентификации.pass:q[\<br\>]**Примечание.** Если [algorithm](#algo-subscriber-profile) = TUAK, то длина должна быть 32 или 64 бита, для других алгоритмов -- 32 бита.                  | string                                             | M   | 2.0.39.0 |
| opc                                                                              | Значение поля конфигурации алгоритма, задаваемого оператором. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).                    | string                                             | O   | 2.0.39.0 |
| HSM_ID                                                                           | Идентификатор модуля HSM.                                                                                                                                                                           | int                                                | O   | 2.0.39.0 |
| aucData                                                                          | Параметры аутентификации. См. [aucData](../aucData/).                                                                                                                                               | object                                             | O   | 2.0.39.0 |
| ssData                                                                           | Перечень дополнительных услуг SS.                                                                                                                                                                   | [[ssData](../ssData/)]                             | O   | 2.0.39.0 |
| ssForw                                                                           | Перечень дополнительных услуг SS переадресации.                                                                                                                                                     | [[ssForwarding](../ssForwarding/)]                 | O   | 2.0.44.0 |
| ssBarring                                                                        | Перечень дополнительных услуг SS запрета.                                                                                                                                                           | [[ssBarring](../ssBarring)]                        | O   | 2.0.44.0 |
| ssCugFeat                                                                        | Перечень свойств дополнительных услуг SS CUG.                                                                                                                                                       | [[ssCugFeat](../ssCugFeat/)]                       | O   | 2.0.44.0 |
| ssCugSub                                                                         | Перечень подписок на дополнительную услугу SS CUG.                                                                                                                                                  | [[ssCugSub](../ssCugSub/)]                         | O   | 2.0.44.0 |
| EpsData                                                                          | Параметры EPS.                                                                                                                                                                                      | [epsData](../epsData/)                             | O   | 2.0.44.0 |
| lcsId                                                                            | Идентификатор профиля LCS.                                                                                                                                                                          | int                                                | O   |          |
| pdpData                                                                          | Перечень профилей данных PDP.                                                                                                                                                                       | [[PdpData](../pdpData/)]                           | O   |          |
| link-eps-data                                                                    | Перечень параметров взаимодействия с контекстом EPS.                                                                                                                                                | [[LinkEpsData](../linkEpsData/)]                   | O   |          |
| csi-list                                                                         | Перечень профилей CSI Link.                                                                                                                                                                         | [[CsiLink](../csiLink/)]                           | O   |          |
| TeleServices                                                                     | Перечень идентификаторов телеуслуг. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                                              | [int]                                              | O   | 2.0.39.0 |
| BearerServices                                                                   | Перечень идентификаторов служб передачи данных. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf).                                  | [int]                                              | O   | 2.0.39.0 |
| WL_DATA                                                                          | Перечень идентификаторов белых списков.                                                                                                                                                             | [int]                                              | O   | 2.0.44.0 |
| BL_DATA                                                                          | Перечень идентификаторов черных списков.                                                                                                                                                            | [int]                                              | O   | 2.0.44.0 |
| WL_NAMES                                                                         | Перечень названий белых списков.                                                                                                                                                                    | [string]                                           | O   | 2.0.44.0 |
| BL_NAMES                                                                         | Перечень названий черных списков.                                                                                                                                                                   | [string]                                           | O   | 2.0.44.0 |
| ODB                                                                              | Запреты, установленные оператором.                                                                                                                                                                  | [Odb](../odb/)                                     | O   | 2.0.44.0 |
| chargingCharacteristics                                                          | Значение `ChargingCharacteristics`.                                                                                                                                                                 | string                                             | O   | 2.0.44.0 |
| aMsisdn                                                                          | Перечень дополнительных номеров MSISDN.                                                                                                                                                             | [string]                                           | O   | 2.0.44.0 |
| imsProfile                                                                       | Профиль IMS.                                                                                                                                                                                        | [ImsProfile](../imsProfile/)                       | O   | 2.0.44.0 |
| imsSubscription                                                                  | Подписка IMS.                                                                                                                                                                                       | [ImsSubscription](../imsSubscription/)             | O   | 2.0.44.0 |
| regionalZoneCodes                                                                | Перечень кодов региональной зоны.                                                                                                                                                                   | [[Regional](../regional/)]                         | O   | 2.0.44.0 |
| csToPsSrvccIndicator                                                             | Флаг передачи голосовой части из домена CS в домен PS.                                                                                                                                              | bool                                               | O   | 2.0.39.0 |
| activeTime                                                                       | Время жизни активной сессии, в секундах.                                                                                                                                                            | int                                                | O   | 2.0.39.0 |
| edrxCycleLength                                                                  | Перечень параметров циклов расширенного режима прерывистого приема. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/17.03.00_60/ts_123682v170300p.pdf).              | [[EdrxCycleLength](../edrxCycleLength/)]           | O   | 2.0.44.0 |
| singleNssais                                                                     | Перечень параметров `S-NSSAI`.                                                                                                                                                                      | [[subSnssai](../subSnssai/)]                       | O   | 2.1.14.0 |
| externalId                                                                       | Идентификатор для внешних устройств IoT.                                                                                                                                                            | string                                             | O   | 2.1.7.0  |
| comment                                                                          | Комментарий к профилю абонента.                                                                                                                                                                     | string                                             | O   | 2.1.9.0  |
| externalGroupIds                                                                 | Перечень групповых идентификаторов устройств IoT.                                                                                                                                                   | [string]                                           | O   | 2.1.10.0 |

### Пример ###

```json
{
  "imsi": "250010000001",
  "msisdn": "79000000001",
  "status": 2,
  "category": 10,
  "networkAccessMode": 0,
  "DefaultForwardingNumber": "867349752",
  "DefaultForwardingStatus": 1,
  "ipSmGwNumber": "23525252",
  "algorithm": 4,
  "ki": "12345678900987654321123456789009",
  "opc": "09876543211234567890098765432112",
  "activeTime": 1000,
  "edrxCycleLength": [
    {
      "ratType": 1005,
      "lengthValue": 8
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "ssForw": [
    {
      "ss_Code": 42,
      "ss_Status": 7,
      "forwardedToNumber": "42513466754",
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "EpsData": {
    "defContextId": 1,
    "ueMaxDl": 10000,
    "ueMaxUl": 10000
  },
  "link-eps-data": [
    {
      "context-id": 2,
      "ipv4": "192.168.1.22",
      "plmnId": "25001"
    }
  ],
  "pdpData": [
    {
      "context-id": 1,
      "type": "0080"
    }
  ],
  "ODB": {
    "generalSetList": [ 1, 6, 19 ],
    "SubscriberStatus": 1
  },
  "imsProfile": {
    "impi": "250010000001@ims.protei.ru",
    "authScheme": 5,
    "sipDigest": {
      "password": "elephant"
    },
    "impus": [
      {
        "Identity": "sip:79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 0,
        "CanRegister": 1,
        "ServiceProfileName": "sp"
      },
      {
        "Identity": "tel:+79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 1,
        "CanRegister": 1,
        "WildcardPsi": "tel:+79000000001!.*!",
        "PsiActivation": 1,
        "ServiceProfileName": "sp"
      }
    ],
    "implicitlySets": [
      { "name": "implSet1" }
    ]
  },
  "imsSubscription": {
    "name": "250010000001",
    "capabilitySetId": 1,
    "prefferedScscfSetId": 1,
    "chargingInformationName": "ci",
    "serviceProfiles": [
      {
        "Name": "sp",
        "CoreNetworkServiceAuthorization": 1,
        "Ifcs": [
          {
            "Name": "ifc1",
            "Priority": 1,
            "ApplicationServerName": "as",
            "ProfilePartIndicator": 1,
            "TriggerPoint": {
              "ConditionTypeCNF": 1,
              "Spt": [
                {
                  "Group": 1,
                  "Method": "INVITE",
                  "SessionCase": 1,
                  "ConditionNegated": 2,
                  "Type": 3,
                  "RequestUri": "http://ims.protei.ru/spt1",
                  "Header": "header",
                  "Content": "headerContent",
                  "SdpLine": "sdpLine",
                  "SdpLineContent": "sdpLineContent",
                  "RegistrationType": 1
                }
              ]
            }
          }
        ]
      }
    ],
    "implicitlyRegisteredSet": [
      {
        "name": "implSet1",
        "impus": [
          {
            "Identity": "sip:protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp",
            "Default": true
          },
          {
            "Identity": "sip:ntc_protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp"
          }
        ]
      }
    ]
  },
  "singleNssais": [
    {
      "snssaiId": 1,
      "type": 0,
      "plmnId": "00101"
    },
    {
      "snssaiId": 2,
      "type": 0,
      "plmnId": "00102"
    }
  ]
}
```

#### [[subscriber-status-code]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

#### [[algorithm-subscriber-profile]]Коды алгоритмов аутентификации

| N | Поле       | Описание                                                                                                                   |
|---|------------|----------------------------------------------------------------------------------------------------------------------------|
| 1 | COMP128 v1 |                                                                                                                            |
| 2 | COMP128 v2 |                                                                                                                            |
| 3 | COMP128 v3 |                                                                                                                            |
| 4 | MILENAGE   | Cм. [3GPP TS 35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf)          |
| 6 | XOR        | Cм. [3GPP TS 34.108, 8.1.2.1](https://www.etsi.org/deliver/etsi_ts/134100_134199/134108/15.02.00_60/ts_134108v150200p.pdf) |
| 7 | TUAK       | Cм. [3GPP TS 35.231](https://www.etsi.org/deliver/etsi_ts/135200_135299/135231/18.00.00_60/ts_135231v180000p.pdf)          |
| 8 | S3G128     | Cм. [ГОСТ 34.10–2018](https://protect.gost.ru/v.aspx?control=8&id=224247)                                                  |
| 9 | S3G256     | Cм. [ГОСТ 34.10–2018](https://protect.gost.ru/v.aspx?control=8&id=224247)                                                  |