
## Параметры регистрации на узле IP-SM-GW SMSRegistrationInfo

| Поле         | Описание             | Тип    | O/M |
|--------------|----------------------|--------|-----|
| ipSmGwNumber | Номер узла IP-SM-GW. | string | O   |
| ipSmGwHost   | Хост узла IP-SM-GW.  | string | O   |
| ipSmGwRealm  | Realm узла IP-SM-GW. | string | O   |

### Пример ###

```json
{
  "ipSmGwNumber": "12345789",
  "ipSmGwHost": "ipsmgw.ims.protei.ru",
  "ipSmGwRealm": "ims.protei.ru"
}
```