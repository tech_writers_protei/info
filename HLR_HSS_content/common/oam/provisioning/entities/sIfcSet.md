
## Параметры SIfcSet

| Поле   | Описание                        | Тип              | O/M |
|--------|---------------------------------|------------------|-----|
| Id     | Идентификатор набора общих iFC. | int              | M   |
| Ifcs   | Перечень iFC.                   | [[Ifc](../ifc/)] | M   |
| Delete | Флаг удаления записи.           | bool             | O   |

### Пример ###

```json
{
  "Id": 1,
  "Ifcs": [
    {
      "ApplicationServerName": "as",
      "Name": "set1_ifc1",
      "Priority": 1,
      "ProfilePartIndicator": 1,
      "TriggerPoint": {
        "ConditionTypeCNF": 1,
        "Spt": [
          {
            "ConditionNegated": 2,
            "Content": "headerContent",
            "Group": 1,
            "Header": "header",
            "Method": "INVITE",
            "RegistrationType": 1,
            "RequestUri": "http://ims.protei.ru/spt1",
            "SdpLine": "sdpLine",
            "SdpLineContent": "sdpLineContent",
            "SessionCase": 1,
            "Type": 3
          }
        ]
      }
    }
  ]
}
```