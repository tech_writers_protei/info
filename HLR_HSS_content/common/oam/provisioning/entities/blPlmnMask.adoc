== Параметры маски PLMN для черного списка BLPlmnMask

[width="100%",cols="8%,82%,6%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M
|mask |Маска PLMN. |regex |M
|country |Название страны. |string |M
|network |Имя сети. |string |M
|action |Код действия со списком.pass:q[<br>]`0` -- create, создать, или modify, изменить;pass:q[<br>]`1` -- delete, удалить. |int |O
|===

=== Пример

[source,json]
----
{
  "mask": "25001",
  "country": "rus",
  "network": "supertelecom"
}
----
