
## Параметры приватности профиля LCS LcsPrivacy

| Поле                 | Описание                                                                                                                                            | Тип                                    | O/M |
|----------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------|-----|
| ssCode               | Код дополнительной услуги SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).    | int                                    | М   |
| ssStatus             | Статус дополнительной услуги SS. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.011](https://www.etsi.org/deliver/etsi_ts/123000_123099/123011/18.00.00_60/ts_123011v180000p.pdf). | int                                    | M   |
| notificationToMsUser | Флаг оповещения о внесенных изменениях.                                                                                                             | bool                                   | O   |
| ecList               | Перечень параметров приватности для внешних абонентов.                                                                                              | [[LcsPrivacyEc](../lcsPrivacyEc/)]     | O   |
| plmnList             | Перечень параметров приватности сетей PLMN, связанных с профилем LCS.                                                                               | [[LcsPrivacyPlmn](../lcsPrivacyPlmn/)] | O   |
| delete               | Флаг удаления записи.                                                                                                                               | bool                                   | O   |

### Пример ###

```json
{
  "ecList": [
    {
      "externalAddress": "1245135",
      "gmlcRestriction": 0,
      "notificationToMsUser": 1
    }
  ],
  "notificationToMsUser": 1,
  "plmnList": [
    {
      "clientExternalId": 1
    }
  ],
  "ssCode": 16,
  "ssStatus": 5
}
```