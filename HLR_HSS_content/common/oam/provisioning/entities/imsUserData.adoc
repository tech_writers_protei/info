== Параметры пользователя IMS ImsUserData

[width="100%",cols="23%,53%,22%,2%",options="header",]
|===
|Поле |Описание |Тип |O/M
|PrivateID |Приватный идентификатор пользователя в сети IMS. |string |M
|ServiceProfiles |Перечень профилей услуги. |[link:../serviceProfile/[ServiceProfile]] |O
|Extension |Расширение идентификатора для определения подписки абонента. Формат:pass:q[<br>]`{ <IdName>: <Value> }` |\{string:string} |O
|*\{* | | |
|  link:#extension[<IdName>] |Название идентификатора. |string |M
|  <Value> |Значение. |string |M
|*}* | | |
|===

=== Пример

[source,json]
----
{
  "imsUserData": {
    "PrivateID": "001010000000306@ims.mnc001.mcc001.3gppnetwork.org",
    "ServiceProfiles": [
      {
        "PublicIdentities": [
          {
            "BarringIndication": 0,
            "Identity": "sip:+76000000306@ims.mnc001.mcc001.3gppnetwork.org"
          },
          {
            "BarringIndication": 0,
            "Identity": "tel:+76000000306"
          }
        ],
        "InitialFilterCriterias": [
          {
            "Name": "001010000000306",
            "Priority": 1,
            "TriggerPoint": {
              "ConditionTypeCNF": 0,
              "Spt": [
                {
                  "ConditionNegated": 0,
                  "Group": 1,
                  "Method": "MESSAGE"
                },
                {
                  "ConditionNegated": 1,
                  "Group": 1,
                  "RequestUri": "X-Protei-IPSMGW"
                },
                {
                  "ConditionNegated": 0,
                  "Group": 2,
                  "Method": "REGISTER"
                }
              ]
            },
            "ApplicationServer": {
              "ServerName": "sip:192.168.0.100:13889",
              "DefaultHandling": 0,
              "ServiceInfo": "serviceInfo",
              "Host": "IP_SM_GW",
              "RepositoryDataSizeLimit": 999999999,
              "IncludeRegisterResponse": 1,
              "IncludeRegisterRequest": 1
            }
          }
        ]
      }
    ],
    "Extension": {
      "IMSI": "001010000000306"
    }
  }
}
----

==== [[extension]]Доступные идентификаторы для расширения

* E164 -- номер в формате E.164. См. https://www.itu.int/rec/T-REC-E.164-201011-I/en[Recommendation ITU-T E.164];
* IMSI -- номер IMSI в формате E.212. См. https://www.itu.int/rec/T-REC-E.212-201609-I/en[Recommendation ITU-T E.212];
* SIP-URI -- идентификатор в формате SIP-URI;
* NAI -- идентификатор в формате NAI;
* Private -- приватный идентификатор сервера Credit-Control.
