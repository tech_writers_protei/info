
## Параметры профиля QoS GPRS QosGprs

| Поле              | Описание                                                                                                                                                                                | Тип    | O/M |
|-------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| qosSubscribed     | QoS-Subscribed, октеты 3-5. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                          | string | O   |
| extQosSubscribed  | Ext-QoS-Subscribed, приоритет распределения и хранения, октеты 6-13. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf). | string | O   |
| ext2QosSubscribed | Ext2-QoS-Subscribed, октеты 14-16. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                   | string | O   |
| ext3QosSubscribed | Ext2-QoS-Subscribed, октеты 17-18. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                                   | string | O   |
| ext4QosSubscribed | Ext4-QoS-Subscribed, индикаторы ARP, PL, PCI и PVI. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).                  | string | O   |
| name              | Имя профиля.                                                                                                                                                                            | string | O   |

### Пример ###

```json
{
  "qosSubscribed": "124214",
  "extQosSubscribed": "035F38",
  "ext2QosSubscribed": "035F3834ACEC"
}
```