
## Параметры белого списка WhiteList

| Поле       | Описание                                                                      | Тип                            | O/M |
|------------|-------------------------------------------------------------------------------|--------------------------------|-----|
| action     | Тип действия. `create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                         | M   |
| name       | Название списка.                                                              | string                         | M   |
| vlr_masks  | Перечень масок номера узла VLR.                                               | [[WLVlrMask](../wlVlrMask/)]   | O   |
| plmn_masks | Перечень масок сети PLMN.                                                     | [[WLPlmnMask](../wlPlmnMask/)] | O   |

### Пример ###

```json
{
  "action": "create",
  "name": "whitelist",
  "vlr_masks": [
    {
      "mask": "7689",
      "country": "rus",
      "network": "supertelecom"
    }
  ]
}
```