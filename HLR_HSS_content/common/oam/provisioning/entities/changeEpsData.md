
## Параметры СhangeEpsData

| Поле   | Описание                          | Тип                    | O/M |
|--------|-----------------------------------|------------------------|-----|
| data   | Модифицированные параметры EPS.   | [EpsData](../epsData/) | M   |
| delete | Флаг удаления значения параметра. | bool                   | O   |

### Пример ###

```json
{
  "data": {
    "defContextId": 1,
    "defIpv4": "192.168.1.22",
    "ratFreqPriorId": 6,
    "ratType": 1004,
    "ueApnOiRep": "apn.replace.protei.ru",
    "ueMaxDl": 10000,
    "ueMaxUl": 10000
  }
}
```