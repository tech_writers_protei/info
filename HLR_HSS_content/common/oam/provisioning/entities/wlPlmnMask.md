
## Параметры маски PLMN для белого списка WlPlmnMask

| Поле    | Описание                                                                                                         | Тип    | O/M |
|---------|------------------------------------------------------------------------------------------------------------------|--------|-----|
| mask    | Маска номера в формате E.212. См. [Recommendation&nbsp;ITU-T&nbsp;E.212](https://www.itu.int/rec/T-REC-E.212-201609-I/en). | regex  | M   |
| country | Название страны.                                                                                                 | string | M   |
| network | Имя сети.                                                                                                        | string | M   |
| action  | Код действия со списком.pass:q[\<br\>]`0` -- create, создать, или modify, изменить;pass:q[\<br\>]`1` -- delete, удалить.             | int    | O   |

### Пример ###

```json
{
  "mask": "25001",
  "country": "rus",
  "network": "supertelecom"
}
```