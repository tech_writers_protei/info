
## Параметры предпочитаемого узла S-CSCF PrefferedScscf

| Поле      | Описание                          | Тип    | O/M |
|-----------|-----------------------------------|--------|-----|
| ScscfName | Имя узла S-CSCF.                  | string | М   |
| Priority  | Приоритет узла S-CSCF.            | int    | M   |
| Delete    | Флаг удаления значения параметра. | bool   | O   |

### Пример ###

```json
{
  "ScscfName": "scscf1",
  "Priority": 1
}
```