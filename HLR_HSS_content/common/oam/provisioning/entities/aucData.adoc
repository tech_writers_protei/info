== Параметры профиля аутентификации AucData

[width="100%",cols="10%,81%,3%,2%,4%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|opId |Идентификатор ключа оператора для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |
|tkId |Идентификатор транспортного ключа для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |
|opcTkId |Идентификатор транспортного ключа OPC. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |2.0.35.0
|cId |Идентификатор аддитивной постоянной для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |
|rId |Идентификатор постоянной поворота для алгоритма MILENAGE. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |
|amfUmts |Поле управления аутентификацией в сетях UMTS. |string |O |
|amfLte |Поле управления аутентификацией в сетях LTE. |string |O |
|amfIms |Поле управления аутентификацией в сетях IMS. |string |O |
|resSize |Длина ответа пользователя для алгоритма MILENAGE, в байтах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |2.0.33.10
|ckSize |Длина ключа конфиденциальности для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |2.0.33.10
|ikSize |Длина ключа целостности для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |2.0.33.10
|macSize |Код сетевой аутентификации для алгоритма MILENAGE, в битах. См. https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf[3GPP TS 35.205]. |int |O |2.0.33.10
|keccakIterations |Количество раундов алгоритма хэширования SHA-3 (Keccak). См. https://nvlpubs.nist.gov/nistpubs/fips/nist.fips.202.pdf[NIST FIPS PUB 202]. |int |O |2.0.33.10
|sqnType |Код способа выбора последовательности SQN.pass:q[<br>]`0` -- не зависимый от времени; `1` -- основанный на времени. |int |O |2.0.37.0
|===

=== Пример

[source,json]
----
{
  "opId": 1,
  "tkId": 1,
  "cId": 1,
  "rId": 1,
  "amfUmts": "0000",
  "amfLte": "8000",
  "amfIms": "8001"
}
----
