
## Параметры Initial Filter Criteria Ifc

| Поле                  | Описание                      | Тип                                        | O/M |
|-----------------------|-------------------------------|--------------------------------------------|-----|
| Name                  | Имя профиля iFC.              | string                                     | M   |
| Priority              | Приоритет профиля iFC.        | int                                        | M   |
| ApplicationServerName | Имя сервера приложений.       | string                                     | M   |
| ProfilePartIndicator  | Статус абонента в сети IMS.   | int                                        | О   |
| TriggerPoint          | Параметры триггерной точки.   | [TriggerPoint](../triggerPoint/)           | O   |
| ApplicationServer     | Параметры сервера приложений. | [ApplicationServer](../applicationServer/) | O   |
| Delete                | Флаг удаления записи.         | bool                                       | O   |

### Пример ###

```json
{
  "ApplicationServerName": "as",
  "Name": "ifc1",
  "Priority": 1,
  "ProfilePartIndicator": 1,
  "TriggerPoint": {
    "ConditionTypeCNF": 1,
    "Spt": [
      {
        "ConditionNegated": 2,
        "Content": "headerContent",
        "Group": 1,
        "Header": "header",
        "Method": "INVITE",
        "RegistrationType": 1,
        "RequestUri": "http://ims.protei.ru/spt1",
        "SdpLine": "sdpLine",
        "SdpLineContent": "sdpLineContent",
        "SessionCase": 1,
        "Type": 3
      }
    ]
  }
}
```