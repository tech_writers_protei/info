
## Параметры Capability

| Поле      | Описание                          | Тип  | O/M |
|-----------|-----------------------------------|------|-----|
| Code      | Код Capability.                   | int  | M   |
| Mandatory | Флаг обязательности Capability.   | bool | M   |
| Delete    | Флаг удаления значения параметра. | bool | O   |

### Пример ###

```json
{
  "Code": 1,
  "Mandatory": 1
}
```