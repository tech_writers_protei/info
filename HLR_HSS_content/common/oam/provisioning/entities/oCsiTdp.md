
## Параметры триггерной точки обнаружения профиля O-CSI OCsiTdp

| Поле              | Описание                                                                                         | Тип                                          | O/M |
|-------------------|--------------------------------------------------------------------------------------------------|----------------------------------------------|-----|
| tdpId             | Идентификатор триггерной точки обнаружения.                                                      | int                                          | М   |
| serviceKey        | Идентификатор службы, `ServiceKey`.                                                              | int                                          | М   |
| gsmScfAddress     | Адрес узла IM-SSF.                                                                               | string                                       | О   |
| defaultHandling   | Код действия при обработке вызова по умолчанию.pass:q[\<br\>]`0` -- продолжить вызов; `1` -- отбить вызов. | int                                          | О   |
| csiTdpDnCriterias | Перечень критериев номера назначения триггерной точки обнаружения O-CSI.                         | [[OCsiTdpDnCriteria](../oCsiTdpDnCriteria/)] | O   |
| csiTdpBsCriterias | Перечень критериев базовой услуги триггерной точки обнаружения O-CSI.                            | [[OCsiTdpBsCriteria](../oCsiTdpBsCriteria/)] | O   |
| csiTdpCtCriterias | Перечень критериев типа вызова триггерной точки обнаружения O-CSI.                               | [[OCsiTdpCtCriteria](../oCsiTdpCtCriteria/)] | O   |
| csiTdpCvCriterias | Перечень критериев причины отбоя триггерной точки обнаружения O-CSI.                             | [[OCsiTdpCvCriteria](../oCsiTdpCvCriteria/)] | O   |
| delete            | Флаг удаления записи.                                                                            | bool                                         | O   |

### Пример ###

```json
{
  "tdpId": 1,
  "serviceKey": 1,
  "gsmScfAddress": "78924813183138",
  "defaultHandling": 2
}
```