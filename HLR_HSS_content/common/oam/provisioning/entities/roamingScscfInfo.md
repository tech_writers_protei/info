
## Параметры Roaming S-CSCFRoamingScscfInfo###

| Поле                    | Описание                                                                                                                                          | Тип      | O/M |
|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|----------|-----|
| scscfName               | Название узла S-CSCF.                                                                                                                             | string   | М   |
| host                    | Хост узла S-CSCF для протокола Diameter.                                                                                                          | string   | O   |
| realm                   | Realm узла S-CSCF для протокола Diameter.                                                                                                         | string   | O   |
| state                   | Флаг активности регистрации.                                                                                                                      | bool     | O   |
| reassignmentPendingFlag | Флаг переназначения узла S-CSCF.                                                                                                                  | bool     | O   |
| sf1                     | Значение `Supported-Features`. См.&nbsp;[3GPP&nbsp;TS&nbsp;29.229](https://www.etsi.org/deliver/etsi_ts/129200_129299/129229/17.02.00_60/ts_129229v170200p.pdf). | [int]    | O   |
| dtRegistered            | Дата и время регистрации абонента.                                                                                                                | datetime | O   |

### Пример ###

```json
{
  "scscfName": "scscf",
  "host": "scscf.ims.protei.ru",
  "realm": "ims.protei.ru",
  "state": 1
}
```