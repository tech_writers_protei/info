
## Параметры местоположения абонента на узле MME MmeLocationInformation

| Поле                     | Описание                                                                                                                                            | Тип                                          | O/M |
|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------|-----|
| ageOfLocationInformation | Значение `AgeOfLocation`.                                                                                                                           | int                                          | O   |
| currentLocationRetrieved | Флаг предоставления текущего местоположения абонента.                                                                                               | bool                                         | O   |
| eUtranCellGlobalIdentity | Глобальный идентификатор соты в сетях E-UTRAN.                                                                                                      | hex                                          | O   |
| geodeticInformation      | Геодезическое местоположение отправки вызова. См. [ITU-T Recommendation Q.763](https://www.itu.int/rec/T-REC-Q.763-199912-I/en).                    | string                                       | O   |
| geographicalInformation  | Описание географической области. См.&nbsp;[3GPP&nbsp;TS&nbsp;23.032](https://www.etsi.org/deliver/etsi_ts/123000_123099/123032/18.01.00_60/ts_123032v180100p.pdf). | string                                       | O   |
| trackingAreaIdentity     | Идентификатор области отслеживания.                                                                                                                 | hex                                          | O   |
| enodebId                 | Идентификатор базовой станции сети.                                                                                                                 | string                                       | O   |
| extendedEnodebId         | Расширенный идентификатор базовой станции сети.                                                                                                     | string                                       | O   |
| userCsgInformation       | Информация о доступе к ячейке закрытой группы, CSG.                                                                                                 | [UserCsgInformation](../userCsgInformation/) | O   |

### Пример ###

```json
{
  "ageOfLocationInformation": 1,
  "eUtranCellGlobalIdentity": "00f1102b2d1010",
  "trackingAreaIdentity": "00f1100001"
}
```