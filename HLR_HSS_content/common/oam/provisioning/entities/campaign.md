
## Параметры операции Campaign

| Поле                                        | Описание                                                            | Тип                                    | O/M | Версия   |
|---------------------------------------------|---------------------------------------------------------------------|----------------------------------------|-----|----------|
| campaignCounter                             | Счетчик результатов выполнения операции.                            | [СampaignCounter](../campaignCounter/) | М   | 2.0.50.0 |
| [campaignStatus](#campaign-status-campaign) | Текущее состояние операции.                                         | string                                 | M   | 2.0.50.0 |
| [campaignType](#campaign-type-campaign)     | Тип операции.                                                       | string                                 | M   | 2.0.50.0 |
| id                                          | Идентификатор операции.                                             | int                                    | M   | 2.0.50.0 |
| creationDate                                | Дата и время создания операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`.   | datetime                               | O   | 2.0.50.0 |
| startDate                                   | Дата и время начала операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`.     | datetime                               | O   | 2.0.50.0 |
| finishDate                                  | Дата и время завершения операции. Формат:pass:q[\<br\>]`YYYY-MM-DDThh:mm:ss`. | datetime                               | O   | 2.0.50.0 |

### Пример ###

```json
{
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "SIMCARDS_INFORMATION_LOAD",
  "id": 49,
  "creationDate": "2022-07-28T12:50:37",
  "startDate": "2022-07-28T12:50:37"
}
```

### [[campaign-status-campaign]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

### [[campaign-type-campaign]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |