
## Параметры запрета доступа AccessRestrictionData

| Поле                                     | Описание                                                 | Тип  | O/M |
|------------------------------------------|----------------------------------------------------------|------|-----|
| utranNotAllowed                          | Флаг запрета регистрации в сетях UTRAN.                  | bool | O   |
| geranNotAllowed                          | Флаг запрета регистрации в сетях GERAN.                  | bool | O   |
| ganNotAllowed                            | Флаг запрета регистрации в сетях GAN.                    | bool | O   |
| iHspaEvolutionNotAllowed                 | Флаг запрета регистрации в сетях HSPA Evolution.         | bool | O   |
| wbEUtranNotAllowed                       | Флаг запрета регистрации в сетях E–UTRAN.                | bool | O   |
| hoToNon3GPPAccessNotAllowed              | Флаг запрета регистрации в сетях с доступом не-3GPP.     | bool | O   |
| nbIotNotAllowed                          | Флаг запрета регистрации в сетях NB-IoT.                 | bool | O   |
| enhancedCoverageNotAllowed               | Флаг запрета регистрации в сетях Enhanced Coverage.      | bool | O   |
| nrSecondaryRatEutranNotAllowed           | Флаг запрета регистрации в сетях Secondary RAT E–UTRAN.  | bool | O   |
| unlicensedSpectrumSecondaryRatNotAllowed | Флаг запрета регистрации в сетях Spectrum Secondary RAT. | bool | O   |
| nr5gsNotAllowed                          | Флаг запрета регистрации в сетях NR–5G.                  | bool | O   |

### Пример ###

```json
{
  "utranNotAllowed": 1,
  "geranNotAllowed": 1,
  "ganNotAllowed": 1,
  "iHspaEvolutionNotAllowed": 1,
  "wbEUtranNotAllowed": 1,
  "hoToNon3GPPAccessNotAllowed": 1,
  "nbIotNotAllowed": 1,
  "enhancedCoverageNotAllowed": 1,
  "nrSecondaryRatEutranNotAllowed": 1,
  "unlicensedSpectrumSecondaryRatNotAllowed": 1,
  "nr5gsNotAllowed": 1
}
```