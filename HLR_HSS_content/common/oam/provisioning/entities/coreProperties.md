
## Параметры настроек CoreProperties

| Поле    | Описание                                  | Тип    | O/M | Версия   |
|---------|-------------------------------------------|--------|-----|----------|
| hlr     | Флаг поддержки регистрации в сетях 2G/3G. | bool   | M   |          |
| hss     | Флаг поддержки регистрации в сетях 4G.    | bool   | M   |          |
| ims     | Флаг поддержки регистрации в сетях IMS.   | bool   | M   |          |
| lcs     | Флаг поддержки услуг геолокации.          | bool   | M   |          |
| version | Версия HLR/HSS Core.                      | string | M   | 2.0.51.0 |

### Пример ###

```json
{
  "hlr": true,
  "hss": true,
  "ims": false,
  "lcs": false,
  "version": "2.0.74.5.1245"
}
```