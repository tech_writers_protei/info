
## Параметры ODB OdbParam

| Поле                                              | Описание                                                                                                                       | Тип   | O/M |
|---------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------|-------|-----|
| [[general-list-odb-param]]general-list | Перечень общих параметров ODB.                                                                                                 | [int] | О   |
| [[hplmn-list-odb-param]]hplmn-list     | Перечень домашних параметров ODB для сети HPLMN.                                                                               | [int] | О   |
| action                                            | Значение, которое будет подставлено в параметры [general-list](#general-list-odb-param) и [hplmn-list](#hplmn-list-odb-param). | int   | M   |
| SubscriberStatus                                  | Флаг активности абонента.                                                                                                      | bool  | O   |

### Пример ###

```json
{
  "general-list": [ 1, 6, 19 ],
  "action": 1,
  "SubscriberStatus": 1
}
```