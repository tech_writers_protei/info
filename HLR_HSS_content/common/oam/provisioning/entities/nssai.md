
## Параметры Network Slice Selection Assistance Information Nssai

| Поле                | Описание                       | Тип                        | O/M | Версия  |
|---------------------|--------------------------------|----------------------------|-----|---------|
| defaultSingleNssais | Перечень S-NSSAI по умолчанию. | [[\<SNssai\>](../snssai/)] | O   | 2.1.4.0 |
| singleNssais        | Перечень S-NSSAI.              | [[\<SNssai\>](../snssai/)] | O   | 2.1.4.0 |


### Пример ###

```json
{
  "defaultSingleNssais": [
    {
      "sst": 1,
      "sd": "123456"
    },
    {
      "sst": 2,
      "sd": "654321"
    }
  ]
}
```