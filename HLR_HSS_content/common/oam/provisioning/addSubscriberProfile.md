
Команда `AddSubscriberProfile` позволяет добавлять профили абонента в базу данных HLR/HSS к ранее загруженной карте.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/AddSubscriberProfile \
  -d '{ "<subscriberProfile>": <SubscriberProfile> }'
```

### Поля тела запроса ###

| Поле              | Описание                    | Тип                                                 | O/M |
|-------------------|-----------------------------|-----------------------------------------------------|-----|
| subscriberProfile | Параметры профиля абонента. | [SubscriberProfile](../entities/subscriberProfile/) | M   |

### Пример тела запроса ###

```json
{
  "imsi": "250010000001",
  "msisdn": "79000000001",
  "status": 2,
  "category": 10,
  "networkAccessMode": 0,
  "DefaultForwardingNumber": "867349752",
  "DefaultForwardingStatus": 7,
  "ipSmGwNumber": "23525252",
  "activeTime": 1000,
  "link-eps-data": [
    {
      "context-id": 2,
      "ipv4": "192.168.1.22",
      "plmnId": "25001"
    }
  ],
  "ODB":{
    "generalSetList": [ 1, 6, 19 ],
    "SubscriberStatus": 1
  },
  "pdpData": [
    {
      "context-id": 1,
      "type": "0080"
    }
  ],
  "EpsData": {
    "defContextId": 1,
    "ueMaxDl": 10000,
    "ueMaxUl": 10000
  },
  "edrxCycleLength": [
    {
      "ratType": 1005,
      "lengthValue": 8
    }
  ],
  "ssData": [
    {
      "ss_Code": 17,
      "ss_Status": 5,
      "sub_option_type": 1,
      "sub_option": 1,
      "tele_service": [ 0, 16, 32 ]
    }
  ],
  "ssForw": [
    {
      "ss_Code": 42,
      "ss_Status": 7,
      "forwardedToNumber": "42513466754",
      "tele_service": [ 0, 16, 32]
    }
  ],
  "imsProfile":{
    "impi": "250010000001@ims.protei.ru",
    "authScheme": 5,
    "sipDigest": { "password": "elephant" },
    "implicitlySets": [ { "name": "implSet1" } ],
    "impus": [
      {
        "Identity": "sip:79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 0,
        "CanRegister": 1,
        "ServiceProfileName": "sp"
      },

      {
        "Identity": "tel:+79000000001@ims.protei.ru",
        "BarringIndication": 0,
        "Type": 1,
        "CanRegister": 1,
        "WildcardPsi": "tel:+79000000001!.*!",
        "PsiActivation": 1,
        "ServiceProfileName": "sp"
      }
    ]
  },
  "imsSubscription": {
    "name": "250010000001",
    "capabilitySetId": 1,
    "prefferedScscfSetId": 1,
    "chargingInformationName": "ci",
    "implicitlyRegisteredSet": [
      {
        "name": "implSet1",
        "impus": [
          {
            "Identity": "sip:protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp",
            "Default":true
          },
          {
            "Identity": "sip:ntc_protei@ims.protei.ru",
            "BarringIndication": 0,
            "Type": 0,
            "CanRegister": 1,
            "ServiceProfileName": "sp"
          }
        ]
      }
    ],
    "serviceProfiles": [
      {
        "Name": "sp",
        "CoreNetworkServiceAuthorization": 1,
        "Ifcs": [
          {
            "Name": "ifc1",
            "Priority": 1,
            "ApplicationServerName": "as",
            "ProfilePartIndicator": 1,
            "TriggerPoint":{
              "ConditionTypeCNF": 1,
              "Spt": [
                {
                  "Group": 1,
                  "Method": "INVITE",
                  "SessionCase": 1,
                  "ConditionNegated": 2,
                  "Type": 3,
                  "RequestUri": "http://ims.protei.ru/spt1",
                  "Header": "header",
                  "Content": "headerContent",
                  "SdpLine": "sdpLine",
                  "SdpLineContent": "sdpLineContent",
                  "RegistrationType": 1
                }
              ]
            }
          }
        ]
      }
    ]
  }
}
```

### Ответ ###

В ответе передаются статус запроса и детализация неуспешного выполнения команды.

```
{
  "status": "<string>",
  "problemDetails": [ <ProblemDetails> ]
}
```

### Поля ответа ###

| Поле           | Описание                                    | Тип                                             | O/M | Версия   |
|----------------|---------------------------------------------|-------------------------------------------------|-----|----------|
| status         | Статус запроса.                             | string                                          | M   | 2.0.39.0 |
| problemDetails | Детализация неуспешного выполнения команды. | [[ProblemDetails](../entities/problemDetails/)] | О   | 2.0.44.0 |