
Команда `ChangeSnssai` позволяет изменить данные Single Network Slice Selection Assistance Information.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeSnssai \
  -d '{
  "action": "<string>",
  "snssai": <Snssai>
}'
```

### Поля тела запроса ###

| Поле   | Описание                                                                                 | Тип                           | O/M | Версия   |
|--------|------------------------------------------------------------------------------------------|-------------------------------|-----|----------|
| action | Выполняемое действие.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                        | M   | 2.1.14.0 |
| snssai | Профиль SNSSAI.                                                                          | [Snssai](../entities/snssai/) | M   | 2.1.14.0 |

### Пример тела запроса ###

```json
{
  "action": "create",
  "snssai": {
    "name": "snssai1",
    "sst": 1,
    "sd": "123456",
    "contexts": [ 1 ]
  }
}
```

### Ответ ###

В ответе передаются статус запроса и параметры профиля S-NSSAI.

```
{
  "status": "<string>",
  "snssai": <Snssai>
}
```

### Поля ответа ###

| Поле   | Описание        | Тип                           | O/M | Версия   |
|--------|-----------------|-------------------------------|-----|----------|
| status | Статус запроса. | string                           | M   | 2.1.14.0 |
| snssai | Профиль SNSSAI. | [Snssai](../entities/snssai/) | M   | 2.1.14.0 |

### Пример ответа ###

```json
{
  "status": "OK",
  "snssai": {
    "name": "snssai1",
    "sst": 1,
    "sd": "123456",
    "contexts": [ 1 ]
  }
}
```