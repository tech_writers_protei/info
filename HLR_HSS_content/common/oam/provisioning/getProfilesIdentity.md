
Команда `GetProfilesIdentity` позволяет получать идентификаторы профилей для общих объектов.

### Запрос ###

```bash
$ curl -X GET https://<host>:<port>/ProfileService/GetProfilesIdentity/<entity>
```

Возможные общие объекты entity приведены в таблице ниже.

### Поля запроса query ###

| Поле                 | Описание                                             | Тип                                                     | Версия   |
|----------------------|------------------------------------------------------|---------------------------------------------------------|----------|
| ApplicationServer    | Сервер приложений.                                   | [ApplicationServer](../entities/applicationServer/)     | 2.0.43.0 |
| AucC                 | Аддитивная постоянная центра аутентификации.         | [CParam](../entities/cParam/)                           | 2.0.43.0 |
| AucOp                | Поле конфигурации алгоритма, задаваемого оператором. | [OpParam](../entities/opParam/)                         | 2.0.43.0 |
| AucR                 | Постоянная поворота центра аутентификации.           | [RParam](../entities/rParam/)                           | 2.0.43.0 |
| AucTk                | Транспортный ключ центра аутентификации.             | [TkParam](../entities/tkParam/)                         | 2.0.43.0 |
| BlackList            | Черные списки.                                       | [BlackList](../entities/blackList/)                     | 2.0.43.0 |
| WhiteList            | Белые списки.                                        | [WhiteList](../entities/whiteList/)                     | 2.0.43.0 |
| ChargingInformation  | Параметры тарификации.                               | [ChargingInformation](../entities/chargingInformation/) | 2.0.43.0 |
| DCsi                 | Профиль D-CSI.                                       | [DCsi](../entities/dCsi/)                               | 2.0.43.0 |
| DImCsi               | Профиль D-IM-CSI.                                    | [DImCsi](../entities/dCsi/)                             | 2.0.43.0 |
| GprsCsi              | Профиль GPRS-CSI.                                    | [GprsCsi](../entities/gprsCsi/)                         | 2.0.43.0 |
| MCsi                 | Профиль M-CSI.                                       | [MCsi](../entities/mCsi/)                               | 2.0.43.0 |
| OCsi                 | Профиль O-CSI.                                       | [OCsi](../entities/oCsi/)                               | 2.0.43.0 |
| OImCsi               | Профиль O-IM-CSI.                                    | [OImCsi](../entities/oCsi/)                             | 2.0.43.0 |
| SmsCsi               | Профиль SMS-CSI.                                     | [SmsCsi](../entities/smsCsi/)                           | 2.0.43.0 |
| SsCsi                | Профиль SS-CSI.                                      | [SsCsi](../entities/ssCsi/)                             | 2.0.43.0 |
| TCsi                 | Профиль Т-CSI.                                       | [TCsi](../entities/tCsi/)                               | 2.0.43.0 |
| UssdCsi              | Профиль USSD-CSI.                                    | [UssdCsi](../entities/ussdCsi/)                         | 2.0.43.0 |
| VtImCsi              | Профиль VT-IM-CSI.                                   | [VtImCsi](../entities/tCsi/)                            | 2.0.43.0 |
| Eps                  | Профиль данных EPS.                                  | [Eps](../entities/epsProfile/)                          | 2.0.43.0 |
| ExternalGroupIds     | Профиль данных ExternalGroupIds.                     | [ExternalGroupIds](../entities/externalGroupIds/)       | 2.0.43.0 |
| Group                | Группа пользователей.                                | [Group](../entities/group/)                             | 2.0.43.0 |
| Lcs                  | Профиль услуг LCS.                                   | [LcsProfile](../entities/lcsProfile/)                   | 2.0.43.0 |
| Pdp                  | Профиль данных PDP.                                  | [PdpProfile](../entities/pdpProfile/)                   | 2.0.43.0 |
| QosEps               | Профиль QoS EPS.                                     | [QosEps](../entities/qosEps/)                           | 2.0.43.0 |
| QosGprs              | Профиль QoS GPRS.                                    | [QosGprs](../entities/qosGprs/)                         | 2.0.43.0 |
| RegionalZoneCode     | Коды зон регионов.                                   | [RegionalZoneCode](../entities/regional/)               | 2.0.43.0 |
| PrefferedScscfSet    | Набор предпочитаемых узлов S-CSCF.                   | [PrefferedScscfSet](../entities/prefferedScscfSet/)     | 2.0.43.0 |
| ProfileTemplate      | Шаблон профиля.                                      | [ProfileTemplate](../entities/ProfileTemplate/)         | 2.0.49.0 |
| RoamingAgreement     | Профиль RoamingAgreement.                            | [RaVlr](../entities/RaVlr/)                             | 2.0.43.0 |
| Scef                 | Узлы SCEF.                                           | [Scef](../entities/scef/)                               | 2.0.43.0 |
| ScscfCapabilitiesSet | Набор возможностей узлов S-CSCF.                     | [CapabilitiesSet](../entities/CapabilitiesSet/)         | 2.0.43.0 |
| SingleNssai          | Профиль Single NSSAI.                                | [Snssai](../entities/snssai/)                           | 2.1.14.0 |
| SharedIfcSet         | Набор общих iFC.                                     | [SIfcSet](../entities/sIfcSet/)                         | 2.0.43.0 |

### Ответ ###

В ответе передаются статус запроса и перечень общих объектов.

```
{
  "status": "<string>",
  "identities": [ <ExternalProfileIdentity> ]
}
```

### Поля ответа ###

| Поле       | Описание                 | Тип                                                               | O/M | Версия   |
|------------|--------------------------|-------------------------------------------------------------------|-----|----------|
| status     | Статус запроса.          | string                                                            | M   |          |
| identities | Перечень общих объектов. | [[ExternalProfileIdentity](../entities/externalProfileIdentity/)] | M   | 2.0.43.0 |

### Пример ответа ###

```json
{
  "status": "OK",
  "identities": [
    {
      "id": 1,
      "name": "internet"
    },
    {
      "id": 2,
      "name": "ims"
    }
  ]
}
```