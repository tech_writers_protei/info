
Команда `SetMSISDN` позволяет изменить набор номеров MSISDN из файла.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/SetMSISDN \
  -d @<path_to_file>.csv
```

В теле запроса передается файл формата CSV.

Формат файла:

```csv
imsi;msisdn;new_msisdn;new_status;
```

### Поля тела запроса ###

| Поле                                          | Описание                | Тип    | O/M |
|-----------------------------------------------|-------------------------|--------|-----|
| imsi                                          | Текущий номер IMSI.     | string | О   |
| msisdn                                        | Текущий номер MSISDN.   | string | О   |
| new_msisdn                                    | Новый номер MSISDN.     | string | М   |
| [new_status](#new-status-set-msisdn-campaign) | Код состояния абонента. | int    | М   |

#### [[new-status-set-msisdn-campaign]]Коды состояния абонента

| N | Статус                | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

### Пример файла формата CSV  ###

```csv
250000000000001;250000000000002;79000000001;250000000000001;
```

### Ответ ###

В ответе передаются параметры операции.

```
{
  "status": "OK",
  "campaignCounter": <CampaignCounter>,
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": <datetime>,
  "startDate": <datetime>,
  "finishDate": <datetime>
}
```

### Поля ответа ###

| Поле                                                   | Описание                 | Тип                                             | O/M | Версия   |
|--------------------------------------------------------|--------------------------|-------------------------------------------------|-----|----------|
| status                                                 | Статус запроса.          | string                                          | M   |          |
| campaignCounter                                        | Счетчик операций.        | [СampaignCounter](../entities/campaignCounter/) | M   | 2.0.56.0 |
| [campaignStatus](#campaign-status-set-msisdn-campaign) | Статус операции.         | string                                          | M   | 2.0.56.0 |
| [campaignType](#campaign-type-set-msisdn-campaign)     | Тип операции.            | string                                          | M   | 2.0.56.0 |
| id                                                     | Идентификатор  операции. | int                                             | M   | 2.0.56.0 |
| creationDate                                           | Дата создания операции.  | datetime                                        | O   | 2.0.56.0 |
| startDate                                              | Дата запуска операции.   | datetime                                        | O   | 2.0.56.0 |
| finishDate                                             | Дата окончания операции. | datetime                                        | O   | 2.0.56.0 |

#### [[campaign-status-set-msisdn-campaign]]Состояния операций

| Поле           | Описание                                      |
|----------------|-----------------------------------------------|
| NEW            | Операция еще не начата                        |
| IN_PROGRESS    | Операция в процессе выполнения                |
| ERROR          | Во время выполнения операции произошла ошибка |
| PARTLY_SUCCEED | Операция выполнена частично                   |
| SUCCEED        | Операция успешно выполнена                    |
| CANCELED       | Операция отменена                             |

#### [[campaign-type-set-msisdn-campaign]]Типы операций

| Поле                      | Описание                         |
|---------------------------|----------------------------------|
| ADD_SUBSCRIBER_PROFILE    | Добавление профиля абонента      |
| CHANGE_IMSI               | Изменение номера IMSI            |
| CHANGE_MSISDN             | Изменение номера MSISDN          |
| CHANGE_PROFILE            | Изменение профиля                |
| DELETE_SUBSCRIBER         | Удаление абонента                |
| DELETE_SUBSCRIBER_PROFILE | Удаление профиля абонента        |
| SIMCARDS_INFORMATION_LOAD | Загрузка информации на SIM-карту |

### Пример ответа ###

```json
{
  "status": "OK",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "CHANGE_MSISDN",
  "id": 49,
  "creationDate": "2022-07-28T12:50:37",
  "startDate": "2022-07-28T12:50:37"
}
```