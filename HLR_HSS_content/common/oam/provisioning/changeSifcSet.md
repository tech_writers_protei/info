
Команда `ChangeSifcSet` позволяет изменять набор общих iFC.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeSifcSet \
  -d '{
  "Action": "<string>",
  "SIfcs": [ <SIfcSet> ]
}'
```

### Поля тела запроса ###

| Поле   | Описание                                                                         | Тип                               | O/M |
|--------|----------------------------------------------------------------------------------|-----------------------------------|-----|
| Action | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                            | M   |
| SIfcs  | Набор общих iFC.                                                                 | [[SIfcSet](../entities/sIfcSet/)] | O   |

### Пример тела запроса ###

```json
{
  "Action": "create",
  "SIfcs": [
    {
      "Id": 1,
      "Ifcs": [
        {
          "Name": "ifc1",
          "Priority": 1,
          "ApplicationServerName": "as",
          "ProfilePartIndicator": 1,

          "TriggerPoint": {
            "ConditionTypeCNF": 1,
            "Spt": [
              {
                "Group": 1,
                "Method": "INVITE",
                "SessionCase": 1,
                "ConditionNegated": 2,
                "Type": 3,
                "RequestUri": "http://ims.protei.ru/spt1",
                "Header": "header",
                "Content": "headerContent",
                "SdpLine": "sdpLine",
                "SdpLineContent": "sdpLineContent",
                "RegistrationType": 1
              }
            ]
          }
        }
      ]
    }
  ]
}
```

### Ответ ###

В ответе передается только статус запроса.