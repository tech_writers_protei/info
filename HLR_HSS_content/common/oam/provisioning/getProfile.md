
Команда `GetProfile` позволяет получать профиль абонента.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetProfile \
  -d '{
  "imsi": "<string>",
  "all": <bool>
}'
```

### Поля тела запроса ###

| Поле                                    | Описание                                                                                                                             | Тип    | O/M | Версия   |
|-----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|--------|-----|----------|
| [[imsi-get-profile]]imsi     | Номер IMSI абонента.                                                                                                                 | string | C   |          |
| [[msisdn-get-profile]]msisdn | Номер MSISDN абонента.                                                                                                               | string | C   |          |
| [[impi-get-profile]]impi     | Приватный идентификатор пользователя в сети IMS.                                                                                     | string | C   |          |
| [[impu-get-profile]]impu     | Публичный идентификатор пользователя в сети IMS.                                                                                     | string | C   |          |
| status                                  | Флаг запроса состояния абонента.                                                                                                     | bool   | O   |          |
| forbid_reg                              | Флаг запроса значения параметра [forbidRegistration_WithoutCamel](../entities/subscriberProfile/#forbid-registration-without-camel). | bool   | O   |          |
| odb-param                               | Флаг запроса параметров запретов, установленных оператором связи.                                                                    | bool   | O   |          |
| pdp-data                                | Флаг запроса параметров PDP.                                                                                                         | bool   | O   |          |
| eps-context-data                        | Флаг запроса параметров контекста EPS.                                                                                               | bool   | O   |          |
| eps-data                                | Флаг запроса параметров EPS.                                                                                                         | bool   | O   |          |
| csi-list                                | Флаг запроса параметров CSI-профиля.                                                                                                 | bool   | O   |          |
| ssData                                  | Флаг запроса параметров дополнительной услуги, SS.                                                                                   | bool   | O   |          |
| ssForw                                  | Флаг запроса параметров дополнительных услуг SS переадресации для абонента.                                                          | bool   | O   |          |
| ssBarring                               | Флаг запроса параметров дополнительных услуг SS запрета для абонента.                                                                | bool   | O   |          |
| teleserviceList                         | Флаг запроса списка телеуслуг абонента.                                                                                              | bool   | O   |          |
| bearerserviceList                       | Флаг запроса списка служб передачи данных абонента.                                                                                  | bool   | O   |          |
| roaming-info                            | Флаг запроса параметров роуминга абонента.                                                                                           | bool   | O   |          |
| roaming-sgsn-info                       | Флаг запроса параметров роумингового узла SGSN абонента.                                                                             | bool   | O   |          |
| defaultForw                             | Флаг запроса параметров переадресации по умолчанию для абонента.                                                                     | bool   | O   |          |
| whiteLists                              | Флаг запроса белых списков абонента.                                                                                                 | bool   | O   |          |
| blackLists                              | Флаг запроса черных списков абонента.                                                                                                | bool   | O   |          |
| lcs                                     | Флаг запроса профиля LCS.                                                                                                            | bool   | O   |          |
| tk                                      | Флаг запроса значения транспортного ключа.                                                                                           | bool   | O   |          |
| group                                   | Флаг запроса параметров группы.                                                                                                      | bool   | O   |          |
| deactivatePsi                           | Флаг запроса значения параметра deactivatePsi.                                                                                       | bool   | O   |          |
| baocWithoutCamel                        | Флаг запроса значения параметра baocWithoutCamel.                                                                                    | bool   | O   |          |
| ipSmGw                                  | Флаг запроса параметров узла IP-SM-GW.                                                                                               | bool   | O   |          |
| networkAccessMode                       | Флаг запроса режима доступа к сети.                                                                                                  | bool   | O   |          |
| fullWBL                                 | Флаг запроса всех белых и черных списков.                                                                                            | bool   | O   |          |
| fullCSI                                 | Флаг запроса всех параметров CSI-профиля.                                                                                            | bool   | O   |          |
| qosGprsId                               | Флаг запроса идентификатора профиля QoS GPRS.                                                                                        | bool   | O   |          |
| qosEpsId                                | Флаг запроса идентификатора профиля QoS EPS.                                                                                         | bool   | O   |          |
| accessRestrictionData                   | Флаг запроса прав доступа.                                                                                                           | bool   | O   |          |
| chargingCharacteristics                 | Флаг запроса значения `ChargingCharacteristics`.                                                                                     | bool   | O   |          |
| ims                                     | Флаг запроса подписки в сетях IMS.                                                                                                   | bool   | O   |          |
| imsFull                                 | Флаг запроса значений всех параметров подписки в сетях IMS.                                                                          | bool   | O   |          |
| roaming-scscf-info                      | Флаг запроса параметров роумингового узла S–CSCF.                                                                                    | bool   | O   |          |
| aMsisdn                                 | Флаг запроса дополнительных номеров MSISDN.                                                                                          | bool   | O   |          |
| periodicLauTimer                        | Флаг запроса значения параметра `PeriodicLauTimer`.                                                                                  | bool   | O   |          |
| periodicRauTauTimer                     | Флаг запроса значения параметра `PeriodicRauLauTimer`.                                                                               | bool   | O   |          |
| HSM_ID                                  | Флаг запроса идентификатора модуля HSM.                                                                                              | bool   | O   |          |
| opc                                     | Флаг запроса точки пункта отправления OPC.                                                                                           | bool   | O   |          |
| ueUsageType                             | Флаг запроса значения параметра ueUsageType.                                                                                         | bool   | O   |          |
| lastActivity                            | Флаг запроса даты и времени последней активности.                                                                                    | bool   | O   |          |
| SCA                                     | Флаг запроса адреса сервисного центра.                                                                                               | bool   | O   |          |
| category                                | Флаг запроса категории абонента.                                                                                                     | bool   | O   |          |
| icsIndicator                            | Флаг запроса значения параметра icsIndicator.                                                                                        | bool   | O   |          |
| csToPsSrvccAllowedIndicator             | Флаг запроса значения параметра `csToPsSrvccIndicator`.                                                                              | bool   | O   | 2.0.35.0 |
| singleNssais                            | Флаг запроса S-NSSAI.                                                                                                                | bool   | O   | 2.1.14.0 |
| externalId                              | Флаг запроса внешнего идентификатора.                                                                                                | bool   | O   | 2.1.7.0  |
| comment                                 | Флаг запроса комментария к профилю.                                                                                                  | bool   | O   | 2.1.9.0  |
| externalGroupIds                        | Флаг запроса групповых идентификаторов IoT-устройств.                                                                                | bool   | O   | 2.1.10.0 |
| niddAuthorization                       | Флаг запроса авторизаций NIDD.                                                                                                       | bool   | O   | 2.1.10.0 |
| imei                                    | Флаг запроса номера IMEI устройства.                                                                                                 | bool   | O   | 2.1.10.0 |
| all                                     | Флаг запроса значений всех параметров.                                                                                               | bool   | O   |          |

**Примечание.** Задается только один из параметров [imsi](#imsi-get-profile), [msisdn](#msisdn-get-profile), [impi](#impi-get-profile) и [impu](#impu-get-profile).

### Пример тела запроса ###

```json
{
  "imsi": "250010000000001",
  "all": true
}
```

### Ответ ###

В ответе передается профиль абонента.

```
{
  "profile": <SubscriberProfileRequested>
}
```

### Поля ответа ###

| Поле    | Описание                                | Тип                                                                   | O/M |
|---------|-----------------------------------------|-----------------------------------------------------------------------|-----|
| status  | Статус запроса.                         | string                                                                | M   |
| profile | Параметры абонента при запросе профиля. | [SubscriberProfileRequested](../entities/subscriberProfileRequested/) | M   |