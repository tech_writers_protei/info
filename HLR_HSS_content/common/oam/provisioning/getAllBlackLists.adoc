[[GetAllBlackLists]]
= GetAllBlackLists

Команда `GetAllBlackLists` позволяет получать все черные списки в системе.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetAllBlackLists
----

== Ответ

В ответе передаются статус запроса и параметры черных списков.

....
{
  "status": "<string>",
  "blackList": [ <BlackList> ]
}
....

== Поля ответа

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|blackList |Перечень черных списков. |[<<oam/provisioning/entities/blackList.adoc#BlackList,BlackList>>] |M
|===
