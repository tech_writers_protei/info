
Команда `ChangeAucConfig` позволяет изменять параметры центра аутентификации.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeAucConfig \
  -d '{
  "opParams": [ <OpParam> ],
  "tkParams": [ <TkParam> ],
  "cParams": [ <CParam> ],
  "rParams": [ <RParam> ]
}'
```

### Поля тела запроса ###

| Поле     | Описание                                          | Тип                               | O/M |
|----------|---------------------------------------------------|-----------------------------------|-----|
| opParams | Перечень полей конфигурации алгоритма оператором. | [[OpParam](../entities/opParam/)] | O   |
| tkParams | Перечень транспортных ключей.                     | [[TkParam](../entities/tkParam/)] | O   |
| cParams  | Перечень аддитивных постоянных.                   | [[CParam](../entities/cParam/)]   | O   |
| rParams  | Перечень постоянных поворота.                     | [[RParam](../entities/rParam/)]   | O   |

### Пример тела запроса ###

```json
{
  "opParams": [
    {
      "id": 1,
      "op": "12345678900987654321123456789009"
    }
  ],
  "tkParams": [
    {
      "id": 1,
      "tk": "12345678900987654321123456789009",
      "algorithm": 0
    }
  ],
  "cParams": [
    {
      "id": 1,
      "c": "123456789009"
    }
  ],
  "rParams": [
    {
      "id": 1,
      "r": "123456789009"
    }
  ]
}
```

### Ответ ###

В ответе передается только статус запроса.