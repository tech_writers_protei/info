
Команда `ControlConfig` позволяет изменять конфигурацию базы данных.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ControlConfig \
  -d '{ "config": <DbConfig> }'
```

### Поля тела запроса ###

| Поле   | Описание                  | Тип                               | O/M |
|--------|---------------------------|-----------------------------------|-----|
| config | Конфигурация базы данных. | [DbConfig](../entities/dbConfig/) | M   |

### Пример тела запроса ###

```json
{
  "config": {
    "mwdMaxCount": 1,
    "maxForward": 1,
    "sqnStart": 1,
    "sqnEnd": 1000,
    "sqnStep": 1,
    "defaultForwardingOptions": 1,
    "defaultForwardingType": 1,
    "activeWhiteList": 1
  }
}
```

### Ответ ###

В ответе передается только статус запроса.