
Команда `DeleteSubscriber` позволяет удалять абонента из базы данных HLR/HSS и центра аутентификации AuC.

При наличии активной регистрации на узлах VLR, SGSN или MME она прекращается запросом MAP-CL или Diameter S6: CLR.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/DeleteSubscriber \
  -d '{
  "imsi": "<string>",
  "msisdn" : "<string>"
}'
```

### Поля тела запроса ###

| Поле                                          | Описание               | Тип    | O/M |
|-----------------------------------------------|------------------------|--------|-----|
| [[imsi-delete-subscriber]]imsi     | Номер IMSI абонента.   | string | C   |
| [[msisdn-delete-subscriber]]msisdn | Номер MSISDN абонента. | string | C   |

**Примечание.** Задается только один из параметров [imsi](#imsi-delete-subscriber) и [msisdn](#msisdn-delete-subscriber).

### Пример тела запроса ###

```json
{ "imsi": "250010000001" }
```

### Ответ ###

В ответе передается только статус запроса.