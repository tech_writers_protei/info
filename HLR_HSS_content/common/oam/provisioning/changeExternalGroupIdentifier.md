
Команда `ChangeExternalGroupIdentifier` позволяет управлять внешним идентификатором группы.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeExternalGroupIdentifier \
  -d '{
  "action": "<string>",
  "externalGroupIdentifier": <ExternalGroupIdentifier>
}'
```

### Поля тела запроса ###

| Поле                    | Описание                                                                         | Тип                                                             | O/M | Версия   |
|-------------------------|----------------------------------------------------------------------------------|-----------------------------------------------------------------|-----|----------|
| action                  | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                                          | M   | 2.1.10.0 |
| externalGroupIdentifier | Внешний идентификатор группы.                                                    | [ExternalGroupIdentifier](../entities/externalGroupIdentifier/) | M   | 2.1.10.0 |

### Пример тела запроса ###

```json
{
  "action":"create",
  "externalGroupIdentifier": {
    "identifier":"externalGroupIdentifier1@nbiot.ru"
  }
}
```

### Ответ ###

В ответе передается только статус запроса.