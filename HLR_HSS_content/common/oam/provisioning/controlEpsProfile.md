
Команда `ControlEpsProfile` позволяет изменять профиль EPS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ControlEpsProfile \
  -d '{
  "action": "<string>",
  "epsProfile": <EpsProfile>,
  "force": <bool> 
}'
```

### Поля тела запроса ###

| Поле       | Описание                                                                         | Тип                                   | O/M |
|------------|----------------------------------------------------------------------------------|---------------------------------------|-----|
| action     | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                | M   |
| epsProfile | Профиль в сетях EPS.                                                             | [EpsProfile](../entities/epsProfile/) | M   |
| force      | Флаг принудительного удаления при наличии связи с профилем абонента.             | bool                                  | O   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "epsProfile": {
    "eps_context_id": 1,
    "service_selection": "test.protei.ru",
    "vplmnDynamicAddressAllowed": 1,
    "qosClassId": 6,
    "allocateRetPriority": 4,
    "maxDl": 1000,
    "maxUl": 1000,
    "pdnType": 1,
    "preEmptionCapability": 1,
    "preEmptionVulnerability": 1
  }
}
```

### Ответ ###

В ответе передается только статус запроса.