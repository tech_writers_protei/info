Команда `GetCsiProfile` позволяет получать профиль CAMEL Subscription Information.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/GetCsiProfile \
  -d '{
  "typeId": <int>,
  "profileId": <int>
}'
----

== Поля тела запроса

[width="100%",cols="51%,37%,6%,6%",options="header",]
|===
|Поле |Описание |Тип |O/M
|link:#type-id-get-csi-profile[typeId] |Код типа CSI. |int |M
|profileId |Идентификатор профиля CSI. |int |M
|===

=== [[type-id-get-csi-profile]]Идентификаторы типа CSI

[cols=",,",options="header",]
|===
|N |Поле |Описание
|0 |O-CSI |Профиль O-CSI
|1 |T-CSI |Профиль T-CSI
|2 |SMS-CSI |Профиль SMS-CSI
|3 |GPRS-CSI |Профиль GPRS-CSI
|4 |M-CSI |Профиль M-CSI
|5 |D-CSI |Профиль D-CSI
|6 |TIF-CSI |Профиль TIF-CSI
|7 |SS-CSI |Профиль SS-CSI
|8 |USSD-CSI |Профиль USSD-CSI
|9 |O-IM-CSI |Профиль O-IM-CSI
|10 |VT-IM-CSI |Профиль VT-IM-CSI
|11 |D-IM-CSI |Профиль D-IM-CSI
|===

== Пример тела запроса

[source,json]
----
{
  "typeId": 0,
  "profileId": 1
}
----

== Ответ

В ответе передаются статус запроса и профиль CSI.

....
{
  "status": "<string>",
  "oCsi": <OCsi>,
  "tCsi": <TCsi>,
  "smsCsi": <SmsCsi>,
  "gprsCsi": <GprsCsi>,
  "mCsi": <MCsi>,
  "dCsi": <DCsi>,
  "ssCsi": <SsCsi>,
  "ussdCsi": <UssdCsi>,
  "oImCsi": <OImCsi>,
  "vtCsi": <VtCsi>,
  "dImCsi": <DImCsi>
}
....

== Поля ответа

[width="100%",cols="8%,64%,25%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|status |Статус запроса. |string |M
|oCsi |Профиль Originating-CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 0`. |link:../entities/oCsi/[OCsi] |С
|tCsi |Профиль Terminating CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 1`. |link:../entities/tCsi/[TCsi] |С
|smsCsi |Профиль SMS-CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 2`. |link:../entities/smsCsi/[SmsCsi] |С
|gprsCsi |Профиль GPRS-CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 3`. |link:../entities/gprsCsi/[GprsCsi] |С
|mCsi |Профиль Mobile Management Event CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 4`. |link:../entities/mCsi/[MCsi] |С
|dCsi |Профиль Dialled CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 5`. |link:../entities/dCsi/[DCsi] |С
|ssCsi |Профиль Supplementary Services CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 7`. |link:../entities/ssCsi/[SsCsi] |С
|ussdCsi |Профиль USSD CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 8`. |link:../entities/ussdCsi/[UssdCsi] |С
|oImCsi |Профиль Originating IP Multimedia CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 9`. |link:../entities/oCsi/[OCsi] |С
|vtCsi |Профиль VMSC Terminating CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 10`. |link:../entities/tCsi/[TCsi] |С
|dImCsi |Профиль Dialled IP Multimedia CSI.pass:q[<br>]*Примечание.* Активен при `typeId = 11`. |link:../entities/dCsi/[DCsi] |С
|===

== Пример ответа

[source,json]
----
{
  "status": "OK",
  "oCsi": {}
}
----
