
Команда `GetCapabilitiesSet` позволяет получать набор Capability.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetCapabilitiesSet \
  -d '{ "SetId": <int> }'
```

### Поля тела запроса ###

| Поле  | Описание                         | Тип | O/M |
|-------|----------------------------------|-----|-----|
| SetId | Идентификатор набора Capability. | int | M   |

### Пример тела запроса ###

```json
{ "SetId": 1 }
```

### Ответ ###

В ответе передаются статус запроса и набор CapabilitySet.

```
{
  "status": "<string>",
  "CapabilitiesSet": <CapabilitySet>
}
```

### Поля ответа ###

| Поле            | Описание             | Тип                                           | O/M |
|-----------------|----------------------|-----------------------------------------------|-----|
| status          | Статус запроса.      | string                                        | M   |
| CapabilitiesSet | Набор CapabilitySet. | [CapabilitySet](../entities/capabilitiesSet/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "CapabilitiesSet": {}
}
```