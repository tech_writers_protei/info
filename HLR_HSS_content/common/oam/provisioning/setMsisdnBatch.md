
Команда `SetMSISDNBatch` позволяет изменять номера MSISDN для нескольких абонентов.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SetMSISDNBatch \
  -d '{
  "requests": [ <SetMSISDN> ],
  "skipIfNotFound": <bool>
}'
```

### Поля тела запроса ###

| Поле           | Описание                                      | Тип                          | O/M |
|----------------|-----------------------------------------------|------------------------------|-----|
| requests       | Запрос на изменение номера MSISDN.            | [[SetMSISDN](../setMsisdn/)] | M   |
| skipIfNotFound | Флаг пропуска не существующих номеров MSISDN. | bool                         | M   |

### Пример тела запроса ###

```json
{
  "requests": [
    {
      "imsi": "250010000002",
      "msisdn": "79100000002"
    }
  ],
  "skipIfNotFound": true
}
```

### Ответ ###

В ответе передается только статус запроса.