[[ChangeIMSICampaign]]
= ChangeIMSI

Команда `ChangeIMSI` позволяет изменять набор номеров IMSI из файла.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberServiceCampaign/ChangeIMSI \
  -H 'Content-Type: text/csv' \
  -d @<path_to_file>.csv
----

В теле запроса передается файл формата CSV.

Формат файла:

[source,csv]
----
imsi;msisdn;new_imsi;new_status;delete_old_imsi;
----

== Поля файла

[cols="2,9,2,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|imsi |Текущий номер IMSI. |string |О |2.0.56.1
|msisdn |Текущий номер MSISDN. |string |О |2.0.56.1
|new_imsi |Новый номер IMSI. |string |M |2.0.56.1
|<<new-status-change-imsi,new_status>> |Код состояния абонента. |int |O |2.0.56.1
|delete_old_imsi |Флаг удаления текущей карты. |bool |O |2.0.56.1
|===

=== [[new-status-change-imsi]]Коды состояния абонента

[width="100%",cols="5%,30%,65%",options="header",]
|===
|N |Поле |Описание
|0 |Not provisioned |Карта не привязана к номеру MSISDN
|1 |Provisioned/locked |Карта привязана к номеру MSISDN и заблокирована
|2 |Provisioned/unlocked |В эксплуатации
|3 |Provisioned/suspended |Обслуживание приостановлено
|4 |Terminated |Абонент удален
|===

.Пример файла формата CSV
[source,csv]
----
250000000000001;;250000000000002;79000000001;250000000000001;
----

== Ответ

В ответе передаются статус запроса и параметры операции.

....
{
  "status": "<string>",
  "campaignCounter": <CampaignCounter>,
  "campaignStatus": "<string>",
  "campaignType": "<string>",
  "id": <int>,
  "creationDate": "<datetime>",
  "startDate": "<datetime>",
   "startDate": "<datetime>"
}
....

== Поля ответа

[cols="2,9,2,1,1",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|status |Статус запроса. |string |M |
|campaignCounter |Счетчик операций. |<<oam/provisioning/entities/campaignCounter.adoc#CampaignCounter,CampaignCounter>> |M |2.0.56.1
|<<campaign-status-change-imsi-campaign,campaignStatus>> |Статус операции. |string |M |2.0.56.1
|<<campaign-type-change-imsi-campaign,campaignType>> |Тип операции. Должно быть `CHANGE_IMSI`. |string |M |2.0.56.1
|id |Идентификатор операции. |int |M |2.0.56.1
|creationDate |Дата создания операции. |datetime |О |2.0.56.1
|startDate |Дата начала выполнения операции. |datetime |О |2.0.56.1
|finishDate |Дата завершения выполнения операции. |datetime |О |2.0.56.1
|===

=== [[campaign-status-change-imsi-campaign]]Состояния операций

[cols="1,4",options="header",]
|===
|Поле |Описание
|NEW |Операция еще не начата
|IN_PROGRESS |Операция в процессе выполнения
|ERROR |Во время выполнения операции произошла ошибка
|PARTLY_SUCCEED |Операция выполнена частично
|SUCCEED |Операция успешно выполнена
|CANCELED |Операция отменена
|===

=== [[campaign-type-change-imsi-campaign]]Типы операций

[cols=",",options="header",]
|===
|Поле |Описание
|ADD_SUBSCRIBER_PROFILE |Добавление профиля абонента
|CHANGE_IMSI |Изменение номера IMSI
|CHANGE_MSISDN |Изменение номера MSISDN
|CHANGE_PROFILE |Изменение профиля
|DELETE_SUBSCRIBER |Удаление абонента
|DELETE_SUBSCRIBER_PROFILE |Удаление профиля абонента
|SIMCARDS_INFORMATION_LOAD |Загрузка информации на SIM-карту
|EXPORT_SUBSCRIBER_PROFILE |Экспорт профиля абонента
|===

.Пример ответа
[source,json]
----
{
  "status": "OK",
  "campaignCounter": {
    "total": 10,
    "success": 0,
    "error": 0
  },
  "campaignStatus": "IN_PROGRESS",
  "campaignType": "CHANGE_IMSI",
  "id": 49,
  "creationDate": "2022-07-28T12:50:37",
  "startDate": "2022-07-28T12:50:37"
}
----
