
Команда `GetAllBlackLists` позволяет получать все черные списки в системе.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetAllBlackLists
```

### Ответ ###

В ответе передаются статус запроса и параметры черных списков.

```
{
  "status": "<string>",
  "blackList": [ <BlackList> ]
}
```

### Поля ответа ###

| Поле      | Описание                 | Тип                                   | O/M |
|-----------|--------------------------|---------------------------------------|-----|
| status    | Статус запроса.          | string                                | M   |
| blackList | Перечень черных списков. | [[BlackList](../entities/blackList/)] | M   |