
Команда `ControlCsiProfile` позволяет изменять профиль CAMEL Subscription Information.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ControlCsiProfile \
  -d '{
  "action": "<string>",
  "oCsi": <oCsi>,
  "tCsi": <tCsi>,
  "smsCsi": <smsCsi>,
  "gprsCsi": <gprsCsi>,
  "mCsi": <mCsi>,
  "dCsi": <dCsi>,
  "ssCsi": <ssCsi>,
  "ussdCsi": <ussdCsi>,
  "oImCsi": <oImCsi>,
  "vtImCsi": <vtImCsi>,
  "dImCsi": <dImCsi>
}'
```

### Поля тела запроса ###

| Поле                                              | Описание                                                                         | Тип                             | O/M |
|---------------------------------------------------|----------------------------------------------------------------------------------|---------------------------------|-----|
| action                                            | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                          | M   |
| [[oCsi-control-csi-profile]]oCsi       | Профиль Originating CSI.                                                         | [OCsi](../entities/oCsi/)       | С   |
| [[tCsi-control-csi-profile]]tCsi       | Профиль Terminating CSI.                                                         | [TCsi](../entities/tCsi/)       | С   |
| [[smsCsi-control-csi-profile]]smsCsi   | Профиль SMS-CSI.                                                                 | [SmsCsi](../entities/smsCsi/)   | С   |
| [[gprsCsi-control-csi-profile]]gprsCsi | Профиль GPRS-CSI.                                                                | [GprsCsi](../entities/gprsCsi/) | С   |
| [[mCsi-control-csi-profile]]mCsi       | Профиль Mobile Management Event CSI.                                             | [MCsi](../entities/mCsi/)       | С   |
| [[dCsi-control-csi-profile]]dCsi       | Профиль Dialled CSI.                                                             | [DCsi](../entities/dCsi/)       | С   |
| [[ssCsi-control-csi-profile]]ssCsi     | Профиль Supplementary Services CSI.                                              | [SsCsi](../entities/ssCsi/)     | С   |
| [[ussdCsi-control-csi-profile]]ussdCsi | Профиль USSD CSI.                                                                | [UssdCsi](../entities/ussdCsi/) | С   |
| [[oImCsi-control-csi-profile]]oImCsi   | Профиль Originating IP Multimedia CSI.                                           | [OCsi](../entities/oCsi/)       | С   |
| [[vtCsi-control-csi-profile]]vtCsi     | Профиль VMSC Terminating CSI.                                                    | [TCsi](../entities/tCsi/)       | С   |
| [[dImCsi-control-csi-profile]]dImCsi   | Профиль Dialled IP Multimedia CSI.                                               | [DCsi](../entities/dCsi/)       | С   |
| force                                             | Флаг принудительного удаления при наличии связи с профилем абонента.             | bool                            | O   |

**Примечание.** Задается только один из параметров [oCsi](#oCsi-control-csi-profile), [tCsi](#tCsi-control-csi-profile), [smsCsi](#smsCsi-control-csi-profile), [gprsCsi](#gprsCsi-control-csi-profile), [mCsi](#mCsi-control-csi-profile), [dCsi](#dCsi-control-csi-profile), [ssCsi](#ssCsi-control-csi-profile), [ussdCsi](#ussdCsi-control-csi-profile), [oImCsi](#oImCsi-control-csi-profile), [vtCsi](#vtCsi-control-csi-profile) и [dImCsi](#dImCsi-control-csi-profile).

### Пример тела запроса ###

```json
{
  "action": "create",
  "oCsi": {
    "id": 1,
    "camelCapabilityHandling": 3,
    "csiActive": 1,
    "csiTdps": [
      {
        "tdpId": 1,
        "serviceKey": 1,
        "gsmScfAddress": "78924813183138",
        "defaultHandling": 1
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.