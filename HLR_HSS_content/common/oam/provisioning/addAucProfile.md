
Команда `AddAucProfile` позволяет добавить профиль AuC.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/AddAucProfile \
  -d '{
  "imsi": "<string>",
  "iccid": "<string>",
  "ki": "<string>",
  "algorithm": <int>,
  "opc": "<string>",
  "opId": <int>,
  "tkId": <int>,
  "opcTkId": <int>,
  "cId": <int>,
  "rId": <int>,
  "hsmId": <int>,
  "amfUmts": "<string>",
  "amfLte": "<string>",
  "amfIms": "<string>",
  "resSize": <int>,
  "ckSize": <int>,
  "ikSize ": <int>,
  "macSize": <int>,
  "keccakIterations": <int>,
  "sqnType": <int>
}'
```

### Поля тела запроса ###

| Поле                    | Описание                                                                                                                                                                         | Тип    | O/M |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|-----|
| imsi                    | Номер IMSI абонента.                                                                                                                                                             | string | M   |
| iccid                   | Идентификатор смарт-карты абонента.                                                                                                                                              | string | О   |
| ki                      | Секретный ключ Ki для аутентификации.                                                                                                                                            | hex    | M   |
| [algorithm](#algorithm) | Код алгоритма аутентификации.                                                                                                                                                    | int    | M   |
| opc                     | Значение поля конфигурации алгоритма, задаваемого оператором. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf). | string | С   |
| opId                    | Идентификатор ключа оператора для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).         | int    | O   |
| tkId                    | Идентификатор транспортного ключа для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).     | int    | O   |
| opcTkId                 | Идентификатор транспортного ключа OPC. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).                        | int    | O   |
| cId                     | Идентификатор аддитивной постоянной для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).   | int    | O   |
| rId                     | Идентификатор постоянной поворота для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).     | int    | O   |
| hsmId                   | Идентификатор модуля HSM.                                                                                                                                                        | int    | O   |
| amfUmts                 | Поле управления аутентификацией в сетях UMTS.                                                                                                                                    | string | O   |
| amfLte                  | Поле управления аутентификацией в сетях LTE.                                                                                                                                     | string | O   |
| amfIms                  | Поле управления аутентификацией в сетях IMS.                                                                                                                                     | string | O   |
| resSize                 | Длина ответа пользователя для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).             | int    | O   |
| ckSize                  | Длина ключа конфиденциальности для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).        | int    | O   |
| ikSize                  | Длина ключа целостности для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).               | int    | O   |
| macSize                 | Код сетевой аутентификации для алгоритма MILENAGE. См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).            | int    | O   |
| keccakIterations        | Количество раундов алгоритма хэширования SHA-3 (Keccak). См.&nbsp;[3GPP&nbsp;TS&nbsp;35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf).      | int    | O   |
| sqnType                 | Код способа выбора SQN.pass:q[\<br\>]`0` -- не зависимый от времени; `1` -- основанный на времени.                                                                                         | int    | O   |

#### [[algorithm]]Описание кодов алгоритмов аутентификации

| N | Поле       | Описание                                                                                                                   |
|---|------------|----------------------------------------------------------------------------------------------------------------------------|
| 1 | COMP128 v1 |                                                                                                                            |
| 2 | COMP128 v2 |                                                                                                                            |
| 3 | COMP128 v3 |                                                                                                                            |
| 4 | MILENAGE   | Cм. [3GPP TS 35.205](https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf)          |
| 6 | XOR        | Cм. [3GPP TS 34.108, 8.1.2.1](https://www.etsi.org/deliver/etsi_ts/134100_134199/134108/15.02.00_60/ts_134108v150200p.pdf) |
| 7 | TUAK       | Cм. [3GPP TS 35.231](https://www.etsi.org/deliver/etsi_ts/135200_135299/135231/18.00.00_60/ts_135231v180000p.pdf)          |
| 8 | S3G128     | Cм. [ГОСТ 34.10–2018](https://protect.gost.ru/v.aspx?control=8&id=224247)                                                  |
| 9 | S3G256     | Cм. [ГОСТ 34.10–2018](https://protect.gost.ru/v.aspx?control=8&id=224247)                                                  |

### Пример тела запроса ###

```json
{
  "imsi": "001010000000001",
  "iccid": "0010100000000010000",
  "ki": "0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f0f",
  "algorithm": 4,
  "opId": 1,
  "tkId": 1,
  "rId": 1,
  "cId": 1,
  "amfUmts": "0000",
  "amfLte": "8000",
  "amfIms": "8001",
  "sqnType": 0
}
```

### Ответ ###

В ответе передается только статус запроса.