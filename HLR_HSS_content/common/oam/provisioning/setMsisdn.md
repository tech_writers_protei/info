
Команда `SetMSISDN` позволяет изменять номер MSISDN абонента.

При наличии активной регистрации на VLR, SGSN или MME она прекращается запросом MAP-CLR или Diameter S6: CLR.

При наличии активной регистрации на VLR, SGSN или MME новый номер передается с помощью запросов MAP-ISD или Diameter S6: IDR.

**Примечание.** То же действие можно осуществить с помощью команды [ProfileControl](../../ProfileService/profileControl/).

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/SetMSISDN \
  -d '{
  "imsi": "<string>",
  "msisdn": "<string>",
  "status": "<string>"
}'
```

### Поля тела запроса ###

| Поле                         | Описание                | Тип    | O/M |
|------------------------------|-------------------------|--------|-----|
| imsi                         | Текущий номер IMSI.     | string | M   |
| msisdn                       | Новый номер MSISDN.     | string | M   |
| [status](#status-set-msisdn) | Код состояния абонента. | int    | О   |

#### [[status-set-msisdn]]Коды состояния абонента

| N | Поле                  | Описание                                        |
|---|-----------------------|-------------------------------------------------|
| 0 | Not provisioned       | Карта не привязана к номеру MSISDN              |
| 1 | Provisioned/locked    | Карта привязана к номеру MSISDN и заблокирована |
| 2 | Provisioned/unlocked  | В эксплуатации                                  |
| 3 | Provisioned/suspended | Обслуживание приостановлено                     |
| 4 | Terminated            | Абонент удален                                  |

### Пример тела запроса ###

```json
{
  "imsi": "250010000001",
  "msisdn": "79000000002"
}
```

### Ответ ###

В ответе передается только статус запроса.