
Команда `GetProperties` позволяет получать настройки базы данных, Core и API.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/InternalService/GetProperties
```

### Ответ ###

В ответе передаются параметры настройки базы данных, HLR.Core и API.

```
{
  "db": <DbProperties>,
  "core": <CoreProperties>,
  "api": <ApiProperties>
}
```

### Поля ответа ###

| Поле | Описание                         | Тип                                           | O/M |
|------|----------------------------------|-----------------------------------------------|-----|
| db   | Параметры настройки базы данных. | [DbProperties](../entities/dbProperties/)     | M   |
| core | Параметры настройки HLR.Core.    | [CoreProperties](../entities/coreProperties/) | M   |
| api  | Параметры настройки API.         | [ApiProperties](../entities/apiProperties/)   | M   |

### Пример ответа ###

```json
{
  "db": {
    "user": "hlr",
    "password": "bGsnHXypyew=",
    "url": "jdbc:mysql://localhost:3306/HLR"
  },
  "core": {
    "hlr": true,
    "hss": true,
    "ims": false,
    "lcs": false,
    "version": "2.0.74.5.1245"
  },
  "api": {
    "version": "2.0.51.0"
  }
}
```