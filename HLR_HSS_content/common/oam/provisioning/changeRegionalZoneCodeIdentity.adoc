Команда `ChangeRegionalZoneCodeIdentity` позволяет изменять код региональной зоны.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/AddAndActivateEmptySubscribers \
  -d '{
  "action": "<string>",
  "regional": <Regional>
}
----

== Поля тела запроса

[width="100%",cols="9%,62%,26%,3%",options="header",]
|===
|Поле |Описание |Тип |O/M
|action |Тип действия.pass:q[<br>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. |string |M
|regional |Параметры региональной зоны. |link:../entities/regional/[Regional] |M
|===

== Пример тела запроса

[source,json]
----
{
  "action": "create",
  "regional": {
    "name": "rus",
    "plmns": [
      {
        "plmn": "25001",
        "zoneCodes": [
          { "zoneCode": 1 },
          { "zoneCode": 2 }
        ]
      }
    ],
    "ccndcs": [
      {
        "ccndc": "7901",
        "zoneCodes": [
          { "zoneCode": 2 },
          { "zoneCode": 3 }
        ]
      }
    ]
  }
}
----

== Ответ

В ответе передается только статус запроса.
