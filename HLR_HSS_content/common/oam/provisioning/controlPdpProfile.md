
Команда `ProfileService/ControlPdpProfile` позволяет изменять профиль PDP.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ControlPdpProfile \
  -d '{
  "action": "<string>",
  "pdpProfile": <PdpProfile>,
  "force": <bool>
}'
```

### Поля тела запроса ###

| Поле       | Описание                                                                         | Тип                                   | O/M |
|------------|----------------------------------------------------------------------------------|---------------------------------------|-----|
| action     | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                | M   |
| pdpProfile | Профиль PDP.                                                                     | [PdpProfile](../entities/pdpProfile/) | M   |
| force      | Флаг принудительного удаления при наличии связи с профилем абонента.             | bool                                  | O   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "pdpProfile": {
    "context-id": 1,
    "apn": "test.protei.ru",
    "vplmnAddressAllowed": 1,
    "qosSubscribed": "123456",
    "extQosSubscribed": "123456789",
    "pdpChargingCharacteristics": "0800"
  }
}
```

### Ответ ###

В ответе передается только статус запроса.