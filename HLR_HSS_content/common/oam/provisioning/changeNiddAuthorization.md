
Команда `ChangeNiddAuthorization` позволяет управлять авторизацией NIDD.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeNiddAuthorization \
  -d '{ "niddAuthorizations": [ <NiddAuthorization> ] }'
```

### Поля тела запроса ###

| Поле               | Описание                   | Тип                                                   | O/M | Версия   |
|--------------------|----------------------------|-------------------------------------------------------|-----|----------|
| niddAuthorizations | Перечень авторизаций NIDD. | [[NiddAuthorization](../entities/niddAuthorization/)] | M   | 2.1.10.0 |

### Пример тела запроса ###

```json
{
  "niddAuthorizations": [
    {
      "id": 1,
      "epsContextId": 1,
      "scefId": 1,
      "grantedValidityTime": 1710235457
    },
    {
      "id": 1,
      "epsContextId": 2,
      "scefId": 1,
      "grantedValidityTime": 1710235457,
      "delete": true
    }
  ]
}
```

### Ответ ###

В ответе передается только статус запроса.