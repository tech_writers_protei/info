
Команда `ChangeQosProfile` позволяет изменять профиль QoS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeQosProfile \
  -d '{
  "action": "<string>",
  "qosEps": <qosEps>,
  "qosGprs": <qosGprs>
}'
```

### Поля тела запроса ###

| Поле    | Описание                                                                         | Тип                             | O/M |
|---------|----------------------------------------------------------------------------------|---------------------------------|-----|
| action  | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                          | M   |
| qosGprs | Профиль QoS GPRS.                                                                | [QosGprs](../entities/qosGprs/) | C   |
| qosEps  | Профиль QoS EPS.                                                                 | [QosEps](../entities/qosEps/)   | C   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "qosEps": {
    "qosClassId": 6,
    "allocateRetPriority": 4,
    "maxDl": 1000,
    "maxUl": 1000,
    "preEmptionCapability": 1,
    "preEmptionVulnerability": 1
  }
}
```

### Ответ ###

В ответе передается только статус запроса.