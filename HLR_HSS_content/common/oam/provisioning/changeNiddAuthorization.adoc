Команда `ChangeNiddAuthorization` позволяет управлять авторизацией NIDD.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeNiddAuthorization \
  -d '{ "niddAuthorizations": [ <NiddAuthorization> ] }'
----

== Поля тела запроса

[width="100%",cols="19%,23%,46%,4%,8%",options="header",]
|===
|Поле |Описание |Тип |O/M |Версия
|niddAuthorizations |Перечень авторизаций NIDD. |[link:../entities/niddAuthorization/[NiddAuthorization]] |M |2.1.10.0
|===

== Пример тела запроса

[source,json]
----
{
  "niddAuthorizations": [
    {
      "id": 1,
      "epsContextId": 1,
      "scefId": 1,
      "grantedValidityTime": 1710235457
    },
    {
      "id": 1,
      "epsContextId": 2,
      "scefId": 1,
      "grantedValidityTime": 1710235457,
      "delete": true
    }
  ]
}
----

== Ответ

В ответе передается только статус запроса.
