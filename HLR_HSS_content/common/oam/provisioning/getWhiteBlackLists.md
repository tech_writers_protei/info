
Команда `GetWhiteBlackLists` позволяет получать белые и черные списки.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetWhiteBlackLists \
  -d '{
  "type": "<string>",
  "name": "<string>"
}'
```

### Поля тела запроса ###

| Поле                                 | Описание                        | Тип    | O/M |
|--------------------------------------|---------------------------------|--------|-----|
| type                                 | Тип списка. `white` / `black`.  | string | M   |
| [[id-get-wb-lists]]id     | Идентификационный номер списка. | int    | C   |
| [[name-get-wb-lists]]name | Название списка.                | string | C   |

**Примечание.** Задается только один из параметров [id](#id-get-wb-lists) и [name](#name-get-wb-lists).

### Пример тела запроса ###

```json
{
  "type": "white",
  "name": "wl_rus"
}
```

### Ответ ###

В ответе передаются статус запроса и белый или черный список.

```
{
  "status": "<string>",
  "blackList": <BlackList>,
  "whiteList": <WhiteList>
}
```

### Поля ответа ###

| Поле      | Описание        | Тип                                 | O/M |
|-----------|-----------------|-------------------------------------|-----|
| status    | Статус запроса. | string                              | M   |
| blackList | Черный список.  | [BlackList](../entities/blackList/) | С   |
| whiteList | Белый список.   | [WhiteList](../entities/whiteList/) | С   |

### Пример ответа ###

```json
{
  "status": "OK",
  "blackList": {}
}
```