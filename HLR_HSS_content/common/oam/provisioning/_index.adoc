== Общая информация HTTP API

Все запросы обрабатываются в синхронном режиме. Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Все запросы и ответы должны быть представлены в формате JSON.

Все запросы и ответы JSON имеют вид согласно JSON-схемам, представленным ниже. В каждом запросе должна использоваться только одна операция.

Каждый JSON-запрос и ответ должен иметь кодировку UTF-8, чтобы поддерживать многоязычность.

Приложение должно использовать один адрес URL для отправки запросов PROTEI HLR/HSS:

....
http(s)://<host>:<port>/<web_hook>
....

[width="100%",cols="16%,61%,16%,7%",options="header",]
|===
|Поле |Описание |Тип |O/M
|host |IP-адрес или DNS-имя PROTEI HLR/HSS API. |string/ip |M
|port |Порт для установления соединения. |int |M
|web_hook |URL для отправки запроса. |string |M
|===

== Ответ от PROTEI HLR/HSS

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

[source,json]
----
{ "status": "<string>" }
----

== [[request_status]]Статус запроса

[width="100%",cols="6%,18%,76%",options="header",]
|===
|Код |Поле |Описание
|0 |SUCCESS |Действие выполнено
|201 |TEMPORARYERROR |Не описанная временная ошибка
|101 |INVPAYLOAD |Ошибка при попытке использования payload
|102 |INVEXTID |Ошибка при задании внешнего идентификатора
|103 |INVSCSID |Ошибка узла SCS
|104 |INVPERIOD |Ошибка срока действия
|105 |NOTAUTHORIZED |Узел SCS не имеет соответствующих прав для выполнения действия
|106 |SERVICEUNAVAILABLE |Указанная услуга не доступна
|107 |PERMANENTERROR |Постоянная ошибка
|108 |QUOTAEXCEEDED |Узел SCS исчерпал выделенную квоту
|109 |RATEEXCEEDED |Количество пользовательских запросов с узла SCS превысило установленное ограничение
|110 |REPLACEFAIL |Не удалось заменить триггер устройства
|111 |RECALLFAIL |Не удалось повторно вызвать триггер устройства
|112 |ORIGINALMESSAGESENT |Сообщение, которое должно быть повторно отправлено или изменено, уже отправлено
|===

== Запросы API

* link:./getStatsCampaign/[/CampaignService/GetStatsCampaign] -- получить статистики по операциям;
* link:./stopCampaign/[/CampaignService/StopCampaign] -- остановить операцию;
* link:./getCampaignStatDetail/[/CustomService/GetCampaignStatDetail] -- получить данные статистики операции;
* link:./getExportFile/[/CustomService/GetExportFile] -- получить файл с профилями абонентов;
* link:./getImsiMsisdnInfo/[/CustomService/GetImsiMsisdnInfo] -- получить cвязки IMSI-MISISDN;
* link:./getProfileBatchFile/[/CustomService/GetProfileBatchFile] -- получить списка профилей абонентов в файл;
* link:./getJdbcConfig/[/InternalService/GetJdbcConfig] -- получить параметры подключения к базе данных;
* link:./getProperties/[/InternalService/GetProperties] -- получить настройки базы данных, Core и API;
* link:./addAucProfile/[/ProfileService/AddAucProfile] -- добавить профиль AuC;
* link:./addAucProfileBatch/[/ProfileService/AddAucProfileBatch] -- добавить профили AuC;
* link:./changeAucConfig/[/ProfileService/ChangeAucConfig] -- изменить параметры центра аутентификации AuC;
* link:./changeAucProfile/[/ProfileService/ChangeAucProfile] -- изменить профиль AuC;
* link:./changeCapabilitiesSet/[/ProfileService/ChangeCapabilitiesSet] -- изменить набор возможностей CapabilitiesSet;
* link:./changeExternalGroupIdentifier/[/ProfileService/ChangeExternalGroupIdentifier] -- изменить внешний идентификатор группы;
* link:./changeGroup/[/ProfileService/ChangeGroup] -- изменить группу абонента;
* link:./changeProfileTemplate/[/ProfileService/ChangeProfileTemplate] -- изменить шаблон профиля;
* link:./changeQosProfile/[/ProfileService/ChangeQosProfile] -- изменить профиль QoS;
* link:./changeScef/[/ProfileService/ChangeScef] -- изменить параметры узла SCEF;
* link:./changeSnssai/[/ProfileService/ChangeSnssai] -- изменить данные Single Network Slice Selection Assistance Information;
* link:./changeWhiteBlackLists/[/ProfileService/ChangeWhiteBlackLists] -- изменить белые/черные списки;
* link:./controlAs/[/ProfileService/ControlAs] -- управление сервером приложений;
* link:./controlConfig/[/ProfileService/ControlConfig] -- изменить конфигурацию базы данных;
* link:./controlCsiProfile/[/ProfileService/ControlCsiProfile] -- изменить CAMEL-профиль;
* link:./controlEpsProfile/[/ProfileService/ControlEpsProfile] -- изменить профиль EPS;
* link:./controlPdpProfile/[/ProfileService/ControlPdpProfile] -- изменить профиль PDP;
* link:./controlPrefferedScscfSet/[/ProfileService/ControlPrefferedScscfSet] -- изменить набор предпочитаемых узлов S-CSCF;
* link:./controlRoamingAgreement/[/ProfileService/ControlRoamingAgreement] -- изменить роуминговое соглашение о поколении CAMEL;
* link:./controlUser/[/ProfileService/ControlUser] -- изменить параметры пользователя системы;
* link:./getAllBlackLists/[/ProfileService/GetAllBlackLists] -- получить все черные списки;
* link:./getAllWhiteLists/[/ProfileService/GetAllWhiteLists] -- получить все белые списки;
* link:./getAs/[/ProfileService/GetAs] -- получить параметры сервера приложений;
* link:./getAucConfig/[/ProfileService/GetAucConfig] -- получить параметры центра аутентификации AuC;
* link:./getAucProfile/[/ProfileService/GetAucProfile] -- получить профиля AuC;
* link:./getAucProfileBatch/[/ProfileService/GetAucProfileBatch] -- получить перечень карт согласно фильтру;
* link:./getAudit/[/ProfileService/GetAudit] -- получить информацию о действиях пользователей;
* link:./getCapabilitiesSet/[/ProfileService/GetCapabilitiesSet] -- получить набор возможностей Capability;
* link:./getChargingInformation/[/ProfileService/GetChargingInformation] -- получить информацию о тарификации;
* link:./getConfig/[/ProfileService/GetConfig] -- получить конфигурацию базы данных;
* link:./getCsiProfile/[/ProfileService/GetCsiProfile] -- получить профиль CAMEL;
* link:./getEpsProfile/[/ProfileService/GetEpsProfile] -- получить профиля EPS;
* link:./getGroup/[/ProfileService/GetGroup] -- получить группу;
* link:./getGroups/[/ProfileService/GetGroups] -- получить группы;
* link:./getImsSubscription/[/ProfileService/GetImsSubscription] -- получить подписку IMS;
* link:./getLcsProfile/[/ProfileService/GetLcsProfile] -- получить профиль услуг LCS;
* link:./getPdpProfile/[/ProfileService/GetPdpProfile] -- получить профиль PDP;
* link:./getPrefferedScscfSet/[/ProfileService/GetPrefferedScscfSet] -- получить набор предпочитаемых S-CSCF;
* link:./getProfile/[/ProfileService/GetProfile] -- получить профиль абонента;
* link:./getProfileBatchFile/[/ProfileService/GetProfileBatchFile] -- получить списка профилей абонентов по фильтру;
* link:./getProfilesIdentity/[/ProfileService/GetProfilesIdentity] -- получить список идентификаторов общих сущностей;
* link:./getProfilesValue/[/ProfileService/GetProfilesValue] -- получить список значений общих сущностей;
* link:./getProfileTemplate/[/ProfileService/GetProfileTemplate] -- получить шаблона профиля;
* link:./getQosProfile/[/ProfileService/GetQosProfile] -- получить профиль QoS;
* link:./getRegionalZoneCodeIdentity/[/ProfileService/GetRegionalZoneCodeIdentity] -- получить код региональной зоны;
* link:./getRoamingAgreement/[/ProfileService/GetRoamingAgreement] -- получить роуминговые соглашения о поколении CAMEL;
* link:./getScef/[/ProfileService/GetScef] -- получить параметры узла SCEF;
* link:./getUser/[/ProfileService/GetUser] -- получить параметры пользователя системы;
* link:./getUserBatch/[/ProfileService/GetUserBatch] -- получить список пользователей по фильтру;
* link:./getWhiteBlackLists/[/ProfileService/GetWhiteBlackLists] -- получить белых/черных списков;
* link:./profileControl/[/ProfileService/ProfileControl] -- изменить профиль в базе данных HLR/HSS;
* link:./validateAucConfig/[/ProfileService/ValidateAucConfig] -- проверить параметры AuC;
* link:./addAucProfileCampaign/[/ProfileServiceCampaign/AddAucProfile] -- добавить набор профилей AUC в базу данных HLR/HSS из файла;
* link:./profileControlCampaign/[/ProfileServiceCampaign/ProfileControl] -- изменить AuC-профили из файла;
* link:./addAndActivateEmptySubscribers/[/SubscriberService/AddAndActivateEmptySubscribers] -- создать и активировать пустые профили абонентов;
* link:./addSubscriber/[/SubscriberService/AddSubscriber] -- создать профиль абонента;
* link:./addSubscriberBatch/[/SubscriberService/AddSubscriberBatch] -- добавить список абонентов;
* link:./addSubscriberProfile/[/SubscriberService/AddSubscriberProfile] -- добавить профиля абонента к ранее загруженной карте;
* link:./ChangeImsi/[/SubscriberService/ChangeIMSI] -- сменить номер IMSI;
* link:./changeImsSubscription/[/SubscriberService/ChangeImsSubscription] -- изменить IMS-подписку;
* link:./changeLcsProfile/[/SubscriberService/ChangeLcsProfile] -- изменить профилем LCS;
* link:./changeNiddAuthorization/[/SubscriberService/ChangeNiddAuthorization] -- изменить авторизацию NIDD;
* link:./changeRegionalZoneCodeIdentity/[/SubscriberService/ChangeRegionalZoneCodeIdentity] -- изменить код региональной зоны;
* link:./changeSifcSet/[/SubscriberService/ChangeSifcSet] -- изменить набор общих iFC;
* link:./changeStatus/[/SubscriberService/ChangeStatus] -- изменить статус абонента;
* link:./controlChargingInformation/[/SubscriberService/ControlChargingInformation] -- изменить настройки тарификации;
* link:./deleteSubscriber/[/SubscriberService/DeleteSubscriber] -- удалить профиль абонента из базы данных HLR/HSS с параметрами аутентификации карты;
* link:./deleteSubscriberProfile/[/SubscriberService/DeleteSubscriberProfile] -- удалить профиля абонента из базы данных HLR/HSS без параметров аутентификации карты;
* link:./getSifcSet/[/SubscriberService/GetSifcSet] -- получить набор общих iFC;
* link:./getUserLocation/[/SubscriberService/GetUserLocation] -- получить местоположение абонента;
* link:./sendAlertSc/[/SubscriberService/SendAlertSc] -- уведомить обслуживающий центр об активности абонента в сети;
* link:./sendCancelLocation/[/SubscriberService/SendCancelLocation] -- отменить регистрацию в сетях 2G/3G/4G;
* link:./sendProvideRoamingNumber/[/SubscriberService/SendProvideRoamingNumber] -- получить роуминговый номер абонента;
* link:./sendProvideSubscriberInformation/[/SubscriberService/sendProvideSubscriberInformation] -- получить информацию о регистрации абонента;
* link:./sendRegistrationTermination/[/SubscriberService/SendRegistrationTermination] -- отменить регистрацию в сетях IMS;
* link:./sendReset/[/SubscriberService/SendReset] -- оповестить узел VLR/SGSN/MME об аварийной перезагрузке;
* link:./setMsisdn/[/SubscriberService/SetMSISDN] -- изменить номер MSISDN абонента;
* link:./setMsisdnNBatch/[/SubscriberService/SetMSISDNBatch] -- изменить номера MSISDN абонентов;
* link:./addSubscriberProfileCampaign/[/SubscriberServiceCampaign/AddSubscriberProfile] -- добавить абонентские профили из файла;
* link:./changeImsiCampaign/[/SubscriberServiceCampaign/ChangeIMSI] -- изменить набора номеров IMSI из файла;
* link:./deleteSubscriberCampaign/[/SubscriberServiceCampaign/DeleteSubscriber] -- удалить абонентов, указанных в файле;
* link:./deleteSubscriberProfileCampaign/[/SubscriberServiceCampaign/DeleteSubscriberProfile] -- удалить набор профилей абонентов без удаления карт из файла;
* link:./exportSubscriberProfile/[/SubscriberServiceCampaign/ExportSubscriberProfile] -- экспортировать профили абонентов в файл;
* link:./setMsisdnCampaign/[/SubscriberServiceCampaign/SetMSISDN] -- смена набора номеров MSISDN из файла.
