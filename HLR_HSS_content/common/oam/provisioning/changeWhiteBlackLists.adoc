Команда `ChangeWhiteBlackLists` позволяет изменять белые/черные списки.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ChangeWhiteBlackLists \
  -d '{ "whiteLists": [<list>] }'
----

== Поля тела запроса

[width="100%",cols="16%,31%,47%,6%",options="header",]
|===
|Поле |Описание |Тип |O/M
|whiteLists |Перечень белых списков. |[link:../entities/whiteList/[WhiteList]] |O
|blackLists |Перечень черных списков. |[link:../entities/blackList/[BlackList]] |O
|===

== Пример тела запроса

[source,json]
----
{
  "whiteLists": [
    {
      "action": "create",
      "id": 1,
      "name": "wl_rus",
      "vlr_masks": [
        {
          "mask": "7911",
          "country": "rus",
          "network": "mts"
        },
        {
          "mask": "7921",
          "country": "rus",
          "network": "megafon"
        }
      ],
      "plmn_mask": [
        {
          "mask": "25001",
          "country": "rus",
          "network": "mts"
        },
        {
          "mask": "25002",
          "country": "rus",
          "network": "megafon"
        }
      ]
    }
  ]
}
----

== Ответ

В ответе передается только статус запроса.
