
Команда `ChangeCapabilitiesSet` позволяет изменить набор Capability.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/ChangeCapabilitiesSet \
  -d '{
  "Action": "<string>",
  "CapabilitiesSet": <CapabilitySet>
}'
```

### Поля тела запроса ###

| Поле            | Описание                                                                         | Тип                                           | O/M |
|-----------------|----------------------------------------------------------------------------------|-----------------------------------------------|-----|
| Action          | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                        | M   |
| CapabilitiesSet | Набор Capability.                                                                | [CapabilitySet](../entities/capabilitiesSet/) | M   |

### Пример тела запроса ###

```json
{
  "Action": "create",
  "CapabilitiesSet": {
    "SetId": 1,
    "Capabilities": [
      {
        "Code": 1,
        "Mandatory": 1
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.