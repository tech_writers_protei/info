
Команда `GetConfig` позволяет получать конфигурацию базы данных.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetConfig
```

### Ответ ###

В ответе передаются статус запроса и параметры настройки базы данных.

```
{
  "status": "<string>",
  "config": <DbConfig>
}
```

### Поля ответа ###

| Поле   | Описание                  | Тип                               | O/M |
|--------|---------------------------|-----------------------------------|-----|
| status | Статус запроса.           | string                            | M   |
| config | Конфигурация базы данных. | [DbConfig](../entities/dbConfig/) | M   |

### Пример ответа ###

```json
{
  "status": "OK",
  "config": {
    "mwdMaxCount": 1,
    "maxForward": 1,
    "sqnStart": 1,
    "sqnEnd": 1000,
    "sqnStep": 1,
    "defaultForwardingOptions": 1,
    "defaultForwardingType": 1,
    "activeWhiteList": 1
  }
}
```