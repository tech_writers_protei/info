
Команда `GetCampaignStatDetail` позволяет получать данные статистики операции.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/CustomService/GetCampaignStatDetail \
  -d '{ "id": <int> }'
```

### Поля тела запроса ###

| Поле | Описание                | Тип | O/M |
|------|-------------------------|-----|-----|
| id   | Идентификатор операции. | int | M   |

### Пример тела запроса ###

```json
{ "id": 1 }
```

### Ответ ###

В ответе передается данные статистики операций.

Формат файла:

```csv
DT;Operation;Status;ExecutionTime;SentToHlr;UserHost;UserName;MSISDN;IMSI;Params;
```

### Поля ответа ###

| Поле          | Описание                                    | Тип      | O/M |
|---------------|---------------------------------------------|----------|-----|
| DT            | Дата и время выполнения действия.           | datetime | M   |
| Operation     | Название действия.                          | string   | M   |
| Status        | Состояние выполнения действия.              | string   | M   |
| ExecutionTime | Время выполнения действия, в миллисекундах. | units    | M   |
| SentToHlr     | Флаг запуска операции на узле HLR.Core.     | bool     | M   |
| UserHost      | Адрес хоста пользователя.                   | ip       | M   |
| UserName      | Логин пользователя.                         | string   | M   |
| MSISDN        | Номер MSISDN абонента.                      | string   | C   |
| IMSI          | Номер IMSI абонента.                        | string   | C   |
| Params        | Измененные параметры абонента.              | [object] | O   |

### Пример ответа ###

```csv
DT;Operation;Status;ExecutionTime;SentToHlr;UserHost;UserName;MSISDN;IMSI;Params;
2022-07-28 15:50:37.812;ADD_AUC_CAMPAIGN_TASK;OK;10ms;false;192.168.101.60;System;;250010000000006;;
2022-07-28 15:50:37.810;ADD_AUC_CAMPAIGN_TASK;OK;16ms;false;192.168.101.60;System;;250010000000004;;
```