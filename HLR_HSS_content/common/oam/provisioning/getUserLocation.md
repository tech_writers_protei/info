
Команда `GetUserLocation` позволяет получать местоположения абонента.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/GetUserLocation \
  -d '{
  "imsi": "<string>",
  "epsUserState": <bool>,
  "epsLocationInformation": <bool>,
  "epsCurrentLocation": <bool>,
  "psCurrentLocation": <bool>,
  "сsCurrentLocation": <bool>,
  "default": <bool>
}'
```

### Поля тела запроса ###

| Поле                                          | Описание                                                               | Тип    | O/M | Версия   |
|-----------------------------------------------|------------------------------------------------------------------------|--------|-----|----------|
| [[imsi-get-user-location]]imsi     | Номер IMSI абонента.                                                   | string | C   |          |
| [[msisdn-get-user-location]]msisdn | Номер MSISDN абонента.                                                 | string | С   |          |
| [[imei-get-user-location]]imei     | Номер IMEI абонента.                                                   | string | C   |          |
| epsUserState                                  | Флаг запроса информации о статусе абонента в сети EPS.                 | bool   | O   |          |
| epsLocationInformation                        | Флаг запроса информации о местоположении абонента в сети EPS.          | bool   | O   |          |
| epsCurrentLocation                            | Флаг запроса информации о текущем местоположении абонента в сети EPS.  | bool   | O   |          |
| psCurrentLocation                             | Флаг запроса информации о текущем местоположении абонента в сети PS.   | bool   | O   | 2.1.11.0 |
| csCurrentLocation                             | Флаг запроса информации о текущем местоположении абонента в сети CS.   | bool   | O   | 2.1.11.0 |
| default                                       | Флаг выбора сети для запроса местоположения в порядке EPS -> PS -> CS. | bool   | O   | 2.1.11.0 |

**Примечание.** Задается только один из параметров [imsi](#imsi-get-user-location), [msisdn](#msisdn-get-user-location) и [imei](#imei-get-user-location).

### Пример тела запроса ###

```json
{
  "imsi": "250010000000001",
  "epsUserState": true,
  "epsLocationInformation": true,
  "epsCurrentLocation": true
}
```

### Ответ ###

В ответе передается статус запроса и местоположение абонента в сети EPS.

```
{
  "status": "<string>",
  "locationInformation": <LocationInformation>,
  "problemDetails": [ <ProblemDetails> ]
}
```

### Поля ответа ###

| Поле                | Описание                                 | Тип                                                     | O/M | Версия   |
|---------------------|------------------------------------------|---------------------------------------------------------|-----|----------|
| status              | Статус запроса.                          | string                                                  | M   |          |
| locationInformation | Текущие местоположение абонента в сети.  | [LocationInformation](../entities/locationInformation/) | C   | 2.1.11.0 |
| problemDetails      | Перечень проблем при выполнении запроса. | [[ProblemDetails](../entities/problemDetails/)]         | C   | 2.0.44.0 |

### Пример ответа ###

```json
{
  "status": "OK",
  "locationInformation": {
    "csLocationInformation": {
      "ageOfLocationInformation": 1,
      "cellGlobalId": "00f1102b2d1010",
      "mscNumber": "97572782157",
      "vlrNumber": "97572782157",
      "geographicalInformation": "325322ff",
      "currentLocationRetrieved": true,
      "locationInformationEps": {
        "ageOfLocationInformation": 1,
        "eUtranCellGlobalIdentity": "00f1168b2d1234",
        "trackingAreaIdentity": "00f1100001"
      }
    }
  }
}
```