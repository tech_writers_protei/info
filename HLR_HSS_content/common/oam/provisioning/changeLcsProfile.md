
Команда `ChangeLcsProfile` позволяет изменять профиль LCS.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/SubscriberService/ChangeLcsProfile \
  -d '{
  "action": "<string>",
  "lcsProfile": <LcsProfile>
}'
```


### Поля тела запроса ###

| Поле       | Описание                                                                         | Тип                                   | O/M |
|------------|----------------------------------------------------------------------------------|---------------------------------------|-----|
| action     | Тип действия.pass:q[\<br\>]`create` -- создать; `modify` -- изменить; `delete` -- удалить. | string                                | M   |
| lcsProfile | Профиль LCS.                                                                     | [LcsProfile](../entities/lcsProfile/) | O   |

### Пример тела запроса ###

```json
{
  "action": "create",
  "lcsProfile": {
    "id": 1,
    "gmlcList": [ { "gmlcNumber":"2364657" } ],
    "privacyList": [
      {
        "ssCode": 16,
        "ssStatus": 5,
        "notificationToMsUser": 1,
        "ecList": [
          {
            "gmlcRestriction": 0,
            "notificationToMsUser": 1,
            "externalAddress": "1245135"
          }
        ]
      }
    ],
    "molrList": [
      {
        "ssCode": 15,
        "ssStatus": 2
      }
    ]
  }
}
```

### Ответ ###

В ответе передается только статус запроса.