Команда `DeleteSubscriber` позволяет удалять абонента из базы данных HLR/HSS и центра аутентификации AuC.

При наличии активной регистрации на узлах VLR, SGSN или MME она прекращается запросом MAP-CL или Diameter S6: CLR.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/SubscriberService/DeleteSubscriber \
  -d '{
  "imsi": "<string>",
  "msisdn" : "<string>"
}'
----

== Поля тела запроса

[width="100%",cols="58%,28%,9%,5%",options="header",]
|===
|Поле |Описание |Тип |O/M
|[[imsi-delete-subscriber]]imsi |Номер IMSI абонента. |string |C
|[[msisdn-delete-subscriber]]msisdn |Номер MSISDN абонента. |string |C
|===

*Примечание.* Задается только один из параметров link:#imsi-delete-subscriber[imsi] и link:#msisdn-delete-subscriber[msisdn].

== Пример тела запроса

[source,json]
----
{ "imsi": "250010000001" }
----

== Ответ

В ответе передается только статус запроса.
