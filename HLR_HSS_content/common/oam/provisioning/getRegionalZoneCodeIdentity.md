
Команда `GetRegionalZoneCodeIdentity` позволяет получать код региональной зоны.

### Запрос ###

```bash
$ curl -X POST https://<host>:<port>/ProfileService/GetRegionalZoneCodeIdentity \
  -d '{ "name": "<string>" }'
```

### Поля тела запроса ###

| Поле | Описание               | Тип    | O/M |
|------|------------------------|--------|-----|
| name | Имя региональной зоны. | string | M   |

### Пример тела запроса ###

```json
{ "name": "rus" }
```

### Ответ ###

В ответе передаются статус запроса и код региональной зоны.

```
{
  "status": "<string>",
  "regional": <Regional>
}
```

### Поля ответа ###

| Поле     | Описание               | Тип                               | O/M |
|----------|------------------------|-----------------------------------|-----|
| status   | Статус запроса.        | string                            | M   |
| regional | Код региональной зоны. | [Regional](../entities/regional/) | M   |

### Пример тела запроса ###

```json
{
  "status": "OK",
  "regional": {}
}
```