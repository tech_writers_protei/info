[[ControlRoamingAgreement]]
= ControlRoamingAgreement

Команда `ControlRoamingAgreement` позволяет изменять параметры соглашения о роуминге по фазам CAMEL.

== Запрос

[source,bash]
----
$ curl -X POST https://<host>:<port>/ProfileService/ControlRoamingAgreement \
  -d '{
  "vlrs": {<ibject>}
}'
----

== Поля тела запроса

[cols="2,9,2,1",options="header",]
|===
|Поле |Описание |Тип |O/M
|vlrs |Параметры соглашения о роуминге. |<<oam/provisioning/entities/RAVlr.adoc#RAVlr,RAVlr>> |M
|===

.Пример тела запроса
[source,json]
----
{
  "vlrs": {
    "vlrMask": "7658",
    "camelPhases": 7
  }
}
----

== Ответ

В ответе передается только статус запроса.
