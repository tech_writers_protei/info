
Для включения функциональности NBIOT и настройки профилей абонентов необходимо выполнить следующие действия:

1. Сгенерировать [лицензию](../../config/Core/license/) с ключом NBIOT.
2. Для того чтобы узел HSS мог обрабатывать запросы от узла SCEF, необходимо [добавить](../../interface/API/provisioning/changeScef/) его в список разрешенных.
3. [Создать](../../interface/API/provisioning/controlEpsProfile/) контекст EPS  с необходимой точкой доступа APN и заданными параметрами: nonIpPdnTypeIndicator, nonIpDataDeliveryMechanism, preferredDataMode, pdnConnectionContinuty, scefName.
4. [Создать](../../interface/API/provisioning/addSubscriberProfile/) профиль абонента с созданным контекстом EPS и заданными параметрами activeTime и edrxCycleLength.