from typing import Callable, Iterable

solving: str = """\
[[%Anchor%-total]]
== %Request% Total Count

* Уровень WARN:<br>
  Общее количество сообщений %Request% {period}, меньше ожидаемого значения.
* Уровень ERR:<br>
  Общее количество сообщений %Request% {period}, меньше ожидаемого значения.

=== Причины

* Cause_1;
* Cause_N;

=== Решение

. {open-graph} MME Statistic::Total::TotalByEvent, отражающий количество принятых запросов, разделенное по типам, {period}.
. {open-graph} MME Statistic::%Interface%::%Interface% Type, отражающий количество принятых запросов %Request%.
. Отфильтровать значения только для типа `%Type%`.
. {open-graph} MME Statistic::%Interface%::%Interface% Specific Errors, отражающий количество принятых запросов %Request%, разделенное по кодам ошибок, {period}.
. {open-graph} MME Statistic::%Interface%::%Interface% Duration, отражающий продолжительность выполнения процедуры %Interface%, разделенное по кодам ошибок, {period}.

[[%Anchor%-success-rate]]
== %Request% SuccessRate

* Уровень WARN:<br>
  Доля успешных процедур %Request% {period}, меньше 90%.
* Уровень ERR:<br>
  Доля успешных процедур %Request% {period}, меньше 80%.

=== Причины

* Cause_1;
* Cause_N;

=== Решение

. {open-graph} MME Statistic::Total::TotalByEvent, отражающий количество принятых запросов, разделенное по типам, {period}.
. {open-graph} MME Statistic::%Interface%::%Interface% KPI, отражающий доли успешных и неуспешных процедур %Request%.
. {open-graph} MME Statistic::%Interface%::%Interface% Type, отражающий количество принятых запросов %Request%.
. Отфильтровать значения только для типа `%Type%`.
. {open-graph} MME Statistic::%Interface%::%Interface% Specific Errors, отражающий количество принятых запросов %Request%, разделенное по кодам ошибок, {period}.

"""

requests: list[str] = [
    "Service Request",
    "LTE X2 HO",
    "LTE S1 HO",
    "iRAT HO 4->3",
    "iRAT HO 3->4"
]


def shorten(suffixes: Iterable[str] = None):
    def wrapper(func: Callable):
        def inner(*args, **kwargs):
            _args: list = [*args]
            request: str = kwargs.get("request", _args[0])

            for suffix in suffixes:
                request = request.removesuffix(suffix)

            _args[0] = request
            return func(*_args, **kwargs)
        return inner
    return wrapper


def generate(request: str):
    interface, message_type = request.rsplit(" ", 1)
    anchor: str = _request.lower().replace(' ', '-')
    return (
        solving
        .replace("%Interface%", interface)
        .replace("%Type%", message_type)
        .replace("%Request%", request).replace("%Anchor%", anchor)
    )


if __name__ == '__main__':
    for _request in requests:
        print(generate(_request))

