---
title : "Логирование"
description : "Информация о журналах и внутренних уровнях логирования проекта"
weight : 1
type: docs
---

## Журналы

### Общее

Логирование общей информации о работе системы осуществляется при помощи ведения следующих журналов:

* *info* - журнал, включающий в себя информацию об инициализации и состоянии "основных" процессов системы, например: создание потоков, подключение к *Redis(KeyDB)* и инициализация правил, и т.п.
* *warning* - журнал, содержащий информацию о "критических" сбоях в работе системы, например: обрыв соединения с *Redis(KeyDB)*.
* *trace* - журнал, хранящий в себе информацию о различных "процессах" происходящих в системе, разделен на ряд [уровней](#levels).

Помимо общей информации о работе системы, логирование ведется также для каждого обрабатываемого протокола.

### Протоколы

Логирование обрабатываемых протоколов разделено на *trace-журнал* и *warning-журнал*:

* *SS7FW_trace, DIAMFW_trace, GTPFW_trace* - журналы, содержащие информацию специфичную для каждого из протоколов, разделены на ряд [уровней](#levels).
* *SS7FW_warning, DIAMFW_warning, GTPFW_warning* - журналы, включающие в себя информацию о критических ситуациях, специфичных для каждого из протоколов

### Дополнительно

Помимо журналов, перечисленных выше, для логирования "событий", происходящих при использовании *DPDK*, используются журналы *gtp_forwarder_info* и *gtp_forwarder_warning*

## Уровни {#levels}

Для разделения информации по степени важности были выделены следующие уровни, использующиеся во всех *trace-журналах*:

* <u>**Debug(10)**</u> - уровень, предоставляющий максимально-детализированную информацию о работу системы и обработке сообщений. В связи с большим количеством выходной информации рекомендуется использовать только для внутреннего тестирования.
* <u>**Info(8)**</u> - уровень, содержащий информацию о составе входящий сообщений, результатах проверок, выполняющихся до и во время обработки сообщения при помощи цепочки правил и важных "событиях" в работе системы. Рекомендуется для установки в слабо-нагруженных *production-системах*.
* <u>**Important(5)**</u> - уровень, который, содержит ту же информацию, что и на *Trifling-уровне*, за исключением состава входящих сообщений. В общем случае, рекомендуется для установки в *production-системах*
* <u>**Grand(3)**</u> - уровень, содержащий информацию только о результатах проверки очередного сообщения и его уникальных идентификаторах. Рекомендуется для установки в высоко-нагруженных *production-системах*.
