---
title : "Описание работы с узлом Multi IMSI"
description : ""
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/gtp_fw/description.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---
GTP Multi IMSI осуществляет подмену параметров IMSI, MSISDN, APN по запросу к узлу Multi IMSI.

Запрос на Multi IMSI осуществляется в 
случае успешного декодинга параметров IMSI, MSISDN входящего GTP сообщения.

Пример запроса на узел Multi IMSI:
Endpoint: /MultiIMSI/GtpProxy/GetSubscriptions

```json
{
  "id": 48398393912,
  "subscribers": [
    {
      "imsi": "250770002725692",
      "msisdn": "79410253885"
    },
    {
      "imsi": "251070002725600",
      "msisdn": "79210253800"
    }
  ]
}
```


От Multi IMSI получаем два профиля домашний и дополнительный активный, 
содержащие новые параметры IMSI, MSISDN, MCC, MNC, IP src, IP dst

Пример ответа от узла Multi IMSI:

```json
{
  "id": 48398393912,
  "subscribers": [
    {
      "imsi": "250770002725692",
      "msisdn": "79410253885",
      "home": {
        "imsi": "234568897678934",
        "msisdn": "79213280432",
        "ipSrc": "192.168.1.115",
        "ipDst": "192.168.1.200"
      },
      "additional": {
        "imsi": "250068897678934",
        "msisdn": "79213280569",
        "ipSrc": "192.168.5.150",
        "ipDst": "192.168.5.151"
      }
    },
    {
      "imsi": "251070002725600",
      "msisdn": "79210253800",
      "home": {
        "imsi": "234568897678934",
        "msisdn": "79213280432",
        "ipSrc": "192.168.1.115",
        "ipDst": "192.168.1.200"
      },
      "additional": {
        "imsi": "250068897678934",
        "msisdn": "79213280569",
        "ipSrc": "192.168.5.150",
        "ipDst": "192.168.5.151"
      }
    }
  ]
}
```

Далее заменяем параметры входящего сообщения GTP (IMSI, MSISDN, APN) на полученные из ответа от узла Multi IMSI.

Выбор профиля осуществляется по следующим правилам: 
1. Если исходный запрос из домашней подсети, то для замены выбираем дополнительный активный профиль (additional).
2. Если заданы маски гостевых сетей и запрос определили что запрос из гостевой, то выбираем домашний профиль (home)
3. Если маски гостевых сетей не заданы и определили, что запрос не из домашней подсети, то считаем что запрос из гостевой подсети и выбираем домашний профиль.
