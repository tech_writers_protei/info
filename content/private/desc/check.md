---
title : "Проверки вне цепочки правил"
description : ""
weight : 5
type: docs
---

- Некорректный tcap-буфер (пустой) или ошибка декодирования - BC_INCORRECT_TCAP = 103
- Некорректный tcap-диалог (ошибка декодирования) - BC_INCORRECT_TCAP_DIALOGUE = 104
- Несовпадение мап-версий в сообщениях одной tcap-транзакции - BC_INCORRECT_MAP_VERSION = 107
- Неизвестное сочетание OpCode & ACN - BC_UNKNOWN_OPCODE_AC = 113
- Неизвестный sccp ssn - BC_UNKNOWN_SCCP_SSN = 115
- Tcap-транзакция, для которой нет tcap-begin - BC_INCOMPLETE_TCAP_TAIL = 116
- Системная ошибка (любая другая неидентифицированная ошибка) - BC_SYSTEM_FAIL = 120
- Нет свободных логик - BC_OVERLOAD_SL = 124

[Причины блокировок](../../../common/appendices/block_cause/)
