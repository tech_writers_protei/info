---
title : "Сценарии работы"
description : ""
weight : 5
type: docs
---

### 1. Основная проверка (ALLOWED):

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Status = ALLOWED)
 SS ->> S: HTTP_RESPONSE(pass)
 Note left of SS: Пропускаем сообщение
{{</mermaid>}}

### 2. Основная проверка (not ALLOWED):

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Status != ALLOWED)
 SS ->> S: HTTP_RESPONSE(block)
 Note left of SS: Блокируем сообщение
{{</mermaid>}}

### 1. VelocityCheck, ATI Allowed=1:
  - в цепочке правил задан Velocity Check с ATI allowed = 1 
  - (Dt.Now - Dt.LastActivity) > Validity Period

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
    participant DB as DB
    participant SG as SG
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Action = Velocity Check)
 Note left of R: { "ATI allowed" : "1", "Validity Period" : 3600}
 SS ->> DB: DBRM_REQ(imsi, host))
 Note left of DB: Запрос последней сохраненной активности по абоненту
 DB ->> SS: DBRM_RESP(exist imsi, dtLastActivity, host, ht))
 Note right of SS: Проверка: перемещение не между соседями
 Note right of SS: Проверка: Dt.Now - Dt.LastActivity > Validity Period
 SS ->> SG: HTTP_POST_REQ: AnyTimeInterrogation(imsi)
 SG ->> SS: HTTP_RESPONSE: AnyTimeInterrogation(ATI_ERR=SUCCESS, AgeOfLocation, ATI.Host = Current.Host)
 Note right of SS: Подсчет Velocity
 Note right of SS: Возврат в цепочку правил
 SS ->> R: HANDLE_REQ(handle_data(Velocity, ATI_ERR=SUCCESS))
 Note left of R: Продолжение проверки
 R ->> SS: HANDLE_RESP(Status = ALLOWED)
 SS ->> S: HTTP_RESPONSE(pass)
 Note left of SS: Пропускаем сообщение
 SS ->> DB: DBRM_IND(imsi, host, dtLastActivity))
 Note left of DB: Сохранение последней активности
{{</mermaid>}}


### 2. VelocityCheck, ATI Allowed=0:
  - в цепочке правил задан Velocity Check с ATI allowed = 0 
  - (Dt.Now - Dt.LastActivity) > Validity Period

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
    participant DB as DB
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Action = Velocity Check)
 Note left of R: { "ATI allowed" : "0", "Validity Period" : 3600}
 SS ->> DB: DBRM_REQ(imsi, host))
 Note left of DB: Запрос последней сохраненной активности по абоненту
 DB ->> SS: DBRM_RESP(exist imsi, dtLastActivity, host, ht))
 Note right of SS: Проверка: перемещение не между соседями
 Note right of SS: Проверка: Dt.Now - Dt.LastActivity > Validity Period
 Note right of SS: Подсчет Velocity
 Note right of SS: Возврат в цепочку правил
 SS ->> R: HANDLE_REQ(handle_data(Velocity))
 Note left of R: Продолжение проверки
 R ->> SS: HANDLE_RESP(Status = ALLOWED)
 SS ->> S: HTTP_RESPONSE(pass)
 Note left of SS: Пропускаем сообщение
 SS ->> DB: DBRM_IND(imsi, host, dtLastActivity))
 Note left of DB: Сохранение последней активности
{{</mermaid>}}


### 3. SendATI, Validity Period expired:
  - цепочке правил задан Send ATI
  - (Dt.Now - Dt.LastActivity) > Validity Period

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
    participant DB as DB
    participant SG as SG
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Action = Send ATI)
 Note left of R: { "Validity Period" : 3600}
 SS ->> DB: DBRM_REQ(imsi, host))
 Note left of DB: Запрос последней сохраненной активности по абоненту
 DB ->> SS: DBRM_RESP(exist imsi, dtLastActivity, host, ht))
 Note right of SS: Проверка: Dt.Now - Dt.LastActivity > Validity Period
 SS ->> SG: HTTP_POST_REQ: AnyTimeInterrogation(imsi)
 SG ->> SS: HTTP_RESPONSE: AnyTimeInterrogation(ATI_ERR=SUCCESS, ATI.Host)
 Note right of SS: Возврат в цепочку правил
 SS ->> R: HANDLE_REQ(handle_data(ATI_VLR, ATI_ERR=SUCCESS))
 Note left of R: Продолжение проверки
 R ->> SS: HANDLE_RESP(Status = ALLOWED)
 SS ->> S: HTTP_RESPONSE(pass)
 Note left of SS: Пропускаем сообщение
 SS ->> DB: DBRM_IND(imsi, host, dtLastActivity))
 Note left of DB: Сохранение последней активности
{{</mermaid>}}


### 4. SendATI, Validity Period not expired:
  - в цепочке правил задан Send ATI 
  - (Dt.Now - Dt.LastActivity) <= Validity Period

{{<mermaid>}}
sequenceDiagram
    participant S as STP
    participant SS as SS7FW
    participant R as SS7FW_Rules
    participant DB as DB
autonumber
 S ->> SS: HTTP(TCAP): HTTP_POST(tcap_data)
 Note left of SS: Декодирование tcap_data Проверка целостности сообщения
 SS ->> R: HANDLE_REQ(handle_data(imsi, cgpn, cdpn,...))
 Note left of R: Проверка сообщения цепочкой правил
 R ->> SS: HANDLE_RESP(Action = Send ATI)
 Note left of R: { "Validity Period" : 3600}
 SS ->> DB: DBRM_REQ(imsi, host))
 Note left of DB: Запрос последней сохраненной активности по абоненту
 DB ->> SS: DBRM_RESP(exist imsi, dtLastActivity, host, ht))
 Note right of SS: Проверка: Dt.Now - Dt.LastActivity <= Validity Period
 Note right of SS: Возврат в цепочку правил
 SS ->> R: HANDLE_REQ(handle_data(ATI_VLR = Host из базы))
 Note left of R: Продолжение проверки
 R ->> SS: HANDLE_RESP(Status = ALLOWED)
 SS ->> S: HTTP_RESPONSE(pass)
 Note left of SS: Пропускаем сообщение
 SS ->> DB: DBRM_IND(imsi, host, dtLastActivity))
 Note left of DB: Сохранение последней активности
{{</mermaid>}}


