---
title : "Алгоритм подсчета скорости перемещения абонента Velocity check"
description : ""
weight : 5
type: docs
---

### Сокращения

- Loc - Location - Местоположение абонента
- LA - dtLastActivity - Дата и время последней активности абонента
<!-- - VCS - [VelocityCheckStatus](https://mobiledevelop.git.protei.ru/Protei_SS7FW/docs/config/router_json/#velocitycheck-status) -->
- VCS - [VelocityCheckStatus](../../../common/appendices/vc_status/)

### Схема работы алгоритма

{{<mermaid>}}

stateDiagram
    IsNewLoc: 1. NewLoc?
    IsInDB: 2. Абонент в БД?
    IsCurLoc: 3. CurrentLoc?
    VCS_NoLoc1: 4. VCS_NO_LOC
    NewEqualToCur: 5. New=Current?
    VCS_OK1: 6. VCS_OK
    Neighbors1: 7. Соседи?
    ValidityCheck: 8. ValidityCheck
    VCS_OK2: 9. VCS_OK
    ATIallowed1: 10. ATI Allowed?
    VCS_LOC_EXPIRED_ATI_NOT_ALLOWED: 11. VCS_LOC_EXPIRED_ATI_NOT_ALLOWED
    SendATI1: 12. Запрос ATI?
    AgeOfLoc01: 13. AgeOfLoc=0?
    VCS_AGE_01: 14. VCS_AGE_0
    ATI_NA1: 15. Определили NA по ATI?
    ATIEqCur1: 16. ATI NA = Current NA?
    WARNING: 17. WARNING 
    VCS_OK3: 18. VCS_OK
    VCS_NoLoc2: 19. VCS_NO_LOC
    VCS_LOC_EXPIRED_ATI_FAILED1: 20. VCS_LOC_EXPIRED_ATI_FAILED
    VCS_ATI_FAILED1: 21. VCS_ATI_FAILED
    VCS_NEI1: 22. VCS_NEI
    ATIallowed2: 23. ATI Allowed?
    VCS_NO_LOC_ATI_NOT_ALLOWED: 24. VCS_NO_LOC_ATI_NOT_ALLOWED
    SendATI2: 25. Запрос ATI?
    VCS_ATI_FAILED2: 26. VCS_ATI_FAILED
    VCS_NO_LOC_ATI_FAILED: 27. VCS_NO_LOC_ATI_FAILED
    AgeOfLoc02: 28. AgeOfLoc=0?
    VCS_AGE_02: 29. VCS_AGE_0
    ATILOC: 30. AtiLoc?
    VCS_NoLoc3: 31. VCS_NoLoc
    ATIEqNew: 32. New=Ati?
    VCS_OK4: 33. VCS_OK
    Neighbors2: 34. Соседи?
    VCS_NEI2: 35. VCS_NEI
    VCS_OK5: 36. VCS_OK
    VCS_NoLoc4: 37. VCS_NO_LOC

    [*] --> IsNewLoc
    IsNewLoc --> IsInDB: Определен
    
    IsInDB --> IsCurLoc: Есть
    
    IsCurLoc --> VCS_NoLoc1: Не определен
    IsCurLoc --> NewEqualToCur: Определен
    NewEqualToCur --> VCS_OK1: Да
    NewEqualToCur --> Neighbors1: Нет
    Neighbors1 --> ValidityCheck: Нет
    ValidityCheck --> VCS_OK2: Период актуален
    ValidityCheck --> ATIallowed1: Период просрочен
    ATIallowed1 --> VCS_LOC_EXPIRED_ATI_NOT_ALLOWED: 0
    ATIallowed1 --> SendATI1: 1
    SendATI1 --> AgeOfLoc01: Успешный
    AgeOfLoc01 --> VCS_AGE_01: Да
    AgeOfLoc01 --> ATI_NA1: Нет
    ATI_NA1 --> ATIEqCur1: Да
    ATIEqCur1 --> VCS_OK3: Да
    ATIEqCur1 --> WARNING: Нет
    WARNING --> VCS_OK3
    ATI_NA1 --> VCS_NoLoc2
    SendATI1 --> VCS_LOC_EXPIRED_ATI_FAILED1: ATI_ERR_CODE
    SendATI1 --> VCS_ATI_FAILED1: Нет ответа
    Neighbors1 --> VCS_NEI1

    IsInDB --> ATIallowed2: Нет

    ATIallowed2 --> VCS_NO_LOC_ATI_NOT_ALLOWED: 0
    ATIallowed2 --> SendATI2: 1
    SendATI2 --> VCS_ATI_FAILED2: Нет ответа
    SendATI2 --> VCS_NO_LOC_ATI_FAILED: ATI_ERR_CODE
    SendATI2 --> AgeOfLoc02: Успешный
    AgeOfLoc02 --> VCS_AGE_02: Да
    AgeOfLoc02 --> ATILOC: Да
    ATILOC --> VCS_NoLoc3: Не определен
    ATILOC --> ATIEqNew: Определен
    ATIEqNew --> VCS_OK4: Да
    ATIEqNew --> Neighbors2: Нет
    Neighbors2 --> VCS_NEI2: Да
    Neighbors2 --> VCS_OK5: Нет
    IsNewLoc --> VCS_NoLoc4: Не определен

{{</mermaid>}}

### Описание узлов схемы

| N   | Описание                                                                                                                                                                                                                                                                                                                          |
|-----|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Проверка того, что новое местоположение было найдено                                                                                                                                                                                                                                                                              |
| 2   | Поиск абонента в БД                                                                                                                                                                                                                                                                                                               |
| 3   | Поиск актуальной сетевой зоны абонента. <br> Если в БД сохранена сетевая зона абонента - осуществляется проверка того, что абонент все еще находится в ней <br> В случае, если абонент уже не находится в сохраненной сетевой зоне или она (сетевая зона) не была сохранена в БД - осуществляется поиск сетевой зоны для абонента |
| 4   | Для текущего абонента не была определена текущая сетевая зона <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC(6)*                                                                                                                                                                                                          |
| 5   | Проверка того, что текущая сетевая зона совпадает с новой                                                                                                                                                                                                                                                                         |
| 6   | Текущая сетевая зона равна новой <br> Выход из алгоритма со статусом: *VC_STATUS_OK(0)*                                                                                                                                                                                                                                           |
| 7   | Проверка того, что текущая сетевая зона является соседом новой                                                                                                                                                                                                                                                                    |
| 8   | Проверка времени последней активности абонента: *Dt.Now - Dt.Last activity > Validity period*                                                                                                                                                                                                                                     |
| 9   | Успешная проверка времени последней активности <br> Расчет скорости: *Velocity = Distance/(Now-LA)* <br> Выход из алгоритма со статусом: *LC_STATUS_OK(0)*                                                                                                                                                                        |
| 10  | Проверка возможности отправки ATI на основе флага в *Action* .                                                                                                                                                                                                                                                                    |
| 11  | Время последней активности не прошло проверку, ATI не разрешен. <br> Расчет скорости: *Velocity = Distance/(Now-LA)* <br> Выход из алгоритма со статусом: *VC_STATUS_LOC_EXPIRED_ATI_NOT_ALLOWED(4)*                                                                                                                              |
| 12  | Отправка ATI                                                                                                                                                                                                                                                                                                                      |
| 13  | Ответ на ATI-Запрос успешный, проверка на того, что пользователь сейчас активен: Age Of Location = 0                                                                                                                                                                                                                              |
| 14  | Время последней активности не прошло проверку, пользователь активен (Age Of Location = 0) <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *VC_STATUS_AGE_0(8)*                                                                                                                                       |
| 15  | Поиск сетевой зоны, соответствующей данным, полученным из ответа на ATI запрос                                                                                                                                                                                                                                                    |
| 16  | Сетевая зона, соответствующей данным, полученным из ответа на ATI запрос найдена <br> Сравнение сетевой зоны, соxраненной в БД и сетевой зоной, полученной на основе данных из ATI                                                                                                                                                |
| 17  | Сохраненная сетевая зона не совпадает с сетевой зоной из ATI <br> Обновление сетевой зоны в БД <br> *Warning* с соответствующим сообщением                                                                                                                                                                                        |
| 18  | Время последней активности не прошло проверку, ATI успешный <br> Обновление *Dt.Last Activity* в БД <br> Расчет скорости: *Velocity = Distance/AgeOfLocation* <br> Выход из алгоритма со статусом: *VC_STATUS_OK(0)*                                                                                                              |
| 19  | Время последней активности не прошло проверку, сетевая зона, соответствующая данным, полученным из ответа на ATI запрос не найдена <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC(6)*                                                                                             |
| 20  | Время последней активности не прошло проверку, ATI не прошел проверку <br> Фиксирование *ATI_ERR_CODE* <br> Фиксирование *Dt.Last Activity и Current NA ID* данными, полученными из БД <br> Расчет скорости: *Velocity = Distance/(Now-LA)* <br> Выход из алгоритма со статусом: *VC_STATUS_LOC_EXPIRED_ATI_FAILED(5)*            |
| 21  | Время последней активности не прошло проверку, ответ на ATI не получен <br> Фиксирование *Dt.Last Activity и Current NA ID* данными, полученными из БД <br> Выход из алгоритма со статусом: *VC_STATUS_ATI_FAILED(7)*                                                                                                             |
| 22  | Текущая сетевая зона является соседом новой <br> Выход из алгоритма со статусом: *VC_STATUS_NEI(1)*                                                                                                                                                                                                                               |
| 23  | Проверка возможности отправки ATI на основе флага в *Action*.                                                                                                                                                                                                                                                                     |
| 24  | Абонент не в БД, ATI не разрешен. <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC_ATI_NOT_ALLOWED(3)*                                                                                                                                                                                                                      |
| 25  | Отправка ATI                                                                                                                                                                                                                                                                                                                      |
| 26  | Абонент не в БД, ответ на ATI не получен. <br> Выход из алгоритма со статусом: *VC_STATUS_ATI_FAILED(7)*                                                                                                                                                                                                                          |
| 27  | Абонент не в БД, ATI не прошел проверку <br> Фиксирование *ATI_ERR_CODE* <br> Фиксирование *Dt.Last Activity и Current NA ID* данными, полученными из БД <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC_ATI_FAILED(2)*                                                                                                    |
| 28  | Ответ на ATI-Запрос успешный, проверка на того, что пользователь сейчас активен: Age Of Location = 0                                                                                                                                                                                                                              |
| 29  | Время последней активности не прошло проверку, пользователь активен (Age Of Location = 0) <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *VC_STATUS_AGE_0(8)*                                                                                                                                       |
| 30  | Поиск сетевой зоны, соотвествующей данным, полученным из ответа на ATI запрос                                                                                                                                                                                                                                                     |
| 31  | Сетевая зона, соотвествующая данным, полученным из ответа на ATI запрос не найдена <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC(6)*                                                                                                                                             |
| 32  | Сетевая зона, соотвествующей данным, полученным из ответа на ATI запрос найдена <br> Сравнение новой зоны c сетевой зоной, полученной на основе данных из ATI                                                                                                                                                                     |
| 33  | Новая сетевая зона совпадает сетевой зоной, полученной на основе данных из ATI <br> Выход из алгоритма со статусом: *VC_STATUS_OK(0)*                                                                                                                                                                                             |
| 34  | Новая сетевая зона не соответствует сетевой зоне, полученной на основе данных из ATI <br> Проверка того, что она (новая сетевая зона) является соседом сетевой зоны из ATI                                                                                                                                                        |
| 35  | Новая сетевая зона является соседом сетевой зоной, полученной на основе данных из ATI <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *VC_STATUS_NEI(1)*                                                                                                                                             |
| 36  | Новая сетевая зона не является соседом сетевой зоной, полученной на основе данных из ATI <br> Обновление *Dt.Last Activity* и *NA ID* в БД <br> Расчет скорости: *Velocity = Distance/AgeOfLocation* <br> Выход из алгоритма со статусом: *VC_STATUS_OK(0)*                                                                       |
| 37  | Новая сетевая зона не найдена <br> Выход из алгоритма со статусом: *VC_STATUS_NO_LOC(6)*                                                                                                                                                                                                                                          |
