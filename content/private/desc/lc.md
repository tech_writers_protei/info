---
title : "Алгоритм проверки актуальности информации о местоположении: Location Check"
description : ""
weight : 5
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/common/desc/lc.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---

### Сокращения

- *LCS* - *LocationCheckStatus* представлены [тут](../../../common/appendices/lc_status/)

### Схема работы алгоритма

{{<mermaid>}}

stateDiagram    
    IsInDB: 1. Абонент в БД?
    ValidityCheck: 2. ValidityCheck
    LCS_OK1: 3. LCS_OK
    ATIallowed1: 4. ATI allowed?
    LCS_PERIOD_EXCEEDED_ATI_NOT_ALLOWED: 5. LCS_PERIOD_EXCEEDED_ATI_NOT_ALLOWED
    Send_ATI1: 6. Send ATI
    ATICool1: 7. ATI Успешный?
    LCS_PERIOD_EXCEEDED_ATI_FAILED: 8. LCS_PERIOD_EXCEEDED_ATI_FAILED
    AgeOfLocation1: 9. Age Of Location = 0?
    LCS_AGE_01: 10. LCS_AGE_0
    IsGotATINA: 11. Определили NA по ATI?
    LCS_NO_NA: 12. LCS_NO_NA
    IsNAequal: 13. Current_NA = ATI_NA?
    LCS_OK2: 14. LCS_OK
    LCS_NEW_LOC_ATI: 15. LCS_NEW_LOC_ATI
    ATIallowed2: 16. ATI allowed?
    LCS_NO_LOC_ATI_NOT_ALLOWED: 17. LCS_NO_LOC_ATI_NOT_ALLOWED
    Send_ATI2: 18. Send ATI
    ATICool2: 19. ATI Успешный?
    LCS_NO_LOC_ATI_FAILED: 20. LCS_NO_LOC_ATI_FAILED
    AgeOfLocation2: 21. Age Of Location = 0?
    LCS_AGE_02: 22. LCS_AGE_0
    IsGotATINA2: 23. Определили NA по ATI?
    LCS_OK3: 24. LCS_OK
    LCS_NO_NA2: 25. LCS_NO_NA

    [*] --> IsInDB
    IsInDB --> ValidityCheck: Да
    IsInDB --> ATIallowed2: Нет

    ValidityCheck --> LCS_OK1: Нет
    ValidityCheck --> ATIallowed1: Да
    ATIallowed1 --> LCS_PERIOD_EXCEEDED_ATI_NOT_ALLOWED: Нет
    ATIallowed1 --> Send_ATI1: Да
    Send_ATI1 --> ATICool1
    ATICool1 --> LCS_PERIOD_EXCEEDED_ATI_FAILED: Нет
    ATICool1 --> AgeOfLocation1: Да
    AgeOfLocation1 --> LCS_AGE_01: Да
    AgeOfLocation1 --> IsGotATINA: Нет
    IsGotATINA --> LCS_NO_NA: Нет
    IsGotATINA --> IsNAequal: Да
    IsNAequal --> LCS_OK2: Да
    IsNAequal --> LCS_NEW_LOC_ATI: Нет

    ATIallowed2 --> LCS_NO_LOC_ATI_NOT_ALLOWED: Нет
    ATIallowed2 --> Send_ATI2: Да
    Send_ATI2 --> ATICool2
    ATICool2 --> LCS_NO_LOC_ATI_FAILED: Нет
    ATICool2 --> AgeOfLocation2: Да
    AgeOfLocation2 --> LCS_AGE_02: Да
    AgeOfLocation2 --> IsGotATINA2: Нет
    IsGotATINA2 --> LCS_OK3: Да
    IsGotATINA2 --> LCS_NO_NA2: Нет

{{</mermaid>}}

### Описание узлов схемы

| N   | Описание                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|-----|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1   | Поиск абонента в БД                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| 2   | Проверка времени последней активности абонента: *Dt.Now - Dt.Last activity > Validity period*                                                                                                                                                                                                                                                                                                                                                                                       |
| 3   | Успешная проверка времени последней активности <br> Выход из алгоритма со статусом: *LC_STATUS_OK(0)*                                                                                                                                                                                                                                                                                                                                                                               |
| 4   | Проверка возможности отправки ATI на основе флага в *Action* .                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 5   | Время последней активности не прошло проверку, ATI не разрешен. <br> Выход из алгоритма со статусом: *LC_STATUS_PERIOD_EXCEEDED_ATI_NOT_ALLOWED(4)*                                                                                                                                                                                                                                                                                                                                 |
| 6   | Отправка ATI                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 7   | Проверка ответа на ATI-запрос                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 8   | Время последней активности не прошло проверку, ответ на ATI запрос не прошел проверку. <br> Фиксирование *ATI_ERR_CODE*  <br> Фиксирование *Dt.Last Activity и Current NA ID* данными, полученными из БД <br> Выход из алгоритма со статусом: *LC_STATUS_PERIOD_EXCEEDED_ATI_FAILED(5)*                                                                                                                                                                                             |
| 9   | Проверка на того, что пользователь сейчас активен: Age Of Location = 0                                                                                                                                                                                                                                                                                                                                                                                                              |
| 10  | Время последней активности не прошло проверку, пользователь активен (Age Of Location = 0) <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *LC_STATUS_AGE_0(8)*                                                                                                                                                                                                                                                                                         |
| 11  | Поиск сетевой зоны, соотвествующей данным, полученным из ответа на ATI запрос                                                                                                                                                                                                                                                                                                                                                                                                       |
| 12  | Время последней активности не прошло проверку, сетевая зона, соответствующая ATI не найдена. <br> Сравнение времени регистрации из ATI и текущего сохраненного времени последней активности <br> Если текущее время последней активности меньше чем время регистрации, то обновляем его в соотвествии с временем регистрации <br> Обновление *Dt.Last Activity* в БД <br> Фиксирование *Dt.Last Activity и Current NA ID* <br> Выход из алгоритма со статусом: *LC_STATUS_NO_NA(6)* |
| 13  | Сравнение сетевой зоны, сохарненной в БД и сетевой зоной, полученной на основе данных из ATI                                                                                                                                                                                                                                                                                                                                                                                        |
| 14  | Время последней активности не прошло проверку, сетевая зона из БД совпадает с сетевой зоной из ATI <br> Обновление *Dt.Last Activity* на основе логики из 12 пункта <br> Обновление *Dt.Last Activity* в БД <br> Фиксирование *Dt.Last Activity и Current NA ID* <br> Выход из алгоритма со статусом: *LC_STATUS_OK(0)*                                                                                                                                                             |
| 15  | Время последней активности не прошло проверку, сетевая зона из БД отличается от сетевой зоны из ATI <br> Обновление *Dt.Last Activity* на основе логики из 12 пункта <br> Обновление *Dt.Last Activity* <br> Обновление *NA ID*, на основе полученной из ATI в БД <br> Фиксирование *Dt.Last Activity и Current NA ID* <br> Выход из алгоритма со статусом: *LC_STATUS_NEW_LOC_ATI(1)*                                                                                              |
| 16  | Проверка возможности отправки ATI на основе флага в *Action* .                                                                                                                                                                                                                                                                                                                                                                                                                      |
| 17  | Нет информации по абоненту в БД, ATI не разрешен. <br> Выход из алгоритма со статусом: *LC_STATUS_NO_LOC_ATI_NOT_ALLOWED(3)*                                                                                                                                                                                                                                                                                                                                                        |
| 18  | Отправка ATI                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| 19  | Проверка ответа на ATI-запрос                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| 20  | Нет информации по абоненту в БД, ответ на ATI запрос не прошел проверку. <br> Фиксирование *ATI_ERR_CODE* <br> Выход из алгоритма со статусом: *LC_STATUS_NO_LOC_ATI_FAILED(2)*                                                                                                                                                                                                                                                                                                     |
| 21  | Проверка на того, что пользователь сейчас активен: Age Of Location = 0                                                                                                                                                                                                                                                                                                                                                                                                              |
| 22  | Время последней активности не прошло проверку, пользователь активен (Age Of Location = 0) <br> Обновление *Dt.Last Activity* в БД <br> Выход из алгоритма со статусом: *LC_STATUS_AGE_0(8)*                                                                                                                                                                                                                                                                                         |
| 23  | Поиск сетевой зоны, соотвествующей данным, полученным из ответа на ATI запрос                                                                                                                                                                                                                                                                                                                                                                                                       |
| 24  | Нет информации по абоненту в БД, сетевая зона для ATI была найдена. <br> Обновление *Dt.Last Activity* на основе времени регистрации из ATI <br> Фиксирование *Dt.Last Activity и Current NA ID* <br> Выход из алгоритма со статусом: *LC_STATUS_OK(0)*                                                                                                                                                                                                                             |                                                   |
| 25  | Нет информации по абоненту в БД, сетевая зона для ATI не найдена. <br> Обновление *Dt.Last Activity* на основе времени регистрации из ATI <br> Фиксирование *Dt.Last Activity* <br> Выход из алгоритма со статусом: *LC_STATUS_NO_NA(0)*                                                                                                                                                                                                                                            |