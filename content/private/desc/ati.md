---
title : "Алгоритм запроса ATI"
description : ""
weight : 5
type: docs
---

- LA - dtLastActivity - Дата и время последней активности абонента

{{<mermaid>}}

graph LR;

A[Правило SendATI] --> B{Абонент в БД?};
B --> |Нет| E{Запрос ATI?};
B --> |Есть| D{Now-LA > Validity Period?};
D --> |Просрочен| E;
D --> |Актуален| F([OK]);
E --> |Нет ответа| G([OK]);
E --> |ATI_ERR_CODE| H([OK]);
E --> |ATI_VLR| J([OK]);


{{</mermaid>}}

