---
title : "Описание"
description : ""
weight : 1
type: docs
---

- **[ТЗ VC: https://youtrack.protei/issue/Mobile_SS7FW-46](./vc1/)**


### Сценарии работы

- **[Сценарии работы](./sc/)**
- **[Алгоритм Velocity Check](./vc/)**
- **[Алгоритм Location Check](./lc/)**
- **[Алгоритм SendATI](./ati/)**
- **[Проверки вне цепочки правил](./check/)**

