---
title : "ТЗ: Velocity check"
description : ""
weight : 5
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/common/desc/vc1.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---


![21](../vc.png)
