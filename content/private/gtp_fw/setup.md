---
title : "Настройка системы"
description : "Настройка системы для работы GTP FW"
weight : 1
type: docs
---

Для работы GTP FW необходимо выполнить следующие шаги:

1. Собрать и установить [DPDK](#install-dpdk)
2. Если используется режим `DPDK`, [изменить](#change-driver) драйвер используемых интерфейсов на поддерживаемый DPDK-compatible. Если используется `AF_PACKET`, этот шаг можно пропустить
3. Выделить [hugepages](#allocate-hugepages) 
4. Настроить конфигурацию [gtp_forwarder.cfg](../../../common/config/gtp_forwarder/)
5. Запустить приложение с правами администратора

### Установка DPDK {#install-dpdk}

* Распаковать архив с исходным кодом DPDK v20.05 с [официального сайта](http://core.dpdk.org/download/)
* Заменить python на python3 в скриптах `dpdk-setup.sh` и `dpdk-devbind.py`.  На сервере должен быть установлен пакет python.
* Запустить скрипт `<dpdk_root_dir>/usertools/dpdk-setup.sh`
Выбрать `Option 38  - x86_64-native-linuxapp-gcc` 
По завершении, будет выведен warning следующего вида: 
```shell
==============
Installation cannot run with T defined and DESTDIR undefined
------------------------------------------------------------------------------
 RTE_TARGET exported as x86_64-native-linuxapp-gcc
------------------------------------------------------------------------------
Press enter to continue ...
```
Не обращать внимание на данный вывод. Проверить, что появилась директория `x86_64-native-linuxapp-gcc` в `<dpdk_root_dir>`.

### Изменение драйвера интерфейсов {#change-driver}

* Просмотр текущего состояния интерфейсов:
```shell
dpdk-devbind.py -s

Network devices using kernel driver
===================================
0000:02:00.0 'RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller 8168' if=enp2s0 drv=r8169 unused=vfio-pci *Active*
0000:03:00.0 'QCA9565 / AR9565 Wireless Network Adapter 0036' if=wlp3s0 drv=ath9k unused=vfio-pci 

No 'Baseband' devices detected
==============================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================

No 'Misc (rawdev)' devices detected
===================================
```

* Добавляем драйвер интерфейса в DPDK.

  Запустить скрипт `<dpdk_root_dir>/usertools/dpdk-setup.sh` еще раз. Выбрать необходимый драйвер сетевого интерфейса. Например, `Option 46 - Insert VFIO module`
  Ожидаемый вывод:

  ```shell
  Unloading any existing VFIO module
  Loading VFIO module
  chmod /dev/vfio
  OK
  ```

* Биндинг интерфейсов в DPDK

  3.1 Для драйвера igb_uio (не тестировалось, на oel8 не удалось собрать dpdk с его поддержкой. На Ubuntu должно решиться установкой пакета  dpdk-igb-uio-dkms ) выполнить следующие команды:

  ```shell
  modprobe uio
  insmod igb_uio.ko
  ```
  
  3.2 Для драйвера vfio-pci (тестировалось на oel8 в Etisalat)
  3.2.1 Подгружаем драйвер

  ```shell
  modprobe vfio-pci
  ```
  
  3.2.2 Setup VFIO permissions for regular users before binding:
  
  ```shell
  sudo chmod a+x /dev/vfio
  sudo chmod 0666 /dev/vfio/*
  ```
  
  3.2.3 Смысл следующих команд непонятен, но без них не работает:
  
  ```shell
  modprobe vfio enable_unsafe_noiommu_mode=1
  echo 1 > /sys/module/vfio/parameters/enable_unsafe_noiommu_mode
  ```
  
  3.3 Для драйвера uio_pci_generic выполнить команду: 
  ```shell
  modprobe uio_pci_generic
  ```
  
* Проверяем еще раз интерфейсы, драйвер должен появиться в “unused”:

  ```shell
  [root@EAMSC1GPT_FW_FE_2_PR usertools]# ./dpdk-devbind.py -s
  Network devices using kernel driver
  ===================================
  0000:02:01.0 'VMXNET3 Ethernet Controller 07b0' if=ens33 drv=vmxnet3 unused=vfio-pci *Active*
  0000:02:03.0 'VMXNET3 Ethernet Controller 07b0' if=ens35 drv=vmxnet3 unused=vfio-pci *Active*
  0000:02:04.0 'VMXNET3 Ethernet Controller 07b0' if=ens36 drv=vmxnet3 unused=vfio-pci *Active*
  ```

*  Биндим адаптеры к загруженным драйверам ( Внимание! Не биндить интерфейс, к которому вы подключены по ssh):

  ```shell
  dpdk-devbind.py -b <driver> <device1> <device2> ...
  
  dpdk-devbind.py -b igb_uio 0000:02:00.0 0000:03:00.0
  ```
  
* Снова проверяем состояние интерфейсов, Они должны исчезнуть из вывода linux команд nmcli и ip a. Вместо этого, должны появиться в списке “Network devices using DPDK-compatible driver”
  
  ```shell
  Network devices using DPDK-compatible driver
  ============================================
  0000:02:03.0 'VMXNET3 Ethernet Controller 07b0' drv=vfio-pci unused=vmxnet3
  0000:02:04.0 'VMXNET3 Ethernet Controller 07b0' drv=vfio-pci unused=vmxnet3
  Network devices using kernel driver
  ===================================
  0000:02:01.0 'VMXNET3 Ethernet Controller 07b0' if=ens33 drv=vmxnet3 unused=vfio-pci *Active*
  ```

### Выделение hugepages {#allocate-hugepages}

* После установки DPDK выполнить `<dpdk_root_dir>/usertools/dpdk-setup.sh`
* Выбрать `Setup hugepage mappings for non-NUMA systems`
* Указать необходимое количество hugepages
