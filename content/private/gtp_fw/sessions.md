---
title : "GTP сессии"
description : "Обработка GTP-C Tunnel Management сообщений и хранение GTP сессий"
weight : 1
type: docs
---

## Хранение сессий

Каждая GTP сессия содержит в себе

* **SGW Control F-TEID** - F-TEID для GTP-C сообщений в направлении SGW/SGSN
* **SGW BearerContexts** - список BearerId и соответствующих им F-TEID для GTP-U сообщений в направлении SGW/SGSN
* **PGW Control F-TEID** - F-TEID для GTP-C сообщений в направлении PGW/GGSN
* **PGW BearerContexts** - список BearerId и соответствующих им F-TEID для GTP-U сообщений в направлении PGW/GGSN

### Локальное хранилище

Каждая сессия хранится в двух таблицах в оперативной памяти по нескольким ключам:

1. Ключи для **control_table** - _SGW Control F-TEID_ и _PGW Control F-TEID_
2. Ключи для **data_table** - все F-TEID из _SGW BearerContexts_ и _PGW BearerContexts_

### Хранение сессий в Redis

Каждая сессия хранится в Redis в [JSON формате](../../db/#gtp-session).
Сохранение в Redis происходит после получения успешного ответа на создание или обновление сессии.

Для уменьшения нагрузки на Redis все ответы кешируются в памяти GTPFW.
Успешно найденные сессии сохраняются в локальное хранилище.
Для неуспешных запросов в памяти сохраняется **Deny запись** - F-TEID, по которому был выполнен неуспешный запрос в
Redis.

При <a name="session_search">поиске</a> сессии используется следующий алгоритм:

1. Проверяем в локальном хранилище. Если не найдено, то переходим к шагу 2.
2. Проверяем закешированные Deny записи. Если найдена, то считаем, что в Redis сессии нет. Если не найдена, то переходим
   к шагу 3.
3. Запрашиваем сессию из Redis. Если найдена, сохраняем в локальное хранилище. Если не найдена, сохраняем Deny запись по
   запрашиваемому F-TEID.

## Обработка GTP-С Tunnel Management сообщений

Для создания/изменения/удаления F-TEID в сессии необходимо дождаться ответа и проверить cause.
Только при успешном ответе GTP FW может однозначно понимать, что сессия добавлена/изменена/удалена.
Однако в соответствии с TS 129.060:

```text
The GGSN may start to forward T-PDUs after the Create PDP Context Response has been sent. The SGSN may start to
forward T-PDUs when the Create PDP Context Response has been received. In this case the SGSN shall also be
prepared to receive T-PDUs from the GGSN after it has sent a Create PDP Context Request but before a Create PDP
Context Response has been received.

The GGSN may start to forward T-PDUs after the Update PDP Context Response has been sent. The SGSN may start to
forward T-PDUs when the Update PDP Context Response has been received. In this case the SGSN shall also be
prepared to receive T-PDUs from the GGSN after it has sent an Update PDP Context Request but before an Update
PDP Context Response has been received.
```

Поэтому GTP FW должен уметь пропускать GTP-U сообщения даже до получения ответа на GTP-C запрос.
Для этого реализована следующая логика управления ключами в таблице:

| Операция над F-TEID в сессии   | Действия при запросе          | Действия при успешном ответе                                                        | Действия при неуспешном ответе |
|--------------------------------|-------------------------------|-------------------------------------------------------------------------------------|--------------------------------|
| <a name="add">Добавление</a>   | Добавить новый ключ в таблицу | Добавить F-TEID в сессию + обновить сессию в Redis                                  | Удалить новый ключ из таблицы  |
| <a name="modify">Изменение</a> | Добавить новый ключ в таблицу | Обновить F-TEID в сессии + удалить старый ключ из таблицы + обновить сессию в Redis | Удалить новый ключ из таблицы  |
| <a name="delete">Удаление</a>  | -                             | Удалить F-TEID из сессии + удалить ключ из таблицы + обновить сессию в Redis        | -                              |

Таким образом пока GTP FW не получил ответ на запрос по созданию/изменению/удалению, будут пропускать GTP-U сообщения,
как по старым F-TEID, так и по новым. А после получения ответа изменения либо финализируются
(пропускаем только по обновленным F-TEID), либо откатываются (пропускаем только по F-TEID до обновления).

## GTPv1 Tunnel Management сообщения

### CreatePDPContextRequest

При получении создается новая сессия:

* **SGW Control F-TEID** = <_TEID Control Plane_ + _GSN Address 1_>.
* **SGW BearerContexts** = { _NSAPI_ -> <_TEID Data I_ + _GSN Address 2_> }.

Сессия сохраняется в таблицах по ключам <TEID Control Plane + GSN Address 1> и <TEID Data I + GSN Address 2>.

### CreatePDPContextResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия созданная при обработке CreatePDPContextRequest.

При cause = _request accepted_:

* В сессию добавляется **PGW Control F-TEID** = <_TEID Control Plane_ + _GSN Address 1_>
* В сессию добавляется **PGW BearerContexts** = { _NSAPI_ -> <_TEID Data I_ + _GSN Address 2_> }

В таблицу добавляются ключи <TEID Control Plane + GSN Address 1> и <TEID Data I + GSN Address 2>.

При других cause сессия полностью удаляется из таблицы.

### UpdatePDPContextRequest

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия. Далее:

* При наличии в сообщении _TEID Control Plane_ и _GSN Address 1_ - заменяем SGW Control F-TEID на полученный <_TEID
  Control Plane_ + _GSN Address 1_>.
* При наличии в сообщении _TEID Data I_ и _GSN Address 2_ - заменяем SGW F-TEID по соответствующему NSAPI на
  полученный <_TEID Data I_ + _GSN Address 2_>.

Ключи в таблицах обновляются в соответствии с описанной выше логикой [изменения](#modify) F-TEID.

### UpdatePDPContextResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия. Далее:

* При cause = _request accepted_:
    * При наличии в сообщении _TEID Control Plane_ и _GSN Address 1_ - заменяем PGW Control F-TEID на полученный <_TEID
      Control Plane_ + _GSN Address 1_>.
    * При наличии в сообщении _TEID Data I_ и _GSN Address 2_ - заменяем PGW F-TEID по соответствующему NSAPI на
      полученный <_TEID Data I_ + _GSN Address 2_>.

Ключи в таблицах обновляются в соответствии с описанной выше логикой [изменения](#modify) F-TEID.

### DeletePDPContextRequest

Не обрабатывается, так как необходимо дождаться _DeletePDPContextResponse_

### DeletePDPContextResponse

По <_GTP Header TEID_ + _Dst IP_> в таблице ищется сессия. Из таблицы удаляются F-TEID, содержащиеся в сессии.
Таким образом сессию полностью удалена.

## GTPv2 Tunnel Management сообщения

### CreateSessionRequest

При получении создается новая сессия:

* **SGW Control F-TEID** = _Sender F-TEID_
* **SGW BearerContexts** = _BearerContextsToBeCreated_

Сессия сохраняется в таблицах по ключам _Sender F-TEID_ и всем F-TEID из добавленных _BearerContexts_.

### CreateSessionResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.
При _cause_ = _request accepted_ или _request accepted partially_. В сессию добавляются:

* **PGW Control F-TEID** = _PGW F-TEID_
* **PGW BearerContexts** = _BearerContextsToBeCreated_

При _cause_ = _request accepted partially_ _PGW BearerContexts_ финализируются или откатываются в соответствии с cause
в _BearerContextsToBeCreated_.

Ключи в таблицах обновляются в соответствии с описанной выше логикой [добавления](#add) F-TEID.

При неуспешных _cause_ сессия полностью удаляется.

### DeleteSessionRequest

Не обрабатывается, так как необходимо дождаться _DeleteSessionResponse_.

### DeleteSessionResponse

По <_GTP Header TEID_ + _Dst IP_> в таблице ищется сессия. Из таблицы удаляются F-TEID, содержащиеся в сессии.
Таким образом сессию полностью удалена.

### ModifyBearerRequest

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия. Далее:

* При наличии Sender F-TEID - заменяем SGW Control F-TEID
* Изменяются F-TEID в SGW BearerContexts в соответствии с _BearerContextsToBeModified_.
* Из SGW BearerContexts и PGW BearerContexts удаляем F-TEID в соответствии с _BearerContextsToBeRemoved_.

Ключи в таблицах обновляются в соответствии с описанной выше логикой [изменения](#modify) и [удаления](#delete) F-TEID.

### ModifyBearerResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.
При _cause_ = _request accepted_ или _request accepted partially_. В сессию добавляются:

* **PGW Control F-TEID** = _PGW F-TEID_
* **PGW BearerContexts** = _BearerContextsToBeCreated_

* При _cause_ = _request accepted_ изменение и удаление всех SGW F-TEID финализируется.
* При _cause_ = _request accepted partially_ _SGW BearerContexts_ финализируются или откатываются в соответствии с cause
  в _BearerContextsModified_ и _BearerContextsMarkedForRemoval_.
* При остальных _cause_ изменение и удаление F-TEID в таблице откатывается.

См. как обрабатываются ответы для операций [изменения](#modify) и [удаления](#delete) F-TEID.

### CreateBearerRequest

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.
В неё добавляется SGW BearerContexts в соответствии с _BearerContexts_ из запроса.

Ключи в таблицах обновляются в соответствии с описанной выше логикой [добавления](#add) F-TEID

### CreateBearerResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.
В неё добавляется PGW BearerContexts в соответствии с _BearerContexts_ из ответа.

* При _cause_ = _request accepted_ добавление всех SGW F-TEID финализируется.
* При _cause_ = _request accepted partially_ _SGW BearerContexts_ финализируются или откатываются в соответствии с cause
  в _BearerContexts_.
* При остальных _cause_ добавление F-TEID в таблице откатывается.

См. как обрабатываются ответы для операции [добавления](#add) F-TEID.

### DeleteBearerRequest

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.
Из SGW BearerContexts и PGW BearerContexts удаляются все F-TEID в соответствии с _EPSBearerIDs_

### DeleteBearerResponse

По <_GTP Header TEID_ + _Dst IP_> в _control_table_ ищется сессия.

* При _cause_ = _request accepted_ удаление всех F-TEID финализируется.
* При _cause_ = _request accepted partially_ удаление всех F-TEID финализируются или откатываются в соответствии с cause
  в _BearerContexts_.
* При остальных _cause_ удаление всех F-TEID откатывается.

См. как обрабатываются ответы для операции [удаления](#delete) F-TEID.

## Примеры сценариев

### GTPv1 создание и обновление сессии

{{<mermaid>}}

sequenceDiagram
    participant S as SGSN (10.10.1.1)
    participant G as GGSN (10.10.1.2)

    S ->> G: 1. CreatePDPContextRequest: TEID_CP = 0x1101, TEID_UP = 0x2101, NSAPI = 5
    G ->> S: 2. CreatePDPContextResponse: TEID_CP = 0x1201, TEID_UP = 0x2201
    S ->> G: 3. UpdatePDPContextRequest: TEID_CP = 0x1101, TEID_UP = 0x3001, NSAPI = 5
    G ->> S: 4. UpdatePDPContextResponse: TEID_CP = 0x1201, TEID_UP = 0x3002

{{</mermaid>}}

**1. После обработки CreatePDPContextRequest**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x2101, ip = 10.10.1.1}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
* ключи в data_table
  * {teid = 0x2101, ip = 10.10.1.1}

**2. После обработки CreatePDPContextResponse**

Данные, хранящиеся в сессии:
* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x2101, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x2201, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x2201, ip = 10.10.1.2}
  * {teid = 0x2101, ip = 10.10.1.1}

**3. После обработки UpdatePDPContextRequest**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x3001, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x2201, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x2201, ip = 10.10.1.2}
  * {teid = 0x3001, ip = 10.10.1.1}
  * {teid = 0x2101, ip = 10.10.1.1} # старый f-teid от SGSN так же используется как ключ, так как SGSN доджен быть готов
    принимать TPDU даже до получения UpdatePDPContextResponse

**4. После обработки UpdatePDPContextResponse**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x3001, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x3002, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x3002, ip = 10.10.1.2}
  * {teid = 0x3001, ip = 10.10.1.1}

### GTPv2 Создание сессии, добавление bearer, удаление bearer 

{{<mermaid>}}

sequenceDiagram
participant S as SGW (10.10.1.1)
participant P as PGW (10.10.1.2)

    S ->> P: 1. CreateSessionRequest: TEID_CP = 0x1101, TEID_UP = 0x2101, BearerId = 6
    P ->> S: 2. CreateSessionResponse: TEID_CP = 0x1201, TEID_UP = 0x2201
    P ->> S: 3. CreateBearerRequest: TEID_UP = 0x3002, BearerId = 5
    S ->> P: 4. CreateBearerResponse: TEID_UP = 0x3001
    P ->> S: 5. DeleteBearerRequest: BearerId = 6
    S ->> P: 6. DeleteBearerResponse

{{</mermaid>}}

**1. После обработки CreateSessionRequest**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {6 : {teid = 0x2101, ip = 10.10.1.1}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
* ключи в data_table
  * {teid = 0x2101, ip = 10.10.1.1}

**2. После обработки CreateSessionResponse**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {6 : {teid = 0x2101, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {6 : {teid = 0x2201, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x2201, ip = 10.10.1.2}
  * {teid = 0x2101, ip = 10.10.1.1}

**3. После обработки CreateBearerRequest**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {6 : {teid = 0x2101, ip = 10.10.1.1};}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x3002, ip = 10.10.1.2}; 6 : {teid = 0x2201, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x3002, ip = 10.10.1.2}
  * {teid = 0x2201, ip = 10.10.1.2}
  * {teid = 0x2101, ip = 10.10.1.1}

**4. После обработки CreateBearerResponse**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x3001, ip = 10.10.1.1}; 6 : {teid = 0x2101, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x3002, ip = 10.10.1.2}; 6 : {teid = 0x2201, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x3002, ip = 10.10.1.2}
  * {teid = 0x2201, ip = 10.10.1.2}
  * {teid = 0x3001, ip = 10.10.1.1}
  * {teid = 0x2101, ip = 10.10.1.1}

**5. После обработки DeleteBearerRequest**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x3001, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x3002, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x3002, ip = 10.10.1.2}
  * {teid = 0x2201, ip = 10.10.1.2} # Всё ещё храним старые fteid, так как может прийти неуспешный ответ, а до него
    могут приходить TDPU, которые нужно пропустить
  * {teid = 0x3001, ip = 10.10.1.1}
  * {teid = 0x2101, ip = 10.10.1.1} # Всё ещё храним старые fteid, так как может прийти неуспешный ответ, а до него
    могут приходить TDPU, которые нужно пропустить

**6. После обработки DeleteBearerResponse**

Данные, хранящиеся в сессии:

* sgw_control_fteid   : {teid = 0x1101, ip = 10.10.1.1}
* sgw_bearer_contexts : {5 : {teid = 0x3001, ip = 10.10.1.1}}
* pgw_control_fteid : {teid = 0x1201, ip = 10.10.1.2}
* pgw_bearer_contexts : {5 : {teid = 0x3002, ip = 10.10.1.2}}

В таблицах сохраняются:

* ключи в control_table
  * {teid = 0x1101, ip = 10.10.1.1}
  * {teid = 0x1201, ip = 10.10.1.2}
* ключи в data_table
  * {teid = 0x3002, ip = 10.10.1.2}
  * {teid = 0x3001, ip = 10.10.1.1}
