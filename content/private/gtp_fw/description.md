---
title : "Схема работы"
description : ""
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/gtp_fw/description.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---

![](../gtp_fw_scheme.jpg)

После чтения [конфигурации](../../../common/config/gtp_forwarder/) на каждую RX очередь создается поток *forwarder*.
В его задачи входит:

* Считывание пакетов из RX очереди сетевого интерфейса
* Подмена dst MAC-адреса на указанный в конфигурации для TX NIC
* Подмена VLAN ID на указанный в конфигурации для TX NIC
* Безусловный forwarding *ARP* и *ICMP* пакетов
* Forwarding пакетов с указанными в конфигурации dst UDP/TCP портами
* Дефрагментация полученных IP пакетов (v4 и v6). Отдельные фрагменты не обрабатываются, пока не будет собран полный
  пакет
* Фрагментация отправляемых IP пакетов в соответствии с заданным MTU
* Проверка UDP портов для определения *GTP-C* и *GTP-U* пакетов
* Ограничение пропускной способности *GTP-C* пакетов с возможностью разделения ограничений по направлениям. Направления
  определяются по IP маскам
* Отправка *GTP-C* пакетов на проверку по правилам и forwarding по их результатам
* Forwarding или drop *GTP-U* пакетов в соответствии с состоянием gtp-туннеля
* Forwarding или drop не *GTP* пакетов в соответствии с конфигурацей

## Обработка GTP-C пакетов

* *forwarder* при получении *GTP-C* пакета направляет их в свою очередь *fwd_to_ate* для обработки по правилам.
* Поток *GTP_PacketsRetriever* непрерывано проверяет очереди *fwd_to_ate*. Полученные пакеты складываются в примитив и
  отправляются в *SLM*
* *SLM* распределяет примитивы по хэндлерам *GTP_C_SL* для дальнейшей обработки:
    * GTP-C пакет декодируется. При ошибках декодирования пакет блокируется.
    * При включенной опции `SeqNumberValidationEnabled` проверятся корреляция *sequence_number* в рамках транзакции.
      Если корреляция нарушена, пакет блокируется.
    * Выполняется проверка сообщения по правилам. При резльутатах `ALLOW` или `TRANSMIT` хэндлер отправляет пакет в
      очередь *ate_to_fwd*. При остальных результатах пакет блокируется.
    * При включенной опции `GtpUSessionValidationEnable` для разрешенных пакетов выполняется создание, обновление или
      удаление [GTP сессии](../sessions/) в соответствии с типом сообщения
* *forwarder* направляет пакеты из очереди *ate_to_fwd* в TX очередь указанного в конфигурации сетевого интерфейса

## Обработка GTP-U пакетов

При включенной опции `GtpUSessionValidationEnable`, выполняется [поиск](../sessions/#session_search)
установленной ранее GTP сессии. В качестве ключа используется F-TEID = `{GTP Header TEID, Dst IP}`.
GTP-U пакет пропускается только если найдена соответствующая сессия.

Если сессия не найдена, то сообщение блокируется, а в ответ отправляется Error Indication со следующей структурой:
* Src MAC = Dst MAC исходного сообщения, Dst MAC = Src MAC исходного сообщения
* Src IP = Dst IP исходного сообщения, Dst IP = Src IP исходного сообщения
* Src UDP Port = Dst UDP Port исходного сообщения, Dst UDP Port = 2152 
* GTP-U Header TEID = 0
* GTP-U Header Sequence Number = 0
* TEID Data I = GTP-U Header TEID исходного сообщения
* GSN Address = Dst IP исходного сообщения

Со структурой Error Indication можно ознакомиться в разделе
7.3.1 [спецификации](https://www.etsi.org/deliver/etsi_ts/129200_129299/129281/16.00.00_60/ts_129281v160000p.pdf).

Если опция `GtpUSessionValidationEnable` выключена, все GTP-U пакеты пропускаются без проверок.
