---
title : "Тестирование"
description : "Схемы тестирования GTP FW"
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/gtp_fw/testing.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---

### Отправка пакетов

Для отправки пакетов по L2 уровню может быть использован [Scapy](https://scapy.readthedocs.io/en/latest/usage.html)

Пример отправки **GTP-C** пакета

```python
data_hex = "4820010600000000065a69000100080052700700725296f24c0006009714203588f54b000800533558115748111056000d001852f010459052f010031182025300030052f0105200010006570009008680464269c11be73d470020000c74657374696e7465726e6574066d6e63303737066d63633235300467707273800001000063000100014f00050001000000004d000700000800000000007f000100004800080000061a8000061a804e0020008080211001000010810600000000830600000000000d00000a000005000010005d002c00490001000557000902848245a269c11be73d50001600680800000000000000000000000000000000000000000300010027720002002100"
data = Raw(bytearray.fromhex(raw_data))

sendp(Ether() / IP(src='127.0.0.1', dst='10.0.0.1') / UDP(sport=5555, dport=2123) / data ,  iface='eth0', count=1)
```

### Функциональные тесты

Для запуска функциональных тестов с помощью [FeatureTester](https://mobiledevelop.git.protei.ru/featureTester/) 
используется отдельная сборка `Protei_SS7FW_gtp_http`, которая включает в себя дополнительный функционал:

1. После анализа **GTP-C** пакета отправляется HTTP POST запрос с телом:

   ```json
   {
     "PDU": {
       "Result": "Pass"
     }
   }
   ```
  
   или
  
   ```json
   {
     "PDU": {
       "Result": "Block"
     }
   }
   ```

   Адрес для отправки запроса указывается **http.cfg** с `DirectionID = 1`

2. При пропуске **GTP-C** пакета по правилам он дополнительно отправляется по L3 уровню на адрес в соответствии с *IP* и *UDP* заголовками 
