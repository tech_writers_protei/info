---
title : "Сборка GTP FW"
description : "Сборка FW с включенным функционалом для обработки GTP пакетов"
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/gtp_fw/build.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---

Сборка проекта Protei_SS7FW осуществляется с включенным флагом `-DBUILD_GTP=ON`

Для сборки используются дополнительные библиотеки, которые подключаются средствами `conan`.

- DPDK v20.05.9
- Packet v1.0.6


Скачивание зависимостей осуществляется автоматически средствами cmake.
Предварительно необходимо указать conan remote и пользователя

```
conan remote add protei_dpi https://conan.protei.ru/artifactory/api/conan/conan-dpi
conan user -p <PASSWORD> -r protei_dpi <USERNAME>
```
