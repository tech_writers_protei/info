---
title : "Формат входящих сообщений"
description : ""
weight : 1
type: docs
---

## SS7

### HTTP_POST_IND STP -> SS7FW

#### json-сообщение STP -> SS7FW

##### Формат

```json
{
  "PDU": {
    "type": "object",
    "properties": {
      "IP_SCTP": {
        "type": "object",
        "properties": {
          "DstIP": {
            "type": "string"
          },
          "DstPort": {
            "type": "integer"
          },
          "SrcIP": {
            "type": "string"
          },
          "SrcPort": {
            "type": "integer"
          }
        }
      },
      "MAP": {
        "type": "object",
        "properties": {
          "IMSI": {
            "type": "string"
          },
          "MSISDN": {
            "type": "string"
          }
        }
      },
      "MTP3": {
        "type": "object",
        "properties": {
          "DPC": {
            "type": "integer"
          },
          "OPC": {
            "type": "integer"
          }
        }
      },
      "MsgID": {
        "type": "integer"
      },
      "SCCP": {
        "type": "object",
        "properties": {
          "CdPA": {
            "type": "object",
            "properties": {
              "Address": {
                "type": "string"
              },
              "NAI": {
                "type": "integer"
              },
              "NP": {
                "type": "integer"
              },
              "RI": {
                "type": "integer"
              },
              "SSN": {
                "type": "integer"
              }
            }
          },
          "CgPA": {
            "type": "object",
            "properties": {
              "Address": {
                "type": "string"
              },
              "NAI": {
                "type": "integer"
              },
              "NP": {
                "type": "integer"
              },
              "RI": {
                "type": "integer"
              },
              "SSN": {
                "type": "integer"
              }
            }
          }
        }
      },
      "TCAP": {
        "type": "object",
        "properties": {
          "AC": {
            "type": "string"
          },
          "DTID": {
            "type": "string"
          },
          "Data": {
            "type": "string"
          },
          "InvokeID": {
            "type": "integer"
          },
          "MessageType": {
            "type": "string"
          },
          "OTID": {
            "type": "string"
          },
          "OpCode": {
            "type": "integer"
          }
        }
      }
    }
  }
}
```

MessageType: `1` -- undirectional; `2` -- begin; `4` -- end; `5` -- continue; `7` -- abort.
Data: буфер tcap_data.
AC, OpCode, InvokeID, IMSI, MSISDN: опциональны, если взяты из сообщения.


###### Пример

```json
{
  "PDU": {
    "MsgID": 1,
    "MTP3": {
      "DPC": 300,
      "OPC": 400
    },
    "SCCP": {
      "CgPA": {
        "Address": "79215301621",
        "NP": 1,
        "TT": 3
      },
      "CdPA": {
        "Address": "79214301622",
        "SSN": 8,
        "NP": 7,
        "TT": 2
      }
    },
    "TCAP": {
      "OTID": "1a101010",
      "MessageType": 2,
      "Data": "62 55 48 04 00 00 00 01 6B 1F 28 1D 06 07 00 11 86 05 01 01 01 A0 12 60 10 80 02 07 80 A1 0A 06 08 04 00 00 01 00 01 00 03 6C 2C A1 2A 02 01 00 02 01 02 30 22 04 08 52 30 85 07 00 00 13 F7 81"
    }
  }
}
```

#### json-сообщение SS7FW -> STP

##### Формат
```
{
  "PDU":{
     "MsgID":{"type":"integer"},
     "Result":{"type":"string"}   {Pass/Block}
     "ReturnUdtsCause": {
       "type":"integer",
       "required": false,
       "description": "Указывает на необходисоть отправки UDTS с причиной ReturnUdtsCause"
     },
     "SCCP":{
       "type":"object",
       "properties":{
         "CdPA":{
           "type":"object",
           "properties":{
             "Address":{"type":"string"},
             "NP":{"type":"integer"},
             "SSN":{"type":"integer"},
             "NAI":{"type":"integer"},
             "RI":{"type":"integer"}
           }
         },
         "CgPA":{"type":"object","properties":{"Address":{"type":"string"},"NP":{"type":"integer"},"SSN":{"type":"integer"},"NAI":{"type":"integer"},"RI":{"type":"integer"}
         }
       }
     },
     "TCAP":{
       "type":"object",
       "properties":{
       "Data":{"type":"string"}  # буфер tcap_data - ответ, сформированный на результате RuleChain (Abort/ReturnError/ReturnResult)
       }
     }   
   }
}
```

-  **Pass** - пропускаем сообщение
-  **Block** - блокируем сообщение

  
##### Пример
```
{
  "PDU": {
    "MsgID": 1,
    "Result": "Block",
    "SCCP": {
      "CdPA": {
        "Address": "79215342637",
        "NAI": 4,
        "NP": 1,
        "RI": 0,
        "SSN": 0,
        "TT": 0
      },
      "CgPA": {
        "Address": "79215342636",
        "NAI": 4,
        "NP": 1,
        "RI": 0,
        "SSN": 0,
        "TT": 0
      }
    },
    "TCAP": {
      "Data": "64 38 49 04 40 00 00 01 6b 26 28 24 06 07 00 11 86 05 01 01 01 a0 19 61 17 a1 09 06 07 04 00 00 01 00 01 03 a2 03 02 01 00 a3 05 a1 03 02 01 00 6c 08 a3 06 02 01 33 02 01 20 "
    }
  }
}
```

## DIAMETER

### HTTP_POST_IND DRA -> SS7FW

#### json-сообщение DRA -> SS7FW

обязательный параметр заголовка: "protocol: diameter"

##### Формат
```
{
 "PDU":{
    "type":"object",
    "properties":{
      "DIAM":{
        "type":"object",
        "properties":{
          "AID":{"type":"string"} # ApplicationID
          "E2E":{"type":"integer"} # End-to-end     
          "HbH":{"type":"integer"} # Hop-by-hop
          "OC":{"type":"integer"}  # OpCode          
          "R":{"type":"integer"}   # lIsRequest(0/1)               
          "AVPList": [
            {
              "Code":{"type":"integer"}, # AVP code
              "Type":{"type":"string"}, #UTF8String/Unsigned32/Enumerated/DiameterIdentity/OctetString/Grouped/...
              "VID":{"type":"integer"}, #VendorID, необязательный, по умолчанию=-1
              "Value":{"type":"string/integer"} # зависит от типа, обязателен для всех типов кроме Type = Grouped
              "AVPList": [ # обязателен для Type = Grouped
                "Code":{"type":"integer"},
                "Type":{"type":"string"},
                "VID":{"type":"integer"}, #VendorID, необязательный
                "Value":{"type":"string/integer"} # зависит от типа, необязательный
                "AVPList": [ # обязателен для Type = Grouped
                  ...
                ]
              ]
            }
          ]
        }
      },
      {
        "IP_SCTP":{
          "type":"object",
          "properties":{
            "SrcIP":{"type":"string"}
            "DstIP":{"type":"string"}    
            "SrcPort":{"type":"integer"}
            "DstPort":{"type":"integer"}
          }
        }
      }
    }
  }
}
```

###### Пример
```
{
 "PDU":{
  "DIAM":{
  "AID":16777251,"AVPList":[
   {"Code":263,"Type":"UTF8String","Value":"diameterbenchmark.protei.ru;1;1;"},
   {"AVPList":[{"Code":266,"Type":"Unsigned32","Value":10415},{"Code":258,"Type":"Unsigned32","Value":16777252}],"Code":260,"Type":"Grouped"},
   {"Code":264,"Type":"DiameterIdentity","Value":"diameterbenchmark.epc.mnc099.mcc250.3gppnetwork.org"},
   {"Code":277,"Type":"Enumerated","Value":1},
   {"Code":296,"Type":"DiameterIdentity","Value":"epc.mnc079.mcc250.3gppnetwork.org"},
   {"Code":293,"Type":"DiameterIdentity","Value":"hss.protei.ru"},
   {"Code":283,"Type":"DiameterIdentity","Value":"protei.ru"},
   {"Code":1,"Type":"UTF8String","Value":"250999990000001"},
   {"Code":1407,"Type":"OctetString","VID":10415,"Value":"52f089"},
   {"Code":1032,"Type":"Enumerated","VID":10415,"Value":1},
   {"Code":1405,"Type":"Unsigned32","VID":10415,"Value":0}],
   "E2E":0,"HBH":-1,"OC":316,"R":1
   },
  "IP_SCTP": {
    "SrcIP": "127.0.0.1",
    "SrcPort": 8000,
    "DstIP": "255.255.0.0",
    "DstPort": 5000
  }
 }
}

```

#### json-сообщение SS7FW -> DRA

##### Формат
```
{
  "PDU":{
     "Result":{"type":"string"}   {Pass/Block}
   }
}
```

-  **Pass** - пропускаем сообщение
-  **Block** - блокируем сообщение

  
##### Пример
```
{
  "PDU": {
    "Result": "Block"
  }
}
```

#### json-сообщение SS7FW <-> SG

[json](https://wiki.protei.ru/doku.php?id=protei:mobile:eir:json)
