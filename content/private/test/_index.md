---
title : "Тестирование"
description : ""
weight : 9
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/test/_index.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---
### **Тестовый стенд 1**

Схема: STP/Скрипты/HTTP_Tester → SS7FW <-> (DB) → EIR(=SG/HTTP_Tester3) → EIR_Tester

* **Скрипты**
  - 192.168.126.76 /usr/protei/mvno/SigtranTester_t2/config/curl_commands
* **HTTP_Tester**
  - 192.168.126.76 /usr/protei/mvno/HTTP_Tester/HTTP_Tester2/HTTP_Tester4 (нагрузочные)
* **EIR_Tester**
  - 192.168.126.153 /usr/protei/ben/SigtranTester.EIR
* **SS7FW**
  - 192.168.126.3 /usr/protei/Protei_SS7FW
  - 192.168.100.61 /home/tarasova/protei/Protei_SS7FW
  - 192.168.126.153 /usr/protei/tarasova/Protei_SS7FW
* **EIR(=SG)**
  - 192.168.126.153 /usr/protei/ben/Protei_EIR
  - 192.168.126.76 /usr/protei/mvno/HTTP_Tester/HTTP_Tester3 (вместо EIR)
* **STP**
  - 192.168.115.232 /usr/protei/EleVin/Protei_STP
* **mysql**
  - 192.168.126.76 Protei_SS7FW/root/elephant
  - 192.168.126.35 Protei_SS7FW/ss7fw/sql


Команда для получения конфигурации сервера: ```lscpu | grep -E '^Thread|^Core|^Socket|^CPU\('```

#### **Тестовый сервер 192.168.126.3**
***Конфигурация сервера:*** 
* CPU info: CPU model:  Westmere E56xx/L56xx/X56xx (Nehalem-C) Total 4, 1 real core(s)
* CPU(s):                4
* Thread(s) per core:    1
* Core(s) per socket:    1
* Socket(s):             4
* Memory info: Total: 7910124 kB

#### **Тестовый сервер 192.168.100.61**
***Конфигурация сервера:*** 
 * CPU info: CPU model:  Intel(R) Core(TM) i3 CPU         540  @ 3.07GHz Total 4, 4 real core(s)
 * CPU(s):              4
 * Thread(s) per core:  2
 * Core(s) per socket:  2
 * Socket(s):           1
 * Memory info: Total: 3900988 kB

#### **Тестовый сервер 192.168.126.153**
***Конфигурация сервера:*** 
* CPU info: CPU model:  Intel Xeon E312xx (Sandy Bridge) Total 4, 1 real core(s)
* CPU(s):                4
* Thread(s) per core:    1
* Core(s) per socket:    1
* Socket(s):             4
* Memory info: Total: 5943936 kB 

#### Сервер mysql 192.168.126.76:
***Конфигурация сервера:*** 
 * CPU info: CPU model:  Intel Xeon E312xx (Sandy Bridge) Total 4, 1 real core(s)
 * CPU(s):                4
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             4
 * Memory info: Total: 3922320 kB

#### Сервер mysql 192.168.126.35(192.168.110.253):
***Конфигурация сервера:*** 
 * CPU info: CPU model:  Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz Total 12, 1 real core(s)
 * CPU(s):                12
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             12
 * Memory info: Total: 16265016 kB


##### **Тест 1**
1. ***Схема:*** HTTP_Tester/HTTP_Tester2 → SS7FW(192.168.126.3) → HTTP_Tester3, ulimit 10000
2. ***Условия теста:*** CoreCount = 4, OpCode = 13, 56 (Транзакция: 2 http-запроса begin-end), Trace(-), CDR(+)
3. ***Результат теста:*** SS7FW_Time(10sec)   : Count = 39015 Average = 9.24368 Min = 0 Max = 194 Q1 = 1 Q2 = 2 Q3 = 8 Q4 = 41 (~2000TPS)

##### **Тест 2**
1. ***Схема:*** HTTP_Tester/HTTP_Tester2/HTTP_Tester4 → SS7FW(192.168.100.61), ulimit 10000
2. ***Условия теста:*** CoreCount = 8, OpCode = 13, 56 (Транзакция: 2 http-запроса begin-end), Trace(-), CDR(+)
3. ***Результат теста:*** SS7FW_Time(10sec)   : Count = 78374 Average = 11.333 Min = 0 Max = 161 Q1 = 0 Q2 = 2 Q3 = 12 Q4 = 58 (~4000TPS)

##### **Тест 2**
1. ***Схема:*** HTTP_Tester/HTTP_Tester2 → SS7FW(192.168.100.61) → HTTP_Tester3, ulimit 10000
2. ***Условия теста:*** CoreCount = 8, OpCode = 2 (Транзакция: 3 http-запроса begin-continue-end, ati-запрос с мин.интервалом в 60сек), Trace(-), CDR(+)
3. ***Результат теста:*** SS7FW_Time(10sec)   : Count = 61068 Average = 9.67711 Min = 0 Max = 193 Q1 = 0 Q2 = 2 Q3 = 9 Q4 = 43 (~2000TPS)

##### **Тест 3**
1. ***Схема:*** HTTP_Tester/HTTP_Tester2 → SS7FW(192.168.100.61), ulimit 10000, DB(192.168.126.76)
2. ***Условия теста:*** CoreCount = 8, MysqlConn = 20, OpCode = 2, (Транзакция: 3 http-запроса begin-continue-end, 2 БД-запроса), Trace(-), CDR(+)
3. ***Результат теста:*** SS7FW_Time(10sec) : Count = 44307 Average = 73.7825 Min = 0 Max = 861 Q1 = 3 Q2 = 28 Q3 = 127 Q4 = 273 (~1450TPS)

##### **Тест 4**
1. ***Схема:*** HTTP_Tester → SS7FW(192.168.100.61) → HTTP_Tester3, ulimit 10000, DB(192.168.126.76)
2. ***Условия теста:*** CoreCount = 8, MysqlConn = 20, OpCode = 2, (Транзакция: 3 http-запроса begin-continue-end, 2 БД-запроса, ati-запрос с мин.интервалом в 60сек), Trace(-), CDR(+)
3. ***Результат теста:*** SS7FW_Time(10sec) : Count = 32705 Average = 100.336 Min = 0 Max = 814 Q1 = 2 Q2 = 28 Q3 = 203 Q4 = 354 (~1100TPS) 

##### **Тест 5**
1. ***Схема:*** HTTP_Tester/HTTP_Tester4 → SS7FW(192.168.100.61)/SS7FW(192.168.126.153) → HTTP_Tester3, ulimit 10000, DB(192.168.126.35)
2. ***Условия теста:*** CoreCount = 8, MysqlConn = 25, OpCode = 2, (Транзакция: 3 http-запроса begin-continue-end, 2 БД-запроса, ati-запрос с мин.интервалом в 60сек), Trace(-), CDR(+)
3. ***Результат теста:***
-  SS7FW_Time(10sec)   : Count = 47572 Average = 13.7472 Min = 0 Max = 708 Q1 = 2 Q2 = 6 Q3 = 15 Q4 = 48 (~1500TPS) 
-  SS7FW_Time(10sec)   : Count = 44886 Average = 8.46181 Min = 0 Max = 65 Q1 = 2 Q2 = 6 Q3 = 12 Q4 = 24 (~1500TPS)


### **Тестовый стенд 2 SS7FW Version 1.0.9.0**

Схема: RAW_Tester/SigTester - STP → SS7FW <-> (DB) → EIR(=HTTP_Tester3)

* **SS7FW**
  - 192.168.77.54 /usr/protei/Protei_SS7FW
* **STP**
  - 192.168.77.104 /usr/protei/Protei_STP
* **mysql**
  - 192.168.110.253 Protei_SS7FW/ss7fw/sql
* **RAW_Tester**
  - 192.168.77.105 /usr/protei/RAW_Tester
* **HTTP_Tester**
  - 192.168.126.76 /usr/protei/mvno/HTTP_Tester3 (прием ATI)

Команда для получения конфигурации сервера: ```lscpu | grep -E '^Thread|^Core|^Socket|^CPU\('```

#### Сервер 192.168.126.76:
***Конфигурация сервера:*** 
 * CPU info: CPU model:  Intel Xeon E312xx (Sandy Bridge) Total 4, 1 real core(s)
 * CPU(s):                4
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             4
 * Memory info: Total: 3922320 kB

#### Сервер mysql 192.168.110.253:
***Конфигурация сервера:*** 
 * CPU info: CPU model:  Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz Total 12, 1 real core(s)
 * CPU(s):                12
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             12
 * Memory info: Total: 16265016 kB

#### Сервер mysql 192.168.77.54:
***Конфигурация сервера:*** 
 * CPU Info: CPU model:  Intel Xeon E312xx (Sandy Bridge) Total 4, 1 real core(s)
 * CPU(s):                4
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             4
 * Memory info: Total: 8199332 kB

 #### Сервер mysql 192.168.77.104:
***Конфигурация сервера:*** 
 * CPU Info: CPU model:  Intel Xeon Processor (Skylake, IBRS) Total 8, 1 real core(s)
 * CPU(s):                8
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             8
 * Memory info: Total: 3879304 kB

 #### Сервер mysql 192.168.77.105:
***Конфигурация сервера:*** 
 * CPU Info: CPU model:  Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz Total 4, 1 real core(s)
 * CPU(s):                4
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             4
 * Memory info: Total: 3879908 kB 

 #### Сервер mysql 192.168.77.106:
***Конфигурация сервера:*** 
 * CPU Info: CPU model:  Intel(R) Xeon(R) Gold 6130 CPU @ 2.10GHz Total 4, 1 real core(s)
 * CPU(s):                4
 * Thread(s) per core:    1
 * Core(s) per socket:    1
 * Socket(s):             4
 * Memory info: Total: 3879916 kB
 


##### **Тест 1 UL**
1. ***Схема:*** RAW_Tester(192.168.77.105) → STP(192.168.77.104) - SS7FWSS7FW(192.168.77.54/4core) → SigTester(192.168.77.106)ulimit 10000, DB(192.168.110.253/25conn)
2. ***Условия теста:*** CoreCount = 4, MysqlConn = 25, OpCode = 2, (Транзакция: 2 http-запроса begin-end, 2 БД-запроса), Trace(-), CDR(+)
- MessagesFile  = "config/RawMessage/UL.msg";
- MultiMsg=2000;
- ChangeByte=109; # vlr
- SendDataInterval = 50;
- SendCount=65;
3. ***Результат теста:***
- 2600 http-msg (1300 UL Begin, 1300 UL Ack, VelocityCheck (without ATI), 1300 sql select, 1300 sql insert)
- SS7FW_Time(10sec)   : Count = 26002 Average = 13.5158 Min = 0 Max = 148 Q1 = 4 Q2 = 11 Q3 = 21 Q4 = 33
- insert into Tm_IMSI (strIMSI, nHT, strHost, dtReg)values ("2... : count:13002
- select nHT, strHost, dtReg from Tm_IMSI where strIMSI = "203... : count:12998

##### **Тест 2 CL**
1. ***Схема:*** RAW_Tester(192.168.77.105) → STP(192.168.77.104) - SS7FWSS7FW(192.168.77.54/4core) → SigTester(192.168.77.106)ulimit 10000, DB(192.168.110.253/25conn)
2. ***Условия теста:*** CoreCount = 4, OpCode = 3, (Транзакция: 2 http-запроса begin-end), Trace(-), CDR(+)
- MessagesFile  = "config/RawMessage/CL.msg";
- MultiMsg=2000;
- ChangeByte=85; # imsi
- SendDataInterval = 25;
- SendCount=70;
3. ***Результат теста:***
- 5600 http-msg (2800 CL Begin, 2800 CL Ack)
- SS7FW_Time(10sec)   : Count = 56217 Average = 4.87029 Min = 0 Max = 105 Q1 = 1 Q2 = 2 Q3 = 6 Q4 = 17

##### **Тест 3 SS**
1. ***Схема:*** RAW_Tester(192.168.77.105) → STP(192.168.77.104) - SS7FWSS7FW(192.168.77.54/4core) → SigTester(192.168.77.106)ulimit 10000, DB(192.168.110.253/25conn)
2. ***Условия теста:*** CoreCount = 4, MysqlConn = 25, OpCode = 12, (Транзакция: 2 http-запроса begin-end, 2 БД-запроса), Trace(-), CDR(+)
- MessagesFile  = "config/RawMessage/ActivateSS1.msg";
- MultiMsg=2000;
- ChangeByte=25; # cgpa
- SendDataInterval = 25;
- SendCount=50;
3. ***Результат теста:***
- 4000 http-msg (2000 SS Begin, 2000 SS Ack, SendATI(ValidityPeriod=10sec), 1500 sql select, 20 sql insert(10 sec))
- SS7FW_Time(10sec)   : Count = 39705 Average = 11.1517 Min = 0 Max = 201 Q1 = 2 Q2 = 6 Q3 = 13 Q4 = 42
- insert into Tm_IMSI (strIMSI, nHT, strHost, dtReg)... : count:199
- select nHT, strHost, dtReg from Tm_IMSI where strI... : count:15305



