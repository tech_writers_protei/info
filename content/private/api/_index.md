---
title : "API"
description : ""
weight : 1
type: docs
---
#### Получение абонента из базы

##### Запрос
URL: **SS7FW/GET_SUBSCRIBER**

```
{
    "IMSI" : "251028625251136"
}
```
или

```
{
    "MSISDN" : "79213280505"
}
```

##### Формат ответа
```
{
	"Result":"Success",
	"IMSI": "5554443332221110",
	"DtLA": "123456789",
	"Subscriber": [ 
	{
		"MSISDN": "77665544332",
		"HT": "1",
		"Host": "998877665",
		"dtReg": 6543234242,
		"NetworkAreaID": 5,
		"LocationID": 6,
		"Latitude": 10.001,
		"Longitude": 50.002,
		"PLMN": "11223",
		"OR": "da.ia.realm"
	}, ...
	]
}
```

##### Описание параметров
 * IMSI - Номер IMSI
 * DtLA -  Время последней активности абонента unix timestamp (секунды с 1 января 1970)
 * Msisdn - Номер MSISDN
 * nHT - Тип (1 - VLR, 2 - SGSN, 3 - MME)
 * Host - GT, OrigHost
 * dtReg - Время регистрации в формате unix timestamp (секунды с 1 января 1970)
 * NetworkAreaID - id сетевой зоны абонента
 * LocationID - id местоположения абонента
 * Latitude/Longitude - широта/долгота местоположения абонента
 * PLMN - PLMN (заполняется для HT=3)
 * OR - OrigRealm (заполняется для HT=3)


#### Получение SRI4SM параметров из БД

##### Запрос
URL: **SS7FW/GET_SRI4SM**

```
{
    "IMSI" : "102030405060708",
    "SMSC" : "7777777777",
}
```


##### Формат ответа
```
{
	"Result": "Success",
	"SRI4SM_Data":{ 
		"MSC" : "888888888",
		"MSISDN" : "987654321"
	}		
}	
```

##### Описание параметров
 * MSC - Адрес обслуживающего узла MSC
 * MSISDN - Номер MSISDN

