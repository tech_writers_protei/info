---
title : "Хранение данных"
description : ""
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/db/_index.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---
### Регистрация абонента (redis)

#### Ключи
Для получения списка регистраций и времени последней активности абонента используется ключ IMSI с префиксом user

#### Параметры
Значения по ключу хранятся в формате JSON. 
Для корректной работы с данным форматом необходима установка дополнительного модуля *RedisJSON*.
* В случае работы с *Redis* используется последняя на момент написания версия - 2.6.8
* В случае работы с *KeyDB* используется версия 2.0, так как она является минимальной необходимой для корректной работы модуля *RediSearch*

Для поиска записей по "значению" используется модуль *RediSearch*, например, для поиска данных об абоненте по его *Msisdn*.
* В случае работы с *Redis* используется последняя на момент написания версия - 2.8.9
* В случае работы с *KeyDB* используется версия 2.2.11, с внесенным в ее исходный код небольших изменений, предотвращених падения базы при попытке загрузки модуля. Данная версия была выбрана из-за того, что в этой версии была добавлена возможность индексации по *JSON*. 

Исправленную версию модуля RediSearch для различных дистрибутивов можно найти [тут](https://jenkins.protei.ru/job/team4/job/Routing/job/Protei_SS7FW/job/Project_Utility/job/ModulesKeyDB/job/RediSearch_KeyDB/).

Модуль RedisJSON для различных дистрибутивов можно найти [тут](https://jenkins.protei.ru/job/team4/job/Routing/job/Protei_SS7FW/job/Project_Utility/job/ModulesKeyDB/job/RedisJSON_KeyDB/).

Для подключения данных модулей к *KeyDB/Redis* в конфигурационный файл, использующийся при запуске, необходимо добавить следующие строки: *loadmodule path/to/rejson.so* и *loadmodule path/to/redisearch.so*. Если запуск осуществляется без использования конфигурационного файла, то в команду по запуску необходимо добавить ключ *--loadmodule* со значением *path/to/rejson.so*. Пример запуска *Redis* с подключением модулей через ключи: *redis-server --loadmodule path/to/rejson.so --loadmodule path/to/redisearch.so*. 

Описание ключей представлено ниже.

| Имя ключа     | Тип        | Присутствие | Описание                                                                     |
|---------------|------------|-------------|------------------------------------------------------------------------------|
| IMSI          | string     | M | IMSI                                                                                   |
| LA            | int        | M | Время последней активности абонента в формате unix timestamp (секунды с 1 января 1970) |
| VLR           | JsonObject | O | Информация о регистрации на VLR                                                        |
| SGSN          | JsonObject | O | Информация о регистрации на SGSN                                                       |
| MME           | JsonObject | O | Информация о регистрации на MME                                                        |

Информация о регистрации на различных хостах также представлена в формате JSON. Описание ключей представлено ниже.


| Имя ключа     | Тип    | Присутствие | Описание                                                             |
|---------------|--------|-------------|----------------------------------------------------------------------|
| HT            | int    | M           | Тип (1 - VLR, 2 - SGSN, 3 - MME)                                     |
| Host          | string | M           | GT, OrigHost                                                         |
| Date          | int    | M           | время регистрации в формате unix timestamp (секунды с 1 января 1970) |
| Msisdn        | string | M           | MSISDN данной регистрации                                            |
| NaID          | int    | M           | Идентификатор Network Area                                           |
| LocID         | int    | M           | Идентификатор Location                                               |
| Latitude      | double | M           | Координаты: широта                                                   |
| Longitude     | double | M           | Координаты: долгота                                                  |
| PLMN          | string | M           | PLMN (заполняется для HT=3)                                          |
| Realm         | string | M           | OrigRealm (заполняется для HT=3)                                     |

Используемые обозначения:
* M - Mandatory (Обязательный)
* O - Optional  (Опциональный)

#### Пример

Минимальный набор данных об абоненте:
```
JSON.GET user:66650000000014

{
  "IMSI": "66650000000014",
  "LA": 1702479243
}
```

Обычный набор данных об абоненте:
```
JSON.GET user:44450000000006
{
  "LA": 1702479233,
  "IMSI": "44450000000006",
  "SGSN": {
    "HT": 2,
    "Host": "213661943306",
    "Msisdn": "",
    "Date": 1667393149,
    "NaID": 3,
    "LocID": 37,
    "Latitude": 0,
    "Longitude": 0,
    "PLMN": "",
    "Realm": ""
  },
  "VLR": {
    "HT": 1,
    "Host": "937300000005",
    "PLMN": "",
    "Date": 1702479241,
    "NaID": 1,
    "Realm": "",
    "LocID": 35,
    "Msisdn": "",
    "Latitude": 34.5328,
    "Longitude": 69.1658
  }
}
```
#### Параметры запросов SRI4SM

Для получения параметров SRI4SM используется ключ IMSI_SMSC с префиксом sms
Информация параметрах SRI4SM представлена в формате JSON. Описание ключей представлено ниже.

| Имя ключа | Тип    | Присутствие | Описание              |
|-----------|--------|-------------|-----------------------|
| IMSI      | string | M           | IMSI                  |
| SMSC      | string | M           | ServiceCentreAddress  |
| MSISDN    | string | O           | MSISDN                |
| MSC       | string | O           | MSC-number            |

#### Пример
```
JSON.GET sms:250439592974091_79262000147

{
"IMSI": "250439592974091",
"SMSC": "79262000147",
"MSISDN" : "79592974091",
"MSC" : "79585411158"
}
```

#### TCAP transaction info

| Название           | Тип      | Описание                                            |
|--------------------|----------|-----------------------------------------------------|
| TrID               | uint64_t | Transaction ID                                      |
| TcapStartTime      | uint64_t | время начала транзакции, µs                         |
| PrevMessageType    | int      | Тип предыдущего сообщения (STP_BEGIN, STP_CONTINUE) |
| Imsi               | string   | IMSI                                                |
| Msisdn             | string   | MSISDN                                              |
| HT                 | int      | Host type: 1 - VLR, 2 - SGSN, 3 - MME               |
| Host               | string   | GT, Origin host                                     |
| dtTransactionBegin | int      | время начала транзакции в формате unix timestamp    |
| UpdateHost         | string   | Origin Host or ATI host                             |
| HasDialogue        | bool     | Наличие диалоговой части                            |
| Version            | int      | TCAP version                                        |
| AC                 | string   | Application context                                 |
| OpCode             | int      | Код операции                                        |
| CgPA               | string   | Calling number                                      |
| NaID               | int      | Network Area ID                                     |
| LocID              | int      | Location ID                                         |
| Latitude           | double   | Координаты: широта                                  |
| Longitude          | double   | Координаты: долгота                                 |
| SRI4SM_SMSC        | string   | Параметр SMSC из сообщения  SendRoutingInfoForSM    |

#### DIAM transaction info

| Название           | Тип      | Описание                                         |
|--------------------|----------|--------------------------------------------------|
| TrID               | uint64_t | Transaction ID                                   |
| DiamStartTime      | uint64_t | время начала транзакции, µs                      |
| OpCode             | int      | Код операции                                     |
| UserName           | string   | IMSI                                             |
| OH                 | string   | Origin host                                      |
| dtTransactionBegin | int      | время начала транзакции в формате unix timestamp |
| NaID               | int      | Network Area ID                                  |
| LocID              | int      | Location ID                                      |
| Latitude           | double   | Координаты: широта                               |
| Longitude          | double   | Координаты: долгота                              |
| strPLMN            | string   | PLMN                                             |
| strRealm           | string   | Origin Realm                                     |


#### GTP session {#gtp-session}

Описание JSON структуры GTP сессии

| Имя ключа          | Тип        | O/M | Описание                    |
|--------------------|------------|-----|-----------------------------|
| sgw                | JsonObject | M   | Информация об узле SGW/SGSN |
| pgw                | JsonObject | M   | Информация об узле PGW/GGSN |
| creation_timestamp | uint32_t   | M   | Время создания сессии, s    |

Описание JSON структуры информации об узлах

| Имя ключа   | Тип        | O/M | Описание                                   |
|-------------|------------|-----|--------------------------------------------|
| **cp**      | JsonObject | M   | Информация о ControlPlane F-TEID           |
| teid        | int        | M   | TEID для отправки ControlPlane сообщений   |
| ip          | string     | M   | Dst IP для отправки ControlPlane сообщений |
| **bearers** | JsonArray  | M   | Информация о UserPlane Bearer контекстах   |
| id          | int        | M   | Bearer ID для контекста                    |
| teid        | int        | M   | TEID для отправки UserPlane сообщений      |
| ip          | string     | M   | Dst IP для отправки UserPlane сообщений    |

**Примечание**. `ip` хранится в виде hex строки, которая кодирует байтовое представление ip в network order.
Для IPv4 используется 8 символов (4 байта), для IPv6 - 32 символа (16 байт)

#### Пример

```json
JSON.GET gtp_session:4353_01010a0a
{
  "sgw": {
    "cp": {
      "teid": 4353,
      "ip": "01010a0a"
    },
    "bearers": [
      {
        "teid": 8449,
        "ip": "01010a0a",
        "id": 6
      }
    ]
  },
  "pgw": {
    "cp": {
      "teid": 4609,
      "ip": "02010a0a"
    },
    "bearers": [
      {
        "teid": 8705,
        "ip": "02010a0a",
        "id": 6
      }
    ]
  }
}
```


