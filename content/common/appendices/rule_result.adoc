---
title: "Статусы проверки"
description: "Перечень возможных результатов проверки сообщений цепочкой правил"
weight: 30
type: docs
draft: true
---
[[rule-result]]
// tag::rule-result[]
.Результат обработки правилами
[options="header",cols="1,2,5"]
|===
|Код |Результат |Описание 

|0 |RESULT_UNKNOWN |Результат неизвестен
|1 |RESULT_ALLOW |Продолжить обработку
|2 |RESULT_REJECT |Блокировать, отправить ответ TCAP_ABORT
|3 |RESULT_TRANSMIT |Пропустить дальше
|5 |RESULT_SILENT_DROP |Блокировать без оповещения
|6 |RESULT_RETURN_RESULT |Продолжить обработку, отправить ответ TCAP_END, RETURN_RESULT
|7 |RESULT_RETURN_ERROR |Блокировать, отправить ответ TCAP_END, RETURN_ERROR(ErrorCode)
|[line-through]#8# |[line-through]#RESULT_LOCAL_LOOP# |[line-through]#Блокировать ввиду зацикливания правил, отправить ответ TCAP_ABORT#
|11 |RESULT_SEND_ATI |Отправить запрос MAP-Any-Time-Interrogation
|12 |RESULT_VELOCITY_CHECK |Проверить скорость перемещения
|13 |RESULT_LOCATION_CHECK |Проверить актуальность местоположения
|14 |RESULT_RETURN_UDTS |Продолжить обработку, отправить ответ RETURN_UDTS(ReturnCause)
|===
// end::rule-result[]