---
title: "Rule"
description: "Параметры правила Rule"
weight: 30
type: docs
draft: true
---

**Rule** задает правила маршрутизации сообщений.

### Описание параметров ###

| Параметр                                    | Описание                                                                                                                                                                                                                                                                                                                                                                                      | Тип          | O/M | P/R | Версия |
|---------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|-----|-----|--------|
| ID                                          | Уникальный идентификатор правила маршрутизации.                                                                                                                                                                                                                                                                                                                                               | int          | O   | R   |        |
| Name                                        | Уникальное имя правила маршрутизации.                                                                                                                                                                                                                                                                                                                                                         | string       | M   | R   |        |
| Type                                        | Тип узла маршрутизации. Должно быть `Rule`.                                                                                                                                                                                                                                                                                                                                                   | string       | M   | R   |        |
| Category                                    | Категория правила. Диапазон: 1-3.                                                                                                                                                                                                                                                                                                                                                             | int          | O   | R   |        |
| AttackType                                  | Вид атаки.                                                                                                                                                                                                                                                                                                                                                                                    | string       | O   | R   |        |
| Key                                         | Перечень ключей для задания адресации.                                                                                                                                                                                                                                                                                                                                                        | \[object\]   | O   | R   |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| &nbsp;&nbsp;ID                              | Уникальный идентификатор условия.                                                                                                                                                                                                                                                                                                                                                             | int          | O   | R   |        |
| &nbsp;&nbsp;<a name="field">Field</a>       | Анализируемый параметр.                                                                                                                                                                                                                                                                                                                                                                       | string       | O   | R   |        |
| &nbsp;&nbsp;<a name="operator">Operator</a> | Оператор, задающий отношение для параметра [Field](#field).                                                                                                                                                                                                                                                                                                                                   | string       | O   | R   |        |
| &nbsp;&nbsp;<a name="value">Value</a>       | Значение, используемое оператором [Operator](#operator) для сравнения или проверки параметра [Field](#field).                                                                                                                                                                                                                                                                                 | string/regex | O   | R   |        |
| &nbsp;&nbsp;Type                            | Тип значения [Value](#value).<br>`Int` -- числовой тип;<br>`String` -- строковый тип;<br>`Field` -- название параметра;<br>`List` -- имя файла с префиксамии;<br>`Mask_list` - имя файла с перечнем регулярных выражений;<br>`Vocabulary` -- имя файла с перечнем стоп-слов;<br>`Tag` -- название тега;<br>`Network` -- имя сетевой зоны;<br> `Current_network_area` -- текущая сетевая зона. | string       | O   | R   |        |
| &nbsp;&nbsp;Condition                       | Условие связи с предыдущим блоком.<br>`AND` -- логическое И;<br>`OR` -- логическое ИЛИ;<br>`NOT` -- логическое НЕ. `<Condition1> NOT <Condition2> = <Condition1> AND (NOT <Condition2>)`<br>По умолчанию: AND.<br>**Примечание.** При задании первого блока считается, что нулевой блок всегда True.                                                                                          | string       | O   | R   |        |
| &nbsp;&nbsp;Tag                             | Теги сообщения.                                                                                                                                                                                                                                                                                                                                                                               | \[string\]   | O   | R   |        |
| &nbsp;&nbsp;Option                          | Дополнительный атрибут параметра [Field](#field).<br>`Length` -- количество символов;<br>`Prefix` -- префикс;<br>`Postfix` -- постфикс;<br>`Network` -- сетевая зона.                                                                                                                                                                                                                         | string       | O   | R   |        |
| &nbsp;&nbsp;SizeOfPrefix                    | Длина префикса.<br>**Примечание.** Используется только при `Option = Prefix`.                                                                                                                                                                                                                                                                                                                 | int          | O   | R   |        |
| &nbsp;&nbsp;Start                           | Порядковый номер символа, с которого начинается постфикс.<br>**Примечание.** Используется только при `Option = Postfix`.                                                                                                                                                                                                                                                                      | int          | O   | R   |        |
| **}**                                       |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| Threshold                                   | Параметры порога.                                                                                                                                                                                                                                                                                                                                                                             | object       |     |     |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| &nbsp;&nbsp;Param                           | Параметр, для которого устанавливается пороговое значение.                                                                                                                                                                                                                                                                                                                                    | string       | O   | R   |        |
| &nbsp;&nbsp;Limit                           | Пороговое значение.                                                                                                                                                                                                                                                                                                                                                                           | int          | O   | R   |        |
| &nbsp;&nbsp;Interval                        | Интервал проверки достижения порогового значения, в секундах.                                                                                                                                                                                                                                                                                                                                 | int          | O   | R   |        |
| *}*                                         |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| [Actions](#actions)                         | Дальнейшие осуществляемые действия.                                                                                                                                                                                                                                                                                                                                                           | \[object\]   | M   | R   |        |
| **{**                                       |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| &nbsp;&nbsp;Name                            | Название действия.                                                                                                                                                                                                                                                                                                                                                                            | string       | O   | R   |        |
| &nbsp;&nbsp;Data                            | Дополнительная информация.                                                                                                                                                                                                                                                                                                                                                                    | object       | O   | R   |        |
| **}**                                       |                                                                                                                                                                                                                                                                                                                                                                                               |              |     |     |        |
| Next                                        | Название узла, куда перенаправляется запрос.<br>**Примечание.** Должно совпадать с именем существующего правила/маршрутизатора/"OUT".                                                                                                                                                                                                                                                         | string       | M   | R   |        |

### Операторы для анализа параметров {#operators}

| Оператор                | Описание                                                                                                                   |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------|
| `==` / `!=`             | Оператор "равно" используется для сравнения значения параметра с точным значением или значением другого параметра.         | 
| `~` / `!~`              | Оператор "эквивалентно" используется для проверки попадания под условия маски.                                             |
| `in`                    | Оператор "включено" используется для проверки наличия значения в списке или словаре.                                       |
| `>` / `<` / `>=` / `<=` | Оператор "меньше/больше" используется для сравнения значения параметра с точным значением или значением другого параметра. |

### Доступные действия {#actions}

#### Передать сообщение в сеть без обработки, Transmit

```json
{ "Name": "Transmit" }
```

#### Остановить обработку сообщения, проверить актуальность информации о местоположении, Location Check

```json
{
  "Name": "LocationCheck",
  "Data": {
    "AtiAllowed": 1,
    "EnablePaging": 1,
    "ValidityPeriod": 10000,
    "PLMNSearch": 1
  }
}
```

| Параметр       | Описание                                            | Тип  |
|----------------|-----------------------------------------------------|------|
| AtiAllowed     | Флаг разрешения запроса MAP-Any-Time-Interrogation. | bool |
| EnablePaging   | Флаг включения пейджинга.                           | bool |
| ValidityPeriod | Время жизни сообщения, в секундах.                  | int  |
| PLMNSearch     | Флаг, указывающий на поле для поиска сетевой зоны.  | bool |

#### Прекратить обработку сообщения и ответить отбоем, Reject

```json
{ "Name": "Reject" }
```

#### Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_ERROR с кодом ошибки ErrorCode, ReturnError

```json
{
  "Name": "ReturnError",
  "Data": {
    "ErrorCode": 0
  }
}
```

| Параметр                       | Описание                 | Тип |
|--------------------------------|--------------------------|-----|
| [ErrorCode](#rule-result-rule) | Код возвращаемой ошибки. | int |

#### Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_RESULT, ReturnResult

```json
{ "Name": "ReturnResult" }
```

**Примечание.** При получении во время обработки MAP-Provide-Subscriber-Information формируется и отправляется ответное JSON-сообщение в виде raw-буфера. 
Значения параметров в ответе настраиваются в секции [ss7fw.cfg::\[PSI\]](../../ss7fw/#psi) и с помощью параметра [ss7fw::\[General\]::PLMN](../../ss7fw/#plmn).

#### Прекратить обработку сообщения и ответить сообщением UDTS c причиной ReturnCause, ReturnUDTS

```json
{
  "Name": "ReturnUDTS",
  "Data": {
    "ReturnCause": 0
  }
}
```

| Параметр                         | Описание                 | Тип |
|----------------------------------|--------------------------|-----|
| [ReturnCause](#rule-result-rule) | Причина отбоя сообщения. | int |

**Примечание.** В ответе на STP в JSON добавляется поле `ReturnUdtsCause`. 
Наличие этого поля указывает на необходимость отправки UDTS.

#### Отправить запрос MAP-Any-Time-Interrogation, SendATI

```json
{
  "Name": "SendATI",
  "Data": {
    "EnablePaging": 1,
    "ValidityPeriod": 10000
  }
}
```

| Параметр       | Описание                           | Тип  |
|----------------|------------------------------------|------|
| EnablePaging   | Флаг включения пейджинга.          | bool |
| ValidityPeriod | Время жизни сообщения, в секундах. | int  |

#### Отправить запрос SRI4SM, SendSRI4SM

```json
{
  "Name": "SendSRI4SM",
  "Data": {
    "EnablePaging": 0,
    "ValidityPeriod": 3600,
    "SendAtiIfSri4smNotPossible": 0
  }
}
```

| Параметр                   | Описание                                                                                                                                                 | Тип   |
|----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|-------|
| EnablePaging               | Флаг включения пейджинга.                                                                                                                                | bool  |
| ValidityPeriod             | Период годности сообщения, в секундах.                                                                                                                   | int   |
| SendAtiIfSri4smNotPossible | Флаг отправки запроса MAP-Any-Time-Interrogation при невозможности отправки MAP-Send-Routing-Information-For-SM ввиду отсутствия номера MSISDN абонента. | bool  |

#### Отправить оповещение с аварией и продолжить обработку сообщения, SendAlarm

```json
{ "Name": "SendAlarm" }
```

#### Задать сообщению тег, SetTag

```json
{
  "Name": "SetTag",
  "Data": {
    "TagName1": "tag_1",
    "TagNameN": "tag_N"
  }
}
```

| Параметр | Описание           | Тип    |
|----------|--------------------|--------|
| TagName  | Наименование тега. | string |

#### Прекратить обработку без отправки ответа, SilentDrop

```json
{ "Name": "SilentDrop" }
```

#### Перейти к определенному правилу, GOTO

```json
{
  "Name": "GOTO",
  "Data": "<rule_name>"
}
```

#### Задать параметру значение, SetParam

```json
{
  "Name": "SetParam",
  "Data": {
    "paramName": {
      "variable": "GT_B_Address"
    },
    "valueType": "Field",
    "paramValueField": {
      "variable": "GT_A_Address"
    },
    "operator": "DelPrefix",
    "operatorValueString": {
      "VAL": "5"
    }
  }
}
```

| Параметр                            | Описание                                                                                                        | Тип    |
|-------------------------------------|-----------------------------------------------------------------------------------------------------------------|--------|
| paramName                           | Имя параметра. Формат: `"paramName": { "variable": "<variable>" }`.                                             | object |
| <a name="value">paramValueField</a> | Имя параметра, значение которого используется. Формат: `"paramValueField": { "variable": "<variable>" }`.       | object |
| variable                            | Имя переменной.                                                                                                 | string |
| valueType                           | Тип значения. `String` / `Int` / `Field`.                                                                       | string |
| paramValueString                    | Задаваемое значение параметра.                                                                                  | string |
| <a name="operator">operator</a>     | Действие со значением [paramValueField](#value). `DelPrefix` / `AddPrefix`.                                     | string |
| operatorValueString                 | Дополнительные параметры действия [operator](#operator). Формат: `"operatorValueString": { "VAL": "<value>" }`. | object |
| VAL                                 | Значение параметра действия [operator](#operator).                                                              | string |

#### Проверить скорость перемещения, VelocityCheck

```json
{
  "Name": "VelocityCheck",
  "Data": {
    "AtiAllowed": 1,
    "EnablePaging": 1,
    "ValidityPeriod": 10000,
    "PLMNSearch": 1
  }
}
```

| Параметр       | Описание                                            | Тип  |
|----------------|-----------------------------------------------------|------|
| AtiAllowed     | Флаг разрешения запроса MAP-Any-Time-Interrogation. | bool |
| EnablePaging   | Флаг включения пейджинга.                           | bool |
| ValidityPeriod | Время жизни сообщения, в секундах.                  | int  |
| PLMNSearch     | Флаг, указывающий на поле для поиска сетевой зоны.  | bool |

#### Преобразовать данные, ConvertTo

```json
{
  "Name": "ConvertTo",
  "Data": {
    "Type": 1,
    "SMPP_Direction": 0
  }
}
```

| Параметр       | Описание                                                      | Тип |
|----------------|---------------------------------------------------------------|-----|
| Type           | Код типа данных, к которому необходимо конвертировать данные. | int |
| SMPP_Direction | Идентификатор направления SMPP.                               | int |

### Параметры, доступные для анализа

| Name                                     | Description                                                                              | Тип             | Версия     |
|------------------------------------------|------------------------------------------------------------------------------------------|-----------------|------------|
| GT_A_Address                             | Адрес SCCP <abbr title="Global Title">GT</abbr> отправителя.                             | regex           |            |
| GT_A_NP                                  | Номер <abbr title="Numbering Plan">NP</abbr> отправителя.                                | int             |            |
| GT_A_SSN                                 | Номер <abbr title="Subsystem Number">SSN</abbr> отправителя.                             | int             |            |
| GT_A_TT                                  | Номер <abbr title="Translation Type">TT</abbr> отправителя.                              | int             |            |
| GT_A_NAI                                 | Номер <abbr title="Nature of Address Indicator">NAI</abbr> отправителя.                  | int             |            |
| GT_A_ES                                  | Номер <abbr title="Encoding Scheme">ES</abbr> отправителя.                               | int             |            |
| GT_B_Address                             | Адрес SCCP <abbr title="Global Title">GT</abbr> получателя.                              | regex           |            |
| GT_B_NP                                  | Номер <abbr title="Numbering Plan">NP</abbr> получателя.                                 | int             |            |
| GT_B_SSN                                 | Номер <abbr title="Subsystem Number">SSN</abbr> получателя.                              | int             |            |
| GT_B_TT                                  | Номер <abbr title="Translation Type">TT</abbr> получателя.                               | int             |            |
| GT_B_NAI                                 | Номер <abbr title="Nature of Address Indicator">NAI</abbr> получателя.                   | int             |            |
| GT_B_ES                                  | Номер <abbr title="Encoding Scheme">ES</abbr> получателя.                                | int             |            |
| MAP_IMSI                                 | Номер IMSI в MAP-запросах.                                                               | regex           |            |
| MAP_MSISDN                               | Номер MSISDN в MAP-запросах.                                                             | regex           |            |
| MAP_SMSC                                 | Адрес узла SMSC в MAP-запросах.                                                          | regex           |            |
| MAP_MSC                                  | Адрес узла MSC в MAP-запросах.                                                           | regex           |            |
| MAP_MLC                                  | Адрес узла MLC в MAP-запросах.                                                           | string          | 1.1.0.27   |
| MAP_VLR                                  | Адрес узла VLR в MAP-запросах.                                                           | regex           |            |
| MAP_SGSN                                 | Адрес узла SGSN в MAP-запросах.                                                          | regex           |            |
| MAP_AC                                   | Имя контекста приложения, <abbr title="Application Context Name">ACN</abbr>.             | string          |            |
| MAP_REQ_PARAM                            | Параметры Request Parameters.                                                            | array\{int\}*   |            |
| MAP_OCSI_SCF                             | Адрес gsmSCF с профилем O-CSI.                                                           | regex           |            |
| MAP_HLR                                  | Адрес узла HLR.                                                                          | regex           |            |
| SMS_OA                                   | Адрес отправителя SMS-сообщения, `SM-RP-OA`.                                             | string          | 1.1.0.12   |
| SMS_OA_NPI                               | Индикатор <abbr title="Numbering Plan Indicator">NPI</abbr> отправителя, `SM–RP–OA–NPI`. | int             | 1.1.0.12   |
| SMS_OA_TON                               | Тип <abbr title="Type of Numbering">TON</abbr> отправителя, `SM-RP-OA-TON`.              | int             | 1.1.0.12   |
| SMS_DA                                   | Адрес получателя SMS-сообщения, `SM-RP-DA`.                                              | string          | 1.1.0.12   |
| SMS_DA_NPI                               | Индикатор <abbr title="Numbering Plan Indicator">NPI</abbr> получателя, `SM–RP–DA–NPI`.  | int             | 1.1.0.12   |
| SMS_DA_TON                               | Тип <abbr title="Type of Numbering">TON</abbr> получателя, `SM-RP-DA-TON`.               | int             | 1.1.0.12   |
| SMS_PID                                  | Идентификатор протокола, `PID`.                                                          | int             | 1.1.0.14   |
| CAP_IMSI                                 | Номер IMSI в СAP-запросах.                                                               | regex           |            |
| CAP_VLR                                  | Адрес узла VLR в СAP-запросах.                                                           | regex           |            |
| CAP_MSC                                  | Адрес узла MSC в СAP-запросах.                                                           | regex           |            |
| CAP_MSISDN                               | Номер MSISDN в СAP-запросах.                                                             | regex           |            |
| CAP_GSMSCF                               | Адрес gsmSCF.                                                                            | regex           |            |
| CAP_SERVICEKEY                           | Код ServiceKey.                                                                          | int             | 1.0.9.2    |
| CAP_EVENTTYPEBCSM                        | Код EventTypeBCSM.                                                                       | int             | 1.0.9.2    |
| MTP3_OPC                                 | Код Origination Point Code.                                                              | regex           |            |
| MTP3_DPC                                 | Код Destination Point Code.                                                              | regex           |            |
| IP_Src                                   | IP-адрес источника в формате CIDR.                                                       | ip/\[ip\]       | 1.0.12.0   |
| IP_Dst                                   | IP-адрес назначения в формате CIDR.                                                      | ip/\[ip\]       | 1.0.12.0   |
| Src_Port                                 | Порт источника.                                                                          | int             | 1.0.12.0   |
| Dst_Port                                 | Порт назначения.                                                                         | int             | 1.0.12.0   |
| DIAM_UserName                            | Значение Diameter AVP: `User-Name`.                                                      | string          | 1.0.4.0    |
| DIAM_OH                                  | Значение Diameter AVP: `Origin-Host`.                                                    | regex           | 1.0.4.0    |
| DIAM_OR                                  | Значение Diameter AVP: `Origin-Realm`.                                                   | regex           | 1.0.4.0    |
| DIAM_DH                                  | Значение Diameter AVP: `Destination-Host`.                                               | regex           | 1.0.4.0    |
| DIAM_DR                                  | Значение Diameter AVP: `Destination-Realm`.                                              | regex           | 1.0.4.0    |
| DIAM_OH_MCCMNC                           | Значение MCC + MNC для `Origin-Host`.                                                    | string          | 1.0.4.0    |
| DIAM_OR_MCCMNC                           | Значение MCC + MNC для `Origin-Realm`.                                                   | string          | 1.0.4.0    |
| DIAM_DH_MCCMNC                           | Значение MCC + MNC для `Destination-Host`.                                               | string          | 1.0.4.0    |
| DIAM_DR_MCCMNC                           | Значение MCC + MNC для `Destination-Realm`.                                              | string          | 1.0.4.0    |
| DIAM_VPLMNId_MCCMNC                      | Значение MCC + MNC для `VPLMN-Id`.                                                       | string          | 1.0.4.0    |
| DIAM_AID                                 | Значение Diameter AVP: `Application-ID`.                                                 | int             | 1.0.4.0    |
| DIAM_OpCode                              | Значение Diameter AVP: `Command-Code`.                                                   | int             | 1.0.4.0    |
| DIAM_IsRequest                           | Флаг сообщения, являющегося запросом.                                                    | bool            | 2023-08-23 |
| Tag                                      | Тег сообщения.                                                                           | {string,string} | 2023-08-23 |
| [TCAP_Msg_Type](#tcap-message-type-rule) | Тип сообщения TCAP, `Message Type`.                                                      | string          | 2023-08-23 |
| Velocity                                 | Скорость перемещения (км/ч).                                                             | int             | 1.0.9.0    |
| [VC_Status](#vc-status-rule)             | Статус проверки скорости перемещения.                                                    | int             | 1.0.9.0    |
| [LC_Status](#lc-status-rule)             | Статус проверки алгоритма актуальности местоположения.                                   | int             | 1.0.15.3   |
| ATI_ERR_CODE                             | Cтатус запроса MAP-Any-Time-Interrogation.                                               | int             | 1.0.9.0    |
| ATI_VLR                                  | Адрес узла VLR для запроса MAP-Any-Time-Interrogation.                                   | string          | 1.0.9.0    |
| ORIG_REF                                 | Значение TCAP `Origination Reference`.                                                   | string          | 1.0.9.0    |
| DEST_REF                                 | Значение TCAP `Destination Reference`.                                                   | string          | 1.0.9.0    |
| SRI4SM_ERR_CODE                          | Статус запроса MAP-Send-Routing-Info-Short-Message.                                      | int             | 1.0.10.0   |
| SRI4SM_MSC                               | Адрес узла MSC для запроса MAP-Send-Routing-Info-Short-Message.                          | string          | 1.0.10.0   |
| SRI4SM_IMSI                              | IMSI для запроса MAP-Send-Routing-Info-Short-Message.                                    | string          | 1.1.0.13   |
| SRI4SM_SMSC                              | Service center address для запроса MAP-Send-Routing-Info-Short-Message.                  | string          | 1.1.0.13   |
| SRI4SM_MSISDN                            | MSISDN для запроса MAP-Send-Routing-Info-Short-Message.                                  | string          | 1.1.0.13   |
| GTP_IMSI                                 | Номер IMSI в GTP-запросах.                                                               | regex           |            |
| GTP_MSISDN                               | Номер MSISDN в GTP-запросах.                                                             | regex           |            |
| GTP_MEI                                  | Номер IMEI в GTP-запросах.                                                               | regex           |            |
| GTP_APN                                  | Имя точки доступа, <abbr title="Access Point Name">APN</abbr>.                           | string          |            |
| GTP_PLMN                                 | Идентификатор PLMN.                                                                      | string          |            |
| GTP_RAT_Type                             | Код типа <abbr title="Radio Access Technology">RAT</abbr>.                               | int             |            |
| GTP_Interface_Type                       | Код типа интерфейса.                                                                     | int             |            |
| [GTP_Message_Type](#gtp-message-rule)    | Код типа запроса GTP.                                                                    | int             |            |
| GTP_Version                              | Номер версии протокола GTP.                                                              | int             |            |
| TCAP_component_type                      | Флаг наличия вложенности в TCAP.                                                         | bool            | 1.1.0.18   |

Доступные операторы для проверки содержимого данного поля: `==`, `!=`, `<`, `>`, `<=`, `>=`.
Тип сравниваемого значения - int. 
Если хотя бы один из элементов списка не удовлетворяет оператору, то проверка по ключу завершается неуспешно.

#### Типы сообщений TCAP {#tcap-message-type-rule}

include::content/common/appendices/tcap_message_type.adoc[]

#### Код сообщений GTP {#gtp-message-rule}

include::content/common/appendices/gtp_message.adoc[]

#### Статус VelocityCheck, VC_Status {#vc-status-rule}

include::content/common/appendices/vc_status.adoc[]

#### Статус LocationCheck, LC_Status {#lc-status-rule}

include::content/common/appendices/lc_status.adoc[]

#### Результат обработки правилами {#rule-result-rule}

include::content/common/appendices/rule_result.adoc[]