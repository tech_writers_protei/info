---
title: "Rule WEB"
description: "Описание параметров использования Rule в WEB"
weight: 30
type: docs
draft: true
---

## Описание параметров SS7 Rule

### Key GT Rule

| Field CORE   | Type CORE | Version CORE | Field WEB    | Version WEB | Description                              | Threshold | Help   | 
|--------------|-----------|--------------|--------------|-------------|------------------------------------------|-----------|--------|
| GT_A_Address | regex     |              | GT_A_Address | -           | Адрес SCCP GT (Global Title) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список сетевых зон          | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |                                              |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                           | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|---------------------------------------|-----------|--------|
| GT_A_NP    | int       |              | GT_A_NP   | -           | Номер NP (Numbering Plan) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=) |            | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                                         |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix                          |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                         |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                         |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                         |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                         |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                              | Threshold | Help   | 
|------------|-----------|--------------|------------|-------------|------------------------------------------|-----------|--------|
| GT_A_SSN   | int       |              | GT_A_SSN   | -           | Номер SSN (Subsystem Number) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                             | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|-----------------------------------------|-----------|--------|
| GT_A_TT    | int       |              | GT_A_TT   | -           | Номер TT (Translation Type) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB   | Version WEB | Description                                         | Threshold | Help   | 
|------------|-----------|--------------|-------------|-------------|-----------------------------------------------------|-----------|--------|
| GT_A_NAI   | int       |              | GT_A_NAI    | -           | Номер NAI (Nature of Address Indicator) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                            | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------------------|-----------|--------|
| GT_A_ES    | int       |              | GT_A_ES   | -           | Номер ES (Encoding Scheme) отправителя | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE   | Type CORE | Version CORE | Field WEB    | Version WEB | Description                              | Threshold | Help   | 
|--------------|-----------|--------------|--------------|-------------|------------------------------------------|-----------|--------|
| GT_B_Address | regex     |              | GT_B_Address | -           | Адрес SCCP GT (Global Title) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список сетевых зон          | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |                                              |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                           | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|---------------------------------------|-----------|--------|
| GT_B_NP    | int       |              | GT_B_NP   | -           | Номер NP (Numbering Plan) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                    | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-----------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=)   |            | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix                            |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                              | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|------------------------------------------|-----------|--------|
| GT_B_SSN   | int       |              | GT_B_SSN  | -           | Номер SSN (Subsystem Number) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                             | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|-----------------------------------------|-----------|--------|
| GT_B_TT    | int       |              | GT_B_TT   | -           | Номер TT (Translation Type) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                                         | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|-----------------------------------------------------|-----------|--------|
| GT_B_NAI   | int       |              | GT_B_NAI  | -           | Номер NAI (Nature of Address Indicator) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                            | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------------------|-----------|--------|
| GT_B_ES    | int       |              | GT_B_ES   | -           | Номер ES (Encoding Scheme) получателя  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp                         | Regexp     | 
| String               |     |     |     |     |     |     |                 |                 |                      |                                |            |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |            | 

</details>

-----

### Key MAP Rule

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------|-----------|--------|
| MAP_IMSI   | regex     |              | MAP_IMSI  | -           | Номер IMSI в MAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                 | Threshold | Help   | 
|------------|-----------|--------------|------------|-------------|-----------------------------|-----------|--------|
| MAP_MSISDN | regex     |              | MAP_MSISDN | -           | Номер MSISDN в MAP-запросах | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                     | Threshold | Help   | 
|------------|-----------|--------------|------------|-------------|---------------------------------|-----------|--------|
| MAP_SMSC   | regex     |              | MAP_SMSC   | -           | Адрес узла SMSC в MAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| MAP_MSC    | regex     |              | MAP_MSC   | -           | Адрес узла MSC в MAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| MAP_MLC    | string    | 1.1.0.27     | MAP_MLC   | -           | Адрес узла MLC в MAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| MAP_VLR    | regex     |              | MAP_VLR   | -           | Адрес узла VLR в MAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| MAP_SGSN   | regex     |              | MAP_SGSN  | -           | Адрес узла SGSN в MAP-запросах | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| MAP_AC     | string    |              | MAP_AC    | -           | Имя контекста приложения, ACN  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE    | Type CORE   | Version CORE | Field WEB     | Version WEB | Description                  | Threshold | Help | 
|---------------|-------------|--------------|---------------|-------------|------------------------------|-----------|------|
| MAP_REQ_PARAM | array{int}* |              | MAP_REQ_PARAM | -           | Параметры Request Parameters | +         | -    |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE   | Type CORE | Version CORE | Field WEB     | Version WEB | Description                   | Threshold | Help   | 
|--------------|-----------|--------------|---------------|-------------|-------------------------------|-----------|--------|
| MAP_OCSI_SCF | regex     |              | MAP_OCSI_SCF  | -           | Адрес gsmSCF с профилем O-CSI | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description     | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|-----------------|-----------|--------|
| MAP_HLR    | regex     |              | MAP_HLR   | -           | Адрес узла HLR  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

### Key SS7 Rule

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------|-----------|--------|
| SMS_OA     | string    | 1.1.0.12     | SMS_OA    | -           | Адрес отправителя SM-RP-OA | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                   |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | TemplSelector                  | TemplSelector                |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                              | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений  |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                              | 

</details>

-----

| Field CORE   | Type CORE | Version CORE | Field WEB  | Version WEB | Description                       | Threshold | Help   | 
|--------------|-----------|--------------|------------|-------------|-----------------------------------|-----------|--------|
| SMS_OA_TON   | int       | 1.1.0.12     | SMS_OA_TON | -           | Тип TON отправителя, SM-RP-OA-TON | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                    | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-----------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=)   |            |
| String               |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                           |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                              | Threshold | Help | 
|------------|-----------|--------------|------------|-------------|------------------------------------------|-----------|------|
| SMS_OA_NPI | int       | 1.1.0.12     | SMS_OA_NPI | -           | Индикатор NPI отправителя, SM–RP–OA–NPI  | +         | -    |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                    | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-----------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=)   |            |
| String               |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                           |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                               | Threshold | Help  | 
|------------|-----------|--------------|-----------|-------------|-------------------------------------------|-----------|-------|
| SMS_DA     | string    | 1.1.0.12     | SMS_DA    | -           | Адрес получателя SMS-сообщения, SM-RP-DA  | +         | -     |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                   |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|------------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | TemplSelector                  | TemplSelector                |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                              | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений  |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                              | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                              | 

</details>
-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                      | Threshold | Help | 
|------------|-----------|--------------|------------|-------------|----------------------------------|-----------|------|
| SMS_DA_TON | int       | 1.1.0.12     | SMS_DA_TON | -           | Тип TON получателя, SM-RP-DA-TON | +         | -    |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                    | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-----------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=)   |            |
| String               |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                           |            |

</details>

-----

| Field CORE  | Type CORE | Version CORE | Field WEB    | Version WEB | Description                             | Threshold | Help    | 
|-------------|-----------|--------------|--------------|-------------|-----------------------------------------|-----------|---------|
| SMS_DA_NPI  | int       | 1.1.0.12     | SMS_DA_NPI   | -           | Индикатор NPI получателя, SM–RP–DA–NPI  | +         | -       |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                                    | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-----------------------------------------------------------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | MultiSelect (~/ !~), Regexp (== / != / > / < / >= / <=)   |            |
| String               |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                                           |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                                           |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                                           |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                   | Threshold | Help    | 
|------------|-----------|--------------|-----------|-------------|-------------------------------|-----------|---------|
| SMS_PID    | int       | 1.1.0.14     | SMS_PID   | -           | Идентификатор протокола, PID  | +         | -       |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format       | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------|------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |              |            |
| String               |     |     |     |     | X   | X   |                 |                 |                      | MultiSelect  |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |              |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |              |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |              |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |              |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |              |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |              |            |

</details>

-----

### Key CAP Rule

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------|-----------|--------|
| CAP_IMSI   | regex     |              | CAP_IMSI  | -           | Номер IMSI в СAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список сетевых зон          | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |                                              |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| CAP_VLR    | regex     |              | CAP_VLR   | -           | Адрес узла VLR в СAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                    | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|--------------------------------|-----------|--------|
| CAP_MSC    | regex     |              | CAP_MSC   | -           | Адрес узла MSC в СAP-запросах  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

-----

| Field CORE  | Type CORE | Version CORE | Field WEB   | Version WEB | Description                 | Threshold | Help   | 
|-------------|-----------|--------------|-------------|-------------|-----------------------------|-----------|--------|
| CAP_MSISDN  | regex     |              | CAP_MSISDN  | -           | Номер MSISDN в СAP-запросах | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select                                       | Список сетевых зон          | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |                                              |                             | 

</details>

-----

| Field CORE  | Type CORE | Version CORE | Field WEB   | Version WEB | Description   | Threshold | Help   | 
|-------------|-----------|--------------|-------------|-------------|---------------|-----------|--------|
| CAP_GSMSCF  | regex     |              | CAP_GSMSCF  | -           | Адрес gsmSCF  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>


-----

| Field CORE      | Type CORE | Version CORE | Field WEB       | Version WEB | Description     | Threshold | Help   | 
|-----------------|-----------|--------------|-----------------|-------------|-----------------|-----------|--------|
| CAP_SERVICEKEY  | int       | 1.0.9.2      | CAP_SERVICEKEY  | -           | Код ServiceKey  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp  |            | 
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            | 

</details>

-----

| Field CORE         | Type CORE | Version CORE | Field WEB          | Version WEB | Description        | Threshold | Help   | 
|--------------------|-----------|--------------|--------------------|-------------|--------------------|-----------|--------|
| CAP_EVENTTYPEBCSM  | int       | 1.0.9.2      | CAP_EVENTTYPEBCSM  | -           | Код EventTypeBCSM  | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp  |            | 
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            | 
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            | 

</details>

-----

### Key SRI4SM Rule

| Field CORE  | Type CORE | Version CORE | Field WEB     | Version WEB | Description                                           | Threshold | Help (in field) | 
|-------------|-----------|--------------|---------------|-------------|-------------------------------------------------------|-----------|-----------------|
| SRI4SM_IMSI | string    | 1.1.0.13     | SRI4SM_IMSI   | -           | IMSI для запроса MAP-Send-Routing-Info-Short-Message  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                           |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE  | Type CORE | Version CORE | Field WEB   | Version WEB | Description                                                            | Threshold | Help (in field) | 
|-------------|-----------|--------------|-------------|-------------|------------------------------------------------------------------------|-----------|-----------------|
| SRI4SM_SMSC | string    | 1.1.0.13     | SRI4SM_SMSC | -           | Service center address для запроса MAP-Send-Routing-Info-Short-Message | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                           |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB    | Version WEB | Description                                                     | Threshold | Help (in field) | 
|------------|-----------|--------------|--------------|-------------|-----------------------------------------------------------------|-----------|-----------------|
| SRI4SM_MSC | string    | 1.0.10.0     | SRI4SM_MSC   | -           | Адрес узла MSC для запроса MAP-Send-Routing-Info-Short-Message  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                           |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE     | Type CORE | Version CORE | Field WEB     | Version WEB | Description                                             | Threshold | Help (in field) | 
|----------------|-----------|--------------|---------------|-------------|---------------------------------------------------------|-----------|-----------------|
| SRI4SM_MSISDN  | string    | 1.1.0.13     | SRI4SM_MSISDN | -           | MSISDN для запроса MAP-Send-Routing-Info-Short-Message  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                           |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE       | Type CORE | Version CORE | Field WEB | Version WEB | Description                                         | Threshold | Help (in field) | 
|------------------|-----------|--------------|-----------|-------------|-----------------------------------------------------|-----------|-----------------|
| SRI4SM_ERR_CODE  | int       | 1.0.10.0     |           | -           | Статус запроса MAP-Send-Routing-Info-Short-Message  | -         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |        |            |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |

</details>

-----

### Key Other Rule

| Field CORE           | Type CORE | Version CORE | Field WEB            | Version WEB | Description                      | Threshold | Help (in field) | 
|----------------------|-----------|--------------|----------------------|-------------|----------------------------------|-----------|-----------------|
| TCAP_component_type  | bool      | 1.1.0.18     | TCAP_component_type  | -           | Флаг наличия вложенности в TCAP  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Select  |            |
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            |

</details>

-----

| Field CORE     | Type CORE | Version CORE | Field WEB | Version WEB | Description                       | Threshold | Help (in field) | 
|----------------|-----------|--------------|-----------|-------------|-----------------------------------|-----------|-----------------|
| TCAP_Msg_Type  | string    | 2023-08-23   |           | -           | Тип сообщения TCAP, Message Type  | -         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |        |            |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |

</details>

-----

| Field CORE | Type CORE       | Version CORE | Field WEB | Version WEB | Description    | Threshold | Help (in field) | 
|------------|-----------------|--------------|-----------|-------------|----------------|-----------|-----------------|
| Tag        | {string,string} | 2023-08-23   | Tag       | -           | Тег сообщения  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                    | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|-------------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                     |     |     |     |     |     |     |                 |                 |                      |         |            |
| String                  |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                   |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                    |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList                |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea             |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area    |     |     |     |     |     |     |                 |                 |                      |         |            |
| Tag from action Set_Tag | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp  | Regexp     |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------|-----------|--------|
| MTP3_OPC   | regex     |              | MTP3_OPC  | -           | Код Origination Point Code | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                | Threshold | Help   | 
|------------|-----------|--------------|-----------|-------------|----------------------------|-----------|--------|
| MTP3_DPC   | regex     |              | MTP3_DPC  | -           | Код Destination Point Code | +         | -      |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                       | Threshold | Help | 
|------------|-----------|--------------|-----------|-------------|-----------------------------------|-----------|------|
| IP_Src     | ip/[ip]   | 1.0.12.0     | IP_Src    | -           | IP-адрес источника в формате CIDR | +         | -    |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation           |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|----------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                      | 
| String               |     |     |     |     |     |     |                 |                 |                      |         |                      |
| IP                   | X   | X   |     |     |     |     |                 |                 |                      | Regexp  | Regexp               |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                      | 
| List                 |     |     |     |     |     |     |                 |                 |                      | Select  |                      | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      | Select  |                      |
| IP list              | X   | X   |     |     |     |     |                 |                 |                      | Select  | Список IP адресов    | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select  | Список сетевых зон   | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |         |                      | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                         | Threshold | Help | 
|------------|-----------|--------------|-----------|-------------|-------------------------------------|-----------|------|
| IP_Dst     | ip/[ip]   | 1.0.12.0     | IP_Dst    | -           | IP-адрес назначения в формате CIDR  | +         | -    |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation           |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|----------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                      | 
| String               |     |     |     |     |     |     |                 |                 |                      |         |                      |
| IP                   | X   | X   |     |     |     |     |                 |                 |                      | Regexp  | Regexp               |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                      | 
| List                 |     |     |     |     |     |     |                 |                 |                      | Select  |                      | 
| MaskList             |     |     |     |     |     |     |                 |                 |                      | Select  |                      |
| IP list              | X   | X   |     |     |     |     |                 |                 |                      | Select  | Список IP адресов    | 
| NetworkArea          |     |     |     |     | X   | X   |                 |                 |                      | Select  | Список сетевых зон   | 
| Current network area |     |     |     |     | X   | X   |                 |                 |                      |         |                      | 

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description     | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|-----------------|-----------|-----------------|
| SrcPort    | int       | 1.0.12.0     | SrcPort   |             | Порт источника  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   |     |     |     |     | X               |                 |                      | Regexp  | 0 - 65535  |
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB   | Version WEB | Description     | Threshold | Help (in field) | 
|------------|-----------|--------------|-------------|-------------|-----------------|-----------|-----------------|
| DstPort    | int       | 1.0.12.0     | DstPort     |             | Порт назначения | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   |     |     |     |     | X               |                 |                      | Regexp  | 0 - 65535  |
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB    | Version WEB | Description           | Threshold | Help (in field) | 
|------------|-----------|--------------|--------------|-------------|-----------------------|-----------|-----------------|
| Velocity   | int       | 1.0.9.0      | Velocity     |             | Скорость перемещения  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp  | Regexp     |
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB   | Version WEB | Description                          | Threshold | Help (in field) | 
|------------|-----------|--------------|-------------|-------------|--------------------------------------|-----------|-----------------|
| VC_Status  | int       | 1.0.9.0      | VC_Status   |             | Статус проверки скорости перемещения | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format       | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------|------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp       | Regexp     |
| String               |     |     | X   | X   |     |     |                 |                 |                      | MultiSelect  |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |              |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |              |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |              |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |              |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |              |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |              |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                                            | Threshold | Help (in field) | 
|------------|-----------|--------------|------------|-------------|--------------------------------------------------------|-----------|-----------------|
| LC_Status  | int       | 1.0.15.3     | LC_Status  |             | Статус проверки алгоритма актуальности местоположения  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format      | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|-------------|------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |             |            |
| String               |     |     |     |     | X   | X   |                 |                 |                      | MultiSelect |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |             |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |             |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |             |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |             |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |             |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |             |            |

</details>

-----

| Field CORE   | Type CORE | Version CORE | Field WEB    | Version WEB | Description                               | Threshold | Help (in field) | 
|--------------|-----------|--------------|--------------|-------------|-------------------------------------------|-----------|-----------------|
| ATI_ERR_CODE | int       | 1.0.9.0      | ATI_ERR_CODE |             | Cтатус запроса MAP-Any-Time-Interrogation | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------|
| Int                  | X   | X   | X   | X   |     |     | X               |                 |                      | Regexp  | Regexp     |
| String               |     |     |     |     |     |     |                 |                 |                      |         |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |            |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                                            | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|--------------------------------------------------------|-----------|-----------------|
| ATI_VLR    | string    | 1.0.9.0      | ATI_VLR   |             | Адрес узла VLR для запроса MAP-Any-Time-Interrogation  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                          | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|--------------------------------------|-----------|-----------------|
| ORIG_REF   | string    | 1.0.9.0      | ORIG_REF  |             | Значение TCAP Origination Reference  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                         | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|-------------------------------------|-----------|-----------------|
| DEST_REF   | string    | 1.0.9.0      | DEST_REF  |             | Значение TCAP Destination Reference | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| String               | Х   | Х   | X   | X   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | X   | X   | X   | X   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             | 
| List                 |     |     |     |     | X   | X   |                 |                 |                      | Select                         | Список префиксов            | 
| MaskList             |     |     | Х   | Х   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             | 
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             | 

</details>

### Action SS7 Rule

| Field CORE | Version CORE | Field WEB | Version WEB | Description                                        | Help  | 
|------------|--------------|-----------|-------------|----------------------------------------------------|-------|
| Reject     | -            | REJECT    | -           | Прекратить обработку сообщения и ответить отбоем   | -     |

<details><summary>Example</summary>

```
{
  "Name": "Reject"
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB  | Version WEB | Description                                   | Help    | 
|------------|--------------|------------|-------------|-----------------------------------------------|---------|
| SendATI    | -            | SEND_ATI   | -           | Отправить запрос MAP-Any-Time-Interrogation   | -       |

<details><summary>Action parameters</summary>

| Parameters     | Type | Default value | Description                                                                    | 
|----------------|------|---------------|--------------------------------------------------------------------------------|
| EnablePaging   | bool | 0             | Включение или отключение paging                                                |
| ValidityPeriod | int  | 0             | Время, в течении которого значение AgeOfLocation является допустимым (секунды) |

</details>

<details><summary>Example</summary>

```
{
  "Name": "SendATI",
  "Data": {
    "EnablePaging": 1,
    "ValidityPeriod": 10000
  }
}
```

</details>

-----

| Field CORE      | Version CORE | Field WEB        | Version WEB | Description                    | Help         | 
|-----------------|--------------|------------------|-------------|--------------------------------|--------------|
| VelocityCheck   | -            | VELOCITY_CHECK   | -           | Проверить скорость перемещения | -            |

<details><summary>Action parameters</summary>

| Parameters     | Type | Default value | Description                                                                    |
|----------------|------|---------------|--------------------------------------------------------------------------------|
| EnablePaging   | bool | 0             | Флаг включение или отключение paging                                           |
| ValidityPeriod | int  | 0             | Время, в течении которого значение AgeOfLocation является допустимым (секунды) |
| AtiAllowed     | bool | 0             | Флаг разрешения запроса MAP-Any-Time-Interrogation                             |
| PLMNSearch     | bool | 0             | Флаг, указывающий на поле для поиска сетевой зоны                              |

</details>

<details><summary>Example</summary>

```
{
  "Name": "SendATI",
  "Data": {
    "AtiAllowed": 1,
    "EnablePaging": 1,
    "ValidityPeriod": 10000,
    "PLMNSearch": 1
  }
}
```

</details>

## Описание параметров DIAMETER Rule

### Key DIAMETER Rule

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                        | Threshold | Help      | 
|------------|-----------|--------------|------------|-------------|------------------------------------|-----------|-----------|
| DIAM_OH    | regex     | 1.0.4.0      | DIAM_OH    | -           | Значение Diameter AVP: Origin-Host | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |        |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |                             |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |                             |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |        |                             |

</details>

------

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                         | Threshold | Help      | 
|------------|-----------|--------------|------------|-------------|-------------------------------------|-----------|-----------|
| DIAM_OR    | regex     | 1.0.4.0      | DIAM_OR    | -           | Значение Diameter AVP: Origin-Realm | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |                             |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                               | Threshold | Help      | 
|------------|-----------|--------------|------------|-------------|-------------------------------------------|-----------|-----------|
| DIAM_DH    | regex     | 1.0.4.0      | DIAM_DH    | -           | Значение Diameter AVP: Destination - Host | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |                             |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                                 | Threshold | Help      | 
|------------|-----------|--------------|------------|-------------|---------------------------------------------|-----------|-----------|
| DIAM_DR    | regex     | 1.0.4.0      | DIAM_DR    | -           | Значение Diameter AVP: Destination - Realm  | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     |     |     |                 |                 |                      |         |                             |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE      | Type CORE | Version CORE | Field WEB       | Version WEB | Description                          | Threshold | Help      | 
|-----------------|-----------|--------------|-----------------|-------------|--------------------------------------|-----------|-----------|
| DIAM_OH_MCCMNC  | string    | 1.0.4.0      | DIAM_OH_MCCMNC  | -           | Значение MCC + MNC для Origin - Host | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select  | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE      | Type CORE | Version CORE | Field WEB       | Version WEB | Description                           | Threshold | Help      | 
|-----------------|-----------|--------------|-----------------|-------------|---------------------------------------|-----------|-----------|
| DIAM_OR_MCCMNC  | string    | 1.0.4.0      | DIAM_OR_MCCMNC  | -           | Значение MCC + MNC для Origin - Realm | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select  | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE      | Type CORE | Version CORE | Field WEB       | Version WEB | Description                               | Threshold | Help      | 
|-----------------|-----------|--------------|-----------------|-------------|-------------------------------------------|-----------|-----------|
| DIAM_DH_MCCMNC  | string    | 1.0.4.0      | DIAM_DH_MCCMNC  | -           | Значение MCC + MNC для Destination - Host | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select  | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE      | Type CORE | Version CORE | Field WEB       | Version WEB | Description                                | Threshold | Help      | 
|-----------------|-----------|--------------|-----------------|-------------|--------------------------------------------|-----------|-----------|
| DIAM_DR_MCCMNC  | string    | 1.0.4.0      | DIAM_DR_MCCMNC  | -           | Значение MCC + MNC для Destination - Realm | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select  | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE            | Type CORE | Version CORE | Field WEB            | Version WEB | Description                      | Threshold | Help      | 
|-----------------------|-----------|--------------|----------------------|-------------|----------------------------------|-----------|-----------|
| DIAM_VPLMNId_MCCMNC   | string    | 1.0.4.0      | DIAM_VPLMNId_MCCMNC  | -           | Значение MCC + MNC для VPLMN-Id  | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |         |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp  | Regexp                      |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select  | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select  | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |         |                             |

</details>

------

| Field CORE       | Type CORE | Version CORE | Field WEB        | Version WEB | Description                       | Threshold | Help      | 
|------------------|-----------|--------------|------------------|-------------|-----------------------------------|-----------|-----------|
| DIAM_UserName    | string    | 1.0.4.0      | DIAM_UserName    | -           | Значение Diameter AVP: User-Name  | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | x   | x   | x   | x   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | x   | x   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     | x   | x   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

------

| Field CORE   | Type CORE | Version CORE | Field WEB     | Version WEB | Description                             | Threshold | Help      | 
|--------------|-----------|--------------|---------------|-------------|-----------------------------------------|-----------|-----------|
| DIAM_AID     | int       | 1.0.4.0      | DIAM_AID      | -           | Значение Diameter AVP: Application - ID | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  | x   | x   | x   | x   |     |     | x               |                 |                      | Regexp | Regexp     |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |

</details>

------

| Field CORE     | Type CORE | Version CORE | Field WEB       | Version WEB | Description                           | Threshold | Help      | 
|----------------|-----------|--------------|-----------------|-------------|---------------------------------------|-----------|-----------|
| DIAM_OpCode    | int       | 1.0.4.0      | DIAM_OpCode     | -           | Значение Diameter AVP: Command - Code | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  | x   | x   | x   | x   |     |     | x               |                 |                      | Regexp | Regexp     |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |

</details>

------

| Field CORE      | Type CORE | Version CORE  | Field WEB       | Version WEB | Description                          | Threshold | Help      | 
|-----------------|-----------|---------------|-----------------|-------------|--------------------------------------|-----------|-----------|
| DIAM_IsRequest  | bool      | 2023-08-23    | DIAM_IsRequest  | -           | Флаг сообщения, являющегося запросом | +         | -         |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |        |            |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |
| Boolean              | x   |     |     |     |     |     |                 |                 |                      | Select | true/false |

</details>

------

### Action DIAMETER Rule

## Описание параметров GTP Rule

### Key GTP Rule

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description                 | Threshold | Help (in field) | 
|------------|-----------|--------------|------------|-------------|-----------------------------|-----------|-----------------|
| GTP_IMSI   | regex     |              | GTP_IMSI   | -           | Номер IMSI в GTP-запросах   | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE  | Type CORE | Version CORE | Field WEB  | Version WEB | Description                 | Threshold | Help (in field) | 
|-------------|-----------|--------------|------------|-------------|-----------------------------|-----------|-----------------|
| GTP_MSISDN  | regex     |              | GTP_MSISDN | -           | Номер MSISDN в GTP-запросах | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                           |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                                       | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                             |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон          |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description               | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|---------------------------|-----------|-----------------|
| GTP_IMEI   | regex     |              | GTP_IMEI  | -           | Номер IMEI в GTP-запросах | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                         | Validation                  |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------------------------------|-----------------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                         | Regexp                      |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix |                             |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                         | Список префиксов            |
| MaskList             |     |     | X   | X   |     |     |                 |                 |                      | Select                         | Список регулярных выражений |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |                                |                             |
| Current network area |     |     |     |     |     |     |                 |                 |                      |                                |                             |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB | Version WEB | Description                                | Threshold | Help (in field) | 
|------------|-----------|--------------|-----------|-------------|--------------------------------------------|-----------|-----------------|
| GTP_APN    | string    |              | GTP_APN   | -           | Имя точки доступа, APN (Access Point Name) | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation           |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|----------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              | -                    |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | Regexp               |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                      |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов     |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                              |                      |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                      |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон   |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                      |

</details>

-----

| Field CORE   | Type CORE | Version CORE | Field WEB     | Version WEB | Description                             | Threshold | Help (in field) | 
|--------------|-----------|--------------|---------------|-------------|-----------------------------------------|-----------|-----------------|
| GTP_RAT_Type | int       |              | GTP_RAT_Type  | -           | Код типа RAT (Radio Access Technology)  | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation       |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp  | 0 - 255          |
| String               |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                  |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список префиксов |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |                  |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                  |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |                  |

</details>

-----

| Field CORE | Type CORE | Version CORE | Field WEB  | Version WEB | Description           | Threshold | Help (in field) | 
|------------|-----------|--------------|------------|-------------|-----------------------|-----------|-----------------|
| GTP_PLMN   | string    |              | GTP_PLMN   | -           | Идентификатор PLMN    | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format                                       | Validation           |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|----------------------------------------------|----------------------|
| Int                  |     |     |     |     |     |     |                 |                 |                      |                                              |                      |
| String               | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Regexp                                       | 00000 - 999999       |
| Field                | Х   | Х   | Х   | Х   |     |     |                 |                 |                      | Value, Length, Prefix, Postfix, Network area |                      |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список префиксов     |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |                                              |                      |
| IP list              |     |     |     |     |     |     |                 |                 |                      |                                              |                      |
| NetworkArea          |     |     |     |     | Х   | Х   |                 |                 |                      | Select                                       | Список сетевых зон   |
| Current network area |     |     |     |     | Х   | Х   |                 |                 |                      |                                              |                      |

</details>

-----

| Field CORE         | Type CORE  | Version CORE | Field WEB          | Version WEB | Description           | Threshold | Help (in field) | 
|--------------------|------------|--------------|--------------------|-------------|-----------------------|-----------|-----------------|
| GTP_Interface_Type | int        |              | GTP_Interface_Type | -           | Код типа интерфейса   | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation       |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp  | 0 - 63           |
| String               |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                  |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список префиксов |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |                  |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                  |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |                  |

</details>

-----

| Field CORE        | Type CORE  | Version CORE | Field WEB          | Version WEB | Description          | Threshold | Help (in field) | 
|-------------------|------------|--------------|--------------------|-------------|----------------------|-----------|-----------------|
| GTP_Message_Type  | int        |              | GTP_Message_Type   | -           | Код типа запроса GTP | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format  | Validation       |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|---------|------------------|
| Int                  | X   | X   |     |     |     |     |                 |                 |                      | Regexp  | 0 - 255          |
| String               |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Field                |     |     |     |     |     |     |                 |                 |                      |         |                  |
| List                 |     |     |     |     | Х   | Х   |                 |                 |                      | Select  | Список префиксов |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |         |                  |
| IP list              |     |     |     |     |     |     |                 |                 |                      |         |                  |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |         |                  |
| Current network area |     |     |     |     |     |     |                 |                 |                      |         |                  |

</details>

-----

| Field CORE    | Type CORE  | Version CORE | Field WEB       | Version WEB | Description                  | Threshold | Help (in field) | 
|---------------|------------|--------------|-----------------|-------------|------------------------------|-----------|-----------------|
| GTP_Version   | int        |              | GTP_Version     | -           | Номер версии протокола GTP   | +         | -               |

<details><summary>Extended description</summary>

| TYPE                 | ==  | !=  | ~   | !~  | in  | !in | > / < / >= / <= | Help (in value) | Help (hint in value) | Format | Validation |
|----------------------|-----|-----|-----|-----|-----|-----|-----------------|-----------------|----------------------|--------|------------|
| Int                  | X   |     |     |     |     |     |                 |                 |                      | Select |            |
| String               |     |     |     |     |     |     |                 |                 |                      |        |            |
| Field                |     |     |     |     |     |     |                 |                 |                      |        |            |
| List                 |     |     |     |     |     |     |                 |                 |                      |        |            |
| MaskList             |     |     |     |     |     |     |                 |                 |                      |        |            |
| IP list              |     |     |     |     |     |     |                 |                 |                      |        |            |
| NetworkArea          |     |     |     |     |     |     |                 |                 |                      |        |            |
| Current network area |     |     |     |     |     |     |                 |                 |                      |        |            |

</details>

-----

### Action Other Rule

| Field CORE | Version CORE | Field WEB | Version WEB | Description                              | Help  | 
|------------|--------------|-----------|-------------|------------------------------------------|-------|
| Transmit   | -            | TRANSMIT  | -           | Передать сообщение в сеть без обработки  | -     |

<details><summary>Example</summary>

```
{
  "Name": "Transmit"
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB   | Version WEB | Description                                                      | Help  | 
|------------|--------------|-------------|-------------|------------------------------------------------------------------|-------|
| SendAlarm  | -            | SEND_ALARM  | -           | Отправить оповещение с аварией и продолжить обработку сообщения  | -     |

<details><summary>Example</summary>

```
{
  "Name": "SendAlarm"
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB   | Version WEB | Description                                                      | Help  | 
|------------|--------------|-------------|-------------|------------------------------------------------------------------|-------|
| SendAlarm  | -            | SEND_ALARM  | -           | Отправить оповещение с аварией и продолжить обработку сообщения  | -     |

<details><summary>Example</summary>

```
{
  "Name": "SendAlarm"
}
```

</details>

-----

| Field CORE    | Version CORE | Field WEB      | Version WEB | Description                                                              | Help  | 
|---------------|--------------|----------------|-------------|--------------------------------------------------------------------------|-------|
| ReturnResult  | -            | RETURN_RESULT  | -           | Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_RESULT  | -     |

<details><summary>Example</summary>

```
{
  "Name": "ReturnResult"
}
```

</details>

-----

| Field CORE  | Version CORE | Field WEB    | Version WEB | Description                               | Help  | 
|-------------|--------------|--------------|-------------|-------------------------------------------|-------|
| SilentDrop  | -            | SILENT_DROP  | -           | Прекратить обработку без отправки ответа  | -     |

<details><summary>Example</summary>

```
{
  "Name": "SilentDrop"
}
```

</details>

-----

| Field CORE  | Version CORE | Field WEB    | Version WEB | Description              | Help    | 
|-------------|--------------|--------------|-------------|--------------------------|---------|
| SendSRI4SM  | -            | SEND_SRI4SM  | -           | Отправить запрос SRI4SM  | -       |

<details><summary>Action parameters</summary>

| Parameters                  | Type | Default value | Description                                                                                                                                              | 
|-----------------------------|------|---------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| EnablePaging                | bool | 0             | Включение или отключение paging                                                                                                                          |
| ValidityPeriod              | int  | 3600          | Период годности сообщения, в секундах                                                                                                                    |
| SendAtiIfSri4smNotPossible  | bool | 0             | Флаг отправки запроса MAP-Any-Time-Interrogation при невозможности отправки MAP-Send-Routing-Information-For-SM ввиду отсутствия номера MSISDN абонента  |

</details>

<details><summary>Example</summary>

```
{
 "Name": "SendSRI4SM",
  "Data": {
    "EnablePaging": 0,
    "ValidityPeriod": 3600,
    "SendAtiIfSri4smNotPossible": 0
  }
}
```

</details>

-----

| Field CORE     | Version CORE | Field WEB        | Version WEB | Description                                                                         | Help         | 
|----------------|--------------|------------------|-------------|-------------------------------------------------------------------------------------|--------------|
| LocationCheck  | -            | LOCATION_CHECK   | -           | Остановить обработку сообщения, проверить актуальность информации о местоположении  | -            |

<details><summary>Action parameters</summary>

| Parameters     | Type | Default value | Description                                                                    |
|----------------|------|---------------|--------------------------------------------------------------------------------|
| AtiAllowed     | bool | 0             | Флаг разрешения запроса MAP-Any-Time-Interrogation                             |
| EnablePaging   | bool | 0             | Флаг включение или отключение paging                                           |
| ValidityPeriod | int  | 0             | Время, в течении которого значение AgeOfLocation является допустимым (секунды) |
| PLMNSearch     | bool | 0             | Флаг, указывающий на поле для поиска сетевой зоны                              |

</details>

<details><summary>Example</summary>

```
{
 "Name": "LocationCheck",
  "Data": {
    "AtiAllowed": 1,
    "EnablePaging": 1,
    "ValidityPeriod": 10000,
    "PLMNSearch": 1
  }
}
```

</details>

-----

| Field CORE   | Version CORE | Field WEB     | Version WEB | Description                                                                                      | Help    | 
|--------------|--------------|---------------|-------------|--------------------------------------------------------------------------------------------------|---------|
| ReturnError  | -            | RETURN_ERROR  | -           | Прекратить обработку сообщения и ответить сообщением TCAP_RETURN_ERROR с кодом ошибки ErrorCode  | -       |

<details><summary>Action parameters</summary>

| Parameters | Type | Default value | Description             | 
|------------|------|---------------|-------------------------|
| ErrorCode  | int  | 34            | Код возвращаемой ошибки |

</details>

<details><summary>Example</summary>

```
{
 "Name": "ReturnError",
  "Data": {
    "ErrorCode": 0
  }
}
```

</details>

-----

| Field CORE  | Version CORE | Field WEB    | Version WEB | Description                                                                       | Help    | 
|-------------|--------------|--------------|-------------|-----------------------------------------------------------------------------------|---------|
| ReturnUDTS  | -            | RETURN_UDTS  | -           | Прекратить обработку сообщения и ответить сообщением UDTS c причиной ReturnCause  | -       |

<details><summary>Action parameters</summary>

| Parameters  | Type | Default value | Description             | 
|-------------|------|---------------|-------------------------|
| ReturnCause | int  | 0             | Причина отбоя сообщения |

</details>	

<details><summary>Example</summary>

```
{
  "Name": "ReturnUDTS",
  "Data": {
    "ReturnCause": 0
  }
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB | Version WEB | Description                      | Help    | 
|------------|--------------|-----------|-------------|----------------------------------|---------|
| GOTO       | -            | GOTO      | -           | Перейти к определенному правилу  | -       |

<details><summary>Action parameters</summary>

| Parameters | Type   | Default value | Description      | 
|------------|--------|---------------|------------------|
| rule_name  | string | OUT           | Точка назначения |

</details>	

<details><summary>Example</summary>

```
{
  "Name": "GOTO",
  "Data": "<rule_name>"
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB  | Version WEB | Description                      | Help    | 
|------------|--------------|------------|-------------|----------------------------------|---------|
| SetParam   | -            | SET_PARAM  | -           | Перейти к определенному правилу  | -       |

<details><summary>Action parameters</summary>

| Parameters          | Type    | Default value | Description                                                                                            | 
|---------------------|---------|---------------|--------------------------------------------------------------------------------------------------------|
| paramName           | object  |               | Имя параметра. Формат: "paramName": { "variable": "<variable>" }                                       |
| paramValueField     | object  |               | Имя параметра, значение которого используется. Формат: "paramValueField": { "variable": "<variable>" } |
| variable            | string  |               | Имя переменной                                                                                         |
| valueType           | string  |               | Тип значения. String / Int / Field                                                                     |
| paramValueString    | string  |               | Задаваемое значение параметра                                                                          |
| operator            | string  |               | Действие со значением paramValueField. DelPrefix / AddPrefix                                           |
| operatorValueString | object  |               | Дополнительные параметры действия operator. Формат: "operatorValueString": { "VAL": "<value>" }        |
| VAL                 | string  |               | Значение параметра действия operator                                                                   |

</details>	

<details><summary>Example</summary>

```
{
  "Name": "SetParam",
  "Data": {
    "paramName": {
      "variable": "GT_B_Address"
    },
    "valueType": "Field",
    "paramValueField": {
      "variable": "GT_A_Address"
    },
    "operator": "DelPrefix",
    "operatorValueString": {
      "VAL": "5"
    }
  }
}
```

</details>

-----

| Field CORE | Version CORE | Field WEB | Version WEB | Description           | Help    | 
|------------|--------------|-----------|-------------|-----------------------|---------|
| SetTag     | -            | SET_TAG   | -           | Задать сообщению тег  | -       |

<details><summary>Action parameters</summary>

| Parameters | Type   | Default value | Description        | 
|------------|--------|---------------|--------------------|
| TagName    | string | -             | Наименование тега  |

</details>	

<details><summary>Example</summary>

```
{
   "Name": "SetTag",
  "Data": {
    "TagName1": "tag_1",
    "TagNameN": "tag_N"
  }
}
```

</details>

-----