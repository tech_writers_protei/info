---
title: "RouterTCAP"
description: "Параметры маршрутизатора RouterTCAP"
weight: 30
type: docs
---

**RouterTCAP** маршрутизирует сообщения по коду операции и имен контекста приложения.

**Примечание.** Также может подменять значения CdPA, CgPA.

### Описание параметров ###

| Параметр                                       | Описание                                                                                                                                  | Тип        | O/M | P/R | Версия |
|------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| ID                                             | Уникальный идентификатор узла маршрутизации.                                                                                              | int        | O   | R   |        |
| Name                                           | Уникальное имя узла маршрутизации.                                                                                                        | string     | M   | R   |        |
| Type                                           | Тип узла маршрутизации. Должно быть `RouterTCAP`.                                                                                         | string     | M   | R   |        |
| Routing                                        | Параметры переадресации.                                                                                                                  | \[object\] | M   | R   |        |
| *{*                                            |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Key                                | Главный ключ для задания адресации.                                                                                                       | \[object\] | M   | R   |        |
| &nbsp;&nbsp;*{*                                |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;AppContextType         | Тип контекста приложения.<br>`MAP` / `CAP`. По умолчанию: MAP.                                                                            | string     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[OpCode](#opcode)      | Код операции.                                                                                                                             | int        | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[AppContextName](#acn) | Имя прикладного контекста.<br>**Примечание.** Записываются только цифры, без точек.                                                       | string     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;OpCodeMask             | Шаблон кода операции.                                                                                                                     | regex      | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;ComponentType          | Флаг наличия вложенности в TCAP.                                                                                                          | bool       | O   | R   |        |
| &nbsp;&nbsp;*}*                                |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;Route                              | Параметры последующей маршрутизации при совпадении ключа.                                                                                 | object     | O   | R   |        |
| &nbsp;&nbsp;*{*                                |                                                                                                                                           |            |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_A_set](#gt_set)    | Параметры измененного GT отправителя.                                                                                                     | object     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;[GT_B_set](#gt_set)    | Параметры измененного GT получателя.                                                                                                      | object     | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;GOTO                   | Название узла, куда перенаправляется запрос.<br>**Примечание.** Должно совпадать с именем существующего правила / маршрутизатора / "OUT". | string     | M   | R   |        |
| &nbsp;&nbsp;*}*                                |                                                                                                                                           |            |     |     |        |
| *}*                                            |                                                                                                                                           |            |     |     |        |

#### Параметры изменения GT {#gt_set}

| Параметр              | Описание                                                       | Тип    | O/M | P/R | Версия |
|-----------------------|----------------------------------------------------------------|--------|-----|-----|--------|
| TT                    | Значение <abbr title="Translation Type">TT</abbr>.             | int    | O   | R   |        |
| NAI                   | Значение <abbr title="Nature of Address Indicator">NAI</abbr>. | int    | O   | R   |        |
| SSN                   | Значение <abbr title="Subsystem Number">SSN</abbr>.            | int    | O   | R   |        |
| NP                    | Значение <abbr title="Numbering Plan">NP</abbr>.               | int    | O   | R   |        |
| ES                    | Значение <abbr title="Encoding Scheme">ES</abbr>.              | int    | O   | R   |        |
| RI                    | Значение <abbr title="Routing Indicator">RI</abbr>.            | int    | O   | R   |        |
| Address               | Параметры изменения адреса.                                    | object | O   | R   |        |
| *{*                   |                                                                |        |     |     |        |
| &nbsp;&nbsp;DelDigits | Количество удаляемых цифр из начала номера.                    | int    | O   | R   |        |
| &nbsp;&nbsp;AddPrefix | Символы, добавляемые в начало номера.                          | string | O   | R   |        |
| *}*                   |                                                                |        |     |     |        |

#### Пример ####

```json
{
  "ID": 30,
  "Name": "MainSS7",
  "Type": "RouterTCAP",
  "Routing": [
    {
      "Key": { 
        "AppContextType": "MAP",
        "OpCode": 56,
        "AppContextName": "040001013",        
        "OpCodeMask": "3|4|81"       
      }, 
      "Route": {
        "GT_A_set": {},
        "GT_B_set": {},
        "GOTO": "MO_Router"
      }
    },
    {
      "Key": { "OpCode": 2 },
      "Route": { "GOTO": "UL_Router" }
    },
    {
      "Key": { "OpCode": 7 },
      "Route": { "GOTO": "ISD_Router" }
    },
    {
      "Key": { "OpCode": 3 },
      "Route": { "GOTO": "CL_Router" }
    },
    {
      "Key": {},
      "Route": { "GOTO": "UnknownOpCodeRule" }
    }
  ]
}
```

#### Имена контекстов приложения, ACN {#acn}

Подробную информацию см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).

* MAP:
  * networkLocUpContext -- "0400101";
  * locationCancellationContext -- "0400102";
  * roamingNumberEnquiryContext -- "0400103";
  * istAlertingContext -- "0400104";
  * locationInfoRetrievalContext -- "0400105";
  * callControlTransferContext -- "0400106";
  * reportingContext -- "0400107";
  * callCompletionContext -- "0400108";
  * serviceTerminationContext -- "0400109";
  * resetContext -- "04001010";
  * handoverControlContext -- "04001011";
  * sIWFSAllocationContext -- "04001012";
  * equipmentMngtContext -- "04001013";     
  * infoRetrievalContext -- "04001014";
  * interVlrInfoRetrievalContext -- "04001015";
  * subscriberDataMngtContext -- "04001016";
  * tracingContext -- "04001017";
  * networkFunctionalSsContext -- "04001018";
  * networkUnstructuredSsContext -- "04001019";
  * shortMsgGatewayContext -- "04001020";
  * shortMsgRelayContext -- "04001021";
  * shortMsgMO_RelayContext -- "04001021";
  * subscriberDataModificationNotificationContext -- "04001022";
  * shortMsgAlertContext -- "04001023";
  * mwdMngtContext -- "04001024";
  * shortMsgMT_RelayContext -- "04001025";
  * imsiRetrievalContext -- "04001026";
  * msPurgingContext -- "04001027";
  * subscriberInfoEnquiryContext -- "04001028";
  * anyTimeInterrogation -- "04001029";
  * groupCallControlContext -- "04001031";
  * gprsLocationUpdateContext -- "04001032";
  * gprsLocationInfoRetrievalContext -- "04001033";
  * failureReportContext -- "04001034";
  * gprsNotifyContext -- "04001035";
  * ss_InvocationNotificationContext -- "04001036";
  * locationSvcGatewayContext -- "04001037";
  * locationSvcEnquiryContext -- "04001038";
  * authenticationFailureReportContext -- "04001039";
  * mm_EventReportingContext -- "04001042";
  * anyTimeInfoHandlingContext -- "04001043";
* CAP:
  * capv3AC -- "0400121";
  * capv2AC -- "04001050";

#### Коды операций, OpCode {#opcode}

Подробную информацию см. [3GPP TS 29.002](https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf).

* MAP:
  * -2 -- MAP_BeginSubscriberActivityErricsson;
  * 2 -- MAP_UpdateLocation;
  * 3 -- MAP_CancelLocation;
  * 4 -- MAP_ProvideRoamingNumber;
  * 5 -- MAP_NoteSubscriberDataModified;
  * 6 -- MAP_ResumeCallHandling;
  * 7 -- MAP_InsertSubscriberData;
  * 8 -- MAP_DeleteSubscriberData;
  * 9 -- MAP_SendParameters;
  * 10 -- MAP_RegisterSS;
  * 11 -- MAP_EraseSS;
  * 12 -- MAP_ActivateSS;
  * 13 -- MAP_DeactivateSS;
  * 14 -- MAP_InterrogateSS;
  * 15 -- MAP_AuthenticationFailureReport;
  * 17 -- MAP_RegisterPassword;
  * 18 -- MAP_GetPassword;
  * 19 -- MAP_ProcessUnstructuredSS_Data;
  * 22 -- MAP_SendRoutingInfo;
  * 23 -- MAP_UpdateGprsLocation;
  * 24 -- MAP_SendRoutingInfoForGprs;
  * 25 -- MAP_FailureReport;
  * 26 -- MAP_NoteMsPresentForGprs;
  * 28 -- MAP_PerformHandover;
  * 29 -- MAP_SendEndSignal;
  * 30 -- MAP_PerformSubsequentHandover;
  * 31 -- MAP_ProvideSIWFSNumber;
  * 32 -- MAP_SIWFSSignallingModify;
  * 33 -- MAP_ProcessAccessSignalling;
  * 34 -- MAP_ForwardAccessSignalling;
  * 35 -- MAP_NoteInternalHandover;
  * 37 -- MAP_Reset;
  * 38 -- MAP_ForwardCheckSS_Indication;
  * 39 -- MAP_PrepareGroupCall;
  * 40 -- MAP_SendGroupCallEndSignal;
  * 41 -- MAP_ProcessGroupCallSignalling;
  * 42 -- MAP_ForwardGroupCallSignalling;
  * 43 -- MAP_CheckIMEI;
  * 44 -- MAP_MT_ForwardSM;
  * 45 -- MAP_SendRoutingInfoForSM;
  * 46 -- MAP_ForwardSM;
  * 46 -- MAP_MO_ForwardSM;
  * 47 -- MAP_ReportSM_DeliveryStatus;
  * 48 -- MAP_NoteSubscriberPresent;
  * 49 -- MAP_AlertServiceCentreWithoutResult;
  * 50 -- MAP_ActivateTraceMode;
  * 51 -- MAP_DeactivateTraceMode;
  * 52 -- MAP_TraceSubscriberActivity;
  * 54 -- MAP_BeginSubscriberActivity;
  * 55 -- MAP_SendIdentification;
  * 56 -- MAP_SendAuthenticationInfo;
  * 57 -- MAP_RestoreData;
  * 58 -- MAP_SendIMSI;
  * 59 -- MAP_ProcessUnstructuredSS_Request;
  * 60 -- MAP_UnstructuredSS_Request;
  * 61 -- MAP_UnstructuredSS_Notify;
  * 62 -- MAP_AnyTimeSubscriptionInterrogation;
  * 63 -- MAP_InformServiceCentre;
  * 64 -- MAP_AlertServiceCentre;
  * 65 -- MAP_AnyTimeModification;
  * 66 -- MAP_ReadyForSM;
  * 67 -- MAP_PurgeMS;
  * 68 -- MAP_PrepareHandover;
  * 69 -- MAP_PrepareSubsequentHandover;
  * 70 -- MAP_ProvideSubscriberInfo;
  * 71 -- MAP_AnyTimeInterrogation;
  * 72 -- MAP_SS_InvocationNotification;
  * 73 -- MAP_SetReportingState;
  * 74 -- MAP_StatusReport;
  * 75 -- MAP_RemoteUserFree;
  * 76 -- MAP_RegisterCC_Entry;
  * 77 -- MAP_EraseCC_Entry;
  * 83 -- MAP_ProvideSubscriberLocation;
  * 85 -- MAP_SendRoutingInfoForLCS;
  * 86 -- MAP_SubscriberLocationReport;
  * 87 -- MAP_IST_Alert;
  * 88 -- MAP_IST_Command;
  * 89 -- MAP_NoteMM_Event;
* CAP:
  * 0 -- CAP_InitialDP;
  * 16 -- CAP_AssistRequestInstructions;
  * 17 -- CAP_EstablishTemporaryConnection;
  * 18 -- CAP_DisconnectForwardConnection;
  * 19 -- CAP_ConnectToResource;
  * 20 -- CAP_Connect;
  * 22 -- CAP_ReleaseCall;
  * 23 -- CAP_RequestReportBCSMEvent;
  * 24 -- CAP_EventReportBCSM;
  * 31 -- CAP_Continue;
  * 33 -- CAP_ResetTimer;
  * 34 -- CAP_FurnishChargingInformation;
  * 35 -- CAP_ApplyCharging;
  * 36 -- CAP_ApplyChargingReport;
  * 41 -- CAP_CallGap;
  * 44 -- CAP_CallInformationReport;
  * 45 -- CAP_CallInformationRequest;
  * 46 -- CAP_SendChargingInformation;
  * 47 -- CAP_PlayAnnouncement;
  * 48 -- CAP_PromptAndCollectUserInformation;
  * 49 -- CAP_SpecializedResourceReport;
  * 53 -- CAP_Cancel;
  * 55 -- CAP_ActivityTest;
  * 60 -- CAP_InitialDPSMS;
  * 61 -- CAP_FurnishChargingInformationSMS;
  * 62 -- CAP_ConnectSMS;
  * 63 -- CAP_RequestReportSMSEvent;
  * 64 -- CAP_EventReportSMS;
  * 65 -- CAP_ContinueSMS;
  * 66 -- CAP_ReleaseSMS;
  * 67 -- CAP_ResetTimerSMS;
  * 70 -- CAP_ActivityTestGPRS;
  * 71 -- CAP_ApplyChargingGPRS;
  * 72 -- CAP_ApplyChargingReportGPRS;
  * 73 -- CAP_CancelGPRS;
  * 74 -- CAP_ConnectGPRS;
  * 75 -- CAP_ContinueGPRS;
  * 76 -- CAP_EntityReleasedGPRS;
  * 77 -- CAP_FurnishChargingInformationGPRS;
  * 78 -- CAP_InitialDPGPRS;
  * 79 -- CAP_ReleaseGPRS;
  * 80 -- CAP_EventReportGPRS;
  * 81 -- CAP_RequestReportGPRSEvent;
  * 82 -- CAP_ResetTimerGPRS;
  * 83 -- CAP_SendChargingInformationGPRS;
  * 88 -- CAP_ContinueWithArgument.
