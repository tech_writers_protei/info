---
title: "gtp.cfg"
description: "Параметры логики GTP"
weight: 20
type: docs
---

В файле задаются настройки работы по протоколам GTP.

## Используемые секции

* [\[General\]](#general-gtp) -- общие параметры работы с GTP;
* [\[SeqNumberValidation\]](#seq-number-validation) -- параметры проверки транзакций по значению `Sequence Number`;
* [\[GTP_C_Limits\]](#gtp-c-limits) -- параметры ограничений для протокола GTP-C;
* [\[GtpSession\]](#gtp-session-gtp) -- параметры хранения GTP-сессий.

### Описание параметров ###

| Параметр                                                        | Описание                                                                                                                                                                                                                                                                                                       | Тип           | O/M | P/R | Версия    |
|-----------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|-----|-----|-----------|
| **<a name="general-gtp">\[General\]</a>**                       | Общие параметры работы с GTP.                                                                                                                                                                                                                                                                                  | object        | O   | P   | 1.0.14.10 |
| Mode                                                            | Режим работы с сетью.<br>`DPDK` -- форвардинг пакетов с сетевых интерфейсов;<br>`UPF` -- получение и отправка GTP-C пакетов через UPF.<br>По умолчанию: DPDK.                                                                                                                                                  | string        | O   | P   | 1.1.0.29  |
| SeqNumberValidationEnabled                                      | Флаг включения проверки ответов по порядковому номеру, `Sequence Number`, на уровне транзакции.<br>По умолчанию: 0.                                                                                                                                                                                            | bool          | O   | R   | 1.0.14.10 |
| CheckWhiteListIpMasks                                           | Флаг ограничения обработки сообщений только с направления в списке [Directions](#directions) для GTP-C/GTP-U.                                                                                                                                                                                                  | bool          | O   | R   | 1.1.0.29  |
| gtpCSessionValidationEnable                                     | Флаг проверки соответствия GTP-C сообщений установленным сессиям. <br>По умолчанию: 0.                                                                                                                                                                                                                         | bool          | O   | R   | 1.1.2.0   |
| gtpSessionSave                                                  | Флаг сохранения GTP-сессий в базе данных. <br>По умолчанию: 0.                                                                                                                                                                                                                                                 | bool          | O   | R   | 1.1.2.0   |
| **<a name="seq-number-validation">\[SeqNumberValidation\]</a>** | Параметры алгоритма проверки транзакций по порядковому номеру, `Sequence Number`.                                                                                                                                                                                                                              | object        | O   | P   | 1.0.14.10 |
| <a name="transaction-expire-time">TransactionExpireTime</a>     | Время хранения транзакций, в миллисекундах.<br>По умолчанию: 30&nbsp;000.                                                                                                                                                                                                                                      | int           | O   | R   | 1.0.14.10 |
| TransactionCleanPeriod                                          | Период просмотра таблицы с транзакциями для удаления записей, с временем хранения большим, чем [TransactionExpireTime](#transaction-expire-time), в миллисекундах.<br>По умолчанию: 3000.                                                                                                                      | int           | O   | R   | 1.0.14.10 |
| <a name="transaction-store-size">TransactionStoreSize</a>       | Размер таблицы для хранения транзакций.<br>По умолчанию: 10&nbsp;000&nbsp;000.                                                                                                                                                                                                                                 | int           | O   | P   | 1.0.14.10 |
| **<a name="gtp-c-limits">\[GTP_C_Limits\]</a>**                 | Параметры ограничения пропускной способности GTP-C интерфейса.                                                                                                                                                                                                                                                 | object        | O   | R   | 1.1.0.29  |
| <a name="max-queue-size">MaxQueueSize</a>                       | Максимальный размер очереди примитивов.<br>По умолчанию: 0, ограничение не используется.<br>**Примечание.** По достижении активируется аварийный режим: дальнейшие действия с GTP-C пакетами определяется лишь значением поля [TransmitOverloadPackets](#transmit-overload-packets), без проверки по правилам. | int           | O   | R   | 1.1.0.29  |
| NormalQueueSize                                                 | Порог очереди примитивов для отключения аварийного режима и обработки GTP-C пакетов согласно правилам.<br>По умолчанию: 0.8 * [MaxQueueSize](#max-queue-size).                                                                                                                                                 | int           | O   | R   | 1.1.0.29  |
| <a name="transmit-overload-packets">TransmitOverloadPackets</a> | Флаг передачи GTP-C пакетов при активации аварийного режима или отсутствии свободных логик.<br>По умолчанию: 1.                                                                                                                                                                                                | bool          | O   | R   | 1.1.0.29  |
| DefaultMaxPackets                                               | Максимальное пропускаемое количество GTP-C пакетов в секунду, не попадающих ни под одно из направлений, заданных в поле [Directions](#directions).<br>По умолчанию: 0, ограничение не используется.                                                                                                            | int           | O   | R   | 1.1.0.29  |
| MinLength                                                       | Минимальный размер GTP-C пакета.<br>По умолчанию: 0, ограничение не используется.                                                                                                                                                                                                                              | int           | O   | R   | 1.1.0.29  |
| MaxLength                                                       | Максимальный размер GTP-C пакета.<br>По умолчанию: 0, ограничение не используется.                                                                                                                                                                                                                             | int           | O   | R   | 1.1.0.29  |
| <a name="directions">Directions</a>                             | Параметры для ограничений пропускной способности по направлениям.                                                                                                                                                                                                                                              | \[object\]    | O   | R   | 1.1.0.29  |
| IpMasks                                                         | Перечень масок IP-адресов.                                                                                                                                                                                                                                                                                     | \[ip/string\] | M   | R   | 1.1.0.29  |
| MaxPackets                                                      | Максимальное пропускаемое количество GTP-C пакетов в секунду, если адрес источника, `SrcIP`, попадает под одну из масок. <br>По умолчанию: 0                                                                                                                                                                   | int           | O   | R   | 1.1.0.29  |
| **<a name="gtp-session-gtp">\[GtpSession\]</a>**                | Параметры сохранения сессий GTP.                                                                                                                                                                                                                                                                               | object        | O   | P   | 1.1.2.0   |
| SessionStorageSize                                              | Размер таблицы для хранения сессий.<br>По умолчанию: 1&nbsp;048&nbsp;576.                                                                                                                                                                                                                                      | int           | O   | P   | 1.1.2.0   |
| SessionExpireTime                                               | Время жизни сессии, в секундах. По умолчанию: 86&nbsp;400.<br>**Примечание.** Если 0, то время жизни не ограничивается.                                                                                                                                                                                        | int           | O   | R   | 1.1.2.0   |
| SessionCleanupInterval                                          | Интервал очистки таблиц от устаревших сессий, в секундах.<br>По умолчанию: 60.                                                                                                                                                                                                                                 | int           | O   | R   | 1.1.2.0   |
| SessionDenyTTL                                                  | Время жизни закешированных неуспешных ответов от Redis по запросам на получение GTP-сессии, в секундах.<br>По умолчанию: 3600.                                                                                                                                                                                 | int           | O   | R   | 1.1.2.0   |

**Примечание.** Размер таблицы для хранения транзакций, [TransactionStoreSize](#transaction-store-size) вычисляется по формуле:

`2 * tps * TransactionExpireTime / 1000`

tps — количество транзакций в секунду.

#### Пример ####

```ini
[General]
Mode = DPDK
SeqNumberValidationEnabled = 0;
CheckWhiteListIpMasks = 1
gtpCSessionValidationEnable = 1;
gtpSessionSave = 1;

[SeqNumberValidation]
TransactionExpireTime = 30000;
TransactionCleanPeriod = 3000;
TransactionStoreSize = 10000000;

[GTP_C_Limits]
MaxQueueSize = 50000
NormalQueueSize = 40000
TransmitOverloadPackets = 1
DefaultMaxPackets = 10000
Directions = {
  {
    IpMasks = { 10.10.1.0/24; e80:a0a4::/32 }
    MaxPackets = 5000
  }
  {
    IpMasks = { 10.10.2.0/24; fe80:a885::/32 }
    MaxPackets = 7000
  }
}

[GtpSession]
SessionStorageSize = 1048576;
SessionExpireTime = 86400;
SessionCleanupInterval = 60;
SessionDenyTTL = 3600;
```