---
title: "long_mnc.json"
description: "Параметры PLMN с трехзначными MNC"
weight: 20
type: docs
---

В файле задаются сети PLMN, имеющие трехзначные MNC.

Консольная команда для обновления конфигурационного файла без перезапуска узла - **reload long_mnc.json**, см. [Управление](../../oam/system_management/).

### Описание параметров ###

| Параметр | Описание                                    | Тип    | O/M | P/R | Версия   |
|----------|---------------------------------------------|--------|-----|-----|----------|
| mcc      | Мобильный код страны.                       | string | M   | R   | 1.1.0.31 |
| mnc      | Код мобильной сети, состоящий из трёх цифр. | string | M   | R   | 1.1.0.31 |

#### Пример ####

```json
[
  {
    "mcc":"404",
    "mnc":"854"
  },
  {
    "mcc":"404",
    "mnc":"874"
  }
]
```

{{<internal_use>}}

### Схема ###

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "longMnc",
  "description": "Массив PLMN, имеющих трёхзначные MNC",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "mcc": {
        "description": "Мобильный код страны",
        "type": "string",
        "minLength": 3,
        "maxLength": 3
      },
      "mnc": {
        "description": "Код мобильной сети, состоящий из трёх цифр",
        "type": "string",
        "minLength": 3,
        "maxLength": 3
      }
    }
  }
}
```

{{</internal_use>}}