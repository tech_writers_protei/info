---
title: "ap_dictionary"
description: "Параметры преобразуемых переменных"
weight: 20
type: docs
---

В файле задаются настройки значения переменных, которые необходимо преобразовывать в приходящих авариях.

### Описание параметров ###

| Параметр                                                | Описание                                                                                                           | Тип    | O/M | P/R | Версия |
|:--------------------------------------------------------|:-------------------------------------------------------------------------------------------------------------------|:-------|:----|:----|:-------|
| CA_Var                                                  | Компонентный адрес переменной.                                                                                     | string | O   | R   |        |
| <a name="int-value">Int_Value</a>                       | Целочисленное значение переменной, которое подлежит замене.                                                        | int    | O   | R   |        |
| String_Value                                            | Строковое значение переменной, которое заменит [Int_Value](#int-value).                                            | string | O   | R   |        |
| Value                                                   | Значение переменной, для которой задается [SP_Trap](#sp-trap).                                                     | int    | O   | R   |        |
| <a name="specific-trap-offset">Specific_Trap_Offset</a> | Cмещение для формирования SP_Trap.                                                                                 | int    | С   | С   |        |
| <a name="sp-trap">SP_Trap</a>                           | Cлужебное слово.<br>**Примечание.** Должно быть всегда при указании [Specific_Trap_Offset](#specific-trap-offset). | string | M   | C   |        |

### Форматы ###

Для каждого значения переменной можно задать свой SpecificTrap.

```ini
<CA_Var> = {
  { <intValue1>; <String_Value>; };
  { <intValue2>; <String_Value>; };
}

<CA_Var> = {
  { <Int_Value>; <String_Value>; }; SP_Trap = <Specific_Trap_Offset>;
  { <Int_Value>; <String_Value>; }; SP_Trap = <Specific_Trap_Offset>;
}

<CA_Var> = {
  <Value>; SP_Trap = <Specific_Trap_Offset>;
}

<CA_Var> = {
  { <Int_Value>; <String_Value>; }; SP_Trap = <Specific_Trap_Offset>;
  { <Int_Value>; <String_Value>; };
  <Value>; SP_Trap = <Specific_Trap_Offset>;
}
```

### Значения Specific_Trap_Offset по умолчанию ###

По умолчанию, значения `SP_Trap` заданы для следующих переменных:

| Переменная | Значение  | SP_Offset |
|:-----------|:----------|:----------|
| OSTATE     | ACTIVATE  | 1         |
| OSTATE     | FAIL      | 2         |
| ASTATE     | UNBLOCKED | 3         |
| ASTATE     | BLOCKED   | 4         |
| HSTATE     | ON        | 5         |
| HSTATE     | OFF       | 6         |

#### Примеры ####

* Значения переменной `OSTATE` 1 и 0 преобразуются, соответственно, в `ACTIVATE` и `FAIL`:

  ```ini
  OSTATE = {
    { 1; ACTIVATE; };
    { 0; FAIL; }
  }
  ```

* Значения переменной `ASTATE` 1 и 0 преобразуются, соответственно, в `UNBLOCKED` и `BLOCKED`, для каждого значения установлен `SP_Trap`:

  ```ini
  ASTATE = {
    { 1; UNBLOCKED; }; SP_Trap = 3;
    { 0; BLOCKED; }; SP_Trap = 4;
  }
  ```

* Значения переменной `HSTATE` 1 и 0 преобразуются, соответственно, в `ON` и `OFF`, для значения `OFF` задан `SP_Trap`:

  ```ini
  HSTATE = {
    { 1; ON; };
    { 0; OFF; }; SP_Trap = 6;
  }
  ```

* Для значений переменной `Alarm` заданы `SP_Trap`:

  ```ini
  Alarm = {
    alarm; SP_Trap = 7;
    no_alarm; SP_Trap = 8;
  }
  ```

#### Пример

```ini
# Dictionary

OSTATE = { { 1; ACTIVATE }; { 0; FAIL }; };
ASTATE = { { 1; UNBLOCKED }; { 0; BLOCKED }; };
HSTATE = { { 1; ON }; { 0; OFF }; };
```