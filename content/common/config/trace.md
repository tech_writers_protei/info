---
title: "trace.cfg"
description: "Параметры ведения xDR и log"
weight: 20
type: docs
---

В файле задаются настройки подсистемы журналирования.

**Примечание.** Наличие файла обязательно.

Консольная команда для обновления конфигурационного файла без перезапуска узла -- **reload trace.cfg**.

### Описание параметров ###

| Параметр                                                        | Описание                                                                                                                                                                                                                                                                                                                                                                                             | Тип                            | O/M | P/R | Версия |
|-----------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------|-----|-----|--------|
| **\[Trace\]**                                                   | Параметры подсистемы журналирования.                                                                                                                                                                                                                                                                                                                                                                 | object                         | O   | R   |        |
| <a name="common">common</a>                                     | Общие настройки системы журналирования.                                                                                                                                                                                                                                                                                                                                                              | object                         | O   | R   |        |
| **{**                                                           |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |
| &nbsp;&nbsp;tracing                                             | Флаг активности системы журналирования.<br>По умолчанию: 1.                                                                                                                                                                                                                                                                                                                                          | bool                           | O   | R   |        |
| &nbsp;&nbsp;dir                                                 | Путь к директории, где находятся журналы.<br>**Примечание.** Путь может содержать ".." и маску формата времени.<br>По умолчанию: от каталога по умолчанию.                                                                                                                                                                                                                                           | string                         | O   | R   |        |
| &nbsp;&nbsp;no_signal                                           | Коды сигналов, не перехватываемых системой журналирования.<br>`all` -- не перехватывать никакие сигналы.<br>**Примечание.** Разделитель -- запятая.<br>По умолчанию: перехватывать все сигналы.                                                                                                                                                                                                      | \[int\]                        | O   | R   |        |
| &nbsp;&nbsp;<a name="separator">separator</a>                   | Разделитель автоматических полей.<br>По умолчанию: " ".<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.                                                                                                                                                                                                                                                      | string                         | O   | R   |        |
| **}**                                                           |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |
| logs                                                            | Перечень параметров журналов. Формат:<br>`{ <log_name_1> = {}; <log_name_N> = {}; }`                                                                                                                                                                                                                                                                                                                 | \[{string,object}\]            | O   | R   |        |
| **{**                                                           |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |
| &nbsp;&nbsp;[mask](#mask)                                       | Маска формата вывода автоматических полей в журнале.                                                                                                                                                                                                                                                                                                                                                 | string                         | O   | R   |        |
| &nbsp;&nbsp;file                                                | Путь к файлу лога.<br>**Примечание.** При указании не существующих директорий создаются все необходимые каталоги. Допускается задание пустого имени файла, если [level](#trace-level) = 0. Тогда запись производится согласно параметру [tee](#tee). При отсутствии запись на диск не производится.<br>Путь может содержать ".." и маску формата времени.<br>По умолчанию: от каталога по умолчанию. | string                         | O   | R   |        |
| &nbsp;&nbsp;<a name="trace-level">level</a>                     | Уровень журнала.<br>**Примечание.** Сообщения с большим уровнем игнорируются.                                                                                                                                                                                                                                                                                                                        | int                            | O   | R   |        |
| &nbsp;&nbsp;<a name="trace-period">period</a>                   | Период обновления файла лога. Формат:<br>`<interval>+<shift>`.<br/>**Пример:** `day+3hour` - файл обновляется каждый день в 3 часа ночи.                                                                                                                                                                                                                                                             | [units*](#measurement-units)   | O   | R   |        |
| &nbsp;&nbsp;\<interval\>                                        | Период между соседними обновлениями.                                                                                                                                                                                                                                                                                                                                                                 | [units^*^](#measurement-units) | O   | R   |        |
| &nbsp;&nbsp;<shift\>                                            | Первоначальный сдвиг.<br/>**Примечание.** Не может превышать длину периода, и в случае некорректного значения игнорируется.                                                                                                                                                                                                                                                                          | [units^*^](#measurement-units) | O   | R   |        |
| &nbsp;&nbsp;buffering                                           | Настройки буферизированной записи.                                                                                                                                                                                                                                                                                                                                                                   | object                         | O   | R   |        |
| &nbsp;&nbsp;**{**                                               |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |
| &nbsp;&nbsp;&nbsp;&nbsp;<a name="cluster-size">cluster_size</a> | Размер кластера, в килобайтах.<br>По умолчанию: 128.                                                                                                                                                                                                                                                                                                                                                 | int                            | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;clusters_in_buffer                      | Количество кластеров [cluster_size](#cluster-size) в буфере.<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                                     | int                            | O   | R   |        |
| &nbsp;&nbsp;&nbsp;&nbsp;overflow_action                         | Действие, выполняемое при переполнении буфера.<br>`erase` -- удаление;<br>`dump` -- запись на диск.<br>По умолчанию: dump.                                                                                                                                                                                                                                                                           | string                         | O   | R   |        |
| &nbsp;&nbsp;**}**                                               |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |
| &nbsp;&nbsp;separator                                           | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.<br>По умолчанию: значение [separator](#separator).                                                                                                                                                                                                                         | string                         | O   | R   |        |
| &nbsp;&nbsp;[type](#type)                                       | Тип журнала и дополнительные настройки.                                                                                                                                                                                                                                                                                                                                                              | string                         | O   | R   |        |
| &nbsp;&nbsp;<a name="tee">tee</a>                               | Дублирование потока вывода.<br>`stdout` / `cout` / `info` / `<log_file_name>`.<br>**Примечание.** При знаке минуса "–" не пишется имя исходного лога при дублировании.                                                                                                                                                                                                                               | string                         | O   | R   |        |
| &nbsp;&nbsp;limit                                               | Максимальное количество строк в файле.<br>**Примечание.** По достижении предела строк файл автоматически открывается заново. Действительное количество строк в файле не исследуется. Если имя файла зависит от времени, то открывается новый файл, иначе файл очищается.                                                                                                                             | int                            | O   | R   |        |
| &nbsp;&nbsp;force_recreate                                      | Флаг создания пустых журналов по прошествии периода [period](#trace-period).<br>По умолчанию: 0.                                                                                                                                                                                                                                                                                                     | bool                           | O   | R   |        |
| **}**                                                           |                                                                                                                                                                                                                                                                                                                                                                                                      |                                |     |     |        |

#### Единицы измерения period, interval, shift {#measurement-units}

* `sec` -- секунда;
* `min` -- минута;
* `hour` -- час;
* `day` -- день;
* `week` -- неделя;
* `month` -- месяц;
* `year` -- год.

#### Модификаторы mask {#mask}

Маска формата вывода автоматических полей в журнале.

| Параметр                | Описание                                                                                               | Тип        |
|-------------------------|--------------------------------------------------------------------------------------------------------|------------|
| date                    | Дата создания. Формат: `DD/MM/YY`                                                                      | date       |
| <a name="time">time</a> | Время создания. Формат: `hh:mm:ss`                                                                     | time       |
| tick                    | Миллисекунды. Формат:<br>если задано [time](#time): `.mss`;<br>если не задано [time](#time): `.mssmss` | string     |
| state                   | Состояние системы.                                                                                     | int/string |
| pid                     | Идентификатор процесса. Формат: `xxxxxx`                                                               | int        |
| tid                     | Идентификатор потока. Формат: `xxxxxx`                                                                 | int        |
| level                   | Уровень журнала для записи.                                                                            | int        |
| file                    | Файл и строка в файле с исходным кодом, откуда производится вывод.                                     | string     |

#### Модификаторы type {#type}

Три пары взаимоисключающих значений: log/cdr, truncate/append, name_now/name_period.

| Параметр                        | Описание                                                                                                          |
|---------------------------------|-------------------------------------------------------------------------------------------------------------------|
| <a name="name-now">name_now</a> | Текущее время для имени файла.                                                                                    |
| name_period                     | Начало периода записи.                                                                                            |
| <a name="truncate">truncate</a> | Флаг очистки файла при открытии.                                                                                  |
| <a name="append">append</a>     | Файл добавления информации в конец файла.                                                                         |
| log                             | Журнал логов, состоит из [truncate](#truncate) и [name_now](#name-now), при падении пишется информация о сигнале. |
| cdr                             | Журнал CDR, состоит из [append](#append) и [name_now](#name-now), при падении не пишется информация о сигнале.    |

#### Зарезервированные имена журналов ####

* **stdout** -- стандартный вывод;
* **stderr** -- стандартный вывод ошибок;
* **trace** -- журнал по умолчанию;
* **warning** -- журнал предупреждений;
* **error** -- журнал ошибок;
* **config** -- журнал чтения конфигурации;
* **info** -- журнал информации о событиях, адаптирован для стороннего пользователя.

#### Пример ####

```
[Trace]
common = {
  tracing = 1;
  dir = ".";
  no_signal = all;
}

logs = {
  stat = {
    file = "logs/stat/stat-%Y%m%d.log";
    mask = date & time & tick;
    level = 4;
    separator = ";";
  };
  tc_stat = {                                                                                                                                                                                         
    file = "logs/stat.log";                                                                                                                                                                        
    mask = file & date & time & tick;                                                                                                                                                                         
    level = 4;                                                                                                                                                                                        
    separator = ";";                                                                                                                                                                                   
  };
  trace = {
    file = "logs/trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    period = day;
    separator = ";";
  };
  SS7FW_trace = {
    file = "logs/ss7fw_trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    period = day;
    tee = trace;
    separator = ";";
  };
  http_trace = {
    file = "logs/http_trace.log";
    mask = file & date & time & tick & pid;
    level = 10;
    separator = ";";
    tee = trace;
  };
  http_warning = {
    file = "logs/http_warning.log";
    mask = file & date & time & tick & pid;
    level = 1;
    separator = ";";
  };
  GTP_C_trace = {
    file = "logs/gtp_c/GTP_C_%H%M_%d%m%Y.log";
    mask = date & time & tick;
    period = hour;
    level = 10;
  };
  GTP_C_warning = {
    file = "logs/gtp_c_warning.log";
    mask = date & time & tick;
    level = 10;
  };
  profilers = {
    file = "logs/profile.log";
    mask = date & time & tick;
    level = 1;
    separator = ";";
  };
  alarm_info = {
    file = "logs/alarm_info.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 12;
  };
  alarm_trace = {
    file = "logs/alarm_trace.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  alarm_cdr = {
    file = "logs/cdr/alarm_cdr.log";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 4;
  };
  config = {
    file = "logs/config.log";
    mask = file & date & time & tick & pid;
    level = 1;
    period = hour;
    tee = trace;
  };
  warning = {
    file = "logs/warning.log";
    mask = date & time & tick & file;
    level = 1;
    tee = trace;
  };
  info = {
    file = "logs/info.log";
    mask = date & time & tick & file;
    level = 10;
    tee = trace;
  };
  si = {
    file = "logs/si_trace.log";
    mask = date & time & tick & pid & file;
    level = 10;
    tee = trace;
  };
  SigFW_cdr_map = {
    file = "cdr/map/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SigFW_cdr_cap = {
    file = "cdr/cap/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SigFW_tcap_invalid = {
    file = "cdr/tcap_ivalid/cdr-%Y%m%d-%H%M.log";
    separator = ";";
    period = "10min";
    level = 1;
  };
  SS7FW_DIAM_cdr = {
    file = "cdr/cdr_diam/cdr_diam-%Y%m%d-%H%M.log";
    mask = date & time & tick;
    separator = "     ";
    period = day;
    level = 1;
  };
  SS7FW_ATI_cdr = {
    file = "cdr/cdr_ati/cdr_ati-%Y%m%d-%H%M.log";
    mask = date & time & tick;
    separator = ";";
    period = day;
    level = 1;
  };
  SS7FW_udp_cdr = {
    file = "logs/cdr_udp.log";
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  SS7FW_udp_in_cdr = {
    file = "logs/cdr_udp_in.log";
    mask = date & time & tick;
    separator = ";";
    level = 0;
  };
  gtp_c_cdr = {
    file = "cdr/gtp_c/GTP_C_%H%M_%d%m%Y.cdr";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 1;
  };
  gtp_c_invalid_cdr = {
    file = "cdr/gtp_c_invalid/GTP_C_Invalid_%H%M_%d%m%Y.cdr";
    period = hour;
    mask = date & time & tick;
    separator = ";";
    level = 1;
  };
  gtp_mi_cdr = {
    file="cdr/gtp_mi/GTP_MI_%H%M_%d%m%Y.cdr";
    period=hour;
    mask="date & time & tick";
    separator=";";
    level=1;
  };
}
```