---
title: "ap.cfg"
description: "Параметры подсистемы сбора аварий"
weight: 20
type: docs
---
---
title: "ap.cfg"
description: "Параметры подсистемы сбора аварий"
weight: 20
type: docs
---

В файле задаются настройки подсистемы аварийной индикации, Alarm Processor, SNMP-соединений и правил преобразования компонентных адресов в SNMP-адреса.

**Внимание!** Крайне не рекомендуется менять параметры в этом файле.

### Используемые секции ###

* **[General](#general-ap)** -- основные параметры;
* **[Dynamic](#dynamic)** -- переменные и их значения, при которых удаляются динамические объекты;
* **[SNMP](#snmp)** -- параметры SNMP;
* **[StandardMib](#standardmib)** -- параметры объектов из MIB-файла;
* **[AtePath2ObjName](#atepath2objname)** -- правила преобразования АТЕ-пути в SNMP-путь;
* **[SNMPTrap](#snmptrap)** -- правила отправки траповых переменных;
* **[Filter](#filter)** -- параметры фильтрации аварий;
* **[SpecificTrapCA_Object](#st_ca_object)** -- параметры базы SpecificTrap для компонентных адресов;
* **[SpecificTrapCT_Object](#st_ct_object)** -- параметры базы SpecificTrap для компонентных типов;
* **[SpecificTrapCA_Var](#st_ca_var)** -- параметры смещения SpecificTrap;
* **[Logs](#logs)** -- параметры ведения лог-файла;
* **[FilterLevel](#filterlevel)** -- правила фильтрации аварий по журналам;
* **[SNMPv3](#snmpv3)** -- настройки SNMPv3.

### Описание параметров ###

| Параметр                                                 | Описание                                                                                                                                                                                                                                          | Тип        | O/M | P/R | Версия |
|----------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------|-----|-----|--------|
| **<a name="general-ap">\[General\]</a>**                 | Основные параметры.                                                                                                                                                                                                                               | object     | M   | P   |        |
| Root                                                     | Корень дерева.<br>По умолчанию: PROTEI(1.3.6.1.4.1.20873).                                                                                                                                                                                        | string     | O   | P   |        |
| <a name="application-address">ApplicationAddress</a>     | Адрес приложения.<br>По умолчанию: \<application_name\>.                                                                                                                                                                                          | string     | M   | R   |        |
| <a name="max-connection-count">MaxConnectionCount</a>    | Максимальное количество одновременных подключений.<br>По умолчанию: 10.                                                                                                                                                                           | int        | O   | R   |        |
| ManagerThread                                            | Флаг запуска встроенного менеджера в отдельном потоке.<br>По умолчанию: 0.                                                                                                                                                                        | bool       | O   | P   |        |
| <a name="cyclic-walk-tree">CyclicWalkTree</a>            | Флаг циклического обхода деревьев.<br>По умолчанию: 0.                                                                                                                                                                                            | bool       | O   | R   |        |
| **<a name="dynamic">\[Dynamic\]</a>**                    | Переменные и их значения, при которых динамические объекты следует удалять.                                                                                                                                                                       | object     | O   | P   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                                                                                                                                    | string     | O   | P   |        |
| value                                                    | Значение переменной.                                                                                                                                                                                                                              | string     | O   | P   |        |
| **<a name="snmp">\[SNMP\]</a>**                          | Настройки SNMP.                                                                                                                                                                                                                                   | object     | O   | P   |        |
| ListenIP                                                 | IP-адрес, с которым будет устанавливать соединение система обработки сообщений AlarmProcessor.<br>По умолчанию: 0.0.0.0.                                                                                                                          | ip         | O   | P   |        |
| ListenPort                                               | Прослушиваемый порт.<br>Диапазон: 0-65535.<br>По умолчанию: 161.                                                                                                                                                                                  | int        | O   | P   |        |
| OwnEnterprise                                            | SNMP-адрес приложения.<br>По умолчанию: 1.3.6.1.4.1.20873.                                                                                                                                                                                        | string     | O   | P   |        |
| <a name="check-community">CheckCommunity</a>             | Флаг проверки сообщества.<br>По умолчанию: 0.                                                                                                                                                                                                     | bool       | O   | P   |        |
| **<a name="standardmib">\[StandardMib\]</a>**            | Параметры объектов из стандартного MIB-файла.                                                                                                                                                                                                     | object     | O   | R   |        |
| addrSNMP                                                 | Адрес SNMP переменной.                                                                                                                                                                                                                            | string     | O   | R   |        |
| typeVar                                                  | Тип переменной.                                                                                                                                                                                                                                   | string     | O   | R   |        |
| value                                                    | Значение переменной.                                                                                                                                                                                                                              | string     | O   | R   |        |
| **<a name="atepath2objname">\[AtePath2ObjName\]</a>**    | Правила преобразования АТЕ-пути в SNMP-путь.                                                                                                                                                                                                      | object     | O   | P   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                                                                                                                                         | regex      | O   | P   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                                                                                                                                    | string     | O   | P   |        |
| **<a name="snmptrap">\[SNMPTrap\]</a>**                  | Правила посылки трапов.                                                                                                                                                                                                                           | object     | O   | R   |        |
| ipManagerSNMP                                            | IP-адрес SNMP-менеджера.                                                                                                                                                                                                                          | ip         | O   | R   |        |
| portManagerSNMP                                          | Порт SNMP-менеджера.<br>Диапазон: 0-65535.                                                                                                                                                                                                        | int        | O   | R   |        |
| caObjectFilter                                           | Фильтр по адресу объекта.                                                                                                                                                                                                                         | regex      | O   | R   |        |
| ctObjectFilter                                           | Фильтр по типу объекта.                                                                                                                                                                                                                           | regex      | O   | R   |        |
| caVarFilter                                              | Фильтр по адресу переменной.                                                                                                                                                                                                                      | regex      | O   | R   |        |
| **<a name="filter">\[Filter\]</a>**                      | Параметры фильтрации аварий.                                                                                                                                                                                                                      | object     | O   | P   |        |
| caObject                                                 | Фильтр по адресу объекта.<br>По умолчанию: `.*`.                                                                                                                                                                                                  | regex      | O   | P   |        |
| ctObject                                                 | Фильтр по типу объекта.<br>По умолчанию: `.*`.                                                                                                                                                                                                    | regex      | O   | P   |        |
| caVar                                                    | Фильтр по адресу переменной.<br>По умолчанию: `.*`.                                                                                                                                                                                               | regex      | O   | P   |        |
| TrapIndicator                                            | Фильтр по индикатору трапа.<br>По умолчанию: 1.                                                                                                                                                                                                   | string     | O   | P   |        |
| DynamicIndicator                                         | Фильтр по индикатору динамического объекта.<br>По умолчанию: 0.                                                                                                                                                                                   | string     | O   | P   |        |
| **<a name="st_ca_object">\[SpecificTrapCA_Object\]</a>** | Параметры базы SpecificTrap для компонентных адресов.                                                                                                                                                                                             | object     | O   | R   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                                                                                                                                    | regex      | O   | R   |        |
| specificTrapBase                                         | Значение для задания базы SP.                                                                                                                                                                                                                     | int        | O   | R   |        |
| **<a name="st_ct_object">\[SpecificTrapCT_Object\]</a>** | Параметры базы SpecificTrap для компонентных типов.                                                                                                                                                                                               | object     | O   | R   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                                                                                                                                         | string     | O   | R   |        |
| specificTrapBase                                         | Значение для задания базы SP.                                                                                                                                                                                                                     | int        | O   | R   |        |
| **<a name="st_ca_var">\[SpecificTrapCA_Var\]</a>**       | Параметры смещения SpecificTrap.                                                                                                                                                                                                                  | object     | O   | R   |        |
| caObject                                                 | Компонентный адрес объекта.                                                                                                                                                                                                                       | regex      | O   | R   |        |
| specificTrapOffset                                       | Смещение для задания SP.                                                                                                                                                                                                                          | int        | O   | R   |        |
| **<a name="logs">\[Logs\]</a>**                          | Параметры записи текущего состояния объектов в файл.                                                                                                                                                                                              | object     | O   | R   |        |
| TreeTimerPeriod                                          | Период сохранения текущего состояния объектов в лог-файлах, в миллисекундах.<br>По умолчанию: 60&nbsp;000.                                                                                                                                        | int        | O   | R   |        |
| **<a name="filterlevel">\[FilterLevel\]</a>**            | Правила фильтрации аварий по журналам.                                                                                                                                                                                                            | \[object\] | O   | P   |        |
| caObject                                                 | Компонентный адрес объекта.                                                                                                                                                                                                                       | regex      | O   | P   |        |
| ctObject                                                 | Компонентный тип объекта.                                                                                                                                                                                                                         | regex      | O   | P   |        |
| caVar                                                    | Компонентный адрес переменной.                                                                                                                                                                                                                    | regex      | O   | P   |        |
| nLevel                                                   | Уровень журнала.                                                                                                                                                                                                                                  | int        | O   | P   |        |
| **<a name="snmpv3">\[SNMPv3\]</a>**                      | Настройки SNMPv3 для корректного формирования [SNMP_EngineID](#snmp_engine_id) и [ContextEngineID](#context_engine_id). См. [RFC 1910](https://datatracker.ietf.org/doc/html/rfc1910), [RFC 3411](https://datatracker.ietf.org/doc/html/rfc3411). | object     | O   | P   |        |
| <a name="engine-id-conformance">EngineIdConformance</a>  | Флаг поддержки SNMPv3.<br>По умолчанию: 1.                                                                                                                                                                                                        | bool       | O   | P   |        |
| <a name="engine-id-enterprise">EngineIdEnterprise</a>    | Идентификатор организации.<br>По умолчанию: 20873.                                                                                                                                                                                                | string     | O   | P   |        |
| [EngineIdFormat](#engine_id_format)                      | Индикатор форматирования 6 октета и далее, является 5 октетом. См. [RFC 3411](https://datatracker.ietf.org/doc/html/rfc3411).<br>По умолчанию: 4.                                                                                                 | int        | O   | P   |        |
| MaxMessageSize                                           | Максимальный размер обрабатываемого сообщения в октетах.<br>**Примечание.** Используется только для SNMPv3.<br>По умолчанию: 484.                                                                                                                 | int        | O   | P   |        |
| EngineBoots                                              | Количество загрузок модуля с момента последнего конфигурирования.<br>**Примечание.** Используется только для SNMPv3.<br>По умолчанию: 0.                                                                                                          | int        | O   | P   |        |
| TimeWindow                                               | Интервал обновления.<br>По умолчанию: 0.                                                                                                                                                                                                          | int        | O   | P   |        |
| Community                                                | Имя сообщества.<br>**Примечание.** Используется для SNMPv1 и SNMPv2, если [CheckCommunity](#check-community) = 1.<br>По умолчанию: public.                                                                                                        | string     | O   | P   |        |
| <a name="snmp_engine_id">SNMP_EngineID</a>               | Идентификатор движка SNMP.<br>Правила генерации см. [ниже](#snmp_engine_id).<br>По умолчанию: protei.                                                                                                                                             | string     | O   | P   |        |
| <a name="context_engine_id">ContextEngineID</a>          | Идентификатор реализации контекста SNMP.<br>По умолчанию: значение [SNMP_EngineID](#snmp_engine_id).                                                                                                                                              | string     | O   | P   |        |
| ContextName                                              | Имя контекста SNMP.<br>По умолчанию: protei.                                                                                                                                                                                                      | string     | O   | P   |        |
| **<a name="agents_v3">Agents_v3</a>**                    | Перечень адресов для отправки трапов по протоколу SNMPv3.                                                                                                                                                                                         | \[object\] | O   | P   |        |
| addr                                                     | IP-адрес назначения. Формат:<br>`{ "<addr>";"<port>" }`.<br>По умолчанию: 0.0.0.0.                                                                                                                                                                | ip         | O   | P   |        |
| Port                                                     | Порт назначения.<br>Диапазон: 0-65535.                                                                                                                                                                                                            | int        | O   | P   |        |
| User                                                     | Имя пользователя.<br>По умолчанию: protei.                                                                                                                                                                                                        | string     | O   | P   |        |
| Protocol                                                 | Используемый транспортный протокол.<br>`TCP` / `UDP`.<br>По умолчанию: TCP.                                                                                                                                                                       | string     | O   | P   |        |
| **<a name="agents_v2c">Agents_v2c</a>**                  | Перечень адресов для отправки трапов по протоколу SNMPv2c.                                                                                                                                                                                        | \[object\] | O   | P   |        |
| Address                                                  | IP-адрес назначения.<br>По умолчанию: 0.0.0.0.                                                                                                                                                                                                    | ip         | O   | P   |        |
| Port                                                     | Порт назначения.<br>Диапазон: 0-65535.                                                                                                                                                                                                            | int        | O   | P   |        |
| User                                                     | Имя пользователя.<br>**Примечание.** Поле не используется, значение может быть любым.                                                                                                                                                             | string     | O   | P   |        |
| Protocol                                                 | Используемый транспортный протокол.<br>`TCP` / `UDP`.<br>По умолчанию: TCP.                                                                                                                                                                       | string     | O   | P   |        |
| Users                                                    | Перечень пользователей для протокола SNMPv3.                                                                                                                                                                                                      | \[object\] | O   | P   |        |
| Name                                                     | Имя пользователя.<br>**Примечание.** Имя должно совпадать с одним из значений в из [Agents_v3](#agents_v3).<br>По умолчанию: protei.                                                                                                              | string     | O   | P   |        |
| AuthProtocol                                             | Протокол авторизации.<br>**Примечание.** Допускается только значение none, авторизация не используется.<br>По умолчанию: none.                                                                                                                    | string     | O   | P   |        |
| PrivProtocol                                             | Протокол шифрования.<br>**Примечание.** Допускается только значение none, шифрование не используется.<br>По умолчанию: none.                                                                                                                      | string     | O   | P   |        |

#### Значения EngineIdFormat {#engine_id_format}

* 0 -- reserved, unused; зарезервированы;
* 1 -- IPv4 address (4 octets), lowest non-special IP address; IP-адрес формата IPv4, 4 октета, наименьший не особый IP-адрес;
* 2 -- IPv6 address (16 octets), lowest non-special IP address; IP-адрес формата IPv6, 16 октетов, наименьший не особый IP-адрес;
* 3 -- MAC address (6 octets), lowest IEEE MAC address, canonical order; MAC-адрес, 6 октетов, наименьший IEEE MAC-адрес;
* 4 -- Text, administratively assigned, maximum remaining length 27; текст, максимальный размер оставшейся части -- 27;
* 5 -- Octets, administratively assigned, maximum remaining length 27; октеты, максимальный размер оставшейся части -- 27;
* 6-127 -- reserved, unused; зарезервированы;
* 128-255 -- as defined by the enterprise, maximum remaining length 27; определяется организацией, максимальный размер оставшейся части -- 27.

#### Правила генерации snmpEngineID {#snmp_engine_id}

Значение идентификатора snmpEngineID зависит от значения [EngineIdConformance](#engine-id-conformance).

* при `EngineIdConformance = 0`:

`SNMP_EngineID = 80 00 + 51 89 + <EngineID>`

80 00 -- значение `EngineIdConformance = 1`;
51 89 -- значение `EngineIdEnterprise` (20873 в формате hex);

**Примечание.** Длина SNMP_EngineID корректируется строго до 12 байт.

* при `EngineIdConformance = 1`:

`SNMP_EngineID = 00 00 + 51 89 + 04 + <EngineID>`

00 00 -- значение `EngineIdConformance = 1`;
51 89 -- значение `EngineIdEnterprise` (20873 в формате hex);
04 -- значение `EngineIdFormat`.

### Форматы ###

* формат [\[Dynamic\]](#dynamic):

`{ <caVar>;<value>; };`

* формат [\[StandardMib\]](#standardmib):

`{ <addrSNMP>;<typeVar>;<value>; };`

* формат [\[AtePath2ObjName\]](#atepath2objname):

`{ <ctObject>;<caVar>; }`;

Для каждого типа необходимо прописать переменную CA(1) -- адрес объекта, иначе объекты в SNMP-дерево добавляться не будут.

* формат [\[SNMPTrap\]](#snmptrap):

`{ <ipManagerSNMP>;<portManagerSNMP>;<caObjectFilter>;<ctObjectFilter>;<caVarFilter>; };`

* формат [\[SpecificTrapCA_Object\]](#st_ca_object):

`{ <caVar>;<specificTrapOffset>; }`

* формат [\[SpecificTrapCT_Object\]](#st_ct_object):

`{ <ctObject>;<specificTrapBase>; }`

* формат [\[SpecificTrapCA_Var\]](#st_ca_var):

`{ <caObject>;<specificTrapOffset>; }`

* формат [\[FilterLevel\]](#filterlevel):

`{ <caObject>;<ctObject>;<caVar>;<nLevel> }`

* формат [Agents_v3](#agents_v3):

`{ <address>;<port>;<user>;<protocol> };`

* формат [Agents_v2c](#agents_v2c):

`{ <address>;<port>;<user>;<protocol> };`

### Правила вычисления идентификатора трапа (SpecificTrap) ###

1. Значение, заданное для `caObject` или `ctObject`, является базой (`base`) для формирования идентификатора.<br>
Если указан `caObject`, то `ctObject` игнорируется. Если подходящий `caObject` не найден, за основу берется `ctObject`.<br>
Если не найден ни `caObject`, ни `ctObject`, то `base = 0`.
2. Полученное значение умножается на 1000.
3. Значение, заданное для `caVar` и являющееся смещением (`offset`), прибавляется к базе.

**Примечание.** Идентификаторы для `caObject` и `ctObject` должны быть больше 0.

**Примечание.** Идентификаторы для `caVar` должны быть больше 100.

**Примечание.** Идентификаторы для `caVar` в диапазоне 1-100 зарезервированы.

| Переменная | Значение  | SP_Offset |
|:-----------|:----------|:----------|
| OSTATE     | ACTIVATE  | 1         |
| OSTATE     | FAIL      | 2         |
| ASTATE     | UNBLOCKED | 3         |
| ASTATE     | BLOCKED   | 4         |
| HSTATE     | ON        | 5         |
| HSTATE     | OFF       | 6         |

#### Пример ####

```ini
[General]
Root = PROTEI(1,3,6,1,4,1,20873)
ApplicationAddress = SS7FW
MaxConnectionCount = 10
ManagerThread = 1
CyclicTreeWalk = 0

[Dynamic]
[AtePath2ObjName]
{ SS7FW(146).General(1,1); CA(2); }
{ SS7FW(146).General(1,1); OSTATE(3); }
{ SS7FW(146).Traffic,Stat(2,1); CA(2); };
{ SS7FW(146).Traffic,Stat(2,1); PARAM(3); };
{ SS7FW(146).Abonent,Stat(3,1); CA(2); };
{ SS7FW(146).Abonent,Stat(3,1); PARAM(3); };
{ SS7FW(146).Reload(4,1); CA(2); };
{ SS7FW(146).Reload(4,1); OSTATE(3); };
{ SS7FW(146).Reload(4,1); PARAM(4); };

{ SS7FW(146).OVRLOAD,Handler,SL(5,1); CA(2); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Handler,SL(5,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); CA(2); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,Queue,Logic(6,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MINOVR(7,1); PARAM(4); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); CA(2); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); OSTATE(3); };
{ SS7FW(146).OVRLOAD,LICENSE,MAJOVR(8,1); PARAM(4); };

[SNMP]
ListenIP  =  0.0.0.0;
ListenPort  =  3128

[StandardMib]
#sysDescr
{1.3.6.1.2.1.1.1.0;STRING;"CallMe"; };
#sysObjectID
{1.3.6.1.2.1.1.2.0;OBJECT_ID;1.3.6.1.4.1.20873; };

[AtePath2Oid]
[SNMPTrap]
FirstVarOwn  =  0;
IndexID  =  1;

[Filter]
CA_Object = ".*"
CT_Object = ".*"
CA_Var = ".*"
TrapIndicator = -1
DynamicIndicator = -1

[SpecificTrapCT_Object]
{ SS7FW.General; 1; }
{ SS7FW.Traffic.Stat; 2; }
{ SS7FW.Abonent.Stat; 3; }
{ SS7FW.Reload; 4; }
{ SS7FW.OVRLOAD.*; 5; }

[SpecificTrapCA_Var]
{ Alarm.Decode.*; 111; }
{ Alarm.Encode.*; 121; }
{ Alarm.CDI.*; 131; }
{ Warn.ErrCodeInfo.ASP.Connect.*; 141; }
{ Warn.ASPUP.*; 151; }
{ Warn.ASPDN.*; 161; }
{ Info.DAVA.*; 171; }
{ Info.DUNA.*; 181; }
{ Info.SCON.*; 191; }
{ Info.DUPU.*; 211; }
{ Info.DRST.*; 221; }
{ Info.ASP.UP.*; 231; }
{ Info.ASP.Connect.*; 232; }
{ Alarm.DPC.*; 241; }
{ Alarm.UP.*; 251; }
{ Alarm.ChCfg.Invalid.*; 261; }
{ Warn.Act.*; 271; }
{ Warn.Deact.*; 281; }
{ Warn.Reg.*; 291; }
{ Warn.Dereg.*; 311; }
{ Warn.ASP.Failure.*; 321; }
{ Info.LinkUP.Num.*; 331; }
{ Info.ASInit.*; 341; }
{ Info.AS.ACT.*; 342; }
```

### Перегрузка параметров ###

Параметры конфигурации делятся на две группы:

1. Параметры AP_Agent. Формат перегрузки:

   ```bash
   $ ./reload ap_agent.di
   $ ./reload ap_agent.di "config/alarm/ap.cfg"
   ```

   Если параметры не указаны, путь к ap.cfg по умолчанию: `config/ap.cfg`.

2. Параметры AP_Manager. Формат перегрузки:

   ```bash
   $ ./reload ap_manager.di
   $ ./reload ap_manager.di "config/alarm/ap.cfg"
   ```

Перегрузить можно только приведенный ниже список параметров/секций:

1. AP_Agent:

   * [\[General\]](#general):
     * [ApplicationAddress](#application-address);
     * [MaxConnectionCount](#max-connection-count);
   * [\[Logs\]](#logs).

2. AP_Manager:

   * [\[General\]](#general):
     * [CyclicWalkTree](#cyclic-walk-tree);
   * [\[StandardMib\]](#standardmib);
   * [\[SNMPTrap\]](#snmptrap);
   * [\[SpecificTrapCA_Object\]](#st_ca_object);
   * [\[SpecificTrapCT_Object\]](#st_ct_object);
   * [\[SpecificTrapCA_Var\]](#st_ca_var).
