---
title : "История версий"
description : ""
weight : 1
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/history/_index.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_SS7FW"
gitlab_project_path: "MobileDevelop/Protei_SS7FW"
---
## 1.1.2.1.58 (2024-08-30)

- <details>
	<summary>
		Не поднимается GTPFW
	</summary>

	- Critical **Bug**

	**Symptom**: FW падает с core, при подаче трафика

	**Root Cause**: Использование некорректных данных о пакете при записи выходного GTP-C дампа

	**Solution**: Исправлена проблема с передачей некорректных данных

</details>

## 1.1.2.0.57 (2024-08-28)

- <details>
	<summary>
		Проблема формирования CDR
	</summary>

	- Basic **Bug**

	Исправлено формирование cdr для TCAP сообщений без компонентной части и сообщений, содержащих только CAP компоненту

</details>

- <details>
	<summary>
		CORE | Формирование ATI на GT_HLR
	</summary>

	- Basic **Task**

	**Configuration changes**: В секцию [General] ss7fw.cfg было добавлено строковое поле atiGt, использующееся для подставки в CdPN и HLR в рамках отправки ATI запроса.

	В случае отсуствия данного конфигурационного параметра, для подставки в CdPN и HLR используется CdPN из обрабатываемого сообщения

</details>

- <details>
	<summary>
		Анализ SCCP А и В на принадлежность HPLMN
	</summary>

	- Basic **UserStory**

	Добавлен анализ CgPA и CdPA на принадлежность домашнему диапазону HPLMN

</details>

- <details>
	<summary>
		Оптимизация SigFW | Nlohmann v2.0
	</summary>

	- Basic **UserStory**

	Оптимизация SigFW | Nlohmann v2.0

</details>

- <details>
	<summary>
		Перевод GTP_Lib на общий POD
	</summary>

	- Basic **UserStory**

	Зависимость gtp_coder теперь собирается с общим POD.

</details>

- <details>
	<summary>
		GTPFW CheckLocation
	</summary>

	- Basic **UserStory**

	Добавлена возможность обработки action "LocationCheck" для GTP-трафика

	**Configuration changes**: Добавлен новый параметр "gtHlr" в секцию [General] ss7fw.cfg, необходимый для включения в ATI-Request

</details>

- <details>
	<summary>
		GTP Tunnel ID Verification
	</summary>

	- Basic **UserStory**

	Добавлена проверка сообщений gtpv1 и gtpv2 по f-teid (проверяем по базе, где хранятся gtp сессии),

	в случае если проверка не пройдена, то silent drop с записью в CDR

	**Configuration changes**: Из gtp_forwarder.cfg в gtp.cfg конфиг перенесены параметры сессии,

	касающиеся GTP_C/GTP_U траффика (новая секция в gtp.cfg [GtpSession]).

	В секцию [General] добавлен флаг проверки соответствия GTP-C сообщений установленным сессиям и флаг сохранения GTP сессий в БД.

</details>

- <details>
	<summary>
		Не работает reload HTTP Persistant
	</summary>

	- Basic **Bug**

	**Symptom**: после релоада http.cfg с критичными изменениями, требующими пересоздания направлений, в направления не распределяются запросы

	**Root cause**: направления удаляются из компонентного дерева в компонентном Release, вызываемом GarbageCleaner (отложенное удаление)

	**Solution**: не вызывать Tm_Component::Release(), просто удалять объект

</details>

## 1.1.1.1.55 (2024-08-02)

- <details>
	<summary>
		Падение с core
	</summary>

	- Basic **Bug**

	**Symptom**: Падение FW после получения SG Response

	**Root Cause**: Считывание данных, для записи cdr производилось только из MAP части сообщения, случай ее отсутствия не был корректно обработан

	**Condition**: Отправка сообщения, содержащего только CAP часть -> Необходимость отправки ATI запроса по данному сообщению (настройка правил) -> Включение ATI cdr

	**Solution**: Обновлен алгоритм определения параметров для ATI cdr, теперь корректно обрабатывается случай отсутствия MAP части

</details>

## 1.1.1.0.53 (2024-07-11)

- <details>
	<summary>
		Update gtp_mi
	</summary>

	- Basic **UserStory**

	При определении профиля MI реализована следующая логика: если не задан параметр IpMasksGuest,

	то считаем что все сообщения не входящие в подсети IpMasksHome - гостевые. Не отправляем запрос на MI,

	если сообщение не попало ни в одну из подсетей.

	**Configuration changes**: В gtp_mi.cfg параметры Direction ID, IpMasksHome - mandatory; IpMasksGuest - optional

</details>

- <details>
	<summary>
		Доработка API с MI
	</summary>

	- Basic **UserStory**

	В Multi IMSI API добавлен уникальный ID запроса и убраны поля MNC, MCC. Изменена структура запроса и ответа.

	Endpoint для Multi IMSI изменен на /MultiIMSI/GtpProxy/GetSubscriptions.

	В Multi IMSI CDR добавлен уникальный ID запроса на Multi IMSI.

</details>

- <details>
	<summary>
		CORE | Оптимизировать работы mnc_long.json
	</summary>

	- Basic **UserStory**

	Исправлена работа с конфигурационным файлов mnc_long.json путем ускорения поиска по нему и решения проблемы с многопоточностью

</details>

- <details>
	<summary>
		Сборка Rule chain с Werror проекта
	</summary>

	- Basic **UserStory**

	Встраиваемая в данный проект библиотека, RuleChain, была собрана с различными флагами компилятора, с целью уменьшения количества потенциальных ошибок

</details>

- <details>
	<summary>
		Замена MCC и MNC в APN по long_mnc.json
	</summary>

	- Basic **UserStory**

	По IMSI который получаем из ответа от Multi IMSI и конфигу long_mnc.json определяем из скольки символов

	состоит PLMN (если нашли такой PLMN в конфиге, то 6 символов, иначе - 5),

	далее извлекаем PLMN из полученного IMSI, значения MCC и MNC полученные из PLMN

	используем для замены в APN.

</details>

- <details>
	<summary>
		Конфигурирование хранения TCAP транзакции в БД
	</summary>

	- Basic **UserStory**

	**Configuration changes**: В секцию [TCAP] конфигурационного файла ss7fw.cfg было добавлено перезагружаемое поле TcapTransactionMode, отвечающее за работу с TCAP-Транзакциями

</details>

- <details>
	<summary>
		CORE | Добавить проверку поля Diam_UserName при помощи сетевой зоны
	</summary>

	- Basic **Task**

	Добавлена проверка поля Diam_UserName при помощи сетевой зоны, на основании списка IMSI prefix

</details>

- <details>
	<summary>
		Hard | Проверка интерфейса с разрешенного типа сообщения для него
	</summary>

	- Basic **UserStory**

	**Configuration changes**: В секцию [GENERAL] конфигурационного файла gtp.cfg

	добавляем флаг MsgTypeInterfaceTypeCorrelation, отвечающий за включение проверки соответствия типа сообщения

	с типом интерфейса по FS.20

	Для gtp v1 проверяем, что msg type соответствует списку из FS.20: gn/gp, для gtp v2 проверяем,

	что msg type соответствует списку из FS.20: s5/s8; если нет, то не пропускаем сообщение и в CDR

	пишем причину блокировки: BC_GTP_MESSAGE_TYPE_NOT_ALLOWED

</details>

- <details>
	<summary>
		Некорректный декодинг значения CAP ServiceKey
	</summary>

	- Basic **Bug**

	**Symptom**: Некорректно декодируется значение CAP ServiceKey

	**Root Cause**: Пытаемся декодировать ServiceKey даже в тех сообщениях, в которых его не может быть

	**Solution**: Декодируем ServiceKey только у сообщений InitialDP, InitialDPSMS и InitialDPGPRS

</details>

- <details>
	<summary>
		Core| Diameter Error Code
	</summary>

	- Basic **Task**

	Добавлена отправка ErrorCode в ответе на DRA при срабатывании Action ReturnError

</details>

- <details>
	<summary>
		Падение при отсутствии IMSI в ответе MI
	</summary>

	- Basic **Bug**

	**Symptom**: Приложение падает в случае если в ответе от Multi IMSI в профиле не пришло поле IMSI

	**Root Cause**: Пытаемся вытащить параметры MCC, MNC из пустого IMSI.

	**Solution**: Добавлена проверка на случай, если не смогли вытащить IMSI из ответа от Multi IMSI

	Дополнительно скорректирована логика генерации ID в запросе на MI:

	Если не создан запрос на MI, то не генерируем ID и не записываем его в CDR.

</details>

- <details>
	<summary>
		Добавить сравнение поля DIAM_VPLMNid_MCCMNC с полем PLMN в network area
	</summary>

	- Basic **Bug**

	**Symptom**: Отсутствует возможность проверки поля DIAM_VPLMNid_MCCMNC при помощи сетевой зоны

	**Root Cause**: Данное поле не было добавлено в список разрешенных для проверки полей при помощи сетевой зоны

	**Solution**: DIAM_VPLMNid_MCCMNC было добавлено в список разрешенных для проверки полей при помощи сетевой зоны

</details>

- <details>
	<summary>
		Сборка SigFW проекта с Werror
	</summary>

	- Basic **UserStory**

	Исправлены ошибки, возникающие в результате компиляции с использованием флагов, указанных в задаче Mobile_SigFW-528.

</details>

- <details>
	<summary>
		Не применяются новые параметры http.cfg после reload
	</summary>

	- Basic **Bug**

	**Symptom**: при выполнении reload http конфигурации новые параметры в AdditionDir секции не применялись

	**Root cause**: Connection Manager не выполнял обновление данных, если перезагружаемое направление уже было в него добавлено

	**Solution**: метод, отвечающий за добавление информации о направлении в Connection Manager, будет выполнять перезагрузку параметров направления, если информация по направлению уже внесена Connection Manager

	*/

</details>

## 1.1.0.31.46 (2024-06-03)

- <details>
	<summary>
		Оптимизация SigFW | Декодирование SS7/Diam запросов.
	</summary>

	- Basic **UserStory**

	Фактический декодинг сообщения теперь выполняется в логиках

	для обработки конкретных типов сообщений (TCAP_CheckSL, DIAM_CheckSL).

	Из проекта удалены Decoder SL и CoderManager.

	При перегрузке по логикам отвечаем на STP: 503_Service_Unavailable, при этом сообщение не пишется в cdr.

	Убрана статистика по CoderSL (Total CoderSL, Allocated CoderSL).

</details>

- <details>
	<summary>
		Дублирование в profile.log
	</summary>

	- Basic **Bug**

	**Symptom**: Дублирование системной информации в profile.log

	**Root Cause**: Повторная запись информации из профилировщиков

	**Condition**: Любой запуск системы

	**Solution**: Удален повторный вызов запись профилировщика

</details>

- <details>
	<summary>
		В CreateSessionResponse приходит RAT Type 0
	</summary>

	- Basic **Bug**

	**Symptom**: Судя по логам поле RAT type заполнялось, даже тогда, когда отсутствовало в сообщении

	**Root Cause**: Данное поле имело значение по умолчанию 0, что в свою очередь является одним из корректных значений

	**Condition**: Отправка сообщения, не содержащего RAT Type и последующий анализ данного поля

	**Solution**: Тип переменной, хранящей в себе значение RAT Type, был изменен и теперь значение по умолчанию отсутствует

</details>

- <details>
	<summary>
		Подмена VLAN
	</summary>

	- Basic **Bug**

	**Symptom**: Не происходит подмена VlanId в тестовой схеме с SRIOV

	**Root Cause**: DPDK обрезает VLAN header перед тем как отдать пакет на обработку. GTPFW не зная о наличии VLAN не выполняет подмену

	**Solution**: Восстанавалием VLAN header перед началом обработки пакета

</details>

- <details>
	<summary>
		Добавить поддержку драйвера iavf
	</summary>

	- Basic **UserStory**

	Добавлена поддержка драйвера iavf

</details>

- <details>
	<summary>
		Оптимизация SigFW | Распределение Redis connection по потокам.
	</summary>

	- Basic **UserStory**

	Каждый Redis connection привязан к одному из потоков приложения.

	Убрана блокирующая синхронизация при получении коннекции для обращения к Redis.

	**Configuration changes**: Из redis.cfg удален параметр BusyConnectionTimeout.

	Значение ConnectionTimeout по умолчанию уменьшено до 5 секунд

</details>

- <details>
	<summary>
		Оптимизация SigFW | Nlohmann
	</summary>

	- Basic **UserStory**

	Реализован переход на RapidJSON для парсинга запросов по протоколу ss7 (TCAP) и diameter,

	добавлен вывод ошибок при некорректном JSON в теле запроса.

	Формирование HTTP ответов (ss7, diam) на STP теперь осуществляетcя с использованием RapidJson.

	Убрана полная конвертация из PDU в JSON, т.к. ранее использовалась только для вывода трейсов.

</details>

- <details>
	<summary>
		В GTPFW не работает правило фильтрации по IP
	</summary>

	- Basic **Bug**

	**Symptom**: FW блокирует пакеты по правилу проверки IP адреса, хотя не должен

	**Root Cause**: Тип проверяемого значения в случае проверки IP адресов с Web приходил String, а приложение ожидало IP

	**Solution**: Добавлена обработка IP адресов при проверке в правиле полей IP_Src/IP_Dst и типом String

</details>

- <details>
	<summary>
		CORE | Некорректный результат выполнения правил
	</summary>

	- Basic **Bug**

	**Symptom**: Пакет, значения которого не подходят под правило, пропускается в случае настройки одного ключа в правиле

	**Root Cause**: Из-за некорректной внутренней логики, все правила состоящие из одного ключа с **Condition** true всегда завершались успешно

	**Solution**: Теперь **Condition** первого ключа в каждом правиле игнорируется

</details>

- <details>
	<summary>
		Ошибка при декодинге CAP initialDP с ACN = 0.4.0.0.1.21.3.4
	</summary>

	- Basic **Bug**

	**Symptom**: Пакет, значение ACN которого является корректным для его OpCode, блокируется на FW

	**Root Cause**: Список для проверки соответствия между ACN и OpCode не был актуализирован

	**Solution**: ACN был добавлен в список для проверки

</details>

- <details>
	<summary>
		Оптимизация SigFW | Velocity/LocationCheck
	</summary>

	- Basic **UserStory**

	Значительно уменьшено копирование данных при переходе к VC/LC алгоритмам.

	Создан общий интерфейс взаимодйствия с каждым из блокирующих алгоритмов.

	Отправка ATI/SRI4SM запроса тепреь происходит в самой логике, обрабатывающей сообщение

</details>

- <details>
	<summary>
		Получаем инкапсулированный трафик GTP из GTP-U с UPF
	</summary>

	- Basic **UserStory**

	На основе параметров, установленных в upf.cfg, происходит создание и считывание информации из UDP сокетов.

	Контроль событий, происходящих на сокетах осуществляется при помощи epoll.

	Добавлены новые причины блокировок:

	Блокировка gtp c пакета, ip адрес которого не входит в список разрешенных

	Блокировка gtp c пакета, из-за превышения установленной пропускной способности по его ip адресу

</details>

- <details>
	<summary>
		CAP | Проверка ACN
	</summary>

	- Basic **UserStory**

	Актуализирован список сопоставления OpCode и ACN для CAP сообщений

</details>

- <details>
	<summary>
		Не выделять отдельный поток для создания и отправки примитивов в GTP_C_SL
	</summary>

	- Basic **UserStory**

	Формирование примитивов с GTP-C сообщениями происходит в тех же потоках, что и выполнение логики SL

	**Configuration Changes**: Из gtp.cfg удален параметр RetrieverCpuId

</details>

- <details>
	<summary>
		Не релодится интервал статистики
	</summary>

	- Basic **Bug**

	**Symptom**: Не релоадится интервал статистики

	**Root Cause**: Значение интервала перезапуска запоминалось лишь однажды - при инициализации

	**Solution**: При обновлении конфига осуществляется перезапуск таймера статистики с новым интервалом

</details>

- <details>
	<summary>
		Оптимизация SigFW | Tm_CDR_Writer
	</summary>

	- Basic **UserStory**

	Для форматирования данных (TCAP, DIAM, GTP, PacketStatistics) которые затем пишутся в CDR,

	теперь используется библиотека fmt. Добавлены бенчмарки записи CDR для случая со стримами (текущее решение) и

	для случая с использованием библиотеки fmt.

</details>

- <details>
	<summary>
		Добавить long_mnc.json
	</summary>

	- Basic **UserStory**

	**Configuration changes**: Был добавлен новый конфигурационный файл содержащий информацию о наборе пар mcc/mnc

</details>

- <details>
	<summary>
		Некорректно декодируется значение MNC из DIAM адресов
	</summary>

	- Basic **Bug**

	**Symptom**: Некорректно декодируется значение MNC из DIAM адресов

	**Root Cause**: При считывании MNC из DIAM адресов не учитывалось то, что его размер может быть больше 2-ух цифр

	**Solution**: Добавлена обработка обоих случаев (с 2-ух и 3-ех значным значением) MNC при помощи ранее добавленного конфига long_mnc.json

</details>

- <details>
	<summary>
		CORE | Отбой сообщения с отправкой UDTS
	</summary>

	- Basic **Task**

	Добавлен Action ReturnUDTS с параметром ReturnCause. При срабатывании в ответ добавляется ReturnUdtsCause

</details>

- <details>
	<summary>
		Поправить DEST_REG на DEST_REF
	</summary>

	- Basic **Bug**

	**Symptom**: Некорректно декодируется DestinationReference

	**Root Cause**: DestinationReference содержит первый байт (extension + TON + NP) и идентификатор абонента (MSISDN/IMSI).

	При декодировании IMSI не учитывали наличие служебного первого байта.

	**Solution**: При декодировании DestinationReference пропускаем первый байт

</details>

- <details>
	<summary>
		Multi IMSI. Подмена параметров MCC, MNC
	</summary>

	- Basic **UserStory**

	Реализована подмена полей MCC и MNC в APN IE для сообщений GTP версии 1

</details>

- <details>
	<summary>
		Пропуск сообщений при недоступности Redis для Диаметра.
	</summary>

	- Basic **UserStory**

	Добавлен переходный режим для Diameter, который включается при недоступности Redis и выключаетяс по таймеру.

	В diam_cdr добавлен индикатор переходного режима.

	**Configuration changes**: В секцию [DIAM] ss7fw.cfg добавлен параметр SwitchingTimeDIAM

</details>

- <details>
	<summary>
		Не работает reload секции [RulesChainConfig]
	</summary>

	- Basic **Bug**

	**Configuration changes**: В секции [RulesChainConfig] ss7fw.cfg параметры OutDir, InDir, Port, IP и ID читаются только при UseLocalConfig = 0 и сделаны обязательными в этом случае

</details>

- <details>
	<summary>
		SigFW падает в Core
	</summary>

	- Basic **Bug**

	**Symptom**: Приложение падает при быстрой подаче трафика на старте работы.

	**Root Cause**: Трафик начинается обрабатываться до подписки на WEB и инициализации цепочки правил. Происходит обращение к несуществующим правилам.

	**Solution**: Если цепочка правил не проинициализирована, то на HTTP трафик отвечаем 503, а gtp трафик пропускаем без проверок.

</details>

- <details>
	<summary>
		Неправильно пишутся DIAM cdr
	</summary>

	- Basic **Bug**

	Исправлена запись DIAM cdr и GTP-C cdr

</details>

- <details>
	<summary>
		Блокируем DIAM Answer
	</summary>

	- Important **Bug**

	Оптимизировано взаимодействие с Redis, если не требуется проверять Diameter ответы

	**Configuration changes**: В секции [DIAM] ss7fw.cfg параметр DiamWaitAns заменен на DiamAnsMode

</details>

## 1.1.0.30.28 (2024-04-16)


 Режим работы с UPF по GTP
- Basic **UserStory**

**Configuration Changes**: Добавлен новый конфигурационный файл - upf.cfg

- В gtp.cfg в секцию [General] добавлен параметр Mode

- Из gtp_forwarder.cfg в gtp.cfg перенесены - параметр [General] CheckWhiteListIpMasks и секция [GTP_C_Limits]


 Некорректная передача фрагментированных пакетов
- Critical **Bug**

- Пропуск всех фрагментированных пакетов без анализа


 Оптимизация SigFW | Создание потоков
- Basic **UserStory**


 Оптимизация SigFW | Распределение SL по потокам.
- Basic **UserStory**

- В SLM и CoderManager теперь создается общий пул обработчиков SL без привязки к thread id.

- При отправке примитива в логику убран расчет hash для TCAP и DIAM сообщений,

- который требовался ранее для определение номера потока, по которому осуществлялся выбор свободной SL.


 CORE | Некорректный приоритет выбора IMSI
- Basic **Bug**

**Symptom**: Некорректный выбор IMSI в сообщениях ForwardSM

**Root Cause**: Осуществлялось ошибочное заполнение поля IMSI при анализе параметров SM-RP-OA/DA

- Condition: Отправка сообщения ForwardSM и дальнейшая проверка поля IMSI, например, через RuleChain

**Solution**: Исправлено ошибочное заполнение поля IMSI при обработке сообщений типа ForwardSM



## 1.1.0.27.23 (2024-03-01)


 Multi IMSI. Запись в CDR
- Basic **UserStory**

- Реализован отдельный CDR (SigFW_gtp_mi_cdr), куда будут писаться CDR, обработанные через Multi IMSI.

- Расширен справочник возможных cause в случае работы с компонентой Multi IMSI.


 Core | Error Indication GTP-U
- Basic **UserStory**

- Добавлена отправка ErrorIndication в ответ на заблокированные GTP-U сообщения.

- Подмена MAC и VLAN выполняется непосредственно перед отправкой сообщения в сеть.


 Белый список IP адресов
- Basic **UserStory**

**Configuration changes**: в секцию General gtp_forwarder.cfg добавлено новое поле CheckWhiteListIpMasks

- Данное поле осущетслвяет фильтрацию GTP-C/U трафика на основе списка подсетей указанных в поле Directions


 RuleChain. Доработку loop-check
- Basic **UserStory**

- Проверка наличия циклов в router.json производится при инициализации, а не во время проверки правил


 GTPFW warning Unknown protocol ss7
- Basic **Bug**

**Symptom**: GTP-C сообщения с неподдерживаемым типом блокируются

**Root cause**: При получении неподдерживаемого GTP-C сообщения пытаемся обработать его так же, как и успешно декодирванное,

- однако данные для цепочки правил не заполняем и они остаются со значениями по умолчанию.

- Данные не проходят проверку в RuleChain и сообщение блокируется

**Solution**: Сразу форвардим сообщение с неизвестным типом без попыток его как-либо обработать



## 1.1.0.26 (2024-02-20)


 Core | Сохранение GTP сессий в Redis
- Basic **UserStory**

- Добавлено сохранение GTP сессий в Redis. Добавлено получение сессий из Redis в случае её отсутствия в локальном хранилище.

**Configuration changes**: В gtp_forwarder.cfg в секцию [GtpSession] добавлен параметр SessionDenyTTL

**DB changes**: Добавлена запись GTP сессий по ключам с префиком gtp_sesson:



## 1.1.0.25 (2024-02-20)


 US | Использовать отдельное ядро процессора на каждый forwarder
- Basic **UserStory**

- Реализована возможность использование ядра процессора только под forwarder.

- Больше не используется sleep при обработке пакетов в forwarder

**Configuration changes**: В gtp_forwarder.cfg в NIC добавлен параметр CpuIds - ядра процессора используемые для forwarder'ов этого интерфеса



## 1.1.0.24 (2024-02-20)


 Не обновляются параметры SRI4SM в базе
- Basic **Bug**

**Symptom**: При сохранении параметров SRI4SM в базе не обновляются данные с уже существующим ключом

**Root cause**: При обновлении SRI4SM данных был выставлен флаг NX = true, что означает выполнять обновление данных, только

- если такого ключа не существует.

**Solution**: Выставить флаг NX = false при обновлении данных



## 1.1.0.23 (2024-02-20)


 В базе TCAP транзакций не сохраняется неизвестное значение OpCode
- Basic **Bug**

**Symptom**: Некорректный парсинг информации о TCAP транзакции из БД

- Root Cause: Ожидалось, что OpCode всегда является положительным числом, однако его значении может быть равно -1

**Condition**: Отправка пустого tcap.begin (без map) тела -> отправка tcap.continue для данной транзакции

**Solution**: Исправлен тип ожидаемого значения для ряда полей с беззнакового числа на знаковое



## 1.1.0.22 (2024-02-20)


 Прозрачный режим работы для GTPFW
- Basic **UserStory**

- Реализован пропуск Diameter и GTP сообщений при включенном пассивном режиме работы FW независимо от результатов проверок



## 1.1.0.21 (2024-02-20)


 Multi IMSI. Конфигурация
- Basic **UserStory**

- Реализована возможность конфигурирования Multi IMSI GTP FW.

- Также добавлена возможность определения требуемого профиля от Multi Imsi

- в зависимости от попадания в домашнюю или гостевую сеть.

**Configuration changes**: В конфигурации можно задавать http-направление для Multi IMSI,

- а также маски хостов домашней и гостевой сети.

- Добавлен конфиг gtp_mi.cfg c секцией [General], содержащей параметры DirectionID,

- IpMasksHome, IpMasksGuest.



## 1.1.0.20 (2024-02-20)


 Multi IMSI. Подмена IMSI, MSISDN, APN по запросу к MI
- Basic **UserStory**

- Реализован запрос и парсинг ответа от Multi IMSI

- Добавлена подмена параметров MCC и MNC в APN для GTPv2



## 1.1.0.19 (2024-02-20)


 Добавить заброс на получение SRI4SM параметров из базы
- Basic **Freq**

- Добавлена обработка endpoint для получения кэшированной информации из SRI4SM для проверок в MT_SMS



## 1.1.0.18 (2024-02-20)


 Core | Обработка GTP-U трафика
- Basic **Task**

- Добавлено хранение и обновление GTP сессий.

- Добавлена проверка корреляции GTP-U пакетов с установленными сессиями.

**Configuration changes**: В gtp_forwarder.cfg добавлена секция [GtpSession] с параметрами GtpUSessionValidationEnable,

-                        SessionStorageSize, SessionExpireTime, SessionCleanupInterval



## 1.1.0.17 (2024-02-20)


 Изменение способа хранения  и записи информации в БД
- Basic **Task**

**DB changes**: Из записи по абоненту удалено поле 'Imsi';

- Сохранение записей реализовано при помощи LUA скрипта, для предотвращения дальнейших проблем с репликацией;

- Изменен тип значения записей по транзакциям с String (разделенной пайпами) на JSON



## 1.1.0.16 (2024-02-20)


 US CORE | Раздельные таймера на MAP и CAP транзакции
- Basic **UserStory**, Заказчик: **ООО “Тинькофф Мобайл“**

- Добавлена возможность раздельного указания времени жизни MAP и CAP транзакций

**Configuration changes**: В ss7fw.cfg в секцию [TCAP] добавлены парамтеры TransactionLifetimeMAP и TransactionLifetimeCAP



## 1.1.0.15 (2024-02-20)


 Контроль длины сообщения GTP-C
- Basic **UserStory**

**Configuration changes**: В gtp_forwarder.cfg секцию [GTP_C_Limits] добавлены поля MaxLength и MinLength

- Данные поля используются для контроля размер входящего GTP-сообщения



## 1.1.0.14 (2024-02-20)


 Анализ SMS PID
- Basic **Task**

- В FW добавлена обработка SMS_PID и возможность его проверки через RuleChain



## 1.1.0.13 (2024-02-20)


 US | Кеширование информации из SRI4SM для проверок в MT SMS
- Basic **Task**

- Реализована возможность проверки предшествующего успешного SRI4SM при проверке MT SMS.

**Configuration changes**: В ss7fw.cfg в секцию [TCAP] добавлены параметры SRI4SM_DB_SaveMAP и SRI4SM_DB_StorageTimeMAP,

- где SRI4SM_DB_SaveMAP - флаг кеширования SRI4SM транзакций в базу, SRI4SM_DB_StorageTimeMAP - время хранения в базе.

**DB changes**: Добавлены запись SRI4SM параметров (SMSC, MSISDN, IMSI, MSC) из сообщения в базу и

- чтение этих данных из базы, также для сохранение параметров SRI4SM из begin, в TransactionInfo

- добавлено поле SRI4SM_SMSC.



## 1.1.0.12 (2024-02-20)


 Core | Анализ параметров уровня SMS
- Basic **Task**

- Добавлена обработка полей SMS_OA, SMS_OA_TON, SMS_OA_NPI, SMS_DA, SMS_DA_TON, SMS_DA_NP



## 1.1.0.11 (2024-02-20)


 Выводить номер SL в log
- Basic **UserStory**

- В вывод логов добавлены номера SL



## 1.1.0.10 (2024-02-20)


 Пропуск сообщений с unsupported type для GTPv1 и GTPv2
- Basic **UserStory**

**Symptom**: Через FW не проходит сообщение GTPv1 RAN Information Relay(70)

**Root cause**: Не пропускали сообщения с типами декодинг которых не поддерживается библиотекой

**Solution**: Теперь будем пропускать сообщения через FW в случае если смогли задекодить заголовок и даже если

- сообщение попало в список неподдерживаемых библиотекой (версия 1, версия 2); в таком случае декодинг тела не выполняется

- Дополнительно: Передача следующих параметров GTP сообщения (версии, типа сообщения, src/dst ip, port)

- для записи в GTP CDR теперь реализована не через структуры RuleChain



## 1.1.0.9 (2024-02-20)


 Не определяется сетевая зона для хоста из ATI при AgeOfLocation = 0
- Basic **Bug**

- Теперь расчет информации о местонахождении для хоста из ATI рассчитывается сразу после его получения.



## 1.1.0.8 (2024-02-20)


 Корректировка cdr SS7
- Basic **UserStory**

- Журнал SS7FW_cdr, содержащий информацию о входящих сообщениях по протоколам SS7, был возвращен.



## 1.1.0.7 (2024-02-20)


 Обновлять данные регистрации только после завершения DIAM транзакции
- Basic **Bug**

- Обновление DIAM регистрации только после получения ответа



## 1.1.0.6 (2024-02-20)


 US | Один CDR GTP-C
- Basic **UserStory**

- Добавлены следующие журналы:

-  - SigFW_gtp-с_cdr, содержащий информацию о GTP_C сообщениях

-  - SigFW_gtp-с_invalid , содержащий сообщение в raw формате и позицию где была обнаружена проблема при декодинге



## 1.1.0.5 (2024-02-20)


 US | Разделить CDR MAP и CAP
- Basic **UserStory**

- Добавлены следующие журналы:

- - SigFW_cdr_map, содержащий информацию о MAP сообщениях

- - SigFW_cdr_cap, содержащий информацию о CAP сообщениях

- - SigFW_tcap_invalid, содержащий информацию о некорректных TCAP сообщениях, не дошедших до логик



## 1.1.0.4 (2024-02-20)


 Реализовать подмену MAC, VLAN на стороне GTPFW
- Basic **Freq**

- Подмена dst MAC на указанный в конфигурации (src MAC не изменяется)



## 1.1.0.3 (2024-02-20)


 Реализовать подмену MAC, VLAN на стороне GTPFW
- Basic **Freq**

- Добавлена возможность подмены source MAC и VlanId

- Добавлен безусловный проброс ARP, ICMP и ICMPv6 пакетов

- Исправлена фрагментация/дефрагментация и проверка пропускной способности для пакетов, содержащих VLAN

**Configuration changes**: В gtp_forwarder.cfg добавлены

-                        - в секцию [General] UdpPortsToForward и TcpPortsToForward

-                        - в секцию [NICs] MAC и VlanId



## 1.1.0.2 (2024-02-20)


 Обрабатывать значение PLMN из ответа ATI response
- Basic **Task**

- Добавлена обработка поля PLMN из ATI response.

- Выбор сетевой зоны для ATI response теперь определяется по следующей логике:

- Если PLMN был в ответе, то для поиска сетевой зоны используется он, иначе комбинация MCC + MNC



## 1.1.0.1 (2024-02-20)


 US | Ограничение обработки GTP-С пакетов (DDOS)
- Basic **UserStory**

- Добавлено ограничение количества обрабатываемых в секунду GTP-C пакетов.

- Добавлено ограничение размера очереди примитивов в GTP-C логику.

**Configuration changes**: В gtp_forwarder.cfg добавлена секция [GTP_C_Limits]



## 1.1.0.0 (2024-02-20)


 | Хранение данных по IMSI абонента в json
- Basic **UserStory**

- Изменен формат хранения данных в БД, теперь для записи информации используется JSON-структура

- В соотвествии с данным изменением была изменена логика работы основных SL

- Добавлено корректное обновление данных по регистрации на VLR в соотвествии с данными из ATI

- Удалены упоминания MariaDB в коде, mariadb.cfg удален из обработки

- Из ss7fw.cfg убрано поле DatabaseType из-за отсутствия необходимости в нем



## 1.0.15.9 (2024-02-20)


 Запись tcpdump по DPDK интерфейсам
- Basic **UserStory**

- Добавлена запись дампа по проходящим через gtp_forwarder пакетам

**Configuration changes**: В gtp_forwarder.cfg добавлена секция [DumpWriter]



## 1.0.15.8 (2024-02-20)


 US | GTP-C IP Fragmentation
- Basic **UserStory**

- Добавлена фрагментация и дефрагментация IPv4 и IPv6 пакетов

**Configuration changes**: В gtp_forwarder.cfg добавлены [General] MTU; [IpFragmentation] FragmentTTL, MaxFragments и MaxFragmentedPackets



## 1.0.15.7 (2024-02-20)


 Спам в трейсах после успешного DIAM VC
- Basic **Bug**

**Symptom**: На FW приходит запрос diam ULR, далее после успешного vc_status_ok(0)

- наблюдаются повторы в трейсах (повторно заходим в логику Velocity Check)

**Root cause**: В RuleChain не обновлялся RuleResult после обработки всех правил (оставался в состоянии 13 (Velocity check))

**Solution**: На стороне RuleChain очищаем RuleResult

- Дополнительно:

**Symptom**: При запуске тестов Velocity Check по трейсам заходим в Location Check логику

- Root Cause: Флаг Location check изначально содержит "мусор"

**Solution**: Обнуляем флаг Location Check



## 1.0.15.6 (2024-02-20)


 Падение после получения ошибки в ATI response
- Basic **Bug**

**Symptom**: Падение FW ATI Response при ATI response с error code

**Root cause**: При получении ошибки от ATI, код ошибки записывался только в PDU для TCAP

**Solution**: Проверяем какой из PDU не пустой (в зависимости от того какой протокол TCAP или DIAM)

- и записываем текущий код ошибки

- Дополнительно:

**Root cause** : HT проверялся до внесения в него значений

**Solution**: Заполняем HT значением перед проверкой

**Root cause** : ATI_VLR не заполнялся для передачи в RuleChain

**Solution**: Заполняем ATI_VLR в RuleData в DIAM



## 1.0.15.5 (2024-02-20)


 Core | Multi IMSI. Подмена  GTP-C v1 и v2 трафике IMSI, MSISDN, APN
- Basic **UserStory**

- Добавлен метод для подмены параметров IMSI, MSISDN, APN в GTP_С сообщениях V1 и V2

- Поддержка изменения размера GTP пакета при изменении размеров параметров



## 1.0.15.4 (2024-02-20)


 US CORE | Update dt LA
- Basic **UserStory**

- Добавлено обновление времени последней активности абонента на основе HT и GT для определенных MAP и CAP сообщений



## 1.0.15.3 (2024-02-20)


 Core | LOCATION_CHECK
- Basic **Task**

- Добавлен алгоритм Location Check

- Убрана возможность отправки нескольких ATI запросов для одного и того же сообщения

- Немного модифицирован алгоритм Velocity Check



## 1.0.15.2 (2024-02-20)


 Core | Проверка времени последней активности Last Activity
- Basic **Task**

- Добавлено сохранение времени последней активности в отдельную таблицу Redis

- Сформированы объединенные запросы в Redis для регистраций и Last Activity

- Сохранение последней активности в Redis согласно MAP opcodes



## 1.0.15.1 (2024-02-20)


 Добавить Msg_Id в SS7FW_trace
- Basic **Freq**, Заказчик: **ООО “Тинькофф Мобайл“**

- Расширено логирование в правилах по обработке сообщений до цепочки.

- TCAP: добавлены параметры Message ID, OTID и DTID

- DIAM: добавлены параметры End2End, HopByHop и OpCode

- GTP-C: добавлена информация о destination teid



## 1.0.15.0 (2024-02-20)


 Форматирование кода проекта Protei_SigFW
- Basic **UserStory**

- Добавлен clang-format файл с информацией о форматировании, используемом в проекте

- Отформатирован каждый файл проекта на основании файла, описанного выше

- Добавлен скрипт, позволяющий отформатировать каждый файл проекта, за исключением тех, что указаны в .formatignore



## 1.0.14.19 (2024-02-20)


 US | Расширить GTP CDR
- Basic **UserStory**

- В GTP cdr были добавлены следующие поля: IMSI, MSISDN, APN, PLMN, srcIP c UDP, dstIP с UDP



## 1.0.14.18 (2024-02-20)


 Заблокировали Camel CIR и Continue
- Basic **Bug**, Заказчик: **ООО “Тинькофф Мобайл“**

**Symptom**: Заблокировали CAP сообщения с кодом BC_UNKNOWN_OPCODE_AC

**Root cause**: Используется неполная таблица соответствия OpCode и ACN для CAP

**Solution**: Дополнена таблица соответствия CAP OpCode и ACN



## 1.0.14.17 (2024-02-20)


 Core | Current network area - кроспротокольные проверки через NA
- Basic **Task**

- Добавлено новое правило: "принадлежит ли выбранное поле текущей сетевой зоне абонента"

- Добавлено новое действие: "запросить информацию о текущей сетевой зоне абонента для последующего использования"

- Добавлена обработка случая возникновения нескольких блокирующих действий в процессе проверки сообщения



## 1.0.14.16 (2024-02-20)


 Резервное подключение к БД
- Basic **Freq**, Заказчик: **ООО “Тинькофф Мобайл“**

- Реализован приоритетный список подключений к Redis

**Configuration changes**: redis.cfg - Секция [Connection] заменена на [Connections], в которой указывается список подключений

-                                  - К подключениям добавлены приоритеты

-                                  - К подключениям добавлены названия для логирования



## 1.0.14.15 (2024-02-20)


 Падение при отсутствии diam словаря
- Basic **Bug**

- Добавлена обработка ошибки при чтении diameter словаря



## 1.0.14.14 (2024-02-20)


 US CORE | RuleChain | getDBInfo
- Basic **UserStory**

- Добавлена возможность в конфигурации системы устанавливать флаг необходимости запроса в БД до RuleChain,

- чтобы можно было использовать полученные данные в правилах Key, без дополнительных Action в Rule



## 1.0.14.13 (2024-02-20)


 Core | универсальный velocity check
- Basic **UserStory**



## 1.0.14.12 (2024-02-20)


 CORE | Падает GTPFW при старте
- Basic **Bug**

**Symptom**: Падение GTPFW при старте

**Root cause**: При выставлении названия потока больше 19 символов переполняется локальный буфер

**Solution**: Изменено название потока GTP_PacketsRetrieverThread



## 1.0.14.11 (2024-02-20)


 US CORE | Реализовать запись CDR+LOG GTP трафика
- Basic **UserStory**

- Добавлены журналы для GTP трафика 1-ой и 2-ой версии.

- Добавлены отдельный файл логирования для сообщений, связанных с обработкой GTP сообщений



## 1.0.14.10 (2024-02-20)


 US | Реализация алгоритма проверки Sequence-Number для GTPv2 и GTPv1
- Basic **UserStory**

- Добавлена валидация ответов по sequence number (в рамках транзакции)



## 1.0.14.9 (2024-02-20)


 Аутентификация в Redis
- Basic **Freq**

- Добавлена возможность указать Username и Password для подключения к Redis



## 1.0.14.8 (2024-02-20)


 - Перенос сетевых зон
- Basic **Task**

- Изменен способ хранения списка сетевых зон, теперь он хранится в отдельной сущности,

- предоставляющей методы по взаимодействую с ним


 - Местоположения 2.0
- Basic **Task**

- Считывание информации о местоположениях теперь происходит из WEB.

- Список GT теперь не привязан к конкретному местоположению,

- связующим звеном между метоположением и списком GT выступает соответствующая сетевая зона.



## 1.0.14.7 (2024-02-20)


 Проверить сборку FW на Ubuntu
- Basic **Task**

**Symptom**: Падение при декодировании MAP запроса

**Root cause**: В случае декодирования MAP запроса с двухзначным AC происходит выход за границы строкового буфера

**Solution**: Исправлено преобразование AC в строку



## 1.0.14.6 (2024-02-20)


 Сборка с prometheus
- Basic **Freq**, Заказчик: **ООО “Тинькофф Мобайл“**

- Безусловная сборка с prometheus, проверка использования prometheus через config


 CORE | Взаимодействие RSU - FW
- Basic **Task**

- Добавлен SwitchingMode для пропуска хвоста TCAP транзакций в переходный период



## 1.0.14.5 (2024-02-20)


 Core | Хранить в БД imsi + msisdn
- Basic **Task**

- Изменена схема хранения регистраций по msisdn



## 1.0.14.4 (2024-02-20)


 US | Обработка UDP пакетов на ATE
- Basic **UserStory**

- Добавлена обработка по правилам GTP-C пакетов, полученных от gtp_forwarder



## 1.0.14.3 (2024-02-20)


 CORE | Rule Field - Network (сравнение)
- Basic **Task**

- Добавление правила: "Существует ли такая сетевая зона, что два выбранных поля из сообщения лежат в ней"



## 1.0.14.2 (2024-02-20)


 Core | Rule Field - Network (поле)
- Basic **Task**

- Добавление правила: "Находится ли выбранное поле сообщения в выбранной сетевой зоне"



## 1.0.14.1 (2024-02-20)


 CORE | RouterProtocol GTP + RouterGTP
- Basic **Task**

- Изменение формата Json сообщения, представляющего конфигурации RouterGTP



## 1.0.14.0 (2024-02-20)


 US | Собрать обработчик GTP пакетов для обработки UDP
- Basic **UserStory**

- Добавлена обработка трафика на основе DPDK



## 1.0.13.5 (2024-02-20)


 US | Filtering rule GTP - extantion 1
- Basic **UserStory**

- Добавлена обработка траффика GTPv1 и новые параметры фильтрации GTP сообщений



## 1.0.13.4 (2024-02-20)


 US | NUMBER LISTS - Version
- Basic **UserStory**

- Добавлена возможность обновления Number Lists по запросу от системы



## 1.0.13.3 (2024-02-20)


 US | Изменение обработки MsgId от STP
- Basic **UserStory**



## 1.0.13.2.6 (2024-02-20)


  US | Недоступность Redis
- Basic **UserStory**

- При недоступности Redis трафик должен проходить прозрачно.



## 1.0.13.1.5 (2024-02-20)


 SS7 Не работает пассивный режим
- Basic **Bug**, Заказчик: **ООО “Тинькофф Мобайл“**

- Исправление отправки сообщения при работе с пассивным режимом




#### 1.0.12.0 (2022-12-30)
 API SS7FW/GET_SUBSCRIBER
- Basic **Freq**

#### 1.0.11.3 (2022-12-29)

[**Скачать патч базы (1.0.10.0)**](https://git.protei.ru/MobileDevelop/Protei_SS7FW/-/blob/master/Make/patch/patch.1.0.10.0.sql)

Save into DB ati/sri4sm host (for rules SendATI, SendSRI4SM), decode DestRef
- Basic **Bug**
#### 1.0.11.2 (2022-12-28)
 Не применяется правило с тегами
- Basic **Bug**
#### 1.0.11.1 (2022-12-23)
 Обрабатываются не все map opcodes 
- Basic **Bug**
#### 1.0.11.0 (2022-12-23)
 Не обрабатывается список с регулярными выражениями 
- Basic **Bug**
#### 1.0.10.0 (2022-12-21)
[**Скачать патч базы**](https://git.protei.ru/MobileDevelop/Protei_SS7FW/-/blob/master/Make/patch/patch.1.0.10.0.sql)

 Добавить возможность отправлять SRI4SM вместо ATI 
- Basic **Bug**


#### 1.0.9.6 (2022-11-21)

 Не заблокировали сообщение по соответствию MAP_SMSC параметра
- Basic **Bug**

#### 1.0.9.5 (2022-11-14)

 Не выводятся в cdr сообщения, заблокированные по причине ошибки декодирования
- Basic **Bug**

#### 1.0.9.4 (2022-11-10)

 Вернуть проверку соответствия OpCode и ACN в код. 
- Basic **Freq**

#### 1.0.9.3 (2022-11-07)

 утечка логик 
- Basic **Bug**

#### 1.0.9.2 (2022-10-24)

 Добавить анализ полей camel.serviceKey и camel.eventTypeBCSM 
- Basic **Freq**

#### 1.0.9.1 (2022-06-20)

 Управление id направлением для отправки ATI 
- Basic **Freq**

#### 1.0.9.0 (2022-04-06)

 VelocityCheck
- Basic **Freq**

#### 1.0.7.8 (2021-06-08)

 Добавить в CDR EN_ACTION_SEND_ALARM = 5
- Basic **Task**

#### 1.0.7.7 (2021-06-04)

 Вынести UDP-примитивы в отдельную библиотеку 
- Basic **Task**

#### 1.0.7.6 (2021-03-25)

 DIAM  cdr ati для диаметра, передача IsRequest в RuleChain 
- Basic **Task**

#### 1.0.7.6 (2021-03-18)

 DIAM Вставка в базу данных о регистрации, добавление новых кодов ошибок
- Basic **Task**

#### 1.0.7.5 (2021-03-17)

 Изменение адгоритма VelociryCheck для диаметра
- Basic **Task**

#### 1.0.7.4 (2021-03-10)

 DIAM cdr: sessionId wrap  
- Basic **Task**

#### 1.0.7.3 (2021-03-09)

 Добавлен параметр ErrorCode Reject диаметра, изменение алгоритма VelocityCheck 
- Basic **Task**

#### 1.0.7.2 (2021-02-12)

 Добавить в конфигурацию преобразование IMSI в e214 для посылки MAP_ATI
- Basic **Task**

#### 1.0.7.1 (2021-01-17)

 Подготовка тестового стенда для демонстрации 
- Basic **Task**

#### 1.0.7.0 (2020-12-02)

 prometheus статистика
- Basic **Freq**

#### 1.0.6.0 (2020-11-24)

 UDP-интерфейс
- Basic **Freq**

#### 1.0.5.2 (2020-10-12)

 Восстановление GT(CgPA) из TCAP_BEGIN

 Закрытие транзакции для tcap-begin, который не содержит мап-компоненты
- Basic **Freq**

#### 1.0.5.0 (2020-10-02)

 Подсистема аварий

 Поддержка DIAM (реализация запроса MAP_ATI))
- Basic **Freq**

#### 1.0.4.1.1007 (2020-09-14)

 Неполный cdr в случае, когда не задействован функционал Rules 
- Basic **Freq**
- Доработан формат основного cdr

#### 1.0.4.0
 Поддержка DIAM
- Basic **Freq**

 сдр для аналитики
- Basic **Freq**
- Переделан формат основного cdr, изменена логика записи результата блокировки и причины

 Проверка корректности по регулярному выражению
- Basic **Freq**
- Сформирована маска для IMSI: наличие любого нечислового символа (".*[^\\d]+.*")

 Обработка Global OpCode
- Basic **Freq**
- Блокировака PDU, если в нем есть GlobalOpCode

 Поддержка мультикомпонентности
- Basic **Freq**
- Добавлен разбор пришедшей TCAP-Data на список компонент (CAP/MAP), декодирование, проверка правилами
