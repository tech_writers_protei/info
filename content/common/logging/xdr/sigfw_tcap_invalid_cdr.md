---
title: "SigFW TCAP Invalid CDR"
description: "Журнал SigFW_tcap_invalid_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N | Параметр                                     | Описание                                                 | Тип            |
|---|----------------------------------------------|----------------------------------------------------------|----------------|
| 1 | DateTime                                     | Дата и время формирования записи.                        | datetime       |
| 2 | [Cause](#block-cause-sigfw_tcap_invalid_cdr) | Код причины блокировки.                                  | int            |
| 3 | [ParsedPart](#parsed-fields)                 | Перечень обработанных полей сообщений.                   | \[int/string\] |
| 4 | RestPart                                     | Перечень байтов, значение которых не удалось определить. | \[hex\]        |

### Определяемые в сообщении поля {#parsed-fields}

| N   | Параметр      | Описание                                                                                                                              | Тип     |
|-----|---------------|---------------------------------------------------------------------------------------------------------------------------------------|---------|
| 1   | Msg_Id        | Идентификатор сообщения.                                                                                                              | int     |
| 2   | MTP3_OPC      | Значение <abbr title="Origination Point Code">OPC</abbr>.<br>**Примечание.** Если отсутствует, то значение `-1`.                      | int     |
| 3   | MTP3_DPC      | Значение <abbr title="Destination Point Code">DPC</abbr>.<br>**Примечание.** Если отсутствует, то значение `-1`.                      | int     |
| 4   | SCCP.GT_A     | Номер GT отправителя.                                                                                                                 | string  |
| 5   | SCCP.GT_A_SSN | Номер подсистемы, <abbr title="Subsystem Number">SSN</abbr>, GT отправителя.                                                          | int     |
| 6   | SCCP.GT_A_NP  | Код плана нумерации, <abbr title="Numbering Plan">NP</abbr>, GT отправителя.                                                          | int     |
| 7   | SCCP.GT_A_TT  | Код типа трансляции, <abbr title="Translation Type">TT</abbr>, GT отправителя.                                                        | int     |
| 8   | SCCP.GT_A_NAI | Индикатор источника адреса, <abbr title="Nature of Address Indicator">NAI</abbr>, GT отправителя.                                     | int     |
| 9   | SCCP.GT_A_ES  | Схема кодирования, <abbr title="Encoding Scheme">ES</abbr>, GT отправителя.                                                           | int     |
| 10  | SCCP.GT_A_RI  | Индикатор маршрута, <abbr title="Routing Indicator">RI</abbr>, GT отправителя.                                                        | int     |
| 11  | SCCP.GT_B     | Номер GT получателя.                                                                                                                  | string  |
| 12  | SCCP.GT_B_SSN | Номер подсистемы, <abbr title="Subsystem Number">SSN</abbr>, GT получателя.                                                           | int     |
| 13  | SCCP.GT_B_NP  | Код плана нумерации, <abbr title="Numbering Plan">NP</abbr>, GT получателя.                                                           | int     |
| 14  | SCCP.GT_B_TT  | Код типа трансляции, <abbr title="Translation Type">TT</abbr>, GT получателя.                                                         | int     |
| 15  | SCCP.GT_B_NAI | Индикатор источника адреса, <abbr title="Nature of Address Indicator">NAI</abbr>, GT получателя.                                      | int     |
| 16  | SCCP.GT_B_ES  | Схема кодирования, <abbr title="Encoding Scheme">ES</abbr>, GT получателя.                                                            | int     |
| 17  | SCCP.GT_B_RI  | Индикатор маршрута, <abbr title="Routing Indicator">RI</abbr>, GT получателя.                                                         | int     |
| 18  | TCAP_Msg_Type | Тип сообщения.<br>`2` — TCAP_BEGIN / `4` — TCAP_END / `5` — TCAP_CONTINUE / `7` — TCAP_ABORT.                                         | int     |
| 19  | TCAP_ACN      | Имя контекста приложения, <abbr title="Application Context Name">ACN</abbr>.<br>**Примечание.** Записываются только цифры, без точек. | string  |
| 20  | TCAP_OTID     | Значение <abbr title="Original Transaction Identifier">OTID</abbr>.                                                                   | int/hex |
| 21  | TCAP_DTID     | Значение <abbr title="Destination Transaction Identifier">DTID</abbr>.                                                                | int/hex |

#### Пример

```log
2023-12-19 13:58:25;2130706511;0;{0;-1;-1;937300000014;149;1;0;4;6;2;93730342639;6;1;0;4;6;2;5;;00000005;00000006};[];
2023-12-19 13:58:25;2130706511;0;{0;-1;-1;93730342639;6;1;0;4;6;2;937300000014;149;1;0;4;6;2;4;;;00000005};[64 1d 49 4 0 0 0 e1 6c 15 a2 13 2 1 1 30 e 2 1 17 30 9 4 7 91 97 12 82 43 54 f5];
2023-12-19 13:58:25;2130706512;0;{0;-1;-1;937300000015;149;1;0;4;6;2;93730342639;6;1;0;4;6;2;5;;00000007;00000008};[65 1e 48 4 0 0 0 e1 49 4 0 0 0 e2 6c 10 a2 e 2 1 0 30 9 2 1 7 30 4 86 2 7 80];
```

### Код причины блокировки {#block-cause-sigfw_tcap_invalid_cdr}

include::../../appendices/block_cause.adoc[]