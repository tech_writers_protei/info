---
title: "SigFW ATI CDR"
description: "Журнал ss7fw_ati_cdr"
weight: 20
type: docs
---

### Описание используемых полей ###

| N   | Поле                     | Описание                                              | Тип      |
|-----|--------------------------|-------------------------------------------------------|----------|
| 1   | DateTime                 | Дата и время формирования записи.                     | datetime |
| 2   | Session_Id               | Идентификатор сессии, `Session-Id`.                   | int      |
| 3   | OwnAddress               | Номер GT отправителя.                                 | string   |
| 4   | HLR                      | Номер GT получателя.                                  | string   |
| 5   | MAP_IMSI                 | Номер IMSI.                                           | string   |
| 6   | ErrorCode                | Код ошибки протокола MAP или обслуживающей логики SL. | int      |
| 7   | AgeOfLocation            | Значение `AgeOfLocation`.                             | int      |
| 8   | CellID                   | Идентификатор соты, `CellID`.                         | int      |
| 9   | LAC                      | Код области.                                          | int      |
| 10  | CurrentLocationRetrieved | Значение `CurrentLocationRetrieved`.                  | int      |
| 11  | MCC                      | Мобильный код страны, MCC.                            | int      |
| 12  | MNC                      | Код мобильной сети, MNC.                              | int      |
| 13  | MSC                      | Адрес MSC.                                            | string   |
| 14  | VLR                      | Адрес VLR.                                            | string   |

#### Пример ####

```log
2020-06-04 15:51:21.758;10;79215467388;792199000002;250029675465690;0;3;60035;111;0;250;2;79219900001;79215342636;
```