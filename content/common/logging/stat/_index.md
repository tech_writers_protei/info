---
title: "Статистика SigFW"
description: "Описание формата файлов статистики SigFW"
weight: 20
type: docs
---

В данном разделе приведено описание файлов статистики.
По умолчанию файлы хранятся в директории `/usr/protei/log/Protei_SigFW/stat`.