---
title: "gtp_stat"
description: "Статистика по сообщениям через определенные интерфейсы"
weight: 20
type: docs
---

<!-- 
Не знаю, что имеется в виду под finds/inserts, стр. 97
Нашел такое описание rx_mac_short_pkt_dropped, но не понимаю перевода этого предложения, стр. 151
-->

## Основные разделы статистики

* [Статистика ошибок dump_writer](#dump-writer-stats) - значения счетчиков ошибок утилиты записи дампа за все время работы приложения;
* [Статистика работы форвардеров](#forwarder-stats) - значения счетчиков параметров работы:
  * [Статистика по пакетам](#forwarder-packets) - за предыдущий промежуток записи;
  * [Статистика по mempool](#forwarder-mempool) - в момент записи статистики;
  * [Таблица дефрагментации](#forwarder-defragment) - в момент записи статистики;
  * [Статистика ошибок](#forwarder-errors) - за все время работы приложения.
* [Статистика интерфейса](#nic-stats) - значения счетчиков параметров работы за все время работы приложения.

<!-- Статистика GTPFW состоит из нескольких частей:

* Статистика ошибок dump_writer. Счетчики **не обнуляются** в конце интервала записи и показывают количество ошибок,
  накопленное за всё время работы приложения:
    * **dump_writer_drops_inner** - неуспешные попытки добавить пакет в очередь на запись в PCAP-файл. Может возникать, если
      dump_writer не успевает записывать пакеты в файл.
    * **dump_writer_drops_outer** - неуспешные попытки скопировать пакет перед добавлением в очередь. Может говорить о
      неправильной настройке или баге в приложении
* Статистика по форвардерам:
  * Статистика по пакетам показывается за предыдущий интервал записи.
  * **mbuf pool in use** и **mbuf indirect pool in use** состояние внутренних mempool форвардера в момент записи
    статистики.
  * Состояние таблицы дефрагментации в момент записи статистики и счетчики ошибок за всё время работы приложения.
* Статистика по каждому NIC. Счетчики **не обнуляются** в конце интервала записи.
-->

### Описание используемых полей статистики ошибок dump_writer {#dump-writer-stats}

| Параметр                                                      | Описание                                                                        | Тип |
|---------------------------------------------------------------|---------------------------------------------------------------------------------|-----|
| <a name="dump-writer-drops-inner">dump_writer_drops_inner</a> | Количество неуспешных попыток добавить пакет в очередь на запись в файл *.pcap. | int |
| <a name="dump-writer-drops-outer">dump_writer_drops_outer</a> | Количество неуспешных попыток дублировать пакет перед добавлением в очередь.    | int |

**Примечание.** Рост значения [dump_writer_drops_inner](#dump-writer-drops-inner) может означать, что dump_writer не успевает записывать пакеты в файл. Рост значения [dump_writer_drops_outer](#dump-writer-drops-outer) может означать, что dump_writer неправильно настроен.

### Описание используемых полей статистики по форвардерам {#forwarder-stats}

#### Описание используемых полей статистики по пакетам {#forwarder-packets}

| Параметр                                    | Описание                                                                                         | Тип |
|---------------------------------------------|--------------------------------------------------------------------------------------------------|-----|
| Packets Received                            | Общее количество входящих сообщений.                                                             | int |
| GTP-C packets allowed by SL                 | Количество разрешенных к отправке логикой GTP-C пакетов.                                         | int |
| Fragments received                          | Количество полученных IP-фрагментов.                                                             | int |
| Packets reassembled from fragments          | Количество собранных из фрагментов пакетов.                                                      | int |
| Fragments sent                              | Количество отправленных IP-фрагментов.                                                           | int |
| Packets fragmented and sent                 | Количество фрагментированных перед отправкой пакетов.                                            | int |
| Packets sent                                | Количество успешно отправленных в сеть пакетов.                                                  | int |
| Invalid packets received (MTU check failed) | Количество некорректных входящих пакетов.                                                        | int |
| Failed to sent                              | Количество неуспешно отправленных пакетов.                                                       | int |
| Dropped by limits                           | Количество пакетов, отброшенных ввиду внутренних ограничений.                                    | int |
| Unknown ETH type packets dropped            | Количество отброшенных пакетов с неизвестным типом L3-заголовка.                                 | int |
| Unknown IP packets dropped                  | Количество отброшенных IP-пакетов c неизвестным протоколом.                                      | int |
| Failed to parse IP packet headers           | Количество IP-пакетов, отброшенных ввиду неуспешного декодирования L3-/L4-заголовков.            | int |
| GTP-C packets received                      | Количество GTP-C пакетов, перенаправленных для последующей проверки.                             | int |
| GTP-C packets dropped by IP whitelist       | Количество GTP-C пакетов, отброшенных ввиду отсутствия `SrcIP` в списке допустимых.              | int |
| GTP-C packets dropped by SL                 | Количество GTP-C пакетов, отброшенных логикой.                                                   | int |
| GTP-C packets failed to send to SL          | Количество некорректных GTP-C пакетов, не перенаправленных в логику.                             | int |
| GTP-U packets forwarded                     | Количество разрешенных GTP-U пакетов.                                                            | int |
| GTP-U packets dropped by IP whitelist       | Количество GTP-U пакетов, отброшенных ввиду отсутствия `SrcIP` списку допустимых.                | int |
| GTP-U packets dropped by session validation | Количество GTP-U пакетов, отброшенных ввиду несоответствия ранее сохраненным GTP-сессиям.        | int |
| GTP-U packets dropped by other reasons      | Количество GTP-U пакетов, отброшенных ввиду [других причин](#other-reasons).                     | int |
| GTP-U error indication send failed          | Количество заблокированных GTP-U пакетов, для которых не удалось сгенерировать Error Indication. | int |

##### Причины отбоя GTP-U сообщений {#other-reasons}

| N | Описание                                              |
|---|-------------------------------------------------------|
| 1 | Не поддерживаемый формат IP во входящем пакете GTP-U. |
| 2 | Некорректная длина GTP-U пакета.                      |

#### Описание используемых полей статистики по mempool {#forwarder-mempool}

| Параметр                  | Описание                                                        | Тип |
|---------------------------|-----------------------------------------------------------------|-----|
| mbuf pool in use          | Размер используемого пула.                                      | int |
| mbuf indirect pool in use | Размер пула, используемого непрямыми буферами, indirect buffer. | int |

#### Описание используемых полей таблицы дефрагментации {#forwarder-defragment}

| Параметр                     | Описание                                                           | Тип |
|------------------------------|--------------------------------------------------------------------|-----|
| max entries                  | Максимальное количество значений в таблице.                        | int |
| entries in use               | Используемое количество значений в таблице.                        | int |
| finds/inserts                |                                                                    | int |
| entries added                | Количество добавленных значений.                                   | int |
| entries deleted by timeout   | Количество значений, удаленных по истечении таймера.               | int |
| entries reused by timeout    | Количество значений, повторно использованных по истечении таймера. | int |
| total add failures           | Общее количество ошибок добавления значений в таблицу.             | int |
| add no-space failures        | Количество ошибок добавления ввиду отсутствия свободных ячеек.     | int |
| add hash-collisions failures | Количество ошибок добавления ввиду коллизий хэш-значений.          | int |

### Описание используемых полей статистики по интерфейсам {#nic-stats}

| Параметр                     | Описание                                                                  | Тип |
|------------------------------|---------------------------------------------------------------------------|-----|
| rx_good_packets              | Количество успешно принятых пакетов.                                      | int |
| tx_good_packets              | Количество успешно переданных пакетов.                                    | int |
| rx_good_bytes                | Размер успешно принятых пакетов, в байтах.                                | int |
| tx_good_bytes                | Размер успешно переданных пакетов, в байтах.                              | int |
| rx_missed_errors             | Количество не принятых пропущенных пакетов.                               | int |
| rx_errors                    | Количество принятых пакетов с ошибкой.                                    | int |
| tx_errors                    | Количество переданных пакетов с ошибкой.                                  | int |
| rx_mbuf_allocation_errors    | Количество отброшенных пакетов ввиду нехватки дескрипторов.               | int |
| rx_q0packets                 | Количество пакетов, принятых очередью.                                    | int |
| rx_q0bytes                   | Количество байт, принятых очередью.                                       | int |
| rx_q0errors                  | Количество пакетов с ошибкой, принятых очередью.                          | int |
| tx_q0packets                 | Количество пакетов, переданных очередью.                                  | int |
| tx_q0bytes                   | Количество байт, переданных очередью.                                     | int |
| rx_unicast_packets           | Количество принятых unicast-пакетов.                                      | int |
| rx_multicast_packets         | Количество принятых multicast-пакетов.                                    | int |
| rx_broadcast_packets         | Количество принятых broadcast-пакетов.                                    | int |
| rx_dropped_packets           | Количество принятых, но не обработанных пакетов.                          | int |
| rx_unknown_protocol_packets  | Количество принятых пакетов по неизвестному протоколу.                    | int |
| tx_unicast_packets           | Количество переданных unicast-пакетов.                                    | int |
| tx_multicast_packets         | Количество переданных multicast-пакетов.                                  | int |
| tx_broadcast_packets         | Количество переданных broadcast-пакетов.                                  | int |
| tx_dropped_packets           | Количество потерянных при передаче пакетов.                               | int |
| tx_link_down_dropped         | Количество потерянных пакетов ввиду обрыва соединения.                    | int |
| rx_crc_errors                | Количество принятых пакетов с ошибкой контрольной суммы.                  | int |
| rx_illegal_byte_errors       | Количество принятых пакетов, содержащих неразрешенное содержимое.         | int |
| rx_error_bytes               | Количество байт с неразрешенным содержимым.                               | int |
| mac_local_errors             | Количество ошибок локального MAC.                                         | int |
| mac_remote_errors            | Количество ошибок удаленного MAC.                                         | int |
| rx_len_errors                | Количество принятых пакетов с ошибкой длины данных.                       | int |
| tx_xon_packets               | Количество переданных XON-пакетов.                                        | int |
| rx_xon_packets               | Количество принятых XON-пакетов.                                          | int |
| tx_xoff_packets              | Количество переданных XOFF-пакетов.                                       | int |
| rx_xoff_packets              | Количество принятых XOFF-пакетов.                                         | int |
| rx_size_64_packets           | Количество принятых пакетов длиной 64 байта.                              | int |
| rx_size_65_to_127_packets    | Количество принятых пакетов длиной от 65 до 127 байт.                     | int |
| rx_size_128_to_255_packets   | Количество принятых пакетов длиной от 128 до 255 байт.                    | int |
| rx_size_256_to_511_packets   | Количество принятых пакетов длиной от 256 до 511 байт.                    | int |
| rx_size_512_to_1023_packets  | Количество принятых пакетов длиной от 512 до 1023 байт.                   | int |
| rx_size_1024_to_1522_packets | Количество принятых пакетов длиной от 1024 до 1522 байт.                  | int |
| rx_size_1523_to_max_packets  | Количество принятых пакетов длиной более 1523 байт.                       | int |
| rx_undersized_errors         | Количество принятых пакетов длиной менее 64 байт.                         | int |
| rx_oversize_errors           | Количество принятых пакетов с длиной, превышающей максимально допустимую. | int |
| rx_mac_short_pkt_dropped     | Number of MAC short packet discard packets received.                      | int |
| rx_fragmented_errors         | Количество пакетов с ошибочной контрольной суммой длиной менее 64 байт.   | int |
| rx_jabber_errors             | Количество пакетов с ошибочной контрольной суммой длиной более 1523 байт. | int |
| tx_size_64_packets           | Количество переданных пакетов длиной 64 байта.                            | int |
| tx_size_65_to_127_packets    | Количество переданных пакетов длиной от 65 до 127 байт.                   | int |
| tx_size_128_to_255_packets   | Количество переданных пакетов длиной от 128 до 255 байт.                  | int |
| tx_size_256_to_511_packets   | Количество переданных пакетов длиной от 256 до 511 байт.                  | int |
| tx_size_512_to_1023_packets  | Количество переданных пакетов длиной от 512 до 1023 байт.                 | int |
| tx_size_1024_to_1522_packets | Количество переданных пакетов длиной от 1024 до 1522 байт.                | int |
| tx_size_1523_to_max_packets  | Количество переданных пакетов длиной более 1523 байтов.                   | int |


### Пример

```log
2024-04-12 16:44:57
dump_writer_drops_inner : 47695758
dump_writer_drops_outer : 0

FWD[0] Packets Received : 726367
FWD[0] GTP-C packets allowed by SL : 723
FWD[0] Fragments received : 394761
FWD[0] Packets reassembled from fragments : 197379
FWD[0] Fragments sent : 394758
FWD[0] Packets fragmented and sent : 197379
FWD[0] Packets sent : 726363
FWD[0] Invalid packets received (MTU check failed) : 0
FWD[0] Failed to sent : 0
FWD[0] Dropped by limits : 0
FWD[0] Unknown ETH type packets dropped : 0
FWD[0] Unknown IP packets dropped : 0
FWD[0] Failed to parse IP packet headers : 0
FWD[0] GTP-C packets received : 724
FWD[0] GTP-C packets dropped by IP whitelist : 0
FWD[0] GTP-C packets dropped by SL : 0
FWD[0] GTP-C packets failed to send to SL : 0
FWD[0] GTP-U packets forwarded : 330879
FWD[0] GTP-U packets dropped by IP whitelist : 0
FWD[0] GTP-U packets dropped by session validation : 0
FWD[0] GTP-U packets dropped by other reasons : 0
FWD[0] GTP-U error indication send failed : 0
FWD[0] mbuf pool in use : 431
FWD[0] mbuf indirect pool in use : 432

FWD[0] defrag table stats / max entries:	2048;
FWD[0] defrag table stats / entries in use:	0;
FWD[0] defrag table stats / finds/inserts:	0;
FWD[0] defrag table stats / entries added:	0;
FWD[0] defrag table stats / entries deleted by timeout:	0;
FWD[0] defrag table stats / entries reused by timeout:	0;
FWD[0] defrag table stats / total add failures:	0;
FWD[0] defrag table stats / add no-space failures:	0;
FWD[0] defrag table stats / add hash-collisions failures:	0;

FWD[1] Packets Received : 584705
FWD[1] GTP-C packets allowed by SL : 748
FWD[1] Fragments received : 0
FWD[1] Packets reassembled from fragments : 0
FWD[1] Fragments sent : 0
FWD[1] Packets fragmented and sent : 0
FWD[1] Packets sent : 584705
FWD[1] Invalid packets received (MTU check failed) : 0
FWD[1] Failed to sent : 0
FWD[1] Dropped by limits : 0
FWD[1] Unknown ETH type packets dropped : 0
FWD[1] Unknown IP packets dropped : 0
FWD[1] Failed to parse IP packet headers : 0
FWD[1] GTP-C packets received : 748
FWD[1] GTP-C packets dropped by IP whitelist : 0
FWD[1] GTP-C packets dropped by SL : 0
FWD[1] GTP-C packets failed to send to SL : 0
FWD[1] GTP-U packets forwarded : 583943
FWD[1] GTP-U packets dropped by IP whitelist : 0
FWD[1] GTP-U packets dropped by session validation : 0
FWD[1] GTP-U packets dropped by other reasons : 0
FWD[1] GTP-U error indication send failed : 0
FWD[1] mbuf pool in use : 0
FWD[1] mbuf indirect pool in use : 0

FWD[1] defrag table stats / max entries:	2048;
FWD[1] defrag table stats / entries in use:	0;
FWD[1] defrag table stats / finds/inserts:	0;
FWD[1] defrag table stats / entries added:	0;
FWD[1] defrag table stats / entries deleted by timeout:	0;
FWD[1] defrag table stats / entries reused by timeout:	0;
FWD[1] defrag table stats / total add failures:	0;
FWD[1] defrag table stats / add no-space failures:	0;
FWD[1] defrag table stats / add hash-collisions failures:	0;

0000:08:00.0
RX bytes     : 64611363980
dRX bytes    : 498075293
RX Mbps      : 796
RX packets   : 97863413
dRX packets  : 726373
RX errors    : 0
TX bytes     : 10342208210
dTX bytes    : 82801965
TX Mbps      : 132
TX packets   : 74468390
dTX packets  : 584651
TX errors    : 0
missed       : 0
no mbufs     : 0
RX pool 0 used : 2635
RX pool 0 free : 128436
Link Status  : UP
Link Speed   : 10000
Link Duplex  : full
Link Autoneg : autoneg
xstats: rx_good_packets : 97863439
xstats: tx_good_packets : 74468399
xstats: rx_good_bytes : 64611374578
xstats: tx_good_bytes : 10342209828
xstats: rx_missed_errors : 0
xstats: rx_errors : 0
xstats: tx_errors : 0
xstats: rx_mbuf_allocation_errors : 0
xstats: rx_q0packets : 0
xstats: rx_q0bytes : 0
xstats: rx_q0errors : 0
xstats: tx_q0packets : 0
xstats: tx_q0bytes : 0
xstats: rx_unicast_packets : 97863040
xstats: rx_multicast_packets : 83
xstats: rx_broadcast_packets : 296
xstats: rx_dropped_packets : 0
xstats: rx_unknown_protocol_packets : 0
xstats: tx_unicast_packets : 74468016
xstats: tx_multicast_packets : 82
xstats: tx_broadcast_packets : 298
xstats: tx_dropped_packets : 0
xstats: tx_link_down_dropped : 0
xstats: rx_crc_errors : 0
xstats: rx_illegal_byte_errors : 0
xstats: rx_error_bytes : 0
xstats: mac_local_errors : 0
xstats: mac_remote_errors : 1
xstats: rx_len_errors : 0
xstats: tx_xon_packets : 0
xstats: rx_xon_packets : 0
xstats: tx_xoff_packets : 0
xstats: rx_xoff_packets : 0
xstats: rx_size_64_packets : 29203385
xstats: rx_size_65_to_127_packets : 17094620
xstats: rx_size_128_to_255_packets : 8320288
xstats: rx_size_256_to_511_packets : 3455021
xstats: rx_size_512_to_1023_packets : 14
xstats: rx_size_1024_to_1522_packets : 39790095
xstats: rx_size_1523_to_max_packets : 0
xstats: rx_undersized_errors : 0
xstats: rx_oversize_errors : 0
xstats: rx_mac_short_pkt_dropped : 0
xstats: rx_fragmented_errors : 0
xstats: rx_jabber_errors : 0
xstats: tx_size_64_packets : 836
xstats: tx_size_65_to_127_packets : 62663376
xstats: tx_size_128_to_255_packets : 34
xstats: tx_size_256_to_511_packets : 11804151
xstats: tx_size_512_to_1023_packets : 0
xstats: tx_size_1024_to_1522_packets : 0
xstats: tx_size_1523_to_max_packets : 0

0000:09:00.0
RX bytes     : 10342224973
dRX bytes    : 82795140
RX Mbps      : 132
RX packets   : 74468476
dRX packets  : 584601
RX errors    : 0
TX bytes     : 64611300064
dTX bytes    : 498066422
TX Mbps      : 796
TX packets   : 97864270
dTX packets  : 726363
TX errors    : 0
missed       : 0
no mbufs     : 0
RX pool 0 used : 3044
RX pool 0 free : 128027
Link Status  : UP
Link Speed   : 10000
Link Duplex  : full
Link Autoneg : autoneg
xstats: rx_good_packets : 74468484
xstats: tx_good_packets : 97864270
xstats: rx_good_bytes : 10342226655
xstats: tx_good_bytes : 64611300064
xstats: rx_missed_errors : 0
xstats: rx_errors : 0
xstats: tx_errors : 0
xstats: rx_mbuf_allocation_errors : 0
xstats: rx_q0packets : 0
xstats: rx_q0bytes : 0
xstats: rx_q0errors : 0
xstats: tx_q0packets : 0
xstats: tx_q0bytes : 0
xstats: rx_unicast_packets : 74468098
xstats: rx_multicast_packets : 82
xstats: rx_broadcast_packets : 298
xstats: rx_dropped_packets : 0
xstats: rx_unknown_protocol_packets : 0
xstats: tx_unicast_packets : 97863891
xstats: tx_multicast_packets : 83
xstats: tx_broadcast_packets : 296
xstats: tx_dropped_packets : 0
xstats: tx_link_down_dropped : 0
xstats: rx_crc_errors : 0
xstats: rx_illegal_byte_errors : 0
xstats: rx_error_bytes : 0
xstats: mac_local_errors : 1
xstats: mac_remote_errors : 1
xstats: rx_len_errors : 0
xstats: tx_xon_packets : 0
xstats: rx_xon_packets : 0
xstats: tx_xoff_packets : 0
xstats: rx_xoff_packets : 0
xstats: rx_size_64_packets : 836
xstats: rx_size_65_to_127_packets : 62663430
xstats: rx_size_128_to_255_packets : 34
xstats: rx_size_256_to_511_packets : 11804179
xstats: rx_size_512_to_1023_packets : 0
xstats: rx_size_1024_to_1522_packets : 0
xstats: rx_size_1523_to_max_packets : 0
xstats: rx_undersized_errors : 0
xstats: rx_oversize_errors : 0
xstats: rx_mac_short_pkt_dropped : 0
xstats: rx_fragmented_errors : 0
xstats: rx_jabber_errors : 0
xstats: tx_size_64_packets : 29204363
xstats: tx_size_65_to_127_packets : 17094586
xstats: tx_size_128_to_255_packets : 8320270
xstats: tx_size_256_to_511_packets : 3455020
xstats: tx_size_512_to_1023_packets : 14
xstats: tx_size_1024_to_1522_packets : 39790017
xstats: tx_size_1523_to_max_packets : 0
```
