---
title: "Спецификации и рекомендации"
description: "Перечень стандартов и рекомендаций, использованных при разработке продукта PROTEI SigFW"
weight: 100
type: docs
---
== Спецификации и рекомендации
:asciidoctorconfigdir: ../..
:table-caption: Таблица

Использованные при разработке SigFW спецификации и рекомендации.

.RFC
[options="header",cols="1,6"]
|===
|Стандарт |Название 

|https://www.gsma.com/security/resources/fs-07-ss7-and-sigtran-network-security-v4-0/[RFC 3588]
|SS7 and SIGTRAN Network Security

|https://www.gsma.com/security/resources/fs-11-ss7-interconnect-security-monitoring-and-firewall-guidelines-v6-0/[RFC 4004]
|SS7 Interconnect Security Monitoring and Firewall Guidelines

|https://www.gsma.com/security/resources/fs-19-diameter-interconnect-security-v7-0/[RFC 4005]
|Diameter Interconnect Security

|https://www.gsma.com/security/resources/fs-20-gprs-tunnelling-protocol-gtp-security-v3-0/[RFC 4006]
|GPRS Tunnelling Protocol (GTP) Security

|https://www.gsma.com/security/resources/ir-82-ss7-security-network-implementation-guidelines-v5-0/[RFC 4072]
|SS7 Security Network Implementation Guidelines
|===

.GSM Association
[options="header",cols="1,6"]
|===
|Стандарт |Название 

|https://www.gsma.com/security/resources/fs-07-ss7-and-sigtran-network-security-v4-0/[FS.07]
|SS7 and SIGTRAN Network Security

|https://www.gsma.com/security/resources/fs-11-ss7-interconnect-security-monitoring-and-firewall-guidelines-v6-0/[FS.11]
|SS7 Interconnect Security Monitoring and Firewall Guidelines

|https://www.gsma.com/security/resources/fs-19-diameter-interconnect-security-v7-0/[FS.19]
|Diameter Interconnect Security

|https://www.gsma.com/security/resources/fs-20-gprs-tunnelling-protocol-gtp-security-v3-0/[FS.20]
|GPRS Tunnelling Protocol (GTP) Security

|https://www.gsma.com/security/resources/ir-82-ss7-security-network-implementation-guidelines-v5-0/[IR.82]
|SS7 Security Network Implementation Guidelines
|===

.3GPP
[options="header",cols="1,6"]
|===
|Стандарт |Название 

|https://www.etsi.org/deliver/etsi_ts/129000_129099/129060/17.04.00_60/ts_129060v170400p.pdf[3GPP TS 29.060]
|General Packet Radio Service (GPRS); +
GPRS Tunnelling Protocol (GTP) across the Gn and Gp Interface

|https://www.etsi.org/deliver/etsi_ts/129000_129099/129078/17.00.00_60/ts_129078v170000p.pdf[3GPP TS 29.078]
|Digital cellular telecommunications system (Phase 2+) (GSM); +
Universal Mobile Telecommunications System (UMTS); +
Customised Applications for Mobile network Enhanced Logic (CAMEL) Phase X; +
CAMEL Application Part (CAP) specification

|https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.09.00_60/ts_129274v170900p.pdf[3GPP TS 29.274]
|3GPP Evolved Packet System (EPS); +
Evolved General Packet Radio Service (GPRS) Tunnelling Protocol for Control plane (GTPv2-C); +
Stage 3
|===