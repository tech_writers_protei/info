---
title: "Интерфейсы управления"
description: "HTTP REST API"
weight: 4
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path:  "content/common/api/_index.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

## Общая информация HTTP API ##

HTTP API используется для получения информации:

- состояние GTP-/SGs-соединений;
- состояние базовых станций;
- текущий статус абонентов;
- состояние баз данных.

Все запросы обрабатываются в синхронном режиме. Обработка всех клиентских запросов и отправка ответов также осуществляется в синхронном режиме.

Запросы отправляются по протоколу HTTP.

Приложение должно использовать один адрес URL для отправки запросов:

```
http://host:port/mme/v1/command?arg=value&...&arg=value
```

| Поле    | Описание                                 | Тип       | По умолчанию | O/M |
|---------|------------------------------------------|-----------|--------------|-----|
| host    | IP-адрес или DNS-имя PROTEI MME API.     | ip/string | -            | M   |
| port    | Номер порта для установления соединения. | int       | -            | M   |
| command | Имя выполняемой команды.                 | string    | -            | M   |
| arg     | Параметр команды.                        | string    | -            | O   |
| value   | Значение параметра.                      | Any       | -            | O   |

Все запросы осуществляются с помощью метода **HTTP GET**.

## Ответ от PROTEI MME ##

Если ответ на запрос требует передачи объекта, система отправляет объект в формате JSON и **Content-Type: application/json**.

Если ответ на запрос не требует передачи объекта, система отправляет статус выполнения запроса.

```text
200 OK
```

**Примечание.** Именно такой ответ передает система, если не указано иное.

## Команды ##

* [clear_dns_cache](#clear_dns_cache) - очистить cache адресов DNS;
* [deact_bearer](#deact_bearer) - деактивировать bearer-службу;
* [detach](#detach) - удалить регистрацию абонента из сети;
* [detach_by_vlr](#detach_by_vlr) - удалить регистрации абонентов на определенном VLR и запретить новые;
* [disconnect_enb](#disconnect_enb) - разорвать соединение с eNodeB;
* [enable_vlr](#enable_vlr) - разрешить регистрации на определенном VLR;
* [get_db_status](#get_db_status) - получить информацию о состоянии базы данных;
* [get_gtp_peers](#get_gtp_peers) - получить информацию о состоянию GTP-соединений;
* [get_location](#get_location) - получить местоположение абонента;
* [get_metrics](#get_metrics) - получить текущие значения метрик;
* [get_profile](#get_profile) - получить информацию об абоненте;
* [get_s1_peers](#get_s1_peers) - получить информацию о состоянии базовых станций LTE из хранилища MME;
* [get_sgs_peers](#get_sgs_peers) - получить информацию о состоянии SGs-соединений;
* [reset_metrics](#reset_metrics) - сбросить текущие значения метрик;
* [ue_disable_trace](#ue_disable_trace) - деактивировать трассировку на eNodeB для абонента;
* [ue_enable_trace](#ue_enable_trace) - активировать трассировку на eNodeB для абонента.

### <a name="clear_dns_cache">Очистить cache адресов DNS, clear_dns_cache</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/clear_dns_cache
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/clear_dns_cache
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата. 

### <a name="deact_bearer">Деактивировать bearer-службу, deact_bearer</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/deact_bearer&imsi=<imsi>&bearer_id=<id>
```

#### Поля запроса ####

| Поле      | Описание                                    | Тип    | По умолчанию | O/M |
|-----------|---------------------------------------------|--------|--------------|-----|
| imsi      | Номер IMSI абонента.                        | string | -            | M   |
| bearer_id | Идентификатор деактивируемой bearer-службы. | int    | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/deact_bearer&imsi=250481234567890&bearer_id=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No bearer_id` - некорректный запрос: не задано значение **bearer_id**;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to deact bearer` - услуга недоступна ввиду невозможности деактивировать bearer-службу;

### <a name="detach">Удалить регистрацию абонента из сети, detach</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach?imsi=<imsi>&reattach=<bool>&purge=<bool>
```

#### Поля запроса ####

| Поле     | Описание                               | Тип    | По умолчанию | O/M |
|----------|----------------------------------------|--------|--------------|-----|
| imsi     | Номер IMSI абонента.                   | string | -            | M   |
| reattach | Флаг переподключения к сети.           | bool   | 0            | O   |
| purge    | Флаг полного удаления профиля из сети. | bool   | 1            | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach?imsi=25048123456789&reattach=1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;
* `503 Service Unavailable, Unable to detach UE` - услуга недоступна ввиду невозможности удаления регистрации;

### <a name="detach_by_vlr">Удалить регистрации абонентов на определенном VLR и запретить новые, detach_by_vlr</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/detach_by_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | По умолчанию | O/M |
|------|--------------------|-----|--------------|-----|
| ip   | IP-адрес узла VLR. | ip  | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **ip**;

### <a name="disconnect_enb">Разорвать соединение с eNodeB, disconnect_enb</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/disconnect_enb&ip=<ip>
```

#### Поля запроса ####

| Поле | Описание         | Тип | По умолчанию | O/M |
|------|------------------|-----|--------------|-----|
| ip   | IP-адрес eNodeB. | ip  | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/disconnect_enb&ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **ip**;
* `404 Not Found` - указанная eNodeB не найдена;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="enable_vlr">Разрешить регистрации на определенном VLR, enable_vlr</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/enable_vlr?ip=<ip>
```

#### Поля запроса ####

| Поле | Описание           | Тип | По умолчанию | O/M |
|------|--------------------|-----|--------------|-----|
| ip   | IP-адрес узла VLR. | ip  | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/detach_by_vlr?ip=192.168.1.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, No address` - некорректный запрос: не задано значение **ip**;

### <a name="get_db_status">Получить информацию о состоянии базы данных, get_db_status</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_db_status
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_db_status
```

#### Параметры ответа ####

| Поле              | Описание                                                              | Тип            | По умолчанию | O/M |
|-------------------|-----------------------------------------------------------------------|----------------|--------------|-----|
| Writer            | Параметры соединения, добавляющего записи в базу данных.              | object         | -            | M   |
| **{**             |                                                                       |                |              |     |
| State             | Состояние соединения.<br>connected/disconnected/busy/unknown.         | string         | -            | M   |
| Last job          | Параметры последнего изменения.                                       | object         | -            | M   |
| &nbsp;&nbsp;**{** |                                                                       |                |              |     |
| Done              | Флаг завершения операции.                                             | bool           | -            | M   |
| Rows saved        | Количество записанных строк.                                          | int            | -            | M   |
| Timestamp         | Временная метка завершения операции. Формат:<br>`YYYY-MM-DD hh:mm:ss` | datetime       | -            | M   |
| &nbsp;&nbsp;**}** |                                                                       |                |              |     |
| **}**             |                                                                       |                |              |     |
| Readers           | Параметры соединений, считывающих записи из базы данных.              | list\<object\> | -            | M   |
| **{**             |                                                                       |                |              |     |
| State             | Состояние соединения.<br>connected/disconnected/busy/unknown.         | string         | -            | M   |
| **}**             |                                                                       |                |              |     |

#### Пример ответа ####

```json5
{
  "Writer": {
    "State": "connected",
    "Last job": {
      "Done": true,
      "Rows saved": 0,
      "Timestamp": "2023-01-01 01:23:45"
    }
  },
  "Readers": [
    {
      "State": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="get_location">Получить местоположение абонента, get_location</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_location?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | По умолчанию | O/M |
|------|----------------------|--------|--------------|-----|
| imsi | Номер IMSI абонента. | string | -            | M   |

#### Пример запроса ####

```
http://localhost:65535/mme/v1/get_location&imsi=25048123456789
```

#### Параметры ответа ####

| Поле                    | Описание                    | Тип    | По умолчанию | O/M |
|-------------------------|-----------------------------|--------|--------------|-----|
| [EPS_State](#eps-state) | Код состояния абонента EPS. | int    | -            | M   |
| PLMN                    | Идентификатор сети PLMN.    | string | -            | M   |
| TAC                     | Код области отслеживания.   | string | -            | M   |
| CellID                  | Идентификатор соты.         | hex    | -            | M   |
| ENB-ID                  | Идентификатор eNodeB.       | int    | -            | M   |


#### <a name=eps-state>Значения EPS State</a> ####

Полное описание см. [3GPP TS 23.078](https://www.etsi.org/deliver/etsi_ts/123000_123099/123078/17.00.00_60/ts_123078v170000p.pdf).

* 0 - Detached;
* 1 - AttachedNotReachableForPaging;
* 2 - AttachedReachableForPaging;
* 3 - ConnectedNotReachableForPaging;
* 4 - ConnectedReachableForPaging;
* 5 - NotProvidedFromSGSN;

#### Пример ответа ####

```json5
{
  "EPS_State": 0,
  "PLMN": "25002",
  "TAC": "0",
  "CellID": 0x10,
  "ENB-ID": 15000
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="get_metrics">Получить текущие значения метрик, get_metrics</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_metrics
```

### <a name="get_profile">Получить информацию об абоненте, get_profile</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_profile?imsi=<imsi>&guti=<guti>&imeisv=<imeisv>&select=<fields>
```

#### Поля запроса ####

| Поле                        | Описание                                                                                                   | Тип    | По умолчанию | O/M |
|-----------------------------|------------------------------------------------------------------------------------------------------------|--------|--------------|-----|
| imsi                        | Номер IMSI абонента.                                                                                       | string | -            | C   |
| guti                        | Глобальный уникальный временный идентификатор абонента.                                                    | string | -            | C   |
| imeisv                      | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | -            | C   |
| <a name="select">select</a> | Запрашиваемые поля, разделенные запятой.                                                                   | string | -            | O   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_profile?imsi=25048123456789&select=guti,imeisv
```

#### Параметры ответа ####

| Поле   | Описание                                                                                                   | Тип    | По умолчанию | O/M |
|--------|------------------------------------------------------------------------------------------------------------|--------|--------------|-----|
| imsi   | Номер IMSI абонента.                                                                                       | string | -            | C   |
| guti   | Глобальный уникальный временный идентификатор абонента.                                                    | string | -            | C   |
| imeisv | Номер <abbr title="International Mobile Equipment Identifier and Software Version">IMEISV</abbr> абонента. | string | -            | C   |

**Примечание.** Используемые параметры соответствуют значениям, указанным в запросе в поле [select](#select).

#### Пример ответа ####

```json5
{
  "imsi": "250020123456789",
  "guti": "2500200020111111111",
  "imeisv": "3520990012345612"
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404 Not Found, No such UE` - указанное устройство не найдено;

### <a name="get_gtp_peers">Получить информацию о состоянии GTP-соединений, get_gtp_peers</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_gtp_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_gtp_peers
```

#### Параметры ответа ####

| Поле           | Описание                                                                                                                                           | Тип            | По умолчанию | O/M |
|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------|----------------|--------------|-----|
| gtpPeers       | Перечень соединений.                                                                                                                               | list\<object\> | -            | M   |
| ip             | IP-адрес соединения.                                                                                                                               | ip             | -            | M   |
| version        | Версия протокола GTP.<br>V1/V2.                                                                                                                    | string         | -            | M   |
| restartCounter | Значение счетчика перезапусков. См. [3GPP TS 23.007](https://www.etsi.org/deliver/etsi_ts/123000_123099/123007/17.05.01_60/ts_123007v170501p.pdf). | int            | -            | M   |
| state          | Состояние соединения.<br>connected/disconnected/busy/unknown.                                                                                      | string         | -            | M   |

#### Пример ответа ####

```json5
{
  "gtpPeers": [
    {
      "ip": "127.0.0.1",
      "version":"V2",
      "restartCounter": 1,
      "state": "connected"
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="get_s1_peers">Получить информацию о состоянии базовых станций LTE из хранилища MME, get_s1_peers</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_s1_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_s1_peers
```

### <a name="get_sgs_peers">Получить информацию о состоянии SGs-соединений, get_sgs_peers</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/get_sgs_peers
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/get_sgs_peers
```

#### Параметры ответа ####

| Поле     | Описание                                                      | Тип            | По умолчанию | O/M |
|----------|---------------------------------------------------------------|----------------|--------------|-----|
| sgsPeers | Перечень соединений.                                          | list\<object\> | -            | M   |
| ip       | IP-адрес соединения.                                          | ip             | -            | M   |
| gt       | Глобальный заголовок соединения.                              | string         | -            | M   |
| state    | Состояние соединения.<br>connected/disconnected/busy/unknown. | string         | -            | M   |
| blocked  | Флаг блокировки соединения.                                   | bool           | -            | M   |

#### Пример ответа ####

```json5
{
  "sgsPeers": [
    {
      "ip": "127.0.0.1",
      "gt": "250021234567890",
      "state": "connected",
      "blocked": false
    }
  ]
}
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="reset_metrics">Сбросить текущие значения метрик, reset_metrics</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/reset_metrics
```

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/reset_metrics
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `412 Precondition Failed, Metrics disabled` - запрос не выполнен, поскольку сбор метрик не активирован;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;

### <a name="ue_disable_trace">Деактивировать трассировку на eNodeB для абонента, ue_disable_trace</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_disable_trace?imsi=<imsi>
```

#### Поля запроса ####

| Поле | Описание             | Тип    | По умолчанию | O/M |
|------|----------------------|--------|--------------|-----|
| imsi | Номер IMSI абонента. | string | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_disable_trace?imsi=25048123456789
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `406 Not Acceptable, No trace id data` - запрос не может быть выполнен ввиду отсутствия соответствующей логики; 
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;

### <a name="ue_enable_trace">Активировать трассировку на eNodeB для абонента, ue_enable_trace</a> ###

#### Запрос ####

```curl
HTTP GET <host>:<port>/mme/v1/ue_enable_trace?imsi=<imsi>&TCE_IP=<ip>
```

#### Поля запроса ####

| Поле   | Описание                                                                      | Тип    | По умолчанию | O/M |
|--------|-------------------------------------------------------------------------------|--------|--------------|-----|
| imsi   | Номер IMSI абонента.                                                          | string | -            | M   |
| TCE_IP | IP-адрес узла сбора и хранения файлов трассировки, `Trace Collection Entity`. | ip     | -            | M   |

#### Пример запроса ####

```curl
http://localhost:65535/mme/v1/ue_enable_trace?imsi=25048123456789&TCE_IP=127.0.0.1
```

#### Ответ ####

```text
<status>
```

#### Пример ответа ####

```text
200 OK
```

#### Возможные статусы ####

* `200 OK` - успешное выполнение запроса;
* `400 Bad Request, UE already traced` - некорректный запрос: указанное устройство уже отслеживается;
* `400 Bad Request, Trace Collection Entity IP Address empty` - некорректный запрос: не задано значение **TCE_IP**;
* `404_Not_Found, No such UE` - указанное устройство не найдено;
* `500 Internal Server Error, Terminated` - обработка запроса принудительно прервана;
* `500 Internal Server Error, Timeout` - обработка запроса прервана ввиду истечения времени ожидания результата;
* `503 Service Unavailable, No free trace ID` - услуга недоступна ввиду отсутствия свободных логик;
* `503 Service Unavailable, Unable to page UE` - услуга недоступна ввиду невозможности процедуры Paging;