= Пример сборки документации из отдельных файлов
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:revdate: Jun 21, 2023
:revnumber: 1.0.0
:chapter-signifier:
:sectnums:
:sectnumlevels: 5
:experimental:
:icons: font
:toc: auto
:toclevels: 5
:toc-title: Содержание

include::glossary.adoc[]

<<<

include::table_merged_cells.adoc[]

<<<

include::links.adoc[]

<<<