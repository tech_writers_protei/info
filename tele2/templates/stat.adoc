  - id: "{{ service.name }}.max.{{stat.type}}.{{ key }}.{{ stat.metric }}"
    name: "{{ service.name }} Max {{ stat.name }} ({{ key }})"
    state: {{ state }}
    targets:
      - "aliasSub(movingMin(aliasByTags(seriesByTag('Metrics_type=stat', 'name=Statistic.{{ stat.metric }}', 'type={{ stat.type }}', 'key={{ key }}'),'type','key'),'{{ stat.max.duration }}'), 'movingMin\\((.*)\\,.*\\)', '\\1')"
      - "sumSeries(movingAverage(seriesByTag('Metrics_type=stat', 'name=Statistic.Count', 'type={{ stat.type }}'),'{{ stat.max.duration }}'))"
    expression: "{% if stat.max.error is defined %} t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.error }} ? ERROR : {% if stat.max.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.warn }} ? WARN: OK) {% else %} OK {% endif %}{% else %}{% if stat.max.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.warn }} ? WARN: OK) {% else %} OK {% endif %}{% endif %}"
    ttl_state: {{ moira_general_ttl_state }}
    ttl: {{ moira_general_ttl }}
    tags:{% if not stat.stat_critical %} {{ (moira_service_tag + [service.name] + moira_chart_tag + moira_base_tag) | to_json}} {% else %} {{ ( moira_service_tag +[service.name] + moira_chart_tag + moira_critical_tag) | to_json}} {% endif %}

    desc:
      "Service: {{ service.name }}.\n
      Event: {{ stat.name }}.\n\n

      Type: {{ stat.type }}\n
      Key: {{ key }}.\n
      Metric: {{ stat.metric }}.\n
      Duration: {{ stat.max.duration }}.\n\n

      Link to grafana dashboard: {{ grafana_hostport }}/d/{{ service.service_dashboard }}\n
      Impact: {{ stat.impact }}\n
      Description: Value of {{ stat.metric }} ( {{ stat.type }} = {{key}} ) is above the threshold ({% if stat.max.error is defined %} ERROR: {{ stat.max.error }} {%endif%}{%if stat.max.warn is defined %} WARN: {{stat.max.warn}} {% endif %} ) for more than {{ stat.max.duration }}."

            {% endif %} # max
            {% if stat.min is defined %}

  - id: "{{ service.name }}.min.{{stat.type}}.{{ key }}.{{ stat.metric }}"
    name: "{{ service.name }} Min {{ stat.name }} ({{ key }})"
    state: {{ state }}
    targets:
      - "aliasSub(movingMax(aliasByTags(seriesByTag('Metrics_type=stat', 'name=Statistic.{{ stat.metric }}', 'type={{ stat.type }}', 'key={{ key }}'),'type','key'),'{{ stat.min.duration }}'), 'movingMax\\((.*)\\,.*\\)', '\\1')"
      - "sumSeries(movingAverage(seriesByTag('Metrics_type=stat', 'name=Statistic.Count', 'type={{ stat.type }}'),'{{ stat.min.duration }}'))"
    expression: "{% if stat.min.error is defined %} t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.error }} ? ERROR : {% if stat.min.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.warn }} ? WARN: OK) {% else %} OK {% endif %}{% else %}{% if stat.min.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.warn }} ? WARN: OK) {% else %} OK {% endif %}{% endif %}"
    ttl_state: {{ moira_general_ttl_state }}
    ttl: {{ moira_general_ttl }}
    tags:{% if not stat.stat_critical %} {{ (moira_service_tag + [service.name] + moira_chart_tag + moira_base_tag) | to_json}} {% else %} {{ (moira_service_tag + [service.name] + moira_chart_tag + moira_critical_tag) | to_json}} {% endif %}

    desc:
      "Service: {{ service.name }}.\n
      Event:{{ stat.name }}.\n\n

      Type: {{ stat.type }}.\n
      Key: {{ key }}.\n
      Metric: {{ stat.metric }}.\n
      Duration: {{ stat.min.duration }}.\n\n

      Link to grafana dashboard: {{ grafana_hostport }}/d/{{ service.service_dashboard }}\n
      Impact: {{ stat.impact }}\n
      Description: Value of {{ stat.metric }} ( {{ stat.type }} = {{key}} ) is below the threshold ({% if stat.min.error is defined %} ERROR: {{ stat.min.error }} {%endif%}{%if stat.min.warn is defined %} WARN: {{stat.min.warn}} {% endif %} ) for more than {{ stat.min.duration }}."

            {% endif %} # min
          {% endfor %} # keys
        {% else %}
          {% if stat.max is defined %}

  - id: "{{ service.name }}.max.{{stat.type}}.{{ stat.metric }}"
    name: "{{ service.name }} Max {{ stat.name }}"
    state: {{ state }}
    targets:
      - "aliasSub(movingMin(aliasByTags(seriesByTag('Metrics_type=stat', 'name=Statistic.{{ stat.metric }}', 'type={{ stat.type }}'),'type','key'),'{{ stat.max.duration }}'), 'movingMin\\((.*)\\,.*\\)', '\\1')"
      - "sumSeries(movingAverage(seriesByTag('Metrics_type=stat', 'name=Statistic.Count', 'type={{ stat.type }}'),'{{ stat.max.duration }}'))"
    expression: "{% if stat.max.error is defined %} t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.error }} ? ERROR : {% if stat.max.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.warn }} ? WARN: OK) {% else %} OK {% endif %}{% else %}{% if stat.max.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 >= {{ stat.max.warn }} ? WARN: OK) {% else %} OK {% endif %}{% endif %}"
    ttl_state: {{ moira_general_ttl_state }}
    ttl: {{ moira_general_ttl }}
    tags:{% if not stat.stat_critical %} {{ (moira_service_tag + [service.name] + moira_chart_tag + moira_base_tag) | to_json}} {% else %} {{ (moira_service_tag + [service.name] + moira_chart_tag + moira_critical_tag) | to_json}} {% endif %}

    desc:
      "Service: {{ service.name }}.\n
      Event:{{ stat.name }}.\n\n

      Type: {{ stat.type }}.\n
      Metric: {{ stat.metric }}.\n
      Duration: {{ stat.max.duration }}.\n\n

      Link to grafana dashboard: {{ grafana_hostport }}/d/{{ service.service_dashboard }}\n
      Impact: {{ stat.impact }}\n
      Description: Value of {{ stat.metric }} ( {{ stat.type }} ) is above the threshold ({% if stat.max.error is defined %} ERROR: {{ stat.max.error }} {%endif%}{%if stat.max.warn is defined %} WARN: {{stat.max.warn}} {% endif %} ) for more than {{ stat.max.duration }}."

          {% endif %} # max
          {% if stat.min is defined %}

  - id: "{{ service.name }}.min.{{stat.type}}.{{ stat.metric }}"
    name: "{{ service.name }} Min {{ stat.name }}"
    state: {{ state }}
    targets:
    targets:
      - "aliasSub(movingMax(aliasByTags(seriesByTag('Metrics_type=stat', 'name=Statistic.{{ stat.metric }}', 'type={{ stat.type }}'),'type','key'),'{{ stat.min.duration }}'), 'movingMax\\((.*)\\,.*\\)', '\\1')"
      - "sumSeries(movingAverage(seriesByTag('Metrics_type=stat', 'name=Statistic.Count', 'type={{ stat.type }}'),'{{ stat.min.duration }}'))"
    expression: "{% if stat.min.error is defined %} t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.error }} ? ERROR : {% if stat.min.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.warn }} ? WARN: OK) {% else %} OK {% endif %}{% else %}{% if stat.min.warn is defined %} ( t2 >= {{ stat.when_total_above|default('-9999999') }} && t1 <= {{ stat.min.warn }} ? WARN: OK) {% else %} OK {% endif %}{% endif %}"
    ttl_state: {{ moira_general_ttl_state }}
    ttl: {{ moira_general_ttl }}
    tags:{% if not stat.stat_critical %} {{ ( moira_service_tag +[service.name] + moira_chart_tag + moira_base_tag) | to_json}} {% else %} {{ ( moira_service_tag +[service.name] + moira_chart_tag + moira_critical_tag) | to_json}} {% endif %}

    desc:
      "Service: {{ service.name }}.\n
      Event:{{ stat.name }}.\n\n

      Type: {{ stat.type }}.\n
      Metric: {{ stat.metric }}.\n
      Duration: {{ stat.min.duration }}.\n\n

      Link to grafana dashboard: {{ grafana_hostport }}/d/{{ service.service_dashboard }}\n
      Impact: {{ stat.impact }}\n
      Description: Value of {{ stat.metric }} ( {{ stat.type }} ) is below the threshold ({% if stat.min.error is defined %} ERROR: {{ stat.min.error }} {%endif%}{%if stat.min.warn is defined %} WARN: {{stat.min.warn}} {% endif %} ) for more than {{ stat.min.duration }}."