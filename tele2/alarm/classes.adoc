---
title: "Операции с объектами мониторинга"
description: "Описание запросов API для операций с объектами мониторинга"
weight: 35
type: docs
draft: true
---
// tag::external-system-pdf[]
.Параметры внешней системы, externalSystem
[options="header",cols="4,16,2,1"]
|===
|Поле |Описание |Тип |O/M

// tag::external-system-html[]
4+|{

|{spacex1}id
|Идентификатор внешней системы.
|int
|M

|{spacex1}name
|Имя внешней системы.
|string
|M

|{spacex1}url
|Ссылка на внешнюю систему.
|string
|M

4+|}
// end::external-system-html[]
|===

// end::external-system-pdf[]
