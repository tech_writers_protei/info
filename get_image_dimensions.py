from decimal import Decimal
from glob import iglob
from os import sep
from re import compile, Pattern, search, Match, findall, finditer, IGNORECASE

from PIL import Image

_pattern: Pattern = compile(r"image:(.*)(?=\.png|\.jpg|\.jpeg|\.svg)\.\w+\[([^]]*)]", IGNORECASE)
_extensions: tuple[str, ...] = ("png", "jpg", "jpeg", "svg")
_default_height: int = 30


def get_image_storage(basepath: str) -> dict[str, str]:
    _image_storage: dict[str, str] = dict()

    for item in iglob("**/*", root_dir=basepath, recursive=True):
        if item.rsplit(".", 1)[-1].lower() in _extensions:
            name: str = item.rsplit(sep, 1)[-1].rsplit(".", 1)[0]
            path: str = f"{basepath}{sep}{item}"
            _image_storage[name] = path

    return _image_storage


def resize_image(path: str, h: int) -> tuple[int, int]:
    if h == -1:
        h: int = _default_height

    with Image.open(path) as image:
        image.load()

    width, height = image.size
    ratio: Decimal = Decimal(width / height).quantize(Decimal("1.0000"))
    return int(ratio * h), h


def get_asciidoc_files(dirpath: str) -> list[str]:
    return [f"{dirpath}{sep}{file_path}" for file_path in iglob("**/*.adoc", root_dir=dirpath, recursive=True)]


def parse_attributes(attributes: str, substring: str) -> int:
    if substring not in attributes:
        return -1

    for _sub in attributes.split(","):
        if not _sub.startswith(substring):
            continue
        else:
            return int(_sub.removeprefix(substring))


def get_links(file_path: str):
    with open(file_path, "r", encoding="utf8") as f:
        _content: list[str] = f.readlines()

    _dict_links: dict[int, list[tuple[str, int | None]]] = dict()

    for index, line in enumerate(_content):
        _start: int = line.find("image:")

        if _start == -1 or line[_start + 6] == ":":
            continue

        for _m in finditer(_pattern, line):
            if "width" in _m.group(2):
                continue

            print(_m.group(2))
            path: str = _m.group(1)
            h: int = parse_attributes(_m.group(2), "height=")

            if index not in _dict_links:
                _dict_links[index] = []

            _dict_links[index].append((path, h))

    return _dict_links


if __name__ == '__main__':
    _path: str = r"C:\Users\tarasov-a\PycharmProjects\Globus_PASS\content\common\web\main_page.adoc"
    print(get_links(_path))
