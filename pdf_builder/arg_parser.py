# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, Namespace


def parse():
    parser: ArgumentParser = ArgumentParser(
        prog='PDF builder')
    parser.add_argument(
        '--project-dir',
        help="Path to project dir")
    parser.add_argument(
        '--temp-dir',
        help="Path to temp dir")
    parser.add_argument(
        '--shared-dirs',
        help="Path to shared docs dir")
    parser.add_argument(
        '--config',
        help="Project config file")
    parser.add_argument(
        '--pdf-config',
        help="PDF config file with projects settings")
    parser.add_argument(
        '--theme',
        help="PDF theme file")
    parser.add_argument(
        '--processes',
        help="Max count of processes")

    try:
        args: Namespace = parser.parse_args()
    except ArgumentError as e:
        print(f"Invalid argument: {e.argument_name}\n{e.message}")
        raise
    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        raise
    else:
        return args
