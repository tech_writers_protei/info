# -*- coding: utf-8 -*-
from pathlib import Path
from typing import NamedTuple

from icecream import ic
from yaml import safe_dump, safe_load


class Settings(NamedTuple):
    title_page: str
    doctype: str = "book"
    toc: bool = True
    figure_caption: str = "Рисунок"
    chapter_signifier: bool = False
    toc_title: str = "Содержание"
    outlinelevels: int = 2
    sectnumlevels: int = 3
    title_logo_image: str = "images/logo.svg[top=4%,align=right,pdfwidth=3cm]"
    title_logo_image_en: str = "images/logo_en.svg[top=4%,align=right,pdfwidth=3cm]"
    break_section: bool = False
    version: str = "1.0.0"

    @classmethod
    def from_file(cls, content: dict[str, str | int | bool]):
        if "title-page" not in content:
            raise KeyError

        _fields: dict[str, str] = {field.replace("_", "-"): field for field in cls._fields}

        _values: dict[str, str | int | bool] = dict()
        for k, v in _fields.items():
            _values[v] = content.get(k, cls._field_defaults.get(v))

        return cls(**_values)

    def as_dict(self):
        return {_field.replace("_", "-"): getattr(self, _field) for _field in self._fields}

    def __str__(self):
        _: dict[str, dict[str, str | int | bool]] = {"settings": self.as_dict()}
        return safe_dump(_, indent=2, allow_unicode=True, sort_keys=False)

    __repr__ = __str__


class Title(NamedTuple):
    value: str | None = None
    level: int | None = None
    title_files: bool | None = None

    def as_dict(self) -> dict[str, str | int]:
        return {
            _field.replace("_", "-"): getattr(self, _field)
            for _field in self._fields if getattr(self, _field) is not None}

    def __str__(self):
        _: dict[str, dict[str, str | int]] = {"title": self.as_dict()}
        return safe_dump(_, indent=2, allow_unicode=True, sort_keys=False)

    __repr__ = __str__

    @classmethod
    def from_dict(cls, content: dict[str, str | int | bool] = None):
        if content is None:
            return None

        _fields: dict[str, str] = {field.replace("_", "-"): field for field in cls._fields}
        _values: dict[str, str | int | bool] = dict()

        for k, v in _fields.items():
            _values[v] = content.get(k, cls._field_defaults.get(v))

        return cls(**_values)


class Chapter(NamedTuple):
    name: str
    title: Title | None = None
    index: list[str] | None = None
    files: list[str] | None = None

    def as_dict(self) -> dict[str, str | dict | list[str]]:
        _dict = dict()

        if self.title is not None:
            _dict["title"] = self.title.as_dict()

        if self.index is not None:
            _dict["index"] = self.index

        if self.files is not None:
            _dict["files"] = self.files

        return _dict

    def __str__(self):
        _: dict[str, list[str] | Title | None] = {self.name: self.as_dict()}
        return safe_dump(_, indent=2, allow_unicode=True, sort_keys=False)

    __repr__ = __str__

    @classmethod
    def from_dict(cls, content: dict[str, dict[str, list[str] | dict[str, str | int | bool]]], name: str):
        _: dict[str, list[str] | dict[str, str | int | bool]] = content.get(name)

        title: Title | None = Title.from_dict(_.get("title", cls._field_defaults.get("title")))
        index: list[str] = _.get("index", cls._field_defaults.get("index"))
        files: list[str] = _.get("files", cls._field_defaults.get("files"))

        return cls(name, title, index, files)


class ConfigFile(NamedTuple):
    path: str | Path
    settings: Settings
    chapters: list[Chapter] = []

    @classmethod
    def from_yaml(cls, path: str | Path):
        with open(path, "r") as f:
            _ = safe_load(f)

        if "settings" not in _:
            raise KeyError

        settings: Settings = Settings.from_file(_.get("settings"))
        chapters: list[Chapter] = [Chapter.from_dict(_, k) for k in _.keys() if k != "settings"]

        return cls(path, settings, chapters)

    def __iter__(self):
        return iter(self.chapters)

    def __len__(self):
        return len(self.chapters)

    @property
    def chapter_dict(self) -> dict[str, Chapter]:
        return {chapter.name: chapter for chapter in iter(self)}

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self.chapter_dict:
                return self.chapter_dict.get(item, None)
            else:
                raise KeyError

        elif isinstance(item, int):
            if 0 <= item < len(self):
                return self.chapters[item]
            else:
                raise IndexError

        else:
            raise TypeError

    def __str__(self):
        _chapters = {c.name: c.as_dict() for c in self.chapter_dict.values()}
        _values = {"settings": self.settings.as_dict(), **_chapters}

        return safe_dump(_values, indent=2, allow_unicode=True, sort_keys=False)


if __name__ == '__main__':
    _path = "PDF_Protei_MME.yml"
    config_file: ConfigFile = ConfigFile.from_yaml(_path)
    print(str(config_file))
