# -*- coding: utf-8 -*-
from os import environ
from pathlib import Path
from re import Match, search, sub
from shutil import copy2
from subprocess import CalledProcessError, PIPE, STDOUT, run
from sys import exit
from typing import Mapping

from frontmatter import load

_TEMP_DIR: str = "PDF_Build/"
_IMAGES_DIR: str = "PDF_Build/images"

_PATTERN_SHARED: str = r"(?:{{|\{\{)<\s?getcontent\spath=[\"\']([^\"\']+)[\"\']\s?>}}"

_ABBR_FIX: dict[str, str] = {
    "<abbr (?:title|id)=\"[^\"]+\">": "",
    "</abbr>": ""
}
_BR_FIX: dict[str, str] = {
    "pass:q[<br>]": " +\n",
    "pass:q[&lt;br&gt;]": " +\n"
}
_BREAK_FIX: dict[str, str] = {
    "<div style=\"page-break-after: always;\"></div>": "\n<<<\n"
}
_INTERNAL_USE_FIX: dict[str, str] = {
    r"\{\{\s?<\s?internal_use\s?>\s?\}\}[\s\S]*?\{\{\s?<\s?\/\s?internal_use\s?>\s?\}\}": ""
}
_ANCHORS_FIX: dict[str, str] = {
    r"<a name=\"([^\"]+)\">([^<]+)</a>": r"[[\1]]\2",
    r"^(\s*[=#]+)\s+([^\{]+)\s*\{#(.*)}.*$": r"[[\3]]\n\1 \2"
}
_LISTS_FIX: dict[str, str] = {
    r"^\*\s(.*)$": r"\n\*\s\1$"
}
_EMPTY_FIX: dict[str, str] = {
    r"\n{2,}": r"\n"
}
_LINKS_FIX: dict[str, str] = {
    r"<<(?:\.\./)([^#]*)/?(#[^,]*),([^>]*)>>": r"<<\1/\2,\3>>",
    r"<<(?:\.\./)([^#,]*),([^>]*)>>": r"<<\1,\2>>",
    r"../([^\[]*)": r"\1"
}
_MERMAID_FIX: dict[str, str] = {
    r"\{\{\s?<\s?mermaid\s?>\s?\}\}": r"\[source,mermaid\]\n----",
    r"\{\{\s?<\s?/mermaid\s?>\s?\}\}": r"----"
}
_SUB_FIX: dict[str, str] = {
    r"</?sub>": "^"
}
_SUP_FIX: dict[str, str] = {
    r"</?sup>": "~"
}


def convert_path(path: str | Path, base_dir: str | Path):
    return str(Path(path).resolve().relative_to(base_dir, walk_up=True)).replace("/", "_")


def run_command(command: str):
    env = environ.copy()

    try:
        run(
            command,
            env=env,
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
            universal_newlines=True,
            check=True
        )

    except CalledProcessError as e:
        print(f"ERROR: {e.stdout}")
        exit(1)


def multiple_sub(line: str, changes: Mapping[str, str] = None) -> str:
    for k, v in changes.items():
        line = sub(k, v, line)
    return line


def fix_images(line: str):
    def repl(m: Match):
        return f"{m.group(1)}{convert_path(m.group(2), _IMAGES_DIR)}[{m.group(3)}]"

    line: str = sub(r"(image::?)([^\[]*)\[([^]]*)]", repl, line)
    return line


def get_shared_data(line: str):
    _m: Match = search(_PATTERN_SHARED, line)
    if _m:
        _path: str = _m.group(1)
        with open(_path, "r") as f:
            _lines: str = f.read()
        return load(_lines).content


def filenames(project_dir, temp_dir, file):
    # content/common/config/apn_rules.md -> content/common/config/
    dirname: str = f"{Path(file).parent}/"
    src_dir: str = str(Path(project_dir).joinpath(dirname))
    dst_dir: str = str(Path(temp_dir).joinpath(dirname))
    # content/common/config/apn_rules.md -> apn_rules
    filename: str = Path(file).stem
    adoc_filename: str = f"{filename}.adoc" + '.adoc'
    md_filename: str = f"{filename}.md"
    full_src_path: str = str(Path(project_dir).joinpath(file))
    full_dst_path: str = str(Path(temp_dir).joinpath(file))
    return {
        'src_dir': src_dir,
        'dst_dir': dst_dir,
        'filename': filename,
        'adoc_filename': adoc_filename,
        'md_filename': md_filename,
        'full_src_path': full_src_path,
        'full_dst_path': full_dst_path
    }


def copy_file(path: str | Path):
    if isinstance(path, Path):
        path: Path = Path(path).resolve()
    return copy2(path, _TEMP_DIR)


def copy_image(path: str | Path):
    if isinstance(path, Path):
        path: Path = Path(path).resolve()
    return copy2(path, _IMAGES_DIR)
