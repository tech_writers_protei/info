# -*- coding: utf-8 -*-
from pathlib import Path

from pdf_builder.config_file import Chapter, ConfigFile
from pdf_builder.sub_text_file import AsciiDocFile
from pdf_builder.const import _ABBR_FIX, _BR_FIX, _IMAGES_DIR, _INTERNAL_USE_FIX, _MERMAID_FIX


class GeneralAsciiDoc(AsciiDocFile):
    def __init__(self, config_file: ConfigFile):
        super().__init__("PDF_Build", "output.pdf")
        self._config_file: ConfigFile = config_file
        self.add_attribute("imagesdir", f"{_IMAGES_DIR}")
        self._last_level: int = 0

    def add_parameters(self):
        with open("ascii_doc_parameters.adoc", "r") as f:
            _content: str = f.read()

        self._content.insert(0, _content)

    def add_include(self, file: str | Path, leveloffset: int, break_flag: bool = False):
        self.empty()

        if break_flag:
            self + "<<<\n\n"

        self.add_attribute("leveloffset", f"+{leveloffset}")
        self.empty()
        self + f"include::{file}[]"
        self.empty()
        self.add_attribute("leveloffset", f"-{leveloffset}")

    def fix(self, **kwargs):
        kwargs: dict[str, str] = {
            **_INTERNAL_USE_FIX,
            **_ABBR_FIX,
            **_BR_FIX,
            **_MERMAID_FIX}
        super().fix(**kwargs)

    def add_settings(self):
        for k, v in self._config_file.settings.as_dict().items():
            self.add_attribute(k, v)

    # FIXME
    def build(self, chapter: Chapter):
        if chapter.index is not None:
            _file = chapter.index[0]
