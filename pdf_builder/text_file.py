# -*- coding: utf-8 -*-
from pathlib import Path
from re import Match, search, sub
from shutil import copyfile
from typing import Mapping

from frontmatter import load, loads

from const import _PATTERN_SHARED, _TEMP_DIR, convert_path


class TextFile:
    def __init__(self, project_dir: str | Path, path: str | Path):
        self._project_dir: Path = Path(project_dir).resolve()
        self._path: Path = Path(path)
        self._content: list[str] = []
        self._title: str | None = None

    @property
    def full_path(self):
        return self._project_dir.joinpath(self._path)

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, int):
            try:
                _: str = self._content[item]
            except IndexError:
                print(f"Index {item} exceeds the length of file {self._path}")
                raise
            else:
                return _

        elif isinstance(item, slice):
            return self._content[item]

        else:
            print(f"Key {item} must be int or slice, but {type(item)} received")
            raise KeyError

    def read(self):
        try:
            with open(self._path, "r") as f:
                self._content = f.readlines()

        except RuntimeError as e:
            print(f"{e.__class__.__name__}: {e.args}")

        except OSError as e:
            print(f"{e.__class__.__name__}: {e.strerror}")

    def write(self):
        try:
            with open(self._path, "w") as f:
                f.write(str(self))

        except RuntimeError as e:
            print(f"{e.__class__.__name__}: {e.args}")

        except OSError as e:
            print(f"{e.__class__.__name__}: {e.strerror}")

    @property
    def content_index(self) -> int:
        indexes: list[int] = [
            index for index, line in enumerate(iter(self))
            if line.removeprefix("\n") == "---"]
        return max(indexes) + 1

    def set_title(self):
        front_matter: dict[str, object] = load(f"{self._path}").to_dict()
        self._title = front_matter.get("title")

    def duplicate(self, dest_dir: str | Path, file_name: str = str()):
        if file_name is None:
            file_name: str = ""

        Path(dest_dir).mkdir(parents=True, exist_ok=True)
        copyfile(self._path, Path(dest_dir).joinpath(file_name))

    @property
    def dir_name(self) -> Path:
        return self._path.parent

    def names(self):
        src_dir: Path = Path(self._project_dir).joinpath(self.dir_name)
        dst_dir: Path = Path(_TEMP_DIR).joinpath(self.dir_name)
        filename: str = self._path.stem
        md_filename: Path = self._path.with_suffix(".md")
        adoc_filename: Path = self._path.with_suffix(".adoc")
        full_src_path: Path = self._project_dir.joinpath(self._path)
        full_dst_path: Path = Path(_TEMP_DIR).joinpath(self._path)

        return {
            'src_dir': src_dir,
            'dst_dir': dst_dir,
            'filename': filename,
            'adoc_filename': adoc_filename,
            'md_filename': md_filename,
            'full_src_path': full_src_path,
            'full_dst_path': full_dst_path
        }

    def multiple_sub(self, index: int, line: str, changes: Mapping[str, str] = None):
        for k, v in changes.items():
            line = sub(k, v, line)
        self._content[index] = line
        return self

    def fix(self, **kwargs):
        for index, line in enumerate(iter(self)):
            self.get_shared_data(index, line).multiple_sub(index, line, **kwargs)
        return self

    def get_shared_data(self, index: int, line: str):
        _m: Match = search(_PATTERN_SHARED, line)

        if _m:
            _path: str = _m.group(1)

            with open(_path, "r") as f:
                _lines: str = f.read()

            self._content[index] = load(_lines).content
        return self

    def fix_images(self, index: int, line: str):
        def repl(m: Match):
            return f"{m.group(1)}{convert_path(m.group(2), _TEMP_DIR)}[{m.group(3)}]"

        line: str = sub(r"(image:+)([^\[]*)\[([^]]*)]", repl, line)
        self._content[index] = line
        return self

    def empty(self):
        self + "\n"

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        elif hasattr(other, "__str__"):
            self._content.append(f"{other}")
        else:
            return

    def remove_front_matter(self):
        _content: str = "".join(self._content)
        self._content: list[str] = loads(_content).content.splitlines(keepends=True)
