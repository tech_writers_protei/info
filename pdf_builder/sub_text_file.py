# -*- coding: utf-8 -*-
from pathlib import Path
from re import Match, finditer, match
from typing import Any
from shutil import copyfile

from pdf_builder.const import _ABBR_FIX, _ANCHORS_FIX, _BREAK_FIX, _BR_FIX, _IMAGES_DIR, _INTERNAL_USE_FIX, \
    _LINKS_FIX, _MERMAID_FIX, _SUB_FIX, _SUP_FIX, run_command
from pdf_builder.text_file import TextFile


def get_prefix(line: str, sym: str = "="):
    _m: Match = match(rf"^({sym}+).*", line)
    if _m:
        return len(_m.group(1))
    else:
        return -1


class AsciiDocFile(TextFile):
    def level(self):
        lines: list[int] = [get_prefix(line) for line in iter(self) if line.startswith("=")]
        return min(filter(lambda x: x != -1, lines))

    def add_title(self):
        self.set_title()
        self._content.insert(0, f"== {self._title}\n")

    @property
    def attributes(self):
        return [
            index for index, line in enumerate(iter(self))
            if line.startswith((":", "ifdef::", "ifndef::", "ifeval::"))]

    @property
    def imagesdir(self):
        for index in iter(self.attributes):
            line: str = self[index]

            if ":imagesdir:" in line:
                return line.removeprefix("ifndef::imagesdir[").removeprefix(":imagesdir: ").removesuffix("]")

        else:
            return "."

    def convert_to_pdf(self, theme: str | Path, output: str | Path):
        pdf_name: Path = self.full_path.with_suffix(".pdf")
        print(f'Convert to {pdf_name}')

        command: str = (
            f'asciidoctor-pdf -a numbered -r asciidoctor-diagram -a skip-front-matter '
            f'--theme {theme} -o "{pdf_name}" "{output}" && chmod 777 {pdf_name}')

        run_command(command)

    def add_attribute(self, attribute: str, value: Any = None):
        if value is None or value is True:
            _str: str = f":{attribute}:"
        elif value is False:
            _str: str = f":!{attribute}:"
        else:
            _str: str = f":{attribute}: {value}"

        self._content.insert(self.content_index, f"{_str}")

    def copy_images(self):
        for line in iter(self):
            for m in finditer(r"image:+([^\[]+)", line):
                _path: Path = Path(m.group(1)).resolve()
                _: Path = self._project_dir.relative_to(_path)
                _name: str = f'PDF_Build_{str(_).replace("/", "_")}'
                src: Path = self.full_path.joinpath(self.imagesdir).joinpath(_path)
                dst: Path = Path("PDF_Build/images").joinpath(_name)
                copyfile(src, dst)


class MarkdownFile(TextFile):
    def convert_to_asciidoc(self):
        path: Path = self.full_path.with_suffix(".adoc")
        print(f'Convert {self.full_path} to\n\t{path}')
        command: str = f"pandoc -f markdown -t asciidoc --wrap=none -f markdown-smart -i {self.full_path} -o {path}"
        run_command(command)
        return AsciiDocFile(self._project_dir, path)

    def fix(self):
        kwargs = {
            **_BREAK_FIX,
            **_INTERNAL_USE_FIX,
            **_ANCHORS_FIX,
            **_ABBR_FIX,
            **_BR_FIX,
            **_LINKS_FIX,
            **_SUB_FIX,
            **_SUP_FIX}
        super().fix(**kwargs)
