== Параметры и их типы
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:revdate: Jun 1, 2023
:revnumber: 1.0.0
:experimental:
:icons: font

* string:
** MSISDN;
** IMSI;
** IMEI;
** MSC_Address;
** SGSN_Address;
** VLR_Address;
** HLR_Address;
** GT;
** Origin-Host;
** Origin-Realm;
** gsmSCF;
** SCA;
** ContextType;
** UE-APN-OI-Rep;
** PLMN_Id;
** ChargingCharacteristics;
** RSD;
** IMPI;
** IMPU;
** CellGlobalId;
** SAI;
** LAI;
** TAI;
** PDP_Context;
** TEID;

* int:
** MCC;
** MNC;
** ServiceKey;
** OpCode;
** NPI;
** TON;
** ErrorCode;
** OPC;
** DPC;
** SIC;
** NA;
** RC;
** NI;
** TT;
** Supported-Vendor-Id;
** Inband-Security-Id;
** Auth-Application-Id;
** Acct-Application-Id;
** Origin-State-Id;
** Firmware-Revision;
** Vendor-Id;
** cugIndicator;
** basicService;
** teleservice;
** QoS_Id;
** HSM_Id;
** AgeOfLocation;
** CSG_Id;
** ZoneCode;
** PDN_Type;
** ODB;
** QCI;

* hex:
** OTID;
** DTID;
** SSN;

* int/hex:
** LAC;
** CellId;
** SS-Code;

pass:q[<abbr title="Test">Test</abbr>]

pass:q[<div style="page-break-after: always;"></div>]