python3 -m pip install --upgrade pyinstaller
pyinstaller --noconfirm --onefile --console --name "repair" --distpath "../bin" \
--add-data "../LICENSE:." \
--add-data "../MANIFEST.in:." \
--add-data "../pyproject.toml:." \
--add-data "../README.adoc:." \
--add-data "../requirements.txt:." \
--collect-binaries "." \
--paths "." \
--hidden-import "loguru" "./main.py"
rm repair.spec
rm build