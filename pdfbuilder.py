#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, glob
import shutil
import argparse
import re
import yaml, json
import base64
from subprocess import Popen, PIPE, STDOUT, call
from pathlib import Path
from copy import copy

#Autoinstall frontmatter module
try:
    import importlib
    importlib.import_module('frontmatter')
except ImportError:
    import pip
    pip.main(['install', 'python-frontmatter'])
finally:
    globals()['frontmatter'] = importlib.import_module('frontmatter')

#Autoinstall pyvips module
try:
    import importlib
    importlib.import_module('pyvips')
except ImportError:
    import pip
    pip.main(['install', 'pyvips'])
finally:
    import pyvips

#Autoinstall multiprocessing
try:
    from multiprocessing import Pool
except ImportError:
    import pip
    pip.main(['install', 'multiprocessing'])
finally:
    from multiprocessing import Pool


def getTitle(file):
    result = {}
    data = frontmatter.load(file)
    try:
        result['title'] = data['title']
    except Exception:
        result['title'] = ''
    try:
        result['description'] = data['description']
    except Exception:
        result['description'] = ''
    return result


def removeFrontMatter(file):
    data = frontmatter.load(file)
    content_without_metadata = data.content
    return content_without_metadata


def makeDir(dir):
    try:
        Path(dir).mkdir(parents=True, exist_ok=True)
    except Exception as e:
        print(e)


def copyFile(src, dst, filename=''):
    try:
        makeDir(dst)
        shutil.copyfile(src, dst + filename)
    except Exception as e:
        print(e)
        return False
    return True


def splitFilenames(project_dir, temp_dir, file):
    # content/common/config/apn_rules.md -> content/common/config/
    dirname = '/'.join(file.split('/')[0:-1])+'/'
    src_dir = project_dir + '/' + dirname
    dst_dir = temp_dir + '/' + dirname
    # content/common/config/apn_rules.md -> apn_rules
    filename = '.'.join(os.path.basename(file).split('.')[0:-1])
    adoc_filename = filename + '.adoc'
    md_filename = filename + '.md'
    full_src_path = project_dir + '/' + file
    full_dst_path = temp_dir + '/' + file
    print(f'Full_src_path: {full_src_path}')
    print(f'Full_dst_path: {full_dst_path}')
    print(f'src_dir: {src_dir}')
    print(f'dst_dir: {dst_dir}')

    filenames = {
        'src_dir': src_dir,
        'dst_dir': dst_dir,
        'filename': filename,
        'adoc_filename': adoc_filename,
        'md_filename': md_filename,
        'full_src_path': full_src_path,
        'full_dst_path': full_dst_path
    }
    return filenames


def prepareTitle(break_flag, title, level, outlinelevels='', anchor_file=''):
    if title != '':
        anchor = re.sub(r'\s*\b(\d+\w*)', '', str(title.lower().strip()))
        anchor = re.sub(r'[^\w\n]', '-', anchor)
        anchor = re.sub(r'\-{2,}', '-', anchor)
        anchor = anchor.strip('-')
        if break_flag is True:
            anchor = '\n<<<\n[['+anchor+']]\n'
        else:
            anchor = '\n[['+anchor+']]\n'
        result = anchor + anchor_file + outlinelevels + '\n' + level * '=' + ' ' + title + '\n'
    else:
        result = anchor_file + '\n'
    return result


def callProcess(command):
    my_env = os.environ.copy()
    my_env["UID"] = str(os.getuid())
    popen_object = Popen(command, env=my_env, shell=True, stdout=PIPE, stderr=STDOUT, universal_newlines=True)
    stdout, stderr = popen_object.communicate()
    stdout = stdout.strip()
    if popen_object.returncode != 0:
        print("ERROR: %s" % stdout.strip())
        sys.exit(1)


def changeContent(data, pattern, target):
    data = pattern.sub(target, data)
    return data


def convertMarkdownToAsciidoc(dirname, filename):
    md_file = dirname + filename + '.md'
    adoc_file = dirname + filename + '.adoc'
    print(f'Convert {md_file} to\n\t{adoc_file}')
    command = f'docker run --network none -u $UID --rm \
            -v $(pwd):/documents/ git.protei.ru:8443/docker/images/hugo \
            /bin/bash -c "pandoc \
            -f markdown \
            -t asciidoc \
            --wrap=none \
            -f markdown-smart \
            -i {md_file} \
            -o {adoc_file}"'
    callProcess(command)
    with open(adoc_file, 'r') as out:
        content = out.read()
    return content


def convertAsciiDocToPDF(output, pdf_name):
    complete_result.append(pdf_name)
    print(f'Convert to {pdf_name}')
    if 'en_' in pdf_name:
        en_theme = args.theme.replace("theme.yml", "theme_en.yml")
        command = f'docker run --network none -u $UID --rm \
                -v $(pwd):/documents/ git.protei.ru:8443/docker/images/hugo \
                /bin/bash -c "asciidoctor-pdf \
                -a numbered \
                -r asciidoctor-diagram \
                --theme {en_theme} \
                "{output}" \
                -o {pdf_name} && \
                chmod 777 {pdf_name}"'
        callProcess(command)
    else:
        command = f'docker run --network none -u $UID --rm \
                -v $(pwd):/documents/ git.protei.ru:8443/docker/images/hugo \
                /bin/bash -c "asciidoctor-pdf \
                -a numbered \
                -r asciidoctor-diagram \
                --theme {args.theme} \
                "{output}" \
                -o {pdf_name} && \
                chmod 777 {pdf_name}"'
        callProcess(command)


def changeChapters(break_flag, content, level, sym='='):
    if break_flag is True:
        break_chapter = '\n<<<\n'
    else:
        break_chapter = ''

    #break_count = False
    break_count = True
    new_content = ""
    for line in content.splitlines():
        all_pattern = r'^([' + sym + ']+)(\s+.*)$'
        result = re.findall(all_pattern, line)
        if result:
            for item in result:
                anchor = re.sub(r'\s*\b(\d+\w*)', '', str(item[1].lower().strip()))
                anchor = re.sub(r'[^\w\n]', '-', anchor)
                anchor = re.sub(r'\-{2,}', '-', anchor)
                anchor = anchor.strip('-')
                anchor = '[['+anchor+']]\n\n'
                header = level * sym + item[0]
                if header.count(sym) >= 6:
                    header = sym * 6
                if break_count is False:
                    line = anchor + header + item[1]
                else:
                    line = break_chapter + anchor + header + item[1]
            break_count = True
        new_content += line + '\n'
    return new_content


def changeBreaks(line):
    if '<div style="page-break-after: always;"></div>' in line:
        line = '<<<'
    return line


def changeAnchors(line):
    '''
    Функция конвертирует якоря из md формата(a name)
    в adoc формат, иначе pandoc проигнорирует этот якорь,
    и вставит только текст описания якоря, без самого якоря
    <a name="id_anchor">Text anchor</a> конвертируется в формат
    [[id_anchor]]Text anchor
    Или ===== Header1 {#idheader1}
        ##### Header2 {#idheader2}
    ^\s*[=#]+?\s+(.+)\s+?\{#(.+)\}
    '''
    all_pattern = r'<a name=[\"\'](.+?)[\'\"]\>+(.*?)<\/a>'
    match = re.finditer(all_pattern, line)
    if match:
        for item in match:
            anchor = str(item[1]).replace('/', '-')
            if item[2]:
                line = line.replace(item[0], f'[[{anchor}]]{item[2]}')
            else:
                line = line.replace(item[0], f'[[{item[1]}]]\n')

    all_pattern = r'^(\s*[=#]+)\s+(.+)\s+?\{#(.+)\}'
    match = re.findall(all_pattern, line)
    if match:
        for item in match:
            anchor = str(item[2]).replace('/', '-')
            line = f'{item[0]} [[{anchor}]]{item[1]}'

    # Переопределение >>, т.к. строка вида list<mapping<int,int>>
    # определяется adoc как ссылка
    # Правки переноса строк (неэкранированное исчезает при построении PDF)
    #content = re.sub(r'<<', '{lt}{lt}', content)
    line = re.sub(r'\\>\\>\\>', '> > >', line)
    #content = re.sub(r'\<br\>', 'pass:q[\<br\>]', content)
    line = re.sub(r'\<br\>', 'pass:q[\<br\>]', line)

    # Временный костыль для MME, изменение ссылки ../images на ./images
    # SVG -> PNG
    line = re.sub(r'\!\[LOGO.png\]\(LOGO.png\)', '', line)

    return line


def changeLists(line):
    '''
    Функция добавляет новую строку перед списком формата markdown
    * Item 1 -> \n* Item1
    т.к. в исходном файле может не быть перевода каретки перед списком, что
    adoc воспринимает как одну сплошную строку
    '''
    all_pattern = r'^[\ ]?[\-\*]{1}\ .+?$'
    match = re.finditer(all_pattern, line)
    if match:
        for item in match:
            line = line.replace(item[0], '\n'+item[0])
    return line


def changeLinks(content):
    '''
    Конвертация ссылок в adoc файлах из
    link:#clear-dns-cache-cli[clear_dns_cache]
    в
    <<#clear-dns-cache-cli,clear_dns_cache>>
    RegExp: https://regex101.com/r/mnZNJc/1
    '''
    all_pattern = r'link:[.#@]?(.+?(?=/?\[))\/?(\[[^\.\s\/]+\]|\[.+?\s?\]|\[.+?\s?::\s?\[.+?\]\s?::\s?.+?\])'
    for line in content.splitlines():
        result = re.finditer(all_pattern, line)
        if result:
            for link in result:
                full_link = link[0]
                id_link = link[1]
                text_link = link[2][1:-1]
                text_link = re.sub(r'\s?::\s?', '::', text_link)
                if '/' in id_link:
                    id_link = id_link.split('/')[-1]
                id_link = re.sub('#|@|/', '', id_link)
                result_link = f'<<{id_link},{text_link}>>'
                content = content.replace(full_link, result_link)
    return content


def getSharedData(content, shared_dirs, level):
    all_pattern = r'({{|\\{\\{)< getcontent path=[\"\'](.+?)[\"\']?\s?>}}'
    n = 0
    while n < 10:
        for item in re.finditer(all_pattern, content):
            if item[2]:
                for dir in shared_dirs:
                    source_shared = '/'.join(dir.split('/')[0:-2])+'/'
                    print(f'Getting shared content from {source_shared}/{item[2]}')
                    data = removeFrontMatter(source_shared + item[2])
                    content = content.replace(item[0], data)
        n += 1
    return '\n'+content


def convertMermaidMarkdown(md_dst_path):
    mermaid_open_pattern = r'\{\{\s?<\s?mermaid\s?>\s?\}\}'
    mermaid_close_pattern = r'\{\{\s?<\s?\/\s?mermaid\s?>\s?\}\}\ ?\;?'
    new_data = ''
    convert_flag = False
    with open(md_dst_path, 'r') as md_file:
        for line in md_file:
            if re.match(mermaid_open_pattern, line):
                convert_flag = True
                line = re.sub(mermaid_open_pattern, '\n```mermaid\n', line)
            if re.match(mermaid_close_pattern, line):
                line = re.sub(mermaid_close_pattern, '\n```\n', line)
            new_data += line
    if convert_flag:
        print(f'Converting mermaid diagrams in {md_dst_path}')
        with open(md_dst_path, 'w') as md_file:
            md_file.write(new_data)
        command = f'docker run --network none --rm \
            -v "$(pwd)":/documents git.protei.ru:8443/docker/images/docs-tools \
            /bin/bash -c "mmdc -p /opt/puppeteer-config.json \
            -i /documents/{md_dst_path} \
            -o /documents/{md_dst_path}"'
        callProcess(command)


def convertMermaidAsciidoc(content):
    mermaid_open_pattern = r'\{\{\s?<\s?mermaid\s?>\s?\}\}'
    mermaid_close_pattern = r'\{\{\s?<\s?\/\s?mermaid\s?>\s?\}\}\ ?\;?'
    new_data = ''
    for line in content.splitlines():
        if re.match(mermaid_open_pattern, line):
            line = re.sub(mermaid_open_pattern, '\n[mermaid]\n----\n\n', line)
        if re.match(mermaid_close_pattern, line):
            line = re.sub(mermaid_close_pattern, '\n\n----\n', line)
        new_data += line + '\n'
    return new_data


def deleteInternalUse(content):
    internaluse_shortcode = r'\{\{\s?<\s?internal_use\s?>\s?\}\}[\s\S]*?\{\{\s?<\s?\/\s?internal_use\s?>\s?\}\}'
    content = re.sub(internaluse_shortcode, "", content)
    return content


def convertSVGtoPNG(filename):
    print(f'Converting {filename} to png')
    name, ext = os.path.splitext(filename)
    png_file = name + '.png'
    with open(filename, "r") as svg_file:
        data = svg_file.read().replace('Text is not SVG - cannot display', '')
    with open(filename, "w") as svg_file:
        svg_file.write(data)
    image = pyvips.Image.new_from_file(filename, dpi=144)
    image.write_to_file(png_file)
    return png_file


def getImages(content, filenames):
    all_pattern = r'image:+(.+?)\[(.+?)\]'
    images_dir_line = ''
    for line in content.splitlines():

        if 'ifndef::' in line:
            future_del_line = line
            images_dir_line = line
            try:
                images_dir_line = images_dir_line.split('ifndef::imagesdir[:imagesdir: ')[1][0:-1]
            except:
                print(f'Error format: {images_dir_line}')
            images_dir_line = images_dir_line + '/'

        result = re.finditer(all_pattern, line)

        if result:
            for link in result:
                if images_dir_line:
                    full_link_first = link[0]
                    full_link = link[0][0:7] + images_dir_line + link[0][7:]
                    id_link = link[2]
                    image_link = images_dir_line + link[1]
                    image_file = image_link.split('/')[-1]
                else:
                    full_link = link[0]
                    id_link = link[2]
                    image_link = link[1]
                    image_file = image_link.split('/')[-1]
                # Так как для Hugo (html) мы добавляем один уровень
                # то здесь его нужно убрать
                # ../../images/image.jpg -> ../images/image.jpg
                if '../' in image_link:
                    image_link = image_link.replace('../', '', 1)
                # Поиск удаление {{< baseurl >}} в adoc
                # {{< baseurl >}}/Mobile/Protei_PGW/common/images/image.svg ->
                # -> common/images/image.svg
                if '%7B%7B%3C%20baseurl%20%3E%7D%7D' in image_link:
                    image_link = image_link.replace('%7B%7B%3C%20baseurl%20%3E%7D%7D/', '')
                    image_link = '/'.join(image_link.split('/')[2:])
                # Для исключения наложений изображений с одинаковыми именами,
                # добавляем в виде префикса путь src с заменой "/" на "_"
                # PDF_Build_Protei_PGW_content_common_basics_life_cycle_LIFECYCLE.svg
                temp_image_name = filenames["dst_dir"].replace('/', '_') + image_file
                if not copyFile(filenames["src_dir"]+image_link, images_temp_dir + '/', temp_image_name):
                    copyFile(filenames["dst_dir"]+image_link, images_temp_dir + '/', temp_image_name)

                if image_file.endswith('.svg'):
                    temp_image_name = convertSVGtoPNG(images_temp_dir + '/' + temp_image_name)
                    result_link = f'image::{temp_image_name}[{id_link}]'
                else:
                    result_link = f'image::{images_temp_dir}/{temp_image_name}[{id_link}]'

                if '::' in full_link and '::' in result_link:
                    pass
                else:
                    result_link = result_link.replace('::', ':')

                content = content.replace(full_link, result_link)

                if 'ifndef::' in content:
                    result_lunk = result_link.replace('::', ':')
                    content = content.replace(full_link_first, result_link)

    if 'ifndef::' in content:
        content = content.replace(future_del_line, '')

    return content


def pdfBuild(config, temp_dir, project_dir, pdf_name, docs_dir='', shared_dirs='', destination=None):
    break_section = 0
    for chapter in config:
        title = ''
        title_files = True
        files = {}
        outlinelevel = ''
        break_chapters = False
        break_flag = False
        break_flag_section = True
        overwrite_title = False

        if chapter == 'settings':
            title_page = config['settings'].get('title-page', 'Protei')
            doctype = config['settings'].get('doctype', 'book')
            toc = config['settings'].get('toc', True)
            if toc:
                toc = ':toc:'
            else:
                toc = ':!toc:'
            figure_caption = config['settings'].get('figure-caption', 'Рисунок')
            chapter_signifier = config['settings'].get('chapter-signifier', False)
            if chapter_signifier:
                chapter_signifier = ':chapter-signifier:'
            else:
                chapter_signifier = ':!chapter-signifier:'
            try:
                if 'en_' in pdf_name:
                    toc_title = config['settings'].get('toc_title', 'Contents')
                else:
                    toc_title = config['settings'].get('toc_title', 'Содержание')
            except:
                toc_title = config['settings'].get('toc_title', 'Содержание')
            toclevels = config['settings'].get('toclevels', 2)
            sectnumlevels = config['settings'].get('sectnumlevels', 3)
            outlinelevels = config['settings'].get('outlinelevels', 2)
            level = outlinelevels
            version = config['settings'].get('version', '1.0.0')
            if 'en_' in pdf_name:
                title_logo_image = config['settings'].get('title-logo-image-en', 'images/logo_en.svg[top=4%,align=right,pdfwidth=3cm]')
            else:
                title_logo_image = config['settings'].get('title-logo-image', 'images/logo.svg[top=4%,align=right,pdfwidth=3cm]')
            break_section = config['settings'].get('break-section', 0)
            continue
        else:
            level = 2

        if 'title' in config[chapter].keys():
            if 'value' in config[chapter]['title'].keys():
                title = config[chapter]['title']['value']
                overwrite_title = True
            if 'break' in config[chapter]['title'].keys():
                break_flag_section = config[chapter]['title'].get('break', False)
            if 'level' in config[chapter]['title'].keys():
                level = config[chapter]['title']['level']
                if 'outlinelevels' in config[chapter]['title'].keys():
                    if config[chapter]['title']['outlinelevels'] is False:
                        outlinelevel = ''
                    else:
                        outlinelevel = f'[outlinelevels={level}]'
                else:
                    outlinelevel = f'[outlinelevels={level}]'
            if 'title-files' in config[chapter]['title'].keys():
                title_files = config[chapter]['title']['title-files']
            if 'break-chapters' in config[chapter]['title'].keys():
                break_chapters = config[chapter]['title']['break-chapters']
            title = prepareTitle(False, title, level, outlinelevel)

        with open(pdf_name+'.adoc', "a") as out:
            if break_section > 0 and break_flag_section is True:
                out.write('\n<<<\n' + title)
            else:
                out.write(title)

        if 'index' in config[chapter]:
            files['index'] = []
            for file in config[chapter]['index']:
                files['index'].append(file)

        if 'files' in config[chapter]:
            files['files'] = []
            for file in config[chapter]['files']:
                files['files'].append(file)

        for key in files.keys():
            for file in files[key]:
                filenames = splitFilenames(project_dir, temp_dir, file)
                makeDir(filenames['dst_dir'])
                # print(json.dumps(filenames, indent=4))
                # Получение title и description из параметров (FrontMatter)
                meta = getTitle(filenames['full_src_path'])
                if overwrite_title:
                    title = ''
                    overwrite_title = False
                else:
                    title = meta['title']
                file_content = removeFrontMatter(filenames['full_src_path'])
                target_content = ""
                # Вставка общих доков
                if shared_dirs:
                    file_content = getSharedData(file_content, shared_dirs, level)
                if filenames['full_src_path'].endswith('.md'):
                    print(f'Change anchors in {file} to adoc format')
                    for line in file_content.splitlines():
                        line = changeAnchors(line)
                        line = changeBreaks(line)
                        # Дополнительные переносы строк у разделительной линии
                        line = changeContent(line, re.compile('^[-]+$'), '\n-------------\n\n')
                        line = changeLists(line)
                        target_content = target_content + '\n' + line
                    target_content = deleteInternalUse(target_content)
                    print(f'Save temp {filenames["full_dst_path"]}')
                    with open(filenames['full_dst_path'], 'w') as dst:
                        dst.write(target_content)
                    convertMermaidMarkdown(filenames['full_dst_path'])
                    file_content = convertMarkdownToAsciidoc(filenames['dst_dir'], filenames['filename'])
                elif filenames['full_src_path'].endswith('.adoc'):
                    file_content = convertMermaidAsciidoc(file_content)

                # Подготовка и конвертация изображений
                print(f'Prepare images for {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                file_content = getImages(file_content, filenames)
                print(f'Change links in {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                file_content = changeLinks(file_content)
                print(f'Change chapters in {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                # Артефакт adoc, преобразовывание экранированной { в обычную
                file_content = changeContent(file_content, re.compile('\\\{'), '{')
                # Установка автоширины для таблиц
                file_content = changeContent(file_content, re.compile('\\[.*(?=,cols)'), '[%autowidth')

                if break_flag is False:
                    break_section_flag = False
                    break_flag = True
                else:
                    if break_section > 1:
                        if level < 3:
                            break_section_flag = True
                        else:
                            break_section_flag = False
                if break_chapters is True:
                    break_chapters_flag = True
                else:
                    break_chapters_flag = False

                # Якоря для _index не нужны
                if '_index' not in str(filenames["adoc_filename"]):
                    # Если /network_arch/index.md , то имя якоря = директории
                    if 'index.' in str(filenames["adoc_filename"]):
                        anchor_file = '/'.join(filenames["full_src_path"].split('/')[-2:-1])
                    else:
                        anchor_file = filenames["filename"]
                    anchor_file = f'\nanchor:{anchor_file}[]\n\n'
                else:
                    anchor_file = ''

                if key == 'index':
                    title = prepareTitle(break_section_flag, title, level, outlinelevel, anchor_file)
                    file_content = changeChapters(break_chapters_flag, file_content, level-1)
                else:
                    title = prepareTitle(break_section_flag, title, level+1, outlinelevel, anchor_file)
                    file_content = changeChapters(break_chapters_flag, file_content, level)

                with open(pdf_name+'.adoc', "a") as out:
                    if title_files:
                        out.write(title)
                    else:
                        out.write('\n\n')
                    out.write(file_content)
                outlinelevel = ''
    try:
        if 'en_' in pdf_name:
            file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Date: {{docdate}}
Version: {version}
:toclevels: {toclevels}
'''
        else:
            file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Дата: {{docdate}}
Версия: {version}
:toclevels: {toclevels}
'''

    except:
        file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Дата: {{docdate}}
Версия: {version}
:toclevels: {toclevels}
'''

    # https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes-ref/#builtin-attributes-i18n
    with open(pdf_name+'.adoc', 'r') as original:
        modified_data = original.read()

    with open(pdf_name+'.adoc', 'w') as modified:
        modified.write(file_content+"\n" + modified_data)
    convertAsciiDocToPDF(pdf_name+'.adoc', pdf_name)

    copyFile(pdf_name, destination + '/', pdf_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='PDF builder')
    parser.add_argument('--project-dir',
        help="Path to project dir")
    parser.add_argument('--temp-dir',
        help="Path to temp dir")
    parser.add_argument('--shared-dirs',
        help="Path to shared docs dir")
    parser.add_argument('--config',
        help="Project config file")
    parser.add_argument('--pdf-config',
        help="PDF config file with projects settings")
    parser.add_argument('--theme',
        help="PDF theme file")
    parser.add_argument('--processes',
        help="Max count of processes")
    args = parser.parse_args()

    images_temp_dir = 'PDF_Build/images'
    max_processes = int(args.processes)
    complete_result = []

    if args.pdf_config:
        try:
            with open(args.pdf_config, 'r') as pdf_conffile:
                projects_config = yaml.load(pdf_conffile, Loader=yaml.FullLoader)
                if projects_config is None:
                    print('PDF settings file is empty')
                    sys.exit(0)
        except Exception as e:
            print(f'Error when opening PDF settings file {args.pdf_config}: "{e}"')
            sys.exit(1)
    elif args.config:
        try:
            with open(args.config, 'r') as conffile:
                config = yaml.load(args.config, Loader=yaml.FullLoader)
        except Exception as e:
            print(f'Error when opening project settings file {args.config}: "{e}"')
            sys.exit(1)
        try:
            project_dir = args.project_dir
        except Exception:
            print('Project (--project-dir) directory is undefined')
            sys.exit(1)
        try:
            temp_dir = 'PDF_Build/' + args.temp_dir
        except Exception:
            print('Temp (--temp-dir) directory is undefined')
            sys.exit(1)
        shared_dirs = []
        try:
            shared_dirs.append(args.shared_dirs)
        except Exception:
            shared_dirs = []

    def process_pdf_build(project_name, projects_config):
        temp_dir = 'PDF_Build/' + project_name
        config = projects_config[project_name].get('pdf_settings_file')
        project_dir = projects_config[project_name].get('project_dir')
        project = project_name.split('/')[-1]
        docs_dir = projects_config[project_name].get('docs_dir')
        destination = projects_config[project_name].get('destination')
        shared_dirs = projects_config[project_name].get('shared_dirs', '')
        pdf_name = projects_config[project_name].get('pdf_name', project)
        pdf_storage = projects_config[project_name].get('pdf_storage', destination)
        with open(config, 'r') as conffile:
            config = yaml.load(conffile, Loader=yaml.FullLoader)
        os.makedirs(pdf_storage, exist_ok=True)
        pdfBuild(config, temp_dir, project_dir, pdf_name, docs_dir, shared_dirs, pdf_storage)
        os.remove(pdf_name+'.adoc')

    if 'projects_config' in locals():
        with Pool(processes=max_processes) as pool:
            args_list = [(project_name, projects_config) for project_name in projects_config]
            result = pool.starmap_async(process_pdf_build, args_list)
            result.get()
        pool.close()
        pool.join()

    else:
        project = project_dir.rstrip('/').split('/')[-1]
        with open(config, 'r') as conffile:
            config = yaml.load(conffile, Loader=yaml.FullLoader)
        docs_dir = ''
        destination = ''
        pdf_name = project
        pdfBuild(config, temp_dir, project_dir, pdf_name+'.pdf', docs_dir, shared_dirs)

    print(complete_result)
