# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any

from frontmatter import load


def capitalize(value: str):
    return f"{value[0].upper()}{value[1:]}"


def decapitalize(value: str):
    return f"{value[0].lower()}{value[1:]}"


class File:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._frontmatter: dict[str, Any] = load(path).to_dict()

    @property
    def frontmatter(self):
        return self._frontmatter

    @frontmatter.setter
    def frontmatter(self, value):
        self._frontmatter = value

    @property
    def parent(self):
        return self._path.parent

    @property
    def title(self):
        return self._frontmatter.get("title")

    @property
    def description(self):
        return decapitalize(self._frontmatter.get("description"))

    def __str__(self):
        return (
            f"* [/{self.parent.name}/{self.title}](./{self.parent.name}/{decapitalize(self.title)}/) "
            f"-- {self.description};")

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.title == other.title
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.title != other.title
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.title < other.title
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.title <= other.title
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.title > other.title
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.title >= other.title
        else:
            return NotImplemented


class FullDescription:
    def __init__(self):
        self._files: list[File] = []
        self._folders: dict[str, list[File]] = dict()

    def __iter__(self):
        return iter(self._files)

    def __add__(self, other):
        if isinstance(other, File):
            self._files.append(other)
        else:
            return

    __iadd__ = __add__
    __radd__ = __add__

    @property
    def files(self):
        return self._files

    @files.setter
    def files(self, value):
        self._files = value

    def set_folders(self):
        _dict: dict[str, list[File]] = dict()

        for file in iter(self):
            name: str = file.parent.name

            if name not in _dict.keys():
                _dict[name] = []

            _dict[name].append(file)

        self._folders = _dict

    def sort(self):
        for values in self._folders.values():
            values.sort()

    def __str__(self):
        _lines: list[str] = []

        for k, values in self._folders.items():
            _lines.extend(str(v) for v in values)

        _lines.sort()
        return "\n".join(_lines)


def main(path: str | Path):
    full_description: FullDescription = FullDescription()

    for item in Path(path).iterdir():
        if item.is_dir() and item.name != "entities":
            for _ in item.iterdir():
                if not _.stem.endswith("index"):
                    file: File = File(_)
                    full_description + file

    full_description.set_folders()
    full_description.sort()

    print(str(full_description))


def add_title(path: str | Path):
    path: Path = Path(path).resolve()

    with open(path, "r", errors="ignore") as f:
        _: str = f.read()

    _text: str = capitalize(path.stem)
    # _line: str = f"[[{_text}]]\n= {_text}\n\n"
    _line: str = f"[[{_text}]]\n"

    with open(path, "w", errors="ignore") as f:
        f.write(f"{_line}{_}")


if __name__ == '__main__':
    # _path: str = "/Users/andrewtarasov/PycharmProjects/hlr_hss/content/common/oam/provisioning/"
    _path: str = r"HLR_HSS_content\common\oam\provisioning\entities"

    for i in Path(_path).iterdir():
        if i.is_file() and not i.stem.endswith("index") and i.suffix == ".adoc":
            add_title(i)
