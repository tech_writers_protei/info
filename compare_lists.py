from pathlib import Path
from re import Match, Pattern, compile, search
from typing import Iterable


def prepare_items(values: Iterable[str]):
    _pattern: Pattern = compile(r"(?:\|\s<a name=\".*\">|\|\s)([^<]*)(?:</a>\s*\||\s*\|)")
    items: set[str] = set()

    for value in values:
        _m: Match = search(_pattern, value)
        if _m:
            items.add(_m.group(1))
        else:
            items.add(value)

    return items


def compare_lists(iter_1: Iterable[str], iter_2: Iterable[str]):
    list_1: set[str] = prepare_items(iter_1)
    list_2: set[str] = prepare_items(iter_2)

    print(list_1.difference(list_2))
    print(list_2.difference(list_1))


if __name__ == '__main__':
    path_1: str = r".\Protei_MME\content\common\config"
    path_2: str = r".\Protei_MME\content\common\config\rules"

    for item in Path(path_1).iterdir():
        if item.is_file():
            print(f"- content/common/config/{item.name}")

    for item in Path(path_2).iterdir():
        if item.is_file():
            print(f"- content/common/config/rules/{item.name}")