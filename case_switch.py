# -*- coding: utf-8 -*-
from copy import deepcopy
from enum import Enum


def check_bitmask(value: str | bytes):
    return value.isnumeric()

def split_to_words(word: str):
    if word.isupper() or word.islower():
        separator: str = "-" if "-" in word else "_"
        return word.split(separator)

    _upper: list[str] = list(filter(lambda x: x.isupper(), word))

    _ = deepcopy(word)

    for _char in _upper:
        _ = _.replace(_char, f" {_char}")

    return [item.strip() for item in _.split(" ") if item]





class Char(str):
    def __new__(cls, value):
        if not isinstance(value, str) or not hasattr(value, "__str__"):
            raise TypeError
        elif len(value) != 1:
            raise ValueError
        else:
            return str.__new__(cls, value)

    def u(self):
        return ord(self)


class Case:
    def __init__(self, name: str, bitmask: str):
        if not bitmask.isnumeric():
            raise TypeError

        if len(bitmask) != 6:
            raise ValueError

        self._name: str = name
        self._bitmask: str = bitmask
        self._all_lower: bool | None = None
        self._all_upper: bool | None = None
        self._has_dash: bool | None = None
        self._has_under: bool | None = None
        self._first_upper: bool | None = None
        self._other_upper: bool | None = None

    def set_values(self):
        _attrs: tuple[str, ...] = (
            "_all_lower",
            "_all_upper",
            "_has_dash",
            "_has_under",
            "_first_char_upper",
            "_other_char_upper")

        for index, value in enumerate(self._bitmask):
            setattr(self, _attrs[index], bool(value))

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._bitmask == other._bitmask
        else:
            return NotImplemented

    def split_to_words(self, value: str):
        pass



class WordCases:
    def __init__(self):
        self._cases: dict[str, Case] = dict()

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._cases.get(item, None)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, str) and isinstance(value, Case):
            self._cases[key] = value
        else:
            return TypeError

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._cases.keys()
        else:
            return NotImplemented

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, Case) and other.name not in self:
            self[other.name] = other
        else:
            raise ValueError

    __radd__ = __add__
    __iadd__ = __add__


word_cases: WordCases = WordCases()


def register_case(case: Case):
    word_cases + case

class WordCase(Enum):
    CAMEL_CASE = "camelCase"
    COBRA_CASE = "Cobra_case"
    KEBAB_CASE = "kebab-case"
    LOWER_CASE = "lowercase"
    PASCAL_CASE = "PascalCase"
    SCREAM_CASE = "SCREAM_CASE"
    SHAWERMA_CASE = "SHAWERMA-CASE"
    SNAKE_CASE = "snake_case"
    TRAIN_CASE = "Train-Case"
    UPPER_CASE = "UPPERCASE"


snake_case: Case = Case("snake", "100100")
camel_case: Case = Case("camel", "000001")
pascal_case: Case = Case("pascal", "000011")
cobra_case: Case = Case("cobra", "000111")
lower_case: Case = Case("lower", "100000")
upper_case: Case = Case("upper", "010011")
shawerma_case: Case = Case("shawa", "011011")
kebab_case: Case = Case("kebab", "101000")
scream_case: Case = Case("scream", "010111")


if __name__ == '__main__':
    line1 = "check_the_phrase"
    line2 = "check-the-phrase"
    line3 = "checkThePhrase"
    line4 = "CheckThePhrase"
    print(split_to_words(line1))
    print(split_to_words(line2))
    print(split_to_words(line3))
    print(split_to_words(line4))
