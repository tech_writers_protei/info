= �нструкция по резервному копированию и восстановлению PROTEI SigFW
:doctype: book
:toc:
:figure-caption: Рисунок
:!chapter-signifier:
:toc-title: ����������
:outlinelevels: 5
:sectnumlevels: 3
:title-logo-image: image:images/LOGO.png[pdfwidth=25vw]
����: {docdate}
������: 1.1.0.31
:toclevels: 2


[[�-�����-����-�-���-����-�-��]]

== Юридическая информация


[preface]
--
--

����������, ������������ � ��������� ���������, �������� ����������������, ������������� ������������� ��� ������������� ���������� (�������� �������������)
� �� �������� �������� ������� �����.
����� ���������������, ��������������� �/��� ������������� ����������, ������������ � ��������� ���������,
� ����� ��������� �������� �� ���, � ��� ����� � ����������� ��� ��������� �� ������ �����, �������������� ������������� � ���������������� ����������� ��������,
����������� �� ��� "��� ������".

��� "��� ������" �������� ��������������� ������� �� ��������� �������� � ����������, ������������ � ���, � ��������� �� ����� ����� ������� ����� ���������
� ��������� �������� ��� ���������������� ����������� ��������� (��������� ������������).

��� "��� ������" �� ����� ����� ���������� (�������� �������������) �/��� �������� ������ ��������������� �� ������ (������� ��������� ������),
������� ����� ���������� � ���������� ���������� ���������� (�������� �������������) �/��� �������� ������ ���������� ���������.

{empty}

image::LOGO.png[]

194044, �����-��������� +
������� �������������� ��., �.60, ���. � +
������-����� "�������" +
���.: (812) 449-47-27 +
����: (812) 449-47-29 +
Web: https://www.protei.ru +
Email: mailto:info@protei.ru[info@protei.ru] +

[[������������-�����������������-�-������������]]

== ������������, ����������������� � ������������

[[���������]]

anchor:installation[]


=== ���������
:asciidoctorconfigdir: ../
:experimental:
:icons: font
:source-highlighter: highlightjs
:toc: auto

. ������� ����� � �������� ����� DPDK v20.05, `dpdk-20.05.tar.xz`, https://core.dpdk.org/download/[� ������������
�����].
. ����������� ����������� �����.
+
[source,bash]
----
$ tar -xvf dpdk-20.05.tar.xz
----
+
. ��������� ������ `dpdk-setup.sh`, ����������� � ���������� `usertools` �������������� ������.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-setup.sh
----
+
. � ���� `Option` ������� ���, ��������������� �������� *x86_64-native-linuxapp-gcc* � ������ *Step 1*.
+
[source,console]
----
 �������������������-
 Step 1: Select the DPDK environment to build
 �������������������-
 ...
 [40] x86_64-native-linuxapp-clang
 [41] x86_64-native-linuxapp-gcc
 [42] x86_64-native-linuxapp-icc
 ...

Option: 41
----

. ����������� ������� ��������� �����������.

[source,bash]
----
$ dpdk-20.05/usertools/dpdk-devbind.py -status
----

[source,console]
----
Network devices using kernel driver
 ===================================
 0000:02:00.0 'RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller 8168' if=enp2s0 drv=r8169 unused=vfio-pci _Active_
 0000:03:00.0 'QCA9565 / AR9565 Wireless Network Adapter 0036' if=wlp3s0 drv=ath9k unused=vfio-pci 

No 'Baseband' devices detected
 ==============================

No 'Crypto' devices detected
 ============================

No 'Eventdev' devices detected
 ==============================

No 'Mempool' devices detected
 =============================

No 'Compress' devices detected
 ==============================

No 'Misc (rawdev)' devices detected
 ===================================
----

����� ���������� �������� ������� ���������� � DPDK.
[start=6]
. ��������� ������ `<dpdk_root_dir>/usertools/dpdk-setup.sh`.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-setup.sh
----
+
. � ���� `Option` ������� ��� ���������������� ��������, `drv`, � ������ *Step 2*:
* `igb-uio` - [45];
* `uio_pci_generic` - [45];
* `vfio-pci` - [46];
+
[source,console]
----
 �������������������-
 Step 2: Setup linux environment
 �������������������-
 [45] Insert IGB UIO module
 [46] Insert VFIO module
 [47] Insert KNI module
 ...

Option: 46

Unloading any existing VFIO module
 Loading VFIO module
 chmod /dev/vfio
 OK
----
+
. ����� ���������� ��������� ���������� � DPDK.
* ��� �������� `igb-uio`:
[arabic]
.. ���������� ����� `dpdk-igb-uio-dkms`.
+
[source,bash]
----
$ sudo apt install dpdk-igb-uio-dkms
----
+
.. ��������� ������ `uio`.
+
[source,bash]
----
$ modprobe uio
----
+
.. �������� ������ `uio` � ����.
+
[source,bash]
----
$ insmod uio
----
+
* ��� �������� `vfio-pci`:
+
[arabic]
.. ��������� ������ `vfio-pci`.
+
[source,bash]
----
$ modprobe vfio-pci
----
+
.. ������ ��������� ������� ��� �������������.
+
[source,bash]
----
$ sudo chmod 0666 /dev/vfio/*
----
+
.. ��������� ������ `uio`.
+
[source,bash]
----
$ modprobe vfio
----
+
NOTE: ���� ���� <abbr title="Input/Output Memory Management Unit">IOMMU</abbr> ����������,
�� `vfio` �������� ������������ ��� ���������� ��������� `enable_unsafe_noiommu_mode`.
+
[source,bash]
----
$ modprobe vfio enable_unsafe_noiommu_mode=1
----
+
* ��� �������� `uio_pci_generic`:
+
[arabic]
.. ��������� ������ `uio`.
+
[source,bash]
----
$ modprobe uio_pci_generic
----
+
. ��������� ������� �������� � ���� `unused` ��� ������ ���������� �� �����������.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-devbind.py --status
----
+
[source,console]
----
Network devices using kernel driver
===================================
0000:02:00.0 'RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller 8168' if=enp2s0 drv=r8169 unused=vfio-pci *Active*
0000:02:01.0 'VMXNET3 Ethernet Controller 07b0' if=ens33 drv=vmxnet3 unused=vfio-pci
0000:02:03.0 'VMXNET3 Ethernet Controller 07b0' if=ens35 drv=vmxnet3 unused=vfio-pci 
0000:02:04.0 'VMXNET3 Ethernet Controller 07b0' if=ens36 drv=vmxnet3 unused=vfio-pci
0000:03:00.0 'QCA9565 / AR9565 Wireless Network Adapter 0036' if=wlp3s0 drv=ath9k unused=vfio-pci *Active*

No 'Baseband' devices detected
==============================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================

No 'Misc (rawdev)' devices detected
===================================
----
+
. ���������� � ��������� ����������� �������� � ������� ������� `dpdk-devbind.py`.
+
NOTE: ��������� �� ������ ���� ��������, �.�. ������ ������������� ������� `*Active*`.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-devbind.py --bind=<driver> <device1> ... <deviceN>
----
+
** driver - �������� ������������� ��������;
** ip_device - IP-����� ���������� � ������� `0000:XX:XX.X`.
+
[source,bash]
----
$ dpdk-devbind.py --bind=vfio 0000:02:03.0 0000:02:04.0
----
+
. ��������� ��������� ����������� � ���������, ��� ��� ��������� � ������ `Network devices using DPDK-compatible driver` ��� ������ ���������� �� �����������.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-devbind.py --status
----
+
[source,console]
----
Network devices using DPDK-compatible driver
============================================
0000:02:03.0 'VMXNET3 Ethernet Controller 07b0' drv=vfio-pci unused=vmxnet3
0000:02:04.0 'VMXNET3 Ethernet Controller 07b0' drv=vfio-pci unused=vmxnet3

Network devices using kernel driver
===================================
0000:02:00.0 'RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller 8168' if=enp2s0 drv=r8169 unused=vfio-pci *Active*
0000:02:01.0 'VMXNET3 Ethernet Controller 07b0' if=ens33 drv=vmxnet3 unused=vfio-pci *Active*
0000:03:00.0 'QCA9565 / AR9565 Wireless Network Adapter 0036' if=wlp3s0 drv=ath9k unused=vfio-pci *Active*

No 'Baseband' devices detected
==============================

No 'Crypto' devices detected
============================

No 'Eventdev' devices detected
==============================

No 'Mempool' devices detected
=============================

No 'Compress' devices detected
==============================

No 'Misc (rawdev)' devices detected
===================================
----
+
. ��� �������������� ����� ��� hugepages ��������� ������ `dpdk-setup.sh`, ����������� � ���������� `usertools` �������������� ������.
+
[source,bash]
----
$ dpdk-20.05/usertools/dpdk-setup.sh
----
+
. � ���� `Option` ������� ���, ��������������� �������� `Setup hugepage mappings for non-NUMA systems` � ������ `Step 2`.
+
[source,console]
----
 �������������������-
 Step 2: Setup linux environment
 �������������������-
...
[47] Insert KNI module
[48] Setup hugepage mappings for non-NUMA systems
[49] Setup hugepage mappings for NUMA systems
...

Option: 48
----
+
. � ���� `Number of pages` ������� ����������� ���������� �������.
+
[source,console]
----
Removing currently reserved hugepages
Unmounting /mnt/huge and removing directory

  Input the number of 2048kB hugepages
  Example: to have 128MB of hugepages available in a 2MB huge page system,
  enter '64' to reserve 64 * 2MB pages
Number of pages: 64
Reserving hugepages
Creating /mnt/huge and mounting as hugetlbfs
----
+
. ������ ��������� � ���������������� ����� <<gtp_forwarder,gtp_forwarder.cfg>>.
+
. ��������� ���������� �� ����� ��������������.
+
[source,bash]
----
$ sudo systemctl start sigfw
----
+
���
+
[source,bash]
----
$ /usr/protei/Protei_SigFW/start
----

[[����������]]

anchor:system_management[]


=== ����������
:experimental:
:icons: font
:source-highlighter: highlightjs
:toc: auto

[[����������]]

===== ����������

� PROTEI SigFW ������������ ��������� ����������:

* `/usr/protei/Protei_SigFW/` � ������� ����������;
* `/usr/protei/Protei_SigFW/bin` � ���������� ��� ����������� ������;
* `/usr/protei/Protei_SigFW/cdr` � ���������� ��� CDR���������;
* `/usr/protei/Protei_SigFW/config` � ���������� ��� ���������������� ������;
* `/usr/protei/Protei_SigFW/log` � ���������� ��� ���-������;
* `/usr/protei/Protei_SigFW/metrics` � ���������� ��� ������ ������;
* `/usr/protei/Protei_SigFW/scripts` � ���������� ��� ��������, ����������� ������� API;

[[�����������-�������]]

===== ����������� �������

* ����� ��������� PROTEI SigFW, ������� ��������� ���� �� ������:

** ������� `systemctl start` �� ���� �����������������:
+
[source,shell]
----
$ sudo systemctl start sigfw
----
+
** ������ `start` � ������� �����:
+
[source,shell]
----
$ /usr/protei/Protei_SigFW/start
----
+
* ����� ���������� PROTEI SigFW, ������� ��������� ���� �� ������:

** ������� `systemctl stop` �� ���� �����������������:
+
[source,shell]
----
$ sudo systemctl stop sigfw
----
+
** ������ `stop` � ������� �����:
+
[source,shell]
----
$ /usr/protei/Protei_SigFW/stop
----
+
* ����� ��������� ������� ��������� PROTEI SigFW, ������� ��������� ������� `systemctl status` �� ���� �����������������:
+
[source,console]
----
$ /usr/protei/Protei_SigFW/status
 sigfw.service - sigfw
   Loaded: loaded (/usr/lib/systemd/system/sigfw.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2023-10-16 19:34:58 MSK; 3 days ago
 Main PID: 3001 (Protei_SS7FW)
    Tasks: 16 (limit: 48560)
   Memory: 1009.3M
   CGroup: /system.slice/sigfw.service
           3001 ./bin/Protei_SigFW
----
+
* ����� ��������� ������� ������ PROTEI SigFW, ������� ��������� ������ `version` � ������� �����:
+
[source,console]
----
$ /usr/protei/Protei_SigFW/status
Protei_SS7FW
ProductCode 1.0.13.3 build 2619
----
+
* ����� ������������� PROTEI SigFW, ������� ��������� ���� �� ������:

** ������� `systemctl restart` �� ���� �����������������:
+
[source,console]
----
$ sudo systemctl restart sigfw
 sigfw.service - SigFW
   Loaded: loaded (/usr/lib/systemd/system/sigfw.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2023-07-21 19:50:25 MSK; 2 days ago
  Process: 99426 ExecStopPost=/usr/protei/Protei_SigFW/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 99344 ExecStopPost=/usr/protei/Protei_SigFW/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
  Process: 99300 ExecStop=/usr/protei/Protei_SigFW/bin/utils/stop_prog.sh (code=exited, status=0/SUCCESS)
  Process: 99508 ExecStart=/usr/protei/Protei_SigFW/bin/utils/start_prog.sh (code=exited, status=0/SUCCESS)
  Process: 99481 ExecStartPre=/usr/protei/Protei_SigFW/bin/utils/check_history.sh (code=exited, status=0/SUCCESS)
  Process: 99453 ExecStartPre=/usr/protei/Protei_SigFW/bin/utils/move_log.sh (code=exited, status=0/SUCCESS)
 Main PID: 99524 (Protei_SigFW)
    Tasks: 1 (limit: 35612)
   Memory: 2.5M
   CGroup: /system.slice/sigfw.service
           99524 ./bin/Protei_SigFW --sigfw-standalone
----
+
** ������ `restart` � ������� �����:
+
[source,shell]
----
$ /usr/protei/Protei_SigFW/restart
----
+
* ����� ������������� ���������������� ���� `file.cfg`, ������� ��������� ������ `reload` � ������� �����:
+
[source,console]
----
$ /usr/protei/Protei_SigFW/reload <file.cfg>
reload <file> config Ok
----
+
* ����� �������� ���� ����, ������� ��������� ������ `core_dump` � ������� �����:
+
[source,console]
----
$ /usr/protei/Protei_SigFW/core_dump
Are you sure you want to continue? y
Core dump generated!
----

NOTE: ���� ����� �������� � ���������� `/var/lib/systemd/coredump`.

[[����������]]

anchor:update[]


=== ����������
:experimental:
:source-highlighter: highlightjs

�������� ����������� ������ PROTEI SigFW �������� ���� `/usr/protei/Protei_SigFW/bin/Protei_SigFW`.

����� �������� ������ ������������ �����������, ����������:

. ��������� � ���������� `/usr/protei/Protei_SigFW/bin/` ��������� ����.
. ������� ������������� ������ �� ����.
+
[source,bash]
----
$ cd /usr/protei/Protei_SigFW/bin/
$ ln -sf Protei_SigFW.<version>.r64 Protei_SigFW
----
+
. ������������� ����������.
+
[source,bash]
----
$ sudo systemctl restart sigfw
----
+
. ��������� ��������� ����������.
+
[source,bash]
----
$ sudo systemctl status sigfw
----

[[���������-�����������-�-���������-��������������]]

anchor:backup_recovery[]


=== ��������� ����������� � ��������� ��������������
:asciidoctorconfigdir: ../
:experimental:
:source-highlighter: highlightjs
:toc: auto

[[���������-�����������]]

==== ��������� �����������

��������� ���������� ����������� ���� SigFW �������������� ����������� ������� *backup_conf.sh* ��
���������� � ������� ������ ������������ *cron*/**systemd.timer**.

���������� ����������� ��������:

* ���������������� ����� ���� SigFW: `/usr/protei/Protei_SigFW/config/`;
* ����������� ����� ���� SigFW: `/usr/protei/Protei_SigFW/bin/`;
* ���������������� ����� �������: `/etc/`.

SHA-256 ���-����� ���������������� ������ ������������ � ���� `.config_hash.sha256` � ���������� `/usr/protei/Protei_SigFW/config`.

������������� ���������� ��������� ����� ������������ ������������� � ������� ��������� ���-����� ������� ���������������� ������
� ���-������ ���������������� ������ � ��������� ��������� �����, `.config_hash.sha256`.

��������� ����� ����������� �� ����������� ���� SigFW � ���������� `/usr/protei/backup/` � ���� ������ *.tar.gz*.

��� ������ ����� ������: `SigFW.backup_%Y%m%d-%H%M.%S.tar.gz`, ���:

* *%Y%m%d* - ���, ����� � ���� ������ �������� �����;
* *%H%M.%S* - ���, ������ � ������� ������ �������� �����;

[[���������-��������������]]

==== ��������� ��������������

��� ���������� �������������� ���� SigFW ���������� ��������� ��������� ��������:

. ������� ������������ �� root.
+
[source,shell]
----
$ sudo su
----
+
. ��������� ������ `backup_restore` � ���������� `/usr/protei/Protei_SigFW/scripts/` �� ��������� ����, ������ ���
 ������ � ��������� ������.
+
[source,shell]
----
$ /usr/protei/Protei_SigFW/scripts/backup_restore SigFW.backup_<datetime>.tar.gz
----
+
. ��� ������� ������� ��� ����� ������ �� ���������� ���� ������� ��� � ������ �� ���������.
+
[source,console]
----
$ /usr/protei/Protei_SigFW/scripts/backup_restore
Please provide backup_NAME
SigFW.backup_<datetime>.tar.gz
----
+
. ��������� ���������� ������ ������� � ���������� ������.
+
[source,console]
----
$ /usr/protei/Protei_SigFW/scripts/backup_restore SigFW.backup_<datetime>.tar.gz
doing restore..
restore is done
----

� ���������� ����� �������������:

* ���������������� ����� ���� SigFW: `/usr/protei/Protei_SigFW/config/`;
* ����������� ����� ���� SigFW: `/usr/protei/Protei_SigFW/bin/`;
* ���������������� ����� �������: `/etc/`.
