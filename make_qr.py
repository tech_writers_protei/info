from qrcode import make
from qrcode.image.base import BaseImage


if __name__ == '__main__':
    data: str = "https://docs.protei.ru/doc/Mobile/Protei_DPI/common/"
    image: BaseImage = make(data)
    image.save("dpi_qr.svg")
