# -*- coding: utf-8 -*-
from enum import Enum
from typing import get_args, Iterable, NamedTuple, Sequence
from operator import attrgetter


def check_non_negative(value: int):
    return value > 0


def check_all_same(value: Sequence):
    return value.count(value[0]) == len(value)


class Direction(Enum):
    HORIZONTAL = "hor"
    VERTICAL = "vert"


class Coordinate(NamedTuple):
    row: int = None
    column: int = None

    def __bool__(self):
        return self.coord != (None, None)

    @property
    def coord(self):
        return self.row, self.column

    def __str__(self):
        return f"{self.row}:{self.column}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            if bool(self) and bool(other):
                return self.coord == other.coord
            else:
                return False
        elif isinstance(other, tuple):
            if not bool(self):
                return False
            elif len(other) == 2 and get_args(other) == (int, int):
                return self.coord == (*other,)
            else:
                return NotImplemented
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and bool(self) and bool(other) and self.row == other.row:
            return self.column < other.column
        elif isinstance(other, self.__class__) and bool(self) and bool(other) and self.column == other.column:
            return self.row < other.row
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and bool(self) and bool(other) and self.row == other.row:
            return self.column > other.column
        elif isinstance(other, self.__class__) and bool(self) and bool(other) and self.column == other.column:
            return self.row > other.row
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__) and bool(self) and bool(other) and self.row == other.row:
            return self.column <= other.column
        elif isinstance(other, self.__class__) and bool(self) and bool(other) and self.column == other.column:
            return self.row <= other.row
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__) and bool(self) and bool(other) and self.row == other.row:
            return self.column >= other.column
        elif isinstance(other, self.__class__) and bool(self) and bool(other) and self.column == other.column:
            return self.row >= other.row
        else:
            return NotImplemented

    def shift(self, direction: Direction, offset: int = 0):
        if not offset:
            return

        elif direction == Direction.HORIZONTAL:
            if check_non_negative(self.column + offset):
                self.column += offset
            else:
                return NotImplemented

        elif direction == Direction.VERTICAL:
            if check_non_negative(self.row + offset):
                self.row += offset
            else:
                return NotImplemented

        return

    def distance(self, other) -> int:
        if isinstance(other, self.__class__):
            return abs(self.row - other.row) + abs(self.column - other.column)
        else:
            return NotImplemented

    def is_adjacent(self, other):
        if isinstance(other, self.__class__):
            return self.distance(other) == 1
        else:
            return NotImplemented

    def is_adjacent_hor(self, other):
        if isinstance(other, self.__class__):
            return self.row == other.row and self.distance(other) == 1
        else:
            return NotImplemented

    def is_adjacent_vert(self, other):
        if isinstance(other, self.__class__):
            return self.column == other.column and self.distance(other) == 1
        else:
            return NotImplemented


class TableCell:
    def __init__(self, row: int, column: int, value: str = None):
        if value is None:
            value: str = ""

        self._row: int = row
        self._column: int = column
        self._value: str = value
        self._width: int = 1
        self._height: int = 1

    @property
    def coord(self):
        return Coordinate(self._row, self._column)

    @property
    def columns(self) -> list[int]:
        return [self._column + value for value in range(self._width)]

    @property
    def rows(self) -> list[int]:
        return [self._row + value for value in range(self._height)]

    def contains_row(self, index: int) -> bool:
        return index in self.rows

    def contains_column(self, index: int) -> bool:
        return index in self.columns

    def remove(self):
        self._row = None
        self._column = None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.coord == other.coord
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord < other.coord
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.coord > other.coord
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.coord <= other.coord
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.coord >= other.coord
        else:
            return NotImplemented

    def is_adjacent(self, other):
        if isinstance(other, self.__class__):
            return self.coord.is_adjacent(other.coord)
        else:
            return NotImplemented

    def is_same_height(self, other):
        if isinstance(other, self.__class__):
            return self._height == other._height
        else:
            return NotImplemented

    def is_same_width(self, other):
        if isinstance(other, self.__class__):
            return self._width == other._width
        else:
            return NotImplemented

    def relocate(self, *, coord: Coordinate = None, shift_hor: int = None, shift_vert: int = None):
        if all(value is None for value in (coord, shift_hor, shift_vert)):
            return

        elif coord is not None and bool(coord):
            self._row = coord.row
            self._column = coord.column

        else:
            if shift_hor is None:
                shift_hor: int = 0

            if shift_vert is None:
                shift_vert: int = 0

            self.coord.shift(Direction.HORIZONTAL, shift_hor)
            self.coord.shift(Direction.VERTICAL, shift_vert)

        return

    def add_hor_value(self, other):
        if isinstance(other, self.__class__):
            if self._row < other._row:
                self._value = f"{self._value}\n{other._value}"
            elif self._row > other._row:
                self._value = f"{other._value}\n{self._value}"
            else:
                return NotImplemented
        else:
            return NotImplemented

    def add_vert_value(self, other):
        if isinstance(other, self.__class__):
            if self._column < other._column:
                self._value = f"{self._value}\n{other._value}"
            elif self._column > other._column:
                self._value = f"{other._value}\n{self._value}"
            else:
                return NotImplemented
        else:
            return NotImplemented

    def merge(self, other):
        if isinstance(other, self.__class__):
            if self.coord.is_adjacent_hor(other.coord) and self.is_same_height(other):
                self._width += other._width
                self.add_hor_value(other)
                del other
            elif self.coord.is_adjacent_vert(other.coord) and self.is_same_width(other):
                self._height += other._height
                self.add_vert_value(other)
                del other
            else:
                return NotImplemented
        else:
            return NotImplemented

    @property
    def row(self):
        return self._row

    @row.setter
    def row(self, value):
        self._row = value

    @property
    def column(self):
        return self._column

    @column.setter
    def column(self, value):
        self._column = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        self._width = value

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value


class Header(TableCell):
    def __init__(self, column: int, value: str):
        row: int = -1
        super().__init__(row, column, value)


class Table:
    def __init__(self, headers: Iterable[Header] = None, table_cells: Iterable[TableCell] = None):
        if headers is None:
            headers: list[Header] = []

        if table_cells is None:
            table_cells: list[TableCell] = []

        self._headers: list[Header] = [*headers]
        self._table_cells: list[TableCell] = [*table_cells]

    def _validate(self):
        heights: list[int] = []

        for column in range(self.num_columns):
            if not any(header.column == column for header in self._headers):
                raise ValueError
            table_cells: list[TableCell] = self.column(column)
            height: int = sum(table_cell.height for table_cell in table_cells)
            heights.append(height)

        if not check_all_same(heights):
            raise ValueError
        else:
            del heights

        widths: list[int] = []

        for row in range(self.num_rows):
            table_cells: list[TableCell] = self.row(row)
            width: int = sum(table_cell.width for table_cell in table_cells)
            widths.append(width)

        if not check_all_same(widths):
            raise ValueError
        else:
            del widths

    def __len__(self):
        return len(self._table_cells)

    @property
    def num_rows(self) -> int:
        return max(table_cell.row for table_cell in self._table_cells)

    @property
    def num_columns(self) -> int:
        return max(table_cell.column for table_cell in self._table_cells)

    def row(self, index: int) -> list[TableCell]:
        if index <= self.num_rows:
            cells: list[TableCell] = [table_cell for table_cell in self._table_cells if table_cell.row == index]
            return sorted(cells, key=attrgetter("column"))

        else:
            raise KeyError

    def column(self, index) -> list[TableCell]:
        if isinstance(index, int):
            if index <= self.num_columns:
                cells: list[TableCell] = [table_cell for table_cell in self._table_cells if table_cell.column == index]
                return sorted(cells, key=attrgetter("row"))

            else:
                raise KeyError

        elif isinstance(index, str):
            _index: int = self.dict_columns.get(index)
            return self.column(_index)

        else:
            raise TypeError

    @property
    def dict_table_cells(self) -> dict[Coordinate, TableCell]:
        return {table_cell.coord: table_cell for table_cell in self._table_cells}

    @property
    def dict_columns(self) -> dict[str, int]:
        return {header.value: header.column for header in self._headers}

    def __getitem__(self, item):
        if isinstance(item, Coordinate):
            return self.dict_table_cells.get(item, None)

        else:
            raise KeyError

    def __contains__(self, item):
        if isinstance(item, Coordinate):
            return item in self.dict_table_cells.keys()
        else:
            return False

    def add_table_cell(self, table_cell: TableCell):
        if table_cell.coord in self:
            raise ValueError
        else:
            self._table_cells.append(table_cell)

    def add_column(self, header: Header, table_cells: Iterable[TableCell]):
        # FIXME
        pass


class TableElement:
    def __init__(self, table_cells: Iterable[TableCell] = None):
        if table_cells is None:
            table_cells: list[TableCell] = []

        self._table_cells: list[TableCell] = [*table_cells]
