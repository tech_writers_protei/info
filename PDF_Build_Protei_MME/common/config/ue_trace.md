
В этом файле задаётся набор абонентов, по которым выводятся сообщения в **ue_trace**.

Ключ для перезагрузки — **reload ue_trace.json**.

### Описание параметров ###

| Параметр | Описание      | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|---------------|--------|--------------|-----|-----|--------|
| msisdn   | Номер MSISDN. | строка | -            | O   | R   |        |
| imsi     | Номер IMSI.   | строка | -            | O   | R   |        |

### Пример ###

```json
[
  {
    "msisdn" : "76000000316",
    "imsi" : "001010000000398"
  },
  {
    "msisdn" : "76000000317"
  },
  {
    "imsi" : "001010000000399"
  }
]
```