
В этом файле задаются настройки компонента GTP-C.

**Примечание.** Наличие файла обязательно.

Ключ для перезагрузки — **reload component/gtp_c.json**.

### Используемые подсекции ###


* **[localAddress](#local-address)** - параметры локального хоста;

* **[s11](#s11)** - параметры интерфейса S11;

* **[s11u](#s11-u)** - параметры интерфейса S11-U;

* **[s10](#s10)** - параметры интерфейса S10;

* **[s3](#s3)** - параметры интерфейса S3;

* **[sv](#sv)** - параметры интерфейса Sv;

* **[gnGp](#gngp)** - параметры интерфейса GnGp;

* **[timers](#timers)** - параметры таймеров;

* **[repeat](#repeat)** - параметры повторов;

* **[overload](#overload)** - параметры перегрузки;

### Описание подсекций и их параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **[[local-address]]localAddress** | Параметры локального хоста. | объект | - | M | P | |
| [[local-host]]localHost | Прослушиваемый IP-адрес. | строка | - | M | P | |
| [[local-port]]localPort | Прослушиваемый порт. | число | 2123 | O | P | |
| [[dscp]]dscp | Используемое значение DSCP. | число | - | O | P | |
| [[ip_mtu_discover]]ipMtuDiscover | Используемое значение IP MTU Discover. | число | - | O | P | |
| **[[s11]]s11** | Параметры интерфейса S11. | объект | - | O | P | |
| localHost | Прослушиваемый IP-адрес для интерфейса S11. | строка | значение [localAddress::localHost](#local-host) | O | P | |
| localPort | Прослушиваемый порт для интерфейса S11. <br/><br/> | число | значение [localAddress::localPort](#local-port) | O | P | |
| dscp | Используемое значение DSCP. <br/><br/> | число | значение [localAddress::dcsp](#dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [localAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | |
| **[[s11u]]s11u** | Параметры интерфейса S11-U. | объект | - | O | P | |
| localHost | Прослушиваемый IP-адрес для интерфейса S11-U. <br/><br/> | строка | значение [localAddress::localHost](#local-host) | O | P | |
| localPort | Прослушиваемый порт для интерфейса S11-U. | число | 2152 | O | P | |
| dscp | Используемое значение DSCP. <br/><br/> | число | значение [localAddress::dcsp](#dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [LocalAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | 
| **[[s10]]s10** | Параметры интерфейса S10. | объект | - | O | P | |
| localHost | Прослушиваемый IP-адрес для интерфейса S10. <br/><br/> | строка | значение [localAddress::localHost](#local-host) | O | P | |
| localPort | Прослушиваемый порт для интерфейса S10. <br/><br/> | число | значение [localAddress::localPort](#local-port) | O | P | |
| dcsp | Используемое значение DSCP. <br/><br/> | число | значение [localAddress::dcsp](#dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [localAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | 
| **[[s3]]s3** | Параметры интерфейса S3. | объект | - | O | P | |
| [[s3-local-host]]localHost | Прослушиваемый IP-адрес для интерфейса S3. <br/><br/> | строка | значение [localAddress::localHost](#local-host) | O | P | |
| [[s3-local-port]]localPort | Прослушиваемый порт для интерфейса S3. <br/><br/> | число | значение [localAddress::localPort](#local-port) | O | P | |
| [[s3-dscp]]dscp | Используемое значение DSCP. <br/><br/> | число | значение [localAddress::dcsp](#dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [localAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | 
| **[[sv]]sv** | Параметры интерфейса Sv. | объект | - | O | P | |
| [[sv-local-host]]localHost | Прослушиваемый IP-адрес для интерфейса Sv. <br/><br/> | строка | значение [localAddress::localHost](#local-host) | O | P | |
| [[sv-local-port]]localPort | Прослушиваемый порт для интерфейса Sv. <br/><br/> | число | значение [localAddress::localPort](#local-port) | O | P | |
| [[sv-dscp]]dscp | Используемое значение DSCP. <br/><br/> | число | значение [LocalAddress::dcsp](#dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [localAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | 
| **[[gngp]]gnGp** | Параметры интерфейса GnGp. | объект | - | O | P | |
| localHost | Прослушиваемый IP-адрес для интерфейса GnGp. <br/><br/> | строка | значение [s3::localHost](#s3-local-host) | O | P | |
| localPort | Прослушиваемый порт для интерфейса GnGp. <br/><br/> | число | значение [s3::localPort](#s3-local-port) | O | P | |
| dscp | Используемое значение DSCP. <br/><br/> | число | значение [s3::dcsp](#s3-dscp) | O | P | |
| ipMtuDiscover | Используемое значение IP MTU Discover. <br/><br/> | число | значение [localAddress::ipMtuDiscover](#ip_mtu_discover) | O | P | 
| **[[timers]]timers** | Параметры таймеров. | объект | - | O | R | |
| [[resp-timeout]]responseTimeout | Время ожидания ответного сообщения. | числоpass:q[\<br\>]мс | 30&nbsp;000 | O | R | |
| **[[repeat]]repeat** | Параметры повторов запросов. | объект | - | O | R | |
| count | Количество повторных запросов, отправляемых по истечении времени [responseTimeout](#resp-timeout). | число | 4 | O | R | |
| **[[overload]]overload** | Параметры перегрузки. | объект | - | O | R | |
| enable | Флаг детектирования перегрузки. | логический | 0 | O | R | |
| ddn | Максимальное количество сообщений GTP-C: Downlink Data Notification в секунду. | число | 1000 | O | R | |

### Пример ###

```json
{
  "localAddress" : {
    "localHost" : "192.168.100.1",
    "localPort" : 2123
  },
  "s11" : {
    "localHost" : "192.168.100.2",
    "dscp" : 50
  },
  "s11u" : {
    "localHost" : "192.168.100.3",
    "localPort" : 2152
  },
  "s10" : {
    "localHost" : "192.168.100.6",
    "localPort" : 2123,
    "ipMtuDiscover" : 100
  },
  "timers" : {
    "responseTimeout" : 30000
  },
  "repeat" : {
    "count" : 2
  },
  "overload" : {
    "enable" : true,
    "ddn" : 2000
  }
}
```