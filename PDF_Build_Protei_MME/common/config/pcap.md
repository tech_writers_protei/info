
В этом файле задаются настройки PCAP-логгера.

**Примечание.** При отсутствии файла всем параметрам будут заданы значения по умолчанию.

### Используемые подсекции ###


* **[General](#general)** - общие параметры логгера;

### Описание подсекций и их параметров ###

| Параметр                                        | Описание                                                                                        | Тип        | O/M | P/R | Версия  |
|-------------------------------------------------|-------------------------------------------------------------------------------------------------|------------|-----|-----|---------|
| **[[general]]general**               | Общие параметры логгера.                                                                        | объект     |     |     |         |
| enable                                          | Флаг активации логирования.pass:q[\<br\>]По умолчанию: true                                               | логический | O   | P   |         |
| [[separate_by_proto]]separateByProto | Флаг разделения логирования по протоколам, без разделения по абонентам.pass:q[\<br\>]По умолчанию: false. | логический | O   | P   |         |
| fileName                                        | Имя файла, если не указано имя абонента.pass:q[\<br\>]По умолчанию: ue_pcap_log.                          | строка     | O   | P   |         |
| customLogPath                                   | Директория для хранения журнала.pass:q[\<br\>]По умолчанию: /logs/pcap_trace/.                            | строка     | O   | P   |         |
| prefix                                          | Префикс к имени файла.pass:q[\<br\>]По умолчанию: "".                                                     | строка     | O   | P   | |

**Примечание.** Поле [separateByProto](#separate_by_proto) используется только в случаях, когда не указано имя абонента.

#### Пример ####

```json
{
  "general" : {
    "enable" : true,
    "separateByProto" : false,
    "fileName" : "ue_pcap_lo",
    "customLogPath" : "/logs/pcap_trace/",
    "prefix" : "prefix"
  }
}
```