
В этом файле задаются основные настройки Protei MME.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload mme.json**.

### Используемые подсекции ###


* **[general](#general)** - общие параметры узла;  

* **[timeout](#timeout)** - параметры времени ожидания;  

* **[database](#database)** - параметры базы данных;  

* **[gutiReallocation](#guti)** - параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>;  

* **[dcn](#dcn)** - параметры <abbr title="Dedicated Core Network">DCN</abbr>;

* **[ranges](#ranges)** - диапазоны создаваемых ММЕ идентификаторов;  

* **[vendorSpecific](#vendor_specific)** - параметры, задающие ограничения, вызванные оборудованием других производителей;  

* **[profileRestoration](#profile_restoration)** - флаги, разрешающие определённым процедурам искать незнакомый абонентский профиль в БД;  

* **[enbId](#enodeb)** - параметры допустимых eNodeB.

### Описание подсекций и их параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **[[general]]general** | Общие параметры узла | объект | | O | | |
| handlers | Количество задействованных логик. | число | 10 | O | P | |
| coreCount | Количество ядер. | числоpass:q[\<br\>]1-14 | 2 | O | P |       |
| echoSendPeriod | Периодичность отправки сообщения Echo Request по интерфейсу GTP-C. | число, сpass:q[\<br\>]10-600 | 60 | O | R | |
| isr      | Флаг `Operation Indication` в сообщении Delete Session Request. | логический | false | O | R | |
| dnsIpv6 | Флаг запроса IPv6-адреса от DNS-сервера. | логический | false | O | R | |
| releaseOnIcsFail | Индикатор способа очистки контекста после получения сообщения ICS Failure.pass:q[\<br\>]`E` — explicit, явно;/`I` — implicit, неявно. | строка | E | O | R | |
| releaseOnServiceReject | Индикатор способа очистки контекста после получения сообщения Service Reject.pass:q[\<br\>]`E` — explicit, явно / `I` — implicit, неявно. | строка | E | O   | R   | |
| ignoreEnbUeIdReset | Флаг игнорирования значения `UE-associated logical S1-connection Item` при проведении процедуры S1AP-RESET для устройств, имеющих указанный идентификатор `eNB UE S1AP ID`. | логический | false | O | R | |
| radioPriority | Значение `Radio Priority`, отправляемое в SGSN Context Response. | число, pass:q[\<br\>]0-7 | 2 | O | R | |
| radioPrioritySms | Значение `Radio Priority SMS`, отправляемое в SGSN Context Response. | число, pass:q[\<br\>]0-7 | 1 | O | R | |
| oldS1ReleaseCauseCode | Код причины, отправляемый в сообщении S1AP: UE CONTEXT RELEASE ввиду дублирования информации об абоненте.pass:q[\<br\>]`0` - Normal Release, `1` - Release due to E-UTRAN Generated Reason. По умолчанию: 0. | число, 0-1 | 0 | O | R | |
| srvccS1ReleaseCauseCode | Код причины, отправляемый в сообщении S1AP: UE CONTEXT RELEASE COMMAND после успешного SRVCCpass:q[\<br\>]`0` - Normal Release, `1` - Successful Handover. По умолчанию: 0. | число, 0-1 | 0 | O | R | |
| s1apPcsmThreads | Количество потоков, выделяемых для S1AP PCSM. По умолчанию: 1. | число, больше 0 | O | R | |
| **[[timeout]]timeout** | Параметры времени ожидания. | объект | | O | R | |
| contextReqRelease | Время ожидания удаления сессии на предыдущем узле SGW после приёма Context-Request от нового узла ММЕ. | числоpass:q[\<br\>]мс | 5&nbsp;000 | O | R | |
| **[handover](#ho_timeout)** | Параметры времени ожидания при хэндовере | объект | | O | R | |
| **[[database]]database** | Параметры базы данных. | объект | | O | | |
| enable | Флаг взаимодействия с базой данных. | логический | false | O | R | |
| [[hostname]]hostname | DNS-имя или IP-адрес хоста базы данных. | строка | localhost | O | P | |
| [[port]]port | Порт подключения к базе данных. | число | 3306 | O | P | |
| username | Логин для авторизации в базе данных. | строка | - | O | P | |
| password | Пароль для авторизации в базе данных. | строка | - | O | P | |
| dbName | Название базы данных. | строка | - | O | P | |
| updatePeriod | Периодичность обновления базы данных. | числоpass:q[\<br\>]мс | 10&nbsp;000 | O | R | |
| readerCount | Количество соединений базы данных с правами на чтение. | числоpass:q[\<br\>]1-100 | 3 | O | P | |
| unixSock  | Путь до сокета базы данных.pass:q[\<br\>]**Примечание.** Если задан, то параметры [hostname](#hostname) и [port](#port) игнорируются. | строка | - | O | P | |
| **[[guti]]gutiReallocation** | Параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>. | объект | | O | R | |
| reqCount | Максимальное количество обработанных запросов Attach/TAU/Service Request до смены GUTI. | число | 0 | O | R | |
| reallocPeriod | Периодичность смены GUTI. | числоpass:q[\<br\>]мс | 0 | O | R | |
| **[[dcn]]dcn** | Набор используемых <abbr title="Dedicated Core Network">DCN</abbr>. Формат:pass:q[\<br\>]`["<dcn_id>-<relative_capacity>", ...]`. | массив строк | | O | R | |
| \<dcn_id\> | Идентификатор сети. | число | - | M | R | |
| \<relative_capacity\> | Относительная емкость. | число | - | M | R | |
| **[[ranges]]ranges** | Диапазоны создаваемых ММЕ идентификаторов. | объект | | O | P | |
| mmeUeId | Границы диапазона допустимых MME UE ID. Формат:pass:q[\<br\>]`<min>-<max>`.pass:q[\<br\>]**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| mTmsi | Диапазон допустимых M-TMSI для GUTI. Формат:pass:q[\<br\>]`<min>-<max>`.pass:q[\<br\>]**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| teid | Диапазон допустимых TEID, используемых для идентификации абонента на интерфейсе S11. Формат:pass:q[\<br\>]`<min>-<max>`.pass:q[\<br\>]**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| **[[vendor_specific]]vendorSpecific** | Параметры, задающие ограничения, вызванные оборудованием других производителей. | объект | - | O | | |
| longUtranTransparentContainer | Флаг разрешения использования длинного (более 1024 байт) UTRAN Transparent Container при хэндовере от 4G к 3G. | логический | true | O | R | |
| suspend | Флаг поддержки процедуры Suspend для bearer-служб. | логический | false | O | R | |
| detachIndAfterHo | Флаг выполнения процедуры Detach Indication после перехода абонента в 2G/3G. | логический | true | O | R | |
| **[[profile_restoration]]profileRestoration** | Флаги, разрешающие определённым процедурам искать незнакомый абонентский профиль в БД. | объект | - | O | R | |
| serviceReq | Флаг разрешения поиска профиля при получении Service Req. | логический | true | O | R | |
| extServiceReq | Флаг разрешения поиска профиля при получении Extended Service Req. | логический | false | O | R | |
| tau | Флаг разрешения поиска профиля при получении TAU Req. | логический | false | O | R | |
| mtSms | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT SMS. | логический | false | O | R | |
| mtCall | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT Call. | логический | false | O | R | |
| **[[enodeb]]enbId** | Маски белого списка идентификаторов eNodeB. Формат:pass:q[\<br\>]`[ "<active_flag>; <enodeb_id>", ... ]`pass:q[\<br\>]**Примечание.** Если нет записей, то любой идентификатор считается допустимым. | массив строк | - | O | R | |
| \<active_flag\> | Флаг активности маски идентификатора eNodeB. | число (0 или 1) | - | M | R | |
| \<enodeb_id\> | Маска идентификатора eNodeB. | строка (PCRE) | - | M | R | |

#### [[ho_timeout]]handover ####

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| s1HoContextRelease | Время ожидания очистки контекста на предыдущей базовой станции при хэндовере S1. | числоpass:q[\<br\>]мс | 5&nbsp;000 | O | R | |
| hoFromUtranTau | Время ожидания сообщения TAU-Request при хэндовере из сети UTRAN. | числоpass:q[\<br\>]мс | 10&nbsp;000 | O | R | |
| hoFromGeranTau | Время ожидания сообщения TAU-Request при хэндовере из сети GERAN. | числоpass:q[\<br\>]мс | 10&nbsp;000 | O | R | |
| x2HoDelSession | Время ожидания отложенного удаления сессий на исходном SGW при X2-хэндовере. | числоpass:q[\<br\>]мс | 2&nbsp;000 | O | R | |
| eToUtranHoRelease | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в UTRAN. | числоpass:q[\<br\>]мс | 5&nbsp;000 | O | R |  |
| eToGeranHoRelease | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в GERAN. | числоpass:q[\<br\>]мс | 5&nbsp;000 | O | R |  |

### Пример ###

```json
{
  "general" : {
    "handlers" : 100,
    "coreCount" : 3,
    "releaseOnIcsFail" : "I",
    "echoSendPeriod" : 22,
    "radioPrioritySms" : 3
  },
  "timeout" : {
    "contextReqRelease" : 3000,
    "handover" : {
      "hoFromGeranTau" : 8000,
      "eToGeranHoRelease" : 4000
    }
  },
  "database" : {
    "enable" : true,
    "username" : "mme",
    "password" : "777",
    "dbName" : "MME",
    "unixSock" : "/var/lib/mysql/mysql.sock"
  },
  "gutiReallocation" : {
    "reqCount" : 100
  },
  "dcn" : [
    "10-50",
    "24-70"
  ],
  "ranges" : {
    "mmeUeId" : "50-100500",
    "teid" : "100-50000"
  },
  "vendorSpecific" : {
    "suspend" : true,
    "detachIndAfterHo" : false
  },
  "profileRestoration" : {
    "mtSms" : true
  },
  "enbId" : [
    "1; 107217",
    "1; 3584*",
    "0; 3585*"
  ]
}
```