
В этом файле задаются настройки компонента S1AP.

**Примечание.** Наличие секции обязательно.

Ключ для перезагрузки — **reload s1ap.json**.

### Используемые подсекции ###


* **[localAddress](#local-address)** - параметры локального хоста;

* **[timers](#timers)** - параметры таймеров;

* **[security](#security)** - параметры, связанные с проверкой и шифрованием сообщений;

* **[overload](#overload)** - параметры перегрузки;

* **[balancer](#balancer)** - параметры балансировщика;

* **[sctpAdditionalInfo](#sctp_additional_info)** - дополнительные параметры SCTP.

### Описание подсекций и их параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **[[local-address]]localAddress** | Параметры локального хоста. | объект | - | M | P | |
| localHost | Прослушиваемый IP-адрес. | строка | - | M | P | |
| localPort | Прослушиваемый порт. | число | 36412 | O | P | |
| localInterfaces | Список адресов для multihoming. Формат:pass:q[\<br\>]`[ "<ip>:<port>", ... ]` | массив строк | - | O | P | |
| **[[timers]]timers** | Параметры таймеров. | объект | - | O | P | |
| reconnectTimeout | Время ожидания попытки переподключения после разрыва соединения. | числоpass:q[\<br\>]мс | 30&nbsp;000 | O | P | |
| responseTimeout | Время ожидания ответного сообщения. | числоpass:q[\<br\>]мс | 30&nbsp;000 | O | R | |
| **[[security]]security** | Параметры безопасности обработки сообщений. | объект | - | O | R | |
| nodeType | Код типа устройства.pass:q[\<br\>]`mme`/`enodeb` | строка | mme | O | R | |
| encryptionAlg | Список [кодов](#алгоритмы) алгоритмов, используемых для шифрования/дешифрования сообщений, в порядке убывания приоритета. Формат:pass:q[\<br\>]`[<algo>, ... ]` | массив чисел | 3,2,1,0 | O | R | |
| integrityAlg | Список [кодов](#алгоритмы) алгоритмов, используемых для контроля целостности сообщений, в порядке убывания приоритета. Формат:pass:q[\<br\>]`[<algo>, ... ]` | массив чисел | 3,2,1,0 | O | R | |
| **[[overload]]overload** | Параметры перегрузки. | объект | - | O | R | |
| enable | Флаг детектирования перегрузки. | логический | false | O | R | |
| initialUe | Максимальное количество сообщений Initial UE Message в секунду. | число | 1&nbsp;000 | O | R | |
| attachReq | Максимальное количество сообщений Attach Request в секунду. | число | 1&nbsp;000 | O | R | |
| paging | Максимальное количество сообщений Paging в секунду. | число | нет ограничения | O | R | |
| **[[balancer]]balancer** | Параметры балансировщика. | объект | - | O | P | |
| enable | Флаг активации режима работы с балансировщиком. | логический | false | O | P | |
| **[[sctp_additional_info]]sctpAdditionalInfo** | Дополнительные параметры SCTP. | объект | - | O | P | |
| nodelay                      | Флаг активации SCTP nodelay. | логический | false | O | P | |
| maxInitRetransmits                               | Количество попыток отправки сообщения INIT, прежде чем хост считать недоступным. | число | 10 | O | R | |
| initTimeout                                      | Время ожидания сообщения INIT_ACK. | числоpass:q[\<br\>]мс | 1000 | O | R | |
| inStreams                                        | Количество входящих SCTP-потоков. | числоpass:q[\<br\>]1-65&nbsp;536 | 1 | O | P | |
| outStreams                                       | Количество исходящих SCTP-потоков. | числоpass:q[\<br\>]1-65&nbsp;536 | 1 | O | P | |
| associationMaxRetrans                            | Максимальное количество повторных отправок, после которых маршрут считается недоступным. | число | 10 | O | R | |
| rtoMax                                           | Максимальное значение RTO. | числоpass:q[\<br\>]мс | 60&nbsp;000  | O | R | |
| rtoMin                                           | Минимальное значение RTO. | числоpass:q[\<br\>]мс | 1000 | O | R | |
| rtoInitial                                       | Начальное значение RTO. | числоpass:q[\<br\>]мс | 3000 | O | R | |
| hbInterval                                       | Период посылки сигнала heartbeat. | числоpass:q[\<br\>]мс | 30&nbsp;000  | O | R | |
| maxRetrans                                       | Максимальное количество попыток отправки сообщения на адрес. | число | 5 | O | R | |
| sackDelay                                        | Время ожидания отправки сообщения SACK. | число | 200 | O | R | |
| shutdownEvent                                    | Флаг включения индикации о событии SHUTDOWN от ядра. | логический | true | O | R | |
| assocChangeEvent                                 | Флаг включения индикации об изменении состояния ассоциации от ядра. | логический | false | O | R | |
| peerAddrChangeEvent                              | Флаг включения индикации об изменении состояния peer в ассоциации от ядра. | логический | false | O | R | |
| sndBuf                                           | Размер буфера сокета для отправки, `net.core.wmem_default`.pass:q[\<br\>]**Внимание.** Значение удваивается, удвоенный размер не может превышать `net.core.wmem_max`. | число | значения kernel | O | R | |
| ipMtuDiscover                                    | Флаг разрешения использования алгоритма Path MTU Discovery.pass:q[\<br\>]`-1` - нет изменений;pass:q[\<br\>]`0` - алгоритм не используется, флаг заголовка IP `Don't fragment (DF)` не активирован;pass:q[\<br\>]`1` - алгоритм используется, флаг заголовка IP `Don't fragment (DF)` активирован.| число | -1 | O | R | |
| dscp                                             | Значение поля заголовка IP DSCP/ToS. | число | -1 | O | R | |
| txBufStatistics | Флаг включения вывода в `si.log` о состоянии sctp-буфера.pass:q[\<br\>]**Примечание.** Должна быть включена запись журнала **si.log**, для чего достаточно задать `level = 1`. Информация выводится при заполнении SCTP-буфера. | логический | false | O | R | |
| warningTxBufSize | Порог для вывода сообщений SCTP-буфера в журнал **warning.log** в байтах. | число | 1024 | O | R | |
| [[critical]]criticalTxBufSize | Порог для срабатывания индикации о переполнении SCTP-буфера в байтах. | число | 0 | O | R | |
| onCriticalTxBufSize | Действие по достижении [criticalTxBufSize](#critical).pass:q[\<br\>]`0` - отправка индикации о переполнении на верхний уровень;pass:q[\<br\>]`1` - разрыв SCTP-ассоциации. | число |  0 | O | R | |

### Алгоритмы ###


* 0 — отсутствует: EEA0 для шифрования/дешифрования, EIA0 для проверки целостности;  

* 1 — SNOW3G, см. [3GPP Confidentiality and Integrity Algorithms UEA2 & UIA2. Document 2: SNOW 3G Specification](https://www.gsma.com/aboutus/wp-content/uploads/2014/12/snow3gspec.pdf);

* 2 — AES-128, см. [FIPS 197; Advanced Encryption Standard (AES)](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.197-upd1.pdf);   

* 3 — ZUC, см. [Specification of the 3GPP Confidentiality and Integrity Algorithms 128-EEA3 & 128-EIA3. Document 2: ZUC Specification](https://www.gsma.com/aboutus/wp-content/uploads/2014/12/eea3eia3zucv16.pdf).

#### Пример ####

```json
{
  "localAddress" : {
    "localHost" : "192.168.100.1",
    "localPort" : 29118,
    "localInterfaces" : [ "192.168.100.10:29118", "192.168.101.10:29118", "192.168.102.10:29118" ]
  },
  "timers" : {
    "reconnectTimeout" : 20000,
    "responseTimeout" : 10000
  },
  "security" : {
    "encryptionAlg" : [ 2, 1, 0 ],
    "integrityAlg" : [ 3, 1, 0 ]
  },
  "overload" : {
    "enable" : true,
    "initialUe" : 750,
    "attachReq" : 500
  },
  "balancer" : {
    "enable" : true
  }
}
```