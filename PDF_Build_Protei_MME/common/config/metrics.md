
В этом файле задаются настройки сбора метрик.

### Используемые подсекции ###


* [general](#general) - общие параметры сбора метрик;

* [s1Interface](#s1Interface) - параметры сбора метрик S1;

* [s1Attach](#s1Attach) - параметры сбора метрик S1, связанных с регистрацией абонента;

* [s1Detach](#s1Detach) - параметры сбора метрик S1, связанных с дерегистрацией абонента;

* [s1BearerActivation](#s1BearerActivation) - параметры сбора метрик S1, связанных с активацией bearer-служб;

* [s1BearerDeactivation](#s1BearerDeactivation) - параметры сбора метрик S1, связанных с деактивацией bearer-служб;

* [s1BearerModification](#s1BearerModification) - параметры сбора метрик S1, связанных с модификацией bearer-служб;

* [s1Security](#s1Security) - параметры сбора метрик S1, связанных с аутентификацией абонента;

* [s1Service](#s1Service) - параметры сбора метрик S1, связанных с процедурой Service Req;

* [handover](#handover) - параметры сбора метрик, связанных с процедурой Handover;

* [tau](#tau) - параметры сбора метрик, связанных с процедурой Tracking Area Update;

* [sgsInterface](#sgsInterface) - параметры сбора метрик SGs;

* [svInterface](#svInterface) - параметры сбора метрик Sv (SRVCC HO);

* [resource](#resource) - параметры сбора метрик, связанных с ресурсами хоста;

* [users](#users) - параметры сбора различных счётчиков;

* [s11Interface](#s11Interface) - параметры сбора метрик S11;

* [paging](#paging) - параметры сбора метрик, связанных с процедурой Paging;

* [diameter](#diameter) - параметры сбора метрик, связанных с протоколом Diameter;

* [s6aInterface](#s6a-interface) - параметры сбора метрик, связанных с интерфейсом S6a;

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **[[general]]general** | Общие параметры сбора метрик. | | | O | P |          |
| [[enable]]enable | Флаг сбора метрик. | логический | false | O | P |          |
| [[directory]]directory | Директория для хранения метрик. | строка | ../metrics | O | P |          |
| [[granularity]]granularity | Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | 5 | O | P |          |
| [[node-name]]nodeName | Имя узла, указываемое в названии файла с метриками. | строка | | O | P |          |
| [[hh-mm-separator]]hhMmSeparator | Флаг включения разделителя между часами и минутами HH:MM.          | логический | false | O |  P |          |
| **[[s1Interface]]s1Interface** | Параметры для метрик S1. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1Attach]]s1Attach** | Параметры для метрик S1, связанных с регистрацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1Detach]]s1Detach** | Параметры для метрик S1, связанных с дерегистрацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1BearerActivation]]s1BearerActivation** | Параметры для метрик S1, связанных с активацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1BearerDeactivation]]s1BearerDeactivation** | Параметры для метрик S1, связанных с деактивацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1BearerModification]]s1BearerModification** | Параметры для метрик S1, связанных с модификацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1Security]]s1Security** | Параметры для метрик S1, связанных с аутентификацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s1Service]]s1Service** | Параметры для метрик S1, связанных с процедурой Service Request. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[handover]]handover** | Параметры для метрик, связанных с процедурой Handover. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[tau]]tau** | Параметры для метрик, связанных с процедурой Tracking Area Update. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[sgsInterface]]sgsInterface** | Параметры для метрик SGs. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[svInterface]]svInterface** | Параметры для метрик Sv (SRVCC HO). | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[resource]]resource** | Параметры для метрик, связанных с ресурсами хоста. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[users]]users** | Параметры сбора различных счётчиков. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s11Interface]]s11Interface** | Параметры для метрик S11. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[paging]]paging** | Параметры для метрик, связанных с процедурой Paging. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[diameter]]diameter** | Параметры для метрик Diameter. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **[[s6a-interface]]s6aInterface** | Параметры для метрик S6a. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | числоpass:q[\<br\>]мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |

### Пример ###

```json
{
  "general" : {
    "enable" : true,
    "granularity" : 30,
    "directory" : "/usr/protei/Protei_MME/metrics",
    "nodeName" : "MME-1"
  },
  "diameter" : {
    "enable" : true,
    "granularity" : 15
  },
  "s6aInterface" : {
    "enable" : true,
    "granularity" : 1
  }
}
```