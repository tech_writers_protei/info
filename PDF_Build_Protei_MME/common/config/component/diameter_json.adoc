= component/diameter.json

В файле задаются настройки компонента Diameter.

Ключ для перезагрузки — *reload component/diameter.json*.

== Компонента DIAM

.Описание секций компоненты DIAM
[cols=",,",options="header",]
|===
|Параметр |Описание |O/M
|peerTable |Таблица хостов. |M
|routingTable |Таблица realm. |O
|defaultPcsm |Список PCSM по умолчанию. |O
|===

.Описание секции peerTable компоненты DIAM
[width="100%",cols="17%,52%,10%,9%,3%,3%,6%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|hostIdentity |Идентификатор хоста. |строка |- |M |P |
|pcsm |Адрес PCSM. |строка |- |M |P |
|remoteInterfaces |Адрес хоста. |массив строк |- |O |P |
|weight |Вес PCSM. |число |1 |O |P |
|===

* Можно задавать несколько секций в peerTable с одинаковым hostIdentity, поскольку имеется дополнительный параметр remoteInterfaces, связывающий адреса подключающихся клиентов с нужным PCSM.
* Вес используется для распределения нагрузки между PCSM с одинаковым hostIdentity. При отправке сообщения по хосту каждый PCSM с подходящим hostIdentity может быть выбран с вероятностью `weight / totalWeight`, где *totalWeight* — сумма весов всех PCSM с данным hostIdentity.

.Пример
[source,json]
----
{
  "diam": {
    "peerTable": [
      {
        "hostIdentity": "mme.vlr.com",
        "pcsm": "Sg.DIAM.PCSM.1",
        "remoteInterfaces": ["192.168.115.231"]
      },
      {
        "hostIdentity": "mme.vlr.com",
        "pcsm": "Sg.DIAM.PCSM.2",
        "remoteInterfaces": "192.168.115.232:3868"
      },
      {
        "hostIdentity": "mme.vlr.com",
        "pcsm": "Sg.DIAM.PCSM.3",
        "remoteInterfaces": [ "192.168.115.233", "192.168.115.234" ]
      },
      {
        "hostIdentity": "mme.vlr.com",
        "pcsm": "Sg.DIAM.PCSM.4",
        "remoteInterfaces": [":3869"]
      },
      {
        "hostIdentity": "mme.vlr.com",
        "pcsm": "Sg.DIAM.PCSM.5"
      }
    ]
  }
}
----

* На PCSM.1 принимаются подключения с IP-адреса *192.168.115.231*, с любого порта;
* На PCSM.2 принимаются подключения с IP-адреса *192.168.115.232*, с порта *3868*;
* На PCSM.3 принимаются подключения с IP-адресов *192.168.115.233* и *192.168.115.234*, с любого порта;
* На PCSM.4 принимаются подключения с любого IP-адреса, с порта *3869*;
* На PCSM.5 принимаются все подключения.

.Описание секции routingTable компоненты DIAM
[width="100%",cols="14%,36%,22%,13%,4%,4%,7%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|realm |Идентификатор направления. |строка |- |M |P |
|route |Перечень хостов. |массив объектов |- |M |P |
|===

.Формат route
[width="100%",cols="18%,29%,10%,18%,6%,6%,13%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|peer |Идентификатор хоста. |строка |- |M |P |
|weight |Вес хоста. |число |1 |O |P |
|priority |Приоритет хоста. |число |1 |O |P |
|===

* Чем меньше значение *priority*, тем более приоритетным является хост. Сначала маршрут выбирается из хостов с наивысшим приоритетом. При их недоступности выбор производится из хостов со следующим приоритетом и т.д.
* *weight* используется для распределения нагрузки между хостами с одинаковым приоритетом. Каждый хост может быть выбран с вероятностью `weight / totalWeight`, где *totalWeight* - сумма весов всех хостов с данным приоритетом.

.Пример
[source,json]
----
{
  "routingTable": [
    {
      "realm": "example2.realm",
      "route": [
        {
          "peer": "host1",
          "weight": 2
        },
        {
          "peer": "host2",
          "weight": 3
        }
      ]
    }
  ]
}
----

*Примечание.* Распределение нагрузки между `host1` и `host2` в соотношении 2:3.

[source,json]
----
{
  "routingTable": [
    {
      "realm": "example3.realm",
      "route": [
        {
          "peer": "host1",
          "priority": 1
        },
        {
          "peer": "host2",
          "priority": 2
        }
      ]
    }
  ]
}
----

*Примечание.* Отправка сообщений на `host2` выполняется только в случае недоступности `host1`.

[source,json]
----
{
  "routingTable": [
    {
      "realm": "example4.realm",
      "route": [
        {
          "peer": "host1",
          "priority": 1
        },
        {
          "peer": "host2",
          "priority": 2
        }
      ]
    }
  ]
}
----

.Описание секции defaultPcsm компоненты DIAM
[width="100%",cols="16%,33%,10%,17%,6%,6%,12%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|pcsm |Компонентный адрес PCSM. |строка |- |M |P |
|weight |Вес хоста. |число |1 |O |P |
|priority |Приоритет хоста. |число |1 |O |P |
|===

* Чем меньше значение *priority*, тем более приоритетным является PCSM. Сначала маршрут выбирается из PCSM с наивысшим приоритетом. При их недоступности выбор производится из хостов со следующим приоритетом и т.д.
* *weight* используется для распределения нагрузки между хостами с одинаковым приоритетом. Каждый хост может быть выбран с вероятностью `weight / totalWeight`, где *totalWeight* — сумма весов всех хостов с данным приоритетом.

.Пример
[source,json]
----
{
  "defaultPcsm": [
    {
      "pcsm": "Sg.DIAM.PCSM.0",
      "priority": 1,
      "weight": 2
    },
    {
      "pcsm": "Sg.DIAM.PCSM.1",
      "priority": 1,
      "weight": 3
    },
    {
      "pcsm": "Sg.DIAM.PCSM.0",
      "priority": 2
    },
    {
      "pcsm": "Sg.DIAM.PCSM.1",
      "priority": 2
    }
  ]
}
----

== Компонента DIAM.PCSM

.Описание параметров компоненты DIAM.PCSM
[width="100%",cols="14%,58%,2%,4%,21%,,1%",options="header",]
|===
|Параметр |Описание |Тип |Применимость |По умолчанию |O/M |Версия
|pcsmAddress |Компонентный адрес PCSM. |строка |tcp/sctp, client | |M |
|peerIp |Адрес сервера. |ip |tcp/sctp, client |0.0.0.0 |M |
|peerPort |Порт сервера. |число |tcp/sctp, client |0 |M |
|srcIp |Локальный адрес. |ip |tcp/sctp, client |- |O |
|srcPort |Локальный порт. |число |tcp/sctp, client |- |O |
|originState |Идентификатор состояния, `Origin-State`. |число |tcp/sctp, client/server |- |O |
|originHost |Идентификатор хоста, `Origin-Host`. |строка |tcp/sctp, client/server |значение link:../diameter#origin-host-diam[diameter.json :: +
[localPeerCapabilities] :: +
originHost] |O |
|originRealm |Realm хоста, `Origin-Realm`. |строка |tcp/sctp, client/server |значение link:../diameter#origin-realm-diam[diameter.json :: +
[localPeerCapabilities] :: +
originRealm] |O |
|transport |Протокол транспортного уровня. +
`tcp` / `sctp`. |строка |tcp/sctp, client |tcp |O |
|inStreams |Количество входящих потоков. |число |sctp, client |2 |O |
|outStreams |Количество исходящих потоков. |число |sctp, client |2 |O |
|maxInitRetransmits |Максимальное количество попыток отправить сообщение INIT/COOKIE ECHO. |число |sctp, client |- |O |
|initTimeout |Время ожидания сообщения INIT. |число |sctp, client |- |O |
|rtoMax |Максимальное значение RTO. |число |sctp, client |- |O |
|rtoMin |Минимальное значение RTO. |число |sctp, client |- |O |
|rtoInitial |Начальное значение RTO. |число |sctp, client |- |O |
|hbInterval |Периодичность отправки сигнала heartbeat. |число |sctp, client |- |O |
|dscp |Значение поля заголовка IP DSCP/ToS. |число |tcp/sctp, client |- |O |
|associationMaxRetrans |Максимальное количество повторных отправок, при превышении которого маршрут считается недоступным. |число |sctp, client |- |O |
|sackDelay |Время ожидания отправки сообщения SACK. |число |sctp, client |- |O |
|sndBuf |Размер буфера сокета для отправки, параметр `net.core.wmem_default` Linux Kernel. +
*Внимание.* Значение удваивается. Удвоенный размер не может превышать значение `net.core.wmem_max`. |число |sctp, client |- |O |
|shutdownEvent |Флаг включения индикации о событии `SHUTDOWN` от ядра. |логический |sctp, client |- |O |
|assocChangeEvent |Флаг включения индикации об изменении состояния ассоциации от ядра. |логический |sctp, client |- |O |
|peerAddrChangeEvent |Флаг включения индикации об изменении состояния peer в ассоциации от ядра. |логический |sctp, client |- |O |
|localInterfaces |Перечень локальных интерфейсов. Дополнительную информацию по конфигурированию см. link:#LocalAndRemoteAddress[localAddress]. Формат: +
`[ "<ip:port>", "<ip:port>" ]` |массив строк |sctp, client |- |O |
|remoteInterfaces |Перечень удаленных интерфейсов. Дополнительную информацию по конфигурированию см. link:#LocalAndRemoteAddress[remoteAddress]. Формат: +
`[ "<ip:port>", "<ip:port>" ]` |массив строк |sctp, client |- |O |
|applTimeout |Максимальное время ожидания установления Diameter-соединения. +
*Примечание.* Отсчитывается с момента отправки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer. |число |tcp/sctp, client/server |значение link:../diameter#appl-timeout[diameter.json :: +
[Timers] :: +
applTimeout] |O |
|watchdogTimeout |Максимальное время ожидания сообщений Diameter: Device-Watchdog-Request/Answer. +
*Примечание.* Учитывается время прошедшее с момента посылки последнего сообщения, не обязательно Diameter: DWR. |число |tcp/sctp, client/server |значение link:../diameter#watchdog-timeout[diameter.json :: +
[timers] :: +
watchdogTimeout] |O |
|reconnectTimeout |Максимальное время ожидания переустановления соединения. +
*Примечание.* Учитывается время от разрушения соединения до очередной попытки восстановления. |число |tcp/sctp, client/server |значение link:../diameter#reconnect-timeout[diameter.json :: +
[timers] :: +
reconnectTimeout] |O |
|onBusyReconnectTimeout |Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = BUSY`. +
*Примечание.* Если 0, то соединение не переустанавливается. |число |tcp/sctp, client/server |значение link:../diameter#on-busy-reconnect-timeout[diameter.json :: +
[timers] :: +
onBusyReconnectTimeout] |O |
|onShutdownReconnectTimeout |Максимальное время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU`. +
*Примечание.* Если 0, то соединение не переустанавливается. |число |tcp/sctp, client/server |значение link:../diameter#on-shutdown-reconnect-timeout[diameter.json :: +
[timers] :: +
onShutdownReconnectTimeout] |O |
|responseTimeout |Максимальное время ожидания ответа. |число |tcp/sctp, client/server |значение link:../diameter#response-timeout[diameter.json :: +
[timers] :: +
responseTimeout] |O |
|breakdownTimeout |Продолжительность временной недоступности PCSM. |число |tcp/sctp, client/server |значение link:../diameter#breakdown-timeout[diameter.json :: +
[timers] :: +
breakdownTimeout] |O |
|statisticTimeout |Периодичность записи статистики в лог-файлы. |число |tcp/sctp, client/server |значение link:../diameter#statistic-timeout[diameter.json :: +
[timers] :: +
statisticTimeout] |O |
|[[trafficManagerInterval]]trafficManagerInterval |Период подсчета количества входящих и исходящий запросов. |число |tcp/sctp, client/server |1000 |O |
|maxTransactions |Максимальное количество запросов за период подсчета запросов, link:#TrafficManagerInterval[trafficManagerInterval]. +
*Примечание.* Если 0, то ограничение не проверяется. +
На входящие запросы сверх лимита отправляется ответ с `Result-Code = TOO_BUSY (3002)`. На исходящие запросы сверх лимита в логику отправляется Pr_DIAM_SEND_DATA_REJ. |число |tcp/sctp, client/server |0 |O |
|===

=== [[LocalAndRemoteAddress]]Конфигурация local и remote адресов

Для серверных компонент IP-адрес, порт и `localInterfaces` используются соответствующие значения из файла link:../../diameter/[diameter.json]:

* link:../../diameter/#local_host[diameter.json :: [localAddress] :: localHost]
* link:../../diameter/#local_port[diameter.json :: [localAddress] :: localPort]
* link:../../diameter/#local_interfaces[diameter.json :: [localAddress] :: localInterfaces]

Параметры из этого файла для них игнорируются.

Компонента является клиентом, если:

* указан *peerIp* для tcp;
* указан *peerIp* или *remoteInterfaces* для sctp.

Адреса клиентских компонент составляются следующим образом для TCP в порядке приоритетности:

* Для удаленных адресов, *RemoteAddr*:
** ip - peerIp;
** port - peerPort;
* Для локальных адресов, *LocalAddr*:
** ip - srcIp, link:../../diameter/#local_host[diameter.json :: [localAddress] :: localHost];
** port - srcPort;

Для клиентских SCTP-ассоциаций `remoteInterfaces` и `localInterfaces` являются набором 1 основной + дополнительные адреса.

Основной адрес для SCTP определяется следующим образом в порядке приоритетности:

* Для удаленных адресов, *remoteAddr*:
** ip - PeerIP, первый адрес в списке `remoteInterfaces`;
** port - PeerPort, первый порт в `remoteInterfaces`;
* Для локальных адресов, *localAddr*:
** ip - SrcIP, первый адрес в списке `localInterfaces`, link:../../diameter/#local_host[diameter.json :: [localAddress] :: localHost];
** port - SrcPort, первый порт в `localInterfaces`;

=== Обязательность параметров клиентских компонент

[width="100%",cols="21%,17%,62%",options="header",]
|===
|Параметр |Клиент TCP |Клиент SCTP
|peerIp |Обязательный |Обязательный, если не указан `ip` в `remoteInterfaces`
|peerPort |Обязательный |Обязательный, если не указан `port` в `remoteInterfaces`
|srcIP |Опциональный |Опциональный
|srcPort |Опциональный |Опциональный
|localInterfaces |Не используется |Опциональный
|remoteInterfaces |Не используется |Обязательный, если не указан `peerIp`/`peerPort`
|===
