= diameter.json

В файле задаются настройки подключений по протоколу Diameter.

Ключ для перезагрузки — *reload diameter*.

== Используемые секции

* *<<general-diameter,[general]>>* -- основные параметры протокола Diameter;
* *<<local_address,[localAddress]>>* -- параметры локального интерфейса;
* *<<local-peer-capabilities,[localPeerCapabilities]>>* -- параметры локальных peer;
* *<<specific-local-peer-capabilities,[specificLocalPeerCapabilities]>>* -- параметры списка LocalPeerCapabilities, специфичных для отдельного PCSM;
* *<<timers-diameter,[timers]>>* -- параметры таймеров;
* *<<resend,[resend]>>* -- параметры повторной отправки сообщения;

.Описание параметров
[width="99%",cols="34%,33%,6%,11%,4%,4%,8%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|*[[general-diameter]][general]* |Основные параметры протокола Diameter. |объект |- |O |R |
|caseSensitive |Флаг сохранения регистра в строковых AVP вместо приведения к нижнему регистру. |логический |true |O |R |
|receivingFromAnyHost |Флаг обработки запросов, если `Destination-Host` не совпадает с `Local-Host`. |логический |false |O |R |
|maxTimeoutCount |Количество истекших таймеров ожидания ответа, после которого хост считается занятым. |число |10 |O |R |
|useResend |Флаг использования перепосылки по истечении времени ожидания. |логический |false |O |R |
|necromancy |Флаг отправки запросов на занятый хост, если свободные не найдены. |логический |false |O |R |
|unsupportedCommand |Флаг обработки сообщений, которые явно не поддерживаются в библиотеке. |логический |false |O |R |
|replaceOriginIdentities |Флаг подмены `Origin-Host` и `Origin-Realm` на указанные в PCSM значения во всех отправляемых сообщениях. |логический |false |O |R |
|pcsmCreationTimer |Таймер, по истечению которого DIAM переходит в активное состояние не дожидаясь создания оставшихся PCSM. |число +
мс |2000 |O |R |
|*[[local_address]][localAddress]* |Параметры локального интерфейса. |объект |- |M |P |
|[[local_host]]localHost |Адрес локального хоста. |ip |0.0.0.0 |M |P |
|[[local_port]]localPort |Номер локального порта. |число |3868 |M |P |
|transport |Используемый транспортный протокол. +
`tcp` / `sctp`. |строка |tcp |O |P |
|[[local_interfaces]]localInterfaces |Перечень IP-адресов для мультихоуминга. Формат: +
`<ip>:<port>` |массив строк |- |O |P |
|inStreams |Количество входящих SCTP-потоков. |число +
1-65 536 |1 |O |P |
|outStreams |Количество исходящих SCTP-потоков. |число +
1-65 536 |1 |O |P |
|maxInitRetransmits |Количество попыток отправки сообщения INIT, прежде чем хост считать недоступным. |число +
мс |10 |O |R |
|initTimeout |Время ожидания сообщения INIT_ACK. |число +
мс |1000 |O |R |
|rtoMax |Максимальное значение RTO. |число +
мс |60 000 |O |R |
|rtoMin |Минимальное значение RTO. |число +
мс |1000 |O |R |
|rtoInitial |Начальное значение RTO. |число +
мс |3000 |O |R |
|hbInterval |Период посылки сигнала heartbeat. |число +
мс |30 000 |O |R |
|maxRetrans |Максимальное количество попыток отправки сообщения на адрес. |число |5 |O |R |
|dscp |Значение поля заголовка IP DSCP/ToS. |число |-1 |O |R |
|associationMaxRetrans |Максимальное количество повторных отправок, после которых маршрут считается недоступным. |число |10 |O |R |
|sackDelay |Время ожидания отправки сообщения SACK. |число |- |O |R |
|sndBuf |Размер буфера сокета для отправки, `net.core.wmem_default`. +
*Внимание.* Значение удваивается, удвоенный размер не может превышать `net.core.wmem_max`. |число |- |O |R |
|shutdownEvent |Флаг включения индикации о событии SHUTDOWN от ядра. |логический |- |O |R |
|assocChangeEvent |Флаг включения индикации об изменении состояния ассоциации от ядра. |логический |- |O |R |
|peerAddrChangeEvent |Флаг включения индикации об изменении состояния peer в ассоциации от ядра. |логический |- |O |R |
|*[[local-peer-capabilities]][localPeerCapabilities]* |Параметры локальных peer. |объект |- |O |R |
|[[origin-host-diam]]originHost |Идентификатор хоста, `Origin-Host`. |строка |- |M |R |
|[[origin-realm-diam]]originRealm |Realm хоста, `Origin-Realm`. |строка |- |M |R |
|vendorId |Идентификатор производителя, `Vendor-Id`. |число |- |M |R |
|productName |Название системы, `Product-Name`. |строка |- |M |R |
|firmwareRevision |Версия программного обеспечения, `Firmware-Revision`. |число | |O |R |
|originStateId |Идентификатор состояния, `Origin-State-Id`. +
*Примечание.* Если не задан, то каждый раз при перезагрузке `Origin-State-Id` принимает уникальное значение. Задается конкретное значение, чтобы удаленные пиры не инициировали сброс сессий при перезагрузке программного обеспечения. |число |0 |O |R |
|hostIpAddress |Перечень локальных адресов, `Host-IP-Address`. Формат: +
`{"address": <строка>, "family": <число> }`. +
`1` -- `IPv4`, `2` -- `IPv6`, `8` -- `E164`. По умолчанию: 1. |массив объектов |- |M |R |
|authApplicationId |Перечень идентификаторов поддерживаемых приложений, `Auth-Application-Id`. |массив чисел |- |O |R |
|acctApplicationId |Перечень идентификаторов поддерживаемых аккаунтинговых приложений, `Acct-Application-Id`. |массив чисел |- |O |R |
|vendorSpecificApplicationId |Перечень идентификаторов приложений, определяемых вендором, `Vendor-Specific-Application-Id`. Формат см. link:#vendor_specific_application_id[ниже]. |массив объектов |- |O |R |
|inbandSecurityId |Перечень идентификаторов поддерживаемых механизмов обеспечения безопасности, `Inband-Security-Id`.  +
*Примечание.* Поддерживается только `0, NO_SECURITY`. |массив чисел |- |O |R |
|supportedVendorId |Перечень идентификаторов поддерживаемых производителей, `Supported-Vendor-Id`. +
*Примечание.* Используется только для формирования сообщения Diameter: Capabilities-Exchange-Request. |массив чисел |- |O |R |
|drmp |Значение параметра Diameter Routing Message Priority |число 0-15 | |O |R |
|forceDestinationHost |Код формата заполнением AVP Destination-Host. +
`0` - режим обратной совместимости, очищение `Destination-Host` только при повторной отправке запроса; +
`1` - режим без изменения `Destination-Host`; +
`2` - режим, при котором очищается значение `Destination-Host` при не совпадении со значением из PCSM; +
`3` - режим, при котором значение `Destination-Host` заменяется значением из PCSM, если значения не совпадают или AVP отсутствует. |число |0 |O |R |
|*[[specific-local-peer-capabilities]][specificLocalPeerCapabilities]* |Перечень LocalPeerCapabilities, специфичных для отдельного PCSM. Формат: +
`{ "pcsmAddress": <pcsmAddress> , "localPeerCapabilities":<localPeerCapabilities> }`. +
*Примечание.* Формат <localPeerCapabilities> аналогичен link:#local-peer-capabilities[[localPeerCapabilities]]. |массив объектов |- |O |R |
|*[[timers-diameter]][timers]* |Параметры таймеров. |объект |- |O |R |
|[[appl-timeout]]applTimeout |Время ожидания установления Diameter-соединения. +
*Примечание.* Отсчитывается с момента посылки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer. |число +
мс |40 000 |O |R |
|[[watchdog-timeout]]watchdogTimeout |Время ожидания посылки сообщений Diameter: Device-Watchdog-Request/Answer, контроль состояния соединения. +
*Примечание.* Отсчитывается с момента посылки последнего сообщения, не обязательно Diameter: DWR. |число +
мс |10 000 |O |R |
|[[reconnect-timeout]]reconnectTimeout |Время ожидания переустановления соединения. +
*Примечание.* Отсчитывается время от разрушения соединения до очередной попытки восстановления соединения. |число +
мс |30 000 |O |R |
|[[on-busy-reconnect-timeout]]onBusyReconnectTimeout |Время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = BUSY (1)`. +
*Примечание.* Если 0, то соединение не переустанавливается. |число +
мс |60 000 |O |R |
|[[on-shutdown-reconnect-timeout]]onShutdownReconnectTimeout |Время ожидания переустановления соединения после получения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU (2)`. +
*Примечание.* Если 0, то соединение не переустанавливается. |число +
мс |0 |O |R |
|[[response-timeout]]responseTimeout |Время ожидания ответа. |число +
мс |10 000 |O |R |
|[[breakdown-timeout]]breakdownTimeout |Время временной недоступности узла PCSM. |число +
мс |30 000 |O |R |
|[[statistic-timeout]]statisticTimeout |Период вывода статистики в лог-файлы. |число +
мс |60 000 |O |R |
|*[[resend]][resend]* |Параметры повторной отправки сообщения. |объект |- |O |R |
|resetCountForSetBusyTimeout |Период сброса счетчиков link:#count_for_set_busy[countForSetBusy]. |число +
мс |10 000 |O |R |
|resendInfo |Параметры соответствия кодов `Result-Code` и деактивации узла PCSM. Формат см. link:#resend_info[ниже] |массив объектов |- |O |R |
|*[[cdr]][cdr]* |Параметры diam_cdr |объект |- |O |R |
|additionalFields |Конфигурация дополнительных полей журнала diam_cdr. Массив объектов вида `{ "avpName" : <строка>, "avpPath": <avpPath>, "messageType": <messageType>}` выводятся только общие поля |массив объектов | |O |R |
|avpPath |Список пар-идентификаторов AVP (путь) до нужной AVP. AVP идентифицируется парой значений: Id (mandatory) и VendorId (optional). Массив объектов вида `{"id" : <число>, "vendorId" : <число> }` |массив объектов | |O |R |
|messageType |Тип сообщения answer/request |строка |"request" |O |R |
|===

=== [[vendor_specific_application_id]]Формат vendorSpecificApplicationId

Поле vendorSpecificApplicationId может иметь эначения формата

[source,json]
----
{
  "vendorId": <vendor_id>,
  "authApplicationId": <auth_application_id>
}
----

или

[source,json]
----
{
  "vendorId": <vendor_id>,
  "acctApplicationId": <acct_application_id>
}
----

=== [[resend_info]]Формат resendInfo

[width="99%",cols="37%,36%,7%,12%,4%,4%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R
|[[result_code]]resultCode |Значение поля `Result-Code`, при котором совершается повторная отправка сообщений на альтернативный узел PCSM. |число |- |M |R
|[[count_for_set_busy]]countForSetBusy |Количество ответов с указанным значением link:#result_code[resultCode], при котором PCSM становится неактивным. +
*Примечание.* Счетчик сбрасывается при получении ответа с не указанным в перечне `ResultCode`. |число |- |M |R
|===

*Примечание.* `ResultCode = 3004 (TOO_BUSY)` и `CountForSetBusy = 1` добавляется автоматически, если не задан явно.

.Пример
[source,json]
----
{
  "specificLocalPeerCapabilities": [
    {
      "pcsmAddress": "Sg.DIAM.PCSM.2",
      "localPeerCapabilities": {
        "originRealm": "protei.iot1.com",
        "hostIpAddress": [
          "192.168.108.36"
        ],
        "authApplicationId": [
          16777217
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777217
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.4 ",
      "localPeerCapabilities": {
        "originRealm": "protei.ru",
        "hostIpAddress": [
          "192.168.112.165"
        ],
        "authApplicationId": [
          16777216
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777216
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    }
  ]
}
----
