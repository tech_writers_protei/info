
В этом файле задаются настройки сетей, обслуживаемых MME.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload plmn.json**.

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| [[mcc]]mcc | Мобильный код страны. | строка | - | M | R | |
| [[mnc]]mnc | Код мобильной сети. | строка | - | M | R | |
| [[mmegi]]mmegi | Значение MME Group ID.pass:q[\<br\>]См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf). | число | - | M | R | |
| [[mmec]]mmec | Значение MME Code.pass:q[\<br\>]См. [3GPP TS 23.003](https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/17.09.00_60/ts_123003v170900p.pdf). | число | - | M | R | |
| eUtran | Флаг поддержки сети E-UTRAN. | логический | true | O | R | |
| utran | Флаг поддержки сети UTRAN. | логический | true | O | R | |
| geran | Флаг поддержки сети GERAN. | логический | true | O | R | |
| originHost | Значение `Origin-Host` для протокола Diameter. | строка | mmec[\<MMEC\>](#mmec).mmegi[\<MMEGI\>](#mmegi).pass:q[\<br\>]mme.epc.mnc[\<MNC\>](#mnc).mcc[\<MCC\>](#mcc).pass:q[\<br\>]3gppnetwork.org | O | R | |
| originRealm | Значение `Origin-Realm` для протокола Diameter. | строка | epc.mnc[\<MNC\>](#mnc).pass:q[\<br\>]mcc[\<MCC\>](#mcc).3gppnetwork.org | O | R | |
| authentication | Флаг включения аутентификации. | логический | true | O | R | |
| authVectorsNum | Количество запрашиваемых векторов аутентификации. | числоpass:q[\<br\>]1-7 | 1 | O | R | |
| checkImei | Флаг включения проверки IMEI на узле EIR. | логический | false | O | R | |
| allowGreyImei | Флаг разрешения подключения телефона с неизвестным IMEI. | логический | true | O | R | |
| eirHost | Значение `Destination-Host` для узла EIR. | строка | - | O | R | |
| eirRealm | Значение `Destination-Realm` для узла EIR. | строка | - | O | R | |
| [[psm]]psm | Флаг разрешения режима Power Saving Mode.pass:q[\<br\>]См. [3GPP TS 23.682](https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/12.02.00_60/ts_123682v120200p.pdf). | логический | false | O | R | |
| t3324 | Таймер T3324.pass:q[\<br\>]См. [3GPP TS 24.008](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf).pass:q[\<br\>]**Примечание.** Используется только при `PSM = 1`. | числоpass:q[\<br\>]с | значение T3324 абонента | O | R | |
| t3402 | Таймер T3402.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 0 | O | R | |
| t3402Rej | Таймер T3402 для абонентов, не попавших в белый список IMSI. | числоpass:q[\<br\>]с | 0 | O | R | |
| t3412 | Таймер T3412.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 60 | O | R | |
| [[t3412-ext]]t3412Ext | Таймер T3412 Extended.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | не определено | O | R | |
| t3412ExtSource | Приоритетный источник таймера [T3412 Extended](#t3412-ext).pass:q[\<br\>]`mme`/`ue`. | строка | ue | O | R | |
| [[t3413]]t3413 | Таймер T3413.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 10 | O | R | |
| t3413Sgs | Таймер T3413 для процедуры S1AP Paging, вызванного процедурой SGsAP Paging. | числоpass:q[\<br\>]с | значение [T3413](#t3413) | O | R | |
| t3422 | Таймер T3422.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 6 | O | R | |
| [[t3450]]t3450 | Таймер T3450.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 6 | O | R | |
| t3450RepeatCount | Количество повторных отправок по таймеру [T3450](#t3450). | числоpass:q[\<br\>]0-5 | 4 | O | R | |
| [[t3460]]t3460 | Таймер T3460.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 6 | O | R | |
| t3460RepeatCount | Количество повторных отправок по таймеру [T3460](#t3460). | числоpass:q[\<br\>]0-5 | 4 | O | R | |
| [[t3470]]t3470 | Таймер T3470.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | числоpass:q[\<br\>]с | 6 | O | R | |
| t3470RepeatCount | Количество повторных отправок по таймеру [T3470](#t3470). | числоpass:q[\<br\>]0-5 | 4 | O | R | |
| pagingRepeatCount | Количество отправок запросов Paging на каждую станцию eNodeB в рамках текущего шага используемой модели пейджинга. | числоpass:q[\<br\>]1-5 | 3 | O | R | |
| pagingModel | Индикатор используемой модели для процедуры Paging.pass:q[\<br\>]`0` — по умолчанию;pass:q[\<br\>]`1` — начинать с последней базовой станции. | число | 0 | O | R | |
| ueActivityTimeout | Время ожидания активности от устройства UE. | числоpass:q[\<br\>]с | 600 | O | R | |
| [[ims-vops]]imsVops | Значение `IMS Voice over PS Session Indicator`.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | логический | false | O | R | |
| vopsApnNi | Перечень значений APN-NI, которые могут быть использованы для VoPS. | массив строк | ims | O | R | |
| pCscfRestoration | Флаг поддержки процедуры P-CSCF Restoration. | логический | true | O | R | |
| resetOnSetup | Флаг отправки сообщения S1AP: RESET после запроса S1AP: S1 SETUP REQUEST. | логический | false | O | R | |
| allowDefaultApn | Флаг разрешения использования APN по умолчанию. | логический | true | O | R | |
| allowOperatorQci | Флаг разрешения использования <abbr title="QoS Class Identifier">QCI</abbr> из диапазона 128-254. | логический | false | O | R | |
| allowExtDrx | Флаг разрешения использования Extended DRX.pass:q[\<br\>]См. [3GPP TS 25.300](https://www.etsi.org/deliver/etsi_ts/125300_125399/125300/17.00.00_60/ts_125300v170000p.pdf). | логический | false | O | R | |
| ptw | Значение Paging Transmission Window для Extended DRX.pass:q[\<br\>]См. [3GPP TS 24.008, 10.5.5.32 Extended DRX parameters](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf). | число | не определено | O | R | |
| eDrx | Значение <abbr title="Extended Discontinuous Reception">eDRX</abbr>.pass:q[\<br\>]См. [3GPP TS 24.008, 10.5.5.32 Extended DRX parameters](https://www.etsi.org/deliver/etsi_ts/124000_124099/124008/17.08.00_60/ts_124008v170800p.pdf). | число | не определено | O | R | |
| relativeMmeCapacity | Относительная емкость узла MME, Relative MME Capacity.pass:q[\<br\>]См. [3GPP TS 36.413](https://www.etsi.org/deliver/etsi_ts/136400_136499/136413/17.05.00_60/ts_136413v170500p.pdf). | число | 255 | O | R | |
| relativeMmeCapacityForTac | Перечень связей Relative MME Capacity и TAC. Формат:pass:q[\<br\>]`[ "<tac>-<capacity>", ... ]` | массив строк | - | O | R | |
| notifyOnPgwAlloc | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении динамического IP-адреса узла PGW для APN. | логический | true | O | R | |
| notifyOnUeSrvcc | Флаг отправки сообщения Diameter: Notify-Answer на узел HSS при задании или изменении флага поддержки SRVCC на устройстве UE. | логический | true | O | R | |
| emmInfo | Флаг отправки сообщения EMM Information.pass:q[\<br\>]См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | логический | false | O | R | |
| forceIdentityReq | Флаг отправки Identity Req вне зависимости от наличия сохраненного ранее IMSI. | логический | false | O | R | |
| cpCiotOpt | Флаг разрешения оптимизации Control Plane CIoT EPS.pass:q[\<br\>]См. [3GPP TS 36.300](https://www.etsi.org/deliver/etsi_ts/136300_136399/136300/14.10.00_60/ts_136300v141000p.pdf). | логический | false | O | R | |
| indirectFwd | Флаг разрешения создания Indirect Data Forwarding Tunnel при хэндовере. | логический | false | O | R | |
| purgeDelay | Время задержки отправки сообщения Diameter: Purge-Request на узел HSS.pass:q[\<br\>]**Примечание.** По умолчанию сообщение не отправляется, а данные не удаляются. | числоpass:q[\<br\>]с | не определено | O | R | |
| requestImeisv | Флаг требования IMEISV от абонента. | логический | true | O | R | |
| useVlr | Флаг использования VLR. | логический | false | O | R | |
| rejectForeignGuti| Флаг запрета перехода на MME абонентов, которые были зарегистрированы в других PLMN на сторонних MME при работе в условиях RAN Sharing. | логический | true | O | R | |
| rejectOnLuFail | Флаг отправки сообщения SGsAP-LOCATION-UPDATE-REJECT в случае неуспеха процедуры SGsAP-Location-Update.pass:q[\<br\>]См. [3GPP TS 29.118](https://www.etsi.org/deliver/etsi_ts/129100_129199/129118/17.00.00_60/ts_129118v170000p.pdf). | логический | false | O | R | |
| moCsfbInd | Флаг отправки сообщения SGsAP-MO-CSFB-Indication при получении запроса Extended Service Request. | логический | false | O | R | |
| srvcc | Флаг поддержки <abbr title="Single Radio Voice Call Continuity">SRVCC</abbr>.pass:q[\<br\>]См. [3GPP TS 29.280](https://www.etsi.org/deliver/etsi_ts/129200_129299/129280/17.00.00_60/ts_129280v170000p.pdf). | логический | false | O | R | |
| netFullname | Полное название сети. | строка | Protei Network | O | R | |
| netName | Краткое название сети. | строка | Protei | O | R | |
| ldn | <abbr title="Local Distinguished Name">LDN</abbr>, отправляемый при активированном SRVCC. | строка | Protei_MME | O | R | |
| allowEmptyMsisdn | Флаг разрешения регистрации абонентов без MSISDN. | логический | false | O | R | |
| allowUnknownImei | Флаг обслуживания устройств, неизвестных для EIR. | логический | false | O | R | |
| s11UliAlwaysSent | Флаг отправки User Location Information в запросе GTP: Modify Bearer Request.pass:q[\<br\>]См. [3GPP TS 29.274](https://www.etsi.org/deliver/etsi_ts/129200_129299/129274/17.08.00_60/ts_129274v170800p.pdf). | логический | false | O | R | |
| useDefaultApnOnFailure | Флаг использования APN из профиля по-умолчанию при отсутствии известных адресов SGW/PGW для APN, запрошенного абонентом. | логический | false | O | R | |
| defaultLacAsTac | Флаг использования LAC, равного текущему TAC, в случае отсутствия соответствия TAC к LAI в перечне ниже. | логический | false | O | R | |
| equivalentPlmns | Перечень эквивалентных PLMN. Формат:pass:q[\<br\>]`[ "<mcc>-<mnc>", ... ]`pass:q[\<br\>]См. [3GPP TS 29.292](https://www.etsi.org/deliver/etsi_ts/129200_129299/129292/17.00.00_60/ts_129292v170000p.pdf). | массив строк | - | O | R | |
| [[emergency-numbers]]emergencyNumbers | Перечень [типов и правил номеров экстренных служб](../rules/emergency_numbers/). | правила | - | O | R | |
| emergencyAttachWoAuth | Флаг разрешения процедуры Emergency Attach без аутентификации. | логический | false | O | R | |
| [[emergency-pdn]]emergencyPdn | Перечень [правил PDN Connectivity](../rules/emergency_pdn), применяемых в случае Emergency Attach. | правила | - | O | R | |
| [timezone](#timezone) | Часовые пояса. | timezone | UTC+3 | O | R | |
| [[apn-rules]]apnRules | Перечень [правил APN](../rules/apn_rules/). | правила | - | O | R | |
| [[tac-rules]]tacRules | Перечень [правил TAC](../rules/tac_rules/). | правила | - | O | R | |
| [[rac-rules]]racRules | Перечень [правил RAC](../rules/rac_rules/). | правила | - | O | R | |
| forbiddenLac | Перечень кодов LAC, из которых переход абонентов запрещен. | массив чисел | - | O | R | |
| [[pgw-ip]]pgwIp | IP-адрес и FQDN узла PGW по умолчанию. Формат:pass:q[\<br\>]`"(<fqdn>)<ip>"`pass:q[\<br\>]**Примечание.** FQDN может отсутствовать. | строка | - | O | R | |
| [[sgw-ip]]sgwIp | IP-адрес и FQDN узла SGW по умолчанию. Формат:pass:q[\<br\>]`"(<fqdn>)<ip>"`pass:q[\<br\>]**Примечание.** FQDN может отсутствовать. | строка | - | O | R | |
| mmeIp | IP-адреса других узлов MME с соответствующими значениями MMEGI и MMEC. Формат:pass:q[\<br\>]`[ "<mmegi>-<mmec>-<ip>", "<mmec>-<ip>", ... ]`pass:q[\<br\>]**Примечание.** Если MMEGI не задан, то используется значение, связанное с этой PLMN. | массив строк | - | O | R | |
| sgsnIp | IP-адреса узлов SGSN с соответствующими контроллерами RNC. Формат:pass:q[\<br\>]`[ "<rnc>-<ip>", ... ]` | массив строк | - | O | R | |
| nriLength | Количество используемых бит идентификатора <abbr title="Network Resource Identifier">NRI</abbr> узла SGSN.pass:q[\<br\>]**Примечание.** Если не задано, то не используется в рамках соответствующей сети PLMN. | число | - | O | R | |
| reattachTimeout | Длительность ожидания сообщения Attach Req при выполнении процедуры Detach с re-attach. | числоpass:q[\<br\>]мс | 500 | O | R | |
| [[imsiRules]]imsiRules | Перечень [правил](../rules/imsi_rules.adoc), связывающих префиксы IMSI с различными параметрами. | правила | - | O | R | |
| ueUsageTypes | Перечень UE Usage Type, связанных с текущим MMEGI.pass:q[\<br\>]См. [3GPP TS 23.401](https://www.etsi.org/deliver/etsi_ts/123400_123499/123401/17.08.00_60/ts_123401v170800p.pdf). | массив чисел | - | O | R | |
| [[foreign_ue_usage_types]]foreignUeUsageTypes | Перечень связей между UE Usage Type и другими MMEGI. | массив объектов | - | O | R | |
| [[zc-rules]]zcRules | Перечень [правил ZoneCodes](../rules/zc_rules/). | правила | - | O | R | |
| [[code-mapping-rules]]codeMappingRules | Перечень [правил Diameter: Result-Code и EMM Cause](../rules/code_mapping_rules/). | правила | - | O | R | |
| [[forbidden-imei-rules]]forbiddenImeiRules | Перечень [правил запрещённых IMEI](../rules/forbidden_imei_rules/). | правила | - | O | R | |
| [tacLai](#tac_lai) | Перечень соответствий TAC к LAI.| массив строк | - | O | R | |

#### [[timezone]]Формат timezone ####

Формат записи часового пояса: `[TACX_]UTC<sign>HH:MM[_DST+Y]`


* Необязательная часть `TAC<X>_` — задает ассоциацию между часовым поясом и TAC, при отсутствии ассоциируется со всеми TAC в данной PLMN:

 * X — код TAC;

* Обязательная часть `UTC<sign><HH>:<MM>` — задает часовой пояс через отклонение от UTC:

 * sign — знак отклонения от UTC, `+` или `-`;

 * HH — количество часов;

 * MM — количество минут;

* Необязательная часть `_DST+<Y>` — задает дополнительное отклонение при переводе часов на летнее время:

 * Y — количество добавленных часов, диапазон: 0-2;

#### [[tac_lai]]tacLai ####

Параметр хранит перечень соответствий TAC к LAI в виде массива строк. Формат:pass:q[\<br\>]`[ "<tac> = [<plmn>-]<lac>", ... ]`pass:q[\<br\>]**Примечание.** Один TAC может быть связан только с одним LAI, один LAI может быть связан со многими TAC.

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| \<tac\>  | Код зоны отслеживания. | число | - | M | R | |
| \<plmn\> | Код сети PLMN. | число | значение [\<MCC\>](#mcc)[\<MNC\>](#mnc) | O | R | |
| \<lac\>  | Код локальной области LAC. | число | - | M | R | |

#### [[foreign_ue_usage_types]]foreignUeUsageTypes ####

Параметр хранит перечень связей между UE Usage Type и MMEGI. Описание связи задаётся объектом со следующими полями:

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| mmegi    | Значение MME Group ID. | число | - | M | R | |
| ueUsageTypes | Список UE Usage Types. | массив чисел | - | M | R | |

#### Адреса PGW и SGW ####

Адреса SGW и PGW выводятся из нескольких источников. Список источников в порядке убывания приоритета:

1. Правила - [apn_rules.json](../rules/apn_rules/) и [tac_rules.json](../rules/tac_rules/)
2. DNS, настройки клиента задаются с помощью [dns.cfg](../dns/)
3. Параметры [SGW_IP](#sgw-ip) и [PGW_IP](#pgw-ip)

### Пример ###

```json
[
  {
    "mcc" : "250",
    "mnc" : "48",
    "utran" : false,
    "mmegi" : 1,
    "mmec" : 1,
    "t3413" : 5,
    "t3412" : 180,
    "t3402" : 10,
    "t3402Rej" : 10,
    "t3450" : 1,
    "t3460" : 1,
    "t3470" : 1,
    "checkImei" : true,
    "emergencyNumbers" : [ "112", "911" ],
    "eirHost" : "eir.epc.mnc01.mcc250.3gppnetwork.org",
    "useVlr" : false,
    "imsVops" : true,
    "allowGreyImei" : false,
    "resetOnSetup" : false,
    "imsiWhitelist" : "WList1",
    "apnRules" : [ "FullRule", "Rule_1" ],
    "tacRules" : [ "Rule_1", "Rule_2"],
    "foreignUeUsageTypes" : [
      {
        "mmegi" : 2,
        "ueUsageTypes" : [ 32, 64 ]
      }
    ],
    "forceIdentityReq" : true,
    "emmInfo" : true,
    "netFullname" : "Protei_Network_999",
    "netName" : "test99",
    "timezone" : "UTC+4",
    "tacLai" : [
      "5 = 55",
      "4 = 25048-42"
    ]
  },
  {
    "mcc" : "001",
    "mnc" : "01",
    "mmegi" : 2,
    "mmec" : 5,
    "geran" : false,
    "authentication" : false,
    "allowGreyImei" : false,
    "forbiddenLac" : [ 10, 13, 27 ],
    "mmeIp" : [ "1-2-192.168.125.182" ],
    "t3324" : 60,
    "t3413" : 1,
    "t3412" : 120,
    "t3412Ext" : 14400,
    "t3412ExtSource" : "mme",
    "t3402" : 5,
    "t3402Rej" : 10,
    "t3450" : 5,
    "t3450RepeatCount" : 4,
    "t3460" : 6,
    "t3460RepeatCount" : 3,
    "t3470" : 1,
    "ueActivityTimeout" : 600,
    "requestImeisv" : true,
    "authVectorsNum" : 2,
    "emergencyNumbers" : [ "112", "911" ],
    "emergencyPdn" : ["EMRG"],
    "originHost" : "mme1.epc.mnc001.mcc001.3gppnetwork.org",
    "originRealm" : "epc.mnc001.mcc001.3gppnetwork.org",
    "eirHost" : "eir.epc.mnc01.mcc250.3gppnetwork.org",
    "vopsApnNi" : [ "ims" ],
    "rejectOnLuFail" : true,
    "resetOnSetup" : false,
    "purgeDelay" : 0,
    "pagingModel" : 1,
    "foreignUeUsageTypes" : [
      {
        "mmegi" : 10,
        "ueUsageTypes" : [4]
      },
      {
        "mmegi" : 11,
        "ueUsageTypes" : [ 7, 8, 9 ]
      }
    ],
    "imsiRules" : [ "imsiRules1", "some_rules" ],
    "codeMappingRules" : [ "Rule_1", "Rule_2"],
    "relativeMmeCapacity" : 100,
    "forceIdentityReq" : true,
    "timezone" : "TAC1_UTC+2",
    "cpCiotOpt" : true,
    "psm" : true
  },
  {
    "mcc" : "250",
    "mnc" : "60",
    "mmegi" : 7,
    "mmec" : 8
  }
]
```