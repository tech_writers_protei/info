
В файле задаются правила, связывающие зоны с <abbr title="Tracking Area Code">TAC</abbr>.  
Имя правила может использоваться в качестве ссылки в параметре [plmn::zcRules](../../plmn/#zc-rules/).

### Описание параметров ###

| Параметр | Описание                                                                                                           | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|--------------------------------------------------------------------------------------------------------------------|--------|--------------|-----|-----|--------|
| name     | Имя правила.                                                                                                       | строка | -            |  M  |  R  |        |
| zc       | Код зоны.                                                                                                          | число  | -            |  M  |  R  |        |
| tac      | Массив [[note]]значений, представляющих TAC. pass:q[\<br\>]Если TAC не задан, то данный ZC применим к любому TAC. | массив | -            |  O  |  R  |        |
**Примечание.** [Значением](#note) является либо число (в т.ч. hex), либо диапазон.

### Пример ###

```json
[
  {
    "name": "Zone1",
    "zc": 1,
    "tac": [
      1,
      5,
      "10-15"
    ]
  },
  {
    "name": "Zone2",
    "zc": 2,
    "tac": [
      "0x14",
      "0x20-0x38",
      60
    ]
  }
]
```