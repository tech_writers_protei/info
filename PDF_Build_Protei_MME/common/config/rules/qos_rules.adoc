= qos_rules.json

В файле задаются правила, связывающие параметры QoS с регулярным выражением APN-NI и конкретным APN-NI для VoPS. +
Имя правила может использоваться в качестве ссылки в параметре link:imsi_rules.adoc#qos-rules[imsiRules::qosRules].

.Описание параметров
[width="100%",cols="21%,17%,8%,24%,8%,8%,14%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|name |Имя правила. |строка |- |M |R |
|qci |Идентификатор класса QoS службы передачи данных по умолчанию. |число |не определено |O |R |
|apnAmbrDl |Максимальная скорость передачи AMBR для DL-направлений. |число +
бит/с |не определено |O |R |
|apnAmbrUl |Максимальная скорость передачи AMBR для UL-направлений. |число +
бит/с |не определено |O |R |
|ueAmbrDl |Максимальная пользовательская скорость передачи AMBR для DL-направлений. |число +
бит/с |не определено |O |R |
|ueAmbrUl |Максимальная пользовательская скорость передачи AMBR для UL-направлений. |число +
бит/с |не определено |O |R |
|pdpAmbrDl |Максимальная скорость передачи AMBR для PDP для DL-направлений. |число +
бит/с |не определено |O |R |
|pdpAmbrUl |Максимальная скорость передачи AMBR для PDP для UL-направлений. |число +
бит/с |не определено |O |R |
|arpPriorityLvl |Максимальный уровень приоритета ARP. |число |не определено |O |R |
|vopsApnNi |Идентификаторы сети APN, которые могут быть использованы для VoPS. |массив строк |[ "ims" ] |O |R |
|apnNi |Маски APN NI. Если значение не указано, то все APN NI подпадают под правило. |массив строк |[] |O |R |
|===

.Пример
[source,json]
----
[
  {
    "name": "Rule1",
    "apnAmbrDl": 100000,
    "ueAmbrDl": 400000,
    "ueAmbrUl": 500000,
    "apnNi": [ "no_vops_apn" ]
  },
  {
    "name": "Rule2",
    "qci": 3,
    "pdpAmbrDl": 100500,
    "arpPriorityLvl": 2,
    "apnNi": [ "internet", "ims" ]
  }
]
----
