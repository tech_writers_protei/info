
В файле задаются правила, связывающие параметры QoS с регулярным выражением APN-NI и конкретным APN-NI для VoPS.  
Имя правила может использоваться в качестве ссылки в параметре [imsiRules::qosRules](../imsi_rules/#qos-rules).

### Описание параметров ###

| Параметр       | Описание                                                                                                                 | Тип                      | По умолчанию  | O/M | P/R | Версия |
|----------------|--------------------------------------------------------------------------------------------------------------------------|--------------------------|---------------|-----|-----|--------|
| name           | Имя правила.                                                                                                             | строка                   | -             | M   | R   |        |
| qci            | Идентификатор класса QoS службы передачи данных по умолчанию.                                                            | число                    | не определено | O   | R   |        |
| apnAmbrDl      | Максимальная скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для DL-направлений.                  | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| apnAmbrUl      | Максимальная скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для UL-направлений.                  | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| ueAmbrDl       | Максимальная пользовательская скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для DL-направлений. | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| ueAmbrUl       | Максимальная пользовательская скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для UL-направлений. | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| pdpAmbrDl      | Максимальная скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для PDP для DL-направлений.          | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| pdpAmbrUl      | Максимальная скорость передачи <abbr title="Aggregate Maximum Bit Rate">AMBR</abbr> для PDP для UL-направлений.          | числоpass:q[\<br\>]бит/с | не определено | O   | R   |        |
| arpPriorityLvl | Максимальный уровень приоритета <abbr title="Allocation and Retention Policy">ARP</abbr>.                                | число                    | не определено | O   | R   |        |
| vopsApnNi      | Идентификаторы сети APN, которые могут быть использованы для VoPS.                                                       | массив строк             | [ "ims" ]     | O   | R   |        |
| apnNi          | Маски APN NI. Если значение не указано, то все APN NI подпадают под правило.                                             | массив строк             | []            | O   | R   |        |

### Пример ###

```json
[
  {
    "name" : "Rule1",
    "apnAmbrDl" : 100000,
    "ueAmbrDl" : 400000,
    "ueAmbrUl" : 500000,
    "apnNi" : [ "no_vops_apn" ]
  },
  {
    "name" : "Rule2",
    "qci" : 3,
    "pdpAmbrDl" : 100500,
    "arpPriorityLvl" : 2,
    "apnNi" : [ "internet", "ims" ]
  }
]
```