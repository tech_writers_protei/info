
В файле задаются правила, связывающие Diameter Result-Code, Error-Diagnostic и EMM Cause.
Имя правила может использоваться в качестве ссылки в параметре [plmn::codeMappingRules](../../plmn/#code-mapping-rules).

### Описание параметров ###

| Параметр        | Описание                                                                                                                                                               | Тип          | По умолчанию | O/M | P/R | Версия |
|-----------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|--------------|-----|-----|--------|
| name            | Имя правила                                                                                                                                                            | строка       | -            |  M  |  R  |        |
| resultCode      | Значение Diameter: Result-Code. См. [RFC 6733](https://tools.ietf.org/html/rfc6733).                                                                                   | массив чисел | -            |  M  |  R  |        |
| errorDiagnostic | Значения `Error Diagnostic`. См. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.05.00_60/ts_129272v170500p.pdf)                         | массив чисел | -            |  O  |  R  |        |
| emmCause        | Причина завершения вызова в LTE-сетях, `EMM Cause`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf). | число        | -            |  M  |  R  |        |

### Пример ###

```json
[
  {
    "name": "Rule1",
    "resultCode": [5421],
    "emmCause": 123
  },
  {
    "name": "Rule2",
    "resultCode": [1, 2, 3],
    "errorDiagnostic": [1, 2, 3],
    "emmCause": 1
  }
]
```