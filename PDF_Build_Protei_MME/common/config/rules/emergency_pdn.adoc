= emergency_pdn.json

В файле задаются настройки PDN-подключений для служб передачи данных, применяемые в случае процедуры Emergency Attach без запроса профиля у узла HSS. Имя секции может использоваться в качестве ссылки в параметре link:../plmn.adoc#emergency-pdn[PLMN::EmergencyPDN].

.Описание параметров
[width="100%",cols="21%,17%,8%,24%,8%,8%,14%",options="header",]
|===
|Параметр |Описание |Тип |По умолчанию |O/M |P/R |Версия
|name |Имя. |строка |- |M |R |
|apnNi |Индикатор сети APN. |строка |- |M |R |
|servedPartyIp |Обслуживаемый статический IP-адрес. +
*Примечание.* Может быть указан один IPv4, один IPv6 или оба одновременно. |массив строк |- |O |R |
|ambr |Показатели скоростей передачи AMBR для DL- и UL-направлений, в б/с. Формат: +
`[ <dl>, <ul> ]`. |массив чисел |[ 0, 0 ] |O |R |
|pdnType |Тип подключения к сети передачи данных. +
`IPv4`/`IPv6`/`IPv4v6`/`IPv4_or_IPv6`. |строка |"IPv4" |O |R |
|qci |Идентификатор класса QoS. |число |0 |O |R |
|priorityLvl |Уровень приоритета. |число |0 |O |R |
|preemptCap |Флаг `Pre-emption Capability`. +
См. https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf[3GPP TS 23.107]. |логический |false |O |R |
|preemptVuln |Флаг `Pre-emption Vulnerability`. +
См. https://www.etsi.org/deliver/etsi_ts/123100_123199/123107/17.00.00_60/ts_123107v170000p.pdf[3GPP TS 23.107]. |логический |false |O |R |
|===

.Пример
[source,json]
----
[
  {
    "name": "Rule_for_IMS",
    "apnNi": "ims",
    "servedPartyIp": [ "127.0.0.1", "::1" ],
    "ambr": [ 100500, 100500 ],
    "pdnType": "IPv4v6",
    "priorityLvl": 1,
    "preemptVuln": true
  },
  {
    "name": "internet_rule",
    "apnNi": "internet",
    "servedPartyIp": ["192.168.126.67"],
    "pdnType": "IPv4",
    "qci": 7,
    "preemptCap": true,
    "preemptVuln": true
  }
]
----
