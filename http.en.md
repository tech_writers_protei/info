---
title: "http.cfg"
description: "Основных параметров HTTP-соединений"
weight: 20
type: docs
---

The file sets the settings for HTTP connections.

**Note.** The file is required.

## Sections used ##

* **[[Server]](#server)** - server parameters;
	* **[[SSL]](#ssl-s)** - secure connection parameters;
	* **[[Authorization]](#auth-s)** - authorization parameters;
	* **[[KeepAlive]](#keep-alive-s)** - parameters of the **keep-alive** procedure;
* **[[Client]](#client)** - client parameters;
	* **[[SSL]](#ssl-c)** - secure connection parameters;
	* **[[Authorization]](#auth-c)** - authorization parameters;
	* **[[AdditionalDir]](#add-dir)** - connection manager parameters;
	* **[[KeepAlive]](#keep-alive-c)** - parameters of the **keep-alive** procedure;

### The server side ###

| Parameter | Description | Type | O/M | P/R | Version |
| --- | --- | --- | --- | --- | --- |
| **[Server]** |  |  |  |  |  |
| ID | The direction ID. | int | M | R |  |
| Address | The IP address being listened to.Default: 0.0.0.0. | string | O | R |  |
| Port | The port being listened to.By default: 80. | int | O | R |  |
| MaxQueue | The maximum length of the request queue per connection for [Persistent](#persistant) mode.By default: 10. | int | O | R |  |
| MaxBufferSize | The maximum size of the buffer for receiving messages, in bits.**Note.** If "-1", then it is not limited to: 4,294,967,295By default: -1. | int | O | R |  |
| RecvBufferSize | The minimum buffer size for receiving messages, in bits.**Note.** If the message exceeds the buffer size, the buffer will increase up to [MaxBufferSize](#maxbuf-s).Depending on the specifics of the applications, it allows you to save resources. For example, working with XML applications on average requires a smaller buffer size than working with MMS.By default: 65,536. | int | O | R |  |
| ActivityTimer | The waiting time for connection activity, in seconds.**Note.** After expiration, if there are no new requests, the connection is terminated, the HTTP_ClientCL object **is** destroyed.By default: 60. | int | O | R |  |
| StreamCount | The number of server threads when connecting via HTTP/2.By default: 4,294,967,295. | int | O | R | 1.3.0.17 |
| **[SSL]** | Secure connection parameters. | object | O | R |  |
| Version | The SSL version.`1` — TLSv1 / `2` — SSLv2 / `3` — SSLv3 / `4` — SSLv23 / `5` — TLSv1.1 / `6` — TLSv1.2.By default: 2. | int | O | R |  |
| CertificatePath | The path to the certificate file with the *.pem extension. | string | M | R |  |
| PrivateKeyPath | The path to the file containing the secret key. | string | M | R |  |
| Ciphers | The list of supported ciphers. Format:`"<cipher>:<cipher>"`.**Note.** For possible values, see the [OpenSSL Cryptography and SSL/TLS Toolkit](https://www.openssl.org/docs/man1.1.1/man1/ciphers.html). | string | O | R |  |
| PreferServerCiphers | The cipher priority flag is on the server side instead of the client.By default: 0. | int | O | R |  |
| **[Authorization]** | Authorization parameters. | object | O | R |  |
| Type | The type of authorization.`1` — Basic / `2` — Digest (not supported yet).By default: 1. | int | O | R |  |
| User | Login for authorization. | string | M | R |  |
| Password | The password for authorization. | string | O | R |  |
| **[KeepAlive]** | Parameters of the keep-alive procedure. | object | O | R |  |
| PingTimeout | The waiting time between procedures, in seconds.By default: 10. | int | O | R |  |
| PingReqNumber | The number of Ping requests to break the connection.By default: 5. | int | O | R |  |

### The client side ###


| Parameter | Description | Type | O/M | P/R | Version |
| --- | --- | --- | --- | --- | --- |
| **[Client]** |  |  |  |  |  |
| ID | The direction ID.**Note.** Control over the uniqueness of identifiers is assigned to the system administrator. | int | M | R |  |
| DestAddress | The IP address of the server to which the requests should be directed. Format:`"ip_address";port`Starting from v1.2.5.1, DNS names are supported. | {string,int}string | M | R |  |
| SrcAddress | The IP address to send the request if multiple network interfaces are configured. | string | O | R |  |
| Persistant | The flag for activating the Persistent connection usage mode.If the value is non-zero, then after receiving a response, the connection remains active during [ActivityTimer](#activity_timer).Pipeline requests are allowed: the client sends requests without waiting for a response to the previous ones.By default: 0. | bool | O | R |  |
| ActivityTimer | The time of connection activity when activating **Persistent** mode, in seconds.**Note.** After the expiration, if no new requests have been received from the logic, the connection is terminated, an error notification is generated for all requests in the queue, then the HTTP_ClientCL object **is** destroyed.By default: 60. | int | O | R |  |
| ResponseTimer | The waiting time for a response from the server, in seconds.**Note.** Upon expiration, if no response is received, the connection is terminated, an error notification is generated for all requests in the queue, then the HTTP_ClientCL object **is** destroyed.By default: 60. | int | O | R |  |
| MaxQueue | The maximum number of queued requests per TCP connection.**Note.** Upon reaching, the interface creates a new HTTP_ClientCL object for the new TCP connection.By default: 10. | int | O | R |  |
| MaxConnection | The maximum number of simultaneous connections to the server.**Note.** Upon reaching, the interface responds with HTTP_ERROR_IND with the error code `OVERLOADED`.By default: 500. | int | O | R |  |
| MaxBufferSize | The maximum size of the buffer for receiving messages, in bits.**Note.** If "-1", then it is unlimited: 4,294,967,295.By default: -1. | int | O | R |  |
| RecvBufferSize | The minimum buffer size for receiving messages, in bits.**Note.** If the message exceeds the buffer size, the buffer will increase up to [MaxBufferSize](#maxbuf-c).Depending on the specifics of the applications, it allows you to save resources. For example, working with XML applications on average requires a smaller buffer size than working with MMS.By default: 65,536. | int | O | R |  |
| HTTP_Version | The HTTP version for creating the connection.`1` – HTTP/1.0, HTTP/1.1 / `2` – HTTP/2.**Note.** When using HTTP/2, Persistent mode must be [activated](#persistant).By default: 1. | int | O | R | 1.3.0.17 |
| StreamCount | The number of streams when connecting via HTTP/2.By default: 4,294,967,295. | int | O | R | 1.3.0.17 |
| **[SSL]** | Secure connection parameters. | object | O | R |  |
| Version | The SSL version.`1` — TLSv1 / `2` — SSLv2 / `3` — SSLv3 / `4` — SSLv23 / `5` — TLSv1.1 / `6` — TLSv1.2.By default: 0. | int | O | R |  |
| SNI_Enabled | The flag for using SNI.By default: 0. | bool | O | R |  |
| ALPN_Enabled | The ALPN usage flag for HTTP/2.By default: 0. | bool | O | R | 1.3.0.17 |
| **[Authorization]** | Authorization parameters. | object | O | R |  |
| User | Login for authorization. | string | M | R |  |
| Password | The password for authorization. | string | O | R |  |
| **[AdditionalDir]** | Connection manager parameters. | object | O | R | 1.3.0.20 |
| ID | A list of alternate destinations. Format:`{ <direction>; <direction> }`. | [int] | M | R |  |
| BreakDownTimeout | The waiting time for the directions to restart, in milliseconds.By default: 30,000. | int | O | R |  |
| MaxErrorCount | The maximum number of possible errors.By default: 1. | int | O | R |  |
| Necromancy | The connection recovery flag.By default: 0. | bool | O | R |  |
| UseLoadSharing | The code for the type of load distribution between directions.`0` — do not use;`1` — distribute among all;`2` — only between the additional ones when the main one is inactive.By default: 0. | int | O | R | 1.2.8.15 |
| **[KeepAlive]** | Parameters for the keep-alive procedure. | object | O | R |  |
| PingTimeout | The waiting time between procedures, in seconds.By default: 10. | int | O | R |  |
| PingReqNumber | The number of Ping requests to disconnect the connection.By default: 5. | int | O | R |  |

#### Example ####

```
[Server]
MaxBufferSize = 524626
ActivityTimer = 10
{
  ID = 0;
  Address = "192.168.228.2";
  Port = 80;
  SSL = {
    Version = 1;
    CertificatePath = "/home/usr/Projects/HTTP_Test/server/server-cert.pem";
    PrivateKeyPath = "/home/usr/Projects/HTTP_Test/server/server-key.pem";
    Ciphers = "HIGH:!MD5:!RC4:!aNULL:!eNULL";
    PreferServerCiphers = 1
  }
}
{
  ID = 1;
  Address = "192.168.228.2";
  Port = 8080;
}

[Client]
{
  ID = 0;
  DestAddress = { "192.168.205.146"; 8080 };
  Persistant = 1;
  ActivityTimer = 120;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
  HTTP_Version = 1;
  AdditionalDir = {
    ID = { 1; 2 };
    MaxErrorCount = 3;
    BreakDownTimeout = 50000;
    Necromancy = 1;
    UseLoadSharing = 1;
  }
}
{
  ID = 1;
  DestAddress = { "192.168.1.118"; 8881 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
  SSL = {
    Version = 4;
  }
  ActivityTimer = 90;
  ResponseTimer = 60;
  MaxQueue = 5;
  MaxConnection = 50;
}
{
  ID = 2;
  DestAddress = { "192.168.1.118"; 8882 };
  SrcAddress = "192.168.1.119";
  Persistant = 1;
}
```
