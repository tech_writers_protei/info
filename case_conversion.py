from enum import Enum
from typing import NamedTuple, Callable

from loguru import logger


class NameCase(Enum):
    SNAKE_CASE = "snake_case"
    CAMEL_CASE = "camelCase"
    PASCAL_CASE = "PascalCase"
    COBRA_CASE = "Cobra_case"
    LOWER_CASE = "lowercase"
    UPPER_CASE = "UPPERCASE"
    SHAWERMA_CASE = "SHAWERMA-CASE"
    KEBAB_CASE = "kebab-case"
    SCREAM_CASE = "SCREAM_CASE"
    NONE_CASE = "No-Case"

    def __str__(self):
        return f"{self._name_}"

    def __repr__(self):
        return f"{self.__class__.__name__}: {self._value_}"


def is_any_in_functions(*funcs: Callable, **kwargs):
    return any(not func(**kwargs) for func in iter(funcs))


def funcs(line: str):
    def is_all_lowercase():
        return line.islower()

    def is_all_uppercase():
        return line.isupper()

    def is_any_dash():
        return "-" in line and "_" not in line

    def is_any_underline():
        return "_" in line and "-" not in line

    def is_first_upper():
        return line[0].isupper()

    return is_all_lowercase(), is_all_uppercase(), is_any_dash(), is_any_underline(), is_first_upper()


class NameCaseItem(NamedTuple):
    name_case: NameCase
    is_all_lowercase: bool
    is_all_uppercase: bool
    is_any_dash: bool
    is_any_underline: bool
    is_first_uppercase: bool
    is_any_uppercase: bool

    def _bool_attr_items(self):
        return {
            k: v for k, v in self._asdict()
            if isinstance(v, bool)
        }

    def validate_name_case(self, name: str):
        __flag: bool = True
        if self.is_all_lowercase and not name.islower():
            __flag = False


class Name:
    def __init__(self, name: str):
        self._name: str = name
        self._case: NameCase | None = None

    @property
    def case(self):
        return self._case

    @case.setter
    def case(self, value):
        if isinstance(value, NameCase):
            self._case = value
        elif isinstance(value, str):
            self._case = NameCase[value]
        else:
            logger.error(f"Value {value} must be str or NameCase but {type(value)} received")


if __name__ == '__main__':
    _binaries = [
        "0b100100", "0b000001", "0b000011", "0b000111", "0b100000", "0b010011", "0b011011", "0b101000", "0b010111"
    ]
    for _ in iter(_binaries):
        print(int(str(_)[:-1], 2))
