#!/usr/bin/env bash

if [ -z $"echo $SHELL" ]
then
	bash
fi

git submodule add --depth=1 https://git.protei.ru/MobileDevelop/Protei_MME.git content/common/components/mme/
git submodule absorbgitdirs
git -C sub config core.sparseCheckout true
touch .git/info/sparse-checkout

{
	echo "content/common/basics/*"
	echo "content/common/config/*"
	echo "content/common/logging/*"
	echo "content/common/oam/*"
} >> .git/info/sparse-checkout

git submodule set-branch pages
git submodule update --single-branch --recursive

cd ../.git/hooks/
