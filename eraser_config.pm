package Config::Setup;

use strict;

our $config =
{
	cron_start     => 1,
	sleep_delay    => 3600,

	logging =>
	{
		file_name       => 'eraser-%{f_y}%{f_m}%{f_d}.log',
		file_path       => './logs',
		level           => 'ALL',
		max_files       => 10
	},

	ext_path       => '/bin/utils',

	plugins =>
	[
                {
                        enabled    => 1,
                        name       => 'Plugins::SafeDelete',
                        id         => 'logs_gzip',
                        settings   =>
                        {
                                work_dir   => '/usr/protei/eraser',
                                path       => '/usr/protei/log',

                                time_delay => 1*60*60,
                                #max_files  => 1,
                                recursion  => -1,
                                base_remove => 0,
				remove_directories => 0,
                                exclude_dirs =>
                                [
                                   qr(arch$),
                                   qr(reloader$),
                                   qr(reload$),
                                   qr(routing$)
                                ],


                                include_files =>
                                [
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.log$),
                                        qr(sip-(\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.log$),
                                        qr([\w\-]+[\-_](\d{4})_(\d\d)_(\d\d)[\-_](\d{2})_(\d{2})_(\d{2})\.log$),
                                        qr([\d{4}[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_]([\w\-]+)\.log$),
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.pcap$)
#                                        ([\d{4}[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_])([\w\-])\.log$)
                                ],

                                commands   =>
                                [
                                   'zstd -5 -T0 -f --rm -q --no-progress  %{name}',
                                   '# gzipped file %{name}',
                                   'if [ -d %{f_path}/arch ] ; then mv %{name}.zst %{f_path}/arch/
                                    else
                                     mkdir %{f_path}/arch; mv %{name}.zst %{f_path}/arch
                                    fi',
                                ]
                        }
                },

                {
                        enabled    => 1,
                        name       => 'Plugins::SafeDelete',
                        id         => 'logs_remove',
                        settings   =>
                        {
                                work_dir   => '/usr/protei/eraser',
                                path       => '/usr/protei/log',

                                time_delay => 20*24*60*60,
                               # max_files  => 1,
                                recursion  => -1,
                                base_remove => 0,
				remove_directories => 0,
                                exclude_dirs =>
                                [
                                   qr(arch$),
                                   qr(reloader$),
                                   qr(reload$),
                                   qr(routing$)
                                ],


                                include_files =>
                                [
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.log\.zst$),
                                        qr([\w\-]+[\-_](\d{4})_(\d\d)_(\d\d)[\-_](\d{2})_(\d{2})_(\d{2})\.log\.zst$),
                                        qr([\d{4}[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_](\d\d)[\-_]([\w\-]+)\.log\.zst$)
                                ],

                                commands   =>
                                [
                                   'rm -f %{name}',
                                   '# removed file %{name}'
                                ]
                        }
                },
                {
                        enabled    => 1,
                        name       => 'Plugins::SafeDelete',
                        id         => 'cdr_gzip',
                        settings   =>
                        {
                                work_dir   => '/usr/protei/eraser',
                                path       => '/usr/protei/cdr/',

                                time_delay => 1*60*60,
                                #max_files  => 1,
                                recursion  => -1,
                                base_remove => 0,
				remove_directories => 0,
                                exclude_dirs =>
                                [
                                   qr(arch$),
                                   qr(reloader$)
                                ],


                                include_files =>
                                [
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.log$),
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.cdr$)
                                ],

                                commands   =>
                                [
                                   'zstd -5 -T0 -f --rm -q --no-progress  %{name}',
                                   '# gzipped file %{name}',
                                   'if [ -d %{f_path}/arch ] ; then mv %{name}.zst %{f_path}/arch/
                                    else
                                     mkdir %{f_path}/arch; mv %{name}.zst %{f_path}/arch
                                    fi'
                                ]
                        }
                },

                {
                        enabled    => 1,
                        name       => 'Plugins::SafeDelete',
                        id         => 'cdr_remove',
                        settings   =>
                        {
                                work_dir   => '/usr/protei/eraser',
                                path       => '/usr/protei/cdr',

                                time_delay => 90*24*60*60,
                               # max_files  => 1,
                                recursion  => -1,
                                base_remove => 0,
				remove_directories => 0,
                                exclude_dirs =>
                                [
                                   qr(reloader$)
                                ],


                                include_files =>
                                [
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.log\.zst$),
                                        qr([\w\-]+[\-_](\d{4})(\d\d)(\d\d)[\-_](\d{2})(\d{2})\.cdr\.zst$)
                                ],

                                commands   =>
                                [
                                   'rm -f %{name}',
                                   '# removed file %{name}'
                                ]
                        }
                },

                {
                        enabled    => 1,
                        name       => 'Plugins::SafeDelete',
                        id         => 'eraser_logs',
                        settings   =>
                        {
                                work_dir   => '/usr/protei/eraser',
                                path       => '/usr/protei/eraser/logs',
                                time_delay => 5*24*60*60,
                               # max_files  => 1,
                                recursion  => 0,
                                base_remove => 0,
				remove_directories => 0,
                                exclude_dirs =>
                                [
#                                   qr(^trans_stat$),
#                                   qr(^cdr_stat$),
                                   qr(arch$),
                                ],


                                include_files =>
                                [
                                        qr(\.log$),
                                ],

                                commands   =>
                                [
                                   'rm -f %{name}',
                                ]
                        }
                },


	]
};

1;

__END__