#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from argparse import ArgumentParser, Namespace
from os import environ
from shutil import copyfile
import sys
from pathlib import Path
from re import Match, finditer, match, sub
from subprocess import CalledProcessError, PIPE, STDOUT, run
from typing import Any, Mapping, NamedTuple

# import pyvips
import yaml

try:
    from frontmatter import load, loads
except ModuleNotFoundError | ImportError:
    run(["python3", "-m", "pip", "install", "python-frontmatter"])
finally:
    from frontmatter import load, loads

#Autoinstall multiprocessing
try:
    from multiprocessing import Pool
except ModuleNotFoundError | ImportError:
    run(["python3", "-m", "pip", "install", "multiprocessing"])
finally:
    from multiprocessing import Pool

_TEMP_DIR: str = "PDF_Build/"
_IMAGES_DIR: str = "PDF_Build/images"

_PATTERN_SHARED: str = r"(?:{{|\{\{)<\s?getcontent\spath=[\"\']([^\"\']+)[\"\']\s?>}}"

_ABBR_FIX: dict[str, str] = {
    "<abbr (?:title|id)=\"[^\"]+\">": "",
    "</abbr>": ""
}
_BR_FIX: dict[str, str] = {
    "pass:q[<br>]": " +\n",
    "pass:q[&lt;br&gt;]": " +\n"
}
_BREAK_FIX: dict[str, str] = {
    "<div style=\"page-break-after: always;\"></div>": "\n<<<\n"
}
_INTERNAL_USE_FIX: dict[str, str] = {
    r"\{\{\s?<\s?internal_use\s?>\s?\}\}[\s\S]*?\{\{\s?<\s?\/\s?internal_use\s?>\s?\}\}": ""
}
_ANCHORS_FIX: dict[str, str] = {
    r"<a name=\"([^\"]+)\">([^<]+)</a>": r"[[\1]]\2",
    r"^(\s*[=#]+)\s+([^\{]+)\s*\{#(.*)}.*$": r"[[\3]]\n\1 \2"
}
_LISTS_FIX: dict[str, str] = {
    r"^\*\s(.*)$": r"\n\*\s\1$"
}
_EMPTY_FIX: dict[str, str] = {
    r"\n{2,}": r"\n"
}
_LINKS_FIX: dict[str, str] = {
    r"<<(?:\.\./)([^#]*)/?(#[^,]*),([^>]*)>>": r"<<\1/\2,\3>>",
    r"<<(?:\.\./)([^#,]*),([^>]*)>>": r"<<\1,\2>>",
    r"../([^\[]*)": r"\1"
}
_MERMAID_FIX: dict[str, str] = {
    r"\{\{\s?<\s?mermaid\s?>\s?\}\}": r"\[source,mermaid\]\n----",
    r"\{\{\s?<\s?/mermaid\s?>\s?\}\}": r"----"
}


def convert_path(path: str):
    return str(path).replace("/", "_")


def run_command(command: str):
    env = environ.copy()

    try:
        run(
            command,
            env=env,
            shell=True,
            stdout=PIPE,
            stderr=STDOUT,
            universal_newlines=True,
            check=True)

    except CalledProcessError as e:
        print(f"ERROR: {e.stdout}")
        sys.exit(1)


def multiple_sub(line: str, changes: Mapping[str, str] = None) -> str:
    for k, v in changes.items():
        line = sub(k, v, line)
    return line


def fix_images(line: str):
    def repl(m: Match):
        return f"{m.group(1)}{convert_path(m.group(2))}[{m.group(3)}]"

    line: str = sub(r"(image:+)([^\[]*)\[([^]]*)]", repl, line)
    return line


def get_shared_data(line: str):
    _m: Match = match(_PATTERN_SHARED, line)
    if _m:
        _path: str = _m.group(1)
        with open(_path, "r") as f:
            _lines: str = f.read()
        return load(_lines).content


class TextFile:
    def __init__(self, project_dir: str | Path, path: str | Path):
        self._project_dir: Path = Path(project_dir).resolve()
        self._path: Path = Path(path)
        self._content: list[str] = []
        self._title: str | None = None

    @property
    def full_path(self):
        return self._project_dir.joinpath(self._path)

    def __str__(self):
        return "".join(self._content)

    def __iter__(self):
        return iter(self._content)

    def __len__(self):
        return len(self._content)

    def __getitem__(self, item):
        if isinstance(item, int):
            try:
                _: str = self._content[item]
            except IndexError:
                print(f"Index {item} exceeds the length of file {self._path}")
                raise
            else:
                return _

        elif isinstance(item, slice):
            return self._content[item]

        else:
            print(f"Key {item} must be int or slice, but {type(item)} received")
            raise KeyError

    def read(self):
        try:
            with open(self._path, "r") as f:
                self._content = f.readlines()

        except RuntimeError as e:
            print(f"{e.__class__.__name__}: {e.args}")

        except OSError as e:
            print(f"{e.__class__.__name__}: {e.strerror}")

    def write(self):
        try:
            with open(self._path, "w") as f:
                f.write(str(self))

        except RuntimeError as e:
            print(f"{e.__class__.__name__}: {e.args}")

        except OSError as e:
            print(f"{e.__class__.__name__}: {e.strerror}")

    @property
    def content_index(self) -> int:
        indexes: list[int] = [
            index for index, line in enumerate(iter(self))
            if line.removeprefix("\n") == "---"]
        return max(indexes) + 1

    def set_title(self):
        front_matter: dict[str, object] = load(f"{self._path}").to_dict()
        self._title = front_matter.get("title")

    def duplicate(self, dest_dir: str | Path, file_name: str = str()):
        if file_name is None:
            file_name: str = ""

        Path(dest_dir).mkdir(parents=True, exist_ok=True)
        copyfile(self._path, Path(dest_dir).joinpath(file_name))

    @property
    def dir_name(self) -> Path:
        return self._path.parent

    def names(self):
        src_dir: Path = Path(self._project_dir).joinpath(self.dir_name)
        dst_dir: Path = Path(_TEMP_DIR).joinpath(self.dir_name)
        filename: str = self._path.stem
        md_filename: Path = self._path.with_suffix(".md")
        adoc_filename: Path = self._path.with_suffix(".adoc")
        full_src_path: Path = self._project_dir.joinpath(self._path)
        full_dst_path: Path = Path(_TEMP_DIR).joinpath(self._path)

        return {
            'src_dir': src_dir,
            'dst_dir': dst_dir,
            'filename': filename,
            'adoc_filename': adoc_filename,
            'md_filename': md_filename,
            'full_src_path': full_src_path,
            'full_dst_path': full_dst_path
        }

    def fix_links(self, line: str):
        if "index" in self._path.stem:
            return line
        else:
            return multiple_sub(line, _LINKS_FIX)

    def fix(self, **kwargs):
        for index, line in enumerate(iter(self)):
            line: str = multiple_sub(line, **kwargs)
            line: str = self.fix_links(line)
            self._content[index] = line
        self.write()

    def empty(self):
        self + "\n"

    def __add__(self, other):
        if isinstance(other, str):
            self._content.append(other)
        elif hasattr(other, "__str__"):
            self._content.append(f"{other}")
        else:
            return

    def remove_front_matter(self):
        _content: str = "".join(self._content)
        self._content: list[str] = loads(_content).content.splitlines(keepends=True)


class Settings(NamedTuple):
    title_page: str
    figure_caption: str
    toc_title: str
    title_logo_image: str
    version: str
    outlinelevels: int

    @classmethod
    def from_file(cls, path: str | Path):
        with open(path, "r") as f:
            content: dict[str, Any] = yaml.full_load(f)

        settings: dict[str, Any] = content.get("settings")

        title_page: str = settings.get("title-page", "PROTEI")
        figure_caption: str = settings.get("figure-caption", "Рисунок")
        toc_title: str = settings.get("toc-title", "Содержание")
        title_logo_image: str = settings.get("title-logo-image", "images/logo.svg[top=4%,align=right,pdfwidth=3cm]")
        version: str = settings.get("version", "1.0.0")
        outlinelevels: int = settings.get("outlinelevels", 5)

        return cls(title_page, figure_caption, toc_title, title_logo_image, version, outlinelevels)


class Section(NamedTuple):
    name: str
    index: str | None = None
    title_value: str | None = None
    title_level: int | None = None
    files: list[str] = []

    @classmethod
    def from_file(cls, path: str | Path, name: str):
        with open(path, "r") as f:
            content: dict[str, dict[str, str | dict[str, str] | list[str]]] = yaml.full_load(f)

        if name not in content:
            print(f"В файле конфигурации не найдена секция {name}")

        section: dict[str, str | int | list[str] | dict[str, str]] = content.get(name)

        index: str | None = section.get("index", None)[0]
        files: list[str] | None = section.get("files", None)
        title: dict[str, str | int] | None = section.get("title", None)
        if title is None:
            title_value = title_level = None
        else:
            title_value: str | None = title.get("value")
            title_level: str | None = title.get("value")

        return cls(name, index, title_value, title_level, files)

    def __iter__(self):
        return iter(self.files)


class AsciiDocFile(TextFile):
    @property
    def attributes(self):
        return [
            index for index, line in enumerate(iter(self))
            if line.startswith((":", "ifdef::", "ifndef::", "ifeval::"))]

    @property
    def imagesdir(self):
        for index in iter(self.attributes):
            line: str = self[index]

            if ":imagesdir:" in line:
                return line.removeprefix("ifndef::imagesdir[").removeprefix(":imagesdir: ").removesuffix("]")

        else:
            return "."

    def convert_to_pdf(self, theme: str | Path, output: str | Path):
        pdf_name: Path = self.full_path.with_suffix(".pdf")
        print(f'Convert to {pdf_name}')

        command: str = (
            f'asciidoctor-pdf -a numbered -r asciidoctor-diagram -a skip-front-matter '
            f'--theme {theme} -o "{pdf_name}" "{output}" && chmod 777 {pdf_name}')

        run_command(command)

    def add_attribute(self, attribute: str, value: Any = None):
        if value is None or value is True:
            _str: str = f":{attribute}:"
        elif value is False:
            _str: str = f":!{attribute}:"
        else:
            _str: str = f":{attribute}: {value}"
        self._content.insert(self.content_index, f"{_str}")

    def copy_images(self):
        # self.add_attribute("imagesdir", "")
        for line in iter(self):
            for match in finditer(r"image:+([^\[]+)", line):
                _path: Path = Path(match.group(1)).resolve()
                _: Path = self._project_dir.relative_to(_path)
                _name: str = f'PDF_Build_{str(_).replace("/", "_")}'


class GeneralAsciiDoc(AsciiDocFile):
    def __init__(self):
        super().__init__("PDF_Build", "output.pdf")

    def add_attributes(self):
        with open("ascii_doc_parameters.adoc", "r") as f:
            _content: str = f.read()
        self._content.insert(0, _content)

    def add_include(self, file: str | Path, leveloffset: int):
        self.empty()
        self.add_attribute("leveloffset", f"+{leveloffset}")
        self.empty()
        self + f"include::{file}[]"
        self.empty()
        self.add_attribute("leveloffset", f"-{leveloffset}")


class MarkdownFile(TextFile):
    def convert_to_asciidoc(self):
        path: Path = self.full_path.with_suffix(".adoc")
        print(f'Convert {self.full_path} to\n\t{path}')
        command: str = f"pandoc -f markdown -t asciidoc --wrap=none -f markdown-smart -i {self.full_path} -o {path}"
        run_command(command)
        return AsciiDocFile(self._project_dir, path)

    def fix(self):



def getSharedData(content, shared_dirs, level):
    all_pattern = r'({{|\\{\\{)< getcontent path=[\"\'](.+?)[\"\']?\s?>}}'
    n = 0
    while n < 10:
        for item in re.finditer(all_pattern, content):
            if item[2]:
                for dir in shared_dirs:
                    source_shared = '/'.join(dir.split('/')[0:-2])+'/'
                    print(f'Getting shared content from {source_shared}/{item[2]}')
                    data = removeFrontMatter(source_shared + item[2])
                    content = content.replace(item[0], data)
        n += 1
    return '\n'+content


# def convertMermaidMarkdown(md_dst_path):
#     mermaid_open_pattern = r'\{\{\s?<\s?mermaid\s?>\s?\}\}'
#     mermaid_close_pattern = r'\{\{\s?<\s?\/\s?mermaid\s?>\s?\}\}\ ?\;?'
#     new_data = ''
#     convert_flag = False
#     with open(md_dst_path, 'r') as md_file:
#         for line in md_file:
#             if re.match(mermaid_open_pattern, line):
#                 convert_flag = True
#                 line = re.sub(mermaid_open_pattern, '\n```mermaid\n', line)
#             if re.match(mermaid_close_pattern, line):
#                 line = re.sub(mermaid_close_pattern, '\n```\n', line)
#             new_data += line
#     if convert_flag:
#         print(f'Converting mermaid diagrams in {md_dst_path}')
#         with open(md_dst_path, 'w') as md_file:
#             md_file.write(new_data)
#         command = f'mmdc -p /opt/puppeteer-config.json -i "/documents/{md_dst_path}" -o "/documents/{md_dst_path}"'
#         callProcess(command)


# def convertSVGtoPNG(filename):
#     print(f'Converting {filename} to png')
#     name, ext = os.path.splitext(filename)
#     png_file = name + '.png'
#
#     with open(filename, "r") as svg_file:
#         data = svg_file.read().replace('Text is not SVG - cannot display', '')
#
#     with open(filename, "w") as svg_file:
#         svg_file.write(data)
#
#     image = pyvips.Image.new_from_file(filename, dpi=144)
#     image.write_to_file(png_file)
#     return png_file

def parse():
    parser: ArgumentParser = ArgumentParser(
        prog='PDF builder')
    parser.add_argument(
        '--project-dir',
        help="Path to project dir")
    parser.add_argument(
        '--temp-dir',
        help="Path to temp dir")
    parser.add_argument(
        '--shared-dirs',
        help="Path to shared docs dir")
    parser.add_argument(
        '--config',
        help="Project config file")
    parser.add_argument(
        '--pdf-config',
        help="PDF config file with projects settings")
    parser.add_argument(
        '--theme',
        help="PDF theme file")
    parser.add_argument(
        '--processes',
        help="Max count of processes")
    args: Namespace = parser.parse_args()


def getImages(content, filenames):
    all_pattern = r'image:+(.+?)\[(.+?)\]'
    images_dir_line = ''
    for line in content.splitlines():

        if 'ifndef::' in line:
            future_del_line = line
            images_dir_line = line
            try:
                images_dir_line = images_dir_line.split('ifndef::imagesdir[:imagesdir: ')[1][0:-1]
            except:
                print(f'Error format: {images_dir_line}')
            images_dir_line = images_dir_line + '/'

        result = re.finditer(all_pattern, line)

        if result:
            for link in result:
                if images_dir_line:
                    full_link_first = link[0]
                    full_link = link[0][0:7] + images_dir_line + link[0][7:]
                    id_link = link[2]
                    image_link = images_dir_line + link[1]
                    image_file = image_link.split('/')[-1]

                else:
                    full_link = link[0]
                    id_link = link[2]
                    image_link = link[1]
                    image_file = image_link.split('/')[-1]
                # Так как для Hugo (html) мы добавляем один уровень
                # то здесь его нужно убрать
                # ../../images/image.jpg -> ../images/image.jpg
                if '../' in image_link:
                    image_link = image_link.replace('../', '', 1)
                # Поиск удаление {{< baseurl >}} в adoc
                # {{< baseurl >}}/Mobile/Protei_PGW/common/images/image.svg ->
                # -> common/images/image.svg

                if '%7B%7B%3C%20baseurl%20%3E%7D%7D' in image_link:
                    image_link = image_link.replace('%7B%7B%3C%20baseurl%20%3E%7D%7D/', '')
                    image_link = '/'.join(image_link.split('/')[2:])
                # Для исключения наложений изображений с одинаковыми именами,
                # добавляем в виде префикса путь src с заменой "/" на "_"
                # PDF_Build_Protei_PGW_content_common_basics_life_cycle_LIFECYCLE.svg
                temp_image_name = filenames["dst_dir"].replace('/', '_') + image_file
                if not copyFile(filenames["src_dir"]+image_link, images_temp_dir + '/', temp_image_name):
                    copyFile(filenames["dst_dir"]+image_link, images_temp_dir + '/', temp_image_name)

                if image_file.endswith('.svg'):
                    temp_image_name = images_temp_dir + '/' + temp_image_name
                    result_link = f'image::{temp_image_name}[{id_link}]'
                else:
                    result_link = f'image::{images_temp_dir}/{temp_image_name}[{id_link}]'

                if '::' in full_link and '::' in result_link:
                    pass
                else:
                    result_link = result_link.replace('::', ':')

                content = content.replace(full_link, result_link)

                if 'ifndef::' in content:
                    result_lunk = result_link.replace('::', ':')
                    content = content.replace(full_link_first, result_link)

    if 'ifndef::' in content:
        content = content.replace(future_del_line, '')

    return content


def pdfBuild(config, temp_dir, project_dir, pdf_name, docs_dir='', shared_dirs='', destination=None):
    break_section = 0
    for chapter in config:
        title = ''
        title_files = True
        files = {}
        outlinelevel = ''
        break_chapters = False
        break_flag = False
        break_flag_section = True
        overwrite_title = False

        if chapter == 'settings':
            title_page = config['settings'].get('title-page', 'Protei')
            doctype = config['settings'].get('doctype', 'book')
            toc = config['settings'].get('toc', True)
            if toc:
                toc = ':toc:'
            else:
                toc = ':!toc:'
            figure_caption = config['settings'].get('figure-caption', 'Рисунок')
            chapter_signifier = config['settings'].get('chapter-signifier', False)
            if chapter_signifier:
                chapter_signifier = ':chapter-signifier:'
            else:
                chapter_signifier = ':!chapter-signifier:'
            try:
                if 'en_' in pdf_name:
                    toc_title = config['settings'].get('toc_title', 'Contents')
                else:
                    toc_title = config['settings'].get('toc_title', 'Содержание')
            except:
                toc_title = config['settings'].get('toc_title', 'Содержание')
            toclevels = config['settings'].get('toclevels', 2)
            sectnumlevels = config['settings'].get('sectnumlevels', 3)
            outlinelevels = config['settings'].get('outlinelevels', 2)
            level = outlinelevels
            version = config['settings'].get('version', '1.0.0')
            if 'en_' in pdf_name:
                title_logo_image = config['settings'].get('title-logo-image-en', 'images/logo_en.svg[top=4%,align=right,pdfwidth=3cm]')
            else:
                title_logo_image = config['settings'].get('title-logo-image', 'images/logo.svg[top=4%,align=right,pdfwidth=3cm]')
            break_section = config['settings'].get('break-section', 0)
            continue
        else:
            level = 2

        if 'title' in config[chapter].keys():
            if 'value' in config[chapter]['title'].keys():
                title = config[chapter]['title']['value']
                overwrite_title = True
            if 'break' in config[chapter]['title'].keys():
                break_flag_section = config[chapter]['title'].get('break', False)
            if 'level' in config[chapter]['title'].keys():
                level = config[chapter]['title']['level']
                if 'outlinelevels' in config[chapter]['title'].keys():
                    if config[chapter]['title']['outlinelevels'] is False:
                        outlinelevel = ''
                    else:
                        outlinelevel = f'[outlinelevels={level}]'
                else:
                    outlinelevel = f'[outlinelevels={level}]'
            if 'title-files' in config[chapter]['title'].keys():
                title_files = config[chapter]['title']['title-files']
            if 'break-chapters' in config[chapter]['title'].keys():
                break_chapters = config[chapter]['title']['break-chapters']
            title = prepareTitle(False, title, level, outlinelevel)

        with open(pdf_name+'.adoc', "a") as out:
            if break_section > 0 and break_flag_section is True:
                out.write('\n<<<\n' + title)
            else:
                out.write(title)

        if 'index' in config[chapter]:
            files['index'] = []
            for file in config[chapter]['index']:
                files['index'].append(file)

        if 'files' in config[chapter]:
            files['files'] = []
            for file in config[chapter]['files']:
                files['files'].append(file)

        for key in files.keys():
            for file in files[key]:
                filenames = splitFilenames(project_dir, temp_dir, file)
                makeDir(filenames['dst_dir'])
                # print(json.dumps(filenames, indent=4))
                # Получение title и description из параметров (FrontMatter)
                meta = getTitle(filenames['full_src_path'])
                if overwrite_title:
                    title = ''
                    overwrite_title = False
                else:
                    title = meta['title']
                file_content = removeFrontMatter(filenames['full_src_path'])
                target_content = ""
                # Вставка общих доков
                # if shared_dirs:
                #     file_content = getSharedData(file_content, shared_dirs, level)
                if filenames['full_src_path'].endswith('.md'):
                    print(f'Change anchors in {file} to adoc format')
                    for line in file_content.splitlines():
                        line = changeAnchors(line)
                        line = changeBreaks(line)
                        # Дополнительные переносы строк у разделительной линии
                        line = changeContent(line, re.compile('^[-]+$'), '\n-------------\n\n')
                        line = changeLists(line)
                        target_content = target_content + '\n' + line
                    target_content = deleteInternalUse(target_content)
                    print(f'Save temp {filenames["full_dst_path"]}')
                    with open(filenames['full_dst_path'], 'wb') as dst:
                        dst.write(target_content.encode("utf-8"))
                    #     FIXME
                    # convertMermaidMarkdown(filenames['full_dst_path'])
                    file_content = convertMarkdownToAsciidoc(filenames['dst_dir'], filenames['filename'])
                elif filenames['full_src_path'].endswith('.adoc'):
                    file_content = convert_mermaid_asciidoc(file_content)

                # Подготовка и конвертация изображений
                print(f'Prepare images for {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                file_content = getImages(file_content, filenames)
                print(f'Change links in {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                file_content = changeLinks(file_content)
                print(f'Change chapters in {filenames["dst_dir"]}{filenames["adoc_filename"]}')
                # Артефакт adoc, преобразовывание экранированной { в обычную
                file_content = changeContent(file_content, re.compile('\\\{'), '{')
                # Установка автоширины для таблиц
                file_content = changeContent(file_content, re.compile('\\[.*(?=,cols)'), '[%autowidth')

                if break_flag is False:
                    break_section_flag = False
                    break_flag = True
                else:
                    if break_section > 1:
                        if level < 3:
                            break_section_flag = True
                        else:
                            break_section_flag = False
                if break_chapters is True:
                    break_chapters_flag = True
                else:
                    break_chapters_flag = False

                # Якоря для _index не нужны
                if '_index' not in str(filenames["adoc_filename"]):
                    # Если /network_arch/index.md , то имя якоря = директории
                    if 'index.' in str(filenames["adoc_filename"]):
                        anchor_file = '/'.join(filenames["full_src_path"].split('/')[-2:-1])
                    else:
                        anchor_file = filenames["filename"]
                    anchor_file = f'\nanchor:{anchor_file}[]\n\n'
                else:
                    anchor_file = ''

                if key == 'index':
                    title = prepareTitle(break_section_flag, title, level, outlinelevel, anchor_file)
                    file_content = changeChapters(break_chapters_flag, file_content, level-1)
                else:
                    title = prepareTitle(break_section_flag, title, level+1, outlinelevel, anchor_file)
                    file_content = changeChapters(break_chapters_flag, file_content, level)

                with open(pdf_name+'.adoc', "a+b") as out:
                    if title_files:
                        out.write(title.encode("utf-8"))
                    else:
                        out.write(b'\n\n')
                    out.write(file_content.encode("utf-8"))
                outlinelevel = ''
    try:
        if 'en_' in pdf_name:
            file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Date: {{docdate}}
Version: {version}
:toclevels: {toclevels}
'''
        else:
            file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Дата: {{docdate}}
Версия: {version}
:toclevels: {toclevels}
'''

    except:
        file_content = f'''= {title_page}
:doctype: {doctype}
{toc}
:figure-caption: {figure_caption}
{chapter_signifier}
:toc-title: {toc_title}
:outlinelevels: {outlinelevels}
:sectnumlevels: {sectnumlevels}
:title-logo-image: image:{title_logo_image}
Дата: {{docdate}}
Версия: {version}
:toclevels: {toclevels}
'''

    # https://docs.asciidoctor.org/asciidoc/latest/attributes/document-attributes-ref/#builtin-attributes-i18n
    with open(pdf_name+'.adoc', 'rb') as original:
        modified_data = original.read()

    with open(pdf_name+'.adoc', 'wb') as modified:
        modified.write(file_content.encode("utf-8")+b"\n" + modified_data)
    convertAsciiDocToPDF(pdf_name+'.adoc', pdf_name)
    # FIXME

    copyFile(pdf_name, destination + '/', pdf_name)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='PDF builder')
    parser.add_argument('--project-dir',
        help="Path to project dir")
    parser.add_argument('--temp-dir',
        help="Path to temp dir")
    parser.add_argument('--shared-dirs',
        help="Path to shared docs dir")
    parser.add_argument('--config',
        help="Project config file")
    parser.add_argument('--pdf-config',
        help="PDF config file with projects settings")
    parser.add_argument('--theme',
        help="PDF theme file")
    parser.add_argument('--processes',
        help="Max count of processes")
    args = parser.parse_args()

    images_temp_dir = 'PDF_Build/images'
    max_processes = int(args.processes)
    complete_result = []

    if args.pdf_config:
        try:
            with open(args.pdf_config, 'r') as pdf_conffile:
                projects_config = yaml.load(pdf_conffile, Loader=yaml.FullLoader)
                if projects_config is None:
                    print('PDF settings file is empty')
                    sys.exit(0)
        except Exception as e:
            print(f'Error when opening PDF settings file {args.pdf_config}: "{e}"')
            sys.exit(1)
    elif args.config:
        try:
            with open(args.config, 'r') as conffile:
                config = yaml.load(args.config, Loader=yaml.FullLoader)
        except Exception as e:
            print(f'Error when opening project settings file {args.config}: "{e}"')
            sys.exit(1)
        try:
            project_dir = args.project_dir
        except Exception:
            print('Project (--project-dir) directory is undefined')
            sys.exit(1)
        try:
            temp_dir = 'PDF_Build/' + args.temp_dir
        except Exception:
            print('Temp (--temp-dir) directory is undefined')
            sys.exit(1)
        shared_dirs = []
        try:
            shared_dirs.append(args.shared_dirs)
        except Exception:
            shared_dirs = []

    def process_pdf_build(project_name, projects_config):
        temp_dir = 'PDF_Build/' + project_name
        config = projects_config[project_name].get('pdf_settings_file')
        project_dir = projects_config[project_name].get('project_dir')
        project = project_name.split('/')[-1]
        docs_dir = projects_config[project_name].get('docs_dir')
        destination = projects_config[project_name].get('destination')
        shared_dirs = projects_config[project_name].get('shared_dirs', '')
        pdf_name = projects_config[project_name].get('pdf_name', project)
        pdf_storage = projects_config[project_name].get('pdf_storage', destination)
        with open(config, 'r') as conffile:
            config = yaml.load(conffile, Loader=yaml.FullLoader)
        os.makedirs(pdf_storage, exist_ok=True)
        pdfBuild(config, temp_dir, project_dir, pdf_name, docs_dir, shared_dirs, pdf_storage)
        os.remove(pdf_name+'.adoc')

    if 'projects_config' in locals():
        with Pool(processes=max_processes) as pool:
            args_list = [(project_name, projects_config) for project_name in projects_config]
            result = pool.starmap_async(process_pdf_build, args_list)
            result.get()
        pool.close()
        pool.join()

    else:
        project = project_dir.rstrip('/').split('/')[-1]
        with open(config, 'r') as conffile:
            config = yaml.load(conffile, Loader=yaml.FullLoader)
        docs_dir = ''
        destination = ''
        pdf_name = project
        pdfBuild(config, temp_dir, project_dir, pdf_name+'.pdf', docs_dir, shared_dirs)

    print(complete_result)
