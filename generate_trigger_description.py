import csv
from enum import IntEnum, Enum
from typing import NamedTuple, Type, Literal, Sequence


PRIORITIES: tuple[int, ...] = (1, 2, 3, 4, 5)


AlarmSeverity: Type[str] = Literal["WARN", "ERROR"]
RateUnits: Type[str] = Literal["", "%"]
TimeUnits: Type[str] = Literal["с", "м", "ч", "д"]
CountNumber: Type[str] = Literal["1 или более", "все"]
HostBase: Type[str] = Literal["MME Host", "Hardware Host", "Switch"]


def validate_int_non_negative(value: int):
    if value < 0:
        raise NegativeIntError
    return value


def validate_int_spec_values(value: int, __values: Sequence[int] = None):
    if __values is not None and value not in __values:
        raise NotSpecifiedIntError
    return value


class BaseError(Exception):
    """Base class for custom errors."""


class InvalidNameStructureError(BaseError):
    """Invalid structure of name."""


class InvalidSeverityError(BaseError):
    """Invalid severity name."""


class NegativeIntError(BaseError):
    """Int value is less than zero."""


class NotSpecifiedIntError(BaseError):
    """Int value is out of available set."""


class Priority(IntEnum):
    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5

    def __str__(self):
        return f"{self._value_}"

    __repr__ = __str__

    def __int__(self):
        return self._value_


class InterfaceState(NamedTuple):
    state: str
    code: int

    def __str__(self):
        return f"{self.state} ({self.code})"

    __repr__ = __str__


class TimePeriod(NamedTuple):
    value: int
    time_units: TimeUnits

    def __str__(self):
        return f"в течение {self.value} {self.time_units}"

    __repr__ = __str__


class ThresholdRateCount(NamedTuple):
    base_value: int
    rate_units: RateUnits

    def __str__(self):
        return f"{self.base_value}{self.rate_units}"

    __repr__ = __str__


class ThresholdInterfaceAvailability(NamedTuple):
    count_number: CountNumber
    interface: str

    def __str__(self):
        return f"недоступность {self.count_number} {self.interface}-интерфейсов"

    __repr__ = __str__


class NOCAction(Enum):
    INFORM_PS_CORE =(
        "Проинформировать дежурного PSCore по телефону и сделать оповещение по почте на адрес PSCore@tele2.ru.")
    NOTIFY_PS_CORE = "Оповестить по почте PSCore@tele2.ru."
    NOTIFY_CLOUD_CP = "Отправить письмо на CloudCP@tele2.ru."
    CALL_CLOUD = "Позвонить дежурному CloudCP по телефону +79527687031."

    def __str__(self):
        return self._value_

    __repr__ = __str__


class TriggerParams(NamedTuple):
    name: str
    trigger_name: str
    trigger_moira: str
    severity: AlarmSeverity
    threshold: ThresholdInterfaceAvailability | ThresholdRateCount
    description: str
    priority: Priority
    noc_action: NOCAction

    def trigger_short(self):
        if "." not in self.trigger_moira:
            raise InvalidNameStructureError
        return self.trigger_moira.rsplit(".", 1)[-1]
