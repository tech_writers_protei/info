# -*- coding: utf-8 -*-
from collections import deque
from pathlib import Path
from re import sub
from typing import Iterable


_REPLACEMENTS: dict[str, str] = {
    " 3GPP TS 35.205":
        " [3GPP TS 35.205]"
        "(https://www.etsi.org/deliver/etsi_ts/135200_135299/135205/18.00.00_60/ts_135205v180000p.pdf)",
    " 3GPP TS 29.002":
        " [3GPP TS 29.002]"
        "(https://www.etsi.org/deliver/etsi_ts/129000_129099/129002/17.03.00_60/ts_129002v170300p.pdf)",
    " 3GPP TS 23.011":
        " [3GPP TS 23.011]"
        "(https://www.etsi.org/deliver/etsi_ts/123000_123099/123011/18.00.00_60/ts_123011v180000p.pdf)",
    " 3GPP TS 23.032":
        " [3GPP TS 23.032]"
        "(https://www.etsi.org/deliver/etsi_ts/123000_123099/123032/18.01.00_60/ts_123032v180100p.pdf)",
    " 3GPP TS 23.003":
        " [3GPP TS 23.003]"
        "(https://www.etsi.org/deliver/etsi_ts/123000_123099/123003/16.05.00_60/ts_123003v160500p.pdf)",
    " 3GPP TS 29.303":
        " [3GPP TS 29.303]"
        "(https://www.etsi.org/deliver/etsi_ts/129300_129399/129303/18.00.00_60/ts_129303v180000p.pdf)",
    " 3GPP TS 29.212":
        " [3GPP TS 29.212]"
        "(https://www.etsi.org/deliver/etsi_ts/129200_129299/129212/17.02.00_60/ts_129212v170200p.pdf)",
    " 3GPP TS 23.203":
        " [3GPP TS 23.203]"
        "(https://www.etsi.org/deliver/etsi_ts/123200_123299/123203/16.02.00_60/ts_123203v160200p.pdf)",
    " 3GPP TS 23.060":
        " [3GPP TS 23.060]"
        "(https://www.etsi.org/deliver/etsi_ts/123000_123099/123060/17.00.00_60/ts_123060v170000p.pdf)",
    " 3GPP TS 29.272":
        " [3GPP TS 29.272]"
        "(https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf)",
    " 3GPP TS 22.004":
        " [3GPP TS 22.004]"
        "(https://www.etsi.org/deliver/etsi_ts/122000_122099/122004/18.00.01_60/ts_122004v180001p.pdf)",
    " 3GPP TS 32.215":
        " [3GPP TS 32.215]"
        "(https://www.etsi.org/deliver/etsi_ts/132200_132299/132215/05.09.00_60/ts_132215v050900p.pdf)",
    " 3GPP TS 29.229":
        " [3GPP TS 29.229]"
        "(https://www.etsi.org/deliver/etsi_ts/129200_129299/129229/17.02.00_60/ts_129229v170200p.pdf)",
    " 3GPP TS 23.682":
        " [3GPP TS 23.682]"
        "(https://www.etsi.org/deliver/etsi_ts/123600_123699/123682/17.03.00_60/ts_123682v170300p.pdf)",
    " 3GPP TS 29.228":
        " [3GPP TS 29.228]"
        "(https://www.etsi.org/deliver/etsi_ts/129200_129299/129228/18.00.00_60/ts_129228v180000p.pdf)",
    " 3GPP TS 32.299":
        " [3GPP TS 32.299]"
        "(https://www.etsi.org/deliver/etsi_ts/132200_132299/132299/17.00.00_60/ts_132299v170000p.pdf)",
    " ITU-T E.164":
        "[Recommendation ITU-T E.164]"
        "(https://www.itu.int/rec/T-REC-E.164-201011-I/en)",
    " ITU-T E.212":
        "[Recommendation ITU-T E.212]"
        "(https://www.itu.int/rec/T-REC-E.212-201609-I/en)",
    "Recommendation ITU-T Q.763":
        "[ITU-T Recommendation Q.763]"
        "(https://www.itu.int/rec/T-REC-Q.763-199912-I/en)",
    "Recommendation ITU-T Q.767":
        "[ITU-T Recommendation Q.767]"
        "(https://www.itu.int/rec/T-REC-Q.767-199102-I/en)",
    ". **Примечание.**": ".<br>**Примечание.**",
    "<br/>": "<br>",
    ". <br>": ".<br>",
    "тип - ": "",
    r"list, элементы — (\w+)": r"[\1]",
    "Список": "Перечень",
    "список": "перечень",
    "\\<": "",
    "\\>": "",
    "{object}": "object",
    "(2(32)-1)": "2<sup>32</sup> - 1",
    "IMSI-номер": "Номер IMSI",
    "MSISDN-номер": "Номер MSISDN",
    # "Список": "Перечень",
    # "список": "перечень",
    "Возможные значения: ": "",
    "Возможные значения см. ": "См. ",
    "См. описание параметров в разделе ": "См. ",
    "См. описание параметров в ": "См. ",
    "См. в разделе ": "См. ",
    "См.  ": "См. ",
    "Описание параметров см. ": "См. ",
    "Описание параметров см. в разделе ": "См. ",
    "0 — create (создание) / modify (изменение); 1 — delete (удаление)":
        "`0` -- create, создать или modify, изменить;<br>`1` -- delete, удалить",
    " ```": "```",
    ". create (создание) / modify (изменение) / delete (удаление).":
        ".<br>`create` -- создать; `modify` -- изменить; `delete` -- удалить.",
    "[[": "[",
    ")]": ")",
    "##### Пример #####": "#### Пример ####",
    "Пример запроса": "Пример тела запроса",
    # "### Пример ### ": "#### Пример ####",
    # "### Пример ###": "#### Пример ####",
    "0 — ": "`0` -- ",
    "1 — ": "`1` -- ",
    "2 — ": "`2` -- ",
    "3 — ": "`3` -- ",
    "4 — ": "`4` -- ",
    "5 — ": "`5` -- ",
    " — ": " -- ",
    ". `": ".<br>`",
    "Формат:": "Формат:<br>"
}


def multi_replace(line: str):
    for k, v in _REPLACEMENTS.items():
        line: str = line.replace(k, v)
    return line


class MarkdownFile:
    def __init__(self, path: str | Path, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._path: Path = Path(path)
        self._lines: list[str] = [*lines]
        self._header_lines: list[int] = []
        self._markdown_tables: dict[int, MarkdownTable] = dict()
        self._dict_md_lines: dict[int, list[int]] = dict()

    def read(self):
        with open(self._path, "r", encoding="utf-8", errors="ignore") as f:
            content: list[str] = f.readlines()
        self._lines = content

    def write(self):
        with open(self._path, "w", encoding="utf-8", errors="ignore") as f:
            f.write(str(self))

    def __iter__(self):
        return iter(self._lines)

    def __str__(self):
        return "\n".join(iter(self))

    def __getitem__(self, item):
        if isinstance(item, (int, slice)):
            return self._lines[item]
        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._lines[key] = value
        else:
            raise TypeError

    def __len__(self):
        return len(self._lines)

    def get_markdown_table(self, index: int):
        return self._markdown_tables.get(index)

    def find_links(self):
        __flag: bool = False
        _indexes: list[int] = []

        for index, line in enumerate(iter(self)):
            if "appendices" in line or "add_info" in line:
                _indexes.append(index)

                if __flag is False:
                    __flag = True

        if __flag is True:
            _str_indexes: str = ", ".join(map(str, _indexes))
            print(f"Найдены ссылки на appendices.\nФайл: {self._path.name} Строки: {_str_indexes}")

    def set_header_lines(self):
        _: deque = deque(index - 1 for index, line in enumerate(iter(self)) if line.startswith("|--"))
        _.appendleft(0)
        _.append(len(self))
        self._header_lines = [*_]

    def set_dict_md_lines(self):
        _len: int = len(self._header_lines) - 1

        for idx in range(_len):
            self._dict_md_lines[idx] = []
            _from: int = self._header_lines[idx]
            _to: int = self._header_lines[idx + 1]

            self._dict_md_lines[idx].extend(_ for _ in range(_from, _to))

    def set_tables(self):
        for index, v in self._dict_md_lines.items():
            lines: list[str] = [self[value] for value in v]
            markdown_table: MarkdownTable = MarkdownTable(index, lines)
            self._markdown_tables[index] = markdown_table

    def update_tables(self):
        for markdown_table in self._markdown_tables.values():
            markdown_table.update()
            indexes: list[int] = self._dict_md_lines[markdown_table.index]

            for _ in range(len(indexes)):
                self[indexes[_]] = markdown_table[_]


class MarkdownTable:
    def __init__(self, index: int, lines: Iterable[str] = None):
        if lines is None:
            lines: list[str] = []
        self._index: int = index
        self._lines: list[str] = [line.removesuffix("\n") for line in lines]

    def __iter__(self):
        return iter(self._lines)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._lines[item]
        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._lines[key] = value
        else:
            raise TypeError

    @property
    def line_indexes(self) -> list[int]:
        return [index for index, line in enumerate(iter(self)) if
                line.startswith("|") and line.endswith("|") and not line.startswith("|--")]

    def split(self, index: int) -> list[str]:
        return [item.strip() for item in self[index][1:-1].split("|")]

    def add_dot(self, index: int, part: int):
        parts: list[str] = self.split(index)
        item: str = parts[part]

        if not item.endswith("."):
            parts[part] = f"{item}."

        _: str = " | ".join(parts)

        self[index] = f"| {_} |"

    def remove_phrases(self):
        for index, line in enumerate(iter(self)):
            line = multi_replace(line)
            line = sub(r"list, элементы -- (\w+)", r"[\1]", line)
            line = sub(r"list, элемент.? - (\w+)", r"[\1]", line)
            line = sub(r"Команда \*\*(\w+)\*\*", r"Команда `\1`", line)
            line = sub("См. описание .*в разделе", "См. ", line)
            line = line.replace("Описание.", "Описание")
            self[index] = line

    def update(self):
        for index in self.line_indexes:
            self.add_dot(index, 1)
        self.remove_phrases()

    @property
    def index(self):
        return self._index

    @index.setter
    def index(self, value):
        raise AttributeError


def main():
    path: str = r"C:\Users\tarasov-a\PycharmProjects\hlr_hss\content\common\components\Core\config"
    for file in Path(path).iterdir():
        if file.is_file():
            markdown_file: MarkdownFile = MarkdownFile(file)
            markdown_file.read()
            markdown_file.set_header_lines()
            markdown_file.set_dict_md_lines()
            markdown_file.set_tables()
            markdown_file.find_links()
            markdown_file.update_tables()
            markdown_file.write()


if __name__ == '__main__':
    main()
