= Основы работы с draw.io
:chapter-signifier:
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:revdate: Jun 1, 2023
:revnumber: 1.0.0
:experimental:
:icons: font
:imagesdir: ./images

== Краткое знакомство

*_draw.io_*, а точнее https://app.diagrams.net/[app.diagrams.net] --- графический редактор для создания схем, диаграм, чертежей и т.п.
Распространяется свободно. Имеет значительный набор иконок, базовых элементов схем и нотаций из коробки, что упрощает и ускоряет работу с графикой.
Имеет встроенный конвертер для преобразования в форматы PNG, JPEG, VSDX, SVG, XML. Позволяет хранить файлы как в облачном хранилище, так и локально.

== Создание нового файла

. Перейти на https://app.diagrams.net/.
. Выбрать место для хранения файла:
* https://drive.google.com/[Google Drive];
* https://onedrive.live.com/[OneDrive];
* https://www.dropbox.com/ru/[Dropbox];
* https://github.com/[GitHub];
* https://gitlab.com/[GitLab];
* локально;

.Выбор места для сохранения файла
[caption="Рисунок 1. "]
image::draw_io_select_storage.png[pdfwidth=80%,scaledwidth=80%]

NOTE: Можно пропустить этот пункт и сделать выбор позднее.

[start=2]
. В углу задать название файла, изменив имя по умолчанию _'Untitled Diagram'_.
. На нижней панели задать название странице, изменив имя по умолчанию _'Page-1'_.
. На боковой панели загрузить иконки в раздел _'Scratchpad'_.

.Главная страница файла
[caption="Рисунок 2. "]
image::draw_io_main_page.png[width=640,height=309]

* Вариант 1 --- перетащить xml-файл в область раздела.
* Вариант 2:
** Нажать кнопку btn:[Edit].
** Вариант 2.1 --- перетащить xml-файл в указанную область.
** Вариант 2.2 --- нажать кнопку btn:[Import] и указать xml-файл.

.Загрузка собственной библиотеки
[caption="Рисунок 3. "]
image::draw_io_upload.png[width=351,height=251]
