---
title: "HTTP"
description: "Параметры компонента HTTP"
weight: 40
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/http.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этой секции задаются настройки компонента HTTP.

## Используемые подсекции ##

* **[server](#server)** - параметры сервера;
    * **[address](#serverAddress)** - параметры прослушиваемого IP/порта;
    * **[ssl](#serverSSL)** - параметры SSL;
    * **[authorization](#serverAuthorization)** - параметры Authorization;
* **[client](#client)** - параметры клиента;
    * **[destAddress](#clientDestAddress)** - IP/порт сервера;
    * **[ssl](#clientSSL)** - параметры SSL;
    * **[authorization](#clientAuthorization)** - параметры Authorization;
    * **[additionalDir](#clientAdditionalDir)** - параметры менеджера соединений;

### Описание подсекций и их параметров ###

Параметр | Описание | Тип | По умолчанию | O/M | Версия
---------|----------|-----|--------------|-----|-------
**<a name="server">server</a>** ||
id | Идентификатор направления | число | - | M
maxQueue | Максимальная длина очереди запросов в одном соединении (для persistant режима)  | число | 10 | O | -
maxBufferSize | Максимальный размер буфера для приема сообщений.<br>Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295 | число<br>Б | -1 | O | -
recvBufferSize | Минимальный размер буфера для приема сообщений.<br>Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до maxBufferSize.<br>В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS | число<br>Б | 65&nbsp;536  | O | -
activityTimer | Время ожидания активности соединения.<br>По истечении при отсутствии новых запросов соединение разрывается, объект HTTP_ServerCL уничтожается | число<br>с | 60 | O | -
largeBodyStoragePath | Путь к временному файлу для получения больших объемов данных, длина которых превышет maxBufferSize | строка | - | O | -
**<a name="serverAddress">address</a>** ||
ip | Прослушиваемый IP-адрес | строка | 0.0.0.0 | O | -
port | Прослушиваемый порт | число | 80 | O | -
**<a name="serverSSL">ssl</a>** | Секция для конфигурирования защищенного соединения | объект | - | O | -
version | Версия SSL. <br>1 - TLSv1, <br>2 - SSLv2, <br>3 - SSLv3, <br>4 - SSLv23, <br>5 - TLSv1.1, <br>6 - TLSv1.2  | число | 0 | O | -
certificatePath | Путь к файлу сертификата (*.pem) | строка | - | O | -
certificateChainPath | Путь к цепочке подписанных сертификатов | строка | - | O | -
privateKeyPath | Путь к файлу, содержащему секретный ключ | строка | - | O | -
ciphers | Cписок поддерживаемых шифров | строка<br>(c1:c2:…:cN) | - | O | -
preferServerCiphers | Флаг приоритизации шифров на стороне сервера вместо списка клиента | логический | false | O | -
**<a name="serverAuthorization">authorization</a>** | Параметры авторизации | объект | - | O | -
type | Тип авторизации<br>Basic;<br>Digest (пока не поддерживается). | строка | Basic | O | -
user | Логин для авторизации | строка | - | M | -
password | Пароль для авторизации | строка | - | O | -

### **Клиентское направление**

Параметр | Описание | Тип | По умолчанию | O/M | Версия
---------|----------|-----|--------------|-----|-------
**<a name="client">client</a>** ||
id | Идентификатор направления.<br>Контроль за уникальностью идентификаторов возлагается на администратора системы | число | - | M | -
srcAddress | IP-адрес для отправки запроса, если на машине сконфигурировано несколько сетевых интерфейсов (несколько ip-адресов) | строка | - | O | -
persistant | Флаг активации режима использования persistent-соединений.<br>Если значение `true`, то после получения ответа соединение остается активным в течение **activityTimer**.<br>Разрешается отправка запросов конвейером: клиент отправляет запросы, не дожидаясь ответа на предыдущие | логический | false | O | -
activityTimer | Время активности соединения при активации **Persistant**-режима.<br>По истечении, если от логики не поступило новых запросов, соединение разрывается, по всем запросам в очереди формируется оповещение логики об ошибке, затем объект HTTP_ClientCL уничтожается | число<br>сек | 60 | O | -
responseTimer | Время ожидания ответа от сервера.<br>По истечении, если от сервера не поступил ответ, соединение разрывается, по всем запросам в очереди формируется оповещение логики об ошибке, затем объект HTTP_ClientCL уничтожается | число<br>сек | 60 | O | -
maxQueue | Максимальное количество запросов в очереди в одном TCP-соединении.<br>По достижении этого предела HTTP_Interface создает новый объект HTTP_ClientCL (новое TCP-соединение) | число | 10 | O | -
maxConnection | Максимальное количество одновременных соединений к серверу.<br>По достижении этого предела HTTP_Interface будет отвечать HTTP_ERROR_IND с кодом ошибки OVERLOADED | число | 500 | O | -
maxBufferSize  | Максимальный размер буфера для приема сообщений.<br>Если "-1", то не ограничен: 4&nbsp;294&nbsp;967&nbsp;295 | число<br>Б | -1 | O | -
recvBufferSize | Минимальный размер буфера для приема сообщений.<br>Если сообщение превышает размер буфера, то буфер будет увеличиваться вплоть до maxBufferSize.<br>В зависимости от специфики приложений позволяет экономить ресурсы. Например, для работы с XML-приложениями в среднем требуется меньший размер буфера, чем для работы с MMS | число<br>Б | 65&nbsp;536 | O | -
**<a name="clientDestAddress">destAddress</a>** ||
ip | IP-адрес сервера, которому должны быть предназначены запросы | строка | - | М | -
port | Порт сервера, которому должны быть предназначены запросы | число | - | М | -
**<a name="clientSSL">ssl</a>** | Секция для конфигурирования защищенного соединения | объект | - | O | -
version | Версия SSL. <br>1 - TLSv1, <br>2 - SSLv2, <br>3 - SSLv3, <br>4 - SSLv23, <br>5 - TLSv1.1, <br>6 - TLSv1.2  | число | 0 | O | -
sniEnabled | Флаг использования SNI | логический | false | O | -
**<a name="clientAuthorization">authorization</a>** | Параметры авторизации | объект | - | O | -
type | Тип авторизации<br>Any;<br>Basic;<br>Digest. | строка | 1 | O | -
user | Логин для авторизации  | строка | - | M | -
password | Пароль для авторизации | строка | - | O | -
**<a name="clientAdditionalDir">additionalDir</a>** | Параметры менеджера соединений | объект | - | O | >=1.3.0.20
id | Список запасных направлений. | массив чисел | - | M | -
breakDownTimeout | Время ожидания перезагрузки направлений | число<br>мс | 30&nbsp;000 | O | -
maxErrorCount | Максимальное количество возможных ошибок | число | 1 | O | -
necromancy | Флаг восстановления соединений  | логический | false | O | -
useLoadSharing | Тип распределения нагрузки между направлениями<br>0 - не использовать;<br>1 - распределить между всеми;<br>2 - только между дополнительными при неактивном основном. | число | 0 | O | >= 1.2.8.15 (ATE 3.0 / Mobile-146)

#### Пример ####

```json
{
  "server": [
    {
      "id": 0,
      "address": {
        "ip": "195.218.228.2",
        "port": 80
      },
      "maxBufferSize": 524626,
      "activityTimer": 10,
      "ssl": {
        "version": 1,
        "certificatePath": "/home/usr/Projects/HTTP_Test/server/server-cert.pem",
        "privateKeyPath": "/home/usr/Projects/HTTP_Test/server/server-key.pem",
        "ciphers": "HIGH:!MD5:!RC4:!aNULL:!eNULL",
        "preferServerCiphers": true
      }
    }
  ],
  "client": [
    {
      "id": 0,
      "destAddress": {
        "ip":"192.168.205.146",
        "port": 8080
      },
      "persistant": true,
      "activityTimer": 120,
      "responseTimer": 60,
      "maxQueue": 5,
      "maxConnection": 50,
      "additionalDir": {
        "id": [
          1,
          2
        ],
        "maxErrorCount": 3,
        "breakDownTimeout": 50000,
        "necromancy": true,
        "useLoadSharing": 1
      }
    },
    {
      "id": 1,
      "destAddress": {
        "ip":"192.168.1.118",
        "port": 8881
      },
      "srcAddress": "192.168.1.119",
      "persistant": true,
      "ssl": {
        "version": 4
      },
      "activityTimer": 90,
      "responseTimer": 60,
      "maxQueue": 5,
      "maxConnection": 50
    },
    {
      "id": 2,
      "destAddress": {
        "ip":"192.168.1.118",
        "port": 8882
      },
      "srcAddress": "192.168.1.119",
      "persistant": true
    }
  ]
}
```