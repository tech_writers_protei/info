---
title: "metrics.json"
description: "Параметры сбора метрик"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/metrics.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этом файле задаются настройки сбора метрик.

## Используемые подсекции ##

* [general](#general) - общие параметры сбора метрик;
* [s1Interface](#s1Interface) - параметры сбора метрик S1;
* [s1Attach](#s1Attach) - параметры сбора метрик S1, связанных с регистрацией абонента;
* [s1Detach](#s1Detach) - параметры сбора метрик S1, связанных с дерегистрацией абонента;
* [s1BearerActivation](#s1BearerActivation) - параметры сбора метрик S1, связанных с активацией bearer-служб;
* [s1BearerDeactivation](#s1BearerDeactivation) - параметры сбора метрик S1, связанных с деактивацией bearer-служб;
* [s1BearerModification](#s1BearerModification) - параметры сбора метрик S1, связанных с модификацией bearer-служб;
* [s1Security](#s1Security) - параметры сбора метрик S1, связанных с аутентификацией абонента;
* [s1Service](#s1Service) - параметры сбора метрик S1, связанных с процедурой Service Req;
* [handover](#handover) - параметры сбора метрик, связанных с процедурой Handover;
* [tau](#tau) - параметры сбора метрик, связанных с процедурой Tracking Area Update;
* [sgsInterface](#sgsInterface) - параметры сбора метрик SGs;
* [svInterface](#svInterface) - параметры сбора метрик Sv (SRVCC HO);
* [resource](#resource) - параметры сбора метрик, связанных с ресурсами хоста;
* [users](#users) - параметры сбора различных счётчиков;
* [s11Interface](#s11Interface) - параметры сбора метрик S11;
* [paging](#paging) - параметры сбора метрик, связанных с процедурой Paging;
* [diameter](#diameter) - параметры сбора метрик, связанных с протоколом Diameter;
* [s6aInterface](#s6a-interface) - параметры сбора метрик, связанных с интерфейсом S6a;

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **<a name="general">general</a>** | Общие параметры сбора метрик. | | | O | P |          |
| <a name="enable">enable</a> | Флаг сбора метрик. | логический | false | O | P |          |
| <a name="directory">directory</a> | Директория для хранения метрик. | строка | ../metrics | O | P |          |
| <a name="granularity">granularity</a> | Гранулярность сбора метрик. | число<br>мин | 5 | O | P |          |
| <a name="node-name">nodeName</a> | Имя узла, указываемое в названии файла с метриками. | строка | | O | P |          |
| <a name="hh-mm-separator">hhMmSeparator</a> | Флаг включения разделителя между часами и минутами HH:MM.          | логический | false | O |  P |          |
| **<a name="s1Interface">s1Interface</a>** | Параметры для метрик S1. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1Attach">s1Attach</a>** | Параметры для метрик S1, связанных с регистрацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1Detach">s1Detach</a>** | Параметры для метрик S1, связанных с дерегистрацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1BearerActivation">s1BearerActivation</a>** | Параметры для метрик S1, связанных с активацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1BearerDeactivation">s1BearerDeactivation</a>** | Параметры для метрик S1, связанных с деактивацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1BearerModification">s1BearerModification</a>** | Параметры для метрик S1, связанных с модификацией bearer-службы. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1Security">s1Security</a>** | Параметры для метрик S1, связанных с аутентификацией абонента. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s1Service">s1Service</a>** | Параметры для метрик S1, связанных с процедурой Service Request. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="handover">handover</a>** | Параметры для метрик, связанных с процедурой Handover. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="tau">tau</a>** | Параметры для метрик, связанных с процедурой Tracking Area Update. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="sgsInterface">sgsInterface</a>** | Параметры для метрик SGs. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="svInterface">svInterface</a>** | Параметры для метрик Sv (SRVCC HO). | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="resource">resource</a>** | Параметры для метрик, связанных с ресурсами хоста. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="users">users</a>** | Параметры сбора различных счётчиков. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s11Interface">s11Interface</a>** | Параметры для метрик S11. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="paging">paging</a>** | Параметры для метрик, связанных с процедурой Paging. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="diameter">diameter</a>** | Параметры для метрик Diameter. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |
| **<a name="s6a-interface">s6aInterface</a>** | Параметры для метрик S6a. | | | O | P |          |
| enable | Флаг сбора метрик. | логический |[general::enable](#enable) | O | P |          |
| directory | Директория для хранения метрик. | строка | [general::directory](#directory) | O | P |          |
| granularity| Гранулярность сбора метрик. | число<br>мин | [general::granularity](#granularity) | O | P |          |
| nodeName | Имя узла, указываемое в названии файла с метриками. | строка | [general::nodeName](#node-name)| O | P |          |
| hhMmSeparator                                                   | Флаг включения разделителя между часами и минутами HH:MM.          | логический | [general::hhMmSeparator](#hh-mm-separator) | O |  P |          |

#### Пример ####

```json
{
  "general" : {
    "enable" : true,
    "granularity" : 30,
    "directory" : "/usr/protei/Protei_MME/metrics",
    "nodeName" : "MME-1"
  },
  "diameter" : {
    "enable" : true,
    "granularity" : 15
  },
  "s6aInterface" : {
    "enable" : true,
    "granularity" : 1
  }
}
```