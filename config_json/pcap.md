---
title: "PCAP"
description: "Параметры PCAP-логгера"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/pcap.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этом файле задаются настройки PCAP-логгера.

**Примечание.** При отсутствии файла всем параметрам будут заданы значения по умолчанию.

## Используемые подсекции ##

* **[General](#general)** - общие параметры логгера;

### Описание подсекций и их параметров ###

| Параметр                                        | Описание                                                                                        | Тип        | O/M | P/R | Версия  |
|-------------------------------------------------|-------------------------------------------------------------------------------------------------|------------|-----|-----|---------|
| **<a name="general">general</a>**               | Общие параметры логгера.                                                                        | объект     |     |     |         |
| enable                                          | Флаг активации логирования.<br>По умолчанию: true                                               | логический | O   | P   |         |
| <a name="separate_by_proto">separateByProto</a> | Флаг разделения логирования по протоколам, без разделения по абонентам.<br>По умолчанию: false. | логический | O   | P   |         |
| fileName                                        | Имя файла, если не указано имя абонента.<br>По умолчанию: ue_pcap_log.                          | строка     | O   | P   |         |
| customLogPath                                   | Директория для хранения журнала.<br>По умолчанию: /logs/pcap_trace/.                            | строка     | O   | P   |         |
| prefix                                          | Префикс к имени файла.<br>По умолчанию: "".                                                     | строка     | O   | P   | |

**Примечание.** Поле [separateByProto](#separate_by_proto) используется только в случаях, когда не указано имя абонента.

#### Пример ####

```json
{
  "general" : {
    "enable" : true,
    "separateByProto" : false,
    "fileName" : "ue_pcap_lo",
    "customLogPath" : "/logs/pcap_trace/",
    "prefix" : "prefix"
  }
}
```