---
title: "mme.json"
description: "Основные настройки MME"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/mme.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этом файле задаются основные настройки Protei MME.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload mme.json**.

## Используемые подсекции ##

* **[general](#general)** - общие параметры узла;  
* **[timeout](#timeout)** - параметры времени ожидания;  
* **[database](#database)** - параметры базы данных;  
* **[gutiReallocation](#guti)** - параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>;  
* **[dcn](#dcn)** - параметры <abbr title="Dedicated Core Network">DCN</abbr>;
* **[ranges](#ranges)** - диапазоны создаваемых ММЕ идентификаторов;  
* **[vendorSpecific](#vendor_specific)** - параметры, задающие ограничения, вызванные оборудованием других производителей;  
* **[profileRestoration](#profile_restoration)** - флаги, разрешающие определённым процедурам искать незнакомый абонентский профиль в БД;  
* **[enbId](#enodeb)** - параметры допустимых eNodeB.

### Описание подсекций и их параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| **<a name="general">general</a>** | Общие параметры узла | объект | | O | | |
| handlers | Количество задействованных логик. | число | 10 | O | P | |
| coreCount | Количество ядер. | число<br>1-14 | 2 | O | P |       |
| echoSendPeriod | Периодичность отправки сообщения Echo Request по интерфейсу GTP-C. | число, с<br>10-600 | 60 | O | R | |
| isr      | Флаг `Operation Indication` в сообщении Delete Session Request. | логический | false | O | R | |
| dnsIpv6 | Флаг запроса IPv6-адреса от DNS-сервера. | логический | false | O | R | |
| releaseOnIcsFail | Индикатор способа очистки контекста после получения сообщения ICS Failure.<br>`E` — explicit, явно;/`I` — implicit, неявно. | строка | E | O | R | |
| releaseOnServiceReject | Индикатор способа очистки контекста после получения сообщения Service Reject.<br>`E` — explicit, явно / `I` — implicit, неявно. | строка | E | O   | R   | |
| ignoreEnbUeIdReset | Флаг игнорирования значения `UE-associated logical S1-connection Item` при проведении процедуры S1AP-RESET для устройств, имеющих указанный идентификатор `eNB UE S1AP ID`. | логический | false | O | R | |
| radioPriority | Значение `Radio Priority`, отправляемое в SGSN Context Response. | число, <br>0-7 | 2 | O | R | |
| radioPrioritySms | Значение `Radio Priority SMS`, отправляемое в SGSN Context Response. | число, <br>0-7 | 1 | O | R | |
| oldS1ReleaseCauseCode | Код причины, отправляемый в сообщении S1AP: UE CONTEXT RELEASE ввиду дублирования информации об абоненте.<br>`0` - Normal Release, `1` - Release due to E-UTRAN Generated Reason. По умолчанию: 0. | число, 0-1 | 0 | O | R | |
| srvccS1ReleaseCauseCode | Код причины, отправляемый в сообщении S1AP: UE CONTEXT RELEASE COMMAND после успешного SRVCC<br>`0` - Normal Release, `1` - Successful Handover. По умолчанию: 0. | число, 0-1 | 0 | O | R | |
| s1apPcsmThreads | Количество потоков, выделяемых для S1AP PCSM. По умолчанию: 1. | число, больше 0 | O | R | |
| **<a name="timeout">timeout</a>** | Параметры времени ожидания. | объект | | O | R | |
| contextReqRelease | Время ожидания удаления сессии на предыдущем узле SGW после приёма Context-Request от нового узла ММЕ. | число<br>мс | 5&nbsp;000 | O | R | |
| **[handover](#ho_timeout)** | Параметры времени ожидания при хэндовере | объект | | O | R | |
| **<a name="database">database</a>** | Параметры базы данных. | объект | | O | | |
| enable | Флаг взаимодействия с базой данных. | логический | false | O | R | |
| <a name="hostname">hostname</a> | DNS-имя или IP-адрес хоста базы данных. | строка | localhost | O | P | |
| <a name="port">port</a> | Порт подключения к базе данных. | число | 3306 | O | P | |
| username | Логин для авторизации в базе данных. | строка | - | O | P | |
| password | Пароль для авторизации в базе данных. | строка | - | O | P | |
| dbName | Название базы данных. | строка | - | O | P | |
| updatePeriod | Периодичность обновления базы данных. | число<br>мс | 10&nbsp;000 | O | R | |
| readerCount | Количество соединений базы данных с правами на чтение. | число<br>1-100 | 3 | O | P | |
| unixSock  | Путь до сокета базы данных.<br>**Примечание.** Если задан, то параметры [hostname](#hostname) и [port](#port) игнорируются. | строка | - | O | P | |
| **<a name="guti">gutiReallocation</a>** | Параметры переназначения <abbr title="Globally Unique Temporary UE Identity">GUTI</a>. | объект | | O | R | |
| reqCount | Максимальное количество обработанных запросов Attach/TAU/Service Request до смены GUTI. | число | 0 | O | R | |
| reallocPeriod | Периодичность смены GUTI. | число<br>мс | 0 | O | R | |
| **<a name="dcn">dcn</a>** | Набор используемых <abbr title="Dedicated Core Network">DCN</abbr>. Формат:<br>`[{"id": <dcn_id>, "relativeCapacity": <relative_capacity>}, ...]`. | массив объектов | | O | R | |
| \<dcn_id\> | Идентификатор сети. | число | - | M | R | |
| \<relative_capacity\> | Относительная ёмкость. | число | - | M | R | |
| **<a name="ranges">ranges</a>** | Диапазоны создаваемых ММЕ идентификаторов. | объект | | O | P | |
| mmeUeId | Границы диапазона допустимых MME UE ID. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| mTmsi | Диапазон допустимых M-TMSI для GUTI. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| teid | Диапазон допустимых TEID, используемых для идентификации абонента на интерфейсе S11. Формат:<br>`<min>-<max>`.<br>**Примечание.** Каждая из границ должна быть в диапазоне [1; 2<sup>32</sup>-1\]. | строка | - | O | P | |
| **<a name="vendor_specific">vendorSpecific</a>** | Параметры, задающие ограничения, вызванные оборудованием других производителей. | объект | - | O | | |
| longUtranTransparentContainer | Флаг разрешения использования длинного (более 1024 байт) UTRAN Transparent Container при хэндовере от 4G к 3G. | логический | true | O | R | |
| suspend | Флаг поддержки процедуры Suspend для bearer-служб. | логический | false | O | R | |
| detachIndAfterHo | Флаг выполнения процедуры Detach Indication после перехода абонента в 2G/3G. | логический | true | O | R | |
| **<a name="profile_restoration">profileRestoration</a>** | Флаги, разрешающие определённым процедурам искать незнакомый абонентский профиль в БД. | объект | - | O | R | |
| serviceReq | Флаг разрешения поиска профиля при получении Service Req. | логический | true | O | R | |
| extServiceReq | Флаг разрешения поиска профиля при получении Extended Service Req. | логический | false | O | R | |
| tau | Флаг разрешения поиска профиля при получении TAU Req. | логический | false | O | R | |
| mtSms | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT SMS. | логический | false | O | R | |
| mtCall | Флаг разрешения поиска профиля при получении SGsAP-PAGING-REQUEST для MT Call. | логический | false | O | R | |
| **<a name="enodeb">enbId</a>** | Маски белого списка идентификаторов eNodeB. Формат:<br>`[{"enable": <enable>, "id": <id>}, ... ]`<br>**Примечание.** Если нет записей, то любой идентификатор считается допустимым. | массив объектов | - | O | R | |
| \<enable\> | Флаг активности маски идентификатора eNodeB. | логический | true | O | R | |
| \<id\> | Маска идентификатора eNodeB. | строка (PCRE) | - | M | R | |

#### handover {#ho_timeout}

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| s1HoContextRelease | Время ожидания очистки контекста на предыдущей базовой станции при хэндовере S1. | число<br>мс | 5&nbsp;000 | O | R | |
| hoFromUtranTau | Время ожидания сообщения TAU-Request при хэндовере из сети UTRAN. | число<br>мс | 10&nbsp;000 | O | R | |
| hoFromGeranTau | Время ожидания сообщения TAU-Request при хэндовере из сети GERAN. | число<br>мс | 10&nbsp;000 | O | R | |
| x2HoDelSession | Время ожидания отложенного удаления сессий на исходном SGW при X2-хэндовере. | число<br>мс | 2&nbsp;000 | O | R | |
| eToUtranHoRelease | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в UTRAN. | число<br>мс | 5&nbsp;000 | O | R |  |
| eToGeranHoRelease | Время ожидания удаления ресурсов eNodeB и старого SGW при хэндовере из сети E-UTRAN в GERAN. | число<br>мс | 5&nbsp;000 | O | R |  |

### Пример ###

```json
{
  "general" : {
    "handlers" : 100,
    "coreCount" : 3,
    "releaseOnIcsFail" : "I",
    "echoSendPeriod" : 22,
    "radioPrioritySms" : 3
  },
  "timeout" : {
    "contextReqRelease" : 3000,
    "handover" : {
      "hoFromGeranTau" : 8000,
      "eToGeranHoRelease" : 4000
    }
  },
  "database" : {
    "enable" : true,
    "username" : "mme",
    "password" : "777",
    "dbName" : "MME",
    "unixSock" : "/var/lib/mysql/mysql.sock"
  },
  "gutiReallocation" : {
    "reqCount" : 100
  },
  "dcn" : [
    {
      "id": 10,
      "relativeCapacity": 50
    },
    {
      "id": 24,
      "relativeCapacity": 70
    }
  ],
  "ranges" : {
    "mmeUeId" : "50-100500",
    "teid" : "100-50000"
  },
  "vendorSpecific" : {
    "suspend" : true,
    "detachIndAfterHo" : false
  },
  "profileRestoration" : {
    "mtSms" : true
  },
  "enbId" : [
    {
      "id": "107217"
    },
    {
      "enable": true,
      "id": "3584*"
    },
    {
      "enable": false,
      "id": "3585*"
    }
  ]
}
```