---
title: "ue_trace.json"
description: "Параметры трассировки абонентов"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/ue_trace.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этом файле задаётся набор абонентов, по которым выводятся сообщения в **ue_trace**.

Ключ для перезагрузки — **reload ue_trace.json**.

### Описание параметров ###

| Параметр | Описание      | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|---------------|--------|--------------|-----|-----|--------|
| msisdn   | Номер MSISDN. | строка | -            | O   | R   |        |
| imsi     | Номер IMSI.   | строка | -            | O   | R   |        |

#### Пример ####

```json
[
  {
    "msisdn" : "76000000316",
    "imsi" : "001010000000398"
  },
  {
    "msisdn" : "76000000317"
  },
  {
    "imsi" : "001010000000399"
  }
]
```