---
title: "DNS"
description: "Параметры компонента DNS"
weight: 40
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/dns.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются настройки компонента DNS.

**Примечание.** Наличие файла обязательно.

Ключ для перезагрузки — **reload dns**.

## Используемые подсекции ##

* **[client](#client)** - параметры клиентских подключений;
* **[server](#server)** - параметры серверных подключений;
* **[clientPool](#client-pool)** - параметры пула клиентских подключений;

### Описание подсекций и их параметров ###

| Параметр                                     | Описание                                                                                                                          | Тип             | По умолчанию | O/M | Версия  |
|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|-----------------|--------------|-----|---------|
| **<a name="client">\[client\]</a>**          | Параметры клиентской части.                                                                                                       | массив объектов | -            | M   |         |
| id                                           | Идентификатор направления.                                                                                                        | число           | -            | M   |         |
| transport                                    | Транспортный протокол.<br>UDP/TCP.                                                                                                | строка          | UDP          | O   |         |
| destAddress                                  | IP-адрес сервера, куда направляются запросы.                                                                                      | строка              | -            | M   |         |
| destPort                                     | Прослушиваемый порт сервера.                                                                                                      | число           | 53           | O   |         |
| srcAddress                                   | Локальный IP-адрес.                                                                                                               | строка              | 0.0.0.0      | O   |         |
| srcPort                                      | Локальный порт.                                                                                                                   | число           | 0            | O   |         |
| responseTimeout                              | Время ожидания ответа.                                                                                                            | число<br>с      | 60           | O   |         |
| resendOverTcp                                | Флаг перепосылки запроса по TCP для получения полного ответа.                                                                     | логический      | false        | O   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>. | число           | 0            | O   |         |
| ipMtuDiscover                                | Используемое значение IP MTU Discover. | число           | 0            | O   |         |
| **<a name="server">\[server\]</a>**          | Параметры серверной части.                                                                                                        | массив объектов | -            | M   |         |
| id                                           | Идентификатор направления.                                                                                                        | число           | -            | M   |         |
| transport                                    | Транспортный протокол.<br>UDP/TCP.                                                                                                | строка          | UDP          | O   |         |
| address                                      | Прослушиваемый IP-адрес.                                                                                                          | строка          | 0.0.0.0      | O   |         |
| port                                         | Прослушиваемый порт.                                                                                                              | число           | 53           | O   |         |
| dscp                                         | Значение поля заголовка IP <abbr title="Differentiated Services Code Point">DSCP</abbr>/<abbr title="Type of Service">ToS</abbr>. | число           | 0            | O   |         |
| ipMtuDiscover                                | Используемое значение IP MTU Discover. | число           | 0            | O   |         |
| **<a name="client-pool">\[clientPool\]</a>** | Параметры пула клиентов.                                                                                                          | массив объектов | -            | O   |         |
| id                                           | Идентификатор пула клиентских направлений.                                                                                        | число           | -            | M   |         |
| directions                                   | Клиентские направления, задействованные в балансировке.                                                                           | массив чисел    | -            | M   |         |

#### Пример ####

```json
{
  "client": [
    {
      "id": 1,
      "destAddress": "8.8.8.8",
      "resendOverTcp": true
    },
    {
      "id": 2,
      "destAddress": "127.0.0.1",
      "destPort": 1234,
      "responseTimeout": 10
    },
    {
      "id": 3,
      "destAddress": "127.0.0.1",
      "destPort": 53,
      "dscp": 56
    }
  ],
  "server": [
    {
      "id": 1,
      "address": "127.0.0.1",
      "port": 1234
    },
    {
      "id": 2
    }
  ],
  "clientPool": [
    {
      "id": 1,
      "directions": [2, 3]
    }
  ]
}
```