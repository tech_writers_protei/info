---
title: "Описание конфигурационных файлов компонент"
description: ""
weight: 40
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/component/_index.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

Конфигурационные файлы компонент располагаются в директории `/usr/protei/Protei_MME/config/component`
