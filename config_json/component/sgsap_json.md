---
title: "sgsap.json"
description: "Параметры компонента SGsAP"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/component/sgsap_json.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются настройки компонентов SGsAP и SGsAP.PCSM.
Может быть лишь один компонент первого типа, однако компонентов второго типа может быть несколько.

Ключ для перезагрузки — **reload component/sgsap.json**.

**Примечание.** Наличие файла обязательно.

### Описание параметров Sg.SGsAP {#sgsap}

| Параметр                 | Описание                                                                                   | Тип             | По умолчанию | O/M | P/R | Версия |
|--------------------------|--------------------------------------------------------------------------------------------|-----------------|--------------|-----|-----|--------|
| [peerTable](#peer-table) | Таблица пиров.                                                                             | массив объектов | -            | O   | R   |        |
| defaultPcsm              | Имя узла PCSM по умолчанию.                                                                | массив строк    | -            | O   | R   |        |


### Параметры элемента таблицы peerTable {#peer-table}

| Параметр  | Описание                                       | Тип             | По умолчанию | O/M | P/R | Версия  |
|-----------|------------------------------------------------|---------------- |--------------|-----|-----|---------|
| gt        | Глобальный заголовок пира.                     | строка          | -            | M   | R   |         |
| peerName  | Имя пира.                                      | строка          | -            | O   | R   |         |
| pcsmList  | Набор параметров отдельных PCSM.               | массив объектов | -            | M   | R   |         |
| **{**     |                                                |                 |              |     |     |         |
| peerIp    | IP-адрес пира.                                 | ip              | -            | M   | R   |         |
| pcsm      | Компонентный адрес соответствующего узла PCSM. | строка          | -            | M   | R   |         |
| priority  | Приоритет.                                     | число           | 0 (высший)   | O   | R   |         |
| weight    | Вес.                                           | число           | 1            | O   | R   |         |
| **}**     |                                                |                 |              |     |     |         |

### Описание параметров Sg.SGsAP.PCSM ###

| Параметр                         | Описание                                                                         | Тип             | По умолчанию                                                       | O/M | P/R | Версия   |
|----------------------------------|----------------------------------------------------------------------------------|-----------------|--------------------------------------------------------------------|-----|-----|----------|
| **{**                            |                                                                                  |                 |                                                                    |     |     |          |
| pcsmAddress  | Компонентый адрес PCSM.                                                  | строка              | -                                                                  | M   | R   |          |
| <a name="peer-ip">peerIp</a>     | IP-адрес подключения узла PCSM.                                                  | ip              | -                                                                  | M   | R   |          |
| <a name="peer-port">peerPort</a> | Порт подключения узла PCSM.                                                      | строка          | -                                                                  | M   | R   |          |
| <a name="src-ip">srcIp</a>       | Локальный IP-адрес узла PCSM.                                                    | строка          | [sgsap :: \[localAddress\] :: localHost](../../sgsap/#local-host/) | O   | R   |          |
| <a name="src-port">srcPort</a>   | Локальный порт узла PCSM.                                                        | строка          | [sgsap :: \[localAddress\] :: localPort](../../sgsap/#local-port/) | O   | R   |          |
| remoteInterfaces                 | Удаленные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }` | массив строк | -                                                                  | O   | R   |          |
| localInterfaces                  | Локальные адреса для Multihoming. Формат:<br>`{ "<ip>:<port>"; "<ip>:<port>"; }` | массив строк | -                                                                  | O   | R   |          |
| **<a name="sctp_additional_info">sctpAdditionalInfo</a>** | Дополнительные параметры SCTP. | объект | - | O | P | |
| nodelay                      | Флаг активации SCTP nodelay. | логический | false | O | P | |
| maxInitRetransmits                               | Количество попыток отправки сообщения INIT, прежде чем хост считать недоступным. | число | 10 | O | R | |
| initTimeout                                      | Время ожидания сообщения INIT_ACK. | число<br>мс | 1000 | O | R | |
| inStreams                                        | Количество входящих SCTP-потоков. | число<br>1-65&nbsp;536 | 1 | O | P | |
| outStreams                                       | Количество исходящих SCTP-потоков. | число<br>1-65&nbsp;536 | 1 | O | P | |
| associationMaxRetrans                            | Максимальное количество повторных отправок, после которых маршрут считается недоступным. | число | 10 | O | R | |
| rtoMax                                           | Максимальное значение RTO. | число<br>мс | 60&nbsp;000  | O | R | |
| rtoMin                                           | Минимальное значение RTO. | число<br>мс | 1000 | O | R | |
| rtoInitial                                       | Начальное значение RTO. | число<br>мс | 3000 | O | R | |
| hbInterval                                       | Период посылки сигнала heartbeat. | число<br>мс | 30&nbsp;000  | O | R | |
| maxRetrans                                       | Максимальное количество попыток отправки сообщения на адрес. | число | 5 | O | R | |
| sackDelay                                        | Время ожидания отправки сообщения SACK. | число | 200 | O | R | |
| shutdownEvent                                    | Флаг включения индикации о событии SHUTDOWN от ядра. | логический | true | O | R | |
| assocChangeEvent                                 | Флаг включения индикации об изменении состояния ассоциации от ядра. | логический | false | O | R | |
| peerAddrChangeEvent                              | Флаг включения индикации об изменении состояния peer в ассоциации от ядра. | логический | false | O | R | |
| sndBuf                                           | Размер буфера сокета для отправки, `net.core.wmem_default`.<br>**Внимание.** Значение удваивается, удвоенный размер не может превышать `net.core.wmem_max`. | число | значения kernel | O | R | |
| ipMtuDiscover                                    | Флаг разрешения использования алгоритма Path MTU Discovery.<br>`-1` - нет изменений;<br>`0` - алгоритм не используется, флаг заголовка IP `Don't fragment (DF)` не активирован;<br>`1` - алгоритм используется, флаг заголовка IP `Don't fragment (DF)` активирован.| число | -1 | O | R | |
| dscp                                             | Значение поля заголовка IP DSCP/ToS. | число | -1 | O | R | |
| txBufStatistics | Флаг включения вывода в `si.log` о состоянии sctp-буфера.<br>**Примечание.** Должна быть включена запись журнала **si.log**, для чего достаточно задать `level = 1`. Информация выводится при заполнении SCTP-буфера. | логический | false | O | R | |
| warningTxBufSize | Порог для вывода сообщений SCTP-буфера в журнал **warning.log** в байтах. | число | 1024 | O | R | |
| <a name="critical">criticalTxBufSize</a> | Порог для срабатывания индикации о переполнении SCTP-буфера в байтах. | число | 0 | O | R | |
| onCriticalTxBufSize | Действие по достижении [criticalTxBufSize](#critical).<br>`0` - отправка индикации о переполнении на верхний уровень;<br>`1` - разрыв SCTP-ассоциации. | число |  0 | O | R | |
| **}**                            |                                                                                  |                 |                                                                    |     |     |          |

**Примечание.** При значениях `peerIp = ""` и `peerPort = 0` PCSM ожидает подключения с адреса, указанного в описании PCSM в секции [Sg.SGsAP](#sgsap).

#### Пример ####

```json
{
  "sgsap": {
    "peerTable": [
      {
        "peerIp": "192.168.125.154",
        "gt": "79216567568",
        "pcsmList": [
          {
            "peerIp": "192.168.125.154",
            "pcsm": "Sg.SGsAP.PCSM.0"
          }
        ]
      },
      {
        "peerIp": "192.168.125.154",
        "gt": "79216561234",
        "peerName": "MultiIP_Peer",
        "pcsmList": [
          {
            "peerIp": "192.168.126.155",
            "pcsm": "Sg.SGsAP.PCSM.1",
            "weight": 1,
            "priority": 1
          },
          {
            "peerIp": "192.168.126.156",
            "pcsm": "Sg.SGsAP.PCSM.2",
            "weight": 2,
            "priority": 1
          }
        ]
      }
    ],
    "defaultPcsm": ["Sg.SGsAP.PCSM.0"]
  },
  "pcsm": [
    {
      "pcsmAddress": "Sg.SGsAP.PCSM.0",
      "peerIp": "192.168.125.154",
      "peerPort": 29118,
      "srcIp": "192.168.126.67",
      "srcPort": 29119
    },
    {
      "pcsmAddress": "Sg.SGsAP.PCSM.1",
      "peerIp": "192.168.125.155",
      "peerPort": 29118,
      "srcIp": "192.168.126.67",
      "srcPort": 29119
    },
    {
      "pcsmAddress": "Sg.SGsAP.PCSM.2",
      "peerIp": "192.168.125.156",
      "peerPort": 29118,
      "srcIp": "192.168.126.67",
      "srcPort": 29119
    }
  ]
}
```