---
title: "diameter.json"
description: "Файл настройки Diameter-соединений"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/diameter.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются настройки подключений по протоколу Diameter.

Ключ для перезагрузки — **reload diameter**.

## Используемые секции ##

* **[\[general\]](#general)** - основные параметры протокола Diameter;
* **[\[localAddress\]](#local_address)** - параметры локального интерфейса;
* **[\[localPeerCapabilities\]](#local-peer-capabilities)** - параметры локальных peer;
* **[\[specificLocalPeerCapabilities\]](#specific-local-peer-capabilities)** - параметры списка LocalPeerCapabilities, специфичных для отдельного PCSM;
* **[\[timers\]](#timers)** - параметры таймеров;
* **[\[resend\]](#resend)** - параметры повторной отправки сообщения;

### Описание параметров ###

| Параметр                               | Описание                                | Тип    | По умолчанию | O/M | P/R | Версия   |
|----------------------------------------|-----------------------------------------|--------|--------------|-----|-----|----------|
| **<a name="general">\[general\]</a>**  | Основные параметры протокола Diameter.  | объект | -            | O   | R   |          |
| caseSensitive                          | Флаг сохранения регистра в строковых AVP вместо приведения к нижнему регистру. | логический | true | O | R | |
| receivingFromAnyHost                   | Флаг обработки запросов, если `Destination-Host` не совпадает с `Local-Host`.  | логический | false | O | R | |
| maxTimeoutCount                        | Количество истекших таймеров ожидания ответа, после которого хост считается занятым. | число | 10 | O | R | |
| useResend                              | Флаг использования перепосылки по истечении времени ожидания. | логический | false | O | R | |
| necromancy                             | Флаг отправки запросов на занятый хост, если свободные не найдены. | логический | false | O | R | |
| unsupportedCommand                     | Флаг обработки сообщений, которые явно не поддерживаются в библиотеке. | логический | false | O   | R   | |
| replaceOriginIdentities                | Флаг подмены `Origin-Host` и `Origin-Realm` на указанные в PCSM значения во всех отправляемых сообщениях. | логический | false | O   | R   | |
| pcsmCreationTimer                | Таймер, по истечению которого DIAM переходит в активное состояние не дожидаясь создания оставшихся PCSM. | число<br>мс | 2000 | O   | R   | |
| **<a name="local_address">\[localAddress\]</a>** | Параметры локального интерфейса. | объект | - | M | P | |
| <a name="local_host">localHost</a>               | Адрес локального хоста. | ip | 0.0.0.0 | M | P | |
| <a name="local_port">localPort</a>               | Номер локального порта. | число | 3868 | M | P | |
| transport                                        | Используемый транспортный протокол.<br>`tcp`/`sctp`. | строка | tcp | O | P | |
| <a name="local_interfaces">localInterfaces</a>  | Перечень IP-адресов для мультихоуминга. Формат:<br>`<ip>:<port>`| массив строк |- | O   | P   |          |
| inStreams                                        | Количество входящих SCTP-потоков. | число<br>1-65&nbsp;536 | 1 | O | P | |
| outStreams                                       | Количество исходящих SCTP-потоков. | число<br>1-65&nbsp;536 | 1 | O | P | |
| maxInitRetransmits                               | Количество попыток отправки сообщения INIT, прежде чем хост считать недоступным. | число<br>мс | 10 | O | R | |
| initTimeout                                      | Время ожидания сообщения INIT_ACK. | число<br>мс | 1000 | O | R | |
| rtoMax                                           | Максимальное значение RTO. | число<br>мс | 60&nbsp;000  | O | R | |
| rtoMin                                           | Минимальное значение RTO. | число<br>мс | 1000 | O | R | |
| rtoInitial                                       | Начальное значение RTO. | число<br>мс | 3000 | O | R | |
| hbInterval                                       | Период посылки сигнала heartbeat. | число<br>мс | 30&nbsp;000  | O | R | |
| maxRetrans                                       | Максимальное количество попыток отправки сообщения на адрес. | число | 5 | O | R | |
| dscp                                             | Значение поля заголовка IP DSCP/ToS. | число | -1 | O | R | |
| associationMaxRetrans                            | Максимальное количество повторных отправок, после которых маршрут считается недоступным. | число | 10 | O | R | |
| sackDelay                                        | Время ожидания отправки сообщения SACK. | число | - | O | R | |
| sndBuf                                           | Размер буфера сокета для отправки, `net.core.wmem_default`.<br>**Внимание.** Значение удваивается, удвоенный размер не может превышать `net.core.wmem_max`. | число | - | O | R | |
| shutdownEvent                                    | Флаг включения индикации о событии SHUTDOWN от ядра. | логический | - | O | R | |
| assocChangeEvent                                 | Флаг включения индикации об изменении состояния ассоциации от ядра. | логический | - | O | R | |
| peerAddrChangeEvent                              | Флаг включения индикации об изменении состояния peer в ассоциации от ядра. | логический | - | O | R | |
| **<a name="local-peer-capabilities">\[localPeerCapabilities\]</a>** | Параметры локальных peer. | объект | - | O | R | |
| <a name="origin-host-diam">originHost</a>                           | Идентификатор хоста, `Origin-Host`. | строка | - | M | R | |
| <a name="origin-realm-diam">originRealm</a>                         | Realm хоста, `Origin-Realm`. | строка | - | M | R | |
| vendorId                                                            | Идентификатор производителя, `Vendor-Id`. | число | - | M | R | |
| productName                                                         | Название системы, `Product-Name`. | строка | - | M | R | |
| firmwareRevision                                                    | Версия программного обеспечения, `Firmware-Revision`. | число |  | O | R | |
| originStateId                                                       | Идентификатор состояния, `Origin-State-Id`.<br>**Примечание.** Если не задан, то каждый раз при перезагрузке `Origin-State-Id` принимает уникальное значение. Задается конкретное значение, чтобы удаленные пиры не инициировали сброс сессий при перезагрузке программного обеспечения. | число | 0 | O | R | |
| hostIpAddress                                                       | Перечень локальных адресов, `Host-IP-Address`. Формат: `{"address": <строка>, "family": <число> }`. Варианты <family>: 1 - `IPv4`, 2 - `IPv6`, 8 - `E164`. По умолчанию: 1 | массив объектов | - | M | R | |
| authApplicationId                                                   | Перечень идентификаторов поддерживаемых приложений, `Auth-Application-Id`. | массив чисел | - | O | R | |
| acctApplicationId                                                   | Перечень идентификаторов поддерживаемых аккаутинговых приложений, `Acct-Application-Id`. | массив чисел | - | O | R | |
| vendorSpecificApplicationId                                         | Перечень идентификаторов приложений, определяемых вендором, `Vendor-Specific-Application-Id`. Формат см. [ниже](#vendor_specific_application_id). | массив объектов | - | O | R | |
| inbandSecurityId                                                    | Перечень идентификаторов поддерживаемых механизмов обеспечения безопасности, `Inband-Security-Id`. <br>**Примечание.** Поддерживается только `0, NO_SECURITY`. | массив чисел            | - | O | R | |
| supportedVendorId                                                   | Перечень идентификаторов поддерживаемых производителей, `Supported-Vendor-Id`.<br>**Примечание.** Используется только для формирования сообщения Diameter: Capabilities-Exchange-Request. | массив чисел | - | O | R | |
| drmp                                                | Значение параметра Diameter Routing Message Priority | число 0-15 | | O | R | |
| forceDestinationHost                                                | Код формата заполнением AVP Destination-Host.<br>`0` - режим обратной совместимости, очищение `Destination-Host` только при повторной отправке запроса;<br>`1` - режим без изменения `Destination-Host`;<br>`2` - режим, при котором очищается значение `Destination-Host` при не совпадении со значением из PCSM;<br>`3` - режим, при котором значение `Destination-Host` заменяется значением из PCSM, если значения не совпадают или AVP отсутствует. | число | 0 | O | R | |
| **<a name="specific-local-peer-capabilities">\[specificLocalPeerCapabilities\]</a>** | Перечень LocalPeerCapabilities, специфичных для отдельного PCSM. Формат:<br>`{ "pcsmAddress": <pcsmAddress> , "localPeerCapabilities":<localPeerCapabilities> }`.<br>**Примечание.** Формат \<localPeerCapabilities\> аналогичен [\[localPeerCapabilities\]](#local-peer-capabilities). | массив объектов | - | O   | R | |
| **<a name="timers">\[timers\]</a>**                            | Параметры таймеров. | объект | - | O | R | |
| <a name="appl-timeout">applTimeout</a>                         | Время ожидания установления Diameter-соединения.<br>**Примечание.** Отсчитывается с момента посылки запроса на установление TCP-соединения до получения Diameter: Capabilities-Exchange-Answer. | число<br>мс | 40&nbsp;000 | O | R | |
| <a name="watchdog-timeout">watchdogTimeout</a>                 | Время ожидания посылки сообщений Diameter: Device-Watchdog-Request/Answer, контроль состояния соединения.<br>**Примечание.** Отсчитывается с момента посылки последнего сообщения, не обязательно Diameter: DWR. | число<br>мс | 10&nbsp;000 | O | R | |
| <a name="reconnect-timeout">reconnectTimeout</a>               | Время ожидания переустановление соединения.<br>**Примечание.** Отсчитывается время от разрушения соединения до очередной попытки восстановления соединения. | число<br>мс | 30&nbsp;000 | O | R | |
| <a name="on-busy-reconnect-timeout">onBusyReconnectTimeout</a> | Время ожидания переустановления соединения после получения сообщения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = BUSY (1)`.<br>**Примечание.** Если 0, то соединение не переустанавливается. | число<br>мс | 60&nbsp;000 | O | R | |
| <a name="on-shutdown-reconnect-timeout">onShutdownReconnectTimeout</a>  | Время ожидания переустановления соединения после получения Diameter: Disconnect-Peer-Request с причиной `DisconnectCause = DO_NOT_WANT_TO_TALK_TO_YOU (2)`.<br>**Примечание.** Если 0, то соединение не переустанавливается. | число<br>мс | 0 | O | R | |
| <a name="response-timeout">responseTimeout</a>                 | Время ожидания ответа. | число<br>мс | 10&nbsp;000 | O | R | |
| <a name="breakdown-timeout">breakdownTimeout</a>               | Время временной недоступности узла PCSM.  | число<br>мс | 30&nbsp;000 | O | R | |
| <a name="statistic-timeout">statisticTimeout</a>               | Период вывода статистики в лог-файлы.  | число<br>мс | 60&nbsp;000 | O | R | |
| **<a name="resend">\[resend\]</a>**                            | Параметры повторной отправки сообщения.  | объект | - | O | R | |
| resetCountForSetBusyTimeout                                    | Период сброса счетчиков [countForSetBusy](#count_for_set_busy).  | число<br>мс | 10&nbsp;000  | O | R | |
| resendInfo                                                     | Параметры соответствия кодов `Result-Code` и деактивации узла PCSM. Формат см. [ниже](#resend_info)| массив объектов | - | O | R | |
| **<a name="cdr">\[cdr\]</a>**                            | Параметры diam_cdr  | объект | - | O | R | |
| additionalFields | Конфигурация дополнительных полей журнала diam_cdr. Массив объектов вида `{ "avpName" : <строка>, "avpPath": <avpPath>, "messageType": <messageType>}`  выводятся только общие поля | массив объектов | | O | R | |
| avpPath       | Список пар-идентификаторов AVP (путь) до нужной AVP. AVP идентифицируется парой значений: Id (mandatory) и VendorId (optional). Массив объектов вида `{"id" : <число>, "vendorId" : <число> }` | массив объектов | | O | R | |
| messageType    | Тип сообщения answer/request | строка | "request" | O | R | |

### Формат vendorSpecificApplicationId {#vendor_specific_application_id}

Поле vendorSpecificApplicationId может иметь эначения формата

``` json
{
  "vendorId": <vendor_id>,
  "authApplicationId": <auth_application_id>
}
```

или

``` json
{
  "vendorId": <vendor_id>,
  "acctApplicationId": <acct_application_id>
}
```

### Формат resendInfo {#resend_info}

| Параметр                               | Описание                                | Тип    | По умолчанию | O/M | P/R |
|----------------------------------------|-----------------------------------------|--------|--------------|-----|-----|
| <a name="result_code">resultCode</a>                           | Значение поля `Result-Code`, при котором совершается повторная отправка сообщений на альтернативный узел PCSM. | число | - | M | R |
| <a name="count_for_set_busy">countForSetBusy</a>               | Количество ответов с указанным значением [resultCode](#result_code), при котором PCSM становится неактивным.<br>**Примечание.** Счетчик сбрасывается при получении ответа с не указанным в перечне `ResultCode`. | число | - | M | R |

**Примечание.** `ResultCode = 3004 (TOO_BUSY)` и `CountForSetBusy = 1` добавляется автоматически, если не задан явно.

#### Пример ####

```json
{
  "specificLocalPeerCapabilities": {
    {
      "pcsmAddress": "Sg.DIAM.PCSM.2",
      "localPeerCapabilities": {
        "originRealm": "protei.iot1.com",
        "hostIpAddress": [
          {
            "192.168.108.36"
          }
        ],
        "authApplicationId": [
          16777217
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777217
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.4",
      "localPeerCapabilities": {
        "originRealm": "protei.ru",
        "hostIpAddress": [
          {
            "192.168.112.165"
          }
        ],
        "authApplicationId": [
          16777216
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777216
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    }
  }
}
```

<!--
Возможно, конфиг должен выглядеть так?

```json
{
  "specificLocalPeerCapabilities": [ <-- тут array вместо object
    {
      "pcsmAddress": "Sg.DIAM.PCSM.2",
      "localPeerCapabilities": {
        "originRealm": "protei.iot1.com",
        "hostIpAddress": [
          "192.168.108.36" <-- тут string вместо object
        ],
        "authApplicationId": [
          16777217
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777217
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    },
    {
      "pcsmAddress": "Sg.DIAM.PCSM.4",
      "localPeerCapabilities": {
        "originRealm": "protei.ru",
        "hostIpAddress": [
          "192.168.112.165" <-- тут string вместо object
        ],
        "authApplicationId": [
          16777216
        ],
        "vendorSpecificApplicationId": [
          {
            "vendorId": 10415,
            "authApplicationId": 16777216
          }
        ],
        "inbandSecurityId": [
          0
        ],
        "supportedVendorId": [
          10415
        ]
      }
    }
  ]
}
```
-->