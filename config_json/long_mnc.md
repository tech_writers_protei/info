---
title: "long_mnc.json"
description: "Перечисление PLMN с трёхзначными MNC"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/long_mnc.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаётся массив PLMN, имеющих трёхзначные MNC.

Ключ для перегрузки — **reload long_mnc.json**.

### Описание параметров ###

| Параметр          | Описание                                    | Тип    | По умолчанию | O/M | P/R | Версия   |
|-------------------|---------------------------------------------|--------|--------------|-----|-----|----------|
| mcc               | Мобильный код страны.                       | строка | -            | M   | R   | |
| mnc               | Код мобильной сети, состоящий из трёх цифр. | строка | -            | M   | R   | |

#### Пример ####

```json
[
  {
    "mcc": "404",
    "mnc": "854"
  },
  {
    "mcc": "404",
    "mnc": "874"
  }
]
```