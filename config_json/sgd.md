---
title: "sgd.json"
description: "Параметры интерфейса SGd"
weight: 40
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/sgd.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В этом файле задаются настройки интерфейса SGd.

Содержимое задаётся в виде массива строк, которые представляют список адресов SC, для которых используется интерфейс SGd.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload sgd.json**.

### Пример ###

```json
[
  "73469010001",
  "73469876543"
]
```