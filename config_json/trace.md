---
title: "trace.json"
description: "Параметры ведения xDR и log"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/trace.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются настройки подсистемы журналирования.

**Примечание.** Наличие файла обязательно.

Ключ для перегрузки — **reload trace.json**.

### Описание параметров ###

| Параметр          | Описание                                                                                                                                                                                                                                                                                                                                                                  | Тип         | По умолчанию                     | O/M | P/R | Версия |
|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|----------------------------------|-----|-----|--------|
| [common](#common) | Общие настройки системы журналирования.                                                                                                                                                                                                                                                                                                                                   | объект      | -                                | O   | R   |        |
| remoteSide        | Формат: `{"ip": <строка>, "port": <число>}`                                                                                                                                                                        | объект      | -                                | O   | R   |        |
| [logs](#logs)     | Конфигурация журналов.                                                                                                                                                                                                                                                                                                                                                    | массив объектов | -                                | O   | R   |        |


### Формат common {#common} ###

| Параметр                             | Описание                                                                                                                                                                                                                                                                                                                                                                  | Тип         | По умолчанию                     | O/M | P/R | Версия |
|--------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|----------------------------------|-----|-----|--------|
| tracing                              | Флаг активности системы журналирования.                                                                                                                                                                                                                                                                                                                                   | логический  | true                             | O   | R   |        |
| dir                                  | Путь к директории, где находятся журналы.<br>**Примечание.** Путь может содержать ".." и маску формата времени.                                                                                                                                                                                                                                                           | строка      | от каталога по умолчанию         | O   | R   |        |
| noSignal                             | Коды сигналов, не перехватываемых системой журналирования.<br>Значение "all" — не перехватывать никакие сигналы.                                                                                                                                                                                                                                                           | массив чисел/строка | перехватывать все сигналы        | O   | R   |        |
| <a name="separator">separator</a>    | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.                                                                                                                                                                                                                                                 | строка      | " "                              | O   | R   |        |
| localWrite                           |                                                                                                                                                                                                                                                                                                                                                                           | логический  | true                             | O   | R   |        |

### Формат logs {#logs} ###

| Параметр                    | Описание                                                                                                                                                                                                                                                                                                                                                                  | Тип         | По умолчанию                     | O/M | P/R | Версия |
|-----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|----------------------------------|-----|-----|--------|
| logName                     | Наименование журнала.                                                                                                                                                                                                                                                                                                                                                     | строка      | -                                | O   | R   |        |
| aliases                     |                                                                                                                                                                                                                                                                                                                                                                           | массив строк | -                                | O   | R   |        |
| [mask](#mod-mask)           | Маска формата вывода автоматических полей в журнале.                                                                                                                                                                                                                                                                                                                      | массив строк | -                                | O   | R   |        |
| file                        | Путь к файлу лога.<br>**Примечание.** При указании не существующих директорий система создает все необходимые каталоги. Допускается задание пустого имени файла, если `level = 0`. В этом случае запись производится согласно параметру [tee](#tee). При отсутствии этого параметра запись на диск не производится.<br>Путь может содержать ".." и маску формата времени. | строка      | от каталога по умолчанию         | O   | R   |        |
| level                       | Уровень журнала.<br>**Примечание.** Сообщения с большим уровнем игнорируются.                                                                                                                                                                                                                                                                                             | число       | -                                | O   | R   |        |
| localLevel                  |                                                                                                                                                                                                                                                                                                                                                                           | число       | -                                | O   | R   |        |
| [period](#mod-period)       | Период обновления файла лога. Формат:<br>`<interval>+<shift>`<br>interval — интервал между соседними обновлениями;<br>shift — первоначальный сдвиг.<br>**Примечание.** Сдвиг не может превышать длину периода, и в случае некорректного значения игнорируется.                                                                                                            | строка      | -                                | O   | R   |        |
| \<interval\>                | Период между соседними обновлениями.                                                                                                                                                                                                                                                                                                                                      | строка      | -                                | O   | R   |        |
| \<shift\>                   | Первоначальный сдвиг.                                                                                                                                                                                                                                                                                                                                                     | units       | -                                | O   | R   |        |
| [buffering](#mod-buffering) | Настройки буферизированной записи.                                                                                                                                                                                                                                                                                                                                        | объект      | -                                | O   | R   |        |
| separator                   | Разделитель автоматических полей.<br>**Примечание.** Весь вывод времени `date, time, tick` рассматривается как одно поле.                                                                                                                                                                                                                                                 | строка      | значение [separator](#separator) | O   | R   |        |
| [type](#mod-type)           | Тип журнала и дополнительные настройки.                                                                                                                                                                                                                                                                                                                                   | массив строк |                                  | O   | R   |        |
| <a name="tee">tee</a>       | Дублирование потока вывода.<br>stdout/cout/info/\<log_file_name\>.<br>**Примечание.** При знаке минуса "–" не пишется имя исходного лога при дублировании.                                                                                                                                                                                                                | массив строк | -                                | O   | R   |        |
| limit                       | Максимальное количество строк в файле.<br>**Примечание.** По достижении предела строк файл автоматически открывается заново. Действительное количество строк в файле не исследуется. Если имя файла зависит от времени, то открывается новый файл, иначе файл очищается.                                                                                                  | число       | -                                | O   | R   |        |
| forceRecreate               | Флаг создания пустых журналов по прошествии периода [period](#period).                                                                                                                                                                                                                                                                                                     | логический  | false                            | O   | R   |        |

### Модификаторы mask {#mod-mask} ###

Маска формата вывода автоматических полей в журнале. Возможные модификаторы: `date, time, tick, state, pid, tid, level, file`.

| Модификатор             | Описание                                                                                                |
|-------------------------|---------------------------------------------------------------------------------------------------------|
| date                    | Дата создания. Формат: `DD/MM/YY`                                                                       |
| <a name="time">time</a> | Время создания. Формат: `hh:mm:ss`                                                                      |
| tick                    | Миллисекунды. Формат:<br>если задано [time](#time): `.mss`;<br>если не задано [time](#time): `.mssmss`. |
| state                   | Состояние системы.                                                                                      |
| pid                     | Идентификатор процесса. Формат: `xxxxxx`                                                                |
| tid                     | Идентификатор потока. Формат: `xxxxxx`                                                                  |
| level                   | Уровень журнала для записи.                                                                             |
| file                    | Файл и строка в файле с исходным кодом, откуда производится вывод.                                      |

### Формат period {#mod-period} ###

| Параметр | Описание                                                                                                               | Тип    | По умолчанию |
|----------|------------------------------------------------------------------------------------------------------------------------|--------|--------------|
| count    | Количество стандартных периодов.                                                                                       | число  | -            |
| type     | Единицы измерения периода.<br>sec - секунда/min - минута/hour - час/day - день/week - неделя/month - месяц/year - год. | строка | -            |

**Пример:** `day+3hour` - файл обновляется каждый день в 3 часа ночи.

### Модификаторы type {#mod-type} ###

Три пары взаимоисключающих значений: log/cdr, truncate/append, name_now/name_period.

| Модификатор                     | Описание                                                                                            |
|---------------------------------|-----------------------------------------------------------------------------------------------------|
| <a name="name-now">name_now</a> | Текущее время для имени файла.                                                                      |
| name_period                     | Начало периода записи.                                                                              |
| <a name="truncate">truncate</a> | Флаг очистки файла при открытии.                                                                    |
| <a name="append">append</a>     | Файл добавления информации в конец файла.                                                           |
| <a name="log">log</a>           | Состоит из [truncate](#truncate) и [name_now](#name-now), при падении пишется информация о сигнале. |
| <a name="cdr">cdr</a>           | Состоит из [append](#append) и [name_now](#name-now), при падении не пишется информация о сигнале   |

### Формат buffering {#mod-buffering} ###

| Параметр                                | Описание                                                                                          | Тип         | По умолчанию |
|-----------------------------------------|---------------------------------------------------------------------------------------------------|-------------|--------------|
| <a name="cluster-size">clusterSize</a>  | Размер кластера.                                                                                  | число<br>Кб | 128          |
| clustersInBuffer                        | Количество кластеров [clusterSize](#cluster-size) в буфере.                                       | число       | 0            |
| overflowAction                          | Действие, выполняемое при переполнении буфера.<br>`erase` — удаление;<br>`dump` — запись на диск. | строка      | dump         |

### Зарезервированные имена журналов ###

* **stdout** — стандартный вывод;
* **stderr** — стандартный вывод ошибок;
* **trace** — журнал по умолчанию;
* **warning** — журнал предупреждений;
* **error** — журнал ошибок;
* **config** — журнал чтения конфигурации;
* **info** — журнал информации о событиях, адаптирован для стороннего пользователя.

#### Пример ####

```json
{
  "common": {
    "tracing": true,
    "dir": ".",
    "noSignal": "all"
  },
  "logs": [
    {
      "logName": "connect_cdr",
      "file": "cdr/s1ap/connect-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 2,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "dedicated_bearer_cdr",
      "file": "cdr/s1ap/dedicated_bearer-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
	  },
    {
      "logName": "diam_cdr",
      "file": "cdr/diam/diam-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "level": 2,
      "separator": ","
    },
    {
      "logName": "enodeb_cdr",
      "file": "cdr/s1ap/enodeB-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "gtp_c_cdr",
      "file": "cdr/gtp_c/gtp_c-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "period": "1day",
      "level": 2,
      "separator": ","
    },
    {
      "logName": "http_cdr",
      "file": "cdr/http/http-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "irat_handover_cdr",
      "file": "cdr/s1ap/irat_handover-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "lte_handover_cdr",
      "file": "cdr/s1ap/lte_handover-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "paging_cdr",
      "file": "cdr/s1ap/paging_cdr-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "reject_cdr",
      "file": "cdr/reject/reject-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "s1ap_cdr",
      "file": "cdr/s1ap/s1ap-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "s1ap_context_cdr",
      "file": "cdr/s1ap/s1ap_context-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "s1ap_overload_cdr",
      "file": "cdr/s1ap_overload/s1ap_overload-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "period": "1hour",
      "level": 2,
      "separator": ","
    },
    {
      "logName": "sgsap_cdr",
      "file": "cdr/sgsap/sgsap-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time"],
      "period": "1day",
      "level": 1,
      "separator": ","
    },
    {
      "logName": "tau_cdr",
      "file": "cdr/s1ap/tau-%Y%m%d-%H%M.cdr",
      "mask": ["date", "time", "tick"],
      "period": "1day",
      "level": 1,
      "tee": ["s1ap_cdr"],
      "separator": ","
    },
    {
      "logName": "alarm_cdr",
      "file": "logs/alarm/alarm-cdr.log",
      "period": "hour",
      "mask": ["date", "time", "tick"],
      "separator": ";",
      "level": 6
    },
    {
      "logName": "alarm_trace",
      "file": "logs/alarm/alarm_trace.log",
      "period": "hour",
      "mask": ["date", "time", "tick"],
      "separator": ";",
      "level": 6
    },
    {
      "logName": "bc_trace",
      "file": "logs/bc_trace.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "bc_warning",
      "file": "logs/bc_warning.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "COM_warning",
      "file": "logs/com_warning.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "config",
      "file": "logs/config.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10
    },
    {
      "logName": "db_trace",
      "file": "logs/db_trace.log",
      "mask": ["file", "date", "time", "tick"],
      "period": "1hour",
      "level": 10
    },
    {
      "logName": "diam_info",
      "file": "logs/diam_info.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "diam_trace",
      "file": "logs/diam/diam_trace-%Y%m%d-%H%M.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "period": "hour",
      "level": 19
    },
    {
      "logName": "diam_warning",
      "file": "logs/diam_warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "dns_trace",
      "file": ["file", "date", "time", "tick"],
      "level": 10,
      "period": "day"
    },
    {
      "logName": "dns_warning",
      "file": "logs/dns_warning.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "period" : "day"
    },
    {
      "logName": "GTP_C_trace",
      "file": "logs/gtp_c/gtp_c_trace-%Y%m%d-%H%M.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "period": "hour",
      "level": 11
    },
    {
      "logName": "GTP_C_warning",
      "file": "logs/gtp_c_warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "http_trace",
      "file": "logs/http_trace.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "separator": ";"
    },
    {
      "logName": "mme_config",
      "file": "logs/mme_config.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10
    },
    {
      "logName": "profilers",
      "file": "logs/profilers/profilers_%Y%m%d-%H%M.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "period": "hour",
      "separator": ";"
    },
    {
      "logName": "S1AP_trace",
      "file": "logs/s1ap/s1ap_trace-%Y%m%d-%H%M.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "period": "hour",
      "level": 10
    },
    {
      "logName": "S1AP_warning",
      "file": "logs/s1ap_warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": ["date", "time", "tick", "pid", "file"],
      "file": "logs/sctp_binary.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 0,
      "separator": ";"
    },
    {
      "logName": "Sg_info",
      "file": "logs/sg/sg_info.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "Sg_trace",
      "file": "logs/sg/sg_trace.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "Sg_warning",
      "file": "logs/sg/sg_warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "SGsAP_trace",
      "file": "logs/sgsap/sgsap_trace-%Y%m%d-%H%M.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "period": "hour",
      "level": 10
    },
    {
      "logName": "SGsAP_warning",
      "file": "logs/sgsap_warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10
    },
    {
      "logName": "si",
      "file": "logs/si/trace.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10,
      "tee": ["trace", "fsm"]
    },
    {
      "logName": "si_info",
      "file": "logs/si/info.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10,
      "tee": ["trace"]
    },
    {
      "logName": "si_warning",
      "file": "logs/si/warning.log",
      "mask": ["date", "time", "tick", "pid", "file"],
      "level": 10,
      "trace": ["trace"]
    },
    {
      "logName": "trace",
      "file": "logs/trace/trace_%Y%m%d-%H%M.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 11,
      "period": "hour",
      "separator": ";"
    },
    {
      "logName": "ue_trace",
      "file": "logs/trace/ue_trace_%Y%m%d-%H%M.log",
      "mask": ["file", "date", "time", "tick"],
      "level": 10,
      "period": "hour",
      "separator": ";"
    }
  ]
}
```