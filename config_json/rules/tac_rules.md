---
title: "tac_rules.json"
description: "Правила, связываюшие TAC с различными параметрами"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/rules/tac_rules.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются правила, связывающие <abbr title="Tracking Area Code">TAC</abbr> с дополнительными параметрами. Имя правила может использоваться в качестве ссылки в параметре [plmn::tacRules](../../plmn/#tac-rules).

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| name     | Имя правила| строка | -       |  M  |  R  |        |
| <a name="tacList">tacList</a> | Набор кодов зон отслеживания. См. **[Формат TAC](#tac_format)** ниже. | массив | - | M | R | |
| mmeIp | IP-адрес узла MME. | строка | - | O | R | |
| sgw | Набор IP-адресов, <abbr title="Fully Qualified Domain Name">FQDN</abbr>, весов и приоритетов узла SGW. Формат см. [ниже](#address-format). | массив объектов | - | O | R | |
| imsVops | Флаг IMS-VoPS-3GPP. См. [3GPP TS 24.501](https://www.etsi.org/deliver/etsi_ts/124500_124599/124501/17.10.01_60/ts_124501v171001p.pdf).<br>**Примечание.** Учитывается только при активации [plmn::imsVops](../../plmn/#ims-vops). | логический | true | O | R | |
| extraApnLabel | Дополнительный идентификатор, используемый при поиске адреса PGW в DNS. | строка | - | O | R | |
| enableEmergencyPdn | Флаг разрешения использовать <abbr title="Packet Data Network">PDN</abbr> экстренных служб в зоне отслеживания. | логический | true | O | R | |

### Формат TAC {#tac_format} ###

[tacList](#tacList) определяет массив значений TAC. Значения могут задаваться в виде:
- десятичных чисел
- шестнадцатиричных чисел - строка, начинающаяся с "0x", считается таковым
- диапазона, который представлен парой крайних чисел (dec или hex), разделённых дефисом

### Параметры адреса {#address-format}

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| fqdn| Полностью определённое доменное имя. | строка | - | O | R | |
| ip   | IP-адрес. | строка | - | M | R | |
| weight | Вес адреса.<br>**Примечание.** Если 0 или отсутствует, то адрес игнорируется.<br>Если указан единственный адрес без указания веса, то присваивается значение&nbsp;1. | число<br>0-65&nbsp;535 | - | O | R | |
| priority | Приоритет адреса.<br>Чем меньше значение, тем выше приоритет. | число<br>0-65&nbsp;535 | 0 | O | R | |

#### Пример ####

```json
[
  {
    "name": "Rule_all",
    "tacList" : [ 43690, 5234, "123-126", "0xfa" ],
    "mmeIp" : "192.168.128.10",
    "sgw" : [
      {
        "fqdn": "net.3gppnetwork.org",
        "ip": "198.18.1.13",
        "weight": 1,
        "priority": 1
      }
    ],
    "extraApnLabel" : "sorm-mfi"
  },
  {
    "name": "Rule_short",
    "tacList" : [ "155-0xff" ],
    "sgwIp" : [
      {
        "ip": "198.18.1.13",
        "weight": 1,
        "priority": 1
      },
      {
        "ip": "198.18.1.15",
        "weight": 5,
        "priority": 1
      }
    ],
    "enableEmergencyPdn" : false
  }
]
```