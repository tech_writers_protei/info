---
title: "apn_rules.json"
description: "Правила, связываюшие APN NI с адресами PGW и SGW"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/rules/apn_rules.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются правила, связывающие APN NI с адресами PGW и SGW.
Имя правила может использоваться в качестве ссылки в параметре [plmn::apnRules](../../plmn/#apn-rules).

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| name     | Имя правила. | строка | - | M | R | |
| apnNi    | Регулярное выражение APN NI для правила. | строка | - | M | R | |
| [pgw](#parameterizedAddress) | Набор IP-адресов, <abbr title="Fully Qualified Domain Name">FQDN</abbr>, весов и приоритетов узла PGW. | массив объектов | - | M | R | |
| [sgw](#parameterizedAddress) | Набор IP-адресов, <abbr title="Fully Qualified Domain Name">FQDN</abbr>, весов и приоритетов узла SGW. | массив объектов | - | O | R | |

### Параметры адреса {#parameterizedAddress} ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| fqdn | Полностью определённое доменное имя. | строка | - | O | R | |
| ip   | IP-адрес. | строка | - | M | R | |
| weight | Вес адреса.<br>**Примечание.** Если 0 или отсутствует, то адрес игнорируется.<br>Если указан единственный адрес без указания веса, то присваивается значение&nbsp;1. | число<br>0-65&nbsp;535 | - | O | R | |
| priority | Приоритет адреса.<br>Чем меньше значение, тем выше приоритет. | число<br>0-65&nbsp;535 | 0 | O | R | |

#### Пример ####

```json
[
  {
    "name" : "FullRule",
    "apnNi" : "ims",
    "pgw" : [
      {
        "fqdn": "pgw1.s5s8.site.node.epc.mnc001.mcc001.3gppnetwork.org",
        "ip": "192.168.126.244",
        "weight": 1,
        "priority": 1
      }
    ],
    "sgw" : [ 
      {
        "ip": "192.168.0.19",
        "weight": 5,
        "priority": 2
      },
      {
        "ip": "192.168.0.20",
        "weight": 6,
        "priority": 2
      }
    ]
  },
  {
    "name" : "Rule_1",
    "apnNi" : "ims",
    "pgw" : [
      {
        "fqdn": "pgw1.s5s8.site.node.epc.mnc001.mcc001.3gppnetwork.org",
        "ip": "192.168.126.244",
        "weight": 1,
        "priority": 1
      }
    ],
    "sgw" : [ 
      {
        "ip": "192.168.0.19",
        "weight": 5,
        "priority": 2
      },
      {
        "ip": "192.168.0.20",
        "weight": 6,
        "priority": 2
      }
    ]
  },
  {
    "name" : "Rule_2",
    "apnNi" : "internet",
    "pgw" : [
      {
        "ip": "192.168.126.241"
      },
      {
        "ip": "192.168.126.242"
      }
    ]
  },
  {
    "name" : "Rule_3",
    "apnNi" : "internet",
    "sgw" : [ 
      {
        "fqdn": "sgw1.s11.site.node.epc.mnc0250mcc48.3gppnetwork.org",
        "ip": "192.168.0.19"
      }
    ],
    "pgw" : [
      {
        "ip": "192.168.126.244"
      }
    ]
  }
]
```