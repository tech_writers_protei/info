---
title: "imsi_rules.json"
description: "Правила, связывающие префиксы IMSI с различными параметрами"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/rules/imsi_rules.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются правила, связывающие префиксы IMSI с различными параметрами. Имя правила может использоваться в качестве ссылки в параметре [plmn::imsiRules](../../plmn/#imsiRules).

Набор правил позволяет формировать "белый" и "чёрный" списки IMSI. Если IMSI не попадает ни под одно правило, он считается участником "чёрного" списка. Если IMSI попал под правило, то дальше всё определяется наличием параметра [emmCause при prefix](#emm-cause-prefix) - если он указан, то IMSI оказывается в "чёрном" списке и будет с этим cause отбит, иначе - IMSI в "белом" списке.

Отсутствие правил делает любой IMSI участником "белого" списка. Пустой [prefix](#prefix) делает правило подходящим к любому IMSI.

Поиск соответствующего правила выполняется следующим образом:

1. Среди всех правил ищется такое, под [tacList](#tac-list) которого попадает текущий TAC абона
2. Если правило нашлось, то среди правил из списка [imsiList](#imsi-list) ищется такое, [prefix](#prefix) которого больше других совпадает с IMSI:
    - если правило нашлось, то параметры из правила применяются к абону
    - если правило не нашлось, абон получает отказ с причиной [emmCause при tacList](#emm-cause-tac)
3. Если правило в п.1 не нашлось, то среди всех правил с пустым [tacList](#tac-list) ищется такое, [prefix](#prefix) которого больше других совпадает с IMSI:
    - если правило нашлось, то его параметры применяются к абону
    - если правило не нашлось, абон получает отказ с причиной 11 (PLMN not allowed)

Во всех остальных ситуациях абон получает отказ с причиной 11 (PLMN not allowed)

### Описание параметров ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| name     | Имя правила. | строка | - | M | R | |
| <a name="tac-list">tacList</a> | Набор TAC, ограничивающих область действия правила. | массив строк, чисел | - | O | R | |
| <a name="emm-cause-tac">emmCause</a> | Код причины отбоя в сообщении для IMSI, префиксы которых не входят в список. | число | 11 (PLMN not allowed) | O | R | |
| <a name="imsi-list">imsiList</a> | Массив [правил](#imsiItem), привязанных к префиксам IMSI. | массив объектов | - | O | R | |

### Формат элемента imsiList {#imsiItem} ###

| Параметр | Описание | Тип | По умолчанию | O/M | P/R | Версия |
|----------|----------|-----|--------------|-----|-----|--------|
| <a name="prefix">prefix</a> | Префикс IMSI. | строка | - | M | R | |
| <a name="emm-cause-prefix">emmCause</a> | Код причины отбоя в сообщении. | число | не определён | O | R | |
| destRealm | Realm назначения. | строка | "" | O | R | |
| destHost | Хост назначения. | строка | "" | O | R | |
| apnOi | APN OI, прибавляемый к APN NI для формирования полного APN. | строка | если не определён или пуст, APN OI будет сформирован либо из IMSI, либо из PLMN | O | R | |
| <a name="qos-rules">qosRules</a> | Список применяемых [правил QoS](../qos_rules/). | массив строк | [] | O | R | |
| imsVops  | Флаг `IMS Voice over PS session indicator`. См. [3GPP TS 24.301](https://www.etsi.org/deliver/etsi_ts/124300_124399/124301/17.10.00_60/ts_124301v171000p.pdf).<br>**Примечание.** Учитывается только при активации [PLMN::imsVops](../../plmn/#ims-vops) и отсутствии запрета со стороны [PLMN::tacRules](../../plmn/#tac-rules). | логический | true | O | R | |
| featureList1 | Битовая маска опций Feature-List-ID 1. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf). | hex-число (строка) | 0x10000007 | O | R | |
| featureList2 | Битовая маска опций Feature-List-ID 2. Cм. [3GPP TS 29.272](https://www.etsi.org/deliver/etsi_ts/129200_129299/129272/17.04.00_60/ts_129272v170400p.pdf). | hex-число (строка) | 0x08000000 | O | R | |
| foreignUeUsageType | UE Usage Type. При наличии, вынуждает ММЕ отбивать запросы Attach Req и TAU Req с сообщением Reroute NAS Req. | число, от 0 до 255 | не определён | O | R | |

#### Пример ####

```json
[
  {
    "name" : "RuleName1",
    "tacList" : [1, "3-10"],
    "emmCause" : 11,
    "imsiList" : [
      {
        "prefix": "2502010",
        "destRealm": "EPC.MNC020.MCC250.3GPPNETWORK.ORG",
        "destHost": "SAE-HSS01.NSK.EPC.MNC020.MCC250.3GPPNETWORK.ORG",
        "apnOi": "mnc040.mcc250.gprs",
        "qosRules": [ "Qos_Rule1" ],
        "imsVops": true,
        "featureList1": "0x00000000",
        "featureList2": "0x00000000"
      },
      {
        "prefix": "25048123",
        "destRealm": "EPC.MNC048.MCC250.3GPPNETWORK.ORG",
        "destHost": "SAE-HSS01.NSK.EPC.MNC048.MCC250.3GPPNETWORK.ORG",
        "qosRules": [ "Qos_Rule2", "Qos_Rule3" ],
        "imsVops": false,
        "featureList1": "0x00000001",
        "featureList2": "0x00000000",
        "foreignUeUsageType": 15
      }
    ]
  },
  {
    "name" : "RuleName2",
    "imsiList" : [
      {
        "prefix": "25048"
      }
    ]
  }
]
```