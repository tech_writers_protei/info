---
title: "forbidden_imei_rules.json"
description: "Правила, хранящие запрещённые IMEI"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/rules/forbidden_imei_rules.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются правила с перечислением масок запрещённых IMEI.  
Имя правила может использоваться в качестве ссылки 
в параметре [plmn::forbiddenImeiRules](../../plmn/#forbidden-imei-rules).

### Описание параметров ###

| Параметр | Описание                                                          | Тип    | По умолчанию | O/M | P/R | Версия |
|----------|-------------------------------------------------------------------|--------|--------------|-----|-----|--------|
| name     | Имя правила.                                                      | строка | -            |  M  |  R  |        |
| imei     | Массив строковых значений, представляющих собой запрещенный IMEI. | массив | -            |  M  |  R  |        |

#### Пример ####

```json
[
  {
    "name": "IMEI_list_1",
    "imei": ["380715137821", "380715131100"]
  },
  {
    "name": "IMEI_list_2",
    "imei": ["380715137876", "380715[0-9]5"]
  }
]
```