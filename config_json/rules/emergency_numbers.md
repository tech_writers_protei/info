---
title: "emergency_numbers.json"
description: "Параметры номеров экстренных служб"
weight: 20
type: docs
gitlab_docs_enable_new_issue: false
gitlab_docs_enable_edit_ide: true
gitlab_docs_enable_edit: true
gitlab_docs_path: "content/private/config_json/rules/emergency_numbers.md"
gitlab_docs_branch: "pages"
gitlab_project_name: "Protei_MME"
gitlab_project_path: "MobileDevelop/Protei_MME"
---

В файле задаются связи между номерами экстренных служб и их типами. Имя секции может использоваться в качестве ссылки в параметре [plmn::emergencyNumbers](../../plmn/#emergency-numbers).

### Описание параметров ###

| Параметр       | Описание                                                | Тип        | По умолчанию | O/M | P/R | Версия |
|----------------|---------------------------------------------------------|------------|--------------|-----|-----|--------|
| name           | Имя.                                                    | строка     | -            |  M  |  R  |        |
| number         | Номер.                                                  | строка     | -            |  M  |  R  |        |
| police         | Флаг использования номера для полиции.                  | логический | false        |  O  |  R  |        |
| ambulance      | Флаг использования номера для cкорой помощи.            | логический | false        |  O  |  R  |        |
| fireBrigade    | Флаг использования номера для пожарной охраны.          | логический | false        |  O  |  R  |        |
| marineGuard    | Флаг использования номера для береговой охраны.         | логический | false        |  O  |  R  |        |
| mountainRescue | Флаг использования номера для горноспасательной службы. | логический | false        |  O  |  R  |        |

#### Пример ####

```json
[
  {
    "name": "112",
    "number": "112",
    "police": true,
    "ambulance": true,
    "fireBrigade": true,
    "marineGuard": true,
    "mountainRescue": true
  },
  {
    "name": "911",
    "number": "911",
    "ambulance": true,
    "fireBrigade": true,
    "marineGuard": true,
    "mountainRescue": true
  },
  {
    "name": "AmbulanceOnly",
    "number": "03",
    "ambulance": true
  }
]
```